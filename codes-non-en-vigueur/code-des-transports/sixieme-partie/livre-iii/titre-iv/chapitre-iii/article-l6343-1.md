# Article L6343-1

En vue d'assurer préventivement la sûreté des vols, le transporteur aérien met en œuvre les mesures de sûreté sur le fret et les colis postaux préalablement à leur embarquement dans les aéronefs.

Le transporteur aérien :

1° Soit effectue les visites de sûreté mentionnées à l'article L. 6342-2 du fret et des colis postaux qui lui sont remis ;

2° Soit s'assure que ce fret ou ces colis postaux lui sont remis par un agent habilité.

Le fret ou les colis postaux qui ne peuvent faire l'objet de contrôle après leur conditionnement en raison de leurs caractéristiques sont remis à l'agent habilité exclusivement par un chargeur connu.

Peut être agréé en qualité d'agent habilité par l'autorité administrative l'entreprise ou l'organisme qui intervient dans l'organisation du transport de fret ou de colis postaux et qui met en place des contrôles et des procédures appropriées.

Peut être agréé en qualité de chargeur connu par l'autorité administrative l'entreprise ou l'organisme qui met en œuvre directement ou sous son contrôle des mesures appropriées pendant le conditionnement du fret et des colis postaux expédiés à sa demande et qui préserve l'intégrité de ces marchandises jusqu'à leur remise à un agent habilité.
