# Article L6325-1

Les services publics aéroportuaires rendus sur les aérodromes ouverts à la circulation aérienne publique donnent lieu à la perception de redevances pour services rendus fixées conformément au deuxième alinéa de l'article L. 410-2 du code de commerce.

Le montant des redevances tient compte de la rémunération des capitaux investis. Il peut tenir compte des dépenses, y compris futures, liées à la construction d'infrastructures ou d'installations nouvelles avant leur mise en service.

Il peut faire l'objet, pour des motifs d'intérêt général, de modulations limitées tendant à réduire ou compenser les atteintes à l'environnement, améliorer l'utilisation des infrastructures, favoriser la création de nouvelles liaisons ou répondre à des impératifs de continuité et d'aménagement du territoire.

Le produit global de ces redevances ne peut excéder le coût des services rendus sur l'aéroport.
