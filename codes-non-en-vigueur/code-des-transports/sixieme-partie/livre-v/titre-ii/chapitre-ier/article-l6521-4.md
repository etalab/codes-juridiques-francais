# Article L6521-4

L'activité de pilote ou de copilote, mentionnée au 1° de l'article L. 6521-1, ne peut être exercée dans le transport aérien public au-delà de l'âge de soixante ans.

Toutefois, l'intéressé peut être maintenu en activité s'il en formule la demande et répond aux conditions de validité des titres aéronautiques mentionnées au 1° de l'article L. 6521-2 ainsi qu'à la vérification de son aptitude médicale et des conditions de réalisation des vols en équipage, déterminées par décret en Conseil d'Etat.

Cette demande doit, si l'intéressé souhaite pouvoir continuer à exercer l'activité de pilote ou de copilote, être renouvelée chaque année, et en dernier lieu l'année précédant son soixante-cinquième anniversaire.

L'intéressé peut à tout moment, à partir de l'âge de soixante ans, demander à bénéficier d'un reclassement dans un emploi au sol.

Le contrat de travail du navigant n'est pas rompu du seul fait d'une demande de reclassement ou du fait que la limite d'âge mentionnée au premier ou au troisième alinéa est atteinte, sauf s'il est impossible à l'employeur de lui proposer un reclassement dans un emploi au sol ou si l'intéressé refuse d'accepter l'emploi qui lui est proposé.
