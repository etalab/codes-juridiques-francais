# Article L6523-5

Le contrat conclu pour une durée déterminée indique le lieu de destination finale de la mission.

Le contrat de travail à durée déterminée dont le terme survient au cours d'une mission est prorogé jusqu'à l'achèvement de la mission.
