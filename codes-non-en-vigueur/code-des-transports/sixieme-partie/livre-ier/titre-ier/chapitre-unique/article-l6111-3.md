# Article L6111-3

Un aéronef ne peut être immatriculé en France que s'il remplit l'une des conditions suivantes :

1° Il appartient à une personne physique française ou ressortissante d'un autre Etat membre de l'Union européenne ou d'un Etat partie à l'accord sur l'Espace économique européen ;

2° Il appartient à une personne morale constituée en conformité avec la législation d'un Etat membre de l'Union européenne ou d'un Etat partie à l'accord sur l'Espace économique européen et ayant son siège statutaire ou son principal établissement sur le territoire de la République française ou d'un autre Etat membre de l'Union européenne ou d'un autre Etat partie à l'accord sur l'Espace économique européen ;

3° Il est exploité par un transporteur aérien dont la licence d'exploitation a été délivrée par l'autorité administrative française.

Les conditions d'application du présent article et les conditions dans lesquelles l'autorité administrative peut, par dérogation, immatriculer des aéronefs exploités en France mais ne remplissant pas ces conditions sont fixées par arrêté ministériel.
