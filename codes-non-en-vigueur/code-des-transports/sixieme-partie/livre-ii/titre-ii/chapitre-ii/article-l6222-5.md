# Article L6222-5

Les recommandations de sécurité prévues par l'article L. 1621-20 ne s'appliquent qu'à un accident ou un incident d'aviation civile répondant au caractère de gravité défini par l'article L. 6222-3.
