# Article L6731-1

Pour l'application à Saint-Barthélemy de l'article L. 6332-2, les mots : " impartis au maire par l'article L. 2212-2 du code général des collectivités territoriales " sont remplacés par les mots : " impartis au président du conseil territorial par l'article LO 6252-8 du code général des collectivités territoriales ".
