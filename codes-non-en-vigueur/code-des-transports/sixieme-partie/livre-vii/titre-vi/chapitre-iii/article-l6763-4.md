# Article L6763-4

Pour l'application en Nouvelle-Calédonie de l'article L. 6332-2, les mots : " par l'article L. 2212-2 du code général des collectivités territoriales " sont remplacés par les mots : " par l'article L. 131-2 du code des communes de Nouvelle-Calédonie ".
