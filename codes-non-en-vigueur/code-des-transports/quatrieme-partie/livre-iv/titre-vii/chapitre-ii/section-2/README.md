# Section 2 : Saisie du bateau ou du navire

- [Article L4472-3](article-l4472-3.md)
- [Article L4472-4](article-l4472-4.md)
- [Article L4472-5](article-l4472-5.md)
- [Article L4472-6](article-l4472-6.md)
- [Article L4472-7](article-l4472-7.md)
- [Article L4472-8](article-l4472-8.md)
