# Article L4322-19

Lorsque Port autonome de Paris est substitué à des collectivités publiques ou établissements publics concessionnaires d'outillage portuaire, dans les conditions prévues par l'article L. 4322-3, le concessionnaire lui remet gratuitement les terrains, immeubles et outillages compris dans sa concession, les matériels et approvisionnements nécessaires à la gestion de cette concession, ainsi que tous les éléments d'activité détenus par lui au titre de sa concession.
