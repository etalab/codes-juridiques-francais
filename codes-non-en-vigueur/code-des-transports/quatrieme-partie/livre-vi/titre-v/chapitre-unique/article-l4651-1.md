# Article L4651-1

Les dispositions du titre Ier du livre III et des articles L. 4413-1 et L. 4463-4 ne sont pas applicables à Saint-Pierre-et-Miquelon.
