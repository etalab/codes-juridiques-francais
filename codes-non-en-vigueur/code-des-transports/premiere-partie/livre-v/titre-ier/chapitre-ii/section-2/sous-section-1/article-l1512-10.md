# Article L1512-10

Le président du fonds mentionné à l'article L. 1512-6 est nommé par l'autorité administrative compétente sur proposition du conseil d'administration parmi ses membres.
