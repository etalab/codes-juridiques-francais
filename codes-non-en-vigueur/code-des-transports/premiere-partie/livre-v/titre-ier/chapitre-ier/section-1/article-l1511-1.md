# Article L1511-1

Les choix relatifs aux infrastructures, aux équipements et aux matériels de transport dont la réalisation repose, en totalité ou en partie, sur un financement public sont fondés sur l'efficacité économique et sociale de l'opération.

Ils tiennent compte des besoins des usagers, des impératifs de sécurité et de protection de l'environnement, des objectifs de la politique d'aménagement du territoire, des nécessités de la défense, de l'évolution prévisible des flux de transport nationaux et internationaux, du coût financier et, plus généralement, des coûts économiques réels et des coûts sociaux, notamment de ceux résultant des atteintes à l'environnement.
