# Section 3 : La mise en œuvre de la continuité du service public

- [Article L1222-8](article-l1222-8.md)
- [Article L1222-9](article-l1222-9.md)
- [Article L1222-10](article-l1222-10.md)
- [Article L1222-11](article-l1222-11.md)
- [Article L1222-12](article-l1222-12.md)
