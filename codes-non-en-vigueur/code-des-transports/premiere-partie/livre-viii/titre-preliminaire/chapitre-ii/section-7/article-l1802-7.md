# Article L1802-7

Les dispositions du présent code applicables en Polynésie française sont ainsi adaptées :

1° Le représentant de l'Etat en Polynésie française exerce les attributions dévolues au préfet de département et de région ;

2° Le délégué du Gouvernement pour l'action de l'Etat en mer exerce les attributions du préfet maritime ;

3° Le chef du service des affaires maritimes exerce les attributions du directeur départemental des territoires et de la mer ;

4° Les attributions du tribunal de grande instance et de son président ainsi que celles du tribunal d'instance et de son président sont exercées par le tribunal de première instance et son président ;

5° Les références au code du travail sont remplacées par des références aux textes de droit du travail applicables en Polynésie française ;

6° Les références au code de l'urbanisme et au code du commerce sont remplacées par des références aux textes applicables localement en la matière.
