# Article L1802-8

Les dispositions du présent code applicables à Wallis-et-Futuna sont ainsi adaptées :

1° Le représentant de l'Etat à Wallis-et-Futuna exerce les attributions dévolues au préfet de département et de région ;

2° Le délégué du Gouvernement pour l'action de l'Etat en mer exerce les attributions du préfet maritime ;

3° Le chef du service des affaires maritimes exerce les attributions du directeur départemental des territoires et de la mer ;

4° Les attributions du tribunal de grande instance et de son président ainsi que celles du tribunal d'instance et de son président sont exercées par le tribunal de première instance et son président ;

5° Les références au code du travail sont remplacées par des références à la loi n° 52-1322 du 15 décembre 1952 modifiée instituant un code du travail dans les territoires et territoires associés relevant du ministère chargé de l'outre-mer ;

6° Les références au code de l'urbanisme sont remplacées par des références aux textes applicables localement en la matière.
