# Article L1802-3

Pour leur application à Saint-Barthélemy, les dispositions du présent code sont ainsi adaptées :

1° Le représentant de l'Etat à Saint-Barthélemy exerce les attributions dévolues aux préfets de département et aux préfets de région ;

2° Le conseil territorial de Saint-Barthélemy et son président exercent les attributions dévolues aux conseils généraux et régionaux et à leurs présidents respectifs ;

3° Les références à la commune, au département ou à la région sont remplacées par des références à la collectivité d'outre-mer de Saint-Barthélemy ;

4° Le délégué du Gouvernement pour l'action de l'Etat en mer exerce les attributions dévolues au préfet maritime ;

5° Le directeur départemental des territoires et de la mer de la Guadeloupe exerce également ses attributions à Saint-Barthélemy ;

6° Les références au code général des impôts, au code de l'urbanisme et au code de l'environnement sont remplacées par des références aux textes applicables localement en matière fiscale, d'urbanisme et d'environnement.
