# Article L1621-6

L'enquête technique mentionnée à l'article L. 1621-2 est effectuée par un organisme permanent spécialisé ou sous son contrôle dans les conditions suivantes :

1° Pour les événements de mer et les accidents ou incidents de transport terrestre, ont la qualité d'enquêteurs techniques les membres de l'organisme permanent, les membres des corps d'inspection et de contrôle auxquels l'organisme peut faire appel et, le cas échéant, les membres d'une commission d'enquête constituée à la demande de l'organisme auprès du ministre chargé des transports ;

2° Pour les accidents ou incidents d'aviation civile, ont seuls la qualité d'enquêteurs techniques les membres de l'organisme permanent. Toutefois des enquêteurs de première information appartenant aux corps techniques de l'aviation civile peuvent être agréés pour effectuer, sous le contrôle et l'autorité de l'organisme permanent, les opérations d'enquête prévues au présent titre. La commission d'enquête instituée, le cas échéant, par le ministre chargé des transports pour un accident d'aviation civile déterminé, assiste l'organisme permanent.
