# Article L1621-14

Les enquêteurs techniques peuvent rencontrer toute personne concernée et obtiennent, sans que puisse leur être opposé le secret professionnel, communication de toute information ou de tout document concernant les circonstances, entreprises, organismes et matériels en relation avec l'accident ou l'incident et concernant notamment la construction, la certification, l'entretien, l'exploitation des matériels, la préparation du transport, la conduite, l'information et le contrôle du ou des engins de transport impliqués.

Dans les mêmes conditions, les enquêteurs techniques peuvent demander communication de toute information ou de tout document à caractère personnel concernant la formation, la qualification, l'aptitude à la conduite des personnels ou le contrôle de ces engins. Toutefois, celles de ces informations qui ont un caractère médical ne peuvent être communiquées qu'aux médecins rattachés à l'organisme permanent ou désignés pour assister ces enquêteurs.

Il est établi une copie des documents placés sous scellés par l'autorité judiciaire à l'intention de ces enquêteurs.

Les conditions d'application du présent article sont fixées par décret en Conseil d'Etat.
