# Article L1621-2

Sous réserve des dispositions de l'article L. 1621-1, tout accident ou incident de transport terrestre, tout événement de mer, peut faire l'objet d'une enquête technique.

Il en va de même pour tout accident ou incident d'aviation civile. Toutefois, tout accident ou incident grave d'aviation civile au sens des dispositions de l'article L. 6222-3 fait l'objet d'une enquête technique dans les conditions fixées aux articles L. 6222-1 et suivants.
