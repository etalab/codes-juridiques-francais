# Section 6 : Pauses du personnel roulant ou navigant

- [Article L1321-9](article-l1321-9.md)
- [Article L1321-10](article-l1321-10.md)
