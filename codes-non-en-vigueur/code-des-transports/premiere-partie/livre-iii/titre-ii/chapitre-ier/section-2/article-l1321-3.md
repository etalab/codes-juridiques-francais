# Article L1321-3

Dans les branches mentionnées à l'article L. 1321-1, il peut être dérogé par convention ou accord collectif étendu ou par convention ou accord d'entreprise ou d'établissement aux dispositions réglementaires relatives :

1° A l'aménagement et à la répartition des horaires de travail à l'intérieur de la semaine ;

2° Aux conditions de recours aux astreintes ;

3° Aux modalités de récupération des heures de travail perdues ;

4° A la période de référence sur laquelle est calculée la durée maximale hebdomadaire moyenne de travail, dans la limite de quatre mois ;

5° A l'amplitude de la journée de travail et aux coupures.
