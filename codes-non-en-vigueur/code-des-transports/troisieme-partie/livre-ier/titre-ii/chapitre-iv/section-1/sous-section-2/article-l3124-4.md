# Article L3124-4

I. ― Est puni d'un an d'emprisonnement et de 15 000 € d'amende le fait d'effectuer à la demande et à titre onéreux le transport particulier de personnes et de bagages sans être titulaire d'une autorisation de stationnement sur la voie publique en attente de clientèle, ou d'exercer l'activité de conducteur de taxi sans être titulaire de la carte professionnelle en cours de validité.

II. - Les personnes physiques coupables de l'infraction prévue par le présent article encourent également les peines complémentaires suivantes :

1° La suspension, pour une durée de cinq ans au plus, du permis de conduire ;

2° L'immobilisation, pour une durée d'un an au plus, du véhicule qui a servi à commettre l'infraction ;

3° La confiscation du véhicule qui a servi à commettre l'infraction ;

4° L'interdiction, pour une durée de cinq ans au plus, d'entrer et de séjourner dans l'enceinte d'une ou plusieurs infrastructures aéroportuaires ou portuaires, d'une gare ferroviaire ou routière, ou de leurs dépendances, sans y avoir été préalablement autorisée par l'autorité de police compétente.
