# Article L3112-2

Tout contrat passé pour l'exécution de services occasionnels doit comporter des clauses précisant l'objet de la prestation et son prix, les droits et obligations des parties, l'affectation du personnel de conduite, les caractéristiques du matériel roulant ainsi que les conditions d'exécution du service notamment en fonction des personnes ou des groupes de personnes à transporter.

Ces contrats sont régis par l'article L. 1431-1.
