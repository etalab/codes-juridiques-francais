# Article L2111-13

Réseau ferré de France peut créer des filiales ou prendre des participations dans des sociétés, groupements ou organismes ayant un objet connexe ou complémentaire à ses missions.
