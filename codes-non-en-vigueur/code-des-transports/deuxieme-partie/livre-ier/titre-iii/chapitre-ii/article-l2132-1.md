# Article L2132-1

L'Autorité de régulation des activités ferroviaires est composée de sept membres nommés en raison de leur compétence en matière ferroviaire, économique ou juridique, ou pour leur expertise en matière de concurrence. Leur mandat est de six ans non renouvelable.
