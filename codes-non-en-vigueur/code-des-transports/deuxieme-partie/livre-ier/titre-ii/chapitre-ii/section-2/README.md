# Section 2 : Règles applicables au gestionnaire d'infrastructure

- [Article L2122-4](article-l2122-4.md)
- [Article L2122-5](article-l2122-5.md)
- [Article L2122-6](article-l2122-6.md)
- [Article L2122-7](article-l2122-7.md)
- [Article L2122-8](article-l2122-8.md)
