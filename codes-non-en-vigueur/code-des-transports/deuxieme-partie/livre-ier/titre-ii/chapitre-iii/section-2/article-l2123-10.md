# Article L2123-10

Les modalités d'application des articles L. 2123-5 à L. 2123-9, en particulier les règles de fonctionnement du service gestionnaire des trafics et des circulations sont précisées par décret en Conseil d'Etat.
