# Article L2123-11

Il est institué une commission qui est obligatoirement consultée lorsqu'un agent du service gestionnaire des trafics et des circulations ayant eu à connaître, dans l'exercice de ses fonctions, des informations dont la divulgation est visée aux articles L. 2123-5 à L. 2123-9, souhaite exercer des activités dans le secteur ferroviaire en dehors de ce service.

Cette commission rend un avis. Le cas échéant, elle peut fixer un délai avant l'expiration duquel l'agent ne peut exercer de nouvelles fonctions incompatibles avec ses fonctions précédentes. Pendant ce délai, l'agent est reclassé dans un poste de même niveau qui ne comporte pas d'incompatibilités au regard de ses fonctions précédentes ni de ses fonctions futures.

Cette commission est présidée par un magistrat de l'ordre judiciaire et comprend, en outre, un membre de l'Autorité de régulation des activités ferroviaires, le président de Réseau ferré de France ou son représentant, le directeur du service gestionnaire des trafics et des circulations ou son représentant et un représentant des agents du service gestionnaire des trafics et des circulations. Les conditions d'application du présent article sont fixées par décret en Conseil d'Etat.
