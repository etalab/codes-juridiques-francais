# Article L2141-12

La gestion des filiales créées par la Société nationale des chemins de fer français est autonome au plan financier dans le cadre des objectifs du groupe.

Ces filiales ne peuvent recevoir les concours financiers de l'Etat prévus par l'article L. 2141-19.
