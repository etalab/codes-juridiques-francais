# Chapitre Ier : Société nationale des chemins de fer français

- [Section 1 : Objet et missions](section-1)
- [Section 2 : Organisation administrative](section-2)
- [Section 3 : Gestion financière et comptable](section-3)
- [Section 4 : Gestion domaniale](section-4)
- [Section 5 : Contrôle de l'Etat](section-5)
- [Section 6 : Ressources de la Société nationale des chemins de fer français](section-6)
