# Article L2141-16

Les biens immobiliers détenus par la Société nationale des chemins de fer français qui cessent d'être affectés à la poursuite de ses missions peuvent recevoir une autre affectation domaniale ou, à défaut, après déclassement, être aliénés par elle et à son profit ; dans le premier cas, l'Etat ou la collectivité territoriale intéressée lui verse une indemnité égale à leur valeur vénale.
