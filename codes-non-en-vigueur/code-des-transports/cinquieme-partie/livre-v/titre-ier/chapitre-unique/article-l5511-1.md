# Article L5511-1

Pour l'application du présent livre, est considéré comme :

1° Armateur, toute personne pour le compte de laquelle un navire est armé ;

2° Entreprise d'armement maritime, tout employeur de salariés exerçant la profession de marin ;

3° Marin, toute personne remplissant les conditions mentionnées à l'article L. 5521-1, qui contracte un engagement envers un armateur ou s'embarque pour son propre compte, en vue d'occuper à bord d'un navire un emploi relatif à la marche, à la conduite, à l'entretien et au fonctionnement du navire ;

4° Gens de mer, tout marin ou toute autre personne exerçant, à bord d'un navire, une activité professionnelle liée à son exploitation.
