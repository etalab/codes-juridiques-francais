# Section 4 : Dispositions diverses

- [Article L5553-14](article-l5553-14.md)
- [Article L5553-15](article-l5553-15.md)
- [Article L5553-16](article-l5553-16.md)
