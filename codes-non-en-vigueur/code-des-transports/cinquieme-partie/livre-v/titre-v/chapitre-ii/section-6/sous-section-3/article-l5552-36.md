# Article L5552-36

Lorsque les enfants mineurs issus de plusieurs lits sont orphelins de père et de mère, la pension qui aurait été attribuée au conjoint survivant se partage, par parties égales, entre chaque groupe d'orphelins.

La pension temporaire est, dans ce cas, attribuée dans les conditions prévues aux articles L. 5552-31 à L. 5552-33 et L. 5552-35.
