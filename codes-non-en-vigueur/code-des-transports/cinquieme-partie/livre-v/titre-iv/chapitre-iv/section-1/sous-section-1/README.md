# Sous-section 1 : Travail effectif et astreintes

- [Article L5544-2](article-l5544-2.md)
- [Article L5544-3](article-l5544-3.md)
