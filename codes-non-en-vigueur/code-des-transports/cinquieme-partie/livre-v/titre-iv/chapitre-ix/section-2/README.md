# Section 2 : Durée du travail et salaire

- [Article L5549-2](article-l5549-2.md)
- [Article L5549-3](article-l5549-3.md)
