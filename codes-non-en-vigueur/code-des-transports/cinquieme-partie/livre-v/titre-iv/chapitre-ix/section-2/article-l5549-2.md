# Article L5549-2

Les règles particulières relatives à la durée du travail et au repos hebdomadaire des personnels n'exerçant pas la profession de marin et embarqués temporairement à bord d'un navire sont fixées par décret en Conseil d'Etat.

Lorsque ces règles particulières concernent les personnels de droit privé non marins des établissements publics de recherche à caractère industriel ou commercial, ou des groupements dans lesquels les établissements publics de recherche détiennent des participations majoritaires, embarqués à bord d'un navire de recherche océanographique ou halieutique, ce décret est pris après consultation des établissements et groupements ainsi que des organisations les plus représentatives de ces personnels.
