# Chapitre IX : Dispositions applicables aux gens de mer autres que les marins

- [Section 1 : Obligations de l'armateur](section-1)
- [Section 2 : Durée du travail et salaire](section-2)
