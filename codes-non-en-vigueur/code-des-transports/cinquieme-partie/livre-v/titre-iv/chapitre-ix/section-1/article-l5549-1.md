# Article L5549-1

L'armateur assure le rapatriement des personnels n'exerçant pas la profession de marin employés à bord des navires dans les conditions et selon les modalités prévues aux articles L. 5542-29, L. 5542-30 et L. 5542-33.
