# Article L5114-2

Tous les navires francisés et tous les navires en construction sur le territoire de la République française doivent être inscrits sur un fichier tenu par l'autorité administrative.
