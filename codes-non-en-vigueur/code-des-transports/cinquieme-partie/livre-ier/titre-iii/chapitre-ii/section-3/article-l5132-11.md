# Article L5132-11

La responsabilité de l'assistant, à raison des dommages corporels ou matériels en relation directe avec des opérations d'assistance ou de sauvetage, au sens de la convention sur la limitation de la responsabilité en matière de créances maritimes faite à Londres le 19 novembre 1976 modifiée, ainsi qu'à raison de tous autres préjudices résultant de ces opérations, peut être soumise à limitation, quel que soit le fondement de la responsabilité.

Cette limitation est soumise aux conditions applicables à la limitation de responsabilité du propriétaire de navire prévue par le chapitre Ier du titre II du présent livre.

Les préposés de l'assistant ont le droit de se prévaloir de la limitation de responsabilité dans les mêmes conditions que l'assistant lui-même.

Les limites de responsabilité de l'assistant agissant à partir d'un navire autre que celui auquel il fournit ses services d'assistance sont calculées suivant les règles fixées par l'article L. 5121-5.

Les limites de responsabilité de l'assistant n'agissant pas à partir d'un navire ou agissant uniquement à bord du navire auquel il fournit ses services d'assistance sont calculées selon les mêmes règles et sur la base d'une jauge de 1 500 au sens des dispositions du 5 de l'article 6 de la convention mentionnée au premier alinéa du présent article.
