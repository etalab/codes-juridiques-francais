# Article L5132-6

Il n'est dû aucune rémunération d'assistance pour les envois postaux de toute nature.
