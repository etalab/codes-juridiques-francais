# Article L5132-5

Les services du remorqueur ne sont rémunérés pour l'assistance du navire qu'il a remorqué ou de sa cargaison que lorsqu'il s'agit de services exceptionnels ne pouvant être considérés comme l'accomplissement du contrat de remorquage.
