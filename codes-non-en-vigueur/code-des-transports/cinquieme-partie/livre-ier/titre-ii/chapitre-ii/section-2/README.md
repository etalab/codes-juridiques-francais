# Section 2 : Responsabilité civile des propriétaires de navires pour les dommages résultant de la pollution par les hydrocarbures

- [Article L5122-25](article-l5122-25.md)
