# Article L5141-3

Si l'état d'abandon persiste après la mise en œuvre des mesures prévues à l'article L. 5242-16, la déchéance des droits du propriétaire sur le navire ou l'engin flottant abandonné peut être prononcée par décision de l'autorité administrative compétente.

Cette décision ne peut intervenir qu'après mise en demeure au propriétaire de faire cesser, dans un délai qui ne peut être inférieur à un mois, l'état d'abandon dans lequel se trouve son navire ou son engin flottant.
