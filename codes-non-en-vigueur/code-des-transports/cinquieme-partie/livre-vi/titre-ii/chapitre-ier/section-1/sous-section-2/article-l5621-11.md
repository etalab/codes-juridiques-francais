# Article L5621-11

Le contrat d'engagement conclu entre l'armateur et le navigant comporte les mentions prévues par l'article L. 5612-10.
