# Article L5622-3

Les navigants participent à l'élection des délégués de bord mentionnés à l'article L. 5543-2.
