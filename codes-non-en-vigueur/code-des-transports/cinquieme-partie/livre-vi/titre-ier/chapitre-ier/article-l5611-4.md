# Article L5611-4

Les navires immatriculés au registre international français sont soumis aux dispositions des livres Ier, II et IV de la présente partie.

Le port d'immatriculation ainsi que les modalités conjointes de francisation et d'immatriculation de ces navires sont fixées par voie réglementaire.
