# Article L5612-3

A bord des navires immatriculés au registre international français, les membres de l'équipage sont, dans une proportion d'au moins 35 % calculée sur la fiche d'effectif, des ressortissants d'un Etat membre de l'Union européenne, d'un Etat partie à l'accord sur l'Espace économique européen ou de la Confédération suisse.

La fiche d'effectif désigne le document par lequel l'autorité maritime française atteste que l'effectif du navire satisfait aux exigences des conventions internationales suivantes et des mesures prises pour leur application :

1° La convention internationale pour la sauvegarde de la vie humaine en mer, faite à Londres le 1er novembre 1974 modifiée ;

2° La convention internationale sur les normes de formation des gens de mer, de délivrance des brevets et de veille, faite à Londres le 7 juillet 1978 modifiée ;

3° La convention n° 180 sur la durée du travail des gens de mer et les effectifs des navires adoptée le 22 octobre 1996 par l'Organisation internationale du travail.

Le pourcentage visé au premier alinéa est fixé à 25 % pour les navires ne bénéficiant pas ou plus du dispositif d'aide fiscale attribué au titre de leur acquisition.

Le capitaine et l'officier chargé de sa suppléance, garants de la sécurité du navire et de son équipage, de la protection de l'environnement et de la sûreté, sont français, ressortissants d'un Etat membre de l'Union européenne, d'un Etat partie à l'accord sur l'Espace économique européen ou de la Confédération suisse.

L'accès aux fonctions mentionnées à l'alinéa précédent est subordonné à la possession de qualifications professionnelles et à la vérification d'un niveau de connaissance de la langue française et des matières juridiques permettant la tenue des documents de bord et l'exercice des prérogatives de puissance publique dont le capitaine est investi. Un décret en Conseil d'Etat, pris après avis des organisations représentatives d'armateurs et de gens de mer intéressées, précise les conditions d'application de cette dernière disposition.
