# Article L5351-4

Réseau ferré de France est tenu d'assurer le raccordement des voies ferrées portuaires au réseau ferré national dans des conditions techniques et financières fixées par décret en Conseil d'Etat.

Pour chaque port, une convention entre l'autorité portuaire et Réseau ferré de France, soumise à l'approbation ministérielle, fixe les conditions techniques et financières particulières de ce raccordement.
