# Article L5764-1

Les dispositions du livre IV, à l'exception de celles du chapitre V du titre II et du chapitre Ier du titre III sont applicables en Nouvelle-Calédonie.
