# Article L5774-1

Les dispositions du chapitre IV du titre III du livre IV sont applicables en Polynésie française.
