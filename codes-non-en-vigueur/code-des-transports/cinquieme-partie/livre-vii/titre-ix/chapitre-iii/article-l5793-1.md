# Article L5793-1

Les dispositions des articles L. 5341-11 à L. 5342-6 sont applicables aux Terres australes et antarctiques françaises.
