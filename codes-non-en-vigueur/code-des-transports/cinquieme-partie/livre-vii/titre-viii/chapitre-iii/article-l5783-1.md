# Article L5783-1

Les dispositions des articles L. 5341-11 à L. 5342-6 sont applicables à Wallis-et-Futuna.
