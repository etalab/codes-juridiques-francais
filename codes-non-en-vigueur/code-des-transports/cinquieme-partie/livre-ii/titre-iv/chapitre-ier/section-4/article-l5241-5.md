# Article L5241-5

Au cas où le navire ne pourrait prendre la mer sans risque pour la sécurité ou la santé de l'équipage ou des personnes embarquées, le milieu marin et ses intérêts connexes ou les autres navires, son départ peut être interdit ou ajourné, après visite.
