# Article L5241-11

I. ― Est puni de 7 500 € d'amende, le fait, pour le constructeur, l'armateur, le propriétaire ou le capitaine, d'enfreindre les stipulations des conventions internationales suivantes :

1° Convention n° 92 sur le logement des équipages, adoptée le 18 juin 1949 par l'Organisation internationale du travail, en ce qui concerne l'habitabilité et l'hygiène ;

2° Convention internationale sur les lignes de charge, faite à Londres le 5 avril 1966, en ce qui concerne les conditions de délivrance des titres de sécurité et l'organisation des contrôles des navires ;

3° Convention internationale pour la prévention de la pollution par les navires, faite à Londres le 2 novembre 1973, telle qu'elle a été modifiée par le protocole du 17 février 1978, en ce qui concerne la délivrance des certificats de prévention de la pollution, l'organisation des contrôles des navires et les dispositions relatives à la prévention de la pollution, à l'exclusion des rejets ;

4° Convention internationale pour la sauvegarde de la vie humaine en mer, faite à Londres le 1er novembre 1974, en ce qui concerne la construction des navires, la protection contre l'incendie, les installations électriques, la sécurité de la navigation, le transport des cargaisons et des marchandises dangereuses, les radiocommunications, le sauvetage, la délivrance des titres de sécurité et l'organisation des contrôles des navires ;

5° Protocole relatif à la convention internationale de 1974 pour la sauvegarde de la vie humaine en mer, fait à Londres le 17 février 1978, en ce qui concerne la délivrance des titres de sécurité et l'organisation des contrôles des navires, la construction des navires, la protection contre l'incendie, les installations électriques, la sécurité de la navigation.

II. ― La même peine est applicable aux responsables des opérations de chargement, de déchargement, d'emballage et de manutention qui ne respectent pas les stipulations des conventions internationales mentionnées au I, en ce qui concerne le transport des cargaisons, des marchandises dangereuses et des substances nuisibles.
