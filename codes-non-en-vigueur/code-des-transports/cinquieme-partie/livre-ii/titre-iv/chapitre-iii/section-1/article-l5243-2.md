# Article L5243-2

Les contrôleurs des affaires maritimes et les syndics des gens de mer sont habilités à constater les infractions aux marques de franc-bord, et, sur les navires dont la longueur n'excède pas un maximum fixé par voie réglementaire, celles des infractions réprimées par les dispositions du présent titre qui relèvent de leur domaine particulier de compétence.
