# Article L5242-1

I. ― Est puni de deux ans d'emprisonnement et de 7 500 € d'amende le fait, pour le capitaine d'un navire battant pavillon français ou étranger, d'enfreindre, y compris par imprudence ou négligence, dans les eaux territoriales ou les eaux intérieures maritimes françaises :

1° Les règles de circulation maritime édictées en application de la convention sur le règlement international de 1972 pour prévenir les abordages en mer, faite à Londres le 20 octobre 1972, et relatives aux dispositifs de séparation de trafic ;

2° Les règles relatives aux distances minimales de passage le long des côtes françaises édictées par les préfets maritimes.

L'amende est portée à 150 000 € lorsque l'infraction est commise par le capitaine d'un navire transportant une cargaison d'hydrocarbures ou d'autres substances dangereuses définies par voie réglementaire.

II. ― Est puni des mêmes peines le capitaine d'un navire battant pavillon français qui, en dehors des eaux territoriales, ne se conforme pas aux règles mentionnées au 1° du I.
