# Article L5242-3

I. ― Est puni de trois mois d'emprisonnement et de 3 750 € d'amende le fait, pour le capitaine ou le chef de quart d'un navire battant pavillon français ou étranger, d'enfreindre, y compris par imprudence ou négligence, les règles fixées par la convention sur le règlement international de 1972 pour prévenir les abordages en mer, faite à Londres le 20 octobre 1972, et relatives :

1° Aux feux à allumer la nuit et aux signaux à faire en temps de brume ;

2° A la route à suivre et aux manœuvres à exécuter en cas de rencontre d'un navire ou autre bâtiment.

La peine est portée au double si l'infraction est commise par une personne exerçant le commandement dans des conditions irrégulières au sens de l'article L. 5523-2.

Est puni de trois mois d'emprisonnement et de 3 750 € d'amende le pilote qui se rend coupable d'une infraction aux règles sur la route à suivre.

II. ― Lorsque le navire est étranger, les dispositions du I sont applicables aux infractions commises dans les eaux intérieures maritimes ou les eaux territoriales.
