# Article L5242-16

En vue de mettre fin aux dangers que présente un navire ou autre engin flottant abandonné au sens des dispositions du chapitre Ier du titre IV du livre Ier de la présente partie, il peut être procédé à la réquisition des personnes et des biens, avec attribution de compétence à l'autorité judiciaire en ce qui concerne le contentieux du droit à l'indemnité.

Lorsque le propriétaire, l'armateur, l'exploitant ou leurs représentants, dûment mis en demeure de mettre fin, dans le délai qui leur est imparti, aux dangers que présente le navire ou l'engin flottant abandonné, refuse ou néglige de prendre les mesures nécessaires, l'autorité administrative compétente peut intervenir aux frais et risques du propriétaire, de l'armateur ou de l'exploitant.

En cas d'urgence, l'intervention peut être exécutée d'office sans délai.
