# Article L412-5

Dès le commandement d'avoir à libérer les locaux, l'huissier de justice chargé de l'exécution de la mesure d'expulsion en informe le représentant de l'Etat dans le département en vue de la prise en compte de la demande de relogement de l'occupant dans le cadre du plan départemental d'action pour le logement des personnes défavorisées prévu par la loi n° 90-449 du 31 mai 1990 visant à la mise en œuvre du droit au logement. A défaut, le délai avant l'expiration duquel l'expulsion ne peut avoir lieu est suspendu.
