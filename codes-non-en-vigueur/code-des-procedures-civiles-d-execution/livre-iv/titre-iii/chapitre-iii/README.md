# Chapitre III : Le sort des meubles

- [Article L433-1](article-l433-1.md)
- [Article L433-2](article-l433-2.md)
- [Article L433-3](article-l433-3.md)
