# Sous-section 1 : Sanctions applicables aux secteurs de l'électricité et du gaz

- [Article L142-37](article-l142-37.md)
- [Article L142-38](article-l142-38.md)
- [Article L142-39](article-l142-39.md)
- [Article L142-40](article-l142-40.md)
