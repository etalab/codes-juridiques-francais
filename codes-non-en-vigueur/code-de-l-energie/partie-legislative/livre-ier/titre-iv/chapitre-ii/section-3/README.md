# Section 3 : Sanctions pénales

- [Sous-section 1 : Sanctions applicables aux secteurs de l'électricité et du gaz](sous-section-1)
- [Sous-section 2 : Sanctions applicables aux secteurs du gaz et des hydrocarbures](sous-section-2)
