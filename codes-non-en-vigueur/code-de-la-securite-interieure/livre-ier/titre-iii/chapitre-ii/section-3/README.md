# Section 3 : Dispositions particulières à Paris

- [Article L132-11](article-l132-11.md)
- [Article L132-12](article-l132-12.md)
