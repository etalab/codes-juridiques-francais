# Article L132-5

Le conseil local de sécurité et de prévention de la délinquance peut constituer en son sein un ou plusieurs groupes de travail et d'échange d'informations à vocation territoriale ou thématique.

Les faits et informations à caractère confidentiel échangés dans le cadre de ces groupes de travail ne peuvent être communiqués à des tiers.

L'échange d'informations est réalisé selon les modalités prévues par un règlement intérieur établi par le conseil local de sécurité et de prévention de la délinquance sur la proposition des membres du groupe de travail.
