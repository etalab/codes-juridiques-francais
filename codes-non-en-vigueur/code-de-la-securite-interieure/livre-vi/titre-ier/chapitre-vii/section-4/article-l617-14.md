# Article L617-14

Est puni d'un an d'emprisonnement et de 15 000 euros d'amende le fait de mettre obstacle à l'accomplissement des contrôles exercés, dans les conditions prévues à l'article L. 616-1, par les agents mentionnés au premier alinéa de cet article.
