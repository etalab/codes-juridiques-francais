# Section 2 : Agrément des exploitants individuels  et des dirigeants et gérants de personnes morales

- [Article L622-6](article-l622-6.md)
- [Article L622-7](article-l622-7.md)
- [Article L622-8](article-l622-8.md)
