# Chapitre II : Dispositions particulières à Mayotte

- [Article L542-1](article-l542-1.md)
- [Article L542-2](article-l542-2.md)
