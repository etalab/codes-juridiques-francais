# Article L346-2

Pour l'application des dispositions énumérées à l'article L. 346-1 :

1° La référence au département est remplacée par la référence aux îles Wallis et Futuna ;

2° La référence au représentant de l'Etat dans le département est remplacée par la référence à l'administrateur supérieur des îles Wallis et Futuna ;

3° Les montants exprimés en euros sont applicables dans les îles Wallis et Futuna sous réserve de leur contre-valeur en monnaie locale ;

4° Le 2° de l'article L. 324-4est supprimé.
