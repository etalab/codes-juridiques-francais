# Article L347-1

Les dispositions du titre Ier du présent livre sont applicables dans les Terres australes et antarctiques françaises.
