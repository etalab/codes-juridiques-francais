# Article L312-5

Seules les personnes satisfaisant aux prescriptions de l'article L. 2332-1 du code de la défense peuvent se porter acquéreurs dans les ventes publiques des matériels de guerre, armes et munitions et de leurs éléments des 1re, 2e, 3e et 4e catégories ainsi que des armes de 6e catégorie énumérées par décret en Conseil d'Etat.

La vente de ces mêmes matériels par les brocanteurs est interdite.
