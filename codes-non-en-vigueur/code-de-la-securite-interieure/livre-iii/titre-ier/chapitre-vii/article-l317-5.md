# Article L317-5

Est puni de trois ans d'emprisonnement et de 45 000 euros d'amende le fait d'acquérir ou de détenir des armes et des munitions en violation d'une interdiction prévue à l'article L. 312-10 ou à l'article L. 312-13.
