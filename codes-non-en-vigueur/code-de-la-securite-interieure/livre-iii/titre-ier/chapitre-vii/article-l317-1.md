# Article L317-1

Toute infraction aux prescriptions du présent titre peut être constatée par les agents des contributions indirectes et des douanes et par les autorités de police judiciaire qui en dressent procès-verbal.

Ces infractions peuvent également être constatées par les agents relevant du contrôle général des armées qui possèdent, à cet effet, les attributions d'officier de police judiciaire et dont les procès-verbaux sont adressés au ministre de la défense.
