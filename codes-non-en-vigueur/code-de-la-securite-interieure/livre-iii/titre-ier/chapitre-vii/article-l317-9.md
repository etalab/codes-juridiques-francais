# Article L317-9

Les personnes physiques coupables des infractions prévues à l'article L. 317-8 encourent également la peine complémentaire de l'interdiction de séjour, prononcée suivant les modalités prévues par l'article 131-31 du code pénal.
