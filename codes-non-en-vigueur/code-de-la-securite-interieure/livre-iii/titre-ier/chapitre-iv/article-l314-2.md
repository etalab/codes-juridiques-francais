# Article L314-2

Les armes et les munitions de la 1re ou de la 4e catégorie ne peuvent être transférées d'un particulier à un autre que dans les cas où celui à qui l'arme est transférée est autorisé à la détenir dans les conditions indiquées aux articles L. 312-1 à L. 312-4.

Dans tous les cas, les transferts d'armes ou de munitions de la 1re catégorie ou de la 4e catégorie sont constatés suivant des formes définies par voie réglementaire.
