# Sous-section 2 : Règlement des transferts par la commission (R)

- [Article R1425-23](article-r1425-23.md)
- [Article R1425-24](article-r1425-24.md)
- [Article R1425-25](article-r1425-25.md)
