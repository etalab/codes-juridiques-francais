# Article R1424-52

Un arrêté du ministre de l'intérieur fixe les règlements de service et les règles applicables aux formations dispensées aux sapeurs-pompiers qui sont rassemblés dans des guides nationaux de référence. Un arrêté du même ministre détermine les tenues, équipements, insignes et attributs des sapeurs-pompiers.
