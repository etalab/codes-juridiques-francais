# Article R1115-8

La Commission nationale de la coopération décentralisée prévue à l'article L. 1115-6 est présidée par le Premier ministre ou, en son absence, par le ministre chargé de la coopération.

Elle se réunit au moins une fois par an.

Elle comprend, outre son président, trente membres, dont vingt-huit avec voix délibérative et deux personnalités qualifiées avec voix consultative.
