# Article R1115-14

Le secrétariat de la commission est assuré par le délégué pour l'action extérieure des collectivités locales auprès du ministre des affaires étrangères.
