# TITRE Ier : LE COMITÉ DES FINANCES LOCALES

- [CHAPITRE Ier : Composition et fonctionnement du comité des finances locales](chapitre-ier)
- [CHAPITRE II : Composition et fonctionnement de la commission consultative sur l'évaluation des charges](chapitre-ii)
- [CHAPITRE III : Composition et fonctionnement de la commission consultative d'évaluation des normes](chapitre-iii)
