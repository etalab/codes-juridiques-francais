# Section 1 : Organisation et fonctionnement du Conseil national des services publics départementaux et communaux (R)

- [Article D1231-5](article-d1231-5.md)
- [Article R1231-1](article-r1231-1.md)
- [Article R1231-2](article-r1231-2.md)
- [Article R1231-3](article-r1231-3.md)
- [Article R1231-4](article-r1231-4.md)
