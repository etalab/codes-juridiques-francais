# CHAPITRE VIII : Régime général des dérogations à l'obligation de dépôt auprès de l'Etat des fonds des collectivités territoriales et de leurs établissements publics

- [Article R1618-1](article-r1618-1.md)
