# Article R3312-1

Le conseil général choisit de voter le budget du département par nature ou par fonction.
