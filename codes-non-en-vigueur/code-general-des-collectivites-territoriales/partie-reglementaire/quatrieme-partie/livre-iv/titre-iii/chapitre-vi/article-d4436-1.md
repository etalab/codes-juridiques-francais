# Article D4436-1

Le conseil consultatif des populations amérindiennes et bushinenge institué en Guyane par l'article L. 4436-1 comprend vingt membres :

1° Seize représentants d'organismes et associations représentatifs des populations amérindiennes et bushinenge désignés par ces organismes et associations ;

2° Quatre personnalités qualifiées désignées par arrêté du ministre chargé de l'outre-mer.

Un arrêté du ministre chargé de l'outre-mer détermine les organismes et associations représentatifs des populations amérindiennes et bushinenge.
