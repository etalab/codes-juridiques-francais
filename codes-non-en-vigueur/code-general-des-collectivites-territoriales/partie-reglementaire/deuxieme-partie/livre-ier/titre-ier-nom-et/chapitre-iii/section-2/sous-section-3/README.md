# Sous-section 3 : Dispositions relatives aux communes issues d'une fusion comptant 100 000 habitants ou moins

- [Paragraphe 1 : Commission consultative (R).](paragraphe-1)
- [Paragraphe 2 : Associations municipales (R).](paragraphe-2)
- [Paragraphe 3 :Répartition de la seconde part de la dotation prévue à l'article L. 2511-39 (R)](paragraphe-3)
