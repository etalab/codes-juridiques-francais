# Article D2215-1

Le plan de prévention de la délinquance dans le département fixe les priorités de l'Etat en matière de prévention de la délinquance, dans le respect des orientations nationales définies par le comité interministériel de prévention de la délinquance.

Il constitue le cadre de référence de l'Etat pour sa participation aux contrats locaux de sécurité.

Le plan est arrêté par le préfet après consultation du procureur de la République, puis du conseil départemental de prévention de la délinquance, d'aide aux victimes et de lutte contre la drogue, les dérives sectaires et les violences faites aux femmes institué par l'article 10 du décret n° 2006-665 du 7 juin 2006.

Le préfet informe les maires et les présidents des établissements publics de coopération intercommunale compétents en matière de prévention de la délinquance des priorités du plan de prévention de la délinquance dans le département.
