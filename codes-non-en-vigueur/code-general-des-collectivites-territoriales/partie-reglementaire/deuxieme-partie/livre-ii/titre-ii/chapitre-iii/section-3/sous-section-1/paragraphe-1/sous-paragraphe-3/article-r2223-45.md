# Article R2223-45

Les agents qui déterminent directement avec la famille l'organisation et les conditions de la prestation funéraire doivent justifier d'une formation professionnelle d'une durée de quatre-vingt-seize heures.

Cette formation porte sur la législation et la réglementation funéraires (quarante heures) ; la prévoyance funéraire et le tiers payant (seize heures) ; les obligations relatives à l'information des familles (huit heures) ; la psychologie et la sociologie du deuil, les pratiques et la symbolique des différents rites funéraires dont la crémation, sur les soins de conservation (seize heures) ; des cas pratiques concernant l'ensemble des matières enseignées (seize heures).
