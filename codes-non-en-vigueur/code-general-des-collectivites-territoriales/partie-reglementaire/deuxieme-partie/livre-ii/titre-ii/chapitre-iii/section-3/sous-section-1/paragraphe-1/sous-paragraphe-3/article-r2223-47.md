# Article R2223-47

Les personnes qui assurent la direction des régies, entreprises ou associations habilitées doivent justifier d'une formation professionnelle identique à celle définie à l'article R. 2223-46.
