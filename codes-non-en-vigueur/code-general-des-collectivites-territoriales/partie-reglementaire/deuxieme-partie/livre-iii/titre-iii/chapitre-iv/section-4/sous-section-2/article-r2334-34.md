# Article R2334-34

Lorsqu'il est fait application du troisième alinéa de l'article L. 2334-37, le vote a lieu sur des listes complètes sans adjonction ou suppression de noms et sans modification de l'ordre de présentation.

Lorsqu'il est fait application de l'article R. 2334-33, les listes de candidatures sont déposées à la préfecture à une date fixée par arrêté du préfet. Celui-ci fixe également la date limite d'envoi des bulletins de vote. L'élection a lieu par correspondance ; les bulletins de vote sont adressés par lettre recommandée avec demande d'avis de réception au préfet. Ils doivent comporter un nombre de candidats supérieur de moitié au nombre de sièges à pourvoir par chaque collège.

Chaque bulletin est mis sous double enveloppe ; l'enveloppe intérieure ne doit comporter aucune mention ou signe distinctif ; l'enveloppe extérieure doit porter la mention : " Election des membres de la commission prévue à l'article L. 2334-37 du code général des collectivités territoriales ", l'indication du collège auquel appartient l'intéressé, son nom, sa qualité et sa signature.

Les bulletins de vote sont recensés par une commission présidée par le préfet ou son représentant et composée de deux maires désignés par lui.

Un représentant de chaque liste peut assister au dépouillement des bulletins.

En cas d'égalité des suffrages, le candidat le plus âgé est proclamé élu.

Le mandat des membres de la commission expire à chaque renouvellement général des conseils municipaux.

Lorsque, pour quelque cause que ce soit, le siège d'un membre du comité devient vacant, il est attribué, pour la durée du mandat restant à courir, au premier candidat non élu figurant sur la même liste.

Les résultats sont publiés à la diligence du préfet. Ils peuvent être contestés devant le tribunal administratif dans les dix jours qui suivent cette publication par tout électeur, par les candidats et par le préfet.
