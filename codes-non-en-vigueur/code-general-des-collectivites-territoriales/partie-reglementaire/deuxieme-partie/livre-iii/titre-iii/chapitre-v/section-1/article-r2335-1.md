# Article R2335-1

En métropole, la dotation particulière prévue à l'article L. 2335-1 est attribuée aux communes de moins de 1 000 habitants dont le potentiel financier par habitant, tel que défini à l'article L. 2334-4, est inférieur au potentiel financier moyen par habitant des communes de moins de 1 000 habitants majoré de 25 %.

Ce dernier est égal à la somme des potentiels financiers des communes de moins de 1 000 habitants rapportée à la population de ces mêmes communes, prise en compte dans les conditions prévues à l'article L. 2334-2.
