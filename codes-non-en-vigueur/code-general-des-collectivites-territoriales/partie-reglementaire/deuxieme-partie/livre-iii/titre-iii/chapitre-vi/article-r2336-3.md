# Article R2336-3

Pour l'application de l'article L. 2336-3, les attributions de compensation mentionnées au 4° du I de cet article prises en compte sont celles constatées au 15 février de l'année de répartition au compte prévu pour l'imputation des attributions de compensation dans les comptes de gestion des communes au titre de l'année précédant l'année de répartition.
