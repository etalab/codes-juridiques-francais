# Article R2512-15-1

Les agents mentionnés à l'article L. 2512-16 ne peuvent être choisis que parmi les personnels soumis au statut des administrations parisiennes.
