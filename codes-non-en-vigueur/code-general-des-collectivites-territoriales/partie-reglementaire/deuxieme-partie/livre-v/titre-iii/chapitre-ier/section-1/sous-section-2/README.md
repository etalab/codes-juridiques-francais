# Sous-section 2 : Dispositions applicables aux employeurs relevant de régimes autres que le régime des assurances sociales agricoles (R).

- [Article D2531-9](article-d2531-9.md)
- [Article D2531-10](article-d2531-10.md)
- [Article D2531-11](article-d2531-11.md)
- [Article D2531-12](article-d2531-12.md)
- [Article D2531-14](article-d2531-14.md)
- [Article D2531-15](article-d2531-15.md)
- [Article D2531-16](article-d2531-16.md)
- [Article D2531-17](article-d2531-17.md)
- [Article R2531-7](article-r2531-7.md)
- [Article R2531-8](article-r2531-8.md)
- [Article R2531-13](article-r2531-13.md)
