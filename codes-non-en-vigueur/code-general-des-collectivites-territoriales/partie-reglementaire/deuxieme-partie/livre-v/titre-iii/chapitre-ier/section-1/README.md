# Section 1 : Versement destiné aux transports en commun

- [Sous-section 1 : Dispositions communes (R).](sous-section-1)
- [Sous-section 2 : Dispositions applicables aux employeurs relevant de régimes autres que le régime des assurances sociales agricoles (R).](sous-section-2)
- [Sous-section 3 : Dispositions particulières aux employeurs relevant du régime d'assurances sociales agricoles (R).](sous-section-3)
- [Sous-section 4 : Dispositions particulières relatives à la transmission d'information au Syndicat des transports d'Ile-de-France](sous-section-4)
