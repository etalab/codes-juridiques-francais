# Article R2531-13

Les redevables du versement de transport doivent, sous peine de la sanction prévue à l'article R. 243-16 du code de la sécurité sociale, indiquer sur le bordereau récapitulatif des cotisations de sécurité sociale, quelles que soient les modalités de calcul de ces cotisations, l'assiette du versement, c'est-à-dire, pour les salariés employés dans la région d'Ile-de-France, la totalité des salaires payés ainsi que le montant dudit versement.
