# Article D2573-19

L'article D. 2215-1 est applicable en Polynésie française sous réserve du II.

II. ― Le troisième alinéa est ainsi rédigé :

" Le plan est arrêté par le haut-commissaire de la République en Polynésie française après consultation du procureur de la République. ”
