# Paragraphe 3 : Pouvoirs de police portant sur des objets particuliers.

- [Sous-paragraphe 1 : Police dans les campagnes.](sous-paragraphe-1)
- [Sous-paragraphe 2 : Police des funérailles et des lieux de sépulture.](sous-paragraphe-2)
- [Sous-paragraphe 3 : Autres polices.](sous-paragraphe-3)
