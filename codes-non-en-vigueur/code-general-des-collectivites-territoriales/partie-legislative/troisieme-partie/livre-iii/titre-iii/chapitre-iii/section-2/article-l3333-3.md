# Article L3333-3

La taxe mentionnée à l'article L. 3333-2 est assise sur la quantité d'électricité fournie ou consommée, exprimée en mégawattheures ou fraction de mégawattheure.

1. Pour les consommations professionnelles, le tarif de la taxe est fixé selon le barème suivant :

<table>
<tbody>
<tr>
<th>
<br/>QUALITÉ DE L'ÉLECTRICITÉ <p>fournie <br/>
</p>
</th>
<th>
<br/>TARIF EN EURO <p>par mégawattheure <br/>
</p>
</th>
</tr>
<tr>
<td align="center">
<br/>Puissance inférieure ou égale à 36 kilovoltampères <br/>
</td>
<td align="center">
<br/>0,75 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Puissance supérieure à 36 kilovoltampères et inférieure ou égale à 250 kilovoltampères <br/>
</td>
<td align="center">
<br/>0,25 <br/>
</td>
</tr>
</tbody>
</table>

Relèvent de ce barème les consommations professionnelles des personnes qui assurent d'une manière indépendante, en tout lieu, la fourniture de biens et de services quels que soient la finalité ou les résultats de leurs activités économiques, qu'il s'agisse des activités de producteurs, de commerçants ou de prestataires de services, y compris les activités extractives, agricoles et celles des professions libérales ou assimilées.

2. Le tarif de la taxe est fixé à 0,75 € par mégawattheure pour toutes les consommations autres que professionnelles.

3. Le conseil général applique aux montants mentionnés aux 1 et 2 un coefficient multiplicateur unique compris entre 2 et 4. A partir de l'année 2012, la limite supérieure du coefficient multiplicateur est actualisée en proportion de l'indice moyen des prix à la consommation hors tabac établi pour l'année précédente par rapport au même indice établi pour l'année 2009. Les montants qui en résultent sont arrondis à la deuxième décimale la plus proche.

La décision du conseil général doit être adoptée avant le 1er octobre pour être applicable l'année suivante. Le président du conseil général la transmet, s'il y a lieu, au comptable public assignataire du département au plus tard quinze jours après la date limite prévue pour son adoption.

La décision ainsi communiquée demeure applicable tant qu'elle n'est pas rapportée ou modifiée par une nouvelle décision.

Pour 2011, le coefficient multiplicateur mentionné au premier alinéa du présent 3 est, sous réserve du respect des limites qui y sont fixées, égal à la multiplication par 100 du taux en valeur décimale appliqué au 31 décembre 2010 conformément à l'article L. 3333-2 dans sa rédaction antérieure à la promulgation de la loi n° 2010-1488 du 7 décembre 2010 portant nouvelle organisation du marché de l'électricité.

Pour la taxe due au titre de 2012, la décision du conseil général doit être adoptée au plus tard le 15 octobre 2011. Le président du conseil général la transmet au comptable public assignataire du département au plus tard le deuxième jour ouvré suivant le 15 octobre 2011.

En cas de changement du tarif de la taxe au cours d'une période de facturation, les quantités d'électricité concernées sont réparties en fonction des tarifs proportionnellement au nombre de jours de chaque période.
