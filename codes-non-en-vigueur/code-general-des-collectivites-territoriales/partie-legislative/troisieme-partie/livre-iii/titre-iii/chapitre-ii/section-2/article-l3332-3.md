# Article L3332-3

Les recettes de la section d'investissement se composent notamment :

1° Du produit des emprunts ;

2° abrogé ;

3° De la dotation globale d'équipement ;

4° De la dotation départementale d'équipement des collèges ;

5° Des versements au titre du Fonds de compensation pour la taxe sur la valeur ajoutée ;

6° Des subventions de l'Etat et des contributions des communes et des tiers aux dépenses d'investissement ;

7° Des dons et legs en nature et des dons et legs en espèces affectés à l'achat d'une immobilisation financière ou physique ;

8° Du produit des cessions d'immobilisations, selon des modalités fixées par décret ;

9° Du remboursement des capitaux exigibles et des rentes rachetées ;

10° Des surtaxes locales temporaires conformément aux dispositions de la loi du 15 septembre 1942 relative à la perception de surtaxes locales temporaires sur les chemins de fer d'intérêt général, les voies ferrées d'intérêt local, les voies ferrées des quais des ports maritimes ou fluviaux et les services de transports routiers en liaison avec les chemins de fer, des surtaxes locales temporaires destinées à assurer le service des emprunts contractés ou le remboursement des allocations versées ;

11° Des amortissements ;

12° Du virement prévisionnel de la section de fonctionnement et du produit de l'affectation du résultat de fonctionnement conformément à l'article L. 3312-6.
