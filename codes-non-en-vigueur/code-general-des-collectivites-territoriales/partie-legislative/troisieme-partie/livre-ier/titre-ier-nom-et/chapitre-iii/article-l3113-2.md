# Article L3113-2

Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général.

La qualité de chef-lieu de canton est maintenue aux communes qui la possédaient à la date de promulgation de la loi n° 2010-1563 du 16 décembre 2010 de réforme des collectivités territoriales.
