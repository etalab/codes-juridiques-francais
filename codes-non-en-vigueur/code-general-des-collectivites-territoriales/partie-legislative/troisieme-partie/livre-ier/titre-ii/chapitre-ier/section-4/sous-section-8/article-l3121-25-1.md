# Article L3121-25-1

Sur sa demande, le président du conseil général reçoit du représentant de l'Etat dans le département les informations nécessaires à l'exercice de ses attributions.

Sur sa demande, le représentant de l'Etat dans le département reçoit du président du conseil général les informations nécessaires à l'exercice de ses attributions.
