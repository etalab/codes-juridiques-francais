# CHAPITRE Ier : Le conseil général

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Composition](section-2)
- [Section 3 : Démission et dissolution](section-3)
- [Section 4 : Fonctionnement](section-4)
