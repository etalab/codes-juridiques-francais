# Article L3123-16

Les indemnités maximales votées par les conseils généraux pour l'exercice effectif des fonctions de conseiller général sont déterminées en appliquant au terme de référence mentionné à l'article L. 3123-15 le barème suivant :

<table>
<tbody>
<tr>
<td>
<p align="center">POPULATION DÉPARTEMENTALE (habitants) </p>
</td>
<td>
<p align="center">TAUX MAXIMAL (en %) </p>
</td>
</tr>
<tr>
<td valign="top">
<p>Moins de 250 000 </p>
</td>
<td valign="top">
<p align="center">40 </p>
</td>
</tr>
<tr>
<td valign="top">
<p>De 250 000 à moins de 500 000 </p>
</td>
<td valign="top">
<p align="center">50 </p>
</td>
</tr>
<tr>
<td valign="top">
<p>De 500 000 à moins de 1 million </p>
</td>
<td valign="top">
<p align="center">60 </p>
</td>
</tr>
<tr>
<td valign="top">
<p>De 1 million à moins de 1, 25 million </p>
</td>
<td valign="top">
<p align="center">65 </p>
</td>
</tr>
<tr>
<td valign="top">
<p>1, 25 million et plus </p>
</td>
<td valign="top">
<p align="center">70 </p>
</td>
</tr>
</tbody>
</table>

Le conseil général peut, dans des conditions fixées par son règlement intérieur, réduire le montant des indemnités qu'il alloue à ses membres en fonction de leur participation aux séances plénières, aux réunions des commissions dont ils sont membres et aux réunions des organismes dans lesquels ils représentent le département, sans que cette réduction puisse dépasser, pour chacun d'entre eux, la moitié de l'indemnité maximale pouvant lui être allouée en application du présent article.

Les indemnités de fonction des conseillers de Paris fixées à l'article L. 2511-34 sont cumulables, dans la limite des dispositions du II de l'article L. 2123-20, avec celles fixées ci-dessus.
