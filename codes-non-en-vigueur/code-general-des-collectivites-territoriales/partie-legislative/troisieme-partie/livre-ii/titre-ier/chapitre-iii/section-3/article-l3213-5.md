# Article L3213-5

Le conseil général statue sur les transactions concernant les droits du département.
