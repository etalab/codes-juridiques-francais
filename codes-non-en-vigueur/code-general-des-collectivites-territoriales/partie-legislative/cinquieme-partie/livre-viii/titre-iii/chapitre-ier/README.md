# Chapitre Ier : Dispositions générales

- [Article L5831-1](article-l5831-1.md)
- [Article L5831-2](article-l5831-2.md)
