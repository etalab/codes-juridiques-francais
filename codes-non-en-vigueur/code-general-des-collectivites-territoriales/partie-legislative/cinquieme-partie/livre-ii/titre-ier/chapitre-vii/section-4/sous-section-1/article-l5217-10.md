# Article L5217-10

Sous réserve des dispositions du présent titre, la métropole est soumise au livre III de la deuxième partie.
