# Section 4 : Dispositions financières

- [Sous-section 1 : Budget et comptes](sous-section-1)
- [Sous-section 2 : Recettes](sous-section-2)
- [Sous-section 3 : Transferts de charges et de ressources entre la région ou le département et la métropole](sous-section-3)
