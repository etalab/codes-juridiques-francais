# Article L5217-8

Le conseil de la métropole est présidé par le président du conseil de la métropole. Il est composé de conseillers de la métropole.

Les articles L. 5215-16 à L. 5215-18, L. 5215-21, L. 5215-22, L. 5215-26 à L. 5215-29, L. 5215-40 et L. 5215-42 sont applicables aux métropoles.

Pour l'application de l'article L. 5215-40, l'extension du périmètre de la métropole est décidée par décret.
