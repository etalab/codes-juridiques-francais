# Section 6 : Dissolution

- [Article L5212-33](article-l5212-33.md)
- [Article L5212-34](article-l5212-34.md)
