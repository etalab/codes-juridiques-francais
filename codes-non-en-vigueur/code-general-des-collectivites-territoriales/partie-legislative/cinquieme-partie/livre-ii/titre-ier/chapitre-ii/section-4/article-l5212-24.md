# Article L5212-24

Lorsqu'il existe un syndicat intercommunal exerçant la compétence d'autorité organisatrice de la distribution publique d'électricité ou que cette compétence est exercée par le département, la taxe prévue à l'article L. 2333-2 est perçue par le syndicat ou par ce département en lieu et place de toutes les communes dont la population recensée par l'Institut national de la statistique et des études économiques au 1er janvier de l'année est inférieure ou égale à 2 000 habitants ou dans lesquelles la taxe est perçue par le syndicat au 31 décembre 2010. Pour les autres communes, cette taxe peut être perçue par le syndicat ou le département en lieu et place de la commune s'il en est décidé ainsi par délibérations concordantes du syndicat, ou du département s'il exerce cette compétence, et de la commune.

Lorsque la taxe est perçue au profit du syndicat intercommunal ou du département en lieu et place de la commune en application de l'alinéa précédent, l'organe délibérant du syndicat intercommunal ou le conseil général fixe le tarif applicable dans les conditions prévues au deuxième alinéa de l'article L. 2333-4.

Par dérogation à l'alinéa précédent, lorsqu'il est situé hors du territoire métropolitain, le syndicat intercommunal peut fixer le coefficient multiplicateur mentionné au deuxième alinéa de l'article L. 2333-4 dans la limite de 12, sous réserve qu'il affecte la part de la taxe résultant de l'application d'un coefficient multiplicateur excédant 8 à des opérations de maîtrise de la demande d'énergie concernant les consommateurs domestiques.

La décision de l'organe délibérant du syndicat intercommunal ou du conseil général doit être adoptée avant le 1er octobre pour être applicable l'année suivante. Le président du syndicat intercommunal ou du conseil général la transmet, s'il y a lieu, au comptable public assignataire au plus tard quinze jours après la date limite prévue pour son adoption.

La décision ainsi communiquée demeure applicable tant qu'elle n'est pas rapportée ou modifiée par une nouvelle décision.

Pour 2011, le tarif est fixé dans les conditions prévues au cinquième alinéa de l'article L. 2333-4. Il en est de même lorsque la création d'un syndicat prend effet au 1er janvier 2011 et qu'avant cette date son organe délibérant a fixé le taux de la taxe prévue à l'article L. 2333-2 dans les conditions prévues au premier alinéa du présent article dans leur rédaction applicable jusqu'à cette date.

Par dérogation au premier alinéa, les communes visées à la première phrase de ce même alinéa dont la population est inférieure ou égale à 2 000 habitants sont bénéficiaires du produit de la taxe due au titre de l'année 2013 en l'absence de délibération du syndicat intercommunal ou du département avant le 1er octobre 2012 ou lorsque cette délibération a été rapportée avant le 31 décembre 2012. Le tarif applicable est celui en vigueur en 2011 en application de l'avant-dernier alinéa de l'article L. 2333-4.

Pour la taxe due au titre de 2012, la décision de l'organe délibérant du syndicat intercommunal ou du conseil général doit être adoptée au plus tard le 15 octobre 2011. Le président du syndicat intercommunal ou du conseil général la transmet au comptable public assignataire au plus tard le deuxième jour ouvré suivant le 15 octobre 2011.

En cas de changement du tarif de la taxe au cours d'une période de facturation, les quantités d'électricité concernées sont réparties en fonction des tarifs proportionnellement au nombre de jours de chaque période.

Le syndicat intercommunal ou le département peut reverser à une commune une fraction de la taxe perçue sur le territoire de celle-ci.
