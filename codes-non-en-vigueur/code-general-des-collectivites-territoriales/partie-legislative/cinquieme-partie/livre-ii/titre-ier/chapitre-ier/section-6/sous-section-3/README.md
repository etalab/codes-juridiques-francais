# Sous-section 3 : Démocratisation et transparence.

- [Article L5211-36](article-l5211-36.md)
- [Article L5211-37](article-l5211-37.md)
- [Article L5211-39](article-l5211-39.md)
- [Article L5211-39-1](article-l5211-39-1.md)
- [Article L5211-40](article-l5211-40.md)
- [Article L5211-40-1](article-l5211-40-1.md)
