# CHAPITRE Ier : Dispositions générales

- [Article L7251-1](article-l7251-1.md)
- [Article L7251-2](article-l7251-2.md)
