# Article L7252-1

L'assemblée de Martinique peut présenter au Premier ministre des propositions de modification ou d'adaptation des dispositions législatives ou réglementaires en vigueur ou en cours d'élaboration ainsi que toutes propositions relatives aux conditions du développement économique, social et culturel de la collectivité territoriale de Martinique.

Elle peut également faire au Premier ministre toutes remarques ou suggestions concernant le fonctionnement des services publics de l'Etat dans la collectivité.

Le Premier ministre accuse réception dans les quinze jours et fixe le délai dans lequel il apportera une réponse au fond.
