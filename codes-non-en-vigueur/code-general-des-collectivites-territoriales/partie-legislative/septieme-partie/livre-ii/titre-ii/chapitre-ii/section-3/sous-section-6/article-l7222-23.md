# Article L7222-23

Après l'élection de son président et de ses vice-présidents, dans les conditions prévues à l'article L. 7223-2, l'assemblée de Martinique peut former ses commissions et procéder à la désignation de ses membres ou de ses délégués pour siéger au sein d'organismes extérieurs.
