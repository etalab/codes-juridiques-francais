# Sous-section 8 : Relations avec le représentant de l'Etat

- [Article L7222-28](article-l7222-28.md)
- [Article L7222-29](article-l7222-29.md)
- [Article L7222-30](article-l7222-30.md)
- [Article L7222-31](article-l7222-31.md)
