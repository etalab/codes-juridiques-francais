# Sous-section 3 : Séances

- [Article L7222-11](article-l7222-11.md)
- [Article L7222-12](article-l7222-12.md)
- [Article L7222-13](article-l7222-13.md)
- [Article L7222-14](article-l7222-14.md)
