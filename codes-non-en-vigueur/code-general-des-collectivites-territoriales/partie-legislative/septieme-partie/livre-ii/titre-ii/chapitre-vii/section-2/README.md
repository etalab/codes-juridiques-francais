# Section 2 : Droit à la formation

- [Article L7227-12](article-l7227-12.md)
- [Article L7227-13](article-l7227-13.md)
- [Article L7227-14](article-l7227-14.md)
- [Article L7227-15](article-l7227-15.md)
- [Article L7227-16](article-l7227-16.md)
