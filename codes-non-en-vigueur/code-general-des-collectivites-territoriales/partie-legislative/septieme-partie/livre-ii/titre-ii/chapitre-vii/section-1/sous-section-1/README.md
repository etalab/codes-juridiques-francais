# Sous-section 1 : Garanties accordées dans l'exercice 
du mandat ou de la fonction

- [Article L7227-1](article-l7227-1.md)
- [Article L7227-2](article-l7227-2.md)
- [Article L7227-3](article-l7227-3.md)
- [Article L7227-4](article-l7227-4.md)
