# CHAPITRE IV : Le conseil exécutif et le président du conseil exécutif

- [Section 1 : Election et composition](section-1)
- [Section 2 : Attributions du conseil exécutif](section-2)
- [Section 3 : Attributions du président du conseil exécutif](section-3)
