# Sous-section 3 : Séances

- [Article L7122-11](article-l7122-11.md)
- [Article L7122-12](article-l7122-12.md)
- [Article L7122-13](article-l7122-13.md)
