# Section 3 : Fonctionnement

- [Article L7124-4](article-l7124-4.md)
- [Article L7124-5](article-l7124-5.md)
- [Article L7124-6](article-l7124-6.md)
