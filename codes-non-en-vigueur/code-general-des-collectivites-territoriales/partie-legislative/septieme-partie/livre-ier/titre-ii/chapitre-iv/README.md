# CHAPITRE IV : Le conseil économique, social, environnemental, de la culture et de l'éducation de Guyane

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Organisation et composition](section-2)
- [Section 3 : Fonctionnement](section-3)
- [Section 4 : Garanties et indemnités 
accordées aux membres du conseil](section-4)
