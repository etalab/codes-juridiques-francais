# Article L7124-3

La composition du conseil et de ses sections, les conditions de nomination de leurs membres ainsi que la date de leur installation sont fixées par décret en Conseil d'Etat.

Les conseillers à l'assemblée de Guyane ne peuvent être membres du conseil.
