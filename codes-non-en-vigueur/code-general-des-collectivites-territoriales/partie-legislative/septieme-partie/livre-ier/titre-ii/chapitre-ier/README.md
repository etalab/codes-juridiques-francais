# CHAPITRE Ier : Dispositions générales

- [Article L7121-1](article-l7121-1.md)
- [Article L7121-2](article-l7121-2.md)
