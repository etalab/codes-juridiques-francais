# Sous-section 3 : Garanties accordées à l'issue du mandat

- [Article L7125-9](article-l7125-9.md)
- [Article L7125-10](article-l7125-10.md)
- [Article L7125-11](article-l7125-11.md)
