# CHAPITRE IV : Rôle du congrès des élus

- [Article L7324-1](article-l7324-1.md)
- [Article L7324-2](article-l7324-2.md)
- [Article L7324-3](article-l7324-3.md)
