# CHAPITRE II : Fixation par les collectivités territoriales de Guyane et de Martinique des règles applicables sur leur territoire dans un nombre limité de matières relevant du domaine de la loi ou du règlement

- [Article LO7312-1](article-lo7312-1.md)
- [Article LO7312-2](article-lo7312-2.md)
- [Article LO7312-3](article-lo7312-3.md)
