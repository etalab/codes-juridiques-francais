# Article LO6431-1

Le conseil territorial est l'assemblée délibérante de la collectivité.

La composition du conseil territorial et la durée du mandat des conseillers territoriaux sont régies par le titre IV du livre VI du code électoral.

Le président du conseil territorial et les conseillers territoriaux sont tenus de déposer, dans le délai requis, une déclaration de situation patrimoniale dans les conditions prévues par la législation relative à la transparence financière de la vie politique.
