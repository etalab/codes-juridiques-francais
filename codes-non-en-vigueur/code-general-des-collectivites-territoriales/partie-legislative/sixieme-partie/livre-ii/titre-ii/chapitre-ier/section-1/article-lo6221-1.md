# Article LO6221-1

Le conseil territorial est l'assemblée délibérante de la collectivité.

La composition du conseil territorial et la durée du mandat des conseillers territoriaux sont régies par les dispositions du titre II du livre VI du code électoral.

Le président du conseil territorial et les conseillers territoriaux sont tenus de déposer une déclaration de situation patrimoniale, dans le délai et les conditions prévus par la législation relative à la transparence financière de la vie politique.
