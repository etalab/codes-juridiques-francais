# Section 2 : Taxe communale sur la consommation finale d'électricité

- [Article L2333-2](article-l2333-2.md)
- [Article L2333-3](article-l2333-3.md)
- [Article L2333-4](article-l2333-4.md)
- [Article L2333-5](article-l2333-5.md)
