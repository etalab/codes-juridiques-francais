# Article L2337-3

Les communes peuvent recourir à l'emprunt sous réserve des dispositions des articles L. 1611-3.
