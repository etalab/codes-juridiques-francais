# CHAPITRE IV : Suppression de communes

- [Article L2114-1](article-l2114-1.md)
- [Article L2114-2](article-l2114-2.md)
- [Article L2114-3](article-l2114-3.md)
