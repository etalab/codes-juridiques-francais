# Article L2112-10

Les actes qui prononcent la modification des limites territoriales des communes en déterminent toutes les conditions autres que celles mentionnées aux articles L. 2112-7 et L. 2112-8.

Lorsque l'acte requis est un décret, il peut décider que certaines de ces conditions sont déterminées par un arrêté du représentant de l'Etat dans le département.

Le représentant de l'Etat dans le département peut prendre par arrêté toutes dispositions transitoires pour assurer la continuité des services publics jusqu'à l'installation des nouvelles assemblées municipales.
