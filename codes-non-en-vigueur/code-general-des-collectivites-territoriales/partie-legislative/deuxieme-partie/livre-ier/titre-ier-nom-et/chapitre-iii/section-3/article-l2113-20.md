# Article L2113-20

I. - Les communes nouvelles mentionnées à l'article L. 2113-1 bénéficient des différentes parts de la dotation forfaitaire des communes prévues aux articles L. 2334-7 à L. 2334-12.

II. - La dotation de base et la dotation proportionnelle à la superficie de ces communes sont calculées conformément à l'article L. 2334-7.

La première année de la création de la commune nouvelle, la population et la superficie prises en compte sont égales à la somme des populations et superficies des anciennes communes. La garantie prévue au 4° du I de l'article L. 2334-7 est calculée la première année par addition des montants correspondants versés aux anciennes communes l'année précédant la création, indexés, s'il est positif, selon le taux d'évolution de la garantie fixé par le comité des finances locales, et évolue ensuite tel que prévu au quatrième alinéa de ce même 4°.

III. - La commune nouvelle perçoit une part " compensation " telle que définie au 3° du I de l'article L. 2334-7, égale à l'addition des montants dus à ce titre aux anciennes communes, indexés selon le taux d'évolution fixé par le comité des finances locales et minorés, le cas échéant, du prélèvement prévu au 1.2.4.2 de l'article 77 de la loi n° 2009-1673 du 30 décembre 2009 de finances pour 2010.

La commune nouvelle regroupant toutes les communes membres d'un ou plusieurs établissements publics de coopération intercommunale à fiscalité propre perçoit en outre une part " compensation " telle que définie à l'article L. 5211-28-1, égale à l'addition des montants perçus à ce titre par le ou les établissements publics de coopération intercommunale dont elle est issue, indexés selon le taux d'évolution fixé par le comité des finances locales et minorés, le cas échéant, du prélèvement prévu au 1.2.4.2 de l'article 77 de la loi n° 2009-1673 du 30 décembre 2009 précitée.

IV. - Lorsque la commune nouvelle regroupe toutes les communes membres d'un ou plusieurs établissements publics de coopération intercommunale, sa dotation forfaitaire comprend en outre les attributions d'une dotation de consolidation égale au montant de la dotation d'intercommunalité qui aurait été perçue, au titre de la même année, en application des articles L. 5211-29 à L. 5211-33 par le ou les établissements publics de coopération intercommunale auxquels elle se substitue en l'absence de création de commune nouvelle.

Cette dotation évolue selon le taux d'indexation fixé par le comité des finances locales pour la dotation de base.
