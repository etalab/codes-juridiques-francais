# Section 3 : Indemnités des titulaires de mandats municipaux

- [Sous-section 1 : Dispositions générales.](sous-section-1)
- [Sous-section 2 : Remboursement de frais.](sous-section-2)
- [Sous-section 3 : Indemnités de fonction.](sous-section-3)
