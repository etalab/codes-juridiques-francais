# Article L2531-13

I.-Les ressources du fonds de solidarité des communes de la région d'Ile-de-France en 2012,2013,2014 et 2015 sont fixées, respectivement, à 210,230,250 et 270 millions d'euros.

II.-Le fonds de solidarité des communes de la région d'Ile-de-France est alimenté par des prélèvements sur les ressources des communes de la région d'Ile-de-France selon les modalités suivantes :

1° Sont contributrices au fonds les communes de la région d'Ile-de-France dont le potentiel financier par habitant est supérieur au potentiel financier moyen par habitant des communes de la région d'Ile-de-France. Ce dernier est égal à la somme des potentiels financiers des communes de la région d'Ile-de-France rapportée à la population de l'ensemble de ces communes ;

2° Le prélèvement, calculé afin d'atteindre chaque année le montant fixé au I du présent article, est réparti entre les communes contributrices en proportion du carré de leur écart relatif entre le potentiel financier par habitant de la commune et le potentiel financier moyen par habitant des communes de la région d'Ile-de-France, multiplié par la population de la commune telle que définie à l'article L. 2334-2. Ce prélèvement respecte les conditions suivantes :

a) Le prélèvement au titre du fonds de solidarité des communes de la région d'Ile-de-France ne peut excéder 10 % des dépenses réelles de fonctionnement de la commune constatées dans le compte administratif afférent au pénultième exercice ;

b) Il ne peut excéder 120 % en 2012,130 % en 2013,140 % en 2014 et, à compter de 2015,150 % du montant du prélèvement opéré au titre de l'année 2009 conformément à l'article L. 2531-13 dans sa rédaction en vigueur au 31 décembre 2009 ;

c) Le prélèvement sur les communes qui contribuent au fonds pour la première fois fait l'objet d'un abattement de 50 % ;

d) En 2012, lorsqu'une commune fait l'objet d'un prélèvement en application du présent article et bénéficie d'une attribution en application de l'article L. 2531-14, le montant du prélèvement ne peut excéder celui de l'attribution.  Le prélèvement des communes ayant bénéficié de ces dispositions fait l'objet d'un abattement de 50 % en 2013 et de 25 % en 2014 ;

e) Le prélèvement dû par les communes de la région d'Ile-de-France classées parmi les cent cinquante premières communes classées l'année précédente en application du 1° de l'article L. 2334-18-4 est annulé.

III.-Le prélèvement est effectué sur les douzièmes prévus à l'article L. 2332-2 et au II de l'article 46 de la loi n° 2005-1719 du 30 décembre 2005 de finances pour 2006 de la commune concernée.
