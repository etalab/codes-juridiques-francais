# Article L2531-10

Le Syndicat des transports parisiens est habilité à effectuer tout contrôle nécessaire à l'application du I de l'article L. 2531-6 et de l'article L. 2531-7.
