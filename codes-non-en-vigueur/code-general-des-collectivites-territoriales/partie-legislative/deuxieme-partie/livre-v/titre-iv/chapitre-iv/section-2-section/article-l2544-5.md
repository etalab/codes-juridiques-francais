# Article L2544-5

Avant toute décision du représentant de l'Etat dans le département sur les délibérations du conseil municipal relatives aux objets désignés à l'article L. 2544-4, ou à l'aliénation ou au nantissement de biens immobiliers ou de titres appartenant à la section, il peut être institué une commission locale pour donner son avis sur les intérêts particuliers de la section.

L'institution d'une commission locale est obligatoire quand un tiers des électeurs et propriétaires de la section la réclame.

Lorsque la commission locale conclut à l'acceptation d'un don ou legs fait en faveur de la section, l'autorisation aux fins d'acceptation peut être accordée malgré un vote contraire du conseil municipal.
