# Article L4135-7

Le président ou les vice-présidents ayant délégation de l'exécutif du conseil régional qui, pour l'exercice de leur mandat, ont cessé d'exercer leur activité professionnelle bénéficient, s'ils sont salariés, des dispositions des articles L. 3142-60 à L. 3142-64 du code du travail relatives aux droits des salariés élus membres de l'Assemblée nationale et du Sénat.
