# Article L4131-1

Les régions sont administrées par un conseil régional élu au suffrage universel direct.

Il est composé des conseillers territoriaux qui siègent dans les conseils généraux des départements faisant partie de la région.
