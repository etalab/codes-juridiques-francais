# CHAPITRE II : Le conseil régional

- [Section 1 : Composition.](section-1)
- [Section 2 : Démission et dissolution.](section-2)
- [Section 3 : Fonctionnement](section-3)
