# Article L4134-2

La composition des    conseils économiques, sociaux et environnementaux régionaux, les conditions de nomination de leurs membres ainsi que la date de leur installation dans leur nouvelle composition sont fixées par un décret en Conseil d'Etat.

Les conseils économiques, sociaux et environnementaux régionaux comprennent des représentants d'associations et fondations agissant dans le domaine de la protection de l'environnement et des personnalités qualifiées, choisies en raison de leur compétence en matière d'environnement et de développement durable. Un décret fixe leur nombre.
