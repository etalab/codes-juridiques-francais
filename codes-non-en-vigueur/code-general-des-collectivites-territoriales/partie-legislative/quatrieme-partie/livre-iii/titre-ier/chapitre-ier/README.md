# CHAPITRE Ier : Dispositions générales

- [Article L4311-1](article-l4311-1.md)
- [Article L4311-2](article-l4311-2.md)
