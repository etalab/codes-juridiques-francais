# Article L4433-15

Dans les régions de Guadeloupe, de Mayotte et de la Réunion, le schéma d'aménagement mentionné à l'article L. 4433-7 vaut schéma de mise en valeur de la mer, tel qu'il est défini par l'article 57 de la loi n° 83-8 du 7 janvier 1983 relative à la répartition de compétences entre les communes, les départements, les régions et l'Etat, notamment en ce qui concerne les orientations fondamentales de la protection, de l'aménagement et de l'exploitation du littoral.

Les dispositions correspondantes sont regroupées dans un chapitre individualisé au sein du schéma d'aménagement régional.

Ces dispositions doivent avoir recueilli l'accord du représentant de l'Etat préalablement à la mise à disposition du public de l'ensemble du projet de schéma d'aménagement.

Le conseil régional de chacune des régions de Guadeloupe, de Mayotte et de la Réunion est saisi pour avis de tout projet d'accord international portant sur l'exploration, l'exploitation, la conservation ou la gestion des ressources naturelles, biologiques et non biologiques, dans la zone économique exclusive de la République au large des côtes de la région concernée.

En raison de sa situation géographique particulière, la région de la Réunion est tenue informée chaque année de l'élaboration et de la mise en oeuvre des programmes de pêche hauturière par les armements opérant à partir des ports de la Réunion.
