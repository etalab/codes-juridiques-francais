# Section 3 : Attributions des régions d'outre-mer en matière de développement économique et d'aménagement du territoire

- [Sous-section 1 : Schéma d'aménagement régional.](sous-section-1)
- [Sous-section 2 : Agriculture et forêt.](sous-section-2)
- [Sous-section 3 : Emploi et formation professionnelle.](sous-section-3)
- [Sous-section 4 : Mise en valeur des ressources de la mer.](sous-section-4)
- [Sous-section 5 : Energie, ressources minières et développement industriel.](sous-section-5)
- [Sous-section 6 : Transports.](sous-section-6)
- [Sous-section 7 : Logement.](sous-section-7)
- [Sous-section 8 : Routes](sous-section-8)
