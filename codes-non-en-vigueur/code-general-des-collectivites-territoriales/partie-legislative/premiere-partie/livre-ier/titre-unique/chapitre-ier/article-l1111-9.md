# Article L1111-9

I. ― Afin de faciliter la clarification des interventions publiques sur le territoire de la région et de rationaliser l'organisation des services des départements et des régions, le président du conseil régional et les présidents des conseils généraux des départements de la région peuvent élaborer conjointement, dans les six mois qui suivent l'élection des conseillers territoriaux, un projet de schéma d'organisation des compétences et de mutualisation des services. Chaque métropole constituée sur le territoire de la région est consultée de plein droit à l'occasion de son élaboration, de son suivi et de sa révision.

Ce schéma fixe :

a) Les délégations de compétences de la région aux départements et des départements à la région ;

b) L'organisation des interventions financières respectives de la région et des départements en matière d'investissement et de fonctionnement des projets décidés ou subventionnés par une collectivité territoriale ou un groupement de collectivités territoriales ;

c) Les conditions d'organisation et de mutualisation des services.

Le schéma porte au moins sur les compétences relatives au développement économique, à la formation professionnelle, à la construction, à l'équipement et à l'entretien des collèges et des lycées, aux transports, aux infrastructures, voiries et réseaux, à l'aménagement des territoires ruraux et aux actions environnementales. Il peut également concerner toute compétence exclusive ou partagée de la région et des départements.

Il est approuvé par délibérations concordantes du conseil régional et de chacun des conseils généraux des départements de la région.

Il est mis en œuvre par les conventions prévues aux articles L. 1111-8 et L. 5111-1-1.

Les compétences déléguées en application des alinéas précédents sont exercées au nom et pour le compte des collectivités territoriales délégantes.

II.-Afin d'étudier et débattre de tous sujets concernant l'exercice de compétences pour lesquelles une concertation est prévue par la loi et de tous domaines nécessitant une harmonisation entre les deux niveaux de collectivités, il est créé une instance de concertation entre la région et les départements dénommée " conférence des exécutifs ". Cette instance est composée du président du conseil régional, des présidents des conseils généraux, des présidents des conseils de métropoles, des présidents des communautés urbaines, des présidents des communautés d'agglomération et d'un représentant par département des communautés de communes situées sur le territoire régional. Elle se réunit à l'initiative du président du conseil régional au moins une fois par an.
