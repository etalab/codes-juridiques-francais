# Article L1871-1

Les articles L. 1611-1 à L. 1611-5 sont applicables aux communes de la Polynésie française, à leurs établissements publics et à leurs groupements.
