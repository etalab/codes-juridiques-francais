# Section 2 : Réglementation des pâturages communaux  en montagne

- [Article L142-5](article-l142-5.md)
- [Article L142-6](article-l142-6.md)
