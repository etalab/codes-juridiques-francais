# Article L175-8

Pour l'application à Mayotte du titre II du présent livre, la référence à la région est remplacée par la référence au Département de Mayotte et la référence au « plan pluriannuel régional de développement forestier » par la référence au « plan pluriannuel de développement forestier du Département de Mayotte ».
