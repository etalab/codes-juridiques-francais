# Article L177-4

Pour l'application du présent code à Saint-Martin :

1° La référence aux « orientations régionales forestières » est remplacée par la référence aux « orientations territoriales forestières » ;

2° La référence à la « commission régionale de la forêt et des produits forestiers » est remplacée par la référence à la « commission territoriale de la forêt et des produits forestiers » ;

3° La référence au « plan pluriannuel régional de développement forestier » est remplacée par la référence au « plan pluriannuel territorial de développement forestier » ;

4° La référence au conseil régional ou au conseil général est remplacée par la référence au conseil territorial ;

5° La référence au représentant de l'Etat dans la région ou dans le département est remplacée par la référence au représentant de l'Etat à Saint-Martin ;

6° La référence au directeur régional de l'administration chargée des forêts est remplacée par la référence au directeur régional de l'administration chargée des forêts en Guadeloupe.
