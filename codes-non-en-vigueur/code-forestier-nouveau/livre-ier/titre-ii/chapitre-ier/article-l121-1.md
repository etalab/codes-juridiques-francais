# Article L121-1

La politique forestière relève de la compétence de l'Etat. Ses orientations, ses financements et ses investissements s'inscrivent dans le long terme.

Elle a pour objet d'assurer la gestion durable des bois et forêts. Elle prend en compte leurs fonctions économique, écologique et sociale. Elle contribue à l'équilibre biologique en prenant en considération les modifications et phénomènes climatiques. Elle concourt au développement de la qualification des emplois en vue de leur pérennisation. Elle vise à favoriser le regroupement technique et économique des propriétaires et l'organisation interprofessionnelle de la filière forestière pour en renforcer la compétitivité. Elle tend à satisfaire les demandes sociales relatives à la forêt.
