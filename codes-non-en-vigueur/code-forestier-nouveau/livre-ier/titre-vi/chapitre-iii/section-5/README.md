# Section 5 : Rôle de protection des forêts

- [Article L163-12](article-l163-12.md)
- [Article L163-13](article-l163-13.md)
- [Article L163-14](article-l163-14.md)
