# Article L341-7

Lorsque la réalisation d'une opération ou de travaux soumis à une autorisation administrative, à l'exception de celle prévue par le titre Ier du livre V du code de l'environnement, nécessite également l'obtention d'une autorisation de défrichement, celle-ci doit être obtenue préalablement à la délivrance de cette autorisation administrative.
