# Article L214-13

Les collectivités et autres personnes morales mentionnées au 2° du I de l'article L. 211-1 ne peuvent faire aucun défrichement de leurs bois sans autorisation de l'autorité administrative compétente de l'Etat.

Les dispositions du premier alinéa de l'article L. 341-1 leur sont applicables.
