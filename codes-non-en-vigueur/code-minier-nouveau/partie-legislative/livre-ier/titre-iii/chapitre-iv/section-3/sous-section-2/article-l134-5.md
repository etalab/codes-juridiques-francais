# Article L134-5

Le titulaire d'une autorisation de recherches peut seul obtenir, pendant la durée de cette autorisation, un permis d'exploitation qui englobe les emplacements des forages autorisés ou qui est situé en tout ou en partie à l'intérieur du périmètre de cette autorisation.

De plus, si ses travaux ont fourni la preuve qu'un gîte est exploitable et s'il en fait la demande avant son expiration, le titulaire de l'autorisation a droit à l'octroi d'un permis d'exploitation.
