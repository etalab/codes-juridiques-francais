# Article L112-1

Relèvent du régime légal des mines les gîtes renfermés dans le sein de la terre dont on peut extraire de l'énergie sous forme thermique, notamment par l'intermédiaire des eaux chaudes et des vapeurs souterraines qu'ils contiennent, dits " gîtes géothermiques ".
