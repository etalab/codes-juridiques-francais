# Article L611-23

L'étendue d'un permis d'exploitation est déterminée par l'acte accordant le permis. Elle est limitée par la surface engendrée par les verticales indéfiniment prolongées en profondeur et s'appuyant sur un périmètre défini en surface.
