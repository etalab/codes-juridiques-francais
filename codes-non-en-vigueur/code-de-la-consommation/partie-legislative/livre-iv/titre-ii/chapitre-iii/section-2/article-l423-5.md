# Article L423-5

Dans la même décision prononçant la responsabilité du professionnel, le juge fixe le délai dont disposent les consommateurs pour adhérer au groupe afin d'obtenir la réparation de leur préjudice. Ce délai ne peut être inférieur à deux mois ni supérieur à six mois après l'achèvement des mesures de publicité ordonnées par lui.

Il détermine les modalités de cette adhésion et précise si les consommateurs s'adressent directement au professionnel ou par l'intermédiaire de l'association ou de la personne mentionnée à l'article L. 423-9.

L'adhésion au groupe vaut mandat aux fins d'indemnisation au profit de l'association requérante.

L'adhésion au groupe ne vaut ni n'implique adhésion à l'association requérante.
