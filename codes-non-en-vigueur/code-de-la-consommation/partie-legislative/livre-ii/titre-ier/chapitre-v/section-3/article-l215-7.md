# Article L215-7

Les autorités qualifiées pour rechercher et constater les infractions au présent livre pourront, dans tous les lieux mentionnés au premier alinéa de l'article L. 215-3 et sur la voie publique, consigner, dans l'attente des résultats des contrôles nécessaires :

1° Les produits susceptibles d'être falsifiés, corrompus ou toxiques ;

2° Les produits susceptibles d'être impropres à la consommation, à l'exception des produits d'origine animale, des denrées alimentaires en contenant ainsi que des aliments pour animaux d'origine animale ou contenant des produits d'origine animale dont l'impropriété à la consommation ne peut être reconnue qu'en fonction de caractères organoleptiques anormaux ou de signes de pathologie lésionnelle ;

3° Les produits, objets ou appareils susceptibles d'être non conformes aux lois et règlements en vigueur et de présenter un danger pour la santé ou la sécurité des consommateurs ;

4° Les produits susceptibles d'être présentés sous une marque, une marque collective ou une marque collective de certification contrefaisantes.

Les produits, objets ou appareils consignés seront laissés à la garde de leur détenteur.

Les autorités habilitées dressent un procès-verbal mentionnant les produits, objets de la consignation. Ce procès-verbal est transmis dans les vingt-quatre heures au procureur de la République.

La mesure de consignation ne peut excéder une durée de un mois que sur autorisation du procureur de la République.

Mainlevée de la mesure de consignation peut être ordonnée à tout moment par les autorités habilitées ou par le procureur de la République.

Le non-respect de la mesure de consignation est puni des peines prévues à l'article L. 213-1.
