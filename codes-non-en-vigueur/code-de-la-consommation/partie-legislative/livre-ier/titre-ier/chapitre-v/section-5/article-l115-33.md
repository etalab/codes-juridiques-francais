# Article L115-33

Les propriétaires de marques de commerce, de fabrique ou de service peuvent s'opposer à ce que des textes publicitaires concernant nommément leur marque soient diffusés lorsque l'utilisation de cette marque vise à tromper le consommateur ou qu'elle est faite de mauvaise foi.
