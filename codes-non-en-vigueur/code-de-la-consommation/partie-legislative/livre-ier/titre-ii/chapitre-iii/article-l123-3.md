# Article L123-3

Pour l'application du III de l'article L. 121-29 en Nouvelle-Calédonie, en Polynésie française et dans les îles Wallis et Futuna :

1° Les mots : " mentionnés à l'article L. 121-60 " sont remplacés par les mots : " ou groupe de contrats, conclus à titre onéreux, par lequel un professionnel confère à un consommateur, directement ou indirectement, un droit ou un service d'utilisation de biens à temps partagé, ou concernant des produits de vacances à long terme, ou de revente ou d'échange de tels droits ou services " ;

2° Cet alinéa est complété par un alinéa ainsi rédigé :

" Cet article ne s'applique pas non plus au contrat de souscription ou de cession de parts ou actions de sociétés d'attribution d'immeubles en jouissance à temps partagé régi par les dispositions applicables localement relatives aux sociétés d'attribution d'immeubles en jouissance à temps partagé. "
