# Article L121-89

L'offre du fournisseur comporte au moins un contrat d'une durée d'un an.

Le client peut changer de fournisseur dans un délai qui ne peut excéder vingt et un jours à compter de sa demande. En cas de changement de fournisseur, le contrat est résilié de plein droit à la date de prise d'effet d'un nouveau contrat de fourniture d'énergie. Dans les autres cas, la résiliation prend effet à la date souhaitée par le consommateur et, au plus tard, trente jours à compter de la notification de la résiliation au fournisseur. Dans tous les cas, le consommateur reçoit la facture de clôture dans un délai de quatre semaines à compter de la résiliation du contrat. Le remboursement du trop-perçu éventuel est effectué dans un délai maximal de deux semaines après l'émission de la facture de clôture.

Le fournisseur ne peut facturer au consommateur que les frais correspondant aux coûts qu'il a effectivement supportés, par l'intermédiaire du gestionnaire de réseau, au titre de la résiliation et sous réserve que ces frais aient été explicitement prévus dans l'offre. Ceux-ci doivent être dûment justifiés.

Aucun autre frais ne peut être réclamé au consommateur au seul motif qu'il change de fournisseur.
