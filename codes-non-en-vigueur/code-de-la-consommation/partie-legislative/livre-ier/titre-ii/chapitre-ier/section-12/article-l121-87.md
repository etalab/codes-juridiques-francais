# Article L121-87

L'offre de fourniture d'électricité ou de gaz naturel précise, dans des termes clairs et compréhensibles, les informations suivantes :

1° L'identité du fournisseur, l'adresse de son siège social et son numéro d'inscription au registre du commerce et des sociétés ou tout document équivalent pour les sociétés situées hors de France et pour les opérateurs qui ne sont pas inscrits au registre du commerce et des sociétés ;

2° Le numéro de téléphone et, le cas échéant, l'adresse électronique du fournisseur ;

3° La description des produits et des services proposés ;

4° Les prix de ces produits et services à la date de l'offre ainsi que, le cas échéant, les conditions d'évolution de ces prix ;

5° La mention du caractère réglementé ou non des prix proposés et de la possibilité pour une personne ayant renoncé aux tarifs réglementés de vente pour un site donné de revenir ou non sur ce choix ;

6° La durée du contrat et ses conditions de renouvellement ;

7° La durée de validité de l'offre ;

8° Le délai prévisionnel de fourniture de l'énergie ;

9° Les modalités de facturation et les modes de paiement proposés, notamment par le biais d'internet ;

10° Les moyens, notamment électroniques, d'accéder aux informations relatives à l'accès et à l'utilisation des réseaux publics de distribution, en particulier la liste des prestations techniques et leurs prix, les conditions d'indemnisation et les modalités de remboursement applicables dans l'hypothèse où le niveau de qualité de la fourniture d'énergie ou la continuité de la livraison ne sont pas atteints ;

11° Les cas d'interruption volontaire de la fourniture d'énergie, sans préjudice des dispositions de l'article L. 115-3 du code de l'action sociale et des familles ;

12° Les conditions de la responsabilité contractuelle du fournisseur et du gestionnaire du réseau de distribution et les modalités de remboursement ou de compensation en cas d'erreur ou de retard de facturation ou lorsque les niveaux de qualité des services prévus dans le contrat ne sont pas atteints ;

13° L'existence du droit de rétractation prévu aux articles L. 121-21 et L. 121-21-1 du présent code ;

14° Les conditions et modalités de résiliation du contrat ;

15° Les modes de règlement amiable et contentieux des litiges ;

16° Les conditions d'accès à la tarification spéciale " produit de première nécessité " pour l'électricité et au tarif spécial de solidarité pour le gaz naturel ;

17° Les coordonnées du site internet qui fournit gratuitement aux consommateurs soit directement, soit par l'intermédiaire de liens avec des sites internet d'organismes publics ou privés, les informations contenues dans l'aide-mémoire du consommateur d'énergie établi par la Commission européenne ou, à défaut, dans un document équivalent établi par les ministres chargés de la consommation et de l'énergie.

Ces informations sont mises à la disposition du consommateur par écrit ou sur support durable préalablement à la conclusion du contrat. Le consommateur n'est engagé que par sa signature.

Par dérogation à l'alinéa précédent et au premier alinéa de l'article L. 121-21-5, si le consommateur qui emménage dans un site sollicite un fournisseur et souhaite que l'exécution de son contrat conclu à distance commence avant la fin du délai de rétractation mentionné à l'article L. 121-21, le fournisseur recueille sa demande expresse, par tous moyens, et transmet le contrat au consommateur conformément à l'article L. 121-88 accompagné du formulaire de rétractation mentionné au 2° du I de l'article L. 121-17.

Aucune somme n'est due par le consommateur en cas d'exercice de son droit de rétractation si sa demande expresse n'a pas été recueillie conformément à l'avant-dernier alinéa du présent article ou si le fournisseur n'a pas respecté l'obligation d'information prévue au 4° du I de l'article L. 121-17.
