# Article L121-84-8

Dans le respect de l'article L. 121-1, aucune somme ne peut être facturée au consommateur pour un appel depuis le territoire métropolitain, les départements d'outre-mer et les collectivités territoriales de Mayotte, Saint-Barthélemy, Saint-Martin et Saint-Pierre-et-Miquelon à un service téléphonique lorsqu'il lui a été indiqué, sous quelque forme que ce soit, que l'appel à ce service est gratuit. Le présent alinéa est applicable à toute entreprise proposant, directement ou par l'intermédiaire d'un tiers, un service accessible par un service téléphonique au public.
