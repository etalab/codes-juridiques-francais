# Article L121-83-1

Tout fournisseur de services de communications électroniques au sens du 6° de l'article L. 32 du code des postes et des communications électroniques met à la disposition des consommateurs, sous une forme claire, comparable, actualisée et facilement accessible, et tient à jour dans ses points de vente et par un moyen téléphonique ou électronique accessible en temps réel à un tarif raisonnable les informations suivantes :

-les informations visées aux articles L. 111-1, L. 121-83 et, le cas échéant, L. 121-18 du présent code ;

-les produits et services destinés aux consommateurs handicapés ;

-les conséquences juridiques de l'utilisation des services de communications électroniques pour se livrer à des activités illicites ou diffuser des contenus préjudiciables, en particulier lorsqu'ils peuvent porter atteinte au respect des droits et des libertés d'autrui, y compris les atteintes aux droits d'auteur et aux droits voisins ;

-les moyens de protection contre les risques d'atteinte à la sécurité individuelle, à la vie privée et aux données à caractère personnel lors de l'utilisation des services de communications électroniques.
