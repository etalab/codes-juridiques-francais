# Article L122-14

Les personnes morales déclarées pénalement responsables du délit prévu à l'article L. 122-12 encourent, outre l'amende suivant les modalités prévues à l'article 131-38 du code pénal, les peines prévues aux 2° à 9° de l'article 131-39 du même code. L'interdiction mentionnée au 2° du même article 131-39 porte sur l'activité dans l'exercice ou à l'occasion de l'exercice de laquelle l'infraction a été commise. Les peines prévues aux 2° à 7° dudit article ne peuvent être prononcées que pour une durée de cinq ans au plus.
