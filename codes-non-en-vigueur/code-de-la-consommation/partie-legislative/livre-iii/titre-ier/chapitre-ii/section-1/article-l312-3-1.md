# Article L312-3-1

<div align="left">Les  emprunteurs, personnes physiques n'agissant pas pour des besoins  professionnels, ne peuvent contracter de prêts libellés dans une devise  étrangère à l'Union européenne remboursables en monnaie nationale que  s'ils déclarent percevoir principalement leurs revenus ou détenir un  patrimoine dans cette devise au moment de la signature du contrat de  prêt, excepté si le risque de change n'est pas supporté par  l'emprunteur.<br/>
<br/>  Ils sont informés des risques inhérents à un  tel contrat de prêt et les possibilités éventuelles de conversion des  remboursements en monnaie nationale en cours de prêts leur sont  précisées avant l'émission de l'offre de prêt.<br/>
<br/>  Les conditions d'application du présent article sont fixées par décret en Conseil d'Etat.</div>
