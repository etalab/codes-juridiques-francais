# Article R335-2

Le jugement rendu en application de l'article L. 333-2 est susceptible d'appel.
