# Sous-section 2 : La procédure de rétablissement personnel avec liquidation judiciaire

- [Paragraphe 1 : L'ouverture de la procédure](paragraphe-1)
- [Paragraphe 2 :  La déclaration et l'arrêté des créances](paragraphe-2)
- [Paragraphe 3 : La liquidation des biens du débiteur](paragraphe-3)
- [Paragraphe 4 : La clôture de la procédure](paragraphe-4)
- [Paragraphe 5 : Le plan](paragraphe-5)
