# Chapitre IV : Prêt viager hypothécaire

- [Article R*314-1](article-r-314-1.md)
- [Article R*314-2](article-r-314-2.md)
