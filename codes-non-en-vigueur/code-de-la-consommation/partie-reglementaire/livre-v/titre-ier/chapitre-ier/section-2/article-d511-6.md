# Article D511-6

Le Conseil national de la consommation est composé :

1° D'un collège de consommateurs et usagers dont les membres sont nommés pour une durée de trois ans, sur proposition de chacune des associations de défense des consommateurs agréées au niveau national pour ester en justice, par arrêté du ministre chargé de la consommation.

2° D'un collège de professionnels représentant les activités agricoles, industrielles, artisanales, commerciales et de services publics et privés, dont les membres sont nommés pour une durée de trois ans, après avis des ministres intéressés, par arrêté du ministre chargé de la consommation.
