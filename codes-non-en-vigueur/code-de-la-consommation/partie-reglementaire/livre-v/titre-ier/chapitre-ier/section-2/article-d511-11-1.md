# Article D511-11-1

La formation plénière du Conseil national de la consommation est composée de tous les membres du collège des consommateurs et usagers et du collège des professionnels du conseil mentionnés à l'article D. 511-6, ainsi que des participants de droit prévus aux articles D. 511-7 et D. 511-8.
