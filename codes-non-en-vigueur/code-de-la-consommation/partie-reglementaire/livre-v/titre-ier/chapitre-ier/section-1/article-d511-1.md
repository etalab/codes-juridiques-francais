# Article D511-1

Le Conseil national de la consommation est un organisme consultatif placé auprès du ministre chargé de la consommation.

Il a pour objet de permettre la confrontation et la concertation entre les représentants des intérêts collectifs des consommateurs et usagers et les représentants des professionnels, des services publics et des pouvoirs publics, pour tout ce qui a trait aux problèmes de la consommation.
