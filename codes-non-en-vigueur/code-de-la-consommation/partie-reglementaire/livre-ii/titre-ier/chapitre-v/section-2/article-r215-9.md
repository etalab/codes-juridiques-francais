# Article R215-9

Aussitôt après avoir scellé les échantillons, l'agent verbalisateur, s'il est en présence du propriétaire ou détenteur de la marchandise, l'invite à déclarer la valeur des échantillons prélevés. Le propriétaire ou le détenteur pourra justifier cette valeur à l'aide de ses documents comptables.

Le procès-verbal mentionne la valeur déclarée par le propriétaire ou le détenteur et, dans le cas où l'agent verbalisateur estime que cette valeur est exagérée, l'estimation faite par cet agent.

Un récépissé est remis au propriétaire ou détenteur de la marchandise ; il y est fait mention de la nature et des quantités d'échantillons prélevés, de la valeur déclarée et, dans le cas prévu à l'alinéa ci-dessus, de l'estimation faite par l'agent.

En cas de prélèvement en cours de route, le représentant de l'entreprise de transport reçoit pour sa décharge un récépissé indiquant la nature et la quantité des marchandises prélevées ainsi que la valeur estimée par l'agent.
