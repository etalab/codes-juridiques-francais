# Article R215-2

Les fonctionnaires et agents énumérés à l'article L. 215-1 procèdent à des contrôles élémentaires, dans le but d'identifier les marchandises ou de déceler leur éventuelle non-conformité aux caractéristiques qu'elles doivent posséder. Ils dressent procès-verbal de leurs constatations ; ils peuvent y joindre des spécimens d'emballages ou d'étiquetages ainsi qu'un échantillon de la marchandise destinés à servir de pièces à conviction. La quantité du produit rendue inutilisable, dont la non-conformité à la réglementation n'a pas été établie, fait l'objet d'un remboursement sur la base de la valeur estimée par l'agent verbalisateur ou, à défaut, déclarée par le propriétaire ou le détenteur de la marchandise dans les conditions fixées à l'article R. 215-9.

Ils peuvent en outre opérer des prélèvements dans les conditions fixées par les articles ci-après.
