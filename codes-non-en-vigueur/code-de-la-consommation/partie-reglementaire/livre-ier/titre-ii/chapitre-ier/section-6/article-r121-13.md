# Article R121-13

Sont punis des peines d'amende prévues pour les contraventions de la 5e classe :

1° Les ventes ou offres de vente, les prestations de services ou offres de telles prestations faites avec primes aux consommateurs ou acheteurs, prohibées par l'article L. 121-35 ;

2° Les refus ou subordinations à conditions, de ventes ou de prestations de services, interdits par l'article L. 122-1 ;

3° La violation des règles sur la valeur des échantillons fixées à l'article R. 121-8 ;

4° La violation des règles de marquage des objets publicitaires définies à l'article R. 121-10.

En cas de récidive, les peines d'amende prévues pour la récidive des contraventions de la 5e classe sont applicables.
