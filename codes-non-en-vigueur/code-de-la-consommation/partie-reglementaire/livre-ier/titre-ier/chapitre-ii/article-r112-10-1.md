# Article R112-10-1

Les dispositions de l'article R. 112-10 ne s'appliquent pas aux préemballages dont la face la plus grande a une surface inférieure à 10 centimètres carrés, ainsi que pour les bouteilles en verre destinées à être réutilisées, qui sont marquées de manière indélébile et qui, de ce fait, ne portent ni étiquette, ni bague, ni collerette. L'étiquetage de ces produits peut ne comporter que les mentions prévues aux 1°, 4° et 5° de l'article R. 112-9, et, le cas échéant, celles prévues par l'article R. 112-16-1.
