# Article R112-4

On entend par liquide de couverture les produits énumérés ci-après, seuls ou en mélange et également lorsqu'ils se présentent à l'état congelé ou surgelé, dès lors qu'ils ne sont qu'accessoires par rapport aux éléments essentiels de la préparation et ne sont par conséquent pas décisifs pour l'achat, tels que eau, solutions aqueuses de sels, saumures, solutions aqueuses d'acides alimentaires, vinaigre, solutions aqueuses de sucres, solutions aqueuses d'autres substances ou matières édulcorantes, jus de fruits ou de légumes dans le cas de fruits ou légumes.
