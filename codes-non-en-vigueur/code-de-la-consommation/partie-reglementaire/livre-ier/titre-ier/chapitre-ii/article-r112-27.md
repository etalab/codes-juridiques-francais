# Article R112-27

Avant leur mise sur le marché, les denrées alimentaires, qu'elles soient préemballées ou non préemballées, doivent être accompagnées d'une indication permettant d'identifier le lot auquel elles appartiennent.

L'indication du lot est déterminée et apposée, sous sa responsabilité, par le producteur, le fabricant ou le conditionneur de la denrée alimentaire ou par le premier vendeur établi à l'intérieur du territoire de la Communauté européenne.
