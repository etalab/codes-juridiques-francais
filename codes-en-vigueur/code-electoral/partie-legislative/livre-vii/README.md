# Livre VII : Dispositions applicables aux consultations organisées en application des articles 72-4 et 73 de la Constitution

- [Article L559](article-l559.md)
- [Article L560](article-l560.md)
- [Article L561](article-l561.md)
- [Article L562](article-l562.md)
- [Article L563](article-l563.md)
- [Article L564](article-l564.md)
- [Article L565](article-l565.md)
- [Article L566](article-l566.md)
- [Article L567](article-l567.md)
