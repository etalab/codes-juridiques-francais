# Article L415-1

Pour l'application du deuxième alinéa de l'article L. 52-11-1, les mots : " 5 % des suffrages exprimés au premier tour de scrutin " sont remplacés par les mots : " 3 % des suffrages exprimés au premier tour de scrutin ".
