# Article L392

Pour l'application des dispositions du chapitre V bis du titre Ier du livre Ier :

1° Dans l'article L. 52-8, les sommes de 4 600 euros, 150 euros et 15 000 euros sont respectivement remplacées par les sommes de 545 000 francs CFP, de 18 180 francs CFP et de 1 818 000 francs CFP.

2° Dans l'article L. 52-10, la somme de 3 000 euros est remplacée par la somme de 363 600 francs CFP.

3° Pour la Nouvelle-Calédonie, le tableau du deuxième alinéa de l'article L. 52-11 est remplacé par le tableau suivant :

<table>
<tbody>
<tr>
<td rowspan="3" width="227">
<p align="center">Fraction de la population<br/>de la circonscription</p>
</td>
<td colspan="3" width="454">
<p align="center">Plafond par habitant des dépenses électorales (en Francs CFP)</p>
</td>
</tr>
<tr>
<td colspan="2" width="227">
<p align="center">Election des conseillers municipaux</p>
</td>
<td rowspan="2" valign="top" width="227">
<p align="center">Election des membres du congrès et des assemblées de province de la Nouvelle-Calédonie</p>
</td>
</tr>
<tr>
<td width="113">
<p align="center">Listes présentes<br/>au premier tour</p>
</td>
<td width="113">
<p align="center">Listes présentes<br/>au second tour</p>
</td>
</tr>
<tr>
<td valign="top" width="227">
<p align="left">N'excédant pas 15 000 habitants</p>
</td>
<td valign="top" width="113">
<p align="center">146</p>
</td>
<td valign="top" width="113">
<p align="center">200</p>
</td>
<td valign="top" width="227">
<p align="center">127</p>
</td>
</tr>
<tr>
<td valign="top" width="227">
<p align="left">De 15 001 à 30 000 habitants</p>
</td>
<td valign="top" width="113">
<p align="center">128</p>
</td>
<td valign="top" width="113">
<p align="center">182</p>
</td>
<td valign="top" width="227">
<p align="center">100</p>
</td>
</tr>
<tr>
<td valign="top" width="227">
<p align="left">De 30 001 à 60 000 habitants</p>
</td>
<td valign="top" width="113">
<p align="center">110</p>
</td>
<td valign="top" width="113">
<p align="center">146</p>
</td>
<td valign="top" width="227">
<p align="center">91</p>
</td>
</tr>
<tr>
<td valign="top" width="227">
<p align="left">Plus de 60 000 habitants</p>
</td>
<td valign="top" width="113">
<p align="center">100</p>
</td>
<td valign="top" width="113">
<p align="center">137</p>
</td>
<td valign="top" width="227">
<p align="center">64</p>
</td>
</tr>
</tbody>
</table>

4° Pour la Polynésie française, le tableau du deuxième alinéa de l'article L. 52-11 est remplacé par le tableau suivant :

<table>
<tbody>
<tr>
<td rowspan="3">
<p align="center">FRACTION DE LA POPULATION<br/>DE LA CIRCONSCRIPTION</p>
</td>
<td colspan="4">
<p align="center">PLAFOND PAR HABITANT DES DÉPENSES ÉLECTORALES (EN FRANCS CFP)</p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">Election des conseillers municipaux</p>
</td>
<td colspan="2">
<p align="center">Election des membres<br/>de l'assemblée de la Polynésie française</p>
</td>
</tr>
<tr>
<td>
<p align="center">Listes présentes<br/>au premier tour</p>
</td>
<td>
<p align="center">Listes présentes<br/>au second tour</p>
</td>
<td>
<p align="center">Listes présentes<br/>au premier tour</p>
</td>
<td>
<p align="center">Listes présentes<br/>au second tour</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">N'excédant pas 15 000 habitants </p>
</td>
<td align="center">
<br/>156 <br/>
</td>
<td align="center">
<br/>214 <br/>
</td>
<td align="center">
<br/>136 <br/>
</td>
<td align="center">
<br/>186 <br/>
</td>
</tr>
<tr>
<td align="center">
<p align="left">De 15 001 à 30 000 habitants</p>
</td>
<td align="center">
<br/>137 <br/>
</td>
<td align="center">
<br/>195 <br/>
</td>
<td align="center">
<br/>107 <br/>
</td>
<td align="center">
<br/>152 <br/>
</td>
</tr>
<tr>
<td align="center">
<p align="left">De 30 001 à 60 000 habitants</p>
</td>
<td align="center">
<br/>118 <br/>
</td>
<td align="center">
<br/>156 <br/>
</td>
<td align="center">
<br/>97 <br/>
</td>
<td align="center">
<br/>129 <br/>
</td>
</tr>
<tr>
<td align="center">
<p align="left">De plus de 60 000 habitants</p>
</td>
<td align="center">
<br/>107 <br/>
</td>
<td align="center">
<br/>147 <br/>
</td>
<td align="center">
<br/>68 <br/>
</td>
<td align="center">
<br/>94 <br/>
</td>
</tr>
</tbody>
</table>

5° Le plafond des dépenses pour l'élection des députés mentionné au troisième alinéa de l'article L. 52-11 est de 4 545 000 francs CFP ; il est majoré de 20 francs CFP par habitant de la circonscription.

6° Aux articles L. 52-8 et L. 52-11, la référence à l'indice du coût de la vie de l'Institut national de la statistique et des études économiques est remplacée :

a) En Nouvelle-Calédonie, par la référence à l'indice du coût de la vie (hors tabac) de l'Institut territorial de la statistique et des études économiques ;

b) En Polynésie française, par la référence à l'indice des prix à la consommation des ménages de l'Institut territorial de la statistique et des études économiques ;

c) Dans les îles Wallis et Futuna, par la référence à l'indice local des prix à la consommation.

7° Les frais de transport aérien et maritime dûment justifiés, exposés à l'intérieur de la collectivité intéressée par les candidats aux élections législatives et aux élections sénatoriales en Nouvelle-Calédonie, en Polynésie française et aux îles Wallis et Futuna et aux élections au congrès et aux assemblées de province de Nouvelle-Calédonie ou à l'assemblée de la Polynésie française ou à l'assemblée territoriale des îles Wallis et Futuna, ne sont pas inclus dans le plafond des dépenses électorales fixé par l'article L. 52-11.

8° Par dérogation aux dispositions du deuxième alinéa de l'article L. 52-12, le compte de campagne peut également être déposé auprès des services du représentant de l'Etat.
