# Chapitre Ier : Dispositions communes à l'élection du député, des conseillers territoriaux et des conseillers municipaux

- [Article L531](article-l531.md)
- [Article L532](article-l532.md)
- [Article LO530](article-lo530.md)
