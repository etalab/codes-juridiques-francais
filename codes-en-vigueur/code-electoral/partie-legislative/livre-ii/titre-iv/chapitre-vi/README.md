# Chapitre VI : Opérations préparatoires au scrutin

- [Article L309](article-l309.md)
- [Article L310](article-l310.md)
- [Article L311](article-l311.md)
