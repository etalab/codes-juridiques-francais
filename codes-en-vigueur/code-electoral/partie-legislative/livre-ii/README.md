# Livre II : Election des sénateurs des départements

- [Titre Ier  : Composition du Sénat et durée du mandat des sénateurs](titre-ier)
- [Titre II : Composition du collège électoral](titre-ii)
- [Titre III : Désignation des délégués des conseils municipaux](titre-iii)
- [Titre III bis : Désignation des délégués de l'Assemblée de Corse](titre-iii-bis)
- [Titre IV : Election des sénateurs](titre-iv)
- [Titre V : Conditions d'application](titre-v)
- [Titre VI : Dispositions pénales](titre-vi)
