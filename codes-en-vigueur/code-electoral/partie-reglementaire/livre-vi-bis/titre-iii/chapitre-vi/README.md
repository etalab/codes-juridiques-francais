# Chapitre VI : Opérations de vote

- [Article R355](article-r355.md)
- [Article R356](article-r356.md)
- [Article R357](article-r357.md)
