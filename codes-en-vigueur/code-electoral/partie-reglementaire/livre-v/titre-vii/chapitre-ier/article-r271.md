# Article R271

Sont applicables à l'élection des sénateurs en Nouvelle-Calédonie et en Polynésie française, dans leur rédaction en vigueur à la date du décret n° 2014-632 du 18 juin 2014, les dispositions suivantes du livre II du code électoral (partie réglementaire) :

-le titre III ;

-les chapitres IV à VII du titre IV, à l'exception des articles R. 164-1 et R. 169.
