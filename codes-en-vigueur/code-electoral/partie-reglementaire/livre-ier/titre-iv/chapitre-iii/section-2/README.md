# Section 2 : Déclarations de candidature

- [Article R127-2](article-r127-2.md)
- [Article R128](article-r128.md)
- [Article R128-1](article-r128-1.md)
- [Article R128-2](article-r128-2.md)
- [Article R128-3](article-r128-3.md)
