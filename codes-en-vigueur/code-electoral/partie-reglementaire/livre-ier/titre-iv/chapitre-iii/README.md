# Chapitre III : Dispositions spéciales aux communes de 1000 habitants et plus

- [Section 1 : Mode de scrutin](section-1)
- [Section 2 : Déclarations de candidature](section-2)
- [Section 3 : Opérations de vote](section-3)
