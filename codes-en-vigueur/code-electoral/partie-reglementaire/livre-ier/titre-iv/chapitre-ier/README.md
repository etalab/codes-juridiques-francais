# Chapitre Ier : Dispositions applicables à toutes les communes

- [Section 1 bis : Dispositions spéciales à l'exercice par les ressortissants d'un Etat membre de l'Union européenne autre que la France du droit de vote pour l'élection des conseillers municipaux et des membres du Conseil de Paris](section-1-bis)
- [Section 4 : Propagande](section-4)
- [Section 6 : Opérations de vote](section-6)
- [Section 7 : Contentieux](section-7)
