# Article R42

Chaque bureau de vote est composé d'un président, d'au moins deux assesseurs et d'un secrétaire choisi par eux parmi les électeurs de la commune.

Dans les délibérations du bureau, le secrétaire n'a qu'une voix consultative.

Deux membres du bureau au moins doivent être présents pendant tout le cours des opérations électorales.

Le président titulaire, un assesseur titulaire ou le secrétaire d'un bureau de vote ne peuvent exercer les fonctions de membre titulaire ou suppléant d'un autre bureau de vote.
