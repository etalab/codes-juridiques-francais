# Section 3 : Vote par procuration

- [Article R72](article-r72.md)
- [Article R72-1](article-r72-1.md)
- [Article R72-2](article-r72-2.md)
- [Article R73](article-r73.md)
- [Article R74](article-r74.md)
- [Article R75](article-r75.md)
- [Article R76](article-r76.md)
- [Article R76-1](article-r76-1.md)
- [Article R77](article-r77.md)
- [Article R78](article-r78.md)
- [Article R79](article-r79.md)
- [Article R80](article-r80.md)
