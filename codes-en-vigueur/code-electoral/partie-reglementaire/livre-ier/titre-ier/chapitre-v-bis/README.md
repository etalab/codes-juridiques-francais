# Chapitre V bis : Financement et plafonnement des dépenses électorales

- [Article R39-1](article-r39-1.md)
- [Article R39-1-A](article-r39-1-a.md)
- [Article R39-1-B](article-r39-1-b.md)
- [Article R39-2](article-r39-2.md)
- [Article R39-3](article-r39-3.md)
- [Article R39-4](article-r39-4.md)
- [Article R39-5](article-r39-5.md)
- [Article R39-6](article-r39-6.md)
- [Article R39-7](article-r39-7.md)
- [Article R39-8](article-r39-8.md)
- [Article R39-9](article-r39-9.md)
- [Article R39-10](article-r39-10.md)
