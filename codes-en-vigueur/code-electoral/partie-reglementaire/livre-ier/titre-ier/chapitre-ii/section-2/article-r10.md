# Article R10

Le tableau contenant les additions et retranchements opérés par la commission administrative est signé de tous les membres de cette commission et déposé au secrétariat de la mairie le 10 janvier. Tout requérant peut en prendre communication, le recopier et le reproduire par la voie de l'impression.

Le jour même du dépôt, le tableau est affiché par le maire aux lieux accoutumés, où il devra demeurer pendant dix jours.

Les dispositions des deux alinéas précédents sont applicables au tableau des additions opérées par la commission administrative conformément aux dispositions du deuxième alinéa de l'article L. 11-2. Toutefois, dans cette hypothèse, le dépôt du tableau a lieu cinq jours après la date de la clôture des inscriptions d'office fixée par le quatrième alinéa de l'article L. 17.
