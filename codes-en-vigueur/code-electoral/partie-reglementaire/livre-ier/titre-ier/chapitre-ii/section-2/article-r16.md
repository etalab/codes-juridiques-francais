# Article R16

Le dernier jour de février de chaque année, la commission administrative de chaque bureau de vote opère toutes les rectifications régulièrement ordonnées et arrête définitivement la liste électorale. Dans les communes où une commission administrative est chargée de dresser la liste générale des électeurs, cette commission arrête le même jour, définitivement, la liste générale de la commune.

La liste électorale et le tableau définitif des rectifications apportées à la précédente liste électorale sont déposés en mairie.

Le maire transmet dans les huit jours au préfet une copie de la liste électorale générale de la commune soit sur support papier, soit sur support informatique, soit par voie dématérialisée dans les conditions fixées par un arrêté du ministre de l'intérieur, accompagnée d'une copie du ou des tableaux définitifs des rectifications apportées à la précédente liste électorale.

A la demande du préfet, le maire lui transmet la liste électorale établie par bureau de vote.

Tout électeur peut prendre communication et copie de la liste électorale et des tableaux rectificatifs à la mairie, ou à la préfecture pour l'ensemble des communes du département à la condition de s'engager à ne pas en faire un usage purement commercial.
