# Article R20

Les maires sont tenus d'envoyer, dans un délai de huit jours, à l'Institut national de la statistique et des études économiques un avis de toute inscription ou radiation effectuée sur la liste électorale de leur commune.

Lorsque la radiation est demandée par l'Institut national de la statistique et des études économiques, le maire ne lui communique que les décisions de refus accompagnées de leurs motifs.

Mention de la date et du lieu de naissance de chaque électeur doit figurer sur les avis d'inscription ou de radiation.
