# Article R112

Immédiatement après le dépouillement du scrutin, un exemplaire des procès-verbaux des opérations électorales de chaque commune, accompagné des pièces qui y sont réglementairement annexées, est scellé et transmis par porteur au bureau centralisateur du canton qui procède au recensement général des votes. Le résultat est proclamé par son président, qui adresse tous les procès-verbaux et les pièces au sous-préfet ou, dans l'arrondissement chef-lieu du département, au préfet.
