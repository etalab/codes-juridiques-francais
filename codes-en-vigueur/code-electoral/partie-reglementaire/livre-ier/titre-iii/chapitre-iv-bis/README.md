# Chapitre IV bis : Déclarations de candidatures

- [Article R109-1](article-r109-1.md)
- [Article R109-2](article-r109-2.md)
