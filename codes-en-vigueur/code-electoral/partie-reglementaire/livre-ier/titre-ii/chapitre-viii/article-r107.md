# Article R107

Le recensement général des votes est effectué dès la fermeture du scrutin et au fur et à mesure de l'arrivée des procès-verbaux. Il est achevé au plus tard le lundi qui suit le scrutin à minuit. Il est opéré, pour chaque circonscription électorale, par une commission instituée par arrêté du préfet.

Cette commission comprend un magistrat désigné par le premier président de la cour d'appel, président, deux juges désignés par la même autorité, un conseiller départemental et un fonctionnaire de préfecture désignés par le préfet.

Un suppléant de chaque membre peut être désigné dans les mêmes conditions.

Un représentant de chacun des candidats peut assister aux opérations de la commission.

Une même commission peut effectuer le recensement des votes de plusieurs circonscriptions.
