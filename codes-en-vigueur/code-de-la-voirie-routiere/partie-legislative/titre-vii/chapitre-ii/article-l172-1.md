# Article L172-1

Les articles L. 114-7 et L. 114-8 ne sont pas applicables dans les départements de la Guyane et de la Réunion.
