# Chapitre Ier : Dispositions applicables à la ville de Paris.

- [Section 1 : Voies publiques.](section-1)
- [Section 2 : Voies privées](section-2)
- [Article L171-1](article-l171-1.md)
