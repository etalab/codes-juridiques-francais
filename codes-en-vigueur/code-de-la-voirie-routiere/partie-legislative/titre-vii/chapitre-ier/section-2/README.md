# Section 2 : Voies privées

- [Sous-section 1 : Assainissement d'office.](sous-section-1)
- [Sous-section 2 : Classement des voies privées ouvertes à la circulation publique.](sous-section-2)
- [Sous-section 3 : Dispositions financières.](sous-section-3)
