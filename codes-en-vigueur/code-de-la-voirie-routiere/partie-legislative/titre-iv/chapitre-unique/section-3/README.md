# Section 3 : Dispositions relatives à la coordination des travaux exécutés sur les voies communales situées à l'extérieur des agglomérations.

- [Article L141-10](article-l141-10.md)
