# Article R*151-3

L'enquête préalable à l'arrêté conférant le caractère de route express est effectuée dans les formes prévues pour les enquêtes publiques relevant de l'article L. 110-2 du code de l'expropriation pour cause d'utilité publique régies par le titre Ier du livre Ier du même code. Toutefois, le dossier soumis à enquête comprend, outre les documents énumérés à l'article R. 112-4 ou à l'article R. 112-5 :

1° Un plan général de la route indiquant les limites entre lesquelles le caractère de route express doit lui être conféré ;

2° L'indication des dispositions prévues pour l'aménagement des points d'accès à la route express et pour le rétablissement des communications ;

3° La liste des catégories de véhicules ou d'usagers auxquelles tout ou partie de la route express seront en permanence interdits.

Lorsqu'il y a lieu à expropriation, l'enquête publique peut être effectuée conjointement avec l'enquête préalable à la déclaration d'utilité publique des travaux. Le dossier soumis à l'enquête est constitué conformément à l'alinéa précédent.
