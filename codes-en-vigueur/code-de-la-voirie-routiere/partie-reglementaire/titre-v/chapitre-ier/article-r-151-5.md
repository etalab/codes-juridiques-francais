# Article R*151-5

I.-La décision de création ou de suppression d'un point d'accès prévue à l'article L. 151-4 sur une route existante est prise par arrêté préfectoral après enquête publique et avis des départements et des communes intéressés.

II.-L'enquête publique est effectuée dans les formes prévues pour les enquêtes publiques relevant de l'article L. 110-2 du code de l'expropriation pour cause d'utilité publique régies par le titre Ier du livre Ier du même code et, s'il y a lieu, conformément aux dispositions des articles R. 122-1 à R. 122-3. Toutefois, le dossier soumis à enquête comprend, outre les documents énumérés à l'article R. 112-4 du code de l'expropriation pour cause d'utilité publique, l'indication de l'emplacement des accès et la description des aménagements projetés ainsi que les dispositions envisagées pour assurer le rétablissement des communications.

S'il y a lieu à déclaration d'utilité publique, les deux enquêtes sont confondues.

III.-Lorsque la création ou la suppression de points d'accès sur une route express existante n'est pas compatible avec les prescriptions d'un plan local d'urbanisme rendu public ou approuvé, ou d'un document d'urbanisme en tenant lieu et qu'il n'est pas fait application de l'article L. 123-14 du code de l'urbanisme, la décision concernant les accès ne peut être prise qu'après l'approbation de la modification du plan d'occupation des sols ou du document d'urbanisme en tenant lieu.
