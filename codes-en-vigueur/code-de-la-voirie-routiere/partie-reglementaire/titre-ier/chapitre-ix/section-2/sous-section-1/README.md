# Sous-section 1 : Equipements soumis au marquage CE

- [Article R*119-2](article-r-119-2.md)
- [Article R*119-3](article-r-119-3.md)
