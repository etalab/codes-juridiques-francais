# Section 1 : Emprise du domaine public routier communal.

- [Sous-section 1 : Alignement et caractéristiques techniques.](sous-section-1)
- [Sous-section 2 : Enquête publique relative au classement, à l'ouverture, au redressement, à la fixation de la largeur et au déclassement des voies communales.](sous-section-2)
- [Sous-section 3 : Publicité foncière.](sous-section-3)
