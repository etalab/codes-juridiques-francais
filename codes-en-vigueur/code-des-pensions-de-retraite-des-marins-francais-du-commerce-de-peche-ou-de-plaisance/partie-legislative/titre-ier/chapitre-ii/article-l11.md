# Article L11

Le temps de navigation active et professionnelle accompli sur des bâtiments français pourvus d'un rôle d'équipage dans des conditions fixées par voie réglementaire, entre en compte pour sa durée effective, sous réserve des dispositions ci-après :

1° Entrent en compte, pour le double de leur durée, les services militaires et les temps de navigation active et professionnelle accomplis, en période de guerre, dans les conditions fixées par voie réglementaire.

La disposition ci-dessus s'applique à tous les marins du commerce et de la pêche pensionnés antérieurement ou non à l'accomplissement des services susvisés.

Les dispositions des deux phrases qui précèdent ne peuvent ouvrir droit à pension aux marins qui, avant l'accomplissement de leurs services de guerre, auraient abandonné la navigation sans être pensionnés.

2° Entre en compte dans la liquidation des pensions le temps pendant lequel les officiers et marins appartiennent aux cadres permanents des compagnies de navigation maritime, que les intéressés soient embarqués ou non.

3° Donne lieu à bonification, dans les conditions et limites fixées par voie réglementaire, le temps de campagne effectué sur des navires hôpitaux.
