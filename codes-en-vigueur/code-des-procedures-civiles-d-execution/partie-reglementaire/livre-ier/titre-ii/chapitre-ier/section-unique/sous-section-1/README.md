# Sous-section 1 : La compétence

- [Article R121-1](article-r121-1.md)
- [Article R121-2](article-r121-2.md)
- [Article R121-3](article-r121-3.md)
- [Article R121-4](article-r121-4.md)
