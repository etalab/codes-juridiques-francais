# Chapitre II : Les biens saisissables

- [Article R112-1](article-r112-1.md)
- [Article R112-2](article-r112-2.md)
- [Article R112-3](article-r112-3.md)
- [Article R112-4](article-r112-4.md)
- [Article R112-5](article-r112-5.md)
