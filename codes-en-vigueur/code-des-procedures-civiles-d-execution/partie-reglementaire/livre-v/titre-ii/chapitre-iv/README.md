# Chapitre IV : La saisie conservatoire des droits d'associé  et des valeurs mobilières

- [Section 1 : Les opérations de saisie](section-1)
- [Section 2 : La conversion en saisie-vente](section-2)
