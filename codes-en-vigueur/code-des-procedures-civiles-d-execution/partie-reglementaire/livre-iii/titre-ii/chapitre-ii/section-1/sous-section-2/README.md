# Sous-section 2 : L'assignation à comparaître

- [Paragraphe 1 : L'assignation du débiteur](paragraphe-1)
- [Paragraphe 2 : L'assignation des créanciers inscrits](paragraphe-2)
- [Paragraphe 3 : Disposition commune](paragraphe-3)
