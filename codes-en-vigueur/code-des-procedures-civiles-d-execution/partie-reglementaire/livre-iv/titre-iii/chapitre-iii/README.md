# Chapitre III : Le sort des meubles

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Les meubles indisponibles](section-2)
