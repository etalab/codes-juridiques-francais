# Article R251-5

A défaut de contestation dans le délai imparti, le projet de répartition devient définitif. L'agent chargé de la vente procède au paiement des créanciers ayant mis en œuvre une mesure d'exécution forcée. Il consigne auprès de la Caisse des dépôts et consignations les sommes revenant aux créanciers ayant pratiqué une saisie conservatoire ; ces sommes leur sont payées après signification d'un acte de conversion.
