# Article R223-5

Les effets de la déclaration ne peuvent préjudicier au créancier titulaire d'un gage régulièrement inscrit conformément aux dispositions du décret du 30 septembre 1953 précité.
