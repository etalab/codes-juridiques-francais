# Sous-section 1 : L'opposition des créanciers

- [Article R221-41](article-r221-41.md)
- [Article R221-42](article-r221-42.md)
- [Article R221-43](article-r221-43.md)
- [Article R221-44](article-r221-44.md)
- [Article R221-45](article-r221-45.md)
- [Article R221-46](article-r221-46.md)
- [Article R221-47](article-r221-47.md)
- [Article R221-48](article-r221-48.md)
