# Section 5 : Dispositions particulières à la saisie des récoltes sur pieds

- [Article R221-57](article-r221-57.md)
- [Article R221-58](article-r221-58.md)
- [Article R221-59](article-r221-59.md)
- [Article R221-60](article-r221-60.md)
- [Article R221-61](article-r221-61.md)
