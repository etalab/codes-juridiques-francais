# Section 2 : Dispositions particulières

- [Sous-section 1 : La saisie-attribution des créances à exécution successive](sous-section-1)
- [Sous-section 2 : La saisie-attribution des comptes ouverts auprès d'établissements  habilités par la loi à tenir des comptes de dépôt](sous-section-2)
