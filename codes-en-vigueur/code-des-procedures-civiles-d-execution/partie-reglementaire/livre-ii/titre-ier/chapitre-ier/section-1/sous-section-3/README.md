# Sous-section 3 : Le paiement par le tiers saisi

- [Article R211-6](article-r211-6.md)
- [Article R211-7](article-r211-7.md)
- [Article R211-8](article-r211-8.md)
- [Article R211-9](article-r211-9.md)
