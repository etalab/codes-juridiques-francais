# Article R213-3

Si une nouvelle décision change le montant de la pension alimentaire ou les modalités d'exécution de l'obligation, la demande de paiement direct se trouve de plein droit modifiée en conséquence à compter de la notification de la décision modificative qui est faite au tiers dans les conditions prévues aux premier et deuxième alinéas de l'article R. 213-1.
