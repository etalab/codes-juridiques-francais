# Article L213-4

La procédure de paiement direct est applicable aux termes à échoir de la pension alimentaire.

Elle l'est aussi aux termes échus pour les six derniers mois avant la notification de la demande de paiement direct.

Le règlement de ces sommes est fait par fractions égales sur une période de douze mois.
