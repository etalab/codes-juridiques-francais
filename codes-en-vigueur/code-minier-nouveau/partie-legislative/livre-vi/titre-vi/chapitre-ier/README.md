# Chapitre Ier : Dispositions générales

- [Article L661-1](article-l661-1.md)
- [Article L661-2](article-l661-2.md)
- [Article L661-3](article-l661-3.md)
