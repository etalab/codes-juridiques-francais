# Article L131-16-1

L'accès d'une fédération sportive délégataire, en vue de la mise en œuvre d'une éventuelle procédure disciplinaire contre un acteur d'une compétition sportive qui aurait parié sur celle-ci, à des informations personnelles relatives à des opérations de jeu enregistrées par un opérateur de jeux ou de paris en ligne titulaire de l'agrément prévu à l'article 21 de la loi n° 2010-476 du 12 mai 2010 précitée s'effectue par demande adressée à l'Autorité de régulation des jeux en ligne.

L'Autorité de régulation des jeux en ligne communique à des agents de la fédération délégataire spécialement habilités à cette fin dans des conditions prévues par décret les éléments strictement nécessaires, dans le respect des dispositions de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés.
