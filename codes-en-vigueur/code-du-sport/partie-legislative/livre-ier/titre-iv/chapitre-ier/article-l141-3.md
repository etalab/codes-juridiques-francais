# Article L141-3

Le Comité national olympique et sportif français veille au respect de la déontologie du sport définie dans une charte établie par lui.
