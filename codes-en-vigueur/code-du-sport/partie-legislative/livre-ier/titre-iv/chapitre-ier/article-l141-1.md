# Article L141-1

Les associations sportives et les sociétés sportives qu'elles ont constituées, les fédérations sportives et leurs licenciés sont représentés par le Comité national olympique et sportif français.

Les statuts du Comité national olympique et sportif français sont approuvés par décret en Conseil d'Etat.
