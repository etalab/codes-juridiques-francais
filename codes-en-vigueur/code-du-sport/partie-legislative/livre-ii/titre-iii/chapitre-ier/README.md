# Chapitre Ier : Suivi médical des sportifs

- [Section préliminaire](section-preliminaire)
- [Section 1 : Certificat médical](section-1)
- [Section 2 : Rôle des fédérations sportives](section-2)
