# Article L231-2-3

Pour certaines disciplines, dont la liste est fixée par arrêté des ministres chargés des sports et de la santé au regard des risques qu'elles présentent pour la sécurité ou la santé des pratiquants, le certificat médical mentionné aux articles L. 231-2 à L. 231-2-2 ne peut être délivré que dans les conditions prévues au même arrêté.

Le certificat médical mentionné au présent article doit dater de moins d'un an.
