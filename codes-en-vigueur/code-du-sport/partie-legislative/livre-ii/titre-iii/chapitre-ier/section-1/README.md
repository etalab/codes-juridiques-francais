# Section 1 : Certificat médical

- [Article L231-2](article-l231-2.md)
- [Article L231-2-1](article-l231-2-1.md)
- [Article L231-2-2](article-l231-2-2.md)
- [Article L231-2-3](article-l231-2-3.md)
- [Article L231-3](article-l231-3.md)
- [Article L231-4](article-l231-4.md)
