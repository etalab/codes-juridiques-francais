# Article L232-23-4

Lorsque les circonstances le justifient, le président de l'Agence française de lutte contre le dopage peut ordonner à l'encontre du sportif, à titre conservatoire et dans l'attente d'une décision définitive de l'agence, une suspension provisoire de sa participation aux manifestations organisées par les fédérations agréées ou autorisées par la fédération délégataire compétente. Cette décision est motivée. Le sportif est convoqué par le président de l'agence, dans les meilleurs délais, pour faire valoir ses observations sur cette suspension provisoire. La durée de suspension ne peut excéder deux mois. La suspension est renouvelable une fois dans les mêmes conditions.

La durée de la suspension provisoire est déduite de la durée de l'interdiction de participer aux manifestations sportives que l'agence peut ultérieurement prononcer.
