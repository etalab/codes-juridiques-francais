# Article L232-24-1

L'action disciplinaire se prescrit par huit années révolues à compter du jour du contrôle. Ce délai est interrompu par tout acte d'instruction ou de poursuite.

Durant ce délai, l'agence peut réaliser des analyses des échantillons prélevés, dont elle a la garde.
