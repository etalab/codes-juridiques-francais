# Article L241-4

Les dispositions de la section 3 du chapitre II du titre III du présent livre, à l'exception des articles L. 232-9 et L. 232-10, s'appliquent aux contrôles et constats des infractions en matière de dopage animal dans les conditions prévues par le décret en Conseil d'Etat mentionné à l'article L. 241-9.

Pour l'application du premier alinéa, les prélèvements sur tout animal destinés à mettre en évidence l'utilisation de substances et procédés prohibés ou à déceler la présence dans l'organisme de substances interdites sont réalisés sous la responsabilité des personnes mentionnées à l'article L. 232-11, ayant la qualité de vétérinaire ; les examens cliniques et biologiques doivent être réalisés directement par un vétérinaire.
