# Article L222-2-2

Pour l'application de l'article L. 131-7 du code de la sécurité sociale, l'Agence centrale des organismes de sécurité sociale transmet annuellement à l'autorité administrative compétente les données, rendues anonymes, relatives au montant de la rémunération de chaque sportif professionnel qui lui sont transmises par les sociétés mentionnées aux articles L. 122-2 et L. 122-12 du présent code, en précisant la discipline pratiquée par ce sportif.

Les modalités d'application du présent article sont fixées par voie réglementaire.
