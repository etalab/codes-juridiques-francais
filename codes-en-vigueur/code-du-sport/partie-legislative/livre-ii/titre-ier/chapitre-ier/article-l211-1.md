# Article L211-1

Les établissements publics de formation relevant de l'Etat assurent la formation initiale des personnes qui gèrent, animent, encadrent et enseignent les activités physiques et sportives et contribuent à leur formation continue.

Toutefois, la formation des personnels des collectivités territoriales et de leurs établissements publics s'effectue conformément aux dispositions statutaires qui leur sont applicables.
