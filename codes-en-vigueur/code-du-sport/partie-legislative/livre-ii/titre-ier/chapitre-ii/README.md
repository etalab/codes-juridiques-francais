# Chapitre II : Enseignement du sport contre rémunération

- [Section 1 : Obligation de qualification](section-1)
- [Section 2 : Obligation d'honorabilité](section-2)
- [Section 3 : Obligation de déclaration d'activité](section-3)
- [Section 4 : Police des activités d'enseignement](section-4)
