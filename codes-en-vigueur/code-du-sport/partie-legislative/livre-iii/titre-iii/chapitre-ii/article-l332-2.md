# Article L332-2

Les sociétés visées par l'article 1er de la loi n° 83-629 du 12 juillet 1983 réglementant les activités privées de sécurité assurent la surveillance de l'accès aux enceintes dans lesquelles est organisée une manifestation sportive rassemblant plus de 300 spectateurs dans les conditions prévues à l'article 3-2 de cette loi.
