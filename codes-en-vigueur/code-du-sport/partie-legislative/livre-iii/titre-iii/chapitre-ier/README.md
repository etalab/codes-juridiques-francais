# Chapitre Ier : Organisation des manifestations sportives

- [Section 1 : Rôle des fédérations](section-1)
- [Section 2 : Autorisation et déclaration préalables](section-2)
- [Section 3 : Obligation d'assurance des organisateurs de manifestations sportives](section-3)
