# Chapitre II : Garanties d'hygiène et de sécurité

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Dispositions relatives aux baignades et piscines ouvertes au public](section-2)
