# Article L312-2

Tout propriétaire d'un équipement sportif est tenu d'en faire la déclaration à l'administration en vue de l'établissement d'un recensement des équipements.

Les dispositions de l'alinéa précédent ne sont pas applicables aux équipements sportifs à usage exclusivement familial ni à ceux relevant du ministre chargé de la défense.

Un décret en Conseil d'Etat détermine les conditions d'application du présent article.
