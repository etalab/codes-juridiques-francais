# Section 2 : Installations fixes

- [Article L312-5](article-l312-5.md)
- [Article L312-6](article-l312-6.md)
- [Article L312-7](article-l312-7.md)
- [Article L312-8](article-l312-8.md)
- [Article L312-9](article-l312-9.md)
- [Article L312-10](article-l312-10.md)
- [Article L312-11](article-l312-11.md)
