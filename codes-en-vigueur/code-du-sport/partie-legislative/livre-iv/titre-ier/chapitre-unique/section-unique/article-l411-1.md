# Article L411-1

Un prélèvement effectué chaque année dans les conditions déterminées par la loi de finances sur les sommes misées sur les jeux exploités en France et dans les départements d'outre-mer par La Française des jeux est affecté à l'établissement public chargé du développement du sport.
