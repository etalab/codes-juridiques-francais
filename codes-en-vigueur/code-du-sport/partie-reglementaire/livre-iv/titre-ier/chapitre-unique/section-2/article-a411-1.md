# Article A411-1

Le Centre national pour le développement du sport (CNDS) est assujetti au contrôle budgétaire prévu par les articles 220 à 228 du décret n° 2012-1246 du 7 novembre 2012 relatif à la gestion budgétaire et comptable publique dans les conditions fixées à la présente section.
