# Chapitre Ier : Dispositions applicables à Mayotte

- [Article A421-1](article-a421-1.md)
- [Article A421-2](article-a421-2.md)
- [Article A421-3](article-a421-3.md)
- [Article A421-4](article-a421-4.md)
- [Article A421-5](article-a421-5.md)
