# Article A231-2

Les qualifications reconnues par l'ordre ainsi que les diplômes nationaux ou d'université que doivent posséder les médecins amenés à réaliser les examens dans les disciplines prévues à l'article A. 231-1 sont précisés par le règlement préparé par la commission médicale de chaque fédération sportive concernée, adopté par le comité directeur de la fédération ou, le cas échéant, par le conseil fédéral et approuvé par le ministre chargé des sports.
