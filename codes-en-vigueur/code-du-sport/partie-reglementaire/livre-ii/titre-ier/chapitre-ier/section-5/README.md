# Section 5 : Les centres de ressources, d'expertise et de performance sportives

- [Sous-section 1 : Contrôle budgétaire](sous-section-1)
- [Sous-section 2 : Conditions d'exercice du droit de suffrage, d'éligibilité et règles applicables au déroulement du scrutin pour l'élection de membres au conseil d'administration](sous-section-2)
