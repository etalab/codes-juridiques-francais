# Article A211-64

Les centres de ressources, d'expertise et de performance sportives (CREPS) sont assujettis au contrôle budgétaire prévu par les articles 220 à 228 du décret n° 2012-1246 du 7 novembre 2012 relatif à la gestion budgétaire et comptable publique dans les conditions fixées à la présente sous-section.
