# Paragraphe 7 : Validation des acquis de l'expérience

- [Article A212-36](article-a212-36.md)
- [Article A212-37](article-a212-37.md)
- [Article A212-38](article-a212-38.md)
- [Article A212-39](article-a212-39.md)
