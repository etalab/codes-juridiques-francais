# Article A212-29

Dans  le cas d'une spécialité et, le cas échéant, d'une mention, d'une unité  capitalisable complémentaire ou d'un certificat de spécialisation  comportant une activité physique ou sportive, seuls les stagiaires  engagés dans un cursus de formation organisé par la voie des unités  capitalisables, mis en œuvre par un organisme habilité, bénéficient de  l'alternance avec mise en situation pédagogique dans l'entreprise.

L'arrêté  portant création de la spécialité, de la mention, de l'unité  capitalisable complémentaire ou du certificat de spécialisation peut, le  cas échéant, fixer les exigences minimales, en termes d'objectifs  intermédiaires des unités capitalisables, notamment visant les  compétences relatives à la protection des pratiquants et des tiers, pour  placer le stagiaire dans certaines situations déterminées par ledit  arrêté.
