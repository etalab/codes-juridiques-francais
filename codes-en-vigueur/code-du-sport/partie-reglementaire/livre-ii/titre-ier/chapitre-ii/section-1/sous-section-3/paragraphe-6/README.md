# Paragraphe 6 : Certification par unités capitalisables

- [Article A212-31](article-a212-31.md)
- [Article A212-31-1](article-a212-31-1.md)
- [Article A212-32](article-a212-32.md)
- [Article A212-33](article-a212-33.md)
- [Article A212-34](article-a212-34.md)
- [Article A212-35](article-a212-35.md)
