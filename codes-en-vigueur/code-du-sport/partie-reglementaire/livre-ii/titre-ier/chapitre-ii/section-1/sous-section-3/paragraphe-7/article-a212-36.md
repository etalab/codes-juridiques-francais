# Article A212-36

Le jury, créé pour valider les évaluations certificatives réalisées dans une spécialité du diplôme par la voie des unités capitalisables, est chargé d'instruire les dossiers de demande de validation d'acquis de l'expérience.
