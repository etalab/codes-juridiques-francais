# Article A212-45

Le directeur régional de la jeunesse, des sports    et de la cohésion sociale, dans les mêmes conditions, examine la compatibilité du handicap justifiant les aménagements mentionnés ci-dessus avec l'exercice professionnel de l'activité faisant l'objet de la spécialité du diplôme. Il peut apporter une restriction aux prérogatives ouvertes par la possession de la spécialité du diplôme.
