# Article A212-6

L'équipe pédagogique associe les formateurs de l'organisme ou des organismes de formation et les membres de la ou des entreprises ou collectivités d'accueil intervenant dans la formation.

Elle définit l'organisation et la structuration modulaire de la formation, ainsi que les modalités de validation des acquis en cours de formation.
