# Paragraphe 2 : Délivrance du diplôme

- [Article A212-9](article-a212-9.md)
- [Article A212-10](article-a212-10.md)
- [Article A212-11](article-a212-11.md)
- [Article A212-12](article-a212-12.md)
- [Article A212-13](article-a212-13.md)
- [Article A212-14](article-a212-14.md)
