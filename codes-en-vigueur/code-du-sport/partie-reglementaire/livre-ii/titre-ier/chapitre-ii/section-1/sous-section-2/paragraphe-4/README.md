# Paragraphe 4 :  Validation des acquis de l'expérience

- [Article A212-16-1](article-a212-16-1.md)
- [Article A212-16-2](article-a212-16-2.md)
- [Article A212-16-3](article-a212-16-3.md)
- [Article A212-16-4](article-a212-16-4.md)
