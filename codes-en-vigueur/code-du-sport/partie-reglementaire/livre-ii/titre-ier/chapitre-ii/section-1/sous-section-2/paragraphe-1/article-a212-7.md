# Article A212-7

Nonobstant les dispositions des articles L. 6313-1 à L. 6313-11 du code du travail relatif au bilan de compétences, l'équipe pédagogique procède à l'entrée en formation à un positionnement des acquis du candidat. Au préalable, le candidat aura fourni à l'équipe pédagogique un dossier comprenant les pièces suivantes :

- une lettre de motivation ;

- son curriculum vitae reprenant en particulier les étapes de sa formation et, le cas échéant, son expérience professionnelle ;

- les certificats d'exercice établis par les employeurs ;

- une copie conforme de ses diplômes ;

- un certificat médical de non-contre-indication à la pratique des activités correspondant aux supports techniques choisis et datant de moins de trois mois à l'inscription ;

- l'unité d'enseignement " prévention et secours civiques de niveau 1 " (PSC1) ou tout titre équivalent.

Le jury, défini à l'article A. 212-14, valide, le cas échéant, les acquis du candidat au vu du positionnement établi par l'équipe pédagogique et décide, sur proposition de celle-ci, des allégements de formation correspondants.

L'équipe pédagogique définit des parcours individualisés de formation en fonction des allégements accordés dans les conditions prévues à l'alinéa précédent.
