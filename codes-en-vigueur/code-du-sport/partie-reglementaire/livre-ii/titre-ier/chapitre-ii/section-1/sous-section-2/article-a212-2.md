# Article A212-2

La délivrance du diplôme défini à l'article D. 212-11 atteste de l'obtention du niveau requis dans les domaines de compétences communes aux différentes options professionnelles et spécifiques à l'option dont il porte certification. Les domaines de compétences communes et les niveaux qui leur sont attachés sont définis en annexe II-2.

Pour les différentes options, des annexes à l'arrêté prévu à l'article A. 212-3 décrivent les compétences communes dans leur adaptation à l'option, fixent les compétences spécifiques à celle-ci et précisent, s'il y a lieu, les prérogatives et les conditions d'exercice professionnel établies selon les dispositions générales figurant en annexe II-2.
