# Section 1 : Obligation de qualification

- [Sous-section 1 : Liste des diplômes, titres à finalité professionnelle  ou certificats de qualification](sous-section-1)
- [Sous-section 2 : Brevet d'aptitude professionnelle d'assistant animateur technicien  de la jeunesse et des sports](sous-section-2)
- [Sous-section 3 : Brevet professionnel de la jeunesse,  de l'éducation populaire et du sport](sous-section-3)
- [Sous-section 4 : Diplôme d'Etat de la jeunesse,  de l'éducation populaire et du sport](sous-section-4)
- [Sous-section 5 : Diplôme d'Etat supérieur de la jeunesse,  de l'éducation populaire et du sport](sous-section-5)
- [Sous-section 6 : Brevet d'Etat d'éducateur sportif](sous-section-6)
- [Sous-section 7 : Formation générale commune aux métiers d'enseignement, d'encadrement et d'entraînement des sports de montagne](sous-section-7)
- [Sous-section 8 : Commission de reconnaissance des qualifications](sous-section-8)
- [Sous-Section 9 : Organisation des formations conduisant aux diplômes portant sur les activités physiques et sportives s'exerçant en environnement spécifique](sous-section-9)
