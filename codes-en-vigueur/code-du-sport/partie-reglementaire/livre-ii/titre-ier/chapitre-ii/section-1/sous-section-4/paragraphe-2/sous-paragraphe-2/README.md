# Sous-paragraphe 2 : L'habilitation

- [Article A212-52](article-a212-52.md)
- [Article A212-53](article-a212-53.md)
- [Article A212-54](article-a212-54.md)
- [Article A212-55](article-a212-55.md)
- [Article A212-56](article-a212-56.md)
- [Article A212-57](article-a212-57.md)
- [Article A212-58](article-a212-58.md)
