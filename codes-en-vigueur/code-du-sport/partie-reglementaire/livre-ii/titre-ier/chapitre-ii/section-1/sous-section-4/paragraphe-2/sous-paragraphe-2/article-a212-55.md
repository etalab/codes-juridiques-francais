# Article A212-55

Après avis du directeur technique national placé auprès de la fédération ayant reçu délégation pour la discipline concernée par la mention, le directeur régional de la jeunesse, des sports et de la vie associative délivre et notifie l'habilitation à l'organisme concerné, pour une durée et un effectif annuel déterminés en fonction des éléments produits dans la demande mentionnée à l'article A. 212-54.
