# Article A212-49

La spécialité « perfectionnement sportif » du diplôme d'Etat de la jeunesse, de l'éducation populaire et du sport confère à son titulaire les compétences suivantes, attestées par le référentiel de certification :

― concevoir des programmes de perfectionnement sportif ;

― coordonner la mise en œuvre d'un projet de perfectionnement dans un champ disciplinaire ;

― conduire une démarche de perfectionnement sportif ;

― conduire des actions de formation.

Le référentiel professionnel et le référentiel de certification mentionnés aux articles D. 212-37 et D. 212-38 figurent respectivement aux annexes II-3 et II-4.

Lorsque la formation est suivie dans le cadre de la formation initiale, sa durée minimale est de 1 200 heures dont 700 heures en centre de formation.

La présente sous-section a pour objet de déterminer les modalités de préparation et de délivrance de cette spécialité du diplôme d'Etat.
