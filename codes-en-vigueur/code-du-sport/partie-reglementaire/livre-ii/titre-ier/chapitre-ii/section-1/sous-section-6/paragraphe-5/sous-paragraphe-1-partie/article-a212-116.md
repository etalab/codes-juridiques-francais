# Article A212-116

Le candidat à la partie commune du brevet d'Etat d'éducateur sportif du premier degré doit satisfaire à des épreuves portant sur le programme des connaissances fixé en annexe II-6 au présent code. Cet examen comprend :

A. ― Une épreuve écrite (durée : deux heures ; coefficient 2).

L'épreuve écrite comporte deux questions (notées sur 20, affectées chacune d'un coefficient 1) relatives à l'activité du pratiquant. Pour répondre à ces questions, le candidat fait référence aux connaissances issues des sciences biologiques et des sciences humaines, nécessaires à l'éducateur sportif.

B. ― Une épreuve orale (préparation : une heure, exposé : dix minutes maximum par thème ; coefficient 2).

L'épreuve orale comporte plusieurs questions portant sur trois thèmes :

― le cadre institutionnel, socio-économique et juridique dans lequel s'inscrit la pratique des activités physiques et sportives ;

― gestion, promotion, communication liées aux champs d'activité des activités physiques et sportives (APS) ;

― l'esprit sportif.
