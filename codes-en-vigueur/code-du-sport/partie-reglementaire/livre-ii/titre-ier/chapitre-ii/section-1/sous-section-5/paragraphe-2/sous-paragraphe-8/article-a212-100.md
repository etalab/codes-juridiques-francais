# Article A212-100

Le directeur régional de la jeunesse, des sports et de la vie associative qui a habilité l'organisme dispensateur d'une formation peut, pour les personnes présentant une altération substantielle, durable ou définitive d'une ou plusieurs fonctions physiques, sensorielles, mentales, cognitives ou psychiques, d'un polyhandicap ou trouble de santé invalidant et après avis d'un médecin agréé par la fédération française handisport ou par la fédération française du sport adapté, aménager le cursus de formation et les épreuves d'évaluation certificative.
