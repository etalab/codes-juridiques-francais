# Sous-section 5 : Diplôme d'Etat supérieur de la jeunesse,  de l'éducation populaire et du sport

- [Paragraphe 1 : Spécialité « animation socio-éducative ou culturelle »](paragraphe-1)
- [Paragraphe 2 : Spécialité « performance sportive »](paragraphe-2)
