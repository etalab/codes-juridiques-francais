# Sous-paragraphe 7 : Validation des acquis de l'expérience

- [Article A212-94](article-a212-94.md)
- [Article A212-95](article-a212-95.md)
- [Article A212-96](article-a212-96.md)
- [Article A212-97](article-a212-97.md)
