# Article A322-175

L'encadrement est adapté à la nature de l'activité, au niveau et au nombre des pratiquants.

Pour les séances de vol encadrées telles que définies au 1° de l'article A. 322-167 et au 2° de l'article A. 322-167, l'encadrement comprend au minimum :

1° Un opérateur habilité par l'exploitant à la conduite de la machine ;

2° Un moniteur titulaire d'un diplôme, titre à finalité professionnelle ou certificat de qualification, requis par l'article L. 212-1, permettant l'enseignement de la chute libre en soufflerie, ayant reçu de l'exploitant une formation adaptée aux spécificités de la machine qui ne saurait être inférieure à 50 heures.

Pour les séances de vols telles que définies à l'article A. 322-168, la surveillance comprend au minimum un opérateur habilité par l'exploitant à la conduite de la machine.

L'opérateur à la machine doit être capable d'alerter les secours et de prodiguer les premiers soins en attente des services de secours.
