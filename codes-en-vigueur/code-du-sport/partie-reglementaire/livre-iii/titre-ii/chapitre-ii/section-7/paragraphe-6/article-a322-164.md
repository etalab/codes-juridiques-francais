# Article A322-164

Outre les prescriptions de l'article R. 322-4, tout établissement dispose des moyens matériels suivants :

1° Un plan ou une vue aérienne de la zone d'atterrissage permettant de repérer les obstacles éventuels situés aux abords de la zone d'atterrissage ;

2° Une manche à air ou une flamme indiquant le vent ;

3° Une liaison radio sol-air ;

4° Un anémomètre ;

5° Un moyen d'alerte des secours.
