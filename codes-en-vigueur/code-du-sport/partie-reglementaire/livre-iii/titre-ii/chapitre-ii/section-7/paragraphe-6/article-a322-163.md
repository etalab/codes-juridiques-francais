# Article A322-163

Les moyens techniques sont adaptés à la nature de l'activité, au niveau et au nombre des pratiquants.
