# Article A322-137

En cas de blessures et atteintes graves, un vétérinaire doit être consulté. En cas de blessures superficielles, frottements échauffements, coupures ou autres atteintes bénignes, les premiers soins élémentaires doivent être immédiatement apportés.
