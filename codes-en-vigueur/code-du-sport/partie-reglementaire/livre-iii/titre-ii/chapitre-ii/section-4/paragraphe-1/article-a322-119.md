# Article A322-119

Sont dispensés de la déclaration visée à l'article A. 322-117 :

― les établissements hippiques existant au 3 avril 1979 et classés conformément à la réglementation applicable alors ;

― les établissements professionnels existant à la même date et dont l'exploitant est titulaire d'une carte d'identité professionnelle délivrée à cet effet par le ministre chargé de l'agriculture ;

― les établissements d'entraînement de chevaux de courses dirigés par une personne titulaire d'une licence délivrée à cet effet par la Société d'encouragement pour l'amélioration des races de chevaux en France ou la Société des steeple-chases de France ou la Société d'encouragement à l'élevage du cheval français ;

― les établissements ouverts au public pour l'utilisation d'équidés agréés par le ministre chargé de l'agriculture. Cet agrément est délivré après avis du Conseil hippique régional, s'il s'agit de la pratique de l'équitation.
