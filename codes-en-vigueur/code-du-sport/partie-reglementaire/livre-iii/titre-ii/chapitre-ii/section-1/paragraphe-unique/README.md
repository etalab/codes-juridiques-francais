# Paragraphe unique : Obligation de déclarer l'exploitation  d'un établissement d'activités physiques ou sportives

- [Article A322-1](article-a322-1.md)
- [Article A322-2](article-a322-2.md)
- [Article A322-3](article-a322-3.md)
