# Section 6 : Etablissements de pratique  de tir aux armes de chasse

- [Article A322-142](article-a322-142.md)
- [Article A322-143](article-a322-143.md)
- [Article A322-144](article-a322-144.md)
- [Article A322-145](article-a322-145.md)
- [Article A322-146](article-a322-146.md)
