# Chapitre II : Garanties d'hygiène et de sécurité

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Etablissements de natation  et d'activités aquatiques](section-2)
- [Section 3 : Etablissements organisant la pratique   de la plongée subaquatique](section-3)
- [Section 4 : Etablissements ouverts au public  pour l'utilisation d'équidés](section-4)
- [Section 5 : Salles où sont pratiqués les arts martiaux.](section-5)
- [Section 6 : Etablissements de pratique  de tir aux armes de chasse](section-6)
- [Section 7 : Etablissements organisant la pratique du parachutisme](section-7)
- [Section 8 : Prévention des risques résultant de l'usage des équipements de protection individuelle pour la pratique sportive ou de loisirs](section-8)
