# Article A322-57

Lorsque l'activité est encadrée, le cadre est équipé comme les pratiquants.

Il a en permanence à sa disposition une corde de sécurité flottante, un système de remorquage largable, un couteau, des mousquetons et une longe de redressement.

Le responsable de l'établissement doit prévoir pour chaque embarcation ou groupe d'embarcations :

― un gonfleur et un kit de réparation, suivant l'accessibilité de la rivière ;

― une pagaie ou un aviron de rechange ;

― une trousse de secours lorsque les conditions d'isolement l'exigent.
