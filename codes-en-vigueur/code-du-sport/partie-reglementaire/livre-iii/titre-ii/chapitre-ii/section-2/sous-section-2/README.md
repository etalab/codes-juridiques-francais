# Sous-section 2 : Etablissements organisant la pratique  de certaines activités nautiques

- [Paragraphe 1 : Champ d'application](paragraphe-1)
- [Paragraphe 2 : Pratique du canoë, du kayak et de la nage en eau vive](paragraphe-2)
- [Paragraphe 3 : Pratique avec des embarcations gonflables](paragraphe-3)
- [Paragraphe 4 : Pratique en mer](paragraphe-4)
