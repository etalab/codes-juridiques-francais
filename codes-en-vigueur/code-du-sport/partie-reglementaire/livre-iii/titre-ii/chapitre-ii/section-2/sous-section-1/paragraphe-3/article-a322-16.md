# Article A322-16

Le plan d'organisation de la surveillance et des secours, partie intégrante de la déclaration mentionnée à l'article R. 322-1, doit être obligatoirement connu de tous les personnels permanents ou occasionnels de l'établissement.

L'exploitant doit s'assurer que ces personnels sont en mesure de mettre en application ledit plan.
