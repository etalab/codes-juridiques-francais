# Article A331-5

Le calendrier de la fédération sportive sous l'égide de laquelle l'épreuve a été placée indique les avis rendus en application de l'article R. 331-9-1.

La fédération indique sur ce calendrier et selon ses propres modalités, pour chaque manifestation inscrite, si l'avis délivré par la fédération délégataire est favorable ou s'il n'a pas été rendu.

Le calendrier de la fédération concernée et l'indication sur celui-ci de l'avis s'y rapportant constituent la modalité de publication prévue à l'article R. 331-9-1.
