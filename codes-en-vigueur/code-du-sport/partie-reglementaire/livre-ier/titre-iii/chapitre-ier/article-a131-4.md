# Article A131-4

L'accès à ces décisions doit être facilité.

Pour ce faire, celles-ci doivent figurer dans un classement chronologique et par thèmes.
