# Section 1 : Dispositions générales.

- [Article D5334-3](article-d5334-3.md)
- [Article D5334-4](article-d5334-4.md)
- [Article D5334-5](article-d5334-5.md)
- [Article D5334-6](article-d5334-6.md)
- [Article D5334-7](article-d5334-7.md)
- [Article R5334-1](article-r5334-1.md)
- [Article R5334-2](article-r5334-2.md)
