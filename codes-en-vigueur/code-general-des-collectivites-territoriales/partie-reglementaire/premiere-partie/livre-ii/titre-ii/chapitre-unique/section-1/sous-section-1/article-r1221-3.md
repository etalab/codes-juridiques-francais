# Article R1221-3

Dans le délai d'un mois après son installation, le Conseil national désigne en son sein un président.

Celui-ci est choisi parmi les membres élus locaux.
