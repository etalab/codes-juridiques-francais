# Article R1213-30

Afin d'instruire les demandes d'évaluation de normes réglementaires en vigueur, il est créé au sein du conseil national une ou plusieurs formations spécialisées. Le règlement intérieur du conseil national en précise l'organisation et les modalités de fonctionnement.

La formation spécialisée est saisie par le président ou un vice-président des demandes d'évaluation. Elle se prononce sur la recevabilité des demandes d'évaluation présentées par les collectivités territoriales et les établissements publics de coopération intercommunale à fiscalité propre au regard des dispositions du premier alinéa du V de l'article L. 1212-2 et des conditions fixées à l'article R. 1213-29.

Le président du conseil national, sur proposition de la majorité des membres représentant les élus du conseil, saisit la formation spécialisée de toutes normes réglementaires en vigueur dont il estime l'évaluation nécessaire.

La formation spécialisée demande, en tant que de besoin, aux autorités qui ont saisi le conseil national tout élément de nature à faciliter l'évaluation des normes réglementaires.

Les services de l'administration à l'origine de la norme prêtent leur concours et adressent les éléments de nature à permettre les échanges sur cette norme et à éclairer les avis du conseil.

La formation spécialisée a trois mois à compter de sa saisine pour procéder à l'instruction des demandes et préparer le projet d'avis d'évaluation.
