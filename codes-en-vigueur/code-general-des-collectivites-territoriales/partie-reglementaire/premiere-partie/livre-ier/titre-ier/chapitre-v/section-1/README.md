# Section 1 : Groupements d'intérêt public (R)

- [Article D1115-1](article-d1115-1.md)
- [Article D1115-2](article-d1115-2.md)
- [Article D1115-3](article-d1115-3.md)
- [Article D1115-4](article-d1115-4.md)
- [Article D1115-5](article-d1115-5.md)
- [Article D1115-6](article-d1115-6.md)
- [Article D1115-7](article-d1115-7.md)
