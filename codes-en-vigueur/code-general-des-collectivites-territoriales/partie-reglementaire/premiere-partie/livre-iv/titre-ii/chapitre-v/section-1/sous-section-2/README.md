# Sous-section 2 : Compétences (R)

- [Paragraphe 1 : Avis sur les conventions de transfert de personnels et de biens (R)](paragraphe-1)
- [Paragraphe 2 : Fixation du montant des dépenses obligatoires d'incendie et de secours avant l'entrée en vigueur des conventions (R).](paragraphe-2)
