# Section 4 : Retrait et dissolution

- [Article R1431-19](article-r1431-19.md)
- [Article R1431-20](article-r1431-20.md)
- [Article R1431-21](article-r1431-21.md)
