# Article R1311-8

Dans le cas d'un déclassement du domaine public fluvial d'une collectivité territoriale ou d'un groupement de collectivités territoriales, la procédure d'enquête publique prévue au deuxième alinéa de l'article L. 2142-1 du code général de la propriété des personnes publiques est menée par la collectivité dans les conditions prévues pour les enquêtes publiques relevant de l'article L. 110-2 du code de l'expropriation pour cause d'utilité publique régies par le titre Ier du livre Ier du même code.
