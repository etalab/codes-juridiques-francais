# CHAPITRE Ier : Principes généraux

- [Section 1 : Recettes (R)](section-1)
- [Section 2 : Dépenses (R)](section-2)
- [Section 3 : Mandats confiés par les collectivités territoriales et leurs établissements publics en application de l'article L. 1611-7](section-3)
- [Section 4 : Encadrement des conditions d'emprunt et de souscription de contrats financiers](section-4)
