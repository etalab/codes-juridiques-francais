# Article R2334-37

Pour l'application des quatrième, cinquième et sixième alinéas de l'article L. 2334-40, le montant des crédits revenant à chaque département est égal à la somme des deux montants suivants :

1° La somme des attributions calculées en application du cinquième alinéa de l'article L. 2334-40 pour chaque commune éligible du département classée parmi les cent vingt premières du classement mentionné au deuxième alinéa de l'article L. 2334-40 ; l'attribution de chaque commune est calculée en fonction du produit de sa population, telle que définie à l'article L. 2334-2, par l'indice synthétique défini au II de l'article R. 2334-36, sans que ce montant puisse excéder cinq millions d'euros ;

2° La somme des attributions calculées en application du sixième alinéa de l'article L. 2334-40 pour chaque commune éligible du département classée parmi les soixante premières du classement mentionné au deuxième alinéa de l'article L. 2334-40 ; l'attribution de chaque commune est calculée en fonction du produit de sa population, telle que définie à l'article L. 2334-2, par l'indice synthétique défini au II de l'article R. 2334-36, sans que ce montant puisse excéder un million d'euros.
