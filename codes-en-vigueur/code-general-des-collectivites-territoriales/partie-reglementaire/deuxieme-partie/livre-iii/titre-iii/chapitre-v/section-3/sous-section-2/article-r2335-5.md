# Article R2335-5

Les majorations de subvention d'équipement pour les opérations entreprises par les communes fusionnées et les communes nouvelles sont attribuées par le préfet.

Des crédits lui sont délégués à cet effet par le ministre de l'intérieur.
