# Article D2531-2

Le Syndicat des transports d'Ile-de-France est crédité du montant encaissé au titre du versement de transport, après déduction de la retenue prévue à l'article L. 2531-7 :

1° Mensuellement, lorsqu'il est recouvré par les organismes mentionnés aux articles L. 213-1 et L. 752-4 du code de la sécurité sociale. Il fait alors l'objet d'un reversement par l'Agence centrale des organismes de sécurité sociale selon des modalités précisées par arrêté du ministre de l'intérieur, du ministre chargé de la sécurité sociale, du ministre chargé du budget et du ministre chargé des transports ;

2° Trimestriellement, lorsqu'il est recouvré par les caisses de mutualité sociale agricole.
