# Article R2563-4

La quote-part de la dotation de solidarité urbaine et de cohésion sociale et de la dotation de solidarité rurale revenant aux communes de chaque département d'outre-mer est répartie entre les communes de ce département proportionnellement à leur population, telle que définie à l'article L. 2334-2.
