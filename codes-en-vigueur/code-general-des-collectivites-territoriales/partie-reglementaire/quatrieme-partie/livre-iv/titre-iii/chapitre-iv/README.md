# CHAPITRE IV : Dispositions financières et fiscales

- [Section 1 : Conseil économique et social régional et conseil de la culture, de l'éducation et de l'environnement (R).](section-1)
- [Section 2 : Dotation régionale d'équipement scolaire (R).](section-2)
- [Section 3 : Emploi et formation professionnelle (R).](section-3)
- [Section 4 :  Péréquation des ressources fiscales](section-4)
