# Article R3335-1

Pour l'application de l'article L. 3335-1 :

1° La population et le potentiel financier pris en compte sont ceux calculés au titre de l'année de répartition ;

2° Le revenu médian correspond à la médiane des revenus moyens par habitant des départements ;

3° Le nombre de bénéficiaires dont les ressources sont inférieures au montant forfaitaire mentionné au 2 de l'article L. 262-2 du code de l'action sociale et des familles applicable au foyer pris en compte est celui constaté au titre de la pénultième année.
