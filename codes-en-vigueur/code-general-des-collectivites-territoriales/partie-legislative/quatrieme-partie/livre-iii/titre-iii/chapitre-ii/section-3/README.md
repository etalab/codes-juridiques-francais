# Section 3 : Dotation globale de fonctionnement

- [Sous-section 1 : Dispositions générales.](sous-section-1)
- [Sous-section 2 : Dotation forfaitaire.](sous-section-2)
- [Sous-section 3 : Dotation de péréquation.](sous-section-3)
