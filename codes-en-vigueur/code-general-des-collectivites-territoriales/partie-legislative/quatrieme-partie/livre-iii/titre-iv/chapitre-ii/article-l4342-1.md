# Article L4342-1

Le comptable de la région est seul chargé d'exécuter, sous sa responsabilité et sous réserve des contrôles qui lui incombent, le recouvrement des recettes ainsi que le paiement des dépenses de la collectivité dans la limite des crédits régulièrement ouverts par le conseil régional.
