# Article L4141-2

Sont soumis aux dispositions de l'article L. 4141-1 les actes suivants :

1° Les délibérations du conseil régional ou les décisions prises par la commission permanente par délégation du conseil régional à l'exception des délibérations relatives aux taux de promotion pour l'avancement de grade des fonctionnaires, à l'affiliation ou à la désaffiliation aux centres de gestion ainsi qu'aux conventions portant sur les missions supplémentaires à caractère facultatif confiées aux centres de gestion ;

2° Les actes à caractère réglementaire pris par les autorités régionales dans les domaines qui relèvent de leur compétence en application de la loi ;

3° Les conventions relatives aux emprunts, aux marchés et aux accords-cadres, à l'exception des conventions relatives à des marchés et à des accords-cadres d'un montant inférieur à un seuil défini par décret, ainsi que les conventions de concession ou d'affermage de services publics locaux et les contrats de partenariat ;

4° Les décisions individuelles relatives à la nomination, au recrutement, y compris le contrat d'engagement, et au licenciement des agents non titulaires, à l'exception de celles prises pour faire face à un besoin lié à un accroissement temporaire ou saisonnier d'activité, en application des 1° et 2° de l'article 3 de la loi n° 84-53 du 26 janvier 1984 portant dispositions statuaires relatives à la fonction publique territoriale ;

5° Les ordres de réquisition du comptable pris par le président du conseil régional ;

6° Les décisions relevant de l'exercice de prérogatives de puissance publique, prises par des sociétés d'économie mixte locales pour le compte d'une région ou d'un établissement public de coopération interrégionale ;

7° (Supprimé) ;

8° Les décisions prises par les régions d'outre-mer en application des articles L. 611-31 et L. 611-32 du code minier ;

9° Les décisions prises par les régions d'outre-mer en application de l'article L. 4433-15-1.
