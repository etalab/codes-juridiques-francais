# CHAPITRE V : Conditions d'exercice des mandats régionaux

- [Section 1 : Garanties accordées aux titulaires de mandats régionaux](section-1)
- [Section 2 : Droit à la formation.](section-2)
- [Section 3 : Indemnités des titulaires de mandats régionaux.](section-3)
- [Section 4 : Protection sociale](section-4)
- [Section 5 : Responsabilité de la région en cas d'accident.](section-5)
- [Section 7 : Honorariat des anciens conseillers régionaux](section-7)
