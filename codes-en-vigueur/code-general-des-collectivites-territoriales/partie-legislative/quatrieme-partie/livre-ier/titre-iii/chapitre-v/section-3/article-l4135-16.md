# Article L4135-16

Les indemnités maximales votées par les conseils régionaux pour l'exercice effectif des fonctions de conseiller régional sont déterminées en appliquant au terme de référence mentionné à l'article L. 4135-15 le barème suivant :

<table>
<tbody>
<tr>
<td>
<p align="center">POPULATION RÉGIONALE (habitants) </p>
</td>
<td>
<p align="center">TAUX MAXIMAL <br/>(en %) </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="justify">Moins de 1 million </p>
</td>
<td valign="top">
<p align="center">40 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="justify">De 1 million à moins de 2 millions </p>
</td>
<td valign="top">
<p align="center">50 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="justify">De 2 millions à moins de 3 millions </p>
</td>
<td valign="top">
<p align="center">60 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="justify">3 millions et plus </p>
</td>
<td valign="top">
<p align="center">70 </p>
</td>
</tr>
</tbody>
</table>

Dans des conditions fixées par le règlement intérieur, le montant des indemnités que le conseil régional alloue à ses membres est modulé en fonction de leur participation effective aux séances plénières et aux réunions des commissions dont ils sont membres. La réduction éventuelle de ce montant ne peut dépasser, pour chacun des membres, la moitié de l'indemnité pouvant lui être allouée en application du présent article.
