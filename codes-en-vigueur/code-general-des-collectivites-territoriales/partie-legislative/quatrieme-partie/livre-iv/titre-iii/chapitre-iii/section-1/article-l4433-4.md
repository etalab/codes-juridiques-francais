# Article L4433-4

Les conseils régionaux de Guadeloupe, de Guyane, de Martinique peuvent être saisis pour avis de tous projets d'accords concernant la coopération régionale en matière économique, sociale, technique, scientifique, culturelle, de sécurité civile ou d'environnement entre la République française et les Etats de la mer Caraïbe ou les Etats voisins de la Guyane.

Le conseil régional de la Réunion et le conseil général de Mayotte peuvent être saisis dans les mêmes conditions des projets d'accords entre la République française et les Etats de l'océan Indien.

Ils se prononcent à la première réunion qui suit leur saisine.
