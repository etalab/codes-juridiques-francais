# Article L4432-1

Les conseils régionaux de Guadeloupe et de Martinique comprennent chacun quarante et un membres.

Le conseil régional de la Réunion comprend quarante-cinq membres.

Le conseil régional de Guyane comprend trente et un membres.
