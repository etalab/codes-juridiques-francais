# Article L4221-1

Le conseil régional règle par ses délibérations les affaires de la région.

Il statue sur tous les objets sur lesquels il est appelé à délibérer  par les lois et règlements et sur tous les objets d'intérêt régional  dont il est saisi.

Il a compétence pour  promouvoir le développement économique, social, sanitaire, culturel et  scientifique de la région et l'aménagement de son territoire, ainsi que  pour assurer la préservation de son identité et la promotion des langues  régionales, dans le respect de l'intégrité, de l'autonomie et des  attributions des départements et des communes.

Il peut engager des actions complémentaires de celles de l'Etat, des autres collectivités territoriales et des établissements publics situés dans la région, dans les domaines et les conditions fixés par les lois déterminant la répartition des compétences entre l'Etat, les communes, les départements et les régions.
