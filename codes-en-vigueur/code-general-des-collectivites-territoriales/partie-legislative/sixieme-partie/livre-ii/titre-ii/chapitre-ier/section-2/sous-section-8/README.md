# Sous-section 8 : Relations avec le représentant de l'Etat.

- [Article LO6221-30](article-lo6221-30.md)
- [Article LO6221-31](article-lo6221-31.md)
- [Article LO6221-32](article-lo6221-32.md)
- [Article LO6221-33](article-lo6221-33.md)
- [Article LO6221-34](article-lo6221-34.md)
