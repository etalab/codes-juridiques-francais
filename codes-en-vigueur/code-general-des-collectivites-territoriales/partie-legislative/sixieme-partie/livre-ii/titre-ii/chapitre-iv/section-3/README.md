# Section 3 : Responsabilité et protection des élus

- [Article L6224-7](article-l6224-7.md)
- [Article L6224-10](article-l6224-10.md)
- [Article LO6224-8](article-lo6224-8.md)
- [Article LO6224-9](article-lo6224-9.md)
