# Article L5915-3

Le conseil général et le conseil régional délibèrent sur les propositions du congrès des élus départementaux et régionaux.

Les délibérations adoptées par le conseil général et le conseil régional sont transmises au Premier ministre par le président de l'assemblée concernée.
