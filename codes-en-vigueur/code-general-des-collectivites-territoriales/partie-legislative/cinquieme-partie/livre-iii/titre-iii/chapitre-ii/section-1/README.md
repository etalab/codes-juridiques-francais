# Section 1 : Composition du comité du syndicat d'agglomération nouvelle.

- [Article L5332-1](article-l5332-1.md)
- [Article L5332-2](article-l5332-2.md)
