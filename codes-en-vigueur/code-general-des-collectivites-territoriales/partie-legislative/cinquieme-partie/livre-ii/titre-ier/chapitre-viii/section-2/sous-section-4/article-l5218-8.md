# Article L5218-8

Le montant total des dépenses et des recettes de fonctionnement et d'investissement de chaque conseil de territoire est inscrit dans le budget de la métropole.

Les dépenses et les recettes de fonctionnement et d'investissement de chaque conseil de territoire sont détaillées dans un document dénommé " état spécial de territoire ”. Les états spéciaux de territoire sont annexés au budget de la métropole.

Les recettes de fonctionnement et d'investissement dont dispose le conseil de territoire sont constituées d'une dotation de gestion du territoire.

La dotation de gestion du territoire est attribuée pour l'exercice des attributions prévues à l'article L. 5218-7.

Le montant des sommes destinées aux dotations de gestion du territoire est fixé par l'organe délibérant de la métropole. Ces sommes sont réparties entre les conseils de territoire en tenant compte des caractéristiques propres du territoire. Elles constituent des dépenses obligatoires pour la métropole.
