# Article L5218-5

Le siège du conseil de territoire est fixé par le règlement intérieur de la métropole.
