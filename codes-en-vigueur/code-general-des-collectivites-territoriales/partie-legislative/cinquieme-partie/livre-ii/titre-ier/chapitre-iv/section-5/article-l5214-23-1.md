# Article L5214-23-1

Les communautés de communes faisant application des dispositions de l'article 1609 nonies C du code général des impôts dont la population est comprise entre 3 500 habitants et 50 000 habitants au plus ou, lorsqu'elle est inférieure à 3 500 habitants, et qu'elles sont situées en zone de revitalisation rurale de montagne et comprennent au moins dix communes dont une commune siège du bureau centralisateur ou un chef-lieu de canton à la date du 1er janvier 2014 ou la totalité des communes d'un canton ou bien, lorsqu'elle est supérieure à 50 000 habitants, n'inclut pas de commune centre ou de commune chef-lieu de département de plus de 15 000 habitants, sont éligibles à la dotation prévue au quatrième alinéa du II de l'article L. 5211-29 lorsqu'elles exercent au moins quatre des neuf groupes de compétences suivants :

1° En matière de développement économique : aménagement, entretien et gestion de zones d'activité industrielle, commerciale, tertiaire, artisanale ou touristique qui sont d'intérêt communautaire ; actions de développement économique d'intérêt communautaire ;

2° En matière d'aménagement de l'espace communautaire : schéma de cohérence territoriale et schéma de secteur ; à compter du 1er janvier 2018, plan local d'urbanisme, document d'urbanisme en tenant lieu et carte communale ; zones d'aménagement concerté d'intérêt communautaire ;

2° bis Gestion des milieux aquatiques et prévention des inondations dans les conditions prévues à l'article L. 211-7 du code de l'environnement ;

3° Création ou aménagement et entretien de voirie d'intérêt communautaire ;

4° Politique du logement social d'intérêt communautaire et action, par des opérations d'intérêt communautaire, en faveur du logement des personnes défavorisées ;

4° bis En matière de politique de la ville : élaboration du diagnostic du territoire et définition des orientations du contrat de ville ; animation et coordination des dispositifs contractuels de développement urbain, de développement local et d'insertion économique et sociale ainsi que des dispositifs locaux de prévention de la délinquance ; programmes d'actions définis dans le contrat de ville ;

5° Collecte et traitement des déchets des ménages et déchets assimilés ;

6° En matière de développement et d'aménagement sportif de l'espace communautaire : construction, aménagement, entretien et gestion des équipements sportifs d'intérêt communautaire.

7° En matière d'assainissement : l'assainissement collectif et l'assainissement non collectif.

L'éligibilité à la dotation précitée est constatée à la date à laquelle la communauté de communes remplit l'ensemble des conditions requises, par arrêté du représentant de l'Etat dans le département lorsque les communes font partie du même département ou par arrêté conjoint des représentants de l'Etat dans les départements concernés dans le cas contraire.

Pour l'application du premier alinéa, les limites territoriales des cantons sont appréciées au 1er janvier 2014.
