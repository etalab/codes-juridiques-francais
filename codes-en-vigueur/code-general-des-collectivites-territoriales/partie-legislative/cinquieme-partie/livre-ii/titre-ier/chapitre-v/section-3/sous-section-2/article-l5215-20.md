# Article L5215-20

I. - La communauté urbaine exerce de plein droit, au lieu et place des communes membres, les compétences suivantes :

1° En matière de développement et d'aménagement économique, social et culturel de l'espace communautaire :

a) Création, aménagement, entretien et gestion de zones d'activité industrielle, commerciale, tertiaire, artisanale, touristique, portuaire ou aéroportuaire ;

b) Actions de développement économique ;

c) Construction ou aménagement, entretien, gestion et animation d'équipements, de réseaux d'équipements ou d'établissements culturels, socioculturels, socio-éducatifs, sportifs, lorsqu'ils sont d'intérêt communautaire ;

d) Lycées et collèges dans les conditions fixées au titre Ier du livre II et au chapitre Ier du titre II du livre IV ainsi qu'à l'article L. 521-3 du code de l'éducation ;

e) Promotion du tourisme, dont la création d'offices de tourisme ;

f) Programme de soutien et d'aides aux établissements d'enseignement supérieur et de recherche et aux programmes de recherche ;

2° En matière d'aménagement de l'espace communautaire :

a) Schéma de cohérence territoriale et schéma de secteur ; plan local d'urbanisme et documents d'urbanisme en tenant lieu ; définition, création et réalisation d'opérations d'aménagement d'intérêt communautaire, au sens de l'article L. 300-1 du code de l'urbanisme ; et après avis des conseils municipaux, constitution de réserves foncières ;

b) Organisation de la mobilité au sens des articles L. 1231-1, L. 1231-8 et L. 1231-14 à L. 1231-16 du code des transports, sous réserve de l'article L. 3421-2 du même code ; création, aménagement et entretien de voirie ; signalisation ; parcs et aires de stationnement ; plan de déplacements urbains ;

3° En matière d'équilibre social de l'habitat sur le territoire communautaire :

a) Programme local de l'habitat ;

b) Politique du logement ; aides financières au logement social ; actions en faveur du logement social ; action en faveur du logement des personnes défavorisées ;

c) Opérations programmées d'amélioration de l'habitat, actions de réhabilitation et résorption de l'habitat insalubre ;

4° En matière de politique de la ville : élaboration du diagnostic du territoire et définition des orientations du contrat de ville ; animation et coordination des dispositifs contractuels de développement urbain, de développement local et d'insertion économique et sociale ainsi que des dispositifs locaux de prévention de la délinquance ; programmes d'actions définis dans le contrat de ville ;

5° En matière de gestion des services d'intérêt collectif :

a) Assainissement et eau ;

b) Création, extension et translation des cimetières, ainsi que création et extension des crématoriums et des sites cinéraires ;

c) Abattoirs, abattoirs marchés et marchés d'intérêt national ;

d) Services d'incendie et de secours, dans les conditions fixées au chapitre IV du titre II du livre IV de la première partie ;

e) Contribution à la transition énergétique ;

f) Création, aménagement, entretien et gestion de réseaux de chaleur ou de froid urbains ;

g) Concessions de la distribution publique d'électricité et de gaz ;

h) Création et entretien des infrastructures de charge de véhicules électriques ;

6° En matière de protection et mise en valeur de l'environnement et de politique du cadre de vie :

a) Collecte et traitement des déchets des ménages et déchets assimilés ;

b) Lutte contre la pollution de l'air ;

c) Lutte contre les nuisances sonores ;

d) Soutien aux actions de maîtrise de la demande d'énergie ;

e) Gestion des milieux aquatiques et prévention des inondations, dans les conditions prévues à l'article L. 211-7 du code de l'environnement.

7° Aménagement, entretien et gestion des aires d'accueil des gens du voyage.

Lorsque l'exercice des compétences mentionnées au présent paragraphe est subordonné à la reconnaissance de leur intérêt communautaire, cet intérêt est déterminé à la majorité des deux tiers du conseil de la communauté urbaine. Il est défini au plus tard deux ans après l'entrée en vigueur de l'arrêté prononçant le transfert de compétence. A défaut, la communauté urbaine exerce l'intégralité de la compétence transférée.

II. - (Abrogé).

III. - Par convention passée avec le département, une communauté urbaine peut exercer pour le département tout ou partie des compétences qui, dans le domaine de l'action sociale, sont attribuées au département en vertu des articles L. 121-1 et L. 121-2 du code de l'action sociale et des familles.

La convention précise l'étendue et les conditions financières de la délégation ainsi que les conditions dans lesquelles les services départementaux correspondants sont mis à la disposition de la communauté urbaine.

IV. - Par convention passée avec le département, une communauté urbaine dont le plan de déplacements urbains comprend la réalisation d'un service de transport collectif en site propre empruntant des voiries départementales ou prévoit sa réalisation peut, dans le périmètre de transports urbains, exercer en lieu et place du département tout ou partie des compétences qui, dans le domaine de la voirie, sont attribuées au département en vertu des articles L. 131-1 à L. 131-8 du code de la voirie routière. Le refus du conseil départemental de déléguer tout ou partie de ces compétences doit être motivé par délibération. La convention précise l'étendue et les conditions financières de la délégation de compétence ainsi que les conditions dans lesquelles les services départementaux correspondants sont mis à la disposition de la communauté urbaine.

V. - Le conseil de la communauté urbaine est consulté lors de l'élaboration, de la révision et de la modification des schémas et documents de planification en matière d'aménagement, de développement économique et d'innovation, d'enseignement supérieur et de recherche, de transports et d'environnement, dont la liste est fixée par décret en Conseil d'Etat et qui relèvent de la compétence de l'Etat, d'une collectivité territoriale ou de leurs établissements publics, lorsque ces schémas et documents ont une incidence ou un impact sur le territoire de la communauté urbaine.

Le conseil de la communauté urbaine est consulté par le conseil régional lors de l'élaboration du contrat de plan conclu entre l'Etat et la région en application du chapitre III du titre Ier de la loi n° 82-653 du 29 juillet 1982 portant réforme de la planification, afin de tenir compte des spécificités de son territoire.
