# Paragraphe 1 : Organe délibérant des établissements publics de coopération intercommunale à fiscalité propre

- [Article L5211-6](article-l5211-6.md)
- [Article L5211-6-1](article-l5211-6-1.md)
- [Article L5211-6-2](article-l5211-6-2.md)
- [Article L5211-6-3](article-l5211-6-3.md)
