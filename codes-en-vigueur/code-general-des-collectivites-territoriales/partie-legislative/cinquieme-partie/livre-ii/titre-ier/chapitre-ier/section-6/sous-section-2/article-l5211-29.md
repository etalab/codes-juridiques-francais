# Article L5211-29

I. Le montant total de la dotation d'intercommunalité visé à l'article L. 5211-28 est fixé chaque année par le comité des finances locales qui le répartit entre les cinq catégories de groupements suivants :

1° Les communautés urbaines, les métropoles, y compris celle d'Aix-Marseille-Provence, la métropole de Lyon ;

2° Les communautés de communes ne faisant pas application des dispositions de l'article 1609 nonies C du code général des impôts ;

3° Les communautés de communes faisant application des dispositions de l'article 1609 nonies C du code général des impôts ;

4° Les syndicats d'agglomération nouvelle ;

5° Les communautés d'agglomération créées avant le 1er janvier 2005 ;

II. A compter de 2011, la dotation moyenne par habitant de la catégorie des communautés d'agglomération est égale à 45,40 €.

A compter de 2011, la dotation moyenne par habitant de la catégorie des communautés de communes ne faisant pas application de l'article 1609 nonies C du code général des impôts est égale à 20,05 € par habitant.

A compter de 2011, la dotation moyenne par habitant de la catégorie des communautés de communes faisant application des dispositions du même article 1609 nonies C est égale à 24,48 € par habitant.

A compter de 2011, la dotation par habitant de la catégorie des communautés de communes qui remplissent les conditions visées à l'article L. 5214-23-1 du présent code est majorée d'une somme lui permettant d'atteindre 34,06 €.

Les modalités de répartition de la majoration prévue au précédent alinéa sont précisées à l'article L. 5211-30.

La dotation par habitant de la catégorie des communautés urbaines ayant opté pour les dispositions de l'article 1609 nonies C du code général des impôts ne peut être inférieure à celle fixée pour la catégorie des communautés urbaines ne faisant pas application de ces dispositions.

Le montant de la dotation d'intercommunalité affecté à la catégorie définie au 1° du I du présent article est celui qui résulte de l'application du 2 du I de l'article L. 5211-30.

A compter de 2002, la dotation moyenne par habitant des communautés de communes ne faisant pas application des dispositions de l'article 1609 nonies C du code général des impôts qui perçoivent la dotation d'intercommunalité dans cette catégorie au titre de la deuxième année au moins est majorée, le cas échéant, d'une somme lui permettant d'atteindre le montant de la dotation moyenne par habitant qui leur a été notifiée l'année précédente, augmentée comme la dotation forfaitaire visée à l'article L. 2334-7. Pour l'application de ces dispositions en 2002, la dotation moyenne par habitant prise en compte au titre de 2001 intègre la quote-part de la régularisation de la dotation globale de fonctionnement prévue par l'article L. 1613-2-1.A compter de 2011, le montant moyen par habitant correspondant à la majoration est égal à celui perçu en 2010.

Cette majoration est répartie entre les établissements publics de coopération intercommunale bénéficiaires comme les dotations de base et de péréquation auxquelles elle s'ajoute.
