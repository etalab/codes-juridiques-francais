# Article L5211-40

Le président d'un établissement public de coopération intercommunale à fiscalité propre  consulte les maires de toutes les communes membres, à la demande de l'organe délibérant de l'établissement ou du tiers des maires des communes membres.
