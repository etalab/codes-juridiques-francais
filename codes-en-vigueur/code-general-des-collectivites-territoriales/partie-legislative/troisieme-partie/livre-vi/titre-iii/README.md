# TITRE III : ORGANISATION

- [CHAPITRE Ier : Le conseil de la métropole](chapitre-ier)
- [CHAPITRE II : Conditions d'exercice des mandats métropolitains](chapitre-ii)
- [CHAPITRE III : Modalités particulières d'intervention](chapitre-iii)
