# CHAPITRE III : Transferts de charges et produits
entre le département du Rhône et la métropole de Lyon

- [Article L3663-1](article-l3663-1.md)
- [Article L3663-2](article-l3663-2.md)
- [Article L3663-3](article-l3663-3.md)
- [Article L3663-4](article-l3663-4.md)
- [Article L3663-5](article-l3663-5.md)
- [Article L3663-6](article-l3663-6.md)
- [Article L3663-7](article-l3663-7.md)
- [Article L3663-8](article-l3663-8.md)
- [Article L3663-9](article-l3663-9.md)
