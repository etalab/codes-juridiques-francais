# Article L3334-3

Chaque département reçoit une dotation forfaitaire.

I. - A compter de 2015, la dotation forfaitaire de chaque département est égale au montant perçu l'année précédente au titre de cette dotation. Pour chaque département, à l'exception du département de Paris, cette dotation est majorée ou minorée du produit de la différence entre sa population constatée au titre de l'année de répartition et celle constatée au titre de l'année précédant la répartition par un montant de 74,02 € par habitant.

II. - Cette dotation forfaitaire, est minorée d'un montant fixé par le comité des finances locales afin de financer l'accroissement de la dotation forfaitaire mentionnée au deuxième alinéa et, le cas échéant, l'accroissement de la dotation prévue à l'article L. 3334-4. Cette minoration est effectuée dans les conditions suivantes :

1° Les départements dont le potentiel financier par habitant est inférieur à 0,95 fois le potentiel financier moyen par habitant constaté au niveau national bénéficient d'une attribution au titre de leur dotation forfaitaire, calculée en application du I ;

2° La dotation forfaitaire, des départements dont le potentiel financier par habitant est supérieur ou égal à 0,95 fois le potentiel financier moyen par habitant constaté au niveau national est minorée en proportion de leur population et du rapport entre le potentiel financier par habitant du département et le potentiel financier moyen par habitant constaté au niveau national. Cette minoration ne peut être supérieure pour chaque département à 5 % de sa dotation forfaitaire, perçue l'année précédente.

III. - En 2014, le montant de la dotation forfaitaire des départements de métropole et d'outre-mer, à l'exception du Département de Mayotte, est minoré de 476 millions d'euros. Cette minoration est répartie entre les départements en fonction du produit de leur population, telle que définie à l'article L. 3334-2, par un indice synthétique. Cet indice synthétique est constitué :

a) Du rapport entre le revenu par habitant du département et le revenu moyen par habitant de l'ensemble des départements. La population prise en compte est celle issue du dernier recensement ;

b) Du rapport entre le taux moyen national d'imposition de taxe foncière sur les propriétés bâties pour l'ensemble des départements et le taux de cette taxe du département. Les taux retenus sont ceux de l'année précédant l'année de répartition.

L'indice synthétique est obtenu par addition des montants obtenus au a et au b, en pondérant le premier par 70 % et le second par 30 %.

Si, pour un département, la minoration excède le montant perçu au titre de la dotation forfaitaire de l'année de répartition, la différence est prélevée sur les compensations mentionnées au III de l'article 37 de la loi n° 2013-1278 du 29 décembre 2013 de finances pour 2014 ou, à défaut, sur les douzièmes prévus à l'article L. 3332-1-1. Toutefois, si, pour le département de Paris, la minoration excède le montant perçu au titre de la dotation forfaitaire de l'année de répartition, la différence est prélevée sur les compensations mentionnées au même III ou, à défaut, sur la dotation forfaitaire de la commune de Paris prévue à l'article L. 2334-7. Le département de Paris rembourse à la commune de Paris, le cas échéant, le montant ainsi prélevé sur sa dotation forfaitaire. Ce remboursement constitue une dépense obligatoire du département de Paris, au sens de l'article L. 3321-1.

En 2015, la dotation forfaitaire des départements de métropole et d'outre-mer, à l'exception du Département de Mayotte, est minorée de 1 148 millions d'euros. Cette minoration est répartie dans les conditions prévues aux cinq premiers alinéas du présent III.
