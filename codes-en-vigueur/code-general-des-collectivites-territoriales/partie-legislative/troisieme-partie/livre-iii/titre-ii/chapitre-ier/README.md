# CHAPITRE Ier : Dépenses obligatoires

- [Article L3321-1](article-l3321-1.md)
- [Article L3321-2](article-l3321-2.md)
