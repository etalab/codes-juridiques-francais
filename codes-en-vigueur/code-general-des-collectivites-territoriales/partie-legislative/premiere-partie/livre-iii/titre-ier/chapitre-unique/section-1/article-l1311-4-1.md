# Article L1311-4-1

Jusqu'au 31 décembre 2017, les collectivités territoriales et les établissements publics de coopération intercommunale peuvent construire, y compris sur les dépendances de leur domaine public, acquérir ou rénover des bâtiments destinés à être mis à la disposition de l'Etat pour les besoins de la justice, de la police ou de la gendarmerie nationales.

Jusqu'au 31 décembre 2017, les conseils généraux peuvent construire, y compris sur les dépendances de leur domaine public, acquérir ou rénover des bâtiments destinés à être mis à la disposition des services départementaux d'incendie et de secours.

Une convention entre l'Etat et la collectivité ou l'établissement propriétaire précise notamment les engagements financiers des parties, le lieu d'implantation de la ou des constructions projetées et le programme technique de construction. Elle fixe également la durée et les modalités de la mise à disposition des constructions.

Les constructions mentionnées au présent article ainsi que celles qui sont réalisées dans le cadre de contrats de partenariat peuvent donner lieu à la conclusion de contrats de crédit-bail. Dans ce cas, le contrat comporte des clauses permettant de préserver les exigences du service public.
