# Article L1611-7-1

A l'exclusion de toute exécution forcée de leurs créances, les collectivités territoriales et leurs établissements publics peuvent, après avis conforme de leur comptable public et par convention écrite, confier à un organisme public ou privé l'encaissement :

1° Du produit des droits d'accès à des prestations culturelles, sportives et touristiques ;

2° Du revenu tiré des immeubles leur appartenant et confiés en gérance, ou d'autres produits et redevances du domaine dont la liste est fixée par décret ;

3° Du revenu tiré des prestations assurées dans le cadre d'un contrat portant sur la gestion du service public de l'eau, du service public de l'assainissement ou de tout autre service public dont la liste est fixée par décret.

La convention emporte mandat donné à l'organisme d'assurer l'encaissement au nom et pour le compte de la collectivité territoriale ou de l'établissement public mandant. Elle prévoit une reddition au moins annuelle des comptes et des pièces correspondantes. Elle peut aussi prévoir le paiement par l'organisme mandataire du remboursement des recettes encaissées à tort.

Les dispositions comptables et financières nécessaires à l'application du présent article sont précisées par décret.
