# Section 3 : Fonds pour la réparation des dommages causés aux biens des collectivités territoriales et de leurs groupements par les calamités publiques

- [Article L1613-7](article-l1613-7.md)
