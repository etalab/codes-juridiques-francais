# TITRE VI : DISPOSITIONS ÉCONOMIQUES

- [CHAPITRE Ier : Aides aux entreprises](chapitre-ier)
- [CHAPITRE II : Sociétés d'économie mixte locales](chapitre-ii)
- [CHAPITRE III : Sociétés d'économie mixte à opération unique](chapitre-iii)
