# Paragraphe 1 : Catégories de recettes

- [Sous-paragraphe 1 : Recettes de la section de fonctionnement](sous-paragraphe-1)
- [Sous-paragraphe 2 : Recettes de la section d'investissement](sous-paragraphe-2)
- [Sous-paragraphe 3 : Répartition et recouvrement de certaines taxes](sous-paragraphe-3)
