# Article L2573-25

I.-Les articles L. 2223-1 à L. 2223-19 et le dernier alinéa de l'article L. 2223-42 sont applicables aux communes de la Polynésie française sous réserve des adaptations prévues aux II, III et IV.

II.-Pour l'application de l'article L. 2223-1, il est ajouté un dernier alinéa ainsi rédigé :

" Les communes disposent d'un délai de dix ans à compter de l'entrée en vigueur de l'ordonnance n° 2007-1434 du 5 octobre 2007 portant extension des première, deuxième et cinquième parties du code général des collectivités territoriales aux communes de la Polynésie française, à leurs groupements et à leurs établissements publics, pour mettre en oeuvre les dispositions prévues par le présent article. "

III.-Pour son application, l'article L. 2223-19 est ainsi rédigé :

" Art. L. 2223-19. Le service des pompes funèbres peut être exercé par les communes, directement ou par voie de gestion déléguée. Les communes ou leurs délégataires ne bénéficient d'aucun droit d'exclusivité pour l'exercice de cette mission ".

IV.-Pour l'application des articles L. 2223-1 à L. 2223-19, la référence à un décret en Conseil d'Etat est remplacée par la référence à un arrêté du haut-commissaire de la République. "
