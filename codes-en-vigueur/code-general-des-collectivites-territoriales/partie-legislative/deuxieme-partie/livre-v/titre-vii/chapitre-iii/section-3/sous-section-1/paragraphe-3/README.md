# Paragraphe 3 : Pouvoirs de police portant sur des objets particuliers

- [Article L2573-19](article-l2573-19.md)
- [Article L2573-20](article-l2573-20.md)
