# Article L2573-27

Les communes doivent assurer, au plus tard le 31 décembre 2015, le service de la distribution d'eau potable et, au plus tard le 31 décembre 2020, le service de l'assainissement.
