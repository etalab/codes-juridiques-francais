# Article L2123-23

Les maires des communes ou les présidents de délégations spéciales perçoivent une indemnité de fonction fixée en appliquant au terme de référence mentionné à l'article L. 2123-20 le barème suivant :

<table>
<tbody>
<tr>
<th>
<br/>POPULATION <br/>
<br/>(habitants) <br/>
</th>
<th>
<br/>TAUX <br/>
<br/>(en % de l'indice 1015) <br/>
</th>
</tr>
<tr>
<td align="left" valign="middle">
<br/>Moins de 500 <br/>
</td>
<td align="center" valign="middle">
<br/>17 <br/>
</td>
</tr>
<tr>
<td align="left" valign="middle">
<br/>De 500 à 999 <br/>
</td>
<td align="center" valign="middle">
<br/>31 <br/>
</td>
</tr>
<tr>
<td align="left" valign="middle">
<br/>De 1 000 à 3 499 <br/>
</td>
<td align="center" valign="middle">
<br/>43 <br/>
</td>
</tr>
<tr>
<td align="left" valign="middle">
<br/>De 3 500 à 9 999 <br/>
</td>
<td align="center" valign="middle">
<br/>55 <br/>
</td>
</tr>
<tr>
<td align="left" valign="middle">
<br/>De 10 000 à 19 999 <br/>
</td>
<td align="center" valign="middle">
<br/>65 <br/>
</td>
</tr>
<tr>
<td align="left" valign="middle">
<br/>De 20 000 à 49 999 <br/>
</td>
<td align="center" valign="middle">
<br/>90 <br/>
</td>
</tr>
<tr>
<td align="left" valign="middle">
<br/>De 50 000 à 99 999 <br/>
</td>
<td align="center" valign="middle">
<br/>110 <br/>
</td>
</tr>
<tr>
<td align="left" valign="middle">
<br/>100 000 et plus <br/>
</td>
<td align="center" valign="middle">
<br/>145 <br/>
</td>
</tr>
</tbody>
</table>

Dans les communes de 1 000 habitants et plus, le conseil municipal peut, par délibération, fixer une indemnité de fonction inférieure au barème ci-dessus, à la demande du maire.
