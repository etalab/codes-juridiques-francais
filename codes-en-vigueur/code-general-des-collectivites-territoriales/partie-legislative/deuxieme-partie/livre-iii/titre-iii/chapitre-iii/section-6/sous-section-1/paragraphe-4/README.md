# Paragraphe 4 : Assiette et tarif de la taxe de séjour forfaitaire

- [Article L2333-40](article-l2333-40.md)
- [Article L2333-41](article-l2333-41.md)
- [Article L2333-42](article-l2333-42.md)
