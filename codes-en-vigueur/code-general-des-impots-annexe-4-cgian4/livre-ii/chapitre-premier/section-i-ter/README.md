# Section I ter : Retenue à la source sur les dividendes et les revenus assimilés des actions et parts sociales des sociétés françaises

- [Article 188-0 H](article-188-0-h.md)
- [Article 188 H](article-188-h.md)
