# Article 214

I. ― 1° Bénéficient de la délégation automatique de signature prévue au III de l'article 408 de l'annexe II au code général des impôts les agents de la direction générale des finances publiques exerçant les fonctions de responsable d'un service opérationnel rattaché, selon le cas, à une direction départementale des finances publiques ou à une direction spécialisée en matière de contrôle fiscal :

a) Dans la limite de 76 000 € pour les agents ayant au moins le grade d'administrateur des finances publiques et de 60 000 € pour les autres cadres, s'agissant des décisions mentionnées aux 1° et 2° du I de l'article 212 ;

b) Dans la limite de 100 000 €, s'agissant des demandes de remboursement de crédit de taxe sur la valeur ajoutée ;

c) Quel que soit le montant, s'agissant des demandes de plafonnement en fonction de la valeur ajoutée de contribution économique territoriale présentées par une entreprise dont tous les établissements sont situés dans le ressort territorial d'un service des impôts des entreprises et des actes et demandes mentionnés aux 3°, 4° et 5° du I de l'article 212.

2° Lorsque plusieurs services sont regroupés sur un même site, la délégation de signature dont disposent, en application du 1°, les responsables de service est étendue au ressort territorial de l'ensemble des services de ce site.

II. ― Bénéficient de la délégation automatique de signature prévue au III de l'article 408 de l'annexe II au code général des impôts les agents de la direction générale des douanes et droits indirects exerçant les fonctions de responsable d'un service rattaché, selon le cas, à une direction régionale des douanes et droits indirects ou à un service à compétence nationale, s'agissant des décisions mentionnées aux 1°, 2° et 4° du I de l'article 212 :

a) Dans la limite de 76 000 € pour les administrateurs des douanes, responsables de la direction des enquêtes douanières et de la direction des opérations douanières ;

b) Dans la limite de 50 000 € pour les directeurs des services douaniers et les inspecteurs principaux des douanes, responsables d'une division des douanes ;

c) Dans la limite de 25 000 € pour les directeurs des services douaniers, les inspecteurs principaux et les inspecteurs régionaux des douanes et droits indirects responsables d'un bureau de douane ou d'une unité de surveillance, d'un service régional d'enquêtes ou d'un service de viticulture.

III. ― Par acte publié dans les conditions prévues au V de l'article 408 de l'annexe II au code général des impôts, le directeur mentionné au I de ce même article peut, en tant que de besoin, en excluant certaines affaires ou en fixant des plafonds inférieurs à ceux prévus au présent article, réduire l'étendue de la délégation dont bénéficient les responsables placés sous son autorité telle qu'elle résulte du I ou du II.
