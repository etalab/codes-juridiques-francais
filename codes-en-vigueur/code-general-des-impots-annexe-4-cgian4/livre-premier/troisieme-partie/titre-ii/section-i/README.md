# Section I : Impôts directs et taxes assimilées

- [Article 165](article-165.md)
- [Article 166](article-166.md)
- [Article 167](article-167.md)
