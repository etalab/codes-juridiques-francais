# Octroi de certains agréments fiscaux

- [Article 170 quinquies](article-170-quinquies.md)
- [Article 170 sexies](article-170-sexies.md)
- [Article 170 septies F](article-170-septies-f.md)
- [Article 170 septies H](article-170-septies-h.md)
- [Article 170 octies](article-170-octies.md)
- [Article 170 decies](article-170-decies.md)
