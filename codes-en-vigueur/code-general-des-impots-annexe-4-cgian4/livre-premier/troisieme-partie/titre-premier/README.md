# Titre premier : Assiette et contrôle de l'impôt

- [Chapitre I bis : Centres de gestion agréés et associations agréées des professions libérales](chapitre-i-bis)
- [Chapitre III : Réglementation des machines à timbrer](chapitre-iii)
- [Chapitre IV : Systèmes informatiques sécurisés et matériels de validation](chapitre-iv)
- [Chapitre premier : Obligations déclaratives](chapitre-premier)
