# II : Opérations exonérées

- [1° : Exonérations des livraisons de biens à emporter dans les bagages personnels des voyageurs](1)
- [2° : Transports de voyageurs par trains internationaux](2)
