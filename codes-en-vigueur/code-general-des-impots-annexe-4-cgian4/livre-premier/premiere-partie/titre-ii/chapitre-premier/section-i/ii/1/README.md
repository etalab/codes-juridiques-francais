# 1° : Exonérations des livraisons de biens à emporter dans les bagages personnels des voyageurs

- [Article 24 bis](article-24-bis.md)
- [Article 24 ter](article-24-ter.md)
