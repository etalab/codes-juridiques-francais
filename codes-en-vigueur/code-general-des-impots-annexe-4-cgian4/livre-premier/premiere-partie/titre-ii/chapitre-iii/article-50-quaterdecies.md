# Article 50 quaterdecies

Le tarif de la redevance sanitaire de découpage s'établit comme suit (par tonne) :

<table>
<tbody>
<tr>
<td rowspan="2">
<br/>
</td>
<td>
<p align="center">EN EUROS</p>
</td>
</tr>
<tr>
<td>
<p align="center">
<br/>Par tonne </p>
</td>
</tr>
<tr>
<td>
<p>Pour les viandes d'ongulés domestiques </p>
</td>
<td>
<p align="center">
<br/>2 </p>
</td>
</tr>
<tr>
<td>
<p>Pour les viandes de volailles et de lapin d'élevage </p>
</td>
<td>
<p align="center">
<br/>1,5 </p>
</td>
</tr>
<tr>
<td>
<p>Pour les viandes de gibier d'élevage et sauvage : </p>
<p>- petit gibier à plumes, petit gibier à poils </p>
<p>- ratites (autruche, emeu, nandou) </p>
<p>- sanglier et ruminants </p>
</td>
<td>
<p align="center">
<br/>
</p>
<p align="center">
<br/>1,5 </p>
<p align="center">
<br/>3 </p>
<p align="center">
<br/>2</p>
</td>
</tr>
</tbody>
</table>
