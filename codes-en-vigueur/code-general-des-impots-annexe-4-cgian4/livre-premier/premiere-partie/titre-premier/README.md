# Titre premier : Impôts directs et taxes assimilées

- [Chapitre II : Impôt sur les sociétés](chapitre-ii)
- [Chapitre III : Dispositions communes à l'impôt sur le revenu et à l'impôt sur les sociétés](chapitre-iii)
- [Chapitre V : Taxes diverses](chapitre-v)
- [Chapitre premier : Impôt sur le revenu](chapitre-premier)
