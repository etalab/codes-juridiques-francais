# Article 23

Pour l'application du 2 de l'article 119 bis du code général des impôts, un double de la notification visée à l'article 22 devra être déposé au service des impôts du siège social de la collectivité.
