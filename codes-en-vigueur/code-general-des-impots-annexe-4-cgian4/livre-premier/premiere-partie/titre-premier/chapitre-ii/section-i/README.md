# Section I : Champ d'application

- [1° : Régime fiscal des sociétés de personnes, des sociétés en participation, des groupements d'intérêt public, des sociétés à responsabilité limitée, des exploitations agricoles à responsabilité limitée et des sociétés civiles professionnelles. Option pour le régime des sociétés de capitaux](1)
- [2° : Exonérations et régimes particuliers. Sociétés agréées pour le financement des télécommunications](2)
