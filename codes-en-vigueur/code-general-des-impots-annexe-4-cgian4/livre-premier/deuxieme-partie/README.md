# Deuxième partie : Impositions perçues au profit des collectivités locales et de divers organismes

- [Titre I ter : Impositions perçues au profit des régions et de la collectivité territoriale de Corse](titre-i-ter)
- [Titre II : Impositions perçues au profit de certains établissements publics et d'organismes divers](titre-ii)
- [Titre premier : Impositions communales](titre-premier)
