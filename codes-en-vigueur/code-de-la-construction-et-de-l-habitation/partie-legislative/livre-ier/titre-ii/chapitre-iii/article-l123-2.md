# Article L123-2

Des mesures complémentaires de sauvegarde et de sécurité et des moyens d'évacuation et de défense contre l'incendie peuvent être imposés par décrets aux propriétaires, aux constructeurs et aux exploitants de bâtiments et établissements ouverts au public. Ces mesures complémentaires doivent tenir compte des besoins particuliers des personnes handicapées ou à mobilité réduite.
