# Article L125-2-1

Les ascenseurs doivent être équipés de dispositifs de sécurité dans les conditions prévues à l'article L. 125-2-4.
