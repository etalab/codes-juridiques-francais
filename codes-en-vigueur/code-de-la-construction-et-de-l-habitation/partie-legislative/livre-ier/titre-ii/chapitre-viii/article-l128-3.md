# Article L128-3

Les conditions de la normalisation des dispositifs mentionnés aux articles L. 128-1 et L. 128-2 sont déterminées par voie réglementaire.
