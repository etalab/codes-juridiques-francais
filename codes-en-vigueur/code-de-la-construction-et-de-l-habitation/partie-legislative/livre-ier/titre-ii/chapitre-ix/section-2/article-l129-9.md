# Article L129-9

Un décret en Conseil d'Etat définit les modalités d'application de l'article L. 129-8, notamment les caractéristiques techniques du détecteur de fumée normalisé et les conditions de son installation, de son entretien et de son fonctionnement.
