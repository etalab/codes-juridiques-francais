# Article L129-7

Un décret en Conseil d'Etat détermine les conditions d'application du présent chapitre et établit la liste des équipements communs visés à l'article L. 129-1.
