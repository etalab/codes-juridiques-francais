# Section 1 : Dispositions générales pour la sécurité des occupants d'immeubles collectifs à usage d'habitation.

- [Article L129-1](article-l129-1.md)
- [Article L129-2](article-l129-2.md)
- [Article L129-3](article-l129-3.md)
- [Article L129-4](article-l129-4.md)
- [Article L129-4-1](article-l129-4-1.md)
- [Article L129-5](article-l129-5.md)
- [Article L129-6](article-l129-6.md)
- [Article L129-7](article-l129-7.md)
