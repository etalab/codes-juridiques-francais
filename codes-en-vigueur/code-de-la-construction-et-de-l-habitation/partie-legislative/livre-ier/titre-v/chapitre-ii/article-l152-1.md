# Article L152-1

Les infractions aux dispositions des articles L. 111-4, L. 111-7 à L. 111-7-4,
L. 111-8, L. 111-9, L. 111-10, L.-111-10-1, L. 111-10-4,
L. 112-17, L. 112-18, L. 112-19, L. 125-3,
L. 131-4 et L. 135-1, sont constatées par tous officiers ou agents de police judiciaire ainsi que par tous les fonctionnaires et agents de l'Etat et des collectivités publiques commissionnés à cet effet par le maire ou le ministre chargé de la construction et de l'habitation suivant l'autorité dont ils relèvent et assermentés. Les procès-verbaux dressés par ces agents font loi (1) jusqu'à preuve du contraire.

A l'issue de l'achèvement des travaux de bâtiments neufs ou de parties nouvelles de bâtiment soumis à permis de construire, les infractions aux dispositions du deuxième alinéa de l'article L. 111-9 peuvent être également constatées par les agents commissionnés à cet effet et assermentés, prévus par le présent article, au vu d'une attestation établie par un contrôleur technique mentionné à l'article L. 111-23, une personne répondant aux conditions de l'article L. 271-6 ou un architecte au sens de l'article 2 de la loi n° 77-2 du 3 janvier 1977 sur l'architecture.
