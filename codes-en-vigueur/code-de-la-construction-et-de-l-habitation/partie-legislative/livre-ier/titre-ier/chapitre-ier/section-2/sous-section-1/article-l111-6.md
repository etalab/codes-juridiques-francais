# Article L111-6

Conformément à l'article L. 361-4 du code des communes, nul ne peut, sans autorisation, élever aucune habitation, ni creuser aucun puits, à moins de 100 mètres des nouveaux cimetières transférés hors des communes et les bâtiments existants ne peuvent être ni restaurés, ni augmentés sans autorisation.
