# Article L111-6-2-1

Le vendeur professionnel d'un immeuble bâti ou d'une partie d'immeuble bâti, à usage d'habitation ou à usage professionnel et d'habitation, devant être rénové, doit justifier d'une assurance de responsabilité civile professionnelle.
