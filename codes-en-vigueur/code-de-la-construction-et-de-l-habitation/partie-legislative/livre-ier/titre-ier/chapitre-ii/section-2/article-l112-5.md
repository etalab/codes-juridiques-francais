# Article L112-5

Les dispositions relatives à la déclaration obligatoire préalable à tout sondage, ouvrage souterrain ou travail de fouille figurent à l'article L. 411-1 du code minier.
