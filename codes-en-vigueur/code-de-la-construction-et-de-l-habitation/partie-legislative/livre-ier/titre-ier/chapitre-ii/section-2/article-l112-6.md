# Article L112-6

Les dispositions relatives aux modalités de la surveillance administrative des sondages, ouvrages souterrains ou travaux de fouille mentionnés l'article L. 112-5 ainsi que les pouvoirs des autorités administratives habilitées à effectuer cette surveillance figurent à l'article L. 412-1 du code minier.
