# Article L112-13

Conformément aux lois des 8-10 juillet 1791 sur la construction et le classement des places de guerre et des 17-25 juillet 1819 sur les servitudes imposées aux propriétés pour la défense de l'Etat modifiées, les constructions de bâtiments dans la limite des zones de protection existant à l'entour des places de guerre doivent respecter les servitudes imposées par ces textes.
