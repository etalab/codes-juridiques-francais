# Chapitre II : Dispositions diverses.

- [Article L242-1](article-l242-1.md)
- [Article L242-2](article-l242-2.md)
- [Article L242-3](article-l242-3.md)
- [Article L242-4](article-l242-4.md)
