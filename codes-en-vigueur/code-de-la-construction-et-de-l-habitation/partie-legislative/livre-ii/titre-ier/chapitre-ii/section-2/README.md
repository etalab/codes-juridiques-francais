# Section 2 : Dispositions particulières aux sociétés ayant pour objet la construction d'immeubles à usage d'habitation ou à usage professionnel et d'habitation.

- [Article L212-10](article-l212-10.md)
- [Article L212-11](article-l212-11.md)
- [Article L212-12](article-l212-12.md)
- [Article L212-13](article-l212-13.md)
