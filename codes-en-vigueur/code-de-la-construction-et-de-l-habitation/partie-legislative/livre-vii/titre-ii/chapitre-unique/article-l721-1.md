# Article L721-1

Les annonces relatives à la vente d'un lot ou d'une fraction de lot d'un immeuble bâti soumis au statut de la copropriété mentionnent :

1° Le fait que le bien est soumis au statut de la copropriété ;

2° Le nombre de lots ;

3° Le montant moyen annuel de la quote-part, à la charge du vendeur, du budget prévisionnel correspondant aux dépenses courantes définies à l'article 14-1 de la loi n° 65-557 du 10 juillet 1965 fixant le statut de la copropriété des immeubles bâtis.

Les annonces précisent également si le syndicat des copropriétaires fait l'objet de procédures menées sur le fondement des articles 29-1 A et 29-1 de la loi n° 65-557 du 10 juillet 1965 précitée et de l'article L. 615-6 du présent code.
