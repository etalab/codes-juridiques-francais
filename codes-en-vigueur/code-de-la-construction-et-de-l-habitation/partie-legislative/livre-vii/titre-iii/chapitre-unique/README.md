# Chapitre unique : Diagnostic technique global des immeubles relevant du statut de la copropriété.

- [Article L731-1](article-l731-1.md)
- [Article L731-2](article-l731-2.md)
- [Article L731-3](article-l731-3.md)
- [Article L731-4](article-l731-4.md)
- [Article L731-5](article-l731-5.md)
