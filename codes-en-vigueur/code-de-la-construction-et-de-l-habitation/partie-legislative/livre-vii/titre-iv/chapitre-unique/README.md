# Chapitre unique : Opérations de requalification
des copropriétés dégradées

- [Article L741-1](article-l741-1.md)
- [Article L741-2](article-l741-2.md)
