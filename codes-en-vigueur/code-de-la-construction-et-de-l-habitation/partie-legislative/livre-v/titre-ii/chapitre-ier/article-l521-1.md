# Article L521-1

Pour l'application du présent chapitre, l'occupant est le titulaire d'un droit réel conférant l'usage, le locataire, le sous-locataire ou l'occupant de bonne foi des locaux à usage d'habitation et de locaux d'hébergement constituant son habitation principale.

Le propriétaire ou l'exploitant est tenu d'assurer le relogement ou l'hébergement des occupants ou de contribuer au coût correspondant dans les conditions prévues à l'article L. 521-3-1 dans les cas suivants :

-lorsqu'un immeuble fait l'objet d'une déclaration d'insalubrité, d'une mise en demeure ou d'une injonction prise en application des articles L. 1331-22, L. 1331-23, L. 1331-24, L. 1331-25, L. 1331-26-1 et L. 1331-28 du code de la santé publique, si elle est assortie d'une interdiction d'habiter temporaire ou définitive ou si les travaux nécessaires pour remédier à l'insalubrité rendent temporairement le logement inhabitable ;

-lorsqu'un immeuble fait l'objet d'un arrêté de péril en application de l'article L. 511-1 du présent code, si l'arrêté ordonne l'évacuation du bâtiment ou s'il est assorti d'une interdiction d'habiter ou encore si les travaux nécessaires pour mettre fin au péril rendent temporairement le logement inhabitable ;

-lorsqu'un établissement recevant du public utilisé aux fins d'hébergement fait l'objet de mesures destinées à faire cesser une situation d'insécurité en application de l'article L. 123-3.

Cette obligation est faite sans préjudice des actions dont dispose le propriétaire ou l'exploitant à l'encontre des personnes auxquelles l'état d'insalubrité ou de péril serait en tout ou partie imputable.
