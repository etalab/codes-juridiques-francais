# Article L521-3-2

I.-Lorsqu'un arrêté de péril pris en application de l'article L. 511-1 ou des prescriptions édictées en application de l'article L. 123-3 ou de l'article L. 129-3 sont accompagnés d'une interdiction temporaire ou définitive d'habiter et que le propriétaire ou l'exploitant n'a pas assuré l'hébergement ou le relogement des occupants, le maire prend les dispositions nécessaires pour les héberger ou les reloger.

II.-Lorsqu'une déclaration d'insalubrité, une mise en demeure ou une injonction prise sur le fondement des articles L. 1331-22, L. 1331-23, L. 1331-24, L. 1331-25,
L. 1331-26-1 et L. 1331-28 du code de la santé publique est assortie d'une interdiction temporaire ou définitive d'habiter et que le propriétaire ou l'exploitant n'a pas assuré l'hébergement ou le relogement des occupants, le préfet, ou le maire s'il est délégataire de tout ou partie des réservations de logements en application de l'article L. 441-1, prend les dispositions nécessaires pour héberger ou reloger les occupants, sous réserve des dispositions du III.

III.-Lorsque la déclaration d'insalubrité vise un immeuble situé dans une opération programmée d'amélioration de l'habitat prévue par l'article L. 303-1 ou dans une opération d'aménagement au sens de l'article L. 300-1 du code de l'urbanisme et que le propriétaire ou l'exploitant n'a pas assuré l'hébergement ou le relogement des occupants, la personne publique qui a pris l'initiative de l'opération prend les dispositions nécessaires à l'hébergement ou au relogement des occupants.

IV.-Lorsqu'une personne publique, un organisme d'habitations à loyer modéré, une société d'économie mixte ou un organisme à but non lucratif a assuré le relogement, le propriétaire ou l'exploitant lui verse une indemnité représentative des frais engagés pour le relogement, égale à un an du loyer prévisionnel.

V.-Si la commune assure, de façon occasionnelle ou en application d'une convention passée avec l'Etat, les obligations d'hébergement ou de relogement qui sont faites à celui-ci en cas de défaillance du propriétaire, elle est subrogée dans les droits de l'Etat pour le recouvrement de sa créance.

VI.-La créance résultant de la substitution de la collectivité publique aux propriétaires ou exploitants qui ne se conforment pas aux obligations d'hébergement et de relogement qui leur sont faites par le présent article est recouvrée soit comme en matière de contributions directes par la personne publique créancière, soit par l'émission par le maire ou le préfet d'un titre exécutoire au profit de l'organisme ayant assuré l'hébergement ou le relogement.

VII.-Si l'occupant a refusé trois offres de relogement qui lui ont été faites au titre des I, II ou III, le juge peut être saisi d'une demande tendant à la résiliation du bail ou du droit d'occupation et à l'autorisation d'expulser l'occupant.
