# Article L541-1

L'opposition introduite devant le juge administratif au titre exécutoire émis par l'Etat ou par la commune en paiement d'une créance résultant de l'exécution d'office de mesures prises en application des articles L. 1311-4, L. 1331-24, L. 1331-26-1, L. 1331-28, L. 1331-29 et L. 1334-2 du code de la santé publique, des articles L. 123-3, L. 129-2, L. 129-3, L. 511-2 et L. 511-3 du présent code, ou du relogement ou de l'hébergement des occupants effectué en application de l'article L. 521-3-2, n'est pas suspensive.

Dans le cas d'une créance de la commune, les dispositions du troisième alinéa de l'article L. 1617-5 du code général des collectivités territoriales ne sont pas applicables.
