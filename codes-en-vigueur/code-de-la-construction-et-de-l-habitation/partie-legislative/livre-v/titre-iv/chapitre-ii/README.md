# Chapitre II : Meubles des occupants évacués dont le logement a fait l'objet d'une interdiction définitive d'habiter.

- [Article L542-1](article-l542-1.md)
- [Article L542-2](article-l542-2.md)
- [Article L542-3](article-l542-3.md)
- [Article L542-4](article-l542-4.md)
