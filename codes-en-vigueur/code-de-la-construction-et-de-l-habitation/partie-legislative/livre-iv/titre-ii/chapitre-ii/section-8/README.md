# Section 8 : Dispositions provisoirement applicables par suite de la suppression des sociétés coopératives de location coopérative.

- [Article L422-16](article-l422-16.md)
- [Article L422-17](article-l422-17.md)
- [Article L422-18](article-l422-18.md)
- [Article L422-19](article-l422-19.md)
