# Article L411-1

Les dispositions du présent livre ont pour objet de fixer les règles relatives à la construction, l'acquisition, l'aménagement, l'assainissement, la réparation, la gestion d'habitations collectives ou individuelles, urbaines ou rurales, répondant aux caractéristiques techniques et de prix de revient déterminées par décision administrative et destinées aux personnes et aux familles de ressources modestes.

A ces habitations peuvent être adjoints, dans des conditions fixées par décision administrative, des dépendances, des annexes et des jardins privatifs ou collectifs, accolés ou non aux immeubles.

En outre, les ensembles d'habitations mentionnés aux premiers alinéas peuvent comprendre accessoirement des locaux à usage commun et toutes constructions nécessaires à la vie économique et sociale de ces ensembles.

Les organismes d'habitations à loyer modéré peuvent librement louer les aires de stationnement vacantes dont ils disposent par application de l'article L. 442-6-4.

La location est consentie à titre précaire et révocable à tout moment par le bailleur. Un locataire de ce bailleur ne peut se voir opposer un refus de location d'une aire de stationnement au motif que cette aire est louée librement à une personne ne louant pas un logement dans le parc de ce bailleur.
