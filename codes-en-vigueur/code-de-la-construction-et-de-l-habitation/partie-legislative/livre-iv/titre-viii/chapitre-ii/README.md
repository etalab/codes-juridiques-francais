# Chapitre II : Dispositions relatives à la mobilité des locataires.

- [Article L482-1](article-l482-1.md)
- [Article L482-2](article-l482-2.md)
- [Article L482-3](article-l482-3.md)
- [Article L482-4](article-l482-4.md)
