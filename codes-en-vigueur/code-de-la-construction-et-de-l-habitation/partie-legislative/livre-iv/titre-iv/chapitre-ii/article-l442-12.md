# Article L442-12

Sont considérées comme personnes vivant au foyer au titre des articles L. 441-1, L. 441-4 et L. 445-4 :

― le ou les titulaires du bail ;

― les personnes figurant sur les avis d'imposition du ou des titulaires du bail ;

― le concubin notoire du titulaire du bail ;

― le partenaire lié par un pacte civil de solidarité au titulaire du bail ;

― et les personnes réputées à charge au sens des articles 194, 196, 196 A bis et 196 B du code général des impôts.
