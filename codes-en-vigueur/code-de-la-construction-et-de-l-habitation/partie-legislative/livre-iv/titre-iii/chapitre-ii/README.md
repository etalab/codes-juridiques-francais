# Chapitre II : Prêts aux autres organismes et collectivités.

- [Article L432-1](article-l432-1.md)
- [Article L432-2](article-l432-2.md)
- [Article L432-3](article-l432-3.md)
- [Article L432-4](article-l432-4.md)
- [Article L432-6](article-l432-6.md)
