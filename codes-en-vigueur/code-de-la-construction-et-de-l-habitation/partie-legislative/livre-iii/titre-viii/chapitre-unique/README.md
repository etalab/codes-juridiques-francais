# Chapitre unique : Sociétés de tiers-financement

- [Article L381-1](article-l381-1.md)
- [Article L381-2](article-l381-2.md)
