# Article L342-19

I. ― Le personnel de l'Agence nationale de contrôle du logement social comprend :

1° Des fonctionnaires de l'Etat ;

2° Des agents non titulaires de droit public ;

3° Des salariés régis par le code du travail.

II. ― Les personnels chargés de réaliser les contrôles nécessaires à l'accomplissement des missions de l'agence font l'objet d'une habilitation par le ministre compétent.

Les personnels chargés des contrôles sont astreints au secret professionnel, dans les conditions prévues aux articles 226-13 et 226-14 du code pénal. Ce secret ne peut leur être opposé, sauf par les auxiliaires de justice.

III. ― Sont institués auprès du directeur général :

1° Un comité technique compétent pour les personnels mentionnés aux 1° et 2° du I, conformément à l'article 15 de la loi n° 84-16 du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat ;

2° Un comité d'entreprise compétent pour les personnels mentionnés au 3° du I, conformément au titre II du livre III de la deuxième partie du code du travail.

Le directeur général réunit conjointement le comité technique et le comité d'entreprise, dans le respect de leurs attributions respectives, pour connaître des sujets communs à l'ensemble du personnel.

IV. ― Il est institué auprès du directeur général de l'Agence nationale de contrôle du logement social un comité d'hygiène, de sécurité et des conditions de travail compétent pour l'ensemble du personnel de l'établissement. Ce comité exerce les compétences des comités prévus à l'article 16 de la loi n° 84-16 du 11 janvier 1984 précitée, ainsi que celles prévues au chapitre II du titre Ier du livre VI de la quatrième partie du code du travail, sous réserve des adaptations fixées par décret en Conseil d'Etat. Sa composition et son fonctionnement sont fixés par décret en Conseil d'Etat.
