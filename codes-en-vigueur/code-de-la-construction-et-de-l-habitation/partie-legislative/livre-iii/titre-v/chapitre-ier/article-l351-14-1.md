# Article L351-14-1

La décision déclarant la recevabilité de la demande mentionnée au quatrième alinéa de l'article L. 331-3-1 du code de la consommation emporte rétablissement des droits à l'aide personnalisée au logement du locataire, si son versement a été suspendu.

Le déblocage des aides personnalisées au logement s'effectue dans les conditions prévues à l'article L. 351-9 du présent code.
