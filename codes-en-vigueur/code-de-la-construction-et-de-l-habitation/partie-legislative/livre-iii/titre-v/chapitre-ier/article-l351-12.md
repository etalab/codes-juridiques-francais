# Article L351-12

Les informations nécessaires à l'appréciation des conditions d'ouverture, au maintien des droits et au calcul de l'aide personnalisée au logement, notamment les ressources, peuvent être obtenues par les organismes chargés du paiement de l'aide selon les modalités de l'article L. 114-14 du code de la sécurité sociale.

Si l'allocataire ne règle pas la part de la dépense de logement restant à sa charge, le bailleur ou le prêteur auprès duquel l'aide est versée signale la situation de l'allocataire défaillant à l'organisme payeur, dans des conditions définies par décret.

Le bailleur auprès duquel l'aide est versée signale le déménagement de l'allocataire et la résiliation de son bail, dans un délai déterminé par décret.

Si l'allocataire procède à un remboursement anticipé de son prêt, le prêteur auprès duquel l'aide est versée signale ce remboursement anticipé à l'organisme payeur, dans un délai fixé par décret.

Sans préjudice des sanctions pénales encourues, la fraude, la fausse déclaration, l'inexactitude ou le caractère incomplet des informations recueillies en application des alinéas précédents du présent article exposent le bénéficiaire, le demandeur, le bailleur ou le prêteur aux sanctions et pénalités prévues à l'article L. 114-17 du code de la sécurité sociale.

Lorsque les informations ne peuvent pas être obtenues dans les conditions prévues au premier alinéa, les bénéficiaires, les demandeurs ou les bailleurs les communiquent par déclaration auxdits organismes.

Sous réserve des dispositions de l'article L. 353-11, le contrôle des déclarations des demandeurs ou des bénéficiaires de l'aide personnalisée au logement est assuré par le personnel assermenté des organismes et des services chargés du paiement de l'aide. Il peut également contrôler les déclarations des bailleurs, afin de vérifier notamment l'existence ou l'occupation du logement pour lequel l'aide personnalisée au logement est perçue. Les administrations publiques, notamment par application de l'article L. 152 du livre des procédures fiscales, sont tenues de communiquer à ce personnel toutes les pièces nécessaires à l'exercice de ce contrôle.
