# Article L351-9

L'aide personnalisée au logement est versée :

En cas de location, au bailleur du logement, sous réserve des dispositions des articles L. 351-11 et L. 353-9 ;

En cas de mandat de gérance de logements, l'aide personnalisée peut être versée au mandataire ;

Dans les autres cas, à l'établissement habilité à cette fin.

Dans des cas qui seront précisés par décret, elle peut être versée au locataire ou au propriétaire du logement.

Lorsque l'aide est versée au bailleur ou à l'établissement habilité à cette fin, elle est déduite, par les soins de qui reçoit le versement, du montant du loyer et des dépenses accessoires de logement ou de celui des charges de remboursement. Cette déduction doit être portée à la connaissance du bénéficiaire, locataire ou propriétaire du logement.

Sous réserve des dispositions du premier alinéa ci-dessus, l'aide personnalisée au logement est insaisissable et incessible sauf au profit de l'établissement habilité ou du bailleur ou, le cas échéant, de l'organisme payeur dans le cas prévu à l'article L. 351-11, alinéa 3, in fine.
