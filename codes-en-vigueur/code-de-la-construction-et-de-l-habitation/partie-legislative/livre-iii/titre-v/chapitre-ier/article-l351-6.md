# Article L351-6

Le fonds national d'aide au logement est chargé de financer l'aide personnalisée au logement, la prime de déménagement prévue à l'article L. 351-5 et les dépenses de gestion qui s'y rapportent ainsi que les dépenses du conseil national de l'habitat.

Il finance également l'allocation de logement relevant du titre III du livre VIII du code de la sécurité sociale ainsi que les dépenses de gestion qui s'y rapportent.

Le fonds est administré par un conseil de gestion dont la composition, les modes de désignation des membres et les modalités de fonctionnement sont fixés par décret.

Sa gestion est assurée par la Caisse des dépôts et consignations.
