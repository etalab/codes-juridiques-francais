# Chapitre X : Prêt ne portant pas intérêt consenti pour financer la primo-accession à la propriété

- [Section 1 : Conditions du prêt](section-1)
- [Section 2 : Maintien du prêt](section-2)
- [Section 3 : Montant du prêt](section-3)
- [Section 4 : Durée du prêt](section-4)
- [Section 5 : Conventions avec les établissements de crédit ou les sociétés de financement et contrôle](section-5)
- [Article L31-10-1](article-l31-10-1.md)
