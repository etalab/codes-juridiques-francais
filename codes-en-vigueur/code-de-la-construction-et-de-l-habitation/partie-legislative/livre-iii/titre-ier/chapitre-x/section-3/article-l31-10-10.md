# Article L31-10-10

Le coût total de l'opération comprend le coût des travaux éventuellement prévus par l'emprunteur lors de l'acquisition, à l'exception des montants financés au moyen de l'avance mentionnée à l'article 244 quater U du code général des impôts.

Le plafond dans la limite duquel est retenu le coût total d'opération correspond au produit du montant maximal d'opération pour une personne seule par un coefficient familial, arrondi au millier d'euros le plus proche.

Le montant maximal d'opération pour une personne seule est fixé par décret, en fonction de la localisation du logement. Il ne peut être supérieur à 156 000 € ni inférieur à 79 000 €.

Le coefficient familial mentionné au deuxième alinéa est déterminé en fonction du nombre de personnes destinées à occuper à titre de résidence principale le logement, selon le tableau ci-après :

<div align="center">

<table>
<tbody>
<tr>
<td align="center">
<br/>Nombre de personnes <br/>
</td>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>5 et plus <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Coefficient familial <br/>
</td>
<td align="center">
<br/>1,0 <br/>
</td>
<td align="center">
<br/>1,4 <br/>
</td>
<td align="center">
<br/>1,7 <br/>
</td>
<td align="center">
<br/>2,0 <br/>
</td>
<td align="center">
<br/>2,3 <br/>
</td>
</tr>
</tbody>
</table>

</div>
