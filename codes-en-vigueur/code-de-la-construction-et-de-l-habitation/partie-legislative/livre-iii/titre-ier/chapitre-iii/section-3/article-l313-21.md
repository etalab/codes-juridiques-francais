# Article L313-21

Le conseil de surveillance ou le conseil d'administration de l'union arrête les directives mentionnées à l'article L. 313-19 et les avis de l'union prévus par la loi ou la réglementation. Il autorise le recours à l'emprunt.

Lorsque l'union est administrée par un directoire placé sous le contrôle d'un conseil de surveillance, ce dernier détermine les orientations de l'activité de l'union et veille à leur mise en œuvre. Il se saisit de toute question intéressant la bonne marche de l'union et règle par ses délibérations les affaires qui la concernent. Le directoire est chargé de la mise en œuvre des délibérations prises par le conseil de surveillance. Il rend compte de son activité à chaque réunion du conseil de surveillance.

Lorsque l'union est administrée par un conseil d'administration, les fonctions de président sont incompatibles avec les fonctions de directeur général.

Le conseil de surveillance ou le conseil d'administration comporte cinq représentants permanents au plus désignés par les organisations d'employeurs associées et cinq représentants permanents au plus désignés par les organisations de salariés associées. Un suppléant de chacun de ces représentants est désigné dans les mêmes conditions. Le conseil de surveillance ou le conseil d'administration est présidé par l'un des représentants désignés par les organisations d'employeurs associées. Les représentants et leurs suppléants ne peuvent être propriétaires d'actions de l'union. Le conseil se réunit au moins trois fois dans l'année.
