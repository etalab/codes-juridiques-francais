# Article L313-25

Pour ses frais de fonctionnement afférents aux attributions prévues l'article L. 313-19, l'union dispose d'un prélèvement opéré chaque année sur les sommes collectées par les associés collecteurs. Elle en détermine le montant annuel.

Une fraction des sommes prélevées peut être reversée par l'union aux organisations interprofessionnelles d'employeurs et de salariés associées, en défraiement des charges que représente leur participation à l'ensemble des travaux et activités de l'union et de ses associés collecteurs. Une part de ce défraiement peut être versée directement aux représentants de ces organisations en défraiement des frais exposés dans le cadre de leurs travaux et activités exercés pour l'union.

L'assemblée générale de l'union détermine annuellement le montant total du défraiement, dans la limite d'un plafond fixé par arrêté. Ce montant  est réparti par le conseil de surveillance entre les organisations interprofessionnelles d'employeurs et de salariés associées sur la base d'un dossier établi par chaque organisation décrivant la nature des dépenses envisagées et rendant compte de l'emploi des sommes perçues au titre de l'année précédente. Ce défraiement est exclusif de tous autres défraiements, indemnisations ou rémunérations par l'union de ces organisations et de leurs représentants permanents.
