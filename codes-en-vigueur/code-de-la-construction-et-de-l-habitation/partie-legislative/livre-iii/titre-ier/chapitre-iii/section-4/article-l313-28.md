# Article L313-28

Les statuts des sociétés immobilières mentionnées à l'article L. 313-27 doivent contenir des clauses conformes à des clauses types fixées par décret. Ce décret peut en outre apporter des restrictions aux règles d'usage et d'aliénation du patrimoine de ces sociétés.

Ces sociétés, lorsqu'elles ont été constituées antérieurement à la publication de la loi n° 93-122 du 29 janvier 1993 relative à la prévention de la corruption et à la transparence de la vie économique et des procédures publiques, doivent mettre leurs statuts en conformité avec les clauses types mentionnées à l'alinéa précédent, dans un délai de douze mois après la publication du décret établissant ces clauses types.

Si l'assemblée des actionnaires ou des associés n'est pas en mesure de statuer régulièrement sur cette mise en conformité dans le délai imparti, le projet de mise en conformité des statuts est soumis à l'homologation du président du tribunal de commerce statuant sur requête des représentants légaux de la société.

Il est interdit aux présidents, administrateurs ou gérants de ces sociétés qui, volontairement, n'auront pas mis ou fait mettre les statuts en conformité avec les clauses types dans le délai imparti, pendant un délai de cinq années, de diriger, administrer ou gérer à un titre quelconque une des sociétés immobilières concernées par le présent article, et d'engager la signature d'une de ces sociétés.
