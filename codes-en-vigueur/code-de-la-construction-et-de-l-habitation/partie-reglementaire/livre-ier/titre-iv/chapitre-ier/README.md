# Chapitre Ier : Aide à la productivité - Coordination des programmes d'équipement.

- [Section 1 : Aide à la productivité.](section-1)
- [Section 2 : Coordination des programmes d'équipement.](section-2)
