# Article R142-10

Il est institué auprès du Centre scientifique et technique du bâtiment un comité consultatif appelé à donner son avis sur toutes questions d'ordre scientifique ou technique qui pourraient lui être soumises par le ministre ou par le président ou le directeur général du centre, en vue notamment d'assurer une cohérence entre les diverses études et recherches menées, leurs applications et les investissements.

Ce comité est obligatoirement consulté sur le programme général d'études et de recherches mentionné au 2° de l'article R. 142-9.

La composition du comité est fixée par un arrêté du ministre chargé de la construction ; il peut comprendre des fonctionnaires et des personnes privées choisies en raison de leur compétence, notamment en matière de recherche.

Il peut entendre tout expert qu'il désire consulter avant d'émettre un avis.
