# Article R111-45

Le maître d'ouvrage d'une opération de démolition de bâtiment réalise un diagnostic portant sur les déchets issus de ces travaux dans les conditions suivantes :

a) Préalablement au dépôt de la demande de permis de démolir si l'opération y est soumise ;

b) Préalablement à l'acceptation des devis ou à la passation des marchés relatifs aux travaux de démolition dans les autres cas.
