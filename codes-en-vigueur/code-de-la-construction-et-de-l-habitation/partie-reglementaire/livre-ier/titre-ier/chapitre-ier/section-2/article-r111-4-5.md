# Article R111-4-5

Un arrêté du ministre chargé de la construction détermine les modalités d'application des articles R. 111-4-2 à R. 111-4-4.
