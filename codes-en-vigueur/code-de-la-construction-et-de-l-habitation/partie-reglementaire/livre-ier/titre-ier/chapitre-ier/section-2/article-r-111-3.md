# Article R*111-3

Tout logement doit :

a) Etre pourvu d'une installation d'alimentation en eau potable et d'une installation d'évacuation des eaux usées ne permettant aucun refoulement des odeurs ;

b) Comporter au moins une pièce spéciale pour la toilette, avec une douche ou une baignoire et un lavabo, la douche ou la baignoire pouvant toutefois être commune à cinq logements au maximum, s'il s'agit de logements d'une personne groupés dans un même bâtiment ;

c) Etre pourvu d'un cabinet d'aisances intérieur au logement, le cabinet d'aisances pouvant toutefois être commun à cinq logements au maximum s'il s'agit de logements d'une personne et de moins de 20 mètres carrés de surface habitable et à condition qu'il soit situé au même étage que ces logements, le cabinet d'aisances peut ne former qu'une seule pièce avec la pièce spéciale pour la toilette mentionnée au b ;

d) Comporter un évier muni d'un écoulement d'eau et un emplacement aménagé pour recevoir des appareils de cuisson.

Les règles de construction et d'installation des fosses septiques et appareils analogues sont fixées par un arrêté conjoint du ministre chargé de la santé et du ministre chargé de la construction et de l'habitation.

Les immeubles collectifs comportent un local clos et ventilé pour le dépôt des ordures ménagères avant leur enlèvement.
