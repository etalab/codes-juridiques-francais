# Article R*123-34

La commission de sécurité compétente à l'échelon du département est la commission consultative départementale de la protection civile instituée par le décret n° 65-1048 du 2 décembre 1965, modifié par le décret n° 70-818 du 10 septembre 1970.
