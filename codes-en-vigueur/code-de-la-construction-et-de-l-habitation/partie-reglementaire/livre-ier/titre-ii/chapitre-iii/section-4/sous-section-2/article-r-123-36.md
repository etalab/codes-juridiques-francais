# Article R*123-36

La commission consultative départementale de la protection civile est seule compétente pour donner un avis se rapportant aux établissements classés dans la 1re catégorie prévue à l'article R. 123-19.

Elle examine toutes questions et demandes d'avis présentées par les maires ou par les commissions d'arrondissement ou les commissions communales ou intercommunales.

En cas d'avis défavorable donné par ces commissions, les exploitants peuvent demander que la question soit soumise à la commission départementale.

La commission départementale propose au représentant de l'Etat dans le département le renvoi au ministre de l'intérieur des dossiers pour lesquels il apparaît opportun de demander l'avis de la commission centrale de sécurité.
