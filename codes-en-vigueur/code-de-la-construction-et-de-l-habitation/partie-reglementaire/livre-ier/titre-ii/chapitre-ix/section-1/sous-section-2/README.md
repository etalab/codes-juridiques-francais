# Sous-section 2 : Dispositions particulières aux bâtiments en copropriété

- [Article R*129-5](article-r-129-5.md)
- [Article R*129-6](article-r-129-6.md)
- [Article R*129-7](article-r-129-7.md)
- [Article R*129-8](article-r-129-8.md)
- [Article R*129-9](article-r-129-9.md)
