# Article R*129-5

Lorsque les équipements communs d'un immeuble collectif à usage principal d'habitation susceptibles de justifier le recours à la procédure prévue à l'article L. 129-1 sont situés dans un immeuble en copropriété, l'information prévue par l'article R. 129-2 est faite au syndicat des copropriétaires pris en la personne du syndic, qui la transmet aux copropriétaires dans un délai qui ne peut excéder vingt et un jours.

Le syndic dispose alors pour présenter des observations d'un délai qui ne peut être inférieur à deux mois à compter de la date à laquelle il a reçu l'information faite par le maire.
