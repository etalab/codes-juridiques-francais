# Section 2 : Détecteurs de fumée normalisés

- [Article R129-12](article-r129-12.md)
- [Article R129-13](article-r129-13.md)
- [Article R129-14](article-r129-14.md)
- [Article R129-15](article-r129-15.md)
