# Article R122-25

La décision du maire est notifiée directement au propriétaire ; une ampliation en est transmise au représentant de l'Etat dans le département.
