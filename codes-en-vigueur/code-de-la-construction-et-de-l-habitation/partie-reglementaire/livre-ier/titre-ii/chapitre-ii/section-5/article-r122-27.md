# Article R122-27

Un fichier départemental de contrôle des immeubles de grande hauteur est établi et tenu à jour par le représentant de l'Etat dans le département.
