# Article R125-1-3

A la place de tout ou partie des dispositifs de sécurité mentionnés à l'article R. 125-1-2, le propriétaire d'un ascenseur peut mettre en oeuvre des mesures équivalentes si celles-ci ont préalablement obtenu l'accord d'une personne remplissant les conditions prévues à l'article R. 125-2-5. Cet accord, formulé par écrit et assorti d'une analyse de risques établissant que l'ascenseur satisfait aux exigences de sécurité mentionnées à l'article R. 125-1-1, est remis au propriétaire.
