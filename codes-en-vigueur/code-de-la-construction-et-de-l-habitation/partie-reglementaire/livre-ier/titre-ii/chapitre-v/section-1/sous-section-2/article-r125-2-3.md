# Article R125-2-3

Lorsque le propriétaire ne recourt pas à un prestataire de services mais décide d'assurer par ses propres moyens l'entretien de l'ascenseur, il est tenu au respect des prescriptions de l'article R. 125-2. Il tient à jour le carnet d'entretien et établit un rapport annuel d'activité dans les conditions fixées au III de l'article R. 125-2-1.

Le personnel qu'il emploie pour l'exercice de cette mission doit avoir reçu une formation appropriée dans les conditions prévues à l'article 9 du décret n° 95-826 du 30 juin 1995 fixant les prescriptions particulières de sécurité applicables aux travaux effectués sur les ascenseurs, ascenseurs de charges, escaliers mécaniques, trottoirs roulants et installations de parcage automatique de véhicules.
