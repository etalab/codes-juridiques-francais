# Article R125-2-4

Le propriétaire d'un ascenseur est tenu de faire réaliser tous les cinq ans un contrôle technique de son installation.

Le contrôle technique a pour objet :

a) De vérifier que les appareils auxquels s'applique le décret n° 2000-810 du 24 août 2000 relatif à la mise sur le marché des ascenseurs sont équipés des dispositifs prévus par ce décret et que ceux-ci sont en bon état ;

b) De vérifier que les appareils qui n'entrent pas dans le champ d'application du décret du 24 août 2000 susmentionné, sont équipés des dispositifs de sécurité prévus par les articles R. 125-1-1 et R. 125-1-2 et que ces dispositifs sont en bon état, ou que les mesures équivalentes ou prévues à l'article R. 125-1-3 sont effectivement mises en oeuvre ;

c) De repérer tout défaut présentant un danger pour la sécurité des personnes ou portant atteinte au bon fonctionnement de l'appareil.
