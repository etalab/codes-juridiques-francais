# Sous-section 3 : Droits des occupants d'immeubles équipés d'ascenseurs

- [Article R125-2-7](article-r125-2-7.md)
- [Article R125-2-8](article-r125-2-8.md)
