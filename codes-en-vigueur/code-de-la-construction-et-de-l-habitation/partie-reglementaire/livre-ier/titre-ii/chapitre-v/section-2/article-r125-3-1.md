# Article R125-3-1

Toute installation nouvelle de porte automatique de garage dans les bâtiments et groupes de bâtiments d'habitation doit satisfaire aux prescriptions suivantes :

- la porte doit rester solidaire de son support ;

- un système de sécurité doit interrompre immédiatement tout mouvement d'ouverture ou de fermeture de la porte lorsque ce mouvement peut causer un dommage à une personne ;

- lorsque ce système de sécurité est défectueux, le fonctionnement automatique de la porte doit être interrompu ;

- le système de commande de la porte doit être volontaire et personnalisé à moins que la conception de la porte ne permette que son utilisation, même anormale, ne crée aucun danger pour les personnes ;

- l'aire de débattement de la porte doit être correctement éclairée et faire l'objet d'un marquage au sol ;

- tout mouvement de la porte doit être signalé, tant à l'extérieur qu'à l'intérieur, par un feu orange clignotant visible de l'aire de débattement. La signalisation doit précéder le mouvement de la porte ;

- la porte doit pouvoir être manoeuvrée de l'extérieur comme de l'intérieur pour permettre de dégager une personne accidentée. La manoeuvre extérieure est facultative si la pression exercée par la porte est telle qu'elle ne fait pas obstacle au dégagement de la personne accidentée.

Un arrêté du ministre chargé de la construction précise les conditions d'application des sixième et septième alinéas du présent article.
