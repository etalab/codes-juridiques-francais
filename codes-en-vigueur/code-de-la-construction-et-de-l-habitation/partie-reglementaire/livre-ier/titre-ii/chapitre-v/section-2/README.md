# Section 2 : Sécurité des portes automatiques de garage.

- [Article R125-3-1](article-r125-3-1.md)
- [Article R125-3-2](article-r125-3-2.md)
- [Article R125-4](article-r125-4.md)
- [Article R125-5](article-r125-5.md)
