# Chapitre VII : Gardiennage ou surveillance de certains immeubles d'habitation.

- [Article R*127-1](article-r-127-1.md)
- [Article R* 127-8](article-r-127-8.md)
