# Chapitre II : Dispositions particulières aux départements de la Guadeloupe, de la Guyane, de la Martinique et de La Réunion

- [Section 1 : Caractéristiques thermiques et performance énergétique des bâtiments d'habitation](section-1)
- [Section 2 : Caractéristiques acoustiques](section-2)
- [Section 3 : Aération des logements](section-3)
