# Article R*152-11

Est puni de l'amende prévue pour les contraventions de la 3e classe le fait pour une personne, propriétaire d'un local existant, de ne pas mettre en place les dispositifs prévus par les articles R. 131-31 et R. 131-33.
