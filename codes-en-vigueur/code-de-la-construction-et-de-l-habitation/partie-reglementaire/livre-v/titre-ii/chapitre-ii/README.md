# Chapitre II : Financement des opérations de résorption de l'habitat insalubre irrémédiable ou dangereux.

- [Article D*522-1](article-d-522-1.md)
- [Article D*522-2](article-d-522-2.md)
- [Article D*522-3](article-d-522-3.md)
- [Article D*522-5](article-d-522-5.md)
- [Article R522-4](article-r522-4.md)
- [Article R522-6](article-r522-6.md)
- [Article R522-7](article-r522-7.md)
