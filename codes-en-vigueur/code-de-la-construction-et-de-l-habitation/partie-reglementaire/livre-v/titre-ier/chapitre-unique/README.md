# Chapitre unique.

- [Section 1 : Dispositions générales.](section-1)
- [Section 2 : Dispositions particulières aux bâtiments en copropriété.](section-2)
- [Section 3 : Autres dispositions.](section-3)
