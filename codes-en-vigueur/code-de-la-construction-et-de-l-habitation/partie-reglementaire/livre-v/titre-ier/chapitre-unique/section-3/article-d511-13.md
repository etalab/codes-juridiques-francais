# Article D511-13

Lorsque les désordres affectant des monuments funéraires sont susceptibles de justifier le recours à la procédure prévue à l'article L. 511-4-1, le maire en informe, en joignant tous éléments utiles en sa possession, les personnes titulaires de la concession ou leurs ayants droit et les invite à présenter leurs observations dans un délai qu'il fixe et qui ne peut être inférieur à un mois.
