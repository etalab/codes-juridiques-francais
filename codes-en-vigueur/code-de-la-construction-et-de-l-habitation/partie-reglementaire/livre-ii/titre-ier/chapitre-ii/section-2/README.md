# Section 2 : Dispositions propres aux sociétés ayant pour objet la construction d'immeubles à usage d'habitation ou à usage professionnel et d'habitation.

- [Article R*212-12](article-r-212-12.md)
- [Article R*212-13](article-r-212-13.md)
- [Article R*212-14](article-r-212-14.md)
- [Article R*212-15](article-r-212-15.md)
- [Article R*212-16](article-r-212-16.md)
