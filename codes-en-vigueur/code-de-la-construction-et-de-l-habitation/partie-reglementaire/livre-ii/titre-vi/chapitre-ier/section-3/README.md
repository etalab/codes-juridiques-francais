# Section 3 : Dispositions particulières à la conclusion du contrat de ventes d'immeubles à construire pour l'usage d'habitation ou pour l'usage professionnel et d'habitation.

- [Article R*261-11](article-r-261-11.md)
- [Article R*261-12](article-r-261-12.md)
- [Article R*261-13](article-r-261-13.md)
- [Article R*261-14](article-r-261-14.md)
- [Article R*261-15](article-r-261-15.md)
- [Article R*261-16](article-r-261-16.md)
