# Article R*261-18

La garantie d'achèvement résulte de l'existence de conditions propres à l'opération lorsque cette dernière répond à l'une ou l'autre des situations suivantes :

1° Si l'immeuble est mis hors d'eau et n'est grevé d'aucun privilège, hypothèque ou gage immobilier ;

2° Si les trois conditions suivantes sont réunies :

a) Les fondations sont achevées ;

b) Le financement de l'immeuble ou des immeubles compris dans un même programme est assuré à hauteur de 75 % du prix des ventes prévues par :

― les fonds appartenant au vendeur déjà investis dans l'opération ou disponibles pour la financer, à l'exclusion des dations en paiement et des fonds issus d'emprunts ;

― le montant du prix des ventes déjà conclues et pour lesquelles l'acquéreur a fourni une attestation bancaire précisant qu'il dispose des fonds ou valeurs nécessaires à l'achat ou d'un crédit confirmé ;

― les crédits confirmés des banques ou établissements financiers habilités à faire des opérations de crédit immobilier, déduction faite des prêts transférables aux acquéreurs des logements déjà vendus. Ne sont considérés comme crédits confirmés au sens du présent article que les crédits certains, irrévocables et maintenus jusqu'à l'achèvement de l'opération.

Toutefois, le taux de 75 % est réduit à 60 % lorsque le financement est assuré à concurrence de 30 % du prix des ventes par les fonds appartenant au vendeur.

Pour l'appréciation du montant du financement ainsi exigé, il est tenu compte du montant du prix des ventes conclues sous la seule condition suspensive de la justification de ce financement dans les six mois suivant l'achèvement des fondations ;

c) Le vendeur a ouvert un compte unique, propre à l'opération, auprès d'un établissement de crédit et s'engage à y centraliser les fonds assurant le financement du ou des immeubles.
