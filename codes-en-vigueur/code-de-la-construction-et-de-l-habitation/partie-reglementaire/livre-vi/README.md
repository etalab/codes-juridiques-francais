# Livre VI : Dispositions permettant de faire face à des difficultés particulières de logement.

- [Titre Ier : Dispositions générales.](titre-ier)
- [Titre II : Dispositions tendant à faciliter et à orienter la répartition des logements existants.](titre-ii)
- [Titre III : Dispositions tendant à maintenir ou à augmenter le nombre des logements.](titre-iii)
- [Titre V : Sanctions et dispositions diverses.](titre-v)
- [Titre VI : Dispositions relatives à l'outre-mer.](titre-vi)
