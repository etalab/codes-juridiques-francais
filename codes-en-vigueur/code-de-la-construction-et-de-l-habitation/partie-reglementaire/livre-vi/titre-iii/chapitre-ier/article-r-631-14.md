# Article R*631-14

En vue de la délivrance de l'agrément de l'exploitant d'une résidence hôtelière à vocation sociale, le propriétaire de l'immeuble ou du terrain, ou le maître d'ouvrage de l'opération transmet au représentant de l'Etat dans le département d'implantation de la résidence un dossier dont la composition est arrêtée par le ministre chargé du logement.
