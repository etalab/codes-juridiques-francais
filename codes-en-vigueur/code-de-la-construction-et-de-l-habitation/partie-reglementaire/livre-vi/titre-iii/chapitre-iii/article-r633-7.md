# Article R633-7

I.-Dans les logements-foyers logeant de quinze à trente ménages titulaires d'un contrat, le règlement intérieur définit les modalités de désignation des représentants de ces ménages.

II.-Dans les logements-foyers logeant plus de trente ménages titulaires d'un contrat, les représentants de ces ménages sont élus par vote à bulletin secret pour une durée d'un an au moins et de trois ans au plus dans les conditions fixées par le règlement intérieur.

Sont éligibles les personnes titulaires d'un contrat en cours de validité avec l'établissement. Chaque ménage titulaire d'un contrat dispose d'une voix.

Sont élus les candidats ayant obtenu le plus grand nombre de voix. A égalité de voix, il est procédé par tirage au sort entre les intéressés.

Le représentant des ménages au conseil de concertation qui n'est plus titulaire d'un contrat mentionné à l'article L. 633-2 est remplacé dans les conditions fixées par le règlement intérieur de l'établissement.

Des suppléants peuvent être élus dans les mêmes conditions.
