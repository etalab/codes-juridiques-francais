# Chapitre Ier : Service municipal du logement.

- [Article R621-1](article-r621-1.md)
- [Article R621-2](article-r621-2.md)
- [Article R621-3](article-r621-3.md)
