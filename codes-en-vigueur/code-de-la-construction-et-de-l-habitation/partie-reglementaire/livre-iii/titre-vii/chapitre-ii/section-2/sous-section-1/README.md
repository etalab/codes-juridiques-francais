# Sous-section 1 : Dispositions relatives à l'assiette et aux taux des subventions

- [Article R372-9](article-r372-9.md)
- [Article R372-10](article-r372-10.md)
- [Article R372-11](article-r372-11.md)
