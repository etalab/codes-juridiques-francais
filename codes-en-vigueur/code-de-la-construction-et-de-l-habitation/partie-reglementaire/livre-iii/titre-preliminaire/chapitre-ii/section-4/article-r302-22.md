# Article R302-22

Le préfet de région est l'ordonnateur du fonds.

Le     directeur régional des finances publiques en est le comptable assignataire.
