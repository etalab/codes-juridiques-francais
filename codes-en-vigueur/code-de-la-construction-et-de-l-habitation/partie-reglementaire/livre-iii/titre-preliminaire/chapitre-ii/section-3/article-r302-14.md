# Article R302-14

I.-La population ou le nombre d'habitants mentionnés à la section II du chapitre II du titre préliminaire du livre III de la partie législative et à la présente section est la population municipale définie à l'article R. 2151-1 du code général des collectivités territoriales.

II.-Pour les agglomérations ou établissements publics de coopération intercommunale mentionnés à l'article L. 302-5, la nécessité d'effectuer un effort de production supplémentaire est appréciée à l'échelle de l'ensemble des communes de l'agglomération ou membres de l'établissement public de coopération intercommunale.

III.-Les agglomérations ou les établissements publics de coopération intercommunale à fiscalité propre mentionnés à l'article L. 302-5 sont en décroissance démographique dès lors que la population, publiée au 1er janvier de l'année de réalisation de l'inventaire défini à l'article L. 302-6, est inférieure à la population publiée cinq années auparavant ou, par défaut, au dernier recensement général de la population.

IV.-Les communes de plus de 15 000 habitants qui n'appartiennent pas à une agglomération ou un établissement public de coopération intercommunale à fiscalité propre de plus de 50 000 habitants comportant au moins une commune de plus de 15 000 habitants, mentionnées au septième alinéa de l'article L. 302-5, sont en croissance démographique dès lors que la population, publiée au 1er janvier de l'année de réalisation de l'inventaire défini à l'article L. 302-6, est au moins supérieure de 5 % à la population publiée cinq années auparavant ou, par défaut, au dernier recensement général de la population.

V.-La nécessité d'un effort de production supplémentaire de logements locatifs sociaux des communes appartenant aux agglomérations ou établissements publics de coopération intercommunale à fiscalité propre, figurant sur la liste prévue au décret mentionné au deuxième alinéa de l'article L. 302-5, est établie en fonction :

-du ratio entre le nombre de bénéficiaires de l'allocation logement dont le taux d'effort est supérieur à 30 % et le nombre de bénéficiaires de l'allocation logement, établi par extraction des données provenant de la Caisse nationale des allocations familiales, au 1er janvier de l'année précédente ;

-du ratio entre le nombre de logements vacants parmi les logements proposés à la location et le nombre de logements proposés à la location, établi par extraction des données du répertoire des logements locatifs sociaux prévu par l'article L. 411-10, au 1er janvier de l'année précédente ;

-du ratio entre le nombre de demandes de logement locatif social, hors demandes de mutation au sein du parc locatif social, et le nombre d'attributions annuelles, hors mutations internes, établi par extraction des données provenant du système national d'enregistrement de la demande de logement locatif social prévu par l'article L. 441-2-1, au 1er janvier de l'année en cours.

Pour chaque indicateur, une cotation de 10 à 100, par pas de 10 dans le sens croissant pour le premier et le troisième et dans le sens décroissant pour le deuxième, est affectée à l'agglomération ou à l'établissement public de coopération intercommunale en fonction du décile d'appartenance. Le cumul de ces cotations permet l'établissement d'un indicateur global du besoin de logement locatif social sur chacun de ces territoires.

Un effort de production supplémentaire n'est pas justifié dans les agglomérations ou les établissements publics de coopération intercommunale à fiscalité propre lorsque cet indicateur global est inférieur à un seuil précisé par le décret de publication de la liste. Ce décret est mis à jour au début de chaque période triennale définie à l'article L. 302-8. En cours de période, les listes peuvent être modifiées pour tenir compte de l'évolution des périmètres des établissements publics de coopération intercommunale à fiscalité propre.

VI.-La liste des communes en croissance, astreintes à un objectif de 20 % de logements sociaux, prévue au décret mentionné au septième alinéa de l'article L. 302-5 est établie selon une procédure identique à celle définie au V précédent.
