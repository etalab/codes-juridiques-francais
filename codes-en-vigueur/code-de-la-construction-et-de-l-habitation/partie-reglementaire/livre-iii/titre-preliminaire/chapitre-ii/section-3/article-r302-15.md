# Article R302-15

I.-L'inventaire prévu au premier alinéa de l'article L. 302-6 est établi pour chaque bâtiment par la personne morale propriétaire, à défaut par la personne morale gestionnaire. Il comporte les informations suivantes :

A.-Données générales concernant :

1° Informations relatives à l'identité du bailleur et à l'identité du gestionnaire, s'il diffère du propriétaire ;

2° Localisation du bâtiment, date de première mise en location ;

3° Numéro et date d'effet de la convention pour les logements conventionnés mentionnés à l'article L. 351-2, année d'expiration de la convention.

B.-Nombre de logements locatifs sociaux, au sens de l'article L. 302-5, dans le bâtiment et types de financements initiaux, pour chacune des catégories suivantes :

1° Logements appartenant aux organismes d'habitation à loyer modéré et construits ou acquis et améliorés avant le 5 janvier 1977 ;

2° Autres logements conventionnés ;

3° Logements mentionnés au 3° de l'article L. 302-5 ;

4° Logements ou équivalents logement des lits et places mentionnés au 4° de l'article L. 302-5, le nombre de logements équivalents étant obtenu en retenant la partie entière issue du calcul effectué à raison d'un logement pour trois lits en logements-foyers ou pour trois places en centre d'hébergement et de réinsertion sociale.

II.-Les inventaires prévus aux premier et deuxième alinéas de l'article L. 302-6 sont établis selon des modalités définies par arrêté du ministre chargé du logement.
