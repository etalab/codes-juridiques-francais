# Chapitre unique : Dispositions relatives au tiers-financement

- [Article R381-9](article-r381-9.md)
- [Article R381-10](article-r381-10.md)
- [Article R381-11](article-r381-11.md)
- [Article R381-12](article-r381-12.md)
