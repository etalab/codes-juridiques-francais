# Section 4 : Dispositions spécifiques applicables dans les cas de délégation de compétence prévues aux articles L. 301-5-1 et L. 301-5-2.

- [Article R381-7](article-r381-7.md)
- [Article R381-8](article-r381-8.md)
