# Code de l'artisanat

- [Titre I : De l'artisan, du maître artisan et du compagnon.](titre-i)
- [Titre II : Des       chambres de métiers et de l'artisanat de région](titre-ii)
- [Titre IV : De l'apprentissage artisanal.](titre-iv)
- [Titre VI : Des adjudications et des marchés](titre-vi)
- [Titre VII : De l'assistance aux artisans sans travail.](titre-vii)
- [Titre VIII : Dispositions spéciales à l'artisanat dans les départements d'outre-mer.](titre-viii)
- [Titre VIII bis : Dispositions relatives à l'artisanat à Mayotte](titre-viii-bis)
- [Titre VIII ter : Dispositions relatives à l'artisanat à Saint-Pierre-et-Miquelon](titre-viii-ter)
- [Titre IX : Dispositions diverses.](titre-ix)
