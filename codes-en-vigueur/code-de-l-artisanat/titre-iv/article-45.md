# Article 45

La       chambre de métiers et de l'artisanat de région  peut déterminer la durée minimum de l'apprentissage des métiers dans son règlement d'apprentissage, après avoir consulté les organisations artisanales de son ressort. Elle peut aussi limiter le nombre des apprentis admissibles dans les entreprises artisanales de son ressort, après consultation des organisations patronales et ouvrières intéressées, cette limitation n'étant applicable qu'après approbation du ministre chargé de l'enseignement technique.
