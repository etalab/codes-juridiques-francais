# Article 28-2

Avant le 1er juillet de chaque année, le président de la chambre de métiers et de l'artisanat de région adresse à l'autorité de tutelle le compte de gestion de l'année précédente, accompagné des annexes prévues au II de l'article 28-1 et du rapport du commissaire aux comptes.

Le compte de gestion de la chambre de métiers et de l'artisanat de région et ses annexes font l'objet d'une approbation de l'autorité de tutelle.  L'autorité se prononce dans les trente jours de sa saisine ; le défaut de réponse dans ce délai vaut approbation. Si cette autorité ne peut donner son approbation, elle adresse au président de la chambre, dans les trente jours de sa saisine, une demande de modification du ou des documents qui le justifient. La réponse à cette demande fait courir un nouveau délai d'approbation ou non du compte de gestion.

L'autorité de tutelle transmet au ministre chargé de l'artisanat un exemplaire du compte de gestion approuvé assorti de ses annexes, ou un rapport exposant les motifs l'ayant conduite à en refuser l'approbation.

Les chambres de métiers et de l'artisanat de région rendent publics sur leur site internet, dans le mois qui suit l'approbation de ces documents par l'autorité de tutelle, leur compte de gestion assorti de ses annexes ainsi que le rapport du commissaire aux comptes. Elles transmettent ces documents, dans le même délai, à l'Assemblée permanente des chambres de métiers et de l'artisanat.
