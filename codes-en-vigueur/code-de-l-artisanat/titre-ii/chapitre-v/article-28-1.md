# Article 28-1

I. ― Le compte de gestion retrace l'exécution du budget de chaque exercice, en fonctionnement et en investissement.

A l'issue de l'exercice, le président de la chambre adresse le projet de compte de gestion, assorti des pièces et justificatifs nécessaires, au commissaire aux comptes. Celui-ci exerce sa mission conformément aux dispositions des articles L. 823-9 à L. 823-18 du code de commerce.

A sa plus prochaine séance suivant le dépôt du rapport du commissaire aux comptes, l'assemblée générale de la chambre de métiers et de l'artisanat de région adopte le compte de gestion de l'exercice précédent.

Lorsque des chambres départementales de métiers et de l'artisanat s'unissent avec une chambre régionale de métiers et de l'artisanat en une seule chambre de métiers et de l'artisanat de région, l'assemblée générale de la nouvelle chambre adopte les comptes du dernier exercice clos de chacune des chambres qui ont fusionné.

II. ― Le compte de gestion sur lequel se prononce l'assemblée générale est complété des documents annexes suivants :

1° L'état en fin d'exercice des emplois permanents de l'établissement, mentionnant le statut, le grade et l'indice de rémunération de leur détenteur ;

2° Les montants d'imposition votés au titre de l'exercice par l'établissement et agréés par l'autorité de tutelle en application de l'article 1601 du code général des impôts ;

3° Les recettes en taxe d'apprentissage perçues au titre de l'exercice par la chambre, dont celles qu'elle a utilisées pour elle-même et celles reversées aux autres établissements du réseau ;

4° Le tableau de financement retraçant les variations de ressources et emplois financiers de l'exercice ;

5° La balance définitive des comptes du grand livre pour l'exercice ;

6° Le bilan en fin d'exercice ;

7° L'état des emprunts en cours en fin d'exercice, avec leur tableau d'amortissement ;

8° L'état en fin d'exercice des engagements contractés par l'établissement en crédit-bail ;

9° Le tableau financier de synthèse regroupant les principales données budgétaires et financières de l'établissement.
