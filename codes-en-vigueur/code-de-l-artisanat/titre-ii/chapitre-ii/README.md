# Chapitre II : Fonctionnement.

- [Article 18](article-18.md)
- [Article 19](article-19.md)
- [Article 19 bis](article-19-bis.md)
- [Article 19 ter](article-19-ter.md)
- [Article 19 quater](article-19-quater.md)
- [Article 19 quinquies](article-19-quinquies.md)
- [Article 20](article-20.md)
- [Article 20-1](article-20-1.md)
- [Article 21](article-21.md)
