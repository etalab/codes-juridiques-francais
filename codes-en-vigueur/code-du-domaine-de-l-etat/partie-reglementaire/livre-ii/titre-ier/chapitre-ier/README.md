# Chapitre Ier : Occupation temporaire

- [Section 1 : Délivrance des autorisations](section-1)
- [Section 2 : Fixation des redevances.](section-2)
