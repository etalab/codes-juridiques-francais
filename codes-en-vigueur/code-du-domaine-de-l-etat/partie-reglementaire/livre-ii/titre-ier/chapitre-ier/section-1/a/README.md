# A) Arrêtés individuels.

- [Article A12](article-a12.md)
- [Article A13](article-a13.md)
- [Article A14](article-a14.md)
- [Article A15](article-a15.md)
- [Article A16](article-a16.md)
- [Article A17](article-a17.md)
- [Article A18](article-a18.md)
- [Article A19](article-a19.md)
