# Paragraphe 2 : Dispositions spéciales.

- [Article A1](article-a1.md)
- [Article A2](article-a2.md)
- [Article A3](article-a3.md)
- [Article A4](article-a4.md)
- [Article A5](article-a5.md)
