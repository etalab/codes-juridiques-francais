# Chapitre III : Dispositions communes au domaine immobilier et au domaine mobilier - Biens dépendant de successions en déshérence.

- [Article A118](article-a118.md)
- [Article A119](article-a119.md)
- [Article A120](article-a120.md)
