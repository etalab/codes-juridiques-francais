# Article R334-28

L'accord du débiteur mentionné au III de l'article L. 331-3 est donné par écrit sur un formulaire remis à l'intéressé par le secrétariat de la commission.

Ce formulaire informe le débiteur que la procédure de rétablissement personnel est susceptible d'entraîner une décision de liquidation et porte à sa connaissance les dispositions de l'article L. 332-8.
