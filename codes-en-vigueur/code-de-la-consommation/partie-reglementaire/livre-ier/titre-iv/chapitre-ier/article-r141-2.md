# Article R141-2

Les règles applicables aux procès-verbaux relatifs aux visites effectuées dans les conditions de l'
article L. 450-4 du code de commerce
sont fixées à l'article R. 450-2 du même code.
