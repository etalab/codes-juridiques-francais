# Article R141-6

I.-L'autorité administrative mentionnée à l'article L. 141-1-2 est le directeur général de la concurrence, de la consommation et de la répression des fraudes, le chef du service national des enquêtes de la direction générale de la concurrence, de la consommation et de la répression des fraudes, le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi ou le directeur de la direction départementale chargée de la protection des populations ou leur représentant nommément désigné.

II.-La publication prévue au V de l'article L. 141-1-2 s'effectue par voie de presse, par voie électronique ou par voie d'affichage. La diffusion et l'affichage peuvent être ordonnés cumulativement.

La publication peut porter sur l'intégralité ou sur une partie de la décision, ou prendre la forme d'un communiqué informant le public des motifs et du dispositif de cette décision.

La diffusion de la décision est faite au Journal officiel de la République française, par une ou plusieurs autres publications de presse, ou par un ou plusieurs services de communication au public par voie électronique. Les publications ou les services de communication au public par voie électronique chargés de cette diffusion sont désignés dans la décision. Ils ne peuvent s'opposer à cette diffusion.

L'affichage s'effectue dans les lieux et pour la durée indiqués par la décision ; il ne peut excéder deux mois.

En cas de suppression, dissimulation ou lacération des affiches apposées, il est de nouveau procédé à l'affichage.

Les modalités de la publication sont précisées dans la décision prononçant l'amende.

III.-Le ministre chargé de la consommation est l'ordonnateur compétent pour émettre les titres de perception afférents aux sanctions prononcées en application de l'article L. 141-1-2.
