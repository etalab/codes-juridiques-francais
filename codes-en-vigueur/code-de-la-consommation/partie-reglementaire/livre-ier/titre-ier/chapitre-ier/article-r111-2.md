# Article R111-2

<div align="left">I.-Pour l'application du I de l'article L. 111-2, outre les informations prévues à l'article R. 111-1, le professionnel communique au consommateur ou met à sa disposition les informations suivantes : <br/>
<br/>a) Le statut et la forme juridique de l'entreprise ; <br/>
<br/>b) Les coordonnées permettant d'entrer en contact rapidement et de communiquer directement avec lui ; <br/>
<br/>c) Le cas échéant, le numéro d'inscription au registre du commerce et des sociétés ou au répertoire des métiers ; <br/>
<br/>d) Si son activité est soumise à un régime d'autorisation, le nom et l'adresse de l'autorité ayant délivré l'autorisation ; <br/>
<br/>e) S'il est assujetti à la taxe sur la valeur ajoutée et identifié par un numéro individuel en application de l'article 286 ter du code général des impôts, son numéro individuel d'identification ; <br/>
<br/>f) S'il est membre d'une profession réglementée, son titre professionnel, l'Etat membre dans lequel il a été octroyé ainsi que, le cas échéant, le nom de l'ordre ou de l'organisme professionnel auprès duquel il est inscrit ; <br/>
<br/>g) Les conditions générales, s'il en utilise ; <br/>
<br/>h) Le cas échéant, les clauses contractuelles relatives à la législation applicable et la juridiction compétente ; <br/>
<br/>i) L'éventuelle garantie financière ou assurance de responsabilité professionnelle souscrite par lui, les coordonnées de l'assureur ou du garant ainsi que la couverture géographique du contrat ou de l'engagement. <br/>
<br/>II.-En outre, tout professionnel prestataire de services doit également communiquer au consommateur qui en fait la demande les informations complémentaires suivantes : <br/>
<br/>a) Lorsque le prix n'est pas déterminé au préalable par le prestataire pour un type de service donné, le prix du service ou, lorsqu'un prix exact ne peut pas être indiqué, la méthode de calcul permettant au destinataire de vérifier ce dernier, ou un devis suffisamment détaillé ; <br/>
<br/>b) En ce qui concerne les professions réglementées, une référence aux règles professionnelles applicables dans l'Etat membre de l'Union européenne sur le territoire duquel ce professionnel est établi et aux moyens d'y avoir accès ; <br/>
<br/>c) Des informations sur ses activités pluridisciplinaires et ses partenariats qui sont directement liés au service concerné et sur les mesures prises pour éviter les conflits d'intérêts. Ces informations figurent dans tout document d'information dans lequel le prestataire présente de manière détaillée ses services ; <br/>
<br/>d) Les éventuels codes de conduite auxquels il est soumis, l'adresse électronique à laquelle ces codes peuvent être consultés ainsi que les versions linguistiques disponibles ; <br/>
<br/>e) Les informations sur les conditions de recours à des moyens extrajudiciaires de règlement des litiges, lorsque ces moyens sont prévus par un code de conduite, un organisme professionnel ou toute autre instance. <br/>
<br/>III.-Au sens du d du I, un régime d'autorisation s'entend de toute procédure qui a pour effet d'obliger un prestataire ou un destinataire à faire une démarche auprès d'une autorité compétente en vue d'obtenir un acte formel ou une décision implicite relative à l'accès à une activité de services ou à son exercice.</div>
