# Article R112-15

Chaque livraison de denrées alimentaires à des établissements de restauration est accompagnée d'un document portant l'information mentionnée à l'article R. 112-11.
