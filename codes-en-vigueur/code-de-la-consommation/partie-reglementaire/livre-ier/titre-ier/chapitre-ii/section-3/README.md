# Section 3 : Dispositions particulières

- [Article R112-6](article-r112-6.md)
- [Article R112-7](article-r112-7.md)
- [Article R112-8](article-r112-8.md)
