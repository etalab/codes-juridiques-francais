# Article R113-1

Est puni de la peine d'amende prévue pour les contraventions de la cinquième classe le fait de vendre, proposer à la vente ou promouvoir des biens, produits, ou prestations de services à des prix fixés en violation :

- des textes réglementaires pris en application de l'article L. 410-2 du code de commerce reproduit à l'article L. 113-1, ou de ceux ayant le même objet pris en application de l'ordonnance n° 45-1483 du 30 juin 1945 et maintenus en vigueur à titre transitoire par l'article 61 de l'ordonnance n° 86-1243 du 1er décembre 1986, figurant en annexe au présent code ;

- de l'article L. 3122-2 du code des transports.

En cas de récidive, les peines d'amende prévues pour la récidive des contraventions de la cinquième classe sont applicables.
