# Titre Ier : Information des consommateurs

- [Chapitre Ier : Obligation générale d'information précontractuelle](chapitre-ier)
- [Chapitre II : Modes de présentation et inscriptions](chapitre-ii)
- [Chapitre III : Prix et conditions de vente](chapitre-iii)
- [Chapitre V : Valorisation des produits et des services](chapitre-v)
- [Chapitre VII : Transparence sur les conditions sociales de fabrication d'un produit](chapitre-vii)
