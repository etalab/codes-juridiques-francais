# Article R132-2-2

La publicité prévue au second alinéa de l'article L. 132-2 peut être effectuée par voie de presse, par voie électronique ou par voie d'affichage. La diffusion et l'affichage peuvent être ordonnés cumulativement.

La diffusion ou l'affichage peut porter sur tout ou partie de la mesure d'injonction, ou prendre la forme d'un communiqué informant le public des motifs et du dispositif de cette mesure.

La diffusion de la mesure d'injonction peut être faite au Journal officiel de la République française, par une ou plusieurs autres publications de presse, ou par un ou plusieurs services de communication au public par voie électronique. Les publications ou les services de communication au public par voie électronique chargés de cette diffusion sont désignés dans la mesure d'injonction. Ils ne peuvent s'opposer à cette diffusion.

L'affichage s'effectue dans les lieux et pour la durée indiqués par la mesure d'injonction ; il ne peut excéder deux mois. En cas de suppression, dissimulation ou lacération des affiches apposées, il est de nouveau procédé à l'affichage.

Les modalités de la publicité sont précisées dans la mesure d'injonction.
