# Section 1 : La commission des clauses abusives

- [Article R534-1](article-r534-1.md)
- [Article R534-2](article-r534-2.md)
- [Article R534-3](article-r534-3.md)
- [Article R534-4](article-r534-4.md)
