# Article R215-25

L'autorité administrative mentionnée aux articles L. 215-20 et L. 215-21 est le directeur général de la concurrence, de la consommation et de la répression des fraudes, le chef du service national des enquêtes de la direction générale de la concurrence, de la consommation et de la répression des fraudes, le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi ou le directeur de la direction départementale chargée de la protection des populations ou leur représentant nommément désigné.
