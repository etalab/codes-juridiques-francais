# Article R215-10

L'un au moins des échantillons est laissé au propriétaire ou détenteur du produit.

Sous aucun prétexte, il ne doit modifier l'état des échantillons qui lui sont confiés. Les mesures de garantie qui pourront être imposées, à cet égard, seront fixées par l'un des arrêtés ministériels prévus à l'article R. 215-7.

Toutefois, si le propriétaire ou le détenteur ne dispose pas des moyens de conserver le ou les échantillons dans des conditions de nature à permettre la contre-expertise, les échantillons sont conservés dans un endroit désigné par l'agent verbalisateur, mention en est faite au procès-verbal.
