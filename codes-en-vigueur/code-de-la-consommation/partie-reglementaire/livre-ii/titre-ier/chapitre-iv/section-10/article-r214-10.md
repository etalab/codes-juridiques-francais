# Article R214-10

Constituent les mesures d'exécution prévues à l'article L. 214-1 :

1° Les dispositions des articles 1er à 7 du règlement d'exécution (UE) n° 29/2012 de la Commission du 13 janvier 2012 modifié relatif aux normes de commercialisation de l'huile d'olive ;

2° Les dispositions de l'article 78 et de l'annexe VII, partie VIII "Descriptions et définitions des huiles d'olive et huiles de grignons d'olive" du règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 portant organisation commune des marchés des produits agricoles pour les produits définis à la partie VII de l'annexe I de ce règlement ;

3° Les dispositions des articles 1er et 7 du règlement (CEE) n° 2568 / 91 de la Commission du 11 juillet 1991 modifié relatif aux caractéristiques des huiles d'olive et des huiles de grignons d'olive ainsi qu'aux méthodes d'analyse y afférentes et de ses annexes.
