# Article R218-2

Sauf dans les cas prévus aux articles R. 218-5 et R. 218-6, tout prélèvement effectué en application de l'article L. 218-1-2 comporte au moins trois échantillons. Il donne lieu à l'établissement d'un rapport dans les conditions prévues aux articles R. 215-5 et R. 215-6.

Tout échantillon prélevé est mis sous scellés. Ces scellés retiennent une étiquette d'identification portant les indications mentionnées à l'article R. 215-8.
