# Article R218-4

Le service dont dépend l'agent verbalisateur informe le propriétaire ou le détenteur des échantillons des résultats d'analyse de l'échantillon.

Si l'analyse a établi que l'échantillon n'est pas conforme à la réglementation, le détenteur est informé qu'il peut faire réaliser, à ses frais, une contre-analyse par un laboratoire présentant des garanties d'indépendance, de compétence et d'impartialité. Celui-ci vérifie, avant toute analyse, l'intégrité du scellé apposé sur l'échantillon qu'il a reçu. Le laboratoire procède à l'analyse dans le respect de la réglementation applicable.

Si la contre-analyse infirme le résultat de la première analyse, le détenteur peut faire réaliser à ses frais une analyse du troisième échantillon par le laboratoire national de référence, au sens du  règlement (CE) n° 882/2004 du Parlement européen et du Conseil du 29 avril 2004 relatif aux contrôles officiels effectués pour s'assurer de la conformité avec la législation sur les aliments pour animaux et les denrées alimentaires et avec les dispositions relatives à la santé animale et au bien-être des animaux, compétent dans le domaine d'analyse considéré.

Le résultat de cette dernière analyse est le seul pris en compte pour décider des mesures consécutives au contrôle.
