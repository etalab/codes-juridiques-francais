# Article R423-11

Les consommateurs membres du groupe qui n'ont pas exprimé leur acceptation dans le délai et selon les modalités fixés par le juge en application de l'alinéa 2 de l'article L. 423-10 et dans les conditions prévues par l'article R. 423-10 ne sont plus recevables à demander leur indemnisation dans le cadre de l'action de groupe et ne sont pas représentés par l'association requérante.
