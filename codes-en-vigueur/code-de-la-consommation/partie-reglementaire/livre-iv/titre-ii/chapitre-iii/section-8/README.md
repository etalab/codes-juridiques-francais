# Section 8 : Dispositions relatives aux outre-mer

- [Article R423-24](article-r423-24.md)
- [Article R423-25](article-r423-25.md)
