# Article R423-25

Pour l'application du présent chapitre dans les îles Wallis et Futuna, les références au tribunal de grande instance sont remplacées par les références au tribunal de première instance.
