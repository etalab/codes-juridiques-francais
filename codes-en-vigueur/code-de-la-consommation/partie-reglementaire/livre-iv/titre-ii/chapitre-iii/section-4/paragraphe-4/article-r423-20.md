# Article R423-20

Les demandes d'indemnisation auxquelles le professionnel n'a pas fait droit sont portées devant le tribunal de grande instance, en vue de l'audience fixée en application de l'article R. 423-7, dans les formes prévues pour les demandes incidentes et dans le délai fixé par le juge pour le saisir, conformément à l'article L. 423-7.

S'il n'a été saisi d'aucune demande d'indemnisation dans le délai fixé en application du second alinéa de l'article L. 423-7, le juge constate l'extinction de l'instance.
