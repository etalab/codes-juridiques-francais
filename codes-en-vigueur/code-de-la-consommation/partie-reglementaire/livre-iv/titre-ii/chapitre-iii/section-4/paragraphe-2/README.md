# Paragraphe 2 : Adhésion au groupe

- [Article R423-14](article-r423-14.md)
- [Article R423-15](article-r423-15.md)
- [Article R423-16](article-r423-16.md)
- [Article R423-17](article-r423-17.md)
