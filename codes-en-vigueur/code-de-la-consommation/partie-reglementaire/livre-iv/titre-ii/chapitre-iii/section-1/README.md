# Section 1 : Dispositions préliminaires

- [Article R423-1](article-r423-1.md)
- [Article R423-2](article-r423-2.md)
- [Article R423-3](article-r423-3.md)
- [Article R423-4](article-r423-4.md)
- [Article R423-5](article-r423-5.md)
