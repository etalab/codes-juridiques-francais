# Article R423-2

Le tribunal de grande instance territorialement compétent est celui du lieu où demeure le défendeur.

Le tribunal de grande instance de Paris est compétent lorsque le défendeur demeure à l'étranger ou n'a ni domicile ni résidence connus.
