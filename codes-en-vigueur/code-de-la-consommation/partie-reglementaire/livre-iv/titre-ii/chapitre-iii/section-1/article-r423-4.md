# Article R423-4

La demande est formée, instruite et jugée selon les règles applicables à la procédure ordinaire en matière contentieuse devant le tribunal de grande instance.

L'appel est jugé selon la procédure prévue à l'article 905 du code de procédure civile.
