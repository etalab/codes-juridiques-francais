# Article L138-5

<div align="left">Lorsque le consommateur confie la livraison du bien à un transporteur autre que celui proposé par le professionnel, le risque de perte ou d'endommagement du bien est transféré au consommateur à la remise du bien au transporteur.<br/>
<br/>
<br/>
</div>
