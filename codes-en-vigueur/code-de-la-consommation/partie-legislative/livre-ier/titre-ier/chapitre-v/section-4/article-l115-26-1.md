# Article L115-26-1

Les agents mentionnés à l'article L. 215-1 sont habilités à rechercher et à constater les infractions aux dispositions du titre IV du livre VI du code rural et de la pêche maritime et aux textes pris pour son application ainsi qu'aux dispositions des sections 1 à 3 du présent chapitre et aux textes pris pour leur application. Ils disposent à cet effet des pouvoirs d'enquête prévus aux articles L. 215-1 à L. 215-17 du présent code.
