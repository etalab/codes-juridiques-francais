# Article L121-85-1

Tout manquement aux articles L. 121-83 à L. 121-84-11 est passible d'une amende administrative dont le montant ne peut excéder 3 000 € pour une personne physique et 15 000 € pour une personne morale. L'amende est prononcée dans les conditions prévues à l'article L. 141-1-2.
