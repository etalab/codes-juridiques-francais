# Article L121-36

Les pratiques commerciales mises en œuvre par les professionnels à l'égard des consommateurs, sous la forme d'opérations promotionnelles tendant à l'attribution d'un gain ou d'un avantage de toute nature par la voie d'un tirage au sort, quelles qu'en soient les modalités, ou par l'intervention d'un élément aléatoire, sont licites dès lors qu'elles ne sont pas déloyales au sens de l'article L. 120-1.
