# Article L121-50

Constituent, au sens de la présente section, des préparations pour nourrissons les denrées alimentaires destinées à l'alimentation des enfants jusqu'à l'âge de quatre mois accomplis et présentées comme répondant à elles seules à l'ensemble des besoins nutritionnels de ceux-ci.
