# Article L534-4

La commission de la sécurité des consommateurs est composée d'un président nommé par décret en conseil des ministres, de membres des juridictions de l'ordre administratif ou judiciaire. Elle comprend en outre des personnes appartenant aux organisations professionnelles, aux associations nationales de consommateurs et des experts. Ces personnes et experts sont désignés par le ministre chargé de la consommation après avis des ministres intéressés et sont choisis en raison de leurs compétences en matière de prévention des risques.

Un commissaire du Gouvernement désigné par le ministre chargé de la consommation siège auprès de la commission. Il peut, dans les quatre jours d'une délibération de la commission, provoquer une seconde délibération.
