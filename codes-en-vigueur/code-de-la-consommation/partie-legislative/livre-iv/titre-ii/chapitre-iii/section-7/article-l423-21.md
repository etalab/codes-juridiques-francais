# Article L423-21

Les décisions prévues aux articles L. 423-3 et L. 423-10 ainsi que celle résultant de l'application de l'article L. 423-16 ont également autorité de la chose jugée à l'égard de chacun des membres du groupe dont le préjudice a été réparé au terme de la procédure.
