# Section 4 : Explications fournies à l'emprunteur et évaluation de sa solvabilité

- [Article L311-8](article-l311-8.md)
- [Article L311-8-1](article-l311-8-1.md)
- [Article L311-9](article-l311-9.md)
- [Article L311-10](article-l311-10.md)
- [Article L311-10-1](article-l311-10-1.md)
