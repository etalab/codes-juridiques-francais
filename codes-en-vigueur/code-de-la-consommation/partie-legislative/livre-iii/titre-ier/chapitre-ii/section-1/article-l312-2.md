# Article L312-2

Les dispositions du présent chapitre s'appliquent aux prêts qui, quelle que soit leur qualification ou leur technique, sont consentis de manière habituelle par toute personne physique ou morale en vue de financer les opérations suivantes :

1° Pour les immeubles à usage d'habitation ou à usage professionnel et d'habitation :

a) Leur acquisition en propriété ou la souscription ou l'achat de parts ou actions de sociétés donnant vocation à leur attribution en propriété, y compris lorsque ces opérations visent également à permettre la réalisation de travaux de réparation, d'amélioration ou d'entretien de l'immeuble ainsi acquis ;

b) Leur acquisition en jouissance ou la souscription ou l'achat de parts ou actions de sociétés donnant vocation à leur attribution en jouissance, y compris lorsque ces opérations visent également à permettre la réalisation de travaux de réparation, d'amélioration ou d'entretien de l'immeuble ainsi acquis ;

c) Les dépenses relatives à leur réparation, leur amélioration ou leur entretien lorsque le montant du crédit est supérieur à 75 000 € ;

d) Les dépenses relatives à leur construction ;

2° L'achat de terrains destinés à la construction des immeubles mentionnés au 1° ci-dessus.
