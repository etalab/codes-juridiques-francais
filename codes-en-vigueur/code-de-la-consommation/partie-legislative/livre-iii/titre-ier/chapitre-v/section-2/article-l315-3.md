# Article L315-3

Pour leur application en Nouvelle-Calédonie et en Polynésie française :

1° Au premier alinéa de l'article L. 312-3-1, les mots : "étrangère à l'Union européenne remboursables en monnaie nationale" sont remplacés par les mots : "autre que l'euro ou le franc CFP" et, au deuxième alinéa, les mots : "monnaie nationale" sont remplacés par les mots : "euros ou en francs CFP" ;

2° Pour l'application de l'article L. 312-6-1, les mots : "en euros" sont remplacés par les mots : "en euros ou en francs CFP" ;.

3° A l'article L. 312-15, les mots : "et le contrat préliminaire prévu à l'article L. 261-15 du code de la construction et de l'habitation" sont supprimés ;

4° A l'article L. 312-36, les mots : "Le tribunal d'instance" sont remplacés par les mots : "Le tribunal de première instance".
