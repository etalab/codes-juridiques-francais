# Article R931-3

Il peut être demandé au Conseil d'Etat de prononcer une astreinte pour assurer l'exécution d'une décision rendue par le Conseil d'Etat ou par une juridiction administrative spéciale.

Ces demandes ne peuvent être présentées, sauf décision explicite de refus d'exécution opposée par l'autorité administrative, qu'après l'expiration d'un délai de six mois à compter de la date de notification des décisions juridictionnelles.

Toutefois, dans le cas où la décision dont l'exécution est poursuivie a elle-même déterminé un délai dans lequel l'administration doit prendre les mesures d'exécution prescrites, la demande ne peut être présentée qu'à l'expiration de ce délai.
