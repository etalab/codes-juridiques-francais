# Article R931-4

Lorsque le président de la section du contentieux exerce les pouvoirs prévus au dernier alinéa de l'article L. 911-5, il statue par ordonnance motivée.
