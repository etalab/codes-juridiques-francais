# Article R742-6

Les ordonnances ne sont pas prononcées en audience publique.
