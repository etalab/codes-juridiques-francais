# Titre Ier : L'inscription au rôle

- [Chapitre Ier : Dispositions applicables aux tribunaux administratifs et aux cours administratives d'appel](chapitre-ier)
- [Chapitre II : Dispositions applicables au Conseil d'Etat](chapitre-ii)
