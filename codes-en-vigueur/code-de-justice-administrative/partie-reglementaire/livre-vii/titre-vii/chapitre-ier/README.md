# Chapitre Ier : Les questions de répartition de compétence entre juridictions administratives et judiciaires

- [Section 1 : La saisine du Tribunal des conflits](section-1)
- [Section 2 :  La question préjudicielle](section-2)
