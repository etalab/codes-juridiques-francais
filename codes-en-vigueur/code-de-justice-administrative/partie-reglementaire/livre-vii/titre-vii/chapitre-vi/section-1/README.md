# Section 1 : Dispositions communes

- [Article R776-1](article-r776-1.md)
- [Article R776-2](article-r776-2.md)
- [Article R776-3](article-r776-3.md)
- [Article R776-4](article-r776-4.md)
- [Article R776-5](article-r776-5.md)
- [Article R776-6](article-r776-6.md)
- [Article R776-7](article-r776-7.md)
- [Article R776-8](article-r776-8.md)
- [Article R776-9](article-r776-9.md)
