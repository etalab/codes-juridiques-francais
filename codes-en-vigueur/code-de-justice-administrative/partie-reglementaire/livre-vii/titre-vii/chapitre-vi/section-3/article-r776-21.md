# Article R776-21

Le président du tribunal administratif ou le magistrat désigné statue dans le délai de soixante-douze heures prévu au deuxième alinéa du III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile.

Ce délai court à partir de l'heure d'enregistrement de la requête au greffe du tribunal. Lorsque l'étranger est placé en rétention ou assigné à résidence après avoir introduit un recours contre la décision portant obligation de quitter le territoire, il court à compter de la transmission par le préfet de la décision de placement en rétention ou d'assignation à résidence.
