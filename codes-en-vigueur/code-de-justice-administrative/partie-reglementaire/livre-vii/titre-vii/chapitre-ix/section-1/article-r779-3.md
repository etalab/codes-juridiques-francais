# Article R779-3

Le délai de soixante-douze heures imparti au président du tribunal administratif ou à son délégué pour statuer court à partir de l'heure d'enregistrement de la requête au greffe du tribunal.

Lorsqu'elles sont faites par voie électronique conformément aux articles R. 611-8-2 et R. 711-2-1, les communications et convocations sont réputées reçues dès leur mise à disposition dans l'application.
