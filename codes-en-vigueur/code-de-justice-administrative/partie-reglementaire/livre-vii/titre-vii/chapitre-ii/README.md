# Chapitre II : Le contentieux des impôts directs, des taxes sur le chiffre d'affaires et des taxes assimilées

- [Article R772-1](article-r772-1.md)
- [Article R772-2](article-r772-2.md)
- [Article R772-3](article-r772-3.md)
- [Article R772-4](article-r772-4.md)
