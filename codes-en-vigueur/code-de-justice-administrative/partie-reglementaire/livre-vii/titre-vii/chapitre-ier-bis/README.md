# Chapitre Ier bis : La question prioritaire de constitutionnalité

- [Section 1 : Dispositions applicables devant les tribunaux administratifs et les cours administratives d'appel](section-1)
- [Section 2 : Dispositions applicables devant le Conseil d'Etat](section-2)
