# Article R822-5

En cas de désistement avant l'admission du pourvoi, ou si le requérant est réputé s'être désisté en application de l'article R. 611-22, le président de la sous-section donne acte du désistement par ordonnance.

Lorsque le pourvoi devient sans objet avant son admission, le président de la sous-section peut constater par ordonnance qu'il n'y a plus lieu d'y statuer.

Lorsque le pourvoi est irrecevable pour défaut de ministère d'avocat ou entaché d'une irrecevabilité manifeste non susceptible d'être couverte en cours d'instance, le président de la sous-section peut décider par ordonnance de ne pas l'admettre.

Lorsqu'il est manifeste qu'aucun moyen sérieux n'est invoqué, le président de la sous-section peut également décider par ordonnance de ne pas admettre :

1° Les pourvois relevant d'une série qui, sans appeler de nouvelle appréciation ou qualification de faits, présentent à juger en droit des questions identiques à celles que le Conseil d'Etat statuant au contentieux a déjà tranchées par une décision ou examinées par un avis rendu en application de l'article L. 113-1 ;

2° Les pourvois dirigés contre les ordonnances prises en application de l'article R. 222-1 ainsi que celles prises en vertu de l'article R. 351-28 du code de l'action sociale et des familles, de l'article R. 242-97 du code rural et de la pêche maritime, des articles R. 4126-5 et R. 4234-29 du code de la santé publique et des articles L. 145-9,
L. 145-9-2 et R. 145-20 du code de la sécurité sociale ;

3° Les pourvois dirigés contre les ordonnances prises en application du livre V ;

4° Les pourvois qui ne soulèvent que des moyens irrecevables, inopérants ou dépourvus des précisions permettant d'en apprécier le bien-fondé, des moyens de régularité dénués de fondement et des moyens revenant à contester l'appréciation des faits à laquelle se sont souverainement livrés les juges du fond.

Le président de la section du contentieux et les présidents adjoints de cette section peuvent statuer par ordonnance dans les cas prévus au troisième alinéa du présent article.
