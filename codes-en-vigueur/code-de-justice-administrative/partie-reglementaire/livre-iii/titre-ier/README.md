# Titre Ier : La compétence de premier ressort

- [Chapitre Ier : La compétence en raison de la matière](chapitre-ier)
- [Chapitre II : La compétence territoriale des tribunaux administratifs](chapitre-ii)
