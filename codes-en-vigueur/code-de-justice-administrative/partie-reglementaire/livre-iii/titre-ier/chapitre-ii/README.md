# Chapitre II : La compétence territoriale des tribunaux administratifs

- [Section 1 : Principes](section-1)
- [Section 2 : Exceptions](section-2)
