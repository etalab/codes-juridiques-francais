# Article R341-3

Dans le cas où un tribunal administratif est saisi de conclusions distinctes mais connexes relevant les unes de sa compétence et les autres de la compétence en premier et dernier ressort du Conseil d'Etat, son président renvoie l'ensemble de ces conclusions au Conseil d'Etat.
