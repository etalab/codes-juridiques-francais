# Article R232-22

Le président du Conseil supérieur désigne pour chaque affaire un rapporteur qui peut être le secrétaire général des tribunaux administratifs et des cours administratives d'appel ou l'un des membres du conseil. Lorsque le Conseil supérieur émet une proposition, le dossier au vu duquel il se prononce comporte l'avis écrit du conseiller d'Etat, chef de la mission permanente d'inspection des juridictions administratives.

Pour élaborer son rapport sur les propositions afférentes aux nominations, détachements et intégrations prévus aux articles L. 233-3, L. 233-4 et L. 233-5, le rapporteur peut être assisté par une formation restreinte du Conseil supérieur, laquelle peut procéder à toutes les mesures d'instruction utiles, y compris des auditions. Cette formation restreinte est désignée par le Conseil supérieur ; elle comprend un ou plusieurs représentants élus des magistrats des tribunaux administratifs et des cours administratives d'appel.

Le secrétaire général adjoint du Conseil d'Etat chargé des tribunaux administratifs et des cours administratives d'appel participe aux travaux du Conseil supérieur sans voix délibérative. A l'invitation du président, le Conseil supérieur peut entendre les chefs de service du Conseil d'Etat ou leur délégué ainsi que tout expert.
