# Article R234-2

Les premiers conseillers sont nommés au choix après inscription au tableau d'avancement parmi les conseillers qui justifient de trois années au moins de services effectifs dans le corps et ont atteint le 6e échelon de leur grade.

Ils sont classés au 1er échelon du grade de premier conseiller. Les conseillers promus au grade de premier conseiller après avoir atteint le 7e échelon de leur ancien grade conservent, dans la limite d'un an, l'ancienneté acquise dans cet échelon.
