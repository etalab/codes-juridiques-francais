# Article R234-3

Pour l'application des articles R. 233-7 et R. 234-2 ci-dessus, les services effectifs accomplis dans un autre corps recruté par la voie de l'Ecole nationale d'administration sont assimilés à des services effectifs dans le corps des tribunaux administratifs et des cours administratives d'appel.
