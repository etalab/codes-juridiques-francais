# Sous-section 1 : Dispositions relatives au personnel

- [Article R226-1](article-r226-1.md)
- [Article R226-2](article-r226-2.md)
- [Article R226-3](article-r226-3.md)
- [Article R226-4](article-r226-4.md)
