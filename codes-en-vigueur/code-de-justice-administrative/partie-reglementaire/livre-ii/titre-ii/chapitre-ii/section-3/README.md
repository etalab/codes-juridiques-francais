# Section 3 : Fonctionnement des cours administratives d'appel

- [Article R222-25](article-r222-25.md)
- [Article R222-26](article-r222-26.md)
- [Article R222-27](article-r222-27.md)
- [Article R222-28](article-r222-28.md)
- [Article R222-29](article-r222-29.md)
- [Article R222-29-1](article-r222-29-1.md)
- [Article R222-30](article-r222-30.md)
- [Article R222-31](article-r222-31.md)
- [Article R222-32](article-r222-32.md)
