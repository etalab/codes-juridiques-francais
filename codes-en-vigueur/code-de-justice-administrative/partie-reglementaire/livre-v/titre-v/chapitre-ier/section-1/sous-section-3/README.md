# Sous-section 3 : Dispositions communes

- [Article R551-3](article-r551-3.md)
- [Article R551-4](article-r551-4.md)
- [Article R551-5](article-r551-5.md)
- [Article R551-6](article-r551-6.md)
