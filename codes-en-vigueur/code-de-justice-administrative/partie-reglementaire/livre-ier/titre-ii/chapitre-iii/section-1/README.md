# Section 1 : Les sections administratives

- [Article R123-2](article-r123-2.md)
- [Article R123-3](article-r123-3.md)
- [Article R123-3-1](article-r123-3-1.md)
- [Article R123-4](article-r123-4.md)
- [Article R123-5](article-r123-5.md)
- [Article R123-6](article-r123-6.md)
- [Article R123-6-1](article-r123-6-1.md)
- [Article R123-7](article-r123-7.md)
- [Article R123-8](article-r123-8.md)
- [Article R123-9](article-r123-9.md)
- [Article R123-10](article-r123-10.md)
- [Article R123-11](article-r123-11.md)
