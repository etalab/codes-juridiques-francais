# Chapitre III : Le Conseil d'Etat dans l'exercice de ses attributions administratives et législatives

- [Section 1 : Les sections administratives](section-1)
- [Section 2 : L'assemblée générale](section-2)
- [Section 3 : La commission permanente](section-3)
- [Section 4 : Dispositions communes](section-4)
- [Article R123-1](article-r123-1.md)
