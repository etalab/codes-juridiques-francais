# Article R122-28-1

Le secrétariat des séances est assuré par le secrétaire du contentieux, le secrétaire adjoint, les secrétaires de sous-section ainsi que par les agents de la section désignés à cet effet par le président de la section du contentieux.
