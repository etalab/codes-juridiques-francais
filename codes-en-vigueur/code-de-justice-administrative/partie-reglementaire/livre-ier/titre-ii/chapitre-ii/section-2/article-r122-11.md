# Article R122-11

Sous réserve des dispositions de l'article R. 122-12 et de celles de l'article R. 122-17, le jugement des affaires est confié à une sous-section ou à deux, trois ou quatre sous-sections réunies.

Le groupement de sous-sections en formations de jugement est fixé par arrêté du vice-président du Conseil d'Etat, sur proposition du président de la section du contentieux.
