# Article R122-13

Lorsqu'il statue en application des articles L. 512-2 à L. 512-5 et L. 513-3 du code de l'entrée et du séjour des étrangers et du droit d'asile, le président de la section du contentieux, ou son délégué, peut statuer par ordonnance dans les cas prévus à l'article précédent.

Il peut, dans les mêmes conditions, rejeter les requêtes qui ne sont manifestement pas susceptibles d'entraîner l'infirmation du jugement attaqué.

Ces dispositions sont applicables aux appels enregistrés avant le 1er janvier 2005.
