# Article R122-1

La section du contentieux est juge de toutes les affaires qui relèvent de la juridiction du Conseil d'Etat, sous réserve des dispositions de l'article R. 122-17.

Elle est divisée en dix sous-sections qui participent à l'instruction et au jugement des affaires dans les conditions prévues au présent livre.
