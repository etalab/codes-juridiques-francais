# Section 2 : Nomination des membres du Conseil d'Etat choisis parmi les magistrats des tribunaux administratifs et des cours administratives d'appel

- [Article R*133-3](article-r-133-3.md)
- [Article R*133-4](article-r-133-4.md)
- [Article R*133-7](article-r-133-7.md)
- [Article R*133-8](article-r-133-8.md)
- [Article R*133-9](article-r-133-9.md)
