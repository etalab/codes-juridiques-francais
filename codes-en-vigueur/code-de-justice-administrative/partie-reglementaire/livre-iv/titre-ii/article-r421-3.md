# Article R421-3

Toutefois, l'intéressé n'est forclos qu'après un délai de deux mois à compter du jour de la notification d'une décision expresse de rejet :

1° En matière de plein contentieux ;

2° Dans le contentieux de l'excès de pouvoir, si la mesure sollicitée ne peut être prise que par décision ou sur avis des assemblées locales ou de tous autres organismes collégiaux ;

3° Dans le cas où la réclamation tend à obtenir l'exécution d'une décision de la juridiction administrative.
