# Article R431-8

Les parties non représentées devant un tribunal administratif qui ont leur résidence hors du territoire de la République doivent faire élection de domicile dans le ressort de ce tribunal.
