# Article R621-1

La juridiction peut, soit d'office, soit sur la demande des parties ou de l'une d'elles, ordonner, avant dire droit, qu'il soit procédé à une expertise sur les points déterminés par sa décision. La mission confiée à l'expert peut viser à concilier les parties.
