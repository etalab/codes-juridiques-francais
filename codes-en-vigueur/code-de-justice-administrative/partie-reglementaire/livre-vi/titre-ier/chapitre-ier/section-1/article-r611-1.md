# Article R611-1

La requête et les mémoires, ainsi que les pièces produites par les parties, sont déposés ou adressés au greffe.

La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes dans les conditions prévues aux articles R. 611-3, R. 611-5 et R. 611-6.

Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux.
