# Section 2 : Nomination au tour extérieur

- [Article L233-3](article-l233-3.md)
- [Article L233-4](article-l233-4.md)
- [Article L233-4-1](article-l233-4-1.md)
