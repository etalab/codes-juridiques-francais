# Article L136-2

Les sanctions disciplinaires sont prononcées par l'autorité investie du pouvoir de nomination sur proposition du garde des sceaux, ministre de la justice, après avis de la commission consultative.

Toutefois, l'avertissement et le blâme peuvent être prononcés, sans consultation de la commission consultative, par le vice-président du Conseil d'Etat.
