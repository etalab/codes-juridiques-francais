# Livre VII : Le jugement

- [Titre II : L'abstention et la récusation](titre-ii)
- [Titre III : La tenue de l'audience](titre-iii)
- [Titre IV : La décision](titre-iv)
- [Titre VI : Les frais et dépens](titre-vi)
- [Titre VII : Dispositions spéciales](titre-vii)
- [Titre VIII : Dispositions particulières aux tribunaux administratifs d'outre-mer](titre-viii)
