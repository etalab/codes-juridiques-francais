# Sous-section 2 : Contrats passés par les entités adjudicatrices

- [Article L551-5](article-l551-5.md)
- [Article L551-6](article-l551-6.md)
- [Article L551-7](article-l551-7.md)
- [Article L551-8](article-l551-8.md)
- [Article L551-9](article-l551-9.md)
