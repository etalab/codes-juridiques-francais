# Chapitre Ier : Le référé en matière de passation de contrats et marchés

- [Section 1 : Référé précontractuel](section-1)
- [Section 2 : Référé contractuel](section-2)
- [Section 3 : Dispositions applicables en Nouvelle-Calédonie, en Polynésie française et dans les îles Wallis et Futuna.](section-3)
