# Section II : Disponibilité et réserve dans la police nationale.

- [Article R*201-20-1](article-r-201-20-1.md)
- [Article R*201-20-2](article-r-201-20-2.md)
- [Article R*201-20-3](article-r-201-20-3.md)
- [Article R*201-20-4](article-r-201-20-4.md)
- [Article R*201-20-5](article-r-201-20-5.md)
- [Article R*201-20-6](article-r-201-20-6.md)
- [Article R*201-20-7](article-r-201-20-7.md)
