# Paragraphe 2 : Droits résultant des opérations de sélection.

- [Article R*43](article-r-43.md)
- [Article R*43-1](article-r-43-1.md)
- [Article R*43-2](article-r-43-2.md)
- [Article R*43-3](article-r-43-3.md)
