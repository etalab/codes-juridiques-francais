# Paragraphe 1er : Dispositions générales.

- [Article R*40](article-r-40.md)
- [Article R*41](article-r-41.md)
- [Article R*42](article-r-42.md)
