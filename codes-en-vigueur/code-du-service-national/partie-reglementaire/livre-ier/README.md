# LIVRE Ier : Obligations du service national.

- [Chapitre Ier : Le recensement.](chapitre-ier)
- [Chapitre Ier bis : Dispositions relatives au service civique](chapitre-ier-bis)
- [Chapitre II : La journée défense et citoyenneté](chapitre-ii)
