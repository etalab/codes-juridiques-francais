# Article R120-8

Un commissaire du Gouvernement auprès de l'agence est nommé par le ministre chargé de la jeunesse. Celui-ci peut se faire représenter. Il assiste, avec voix consultative, aux séances de toutes les instances de délibération et d'administration du groupement.

Il reçoit communication de tous les documents relatifs au groupement. Il dispose d'un droit de visite dans les locaux appartenant au groupement ou mis à sa disposition.

Pour les décisions qui mettent en jeu l'existence ou le bon fonctionnement du groupement, le commissaire du Gouvernement peut provoquer une nouvelle délibération dans un délai de quinze jours à compter de la date à laquelle le procès-verbal de la séance lui a été communiqué.

Il informe les administrations dont relèvent les établissements publics participant au groupement.

Il adresse chaque année au ministre chargé de la jeunesse et au ministre chargé du budget un rapport sur l'activité et la gestion du groupement.
