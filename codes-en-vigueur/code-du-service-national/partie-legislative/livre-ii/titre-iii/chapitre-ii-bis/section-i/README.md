# Section I : Dispositions générales.

- [Article L94-1](article-l94-1.md)
- [Article L94-2](article-l94-2.md)
