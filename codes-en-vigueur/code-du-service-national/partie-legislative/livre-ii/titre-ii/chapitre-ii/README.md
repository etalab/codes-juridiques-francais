# Chapitre II : Exemptions, dispenses et modalités particulières d'accomplissement des obligations d'activité du service national

- [Section I : Exemptions.](section-i)
- [Section II : Dispenses.](section-ii)
- [Section IV : Condamnés.](section-iv)
