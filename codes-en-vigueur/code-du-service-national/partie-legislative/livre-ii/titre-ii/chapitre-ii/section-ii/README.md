# Section II : Dispenses.

- [Article L31](article-l31.md)
- [Article L32](article-l32.md)
- [Article L32 bis](article-l32-bis.md)
- [Article L33](article-l33.md)
- [Article L34](article-l34.md)
- [Article L35](article-l35.md)
- [Article L36](article-l36.md)
- [Article L37](article-l37.md)
- [Article L38](article-l38.md)
- [Article L39](article-l39.md)
- [Article L40](article-l40.md)
- [Article L40-1](article-l40-1.md)
