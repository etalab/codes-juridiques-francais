# Chapitre III bis : Dispositions particulières au service dans la police nationale.

- [Article L149-1](article-l149-1.md)
- [Article L149-2](article-l149-2.md)
- [Article L149-3](article-l149-3.md)
- [Article L149-4](article-l149-4.md)
- [Article L149-5](article-l149-5.md)
- [Article L149-6](article-l149-6.md)
- [Article L149-7](article-l149-7.md)
- [Article L149-8](article-l149-8.md)
- [Article L149-9](article-l149-9.md)
- [Article L149-10](article-l149-10.md)
