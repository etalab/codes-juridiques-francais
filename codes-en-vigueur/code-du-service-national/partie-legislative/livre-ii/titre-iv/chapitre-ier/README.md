# Chapitre Ier : Dispositions générales

- [Section I : Dispositions pénales](section-i)
- [Section II : Dispositions disciplinaires et administratives.](section-ii)
