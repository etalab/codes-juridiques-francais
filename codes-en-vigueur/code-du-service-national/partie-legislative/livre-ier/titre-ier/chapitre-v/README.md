# Chapitre V : La période militaire d'initiation ou de perfectionnement à la défense nationale.

- [Article L115-1](article-l115-1.md)
- [Article L115-2](article-l115-2.md)
