# Chapitre II : Champ d'application.

- [Article L112-1](article-l112-1.md)
- [Article L112-2](article-l112-2.md)
- [Article L112-5](article-l112-5.md)
- [Article L112-6](article-l112-6.md)
