# Section 3 : Les relations entre la personne volontaire et la personne morale agréée.

- [Article L120-7](article-l120-7.md)
- [Article L120-8](article-l120-8.md)
- [Article L120-9](article-l120-9.md)
- [Article L120-10](article-l120-10.md)
- [Article L120-11](article-l120-11.md)
- [Article L120-12](article-l120-12.md)
- [Article L120-13](article-l120-13.md)
- [Article L120-14](article-l120-14.md)
- [Article L120-15](article-l120-15.md)
- [Article L120-16](article-l120-16.md)
- [Article L120-17](article-l120-17.md)
