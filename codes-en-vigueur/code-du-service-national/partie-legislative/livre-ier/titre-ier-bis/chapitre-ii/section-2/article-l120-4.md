# Article L120-4

La personne volontaire doit posséder la nationalité française, celle d'un Etat membre de l'Union européenne, celle d'un Etat partie à l'accord sur l'Espace économique européen ou justifier être en séjour régulier en France depuis plus d'un an sous couvert de l'un des titres de séjour prévus aux articles L. 313-8 et L. 313-9, aux 1°, 2° et 3° de l'article L. 313-10, aux 1° à 10° de l'article L. 313-11, ainsi qu'aux articles L. 314-8, L. 314-9 et L. 314-11 du code de l'entrée et du séjour des étrangers et du droit d'asile.

La condition de durée de résidence ne s'applique pas aux personnes étrangères volontaires lorsque des volontaires français sont affectés dans les pays dont ces personnes sont ressortissantes, sous réserve des dispositions régissant l'entrée et le séjour des étrangers en France.

Une visite médicale préalable à la souscription du contrat est obligatoire.
