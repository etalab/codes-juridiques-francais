# Article L120-3

Toute personne remplissant les conditions mentionnées à la section 2 du présent chapitre peut souscrire avec une personne morale agréée un contrat de service civique ou de volontariat associatif dans les conditions fixées au présent chapitre.
