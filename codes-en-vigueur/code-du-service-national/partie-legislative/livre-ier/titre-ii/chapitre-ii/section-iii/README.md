# Section III : Dispositions diverses.

- [Article L122-18](article-l122-18.md)
- [Article L122-19](article-l122-19.md)
- [Article L122-20](article-l122-20.md)
