# Article L529

L'Institution nationale des invalides est la maison des combattants âgés, malades ou blessés au service de la patrie.

Elle a pour mission :

1° D'accueillir dans un centre de pensionnaires, à titre permanent ou temporaire, les invalides bénéficiaires du code des pensions militaires d'invalidité et des victimes de la guerre satisfaisant aux conditions fixées par le décret visé à l'article L. 537 ;

2° De dispenser dans un centre médico-chirurgical des soins en hospitalisation ou en consultation en vue de la réadaptation fonctionnelle, professionnelle et sociale des patients ; les personnes accueillies sont en premier lieu les pensionnaires de l'établissement ainsi que les autres bénéficiaires du présent code :

en outre, elle    délivre aux assurés sociaux les soins définis à l' article L. 6111-1 du code de la santé publique  ;

3° De participer aux études et à la recherche sur l'appareillage des handicapés conduites par le ministre chargé des anciens combattants. Ces participations font l'objet d'une convention préalable entre l'Etat et l'établissement lorsqu'elles impliquent un engagement financier spécifique de la part de ce dernier.
