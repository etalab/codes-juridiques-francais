# Article L321

La gratuité du voyage est, en outre, accordée au guide de l'invalide à 100 % bénéficiaire de l'article 18.
