# Article L392

Les modalités d'application des sections 1 (Par. 3), 2, 3, 5, 6, 8 et 9, sont fixées aux articles D. 272 à D. 284 et R. 393, R. 394, R. 395 et R. 395-1.
