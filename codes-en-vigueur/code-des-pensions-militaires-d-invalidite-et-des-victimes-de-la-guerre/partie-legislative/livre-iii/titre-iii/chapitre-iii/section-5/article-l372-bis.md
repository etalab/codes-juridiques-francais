# Article L372 bis

A compter de la promulgation de la loi du 4 janvier 1951, il est ouvert un délai d'un an pour la présentation et pour le renouvellement des demandes qui, à cette date, n'ont pas encore donné lieu à une décision notifiée aux intéressés.

Si les bénéficiaires sont décédés, leurs ayants droit peuvent solliciter, dans le même délai, l'attribution de cette médaille à titre posthume.
