# Article L391

Il est créé un insigne qui est attribué à toute personne répondant aux conditions fixées par le chapitre IV du titre II.
