# Paragraphe 1 : Légion d'honneur.

- [Article L344](article-l344.md)
- [Article L345](article-l345.md)
- [Article L346](article-l346.md)
- [Article L347](article-l347.md)
