# Article L328

Des prêts à long terme peuvent être consentis par les caisses de crédit agricole mutuel aux anciens combattants, aux pensionnés militaires, aux victimes civiles de la guerre et aux pupilles de la Nation, dans les conditions fixées par la loi du 5 août 1920 et les textes qui l'ont modifiée, en vue de leur faciliter l'acquisition de propriétés rurales.
