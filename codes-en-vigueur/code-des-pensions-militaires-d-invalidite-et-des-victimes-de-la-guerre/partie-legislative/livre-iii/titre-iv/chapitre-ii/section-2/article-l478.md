# Article L478

Les dispositions concernant l'organisation de la tutelle ne sont appliquées aux enfants visés à l'article L. 464 que dans les limites où elles sont compatibles avec leur statut personnel.
