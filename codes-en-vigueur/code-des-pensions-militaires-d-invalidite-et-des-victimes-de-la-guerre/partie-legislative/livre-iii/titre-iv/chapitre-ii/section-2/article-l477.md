# Article L477

Le conseil de tutelle, sans jamais s'immiscer dans l'exercice de l'autorité parentale ou de la tutelle, s'assure que les sommes allouées par l'Etat et l'office au pupille sont bien employées à son entretien et à son éducation ou mises en réserve à son profit.

Il assiste le tuteur de son expérience, veille à ce que l'orphelin ne soit pas laissé à l'abandon, à ce qu'il fréquente régulièrement l'école ou l'atelier et soit mis en situation de gagner honorablement sa vie.

Le conseiller de tutelle propose à l'office national toutes mesures qu'il juge utiles dans l'intérêt de l'enfant.

L'office national peut relever de ses fonctions le conseiller de tutelle, soit sur sa propre demande, soit sur celle de la mère, du tuteur, d'un ascendant, du conseil de famille ou d'office.

Si le conseil de famille estime qu'il y ait lieu de nommer un nouveau conseiller de tutelle, la désignation ne peut avoir lieu que dans les conditions spécifiées à l'article L. 476.
