# Article L463

Le bénéfice du présent titre est étendu :

1° Aux orphelins dont le père, la mère ou le soutien de famille est mort de blessures reçues au cours d'opérations effectuées, sur un théâtre d'opérations, par les armées de terre, de mer ou de l'air, lorsque le caractère d'opérations de guerre a été reconnu par arrêtés conjoints des ministres intéressés et du ministre de l'économie et des finances ;

2° Aux enfants nés avant la fin des opérations visées à l'alinéa précédent, ou dans les trois cents jours qui auront suivi leur cessation, lorsque le père, la mère ou le soutien de famille se trouve, à raison de blessures reçues ou de maladies contractées au cours desdites opérations, dans l'incapacité de pourvoir à leurs obligations et à leurs charges de chef de famille.
