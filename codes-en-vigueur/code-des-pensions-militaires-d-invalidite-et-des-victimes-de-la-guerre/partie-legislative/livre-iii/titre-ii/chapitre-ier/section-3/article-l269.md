# Article L269

Les combattants volontaires de la Résistance bénéficient d'une bonification de dix jours pour engagement volontaire.
