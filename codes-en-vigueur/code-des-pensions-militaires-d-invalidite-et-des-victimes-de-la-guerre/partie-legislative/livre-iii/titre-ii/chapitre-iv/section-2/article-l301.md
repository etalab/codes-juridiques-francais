# Article L301

Les réfractaires et leurs ayants cause bénéficient des pensions d'invalidité et de décès prévues, pour les membres de la Résistance, au titre II du livre II, ou de celles prévues pour les victimes civiles de la guerre 1939-1945, au titre III du livre II.
