# Article L279

Les déportés et internés résistants et leurs ayants cause bénéficient de pensions d'invalidité ou de décès dans les conditions prévues par le titre II du livre II.
