# Article L253

Il est créé une carte de combattant qui est attribuée dans les conditions fixées aux articles R. 223 à R. 235.
