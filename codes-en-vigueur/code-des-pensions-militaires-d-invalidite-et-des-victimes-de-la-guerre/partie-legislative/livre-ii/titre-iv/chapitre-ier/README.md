# Chapitre Ier : Militaires ayant servi dans l'armée française au cours de la guerre 1870-1871.

- [Article L225](article-l225.md)
- [Article L226](article-l226.md)
