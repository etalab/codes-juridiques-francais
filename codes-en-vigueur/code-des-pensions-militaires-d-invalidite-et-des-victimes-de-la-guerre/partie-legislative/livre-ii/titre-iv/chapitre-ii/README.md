# Chapitre II : Militaires ayant servi dans l'armée allemande

- [Section 1 : De 1871 à 1914.](section-1)
- [Section 2 : Au cours de la guerre 1914-1918.](section-2-au)
- [Section 3 : Au cours de la guerre 1939-1945.](section-3-au)
- [Section 4 : Dispositions communes aux Alsaciens et Lorrains ayant servi dans l'armée allemande.](section-4)
