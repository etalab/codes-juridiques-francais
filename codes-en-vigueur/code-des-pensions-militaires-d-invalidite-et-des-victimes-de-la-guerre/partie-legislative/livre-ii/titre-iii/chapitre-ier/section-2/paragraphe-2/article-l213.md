# Article L213

Il appartient aux postulants de faire la preuve de leurs droits à pension en établissant notamment :

Pour les victimes elles-mêmes, que l'infirmité invoquée a bien son origine dans une blessure ou dans une maladie causée par l'un des faits définis aux paragraphes 1er et 2 de la section I ; pour les ayants cause, que le décès sur lequel ils fondent leur demande a été causé par l'un de ces mêmes faits.

Néanmoins, sont réputés causés par des faits de guerre, sauf preuve contraire, les décès, même par suite de maladie, s'ils sont survenus soit en France, soit à l'étranger, pendant la détention subie dans les conditions prévues à l'article L. 199.

Les déportés politiques bénéficient de la présomption d'origine pour les maladies, sans condition de délai.

Le bénéfice de la présomption d'origine, tel qu'il est défini à l'article L. 3, est reconnu aux personnes contraintes au travail en pays ennemi.

Le taux de la pension de conjoint survivant prévu au premier alinéa de l'article L. 51 est applicable, sans condition d'âge, d'invalidité ni de ressources, aux conjoints survivants des déportés politiques morts au cours de leur déportation.
