# Article L197

Peuvent également bénéficier des dispositions du présent chapitre :

1° Les Français ou ressortissants français qui, par suite d'un fait de guerre survenu sur le territoire français entre le 2 septembre 1939 et l'expiration d'un délai d'un an à compter du décret fixant la date légale de la cessation des hostilités, ont reçu une blessure, subi un accident ou contracté une maladie ayant entraîné une infirmité ;

2° Les Français ou ressortissants français qui, par suite d'un fait de guerre survenu à l'étranger, dans la période susvisée, ont reçu une blessure, subi un accident ou contracté une maladie ayant entraîné une infirmité, dans le cas où ils ne seraient pas couverts par les accords de réciprocité.
