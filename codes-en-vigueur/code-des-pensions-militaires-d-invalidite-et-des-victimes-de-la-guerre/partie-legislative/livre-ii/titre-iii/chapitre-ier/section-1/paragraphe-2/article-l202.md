# Article L202

Ne sont pas considérés comme faute inexcusable de la victime, au regard des personnes visées au présent paragraphe, le suicide, la tentative de suicide, la mutilation volontaire :

1° S'ils sont survenus à l'occasion ou sous la menace d'arrestation ou d'interrogatoire ou au cours d'une détention, dès lors que l'emprisonnement, l'arrestation ou l'interrogatoire, quelles qu'en soient la nature ou la qualification, auraient été ordonnés par l'ennemi ou par une autorité ou un organisme placé sous son contrôle, pour une cause autre qu'une infraction de droit commun ne pouvant bénéficier de l'ordonnance du 6 juillet 1943 ;

2° S'ils ont été accomplis pour se soustraire à l'obligation de travailler pour l'ennemi ou les autorités ou organismes placés sous son contrôle.
