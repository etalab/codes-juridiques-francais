# Article L246 bis

Les services qui ont été accomplis dans les troupes spéciales du Levant par les militaires de ces troupes, autorisés à servir dans une unité régulière de l'armée française, tout en conservant leur statut spécial, sont considérés comme accomplis dans l'armée française pour les droits à pension.
