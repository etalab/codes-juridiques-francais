# Livre II : Régimes rattachés au régime général et régimes spéciaux

- [Titre Ier : Régimes applicables à certains personnels militaires et assimilés](titre-ier)
- [Titre II : Membres des organisations civiles et militaires de la Résistance](titre-ii)
- [Titre III : Règles applicables aux victimes civiles](titre-iii)
- [Titre IV : Alsaciens et Lorrains](titre-iv)
- [Titre V : Militaires et assimilés originaires d'Algérie et des pays d'outre-mer](titre-v)
- [Titre VI : Etrangers.](titre-vi)
- [Titre VII : Admission de certains étrangers, ainsi que de certains Français victimes de circonstances particulières, au bénéfice des dispositions du présent code.](titre-vii)
