# Article L180

Néanmoins, la preuve contraire est considérée comme rapportée, nonobstant toute constatation, même officielle, lorsqu'il est établi médicalement qu'il est impossible que la maladie ou l'infirmité dont l'aggravation est invoquée ait pu être aggravée par les actes ouvrant droit au bénéfice du présent titre.

Dans tous les cas, la filiation entre la blessure et la maladie ayant fait l'objet de la constatation et l'infirmité invoquée doit être établie médicalement.

Lorsque le droit s'est ouvert au cours de la période prévue à l'alinéa premier de l'article L. 179, le délai imparti pour présenter la demande de pension court de la publication des ordonnances du 3 mars 1945, ou, le cas échéant, des autres faits mentionnés audit article.
