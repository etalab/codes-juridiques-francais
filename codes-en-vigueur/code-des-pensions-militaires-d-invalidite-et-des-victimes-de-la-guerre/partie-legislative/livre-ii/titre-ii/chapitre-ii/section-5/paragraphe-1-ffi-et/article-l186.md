# Article L186

Les militaires de carrière en activité, en congé ou en position de retraite, ayant contracté une infirmité soit dans une unité ou formation des FFI, soit pendant la période prévue à l'article L. 172 et dans l'accomplissement des actes ou dans les circonstances énumérées par ledit article, bénéficient des dispositions prévues aux articles 48 et 49 du Code des pensions civiles et militaires de retraite, et, le cas échéant, de l'article 51 dudit code (1).
