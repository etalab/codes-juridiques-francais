# Article L189-1

Une allocation spéciale est attribuée aux conjoints survivants des aveugles de la Résistance bénéficiaires des dispositions de l'article L. 189 lorsqu'elles justifient d'une durée de mariage sans séparation de corps ou de fait d'au moins quinze ans et ne peuvent prétendre à pension de conjoint survivant au titre du présent code.

Le montant de cette allocation est égal à celui de la majoration prévue au deuxième alinéa de l'article L. 52-2 en faveur des conjoints survivants de grands invalides relevant de l'article L. 18 et bénéficiaires de l'allocation spéciale n° 5 bis, b.

Les conjoints survivants remariés ou ayant conclu un nouveau pacte civil de solidarité ou vivant en état de concubinage notoire perdent leur droit à l'allocation spéciale.
