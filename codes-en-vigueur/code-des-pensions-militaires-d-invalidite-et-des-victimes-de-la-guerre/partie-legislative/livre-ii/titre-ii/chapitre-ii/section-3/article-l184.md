# Article L184

Lorsque le mari et la femme ont droit tous deux à l'application du présent titre, il n'est alloué de majorations pour les enfants que du fait d'un seul de leurs auteurs.
