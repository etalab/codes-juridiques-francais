# Article L147

Les anciens militaires ou marins titulaires de pensions concédées pour blessures reçues ou pour infirmités contractées en service antérieurement au 2 août 1914 reçoivent le bénéfice des taux de pension figurant aux tableaux annexés au livre Ier du présent code, ainsi que le bénéfice des articles L. 18 et L. 19.

Ces dispositions sont applicables à tous autres titulaires de pensions militaires pour invalidité, concédées dans les conditions prévues par les lois des 11 et 18 avril 1831.
