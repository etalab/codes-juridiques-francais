# Article L170

Les pensions accordées par application du présent chapitre ne sont pas cumulables avec les rentes, indemnités ou autres prestations qui peuvent être allouées au titre des mêmes infirmités ou du décès par application d'une autre loi et notamment de la législation des accidents du travail ou de celle des assurances sociales.
