# Article L138

Le droit à pension d'invalidité des militaires de carrière et de leurs ayants cause est fixé dans les conditions prévues aux articles L. 34 à L. 37 et L. 49 du Code des pensions civiles et militaires de retraite.
