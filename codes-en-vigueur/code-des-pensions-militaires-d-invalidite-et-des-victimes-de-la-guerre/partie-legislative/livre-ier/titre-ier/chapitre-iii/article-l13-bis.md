# Article L13 bis

Les victimes civiles de la guerre et les invalides militaires "hors guerre" bénéficient, comme les victimes militaires de guerre, du barème le plus avantageux prévu par les articles L. 12 et L. 13 ci-dessus.
