# Article L3

Lorsqu'il n'est pas possible d'administrer ni la preuve que l'infirmité ou l'aggravation résulte d'une des causes prévues à l'article L. 2, ni la preuve contraire, la présomption d'imputabilité au service bénéficie à l'intéressé à condition :

1° S'il s'agit de blessure, qu'elle ait été constatée avant le renvoi du militaire dans ses foyers ;

2° S'il s'agit d'une maladie, qu'elle n'ait été constatée qu'après le quatre-vingt-dixième jour de service effectif et avant le soixantième jour suivant le retour du militaire dans ses foyers ;

3° En tout état de cause, que soit établie, médicalement, la filiation entre la blessure ou la maladie ayant fait l'objet de la constatation et l'infirmité invoquée.

En cas d'interruption de service d'une durée supérieure à quatre-vingt-dix jours, la présomption ne joue qu'après le quatre-vingt-dixième jour suivant la reprise du service actif.

La présomption définie au présent article s'applique exclusivement aux constatations faites, soit pendant le service accompli au cours de la guerre 1939-1945, soit au cours d'une expédition déclarée campagne de guerre, soit pendant le service accompli par les militaires pendant la durée légale, compte tenu des délais prévus aux précédents alinéas.

Toutefois, la présomption bénéficie aux prisonniers de guerre et internés à l'étranger, à condition que leurs blessures ou maladies aient été régulièrement constatées :

Soit dans les six mois suivant leur arrivée, s'il s'agit de prisonniers rentrés en France avant le 1er mars 1945, date de mise en application de l'ordonnance n° 45-802 du 20 avril 1945, instituant le contrôle médical des prisonniers, travailleurs et déportés ;

Soit, au plus tard, lors de la deuxième visite médicale prévue par l'ordonnance n° 45-802 du 20 avril 1945, sans que ce délai puisse excéder sept mois après le retour en France, s'il s'agit de prisonniers rapatriés après le 28 février 1945.

L'expiration du délai est reportée au 30 juin 1946 dans tous les cas où l'application des dispositions de l'alinéa précédent conduirait à la fixer à une date antérieure.

Un dossier médical doit être constitué pour chaque recrue lors de son examen par le conseil de révision et lors de son incorporation dans les conditions déterminées par décret.
