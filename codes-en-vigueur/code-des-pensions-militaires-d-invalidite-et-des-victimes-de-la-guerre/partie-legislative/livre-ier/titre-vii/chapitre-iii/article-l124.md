# Article L124

La pension définitive ou temporaire, allouée pour cause d'aliénation mentale à un militaire ou marin interné dans un établissement public d'aliénés ou dans un établissement privé faisant fonction d'asile public, est employée, à due concurrence, à acquitter les frais d'hospitalisation.

Toutefois, en cas d'existence de conjoint ou d'enfants et d'ascendants, l'administrateur des biens de l'aliéné ou son tuteur verse, dans les quinze premiers jours de chaque trimestre :

a) Au conjoint ou au représentant légal des enfants, les majorations d'enfants et une somme égale à une pension de veuve du taux normal ;

b) Aux ascendants des aliénés remplissant les conditions prévues au titre IV, une somme égale à la pension prévue à l'article L. 72.

Lorsque les arrérages de la pension allouée à l'interné dont l'aliénation est la conséquence des troubles psychiques ayant ouvert droit à pension se trouvent insuffisants pour permettre à l'administrateur des biens de l'aliéné ou à son tuteur d'effectuer ledit versement, le complément est à la charge de l'Etat.
