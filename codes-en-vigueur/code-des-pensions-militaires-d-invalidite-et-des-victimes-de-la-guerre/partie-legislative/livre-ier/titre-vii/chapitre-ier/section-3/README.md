# Section 3 : Dispositions générales.

- [Article L120](article-l120.md)
- [Article L122](article-l122.md)
- [Article L123](article-l123.md)
