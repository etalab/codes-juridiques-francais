# Article L129

Les appareils nécessaires aux mutilés sont fabriqués soit par les ateliers des centres d'appareillage, soit par l'industrie privée, conformément au cahier des charges.
