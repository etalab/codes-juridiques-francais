# Article L109 bis

Les articles L. 91 à L. 93 du code des pensions civiles et militaires de retraite sont applicables aux pensions servies au titre du présent code.
