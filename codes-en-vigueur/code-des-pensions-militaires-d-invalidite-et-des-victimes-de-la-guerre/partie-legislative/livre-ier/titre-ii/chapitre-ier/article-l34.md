# Article L34

Une allocation aux grands invalides portant le n° 4 bis est attribuée aux grands invalides non bénéficiaires des articles L. 16 ou L. 18, titulaires d'une pension de 95 % ou de 100 % pour plusieurs infirmités dont la plus grave entraîne une invalidité au moins égale à 85 %.

Le taux est fixé ainsi qu'il suit, en fonction de la somme arithmétique des pourcentages d'invalidité attribuables aux infirmités dont l'intéressé est atteint et qui lui ouvrent droit à pension et sans qu'il soit fait application des dispositions prévues par le troisième alinéa de l'article L. 14 :

1° Si la somme des pourcentages d'invalidité est fixée entre 105 et 145 % : 46 points ;

2° Si la somme des pourcentages d'invalidité est fixée entre 150 et 195 % : 92 points ;

3° Si la somme des pourcentages d'invalidité est fixée entre 200 et 245 % : 184 points ;

4° Si la somme des pourcentages d'invalidité est fixée entre 250 et 295 % : 276 points ;

5° Si la somme des pourcentages d'invalidité est fixée entre 300 et 345 % : 368 points ;

6° Si la somme des pourcentages d'invalidité est fixée à 350 % et au-dessus : 460 points.

Lorsque la somme des pourcentages ci-dessus prévus se termine par un chiffre autre qu'un 0 ou un 5, elle est portée au multiple de 5 supérieur.

L'allocation n° 4 bis ne se cumule pas avec les allocations n° 5, 5 bis, 6 ou 8.
