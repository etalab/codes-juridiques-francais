# Article L74

A défaut du père et de la mère, la pension est accordée aux grands-parents dans les conditions prévues à l'article L. 67. Elle est la même que pour les parents.

Chaque grand-parent ou chaque couple de grands-parents ne peut recevoir qu'une seule pension.

La pension est augmentée pour chaque petit enfant décédé, à concurrence de trois, à partir du second inclusivement, par application de l'indice de pension 45, tel qu'il est défini à l'article L. 8 bis du code.
