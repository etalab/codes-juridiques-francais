# Article L76

Les dispositions de l'article L. 75 ont effet à dater du 2 octobre 1941.

Elles s'appliquent nonobstant toutes décisions antérieures de rejet fondées sur des causes d'exclusion qu'elles n'ont pas maintenues.

Lorsque, en raison des dispositions de la loi du 9 septembre 1941, aucune demande n'a encore été présentée, les intéressés seront réputés, pour la détermination du point de départ des arrérages, s'être mis en instance de pension dans le même délai, à compter de l'époque où leurs droits sont ouverts, que celui dans lequel leur demande aura été formulée après la publication de l'ordonnance du 23 août 1945.
