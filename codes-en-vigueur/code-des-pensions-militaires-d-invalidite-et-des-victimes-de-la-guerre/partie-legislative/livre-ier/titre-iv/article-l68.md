# Article L68

Les ascendants de nationalité étrangère, lorsqu'un ou plusieurs de leurs enfants incorporés dans l'armée française sont décédés ou disparus dans les conditions de nature à ouvrir à pension de veuve, sont admis au bénéfice des pensions prévues aux articles L. 67 et L. 77 à condition :

1° Qu'ils résident en France si, lors du fait dommageable, la nation de laquelle ils étaient ressortissants était en guerre avec la France ;

2° Qu'ils ne soient pas bénéficiaires d'une allocation d'ascendant servie par un gouvernement étranger.

Les dispositions de l'alinéa 1° qui précède ont effet :

a) A compter du 2 septembre 1939 pour les décès imputables à la guerre commencée à cette date ;

b) A compter du 3 septembre 1943 pour les décès consécutifs à des événements antérieurs au 2 septembre 1939.

Les ascendants étrangers dont une précédente demande a été rejetée sous le régime de la loi du 28 juillet 1921 modifiée par la loi du 9 décembre 1927 peuvent à nouveau se mettre en instance de pension sans limitation de délai.
