# Article L72

I. La pension est déterminée pour le père ou la mère veufs, divorcés, séparés de corps ou non mariés, de même que pour le père et la mère conjointement, par application de l'indice de pension 213, tel qu'il est défini à l'article L. 8 bis du présent code ; pour le père ou la mère veufs remariés ou qui ont contracté mariage depuis le décès du militaire ou marin, par application de l'indice de pension 106,5 ; en cas de dissolution de ce dernier mariage par veuvage, divorce ou en cas de séparation de corps, la pension est à nouveau déterminée par application de l'indice 213.

II. Les indices de pension 213 et 106,5 visés au paragraphe I sont respectivement majorés de 30 et 15 points en faveur des ascendants âgés :

Soit de soixante-cinq ans ;

Soit de soixante ans s'ils sont infirmes ou atteints d'une maladie incurable ou entraînant une incapacité permanente de travail.

Les conjoints survivants bénéficiaires de la pension au taux exceptionnel prévu à l'article L. 51, 1er alinéa, perçoivent, lorsqu'ils sont admis au bénéfice d'une pension d'ascendant majorée dans les conditions prévues par le présent paragraphe, une allocation complémentaire dont le taux est fixé à 170 points. Cette allocation est soumise aux mêmes conditions de ressources que la pension d'ascendant.
