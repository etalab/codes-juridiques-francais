# Article L73

Si le père ou la mère ont perdu plusieurs enfants des suites de blessures reçues ou de maladies contractées ou aggravées sous les drapeaux, il est alloué une majoration de pension déterminée par application de l'indice de pension 45 tel qu'il est défini à l'article L. 8 bis du code, pour chaque enfant décédé à partir du second inclusivement.
