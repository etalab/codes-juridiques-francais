# Article L492 ter

Le ministre compétent peut décider que la mention "Mort pour le service de la Nation" est portée sur l'acte de décès :

1° D'un militaire tué en service ou en raison de sa qualité de militaire ;

2° D'un autre agent public tué en raison de ses fonctions ou de sa qualité.

Lorsque, pour un motif quelconque, la mention "Mort pour le service de la Nation" n'a pu être inscrite sur l'acte de décès au moment de la rédaction de celui-ci, elle est ajoutée ultérieurement dès que les éléments nécessaires de justification le permettent.

Lorsque la mention "Mort pour le service de la Nation" a été portée sur son acte de décès dans les conditions prévues au présent article, l'inscription du nom du défunt sur un monument de sa commune de naissance ou de dernière domiciliation est obligatoire.

La demande d'inscription est adressée au maire de la commune choisie par la famille ou, à défaut, par les autorités civiles ou militaires, les élus nationaux, les élus locaux, l'Office national des anciens combattants et victimes de guerre par l'intermédiaire de ses services départementaux ou les associations ayant intérêt à agir.

Les enfants des personnes dont l'acte de décès porte la mention "Mort pour le service de la Nation" ont vocation à la qualité de pupille de la Nation.
