# Article L513

Les dispositions du présent chapitre sont applicables aux tombes des personnes civiles, décédées en France ou hors de France, entre le 2 septembre 1939 et la date légale de cessation des hostilités, lorsque la mort est la conséquence directe d'un acte accompli volontairement pour lutter contre l'ennemi et que la mention "Mort pour la France" a été inscrite sur l'acte de décès.
