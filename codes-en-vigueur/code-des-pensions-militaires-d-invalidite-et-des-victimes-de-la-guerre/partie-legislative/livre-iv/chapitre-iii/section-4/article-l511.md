# Article L511

Si la création d'un cimetière réservé à l'inhumation des militaires des armées alliées est demandée par le haut commandement desdites armées, son établissement est assuré dans les conditions prévues aux articles L. 499 à L. 502 par le            ministre chargé des anciens combattants et victimes de guerre, en accord avec le commandant en chef de l'armée intéressée ou son représentant.
