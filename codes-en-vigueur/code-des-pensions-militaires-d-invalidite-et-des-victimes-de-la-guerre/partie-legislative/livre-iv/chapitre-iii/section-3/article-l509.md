# Article L509

A défaut d'accord amiable entre l'Etat et les communes, l'indemnité est fixée, sur les bases qui précèdent, par une commission spéciale d'arbitres instituée dans chaque département comprenant :

1° Le président du tribunal de grande instance ou son délégué, président ;

2° Deux délégués de l'administration des contributions directes ou de l'enregistrement et un suppléant nommés par le préfet sur la proposition des directeurs intéressés ;

3° Deux représentants des communes et un suppléant nommés par le préfet.

La commission statue après avoir entendu, s'ils le demandent, le représentant du            ministre chargé des anciens combattants et victimes de guerre et le représentant de la commune intéressée dûment convoqués.
