# Article A16

Tout bénéficiaire de l'article L. 115, régulièrement hospitalisé au titre dudit article a droit au transport gratuit, conformément aux dispositions de l'article 78.

En dehors du cas où l'hospitalisé est bénéficiaire de l'article L. 18, cas dans lequel la tierce personne qui l'accompagne voyage de droit gratuitement, la gratuité du transport peut encore être accordée aux convoyeurs indispensables, après autorisation spéciale du directeur interdépartemental prise sur avis motivé du médecin contrôleur des soins gratuits ayant connu de la demande d'hospitalisation.
