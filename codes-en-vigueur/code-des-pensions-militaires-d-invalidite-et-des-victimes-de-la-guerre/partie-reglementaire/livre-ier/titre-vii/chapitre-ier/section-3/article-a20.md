# Article A20

En cas de déplacement par voie ferrée, la somme à mandater est le prix du billet de 2e classe, déduction faite, le cas échéant, de la réduction dont l'intéressé peut bénéficier à titre personnel.

En cas de déplacement par voie de terre la somme à mandater est décomptée d'après le tarif des voitures publiques ; si le pensionné n'a pas utilisé un service régulier de voitures publiques, la somme à mandater pour la location d'une voiture particulière est décomptée d'après le tarif fixé par arrêté interministériel ou par arrêté préfectoral.
