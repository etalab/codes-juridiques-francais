# Section 9 : Indemnités diverses.

- [Article A38](article-a38.md)
- [Article A39](article-a39.md)
- [Article A40](article-a40.md)
- [Article A41](article-a41.md)
