# Article A27

Lorsque le directeur interdépartemental des anciens combattants et victimes de guerre estime que les frais de transport de corps ne doivent pas être mis à la charge de l'Etat au titre de l'article L. 115, il en avise immédiatement la famille et le directeur ou gestionnaire de l'établissement.
