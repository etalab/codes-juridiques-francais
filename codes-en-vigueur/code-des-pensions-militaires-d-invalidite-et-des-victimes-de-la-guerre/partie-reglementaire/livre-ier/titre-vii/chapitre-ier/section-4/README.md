# Section 4 : Transfert des corps des pensionnés décédés dans un établissement hospitalier.

- [Article A21](article-a21.md)
- [Article A22](article-a22.md)
- [Article A23](article-a23.md)
- [Article A24](article-a24.md)
- [Article A25](article-a25.md)
- [Article A26](article-a26.md)
- [Article A27](article-a27.md)
