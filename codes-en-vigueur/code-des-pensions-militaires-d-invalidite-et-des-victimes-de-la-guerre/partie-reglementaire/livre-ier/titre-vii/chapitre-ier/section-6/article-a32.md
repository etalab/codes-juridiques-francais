# Article A32

Sous réserve que les prescriptions relatives à leur prise en charge prévues par l'article D. 60 soient respectées, les examens de laboratoire et analyses médicales pouvant être prescrits et pris en charge au titre de l'article L. 115 sont ceux figurant au titre VI du tarif interministériel pour le règlement de certaines prestations sanitaires, institué par l'arrêté du 30 décembre 1949 modifié.
