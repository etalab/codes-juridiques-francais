# Article A33

Les articles de pansements pouvant être prescrits, délivrés et réglés au titre de l'article L. 115 sont des articles de pansements figurant aux chapitres 2 (articles de pansements stériles) et 3 (articles de pansements non stériles) du titre III du tarif interministériel pour le règlement de certaines prestations sanitaires.
