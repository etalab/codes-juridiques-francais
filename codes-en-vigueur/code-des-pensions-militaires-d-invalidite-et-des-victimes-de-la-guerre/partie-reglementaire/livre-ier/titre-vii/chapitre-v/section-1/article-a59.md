# Article A59

Lorsque les placements pratiqués au titre de l'article A. 58 le justifient, l'office départemental peut créer un "comité de patronage" composé en majorité d'employeurs et de membres d'associations de pensionnés et victimes de guerre. Ce comité de patronage doit  guider et conseiller les bénéficiaires de la rééducation, surveiller les progrès de leur rééducation et leur procurer l'appui et le secours moral dont ils peuvent avoir besoin.
