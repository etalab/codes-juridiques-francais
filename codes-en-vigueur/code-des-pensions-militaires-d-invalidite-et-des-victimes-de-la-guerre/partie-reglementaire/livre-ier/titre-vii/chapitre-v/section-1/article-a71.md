# Article A71

Les offices départementaux prennent toutes dispositions nécessaires pour faire assurer par les employeurs l'application, aux bénéficiaires de la rééducation, des lois sur les accidents du travail et sur la sécurité sociale.
