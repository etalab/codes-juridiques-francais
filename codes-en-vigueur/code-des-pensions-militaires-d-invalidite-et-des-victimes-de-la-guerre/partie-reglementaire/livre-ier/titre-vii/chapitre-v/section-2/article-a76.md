# Article A76

Le contrat d'apprentissage concernant les bénéficiaires de la rééducation définis à l'article A. 56 contient ;

1° Les nom, prénoms, âge, profession et domicile de l'employeur ;

2° Les nom, prénoms, âge et domicile de l'apprenti ;

3° La justification de sa qualité d'invalide, de veuve pensionnée ou d'ascendant. Cette justification est faite pour les invalides à l'aide d'une copie du titre de pension ou, à défaut, d'une copie du certificat modèle 15 ; pour les veuves et les ascendants, à l'aide d'une copie du titre de pension ou, à défaut, du titre d'allocation provisoire d'attente ou d'un extrait de l'acte de décès du militaire portant la mention "Mort pour la France" ;

4° La date et la durée du contrat. Cette durée ne peut, en principe, être inférieure à six mois, ni supérieure à deux ans, sauf dérogation consentie par l'office national ;

5° Les conditions de logement, de nourriture, de prix et toutes autres arrêtées entre les parties ;

6° L'engagement pris par l'employeur de traiter l'apprenti avec les égards dus à une victime de la guerre.
