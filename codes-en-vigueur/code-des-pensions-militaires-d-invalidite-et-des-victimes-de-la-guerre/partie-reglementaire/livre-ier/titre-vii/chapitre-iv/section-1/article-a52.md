# Article A52

La commission se réunit sur convocation de son président.

Elle ne peut valablement délibérer qu'en présence de la moitié plus un des membres dont elle est composée.
