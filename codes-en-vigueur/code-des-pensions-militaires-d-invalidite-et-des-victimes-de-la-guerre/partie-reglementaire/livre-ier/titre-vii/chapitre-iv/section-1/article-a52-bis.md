# Article A52 bis

Le secrétariat de la commission est assuré par un fonctionnaire du ministère des anciens combattants et des victimes de la guerre.
