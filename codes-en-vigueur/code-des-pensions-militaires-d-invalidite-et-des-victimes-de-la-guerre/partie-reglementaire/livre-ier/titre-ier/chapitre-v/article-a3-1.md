# Article A3-1

La délégation visée à l'article 1er ne concerne pas l'exécution des décisions de justice, qui continue à être assurée par l'administration centrale.
