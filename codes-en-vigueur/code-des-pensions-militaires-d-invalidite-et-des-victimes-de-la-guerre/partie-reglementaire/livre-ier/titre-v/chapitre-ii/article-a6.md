# Article A6

Les demandes de lettres de service accréditant les intendants militaires près les tribunaux ou les cours régionales des pensions sont adressées par les intendants militaires, commissaires du Gouvernement près les cours régionales des pensions, au       ministre chargé des anciens combattants et victimes de guerre (direction du contentieux, de l'état civil et des recherches, sous-direction du contentieux, bureau des appels) qui transmet pour décision au ministre de la défense nationale.
