# Article A174

Des réductions ou exemptions de redevances sont accordées aux pensionnés de guerre, suivant les conditions définies dans les textes ci-dessous rappelés :

a) Redevance pour droit d'usage de postes récepteurs de radiodiffusion (décret du 27 février 1940, art. 2) ;

b) Redevance d'abonnement et taxes de communications téléphoniques (art. 15 du décret du 15 septembre 1948).
