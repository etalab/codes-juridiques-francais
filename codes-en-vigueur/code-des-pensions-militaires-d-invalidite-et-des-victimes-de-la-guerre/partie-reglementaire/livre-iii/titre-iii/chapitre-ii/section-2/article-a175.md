# Article A175

Pour les infirmités n'ouvrant pas droit à l'article L. 115, les pensionnés au titre du présent code, assurés sociaux, sont dispensés à titre personnel de la participation aux frais médicaux, pharmaceutiques et autres mis à la charge des assurés malades ou invalides.

Ils peuvent prétendre au bénéfice de l'assurance invalidité si leur état d'invalidité a subi une aggravation non susceptible d'être indemnisée au titre du présent code et si le degré total d'incapacité est de deux tiers au moins (art. 81 et 82 de l'ordonnance du 19 octobre 1945).
