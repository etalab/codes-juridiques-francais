# Paragraphe 3 : Dispositions générales.

- [Article A172-11](article-a172-11.md)
- [Article A172-12](article-a172-12.md)
- [Article A172-13](article-a172-13.md)
