# Chapitre III : Décorations et insignes.

- [Section 1 : Légion d'honneur et médaille militaire.](section-1)
- [Section 2 : Médaille des prisonniers civils, déportés et otages de la guerre 1914-1918.](section-2)
- [Section 3 : Médaille de la déportation et de l'internement pour faits de résistance.](section-3)
