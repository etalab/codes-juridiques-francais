# Chapitre Ier : Droits attachés à la qualité de pensionné.

- [Section 1 : Réduction sur les chemins de fer.](section-1)
- [Section 2 : Cartes de priorité.](section-2)
