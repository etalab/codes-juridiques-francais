# Article A195

Pour suivre le recouvrement des produits, la liquidation des dépenses et la conservation du patrimoine des pupilles, l'agent comptable et le préfet tiennent, chacun de leur côté, les livres suivants :

1° Un carnet des droits et produits constatés et des dépenses ;

2° Un livre des comptes individuels des pupilles ;

3° Un registre de leurs biens.
