# Chapitre IV : Statut des réfractaires.

- [Article A166](article-a166.md)
- [Article A167](article-a167.md)
- [Article A168](article-a168.md)
