# Paragraphe 3 bis : Marins du commerce.

- [Article A123-6](article-a123-6.md)
- [Article A123-7](article-a123-7.md)
- [Article A123-8](article-a123-8.md)
- [Article A123-9](article-a123-9.md)
