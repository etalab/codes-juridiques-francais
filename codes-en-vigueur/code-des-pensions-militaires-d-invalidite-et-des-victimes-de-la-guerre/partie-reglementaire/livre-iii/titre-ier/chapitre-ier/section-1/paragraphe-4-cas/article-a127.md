# Article A127

La carte de combattant est refusée ou retirée à toute personne visée à l'article R. 228.
