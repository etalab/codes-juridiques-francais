# Article A152

La veuve non séparée de corps peut obtenir le payement des arrérages de la retraite qui ont couru jusqu'au décès de son mari dans les conditions prévues par l'article 18 de la loi du 12 avril 1922.
