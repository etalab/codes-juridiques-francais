# Section 3 : Dispositions diverses.

- [Article A155](article-a155.md)
- [Article A156](article-a156.md)
- [Article A157](article-a157.md)
