# Article A328

Les candidats pensionnaires, en adressant leur demande et leur dossier au général commandant l'institution nationale des invalides, doivent informer celui-ci de leur intention d'être hébergés en attendant la décision du       ministre chargé des anciens combattants et victimes de guerre.

Ils ne peuvent se rendre à l'institution qu'après qu'ils ont été acceptés par le général commandant.

Jusqu'à leur admission ils sont soumis au régime des hébergés.

Les hébergés rejoignant l'institution nationale des invalides voyagent à leurs frais.

Ils ne doivent être atteints d'aucune maladie contagieuse.

En vue du calcul de la redevance journalière à l'institution, ils déposent au bureau de l'agent comptable, à leur arrivée, leurs titres de pension et les carnets d'allocations spéciales.

S'ils ont des charges de famille, ils en justifient par des certificats de vie pour la femme et les enfants et par un certificat de non-imposition sur le revenu par ascendant âgé de plus de soixante ans.
