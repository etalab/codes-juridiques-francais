# Article A324

Le personnel civil peut, moyennant remboursement, être autorisé à prendre ses repas à l'institution nationale.
