# Article A320

L'invalide en congé qui demande à réintégrer l'institution nationale des invalides doit joindre à sa demande un certificat de bonne vie et moeurs.
