# Article A287

Toutes les pièces justificatives du compte sont visées par le directeur de l'établissement et certifiées par l'ordonnateur.
