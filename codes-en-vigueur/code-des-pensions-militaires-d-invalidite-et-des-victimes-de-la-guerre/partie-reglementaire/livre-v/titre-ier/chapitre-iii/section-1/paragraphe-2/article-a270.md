# Article A270

La comptabilité en deniers comprend, en dépenses, le payement, par le régisseur, des menues dépenses nécessitées par le fonctionnement normal de l'établissement.

Il est interdit d'affecter à leur payement les recettes visées au 1° de l'article A. 268.

Le régisseur économe acquitte lesdites dépenses et doit présenter au comptable de l'office de rattachement, pour obtenir une nouvelle avance, des bordereaux justificatifs appuyés des factures et acquis réels des créanciers.

Ces bordereaux, certifiés par lui et approuvés après vérification par le directeur de l'établissement, sont produits en triple exemplaire.

Une nouvelle avance ne peut être consentie au régisseur économe pour le payement des menues dépenses, aussi longtemps que la précédente n'est pas complètement apurée.
