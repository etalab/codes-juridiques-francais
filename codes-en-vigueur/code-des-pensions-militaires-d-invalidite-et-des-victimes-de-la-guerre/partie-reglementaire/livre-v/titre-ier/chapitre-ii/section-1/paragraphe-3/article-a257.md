# Article A257

Le trésorier-payeur général tient un livre des fonds de l'office destiné à permettre de suivre la situation des fonds susceptibles d'être employés pour l'acquittement des dépenses.
