# Paragraphe 4 : Ecritures et comptes de l'agent comptable.

- [Article A238](article-a238.md)
- [Article A239](article-a239.md)
- [Article A240](article-a240.md)
- [Article A241](article-a241.md)
- [Article A242](article-a242.md)
- [Article A243](article-a243.md)
