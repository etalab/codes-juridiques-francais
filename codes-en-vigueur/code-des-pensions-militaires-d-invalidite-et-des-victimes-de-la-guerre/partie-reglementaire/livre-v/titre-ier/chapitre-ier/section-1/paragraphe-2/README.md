# Paragraphe 2 : Des recettes et des dépenses.

- [Article A227](article-a227.md)
- [Article A228](article-a228.md)
- [Article A229](article-a229.md)
- [Article A230](article-a230.md)
- [Article A231](article-a231.md)
- [Article A232](article-a232.md)
- [Article A233](article-a233.md)
- [Article A234](article-a234.md)
- [Article A234-1](article-a234-1.md)
- [Article A235](article-a235.md)
- [Article A236](article-a236.md)
