# Article A248

Le président de séance est élu par la commission. Sa voix est prépondérante.
