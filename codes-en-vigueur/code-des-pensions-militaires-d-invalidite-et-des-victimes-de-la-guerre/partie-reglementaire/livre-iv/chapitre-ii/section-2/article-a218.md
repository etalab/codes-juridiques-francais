# Article A218

Il n'est dû qu'une seule indemnité par exhumation quel que soit le nombre de délégués accrédités qui pourraient y assister.L'indemnité est à payer au délégué convoqué par le représentant du       ministre chargé des anciens combattants et victimes de guerre.
