# Article A206

Les dispositions du chapitre II du présent livre (3e partie) sont applicables sous les réserves ci-après, à la restitution à leurs familles, des corps des anciens combattants et victimes de la guerre, énumérés aux articles D. 402 et D. 403 et décédés hors de leur résidence habituelle entre le 2 septembre 1939 et la date légale de cessation des hostilités lorsque le transfert demandé est en provenance ou à destination d'un département d'outre-mer ou d'un territoire relevant du ministère chargé de la France d'outre-mer ou de Chine.
