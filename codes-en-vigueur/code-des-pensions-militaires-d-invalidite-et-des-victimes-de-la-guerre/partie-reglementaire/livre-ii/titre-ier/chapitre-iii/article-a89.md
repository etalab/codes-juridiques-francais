# Article A89

L'enquête administrative prévue à l'article A. 87 porte :

a) Sur les conditions d'engagement rendant l'intéressé apte à bénéficier du présent chapitre (2e partie).

Ces conditions sont vérifiées, soit par la présentation de la lettre de service instituée par le décret du 30 janvier 1939, article 1er, soit à défaut de celle-ci par une attestation de la direction départementale de la défense passive, certifiant qu'au moment du fait invoqué l'intéressé servait à la défense passive et précisant en quelle qualité ;

b) Sur les circonstances du fait de service invoqué.

Doivent être précisées les circonstances de temps et de lieu de la blessure, ou les commémoratifs de la maladie, la nature du service accompli à ce moment et, chaque fois que la chose est possible, l'identité des témoins ;

c) Sur les antécédents médicaux de l'intéressé compte tenu des constatations médicales faites lors de la réquisition ou de l'engagement volontaire de l'intéressé.
