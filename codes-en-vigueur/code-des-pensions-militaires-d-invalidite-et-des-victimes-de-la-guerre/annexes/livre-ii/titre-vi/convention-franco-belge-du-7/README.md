# Convention franco-belge du 7 novembre 1929 (Ratifiée le 24 novembre 1932 en exécution de la loi du 25 octobre 1932 et promulguée par décret du 20 janvier 1933).

- [Article Annexe 1, art. 1](article-annexe-1-art-1.md)
- [Article Annexe 1, art. 2](article-annexe-1-art-2.md)
- [Article Annexe 1, art. 3](article-annexe-1-art-3.md)
- [Article Annexe 1, art. 4](article-annexe-1-art-4.md)
