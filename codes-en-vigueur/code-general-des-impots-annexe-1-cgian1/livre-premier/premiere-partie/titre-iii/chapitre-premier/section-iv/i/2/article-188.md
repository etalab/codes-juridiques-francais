# Article 188

Sauf dérogation accordée par le directeur régional des douanes et droits indirects aux conditions qu'il détermine, les alcools dénaturés par un procédé spécial doivent être utilisés au lieu même de leur dénaturation à la fabrication de produits achevés, industriels et marchands, reconnus tels à dire d'experts en cas de contestation entre le fabricant et l'administration.
