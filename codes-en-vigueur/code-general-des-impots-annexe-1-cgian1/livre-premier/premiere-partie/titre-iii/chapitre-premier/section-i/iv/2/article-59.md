# Article 59

Avant toute utilisation d'un appareil ou récipient affecté à la production ou au logement d'alcools ou de boissons passibles de droits indirects, l'exploitant doit remettre au service des douanes et droits indirects le certificat de jaugeage de ce récipient établi par le service des instruments de mesure. Ce certificat doit être renouvelé à l'expiration de sa période de validité.

En cas de déformation, de modification ou de réparation susceptibles de changer la capacité d'un récipient, l'utilisation en est provisoirement interdite. Celle-ci ne peut être reprise qu'après établissement et remise d'un nouveau certificat de jaugeage.

Tous les récipients, y compris ceux dont la contenance est inférieure à dix hectolitres, doivent être pourvus, aux frais de l'exploitant, d'un dispositif de jaugeage agréé par l'administration. Ces récipients doivent, de plus, permettre le prélèvement d'un échantillon moyen sur toute la hauteur du liquide qu'ils contiennent.
