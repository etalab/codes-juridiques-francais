# Article L351-4

Le Conseil des prélèvements obligatoires est présidé par le Premier président de la Cour des comptes. Celui-ci peut se faire représenter par un président de chambre. En cas de partage égal des voix, il a voix prépondérante.
