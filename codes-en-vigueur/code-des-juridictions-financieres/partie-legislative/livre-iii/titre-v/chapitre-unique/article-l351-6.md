# Article L351-6

Les membres du Conseil des prélèvements obligatoires autres que son président sont désignés pour deux ans et leur mandat peut être renouvelé une fois. Cependant, à titre exceptionnel, huit des seize membres désignés en 2005, tirés au sort dans les deux mois suivant la nomination de tous les membres, le sont pour une période de quatre ans et leur mandat peut être renouvelé une fois pour une période de deux ans.

En cas de vacance, pour quelque cause que ce soit, d'un siège autre que celui du président, il est procédé à son remplacement pour la durée restant à courir du mandat. Un mandat exercé pendant moins d'un an n'est pas pris en compte pour l'application de la règle de renouvellement fixée à l'alinéa précédent.
