# Article L351-1

Il est institué un Conseil des prélèvements obligatoires, placé auprès de la Cour des comptes et chargé d'apprécier l'évolution et l'impact économique, social et budgétaire de l'ensemble des prélèvements obligatoires, ainsi que de formuler des recommandations sur toute question relative aux prélèvements obligatoires.
