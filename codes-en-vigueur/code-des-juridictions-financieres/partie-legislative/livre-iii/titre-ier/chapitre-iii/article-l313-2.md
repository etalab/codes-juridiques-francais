# Article L313-2

Toute personne visée à l'article L. 312-1 qui, pour dissimuler un dépassement de crédit, aura imputé ou fait imputer irrégulièrement une dépense sera passible de l'amende prévue à l'article L. 313-1.
