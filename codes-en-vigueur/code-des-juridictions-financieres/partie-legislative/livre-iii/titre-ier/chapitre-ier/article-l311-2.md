# Article L311-2

La Cour est composée comme suit :

- le premier président de la Cour des comptes, président ;

- le président de la section des finances du Conseil d'Etat, vice-président ;

- un nombre égal de conseillers d'Etat et de conseillers maîtres à la Cour des comptes.
