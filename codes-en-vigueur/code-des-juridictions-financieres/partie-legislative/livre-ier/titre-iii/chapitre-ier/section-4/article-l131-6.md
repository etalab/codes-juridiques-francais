# Article L131-6

La Cour des comptes peut condamner les comptables publics et les personnes qu'elle a déclarées comptables de fait à l'amende pour retard dans la production de leurs comptes.
