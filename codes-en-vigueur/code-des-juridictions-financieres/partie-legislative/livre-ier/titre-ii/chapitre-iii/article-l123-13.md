# Article L123-13

Sauf si elle est prononcée par le premier président de la Cour des comptes qui la notifie par ses soins, la sanction est notifiée au magistrat en cause par l'autorité investie du pouvoir de nomination. Elle prend effet le jour de cette notification.
