# Article L123-15

Sous réserve des dispositions de l'article L. 123-17, le magistrat suspendu conserve son traitement, l'indemnité de résidence, le supplément familial de traitement et les prestations familiales obligatoires.
