# CHAPITRE PRELIMINAIRE : Dispositions générales

- [Article L120-1](article-l120-1.md)
- [Article L120-2](article-l120-2.md)
- [Article L120-3](article-l120-3.md)
- [Article L120-4](article-l120-4.md)
