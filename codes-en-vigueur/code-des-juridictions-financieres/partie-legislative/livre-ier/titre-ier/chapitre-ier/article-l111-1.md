# Article L111-1

La Cour des comptes juge les comptes des comptables publics, sous réserve de la compétence que les dispositions du présent code attribuent, en premier ressort, aux chambres régionales et territoriales des comptes.

Elle statue sur les appels formés contre les décisions juridictionnelles rendues par les chambres régionales et territoriales des comptes.
