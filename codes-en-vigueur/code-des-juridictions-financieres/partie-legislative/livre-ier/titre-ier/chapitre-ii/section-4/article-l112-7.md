# Article L112-7

Les magistrats de l'ordre judiciaire et les fonctionnaires appartenant à un corps recruté par la voie de l'Ecole nationale d'administration peuvent exercer les fonctions de rapporteur auprès de la Cour des comptes dans des conditions fixées par décret en Conseil d'Etat. Ils ne peuvent exercer aucune activité juridictionnelle.

Cette disposition est également applicable aux fonctionnaires appartenant à des corps de même niveau de recrutement de la fonction publique de l'Etat, de la fonction publique territoriale, de la fonction publique hospitalière ainsi qu'aux agents de direction et aux agents comptables des organismes de sécurité sociale. Elle s'applique également, dans les conditions prévues par leur statut aux militaires et aux fonctionnaires des assemblées parlementaires appartenant à des corps de même niveau de recrutement.
