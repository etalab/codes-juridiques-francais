# Article L112-5

Des fonctionnaires appartenant au corps de contrôle des ministères ou des personnes ayant exercé des fonctions d'encadrement supérieur au sein de l'Etat ou d'organismes soumis au contrôle des juridictions financières peuvent être nommés conseillers maîtres en service extraordinaire en vue d'assister la Cour des comptes dans l'exercice des compétences mentionnées aux articles L. 111-2 à L. 111-8. Ils ne peuvent exercer aucune activité d'ordre juridictionnel.
