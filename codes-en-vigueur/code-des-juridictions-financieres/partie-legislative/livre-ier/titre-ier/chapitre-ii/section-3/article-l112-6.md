# Article L112-6

Les conseillers maîtres en service extraordinaire, dont le nombre ne peut être supérieur à douze, sont nommés par décret pris en conseil des ministres, après avis du premier président de la Cour des comptes, pour une période de cinq ans non renouvelable.
