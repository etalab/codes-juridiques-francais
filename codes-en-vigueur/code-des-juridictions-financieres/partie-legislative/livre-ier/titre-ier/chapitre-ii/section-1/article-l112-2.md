# Article L112-2

Le procureur général exerce le ministère public près la Cour des comptes et les formations communes aux juridictions mentionnées à l'article L. 111-9-1. Toutefois, lorsqu'une formation commune ne comporte que des membres des chambres régionales des comptes, le procureur général peut confier l'exercice du ministère public à un représentant du ministère public près une chambre régionale des comptes.

Il veille au bon exercice du ministère public près les chambres régionales et territoriales des comptes.
