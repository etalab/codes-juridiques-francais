# Article L141-1

La Cour des comptes est habilitée à se faire communiquer tous documents, de quelque nature que ce soit, relatifs à la gestion des services et organismes soumis à son contrôle.

Le fait de faire obstacle, de quelque façon que ce soit, à l'exercice des pouvoirs attribués aux membres et personnels de la Cour des comptes mentionnés aux sections 1 à 4 du chapitre II du titre Ier du présent livre par le présent code est puni de 15 000 euros d'amende. Le procureur général près la Cour des comptes peut saisir le parquet près la juridiction compétente en vue de déclencher l'action publique.
