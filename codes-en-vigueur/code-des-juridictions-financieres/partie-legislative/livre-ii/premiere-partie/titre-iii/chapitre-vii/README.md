# CHAPITRE VII : Dispositions particulières concernant la collectivité territoriale de Corse

- [Article L237-1](article-l237-1.md)
- [Article L237-2](article-l237-2.md)
