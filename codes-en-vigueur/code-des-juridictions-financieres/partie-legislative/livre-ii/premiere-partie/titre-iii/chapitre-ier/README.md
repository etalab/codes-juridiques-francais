# CHAPITRE Ier : Compétences juridictionnelles

- [Section 1 : Jugement des comptes](section-1)
- [Section 2 : Contrôle de l'apurement administratif des comptes](section-2)
- [Section 3 : Condamnation des comptables à l'amende](section-3)
