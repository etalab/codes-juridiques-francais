# Article L231-8

Pour les comptes soumis au régime de l'apurement administratif et qui ne font pas l'objet d'observations sous réserve des recours éventuels et du droit d'évocation de la chambre régionale des comptes, les arrêtés des autorités compétentes de l'Etat désignées par arrêté du ministre chargé du budget emportent décharge définitive du comptable.
