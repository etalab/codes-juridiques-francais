# Section 2 : Contrôle de l'apurement administratif des comptes

- [Article L231-7](article-l231-7.md)
- [Article L231-8](article-l231-8.md)
- [Article L231-9](article-l231-9.md)
