# Article L211-7

La chambre régionale des comptes concourt au contrôle budgétaire des collectivités territoriales et de leurs établissements publics dans les conditions définies aux sections 1 à 4 du chapitre II du titre III de la première partie du présent livre.
