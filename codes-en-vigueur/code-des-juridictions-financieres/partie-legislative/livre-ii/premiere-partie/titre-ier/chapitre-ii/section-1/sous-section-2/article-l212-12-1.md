# Article L212-12-1

I. - Les chambres régionales des comptes de La Réunion et de Mayotte ont le même président, les mêmes assesseurs et le ou les mêmes représentants du ministère public. Le siège de chacune des chambres régionales des comptes, qui peut être le même, est fixé par un décret en Conseil d'Etat.

II. - Pour l'application à Mayotte de la première partie du livre II du présent code :

1° La référence à la région ou au département est remplacée par la référence au Département de Mayotte ;

2° La référence aux conseils régionaux ou aux conseils généraux est remplacée par la référence au conseil général de Mayotte ;

3° La référence au président du conseil régional ou au président du conseil général est remplacée par la référence au président du conseil général de Mayotte.
