# Article L220-1

Sous réserve des dispositions du présent code, le statut général des fonctionnaires et les décrets en Conseil d'Etat pris pour son application s'appliquent aux membres du corps des chambres régionales des comptes dans des conditions fixées par décret en Conseil d'Etat.
