# Article L223-8

Le Conseil supérieur peut entendre des témoins ; il doit entendre ceux que le magistrat a désignés.
