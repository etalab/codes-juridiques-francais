# Article L244-1

Lorsqu'elle est saisie en application des dispositions de la section 1 du chapitre II du titre III de la première partie du présent livre, la chambre régionale des comptes dispose, pour l'instruction de ces affaires, des pouvoirs définis aux articles L. 241-1 à L. 241-5.
