# Article L243-1

Lorsque la chambre régionale des comptes examine la gestion des collectivités territoriales et des établissements publics locaux, les observations qu'elle présente ne peuvent être formulées sans un entretien préalable entre le magistrat rapporteur ou le président de la chambre et l'ordonnateur de la collectivité territoriale ou de l'établissement public concernés, ainsi que l'ordonnateur qui était en fonctions au cours de l'exercice examiné.
