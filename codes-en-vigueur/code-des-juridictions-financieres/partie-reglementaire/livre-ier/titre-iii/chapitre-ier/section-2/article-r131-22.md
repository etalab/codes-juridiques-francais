# Article R131-22

Les arrêts de décharges et de quitus rendus par la Cour des comptes après apurement des comptes de gestion présentés par les comptables principaux du Trésor s'appliquent également à eux en leur qualité de préposés de la Caisse des dépôts et consignations.
