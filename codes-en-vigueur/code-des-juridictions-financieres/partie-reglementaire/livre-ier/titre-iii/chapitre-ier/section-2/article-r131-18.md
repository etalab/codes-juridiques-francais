# Article R131-18

Le caissier général dont les opérations en numéraire et en valeurs ont été reconnues exactes et régulièrement justifiées est déchargé de sa gestion par la Cour des comptes, qui lui donne quitus lors de sa sortie de fonctions.
