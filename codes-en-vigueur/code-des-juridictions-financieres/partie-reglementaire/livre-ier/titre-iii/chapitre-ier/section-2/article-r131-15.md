# Article R131-15

Le caissier général et les comptables principaux du Trésor préposés de la Caisse des dépôts ne sont responsables envers la Cour des comptes que de la justification du fait matériel de l'encaissement ou du paiement. Cependant, en ce qui concerne les dépenses administratives, qu'il est seul habilité à payer, le caissier général est responsable des dépassements de crédits qui n'auraient pas fait l'objet d'une autorisation préalable du ministre chargé des finances.

Le détail des pièces justificatives que le caissier général et les préposés sont tenus de produire pour leur décharge est fixé par arrêté du ministre chargé des finances, pris sur la proposition du directeur général de la Caisse des dépôts et consignations.
