# Article R131-13

Après que la Cour a déclaré une gestion de fait, elle en juge les comptes produits et statue sur l'application de l'amende prévue à l'article R. 131-1, à l'issue de la procédure contradictoire prévue par les articles R. 142-4 à R. 142-12. Elle le fait au vu de nouvelles conclusions du ministère public, mais sans nouvelle réquisition du procureur général.
