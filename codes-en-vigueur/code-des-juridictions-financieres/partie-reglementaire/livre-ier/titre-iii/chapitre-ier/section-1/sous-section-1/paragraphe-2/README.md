# Paragraphe 2 : Dispositions concernant les receveurs des administrations financières.

- [Article D131-8](article-d131-8.md)
- [Article D131-9](article-d131-9.md)
- [Article D131-10](article-d131-10.md)
