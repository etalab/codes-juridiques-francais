# Article R131-2

Les comptes sont produits annuellement à la Cour des comptes, appuyés des pièces générales et justificatives, dans les conditions fixées par le                 décret n° 2012-1246 du 7 novembre 2012 relatif à la gestion budgétaire et comptable publique et les instructions prises pour son application. La Cour procède à la vérification de ces pièces pour préparer le jugement des comptes des comptables et pour assurer le contrôle de la gestion des ordonnateurs.

Toutefois, en ce qui concerne les opérations de l'Etat, la Cour des comptes reçoit trimestriellement les pièces justificatives des recettes et des dépenses effectuées au titre du budget général, des budgets annexes et des comptes spéciaux du Trésor.

Sont vérifiées dans les locaux des services gestionnaires ou centralisateurs les pièces justifiant les catégories de dépenses ou de recettes publiques fixées par arrêté du ministre chargé du budget pris sur proposition du premier président et du procureur général.
