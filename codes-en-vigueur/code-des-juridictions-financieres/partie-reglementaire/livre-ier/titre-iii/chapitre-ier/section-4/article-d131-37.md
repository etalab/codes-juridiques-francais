# Article D131-37

Dans la limite fixée pour les comptes d'un même exercice par l'article L. 131-7, le taux maximum de l'amende pouvant être infligée à un comptable principal de l'Etat pour retard dans la production de ses comptes est fixé à 200 euros par compte et par mois de retard.
