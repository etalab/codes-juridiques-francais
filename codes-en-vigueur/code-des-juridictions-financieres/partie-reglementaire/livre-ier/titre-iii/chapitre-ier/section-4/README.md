# Section 4 : Condamnation des comptables à l'amende pour retard

- [Article D131-37](article-d131-37.md)
- [Article D131-38](article-d131-38.md)
- [Article D131-39](article-d131-39.md)
