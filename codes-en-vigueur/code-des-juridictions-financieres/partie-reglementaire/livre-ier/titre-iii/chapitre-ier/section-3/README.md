# Section 3 : Contrôle de l'apurement administratif des comptes

- [Article D131-26](article-d131-26.md)
- [Article D131-27](article-d131-27.md)
- [Article D131-28](article-d131-28.md)
- [Article D131-29](article-d131-29.md)
- [Article D131-30](article-d131-30.md)
- [Article D131-31](article-d131-31.md)
- [Article D131-32](article-d131-32.md)
- [Article D131-33](article-d131-33.md)
- [Article D131-34](article-d131-34.md)
- [Article D131-35](article-d131-35.md)
- [Article D131-36](article-d131-36.md)
