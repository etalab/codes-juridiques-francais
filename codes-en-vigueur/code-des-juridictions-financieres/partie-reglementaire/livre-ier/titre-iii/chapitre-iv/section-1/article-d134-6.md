# Article D134-6

Chacun des ministres mentionnés à l'article R. 134-5 désigne par arrêté un directeur d'administration centrale pour le représenter au comité de pilotage institué par ce même article.

Le président du comité de pilotage et les directeurs d'administration centrale qui en sont membres ont voix délibérative. Les décisions sont prises à la majorité des membres présents. En cas de partage égal des voix, celle du président est prépondérante.

Le comité de pilotage comprend également avec voix consultative un représentant de chacune des administrations de tutelle ou de chacun des corps de contrôle mentionnés à l'article R. 134-4, désigné par le ou les ministres compétents.

Le comité associe à ses travaux, à la demande de son président, toute personnalité qualifiée compétente pour l'un des points inscrits à l'ordre du jour. En outre, un avocat général représentant le procureur général près la Cour des comptes, destinataire de l'ordre du jour, peut assister aux travaux de ce comité.
