# Section 5 : Dispositions diverses

- [Article D142-22](article-d142-22.md)
- [Article D142-23](article-d142-23.md)
- [Article D142-24](article-d142-24.md)
- [Article D142-25](article-d142-25.md)
