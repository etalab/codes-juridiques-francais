# Article R142-8

Le rôle des audiences publiques est fixé par le président de la formation de jugement en accord avec le ministère public.

Toute partie est avertie du jour où l'affaire est appelée à l'audience. En cas de transmission sur support papier, la notification est faite par lettre recommandée avec demande d'avis de réception.

Cette notification est faite sept jours au moins avant l'audience.

L'ordre du jour de l'audience est affiché à l'entrée de la Cour.
