# Article R143-4

Pour l'application des dispositions de l'article L. 143-2, lorsque l'organisme n'a pas de conseil d'administration ou d'assemblée générale, le président de l'organisme communique les observations formulées par la Cour des comptes aux organes en tenant lieu, lors de la première réunion qui suit leur réception.
