# Article R141-7

Les observations auxquelles donnent lieu les contrôles sont consignées dans un rapport. Les suites à leur donner font l'objet de propositions motivées.

Après communication au procureur général s'il y a lieu, et à l'exception des rapports établis en matière juridictionnelle, le président de chambre transmet le rapport et les pièces annexées au conseiller maître ou au conseiller maître en service extraordinaire, contre-rapporteur.

En accord avec le procureur général en cas de communication à celui-ci, il inscrit l'examen du rapport à l'ordre du jour de la formation compétente pour les rapports autres que ceux établis en matière juridictionnelle ; cet accord est réputé acquis dès lors que le rapport ainsi que l'ordre du jour ont été communiqués au procureur général au moins trois semaines avant la date de la séance.
