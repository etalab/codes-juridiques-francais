# Article R141-4

La Cour des comptes peut se faire communiquer, par l'intermédiaire du procureur général, les rapports des institutions et corps de contrôle.
