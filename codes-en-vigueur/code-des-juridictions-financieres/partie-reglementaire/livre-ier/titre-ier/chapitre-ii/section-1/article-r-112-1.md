# Article R*112-1

Les magistrats composant la Cour des comptes sont :

Le premier président ;

Les présidents de chambre ;

Les conseillers maîtres ;

Les conseillers référendaires ;

Les auditeurs de 1re classe ;

Les auditeurs de 2e classe.
