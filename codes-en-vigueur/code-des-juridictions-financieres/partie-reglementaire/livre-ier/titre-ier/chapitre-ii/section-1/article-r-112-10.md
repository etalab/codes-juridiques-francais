# Article R*112-10

Les avocats généraux sont désignés par décret parmi les conseillers maîtres ou les conseillers référendaires, après avis du procureur général.

Le premier avocat général est nommé par décret parmi les avocats généraux, après avis du procureur général.
