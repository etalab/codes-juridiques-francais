# Article R112-23

En cas d'absence ou d'empêchement, le premier président est remplacé par le plus ancien des présidents de chambre, chaque président de chambre par le président de section le plus ancien de la chambre ou, à défaut, par le conseiller maître le plus ancien, chaque président de section par le conseiller maître le plus ancien de la section.
