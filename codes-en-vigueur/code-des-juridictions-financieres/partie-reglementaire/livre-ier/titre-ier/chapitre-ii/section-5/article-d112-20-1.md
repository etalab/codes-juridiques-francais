# Article D112-20-1

Le greffe procède aux notifications prévues aux articles R. 142-1, R. 142-4, R. 142-6,
R. 142-8 et R. 142-15.
