# Article R112-17-4

Lorsqu'il y a lieu, pour la Cour des comptes, d'élire un ou plusieurs de ses membres pour la représenter auprès d'une institution, d'un organisme ou d'une commission, sont électeurs tous les membres de la Cour des comptes qui composent la chambre du conseil en formation plénière. L'élection a lieu au scrutin secret à la majorité des suffrages exprimés, dans des conditions fixées par arrêté du premier président.
