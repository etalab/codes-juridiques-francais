# Section 4 : Congé, renouvellement, reprise.

- [Article L461-8](article-l461-8.md)
- [Article L461-9](article-l461-9.md)
- [Article L461-10](article-l461-10.md)
- [Article L461-11](article-l461-11.md)
- [Article L461-12](article-l461-12.md)
- [Article L461-13](article-l461-13.md)
- [Article L461-14](article-l461-14.md)
