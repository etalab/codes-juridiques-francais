# Article L462-14

Le preneur qui a apporté des améliorations au fonds mis en métayage a droit, en quittant les lieux, à une indemnité due par le bailleur.
