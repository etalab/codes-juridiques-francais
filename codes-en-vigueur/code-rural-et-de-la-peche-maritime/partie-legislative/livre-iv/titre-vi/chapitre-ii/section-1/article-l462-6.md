# Article L462-6

Les dispositions de l'article L. 411-32 sont applicables aux baux à métayage dans les départements de la Guadeloupe, de la Martinique, de la Guyane, de la Réunion et de Saint-Pierre-et-Miquelon.
