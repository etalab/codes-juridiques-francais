# Article L462-15

En cas de vente du bien rural qu'il exploite, le preneur bénéficie, à égalité de prix, d'un droit de préemption dont les conditions d'exercice sont déterminées par décret en Conseil d'Etat.
