# Article L462-12

Le preneur est tenu d'user de la chose louée    raisonnablement, en suivant la destination qui lui a été donnée par le bail ; il ne peut sous-louer ni céder son bail sans le consentement exprès et par écrit du bailleur.

Il est tenu d'avertir le bailleur des usurpations qui peuvent être commises sur le fonds.
