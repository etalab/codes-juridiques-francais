# Section 3 : Dispositions diverses et d'application.

- [Article L462-27](article-l462-27.md)
- [Article L462-28](article-l462-28.md)
- [Article L462-29](article-l462-29.md)
