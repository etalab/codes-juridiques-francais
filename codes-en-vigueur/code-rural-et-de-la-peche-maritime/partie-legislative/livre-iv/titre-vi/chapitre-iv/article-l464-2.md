# Article L464-2

Nonobstant toute disposition contraire, en l'absence  de tribunal paritaire des baux ruraux, les attributions de cette juridiction et celles de son président sont exercées par le tribunal d'instance.
