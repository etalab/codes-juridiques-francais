# Chapitre VIII : Dispositions particulières aux baux cessibles hors du cadre familial.

- [Article L418-1](article-l418-1.md)
- [Article L418-2](article-l418-2.md)
- [Article L418-3](article-l418-3.md)
- [Article L418-4](article-l418-4.md)
- [Article L418-5](article-l418-5.md)
