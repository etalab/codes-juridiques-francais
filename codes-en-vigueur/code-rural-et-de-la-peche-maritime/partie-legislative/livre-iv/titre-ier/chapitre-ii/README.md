# Chapitre II : Droit de préemption et droit de priorité

- [Section 1 : Droit de préemption en cas d'aliénation à titre onéreux de biens ruraux.](section-1)
- [Section 2 : Dispositions relatives aux baux conclus entre copartageants d'une exploitation agricole par application de l'article 832-2 du code civil.](section-2)
