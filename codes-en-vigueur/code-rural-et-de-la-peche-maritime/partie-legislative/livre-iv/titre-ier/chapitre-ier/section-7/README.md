# Section 7 : Dispositions particulières aux locations annuelles renouvelables.

- [Article L411-40](article-l411-40.md)
- [Article L411-41](article-l411-41.md)
- [Article L411-42](article-l411-42.md)
- [Article L411-43](article-l411-43.md)
- [Article L411-44](article-l411-44.md)
- [Article L411-45](article-l411-45.md)
