# Sous-section 2 : Durée du bail.

- [Article L411-5](article-l411-5.md)
- [Article L411-6](article-l411-6.md)
- [Article L411-7](article-l411-7.md)
- [Article L411-8](article-l411-8.md)
