# Chapitre VII : Dispositions particulières aux baux à métayage

- [Section 1 : Régime du bail.](section-1)
- [Section 2 : Conversion en baux à ferme.](section-2)
