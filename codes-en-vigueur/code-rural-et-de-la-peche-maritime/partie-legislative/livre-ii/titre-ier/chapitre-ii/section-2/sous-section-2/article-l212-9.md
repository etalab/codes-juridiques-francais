# Article L212-9

Les propriétaires d'équidés et de camélidés sont tenus de les faire identifier par une personne habilitée à cet effet par l'autorité administrative, selon un procédé agréé conformément aux dispositions réglementaires prévues à l'article L. 212-11. Tout changement de propriété d'un équidé ou d'un camélidé doit être déclaré à l'Institut français du cheval et de l'équitation par le nouveau propriétaire. Les détenteurs d'équidés et de camélidés sont tenus de se déclarer auprès de cet établissement dans des conditions définies par décret.

L'Institut français du cheval et de l'équitation s'assure du respect des règles d'identification et de déclaration prévues à l'alinéa précédent. Il est chargé de la tenue du fichier national des équidés et délivre aux propriétaires les documents d'identification obligatoires.
