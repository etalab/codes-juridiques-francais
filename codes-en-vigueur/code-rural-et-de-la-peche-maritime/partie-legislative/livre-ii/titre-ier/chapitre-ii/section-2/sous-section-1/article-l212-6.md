# Article L212-6

La présente sous-section fixe les règles relatives à l'identification des animaux des espèces bovine, ovine, caprine et porcine.
