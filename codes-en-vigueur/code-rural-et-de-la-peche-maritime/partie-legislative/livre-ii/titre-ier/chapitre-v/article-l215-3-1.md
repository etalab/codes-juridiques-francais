# Article L215-3-1

Les gardes champêtres et les agents de police municipale constatent par procès-verbaux les infractions aux dispositions des articles L. 211-14 et L. 211-16 ainsi que des textes ou décisions pris pour leur application.
