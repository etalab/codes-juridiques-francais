# Livre II : Alimentation, santé publique vétérinaire et protection des végétaux

- [Titre Préliminaire : Dispositions communes](titre-preliminaire)
- [Titre Ier : La garde et la circulation des animaux et des produits animaux](titre-ier)
- [Titre II : Mesures de prévention, surveillance et lutte contre les dangers zoosanitaires](titre-ii)
- [Titre III : Qualité nutritionnelle et sécurité sanitaire des aliments](titre-iii)
- [Titre IV : L'exercice de la médecine et de la chirurgie des animaux](titre-iv)
- [Titre V : La protection des végétaux](titre-v)
- [Titre VII : Dispositions particulières aux départements d'outre-mer ainsi qu'à Mayotte, à Saint-Pierre-et-Miquelon, à la Polynésie française, à la Nouvelle-Calédonie et aux îles Wallis et Futuna.](titre-vii)
