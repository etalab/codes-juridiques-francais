# Article L231-1

I.-Les prescriptions générales de la législation relative à l'hygiène alimentaire et les règles sanitaires applicables aux exploitants du secteur alimentaire, du secteur des sous-produits animaux et du secteur de l'alimentation animale sont définies par le II de l'article L. 221-4, le chapitre VI du titre II et par le présent titre ou par les règlements et décisions communautaires entrant dans le champ d'application des dispositions susmentionnées.

II.-Dans l'intérêt de la protection de la santé publique, il doit être procédé :

1° Au contrôle officiel des animaux vivants appartenant à des espèces dont la chair ou les produits sont destinés à l'alimentation humaine ou animale et de leurs conditions de production ;

2° Au contrôle officiel des conditions d'abattage des animaux mentionnés au 1° ci-dessus ;

3° Au contrôle officiel des produits d'origine animale, des denrées alimentaires en contenant, des sous-produits animaux et des aliments pour animaux ;

4° A la détermination et au contrôle officiel des conditions d'hygiène dans lesquelles les produits d'origine animale, les denrées alimentaires en contenant, les sous-produits animaux et les aliments pour animaux sont préparés, transformés, conservés ou éliminés, notamment lors de leur transport et de leur mise en vente ;

5° Au contrôle officiel de la mise en oeuvre des bonnes pratiques d'hygiène et des systèmes d'analyse des dangers et des points critiques pour les maîtriser, en application de l'article 10 du règlement (CE) n° 882 / 2004 du Parlement européen et du Conseil du 29 avril 2004 ;

6° Au contrôle officiel des conditions techniques du transport des denrées alimentaires sous température dirigée.

Les résultats des contrôles effectués en application du plan national de contrôles officiels pluriannuel sont rendus publics selon des modalités fixées par voie réglementaire.
