# Chapitre préliminaire : La politique publique de l'alimentation

- [Article L230-2](article-l230-2.md)
- [Article L230-3](article-l230-3.md)
- [Article L230-4](article-l230-4.md)
- [Article L230-5](article-l230-5.md)
- [Article L230-6](article-l230-6.md)
