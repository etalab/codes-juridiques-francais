# Chapitre VI : Dispositions relatives aux pouvoirs de police administrative

- [Section 1 : Visite des locaux](section-1)
- [Section 2 : Mesures en cas de constatation d'un manquement](section-2)
