# Article L205-1

I. ― Sans préjudice des compétences des officiers et agents de police judiciaire et des autres agents publics spécialement habilités par la loi, sont habilités à rechercher et à constater les infractions prévues et réprimées par le 3° de l'article 444-3 et les articles 444-4,444-6 à 444-9,521-1,521-2, R. 645-8,
R. 654-1 et R. 655-1 du code pénal, ainsi que par le présent livre, à l'exception de la section 2 du chapitre Ier du titre Ier et du titre IV :

1° Les inspecteurs de la santé publique vétérinaire ;

2° Les ingénieurs ayant la qualité d'agent du ministère chargé de l'agriculture ;

3° Les techniciens des services du ministère chargé de l'agriculture ;

4° Les contrôleurs sanitaires des services du ministère chargé de l'agriculture ;

5° Les vétérinaires et préposés sanitaires contractuels de l'Etat ;

6° Les agents du ministère chargé de l'agriculture compétents en matière sanitaire ou phytosanitaire figurant sur une liste établie par arrêté du ministre chargé de l'agriculture.

Sont également habilités à rechercher et à constater ces infractions lorsqu'elles concernent l'élevage, la pêche et la commercialisation des coquillages, les administrateurs, officiers du corps technique et administratif des affaires maritimes, ainsi que les fonctionnaires affectés dans les services exerçant des missions de contrôle dans le domaine des affaires maritimes sous l'autorité ou à la disposition du ministre chargé de la mer.

II. ― Outre les compétences qu'ils tiennent de l'article L. 215-2 du code de la consommation, des articles L. 251-18, L. 253-14,
L. 254-11 et L. 255-9 du présent code, les agents de la concurrence, de la consommation et de la répression des fraudes sont habilités à rechercher et à constater, dans l'exercice de leurs fonctions, les infractions prévues et réprimées par le 3° de l'article 444-3, les articles 444-4,444-6 à 444-9 du code pénal, le titre Ier à l'exception de la section 2 du chapitre Ier et le titre III du présent livre.

III. ― Les formes et conditions de la prestation de serment des agents mentionnés au I sont déterminées par décret en Conseil d'Etat.
