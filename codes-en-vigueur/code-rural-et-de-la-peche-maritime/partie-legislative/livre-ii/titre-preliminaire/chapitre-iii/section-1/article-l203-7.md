# Article L203-7

Le vétérinaire sanitaire concourt, à la demande de l'autorité administrative, à l'exécution des opérations de police sanitaire mentionnées au I de l'article L. 203-8 concernant les animaux pour lesquels il a accepté d'être désigné comme vétérinaire sanitaire par le détenteur ou le responsable du rassemblement d'animaux. Dans ce cas, les dispositions des articles L. 203-8,
L. 203-10 et L. 203-11 lui sont applicables.
