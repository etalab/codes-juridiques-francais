# Article L201-7

Tout propriétaire ou détenteur d'animaux ou de végétaux, ou tout professionnel exerçant ses activités en relation avec des animaux ou végétaux, ainsi que toute personne mentionnée aux deux derniers alinéas de l'article L. 201-2, qui détecte ou suspecte l'apparition d'un danger sanitaire de première catégorie ou la première apparition sur le territoire national d'un danger sanitaire en informe immédiatement l'autorité administrative.

Tout propriétaire ou détenteur de denrées alimentaires ou d'aliments pour animaux soumis aux prescriptions prévues à l'article L. 231-1 et tout laboratoire sont tenus de communiquer immédiatement à l'autorité administrative tout résultat d'examen indiquant qu'une denrée alimentaire ou un aliment pour animaux qu'il a importé, produit, transformé, fabriqué, distribué ou analysé présente ou est susceptible de présenter un danger sanitaire de première catégorie.

Les vétérinaires et les laboratoires communiquent immédiatement à l'autorité administrative tout résultat d'analyse conduisant à suspecter ou constater la présence d'un danger sanitaire de première catégorie ou la première apparition sur le territoire national d'un danger sanitaire.

Les personnes mentionnées au présent article sont également soumises à un devoir d'information sur les dangers sanitaires de deuxième catégorie qui figurent sur une liste établie par l'autorité administrative. L'autorité administrative définit les cas où l'information doit être communiquée à ses services ou à l'association sanitaire régionale mentionnée à l'article L. 201-11.
