# Article L201-11

Dans chaque région, une fédération des organismes à vocation sanitaire constituée sous la forme d'une association régie par la loi du 1er juillet 1901, peut dans des conditions définies par décret en Conseil d'Etat, être reconnue comme association sanitaire régionale si ses statuts satisfont aux conditions suivantes :

1° Avoir pour objet la prévention, la surveillance et la maîtrise de l'ensemble des dangers sanitaires, notamment par l'élaboration du schéma régional de maîtrise des dangers sanitaires prévu à l'article L. 201-12 ;

2° Accepter de plein droit l'adhésion des organisations vétérinaires à vocation technique ;

3° Accepter de plein droit l'adhésion de toute organisation ou association professionnelle dès lors qu'elle exerce une compétence sanitaire dans le territoire considéré et s'engage par son adhésion à veiller au respect par ses membres des réglementations sanitaires et phytosanitaires en vigueur et du schéma régional mentionné à l'article L. 201-12 ;

4° Accepter de plein droit l'adhésion de la région, des départements et des chambres d'agriculture de la région ;

5° Prévoir que les organismes à vocation sanitaire disposent ensemble de la majorité des voix au sein de ses organes délibérants.

Tous les membres de l'association sanitaire régionale ont le droit de participer aux organes délibérants de l'association.
