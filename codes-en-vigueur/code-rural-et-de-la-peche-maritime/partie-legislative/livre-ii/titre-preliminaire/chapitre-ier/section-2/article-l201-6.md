# Article L201-6

Sont habilités à procéder à l'inspection et au contrôle que nécessite l'application du présent titre, des règlements et décisions de l'Union européenne ayant le même objet et des textes pris pour leur application :

1° En ce qui concerne les animaux, les agents mentionnés à l'article L. 221-5 ;

2° En ce qui concerne les végétaux, les agents mentionnés à l'article L. 250-2.
