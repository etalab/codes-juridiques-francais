# Titre IV : L'exercice de la médecine et de la chirurgie des animaux

- [Chapitre Ier : L'exercice de la profession.](chapitre-ier)
- [Chapitre Ier bis :  Les sociétés de participations financières de la profession vétérinaire](chapitre-ier-bis)
- [Chapitre II : L'ordre des vétérinaires.](chapitre-ii)
- [Chapitre III : Dispositions relatives à l'exercice illégal de la médecine et de la chirurgie des animaux](chapitre-iii)
