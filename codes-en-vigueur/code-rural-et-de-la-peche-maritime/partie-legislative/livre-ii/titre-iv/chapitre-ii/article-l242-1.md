# Article L242-1

I.-L'ordre des vétérinaires veille au respect, par les personnes mentionnées aux articles L. 241-1, L. 241-3 et L. 241-17, et par les sociétés de participations financières mentionnées à l'article L. 241-18, des règles garantissant l'indépendance des vétérinaires et de celles inhérentes à leur déontologie, dont les principes sont fixés par le code prévu à l'article L. 242-3.

Il exerce ses missions par l'intermédiaire du Conseil supérieur de l'ordre des vétérinaires, dont le siège se situe à Paris, et des conseils régionaux de l'ordre, dans des conditions prévues par voie réglementaire.

II.-Les ordres régionaux sont institués dans chacune des circonscriptions régionales déterminées par un arrêté du ministre chargé de l'agriculture. Ils sont formés de tous les vétérinaires en exercice remplissant les conditions prévues à l'article L. 241-1 ainsi que des sociétés mentionnées au I de l'article L. 241-17.

Les membres des conseils régionaux de l'ordre sont élus par les vétérinaires mentionnés à l'article L. 241-1 et inscrits au tableau de l'ordre défini à l'article L. 242-4.

Les membres des conseils régionaux de l'ordre élisent les membres du Conseil supérieur de l'ordre des vétérinaires.

Seuls les vétérinaires mentionnés à l'article L. 241-1 établis ou exerçant à titre principal en France sont électeurs et éligibles.

Un décret en Conseil d'Etat fixe les modalités des élections aux conseils régionaux et au conseil supérieur.

Ne sont pas soumis au présent II les vétérinaires et docteurs vétérinaires appartenant au cadre actif du service vétérinaire de l'armée ainsi que les vétérinaires et docteurs vétérinaires investis d'une fonction publique n'ayant pas d'autre activité professionnelle vétérinaire.

III.-Pour l'exercice de ses missions, l'ordre des vétérinaires est habilité à exercer un contrôle des modalités de fonctionnement, de financement et d'organisation des sociétés mentionnées au I. Il peut à ce titre demander aux représentants de ces sociétés de lui communiquer les informations et les documents nécessaires à ce contrôle.
