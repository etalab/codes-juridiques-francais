# Article L242-9

Des décrets en Conseil d'Etat déterminent       les conditions d'application des chapitres Ier et II du présent titre.
