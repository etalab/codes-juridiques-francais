# Article L272-12

Pour son application à Mayotte, l'article L. 252-3 est ainsi rédigé :

" Art. L. 252-3.-Il ne peut être agréé à Mayotte qu'une seule fédération de groupements de défense contre les organismes nuisibles, qui exerce les missions des groupements et fédérations prévues aux articles L. 252-4 et L. 252-5. "
