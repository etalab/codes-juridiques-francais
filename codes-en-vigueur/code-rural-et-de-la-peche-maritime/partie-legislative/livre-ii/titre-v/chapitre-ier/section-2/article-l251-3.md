# Article L251-3

Sont considérés comme des organismes nuisibles tous les ennemis des végétaux ou des produits végétaux, qu'ils appartiennent au règne animal ou végétal ou se présentent sous forme de virus, mycoplasmes ou autres agents pathogènes.

L'autorité administrative dresse la liste des organismes nuisibles qui sont des dangers sanitaires de première catégorie et de deuxième catégorie définis à l'article L. 201-1.
