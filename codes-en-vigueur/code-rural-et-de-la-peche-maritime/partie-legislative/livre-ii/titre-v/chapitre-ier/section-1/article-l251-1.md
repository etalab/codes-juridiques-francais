# Article L251-1

I. - La surveillance biologique du territoire a pour objet de s'assurer de l'état sanitaire et phytosanitaire des végétaux et de suivre l'apparition éventuelle d'effets non intentionnels des pratiques agricoles sur l'environnement. Elle relève de la compétence des agents chargés de la protection des végétaux ou s'effectue sous leur contrôle. Les résultats de cette surveillance font l'objet d'un rapport annuel du Gouvernement à l'Assemblée nationale et au Sénat.

II. - Il est créé un Comité de surveillance biologique du territoire. Ce comité est consulté sur les protocoles et méthodologies d'observation nécessaires à la mise en œuvre de la surveillance biologique du territoire et sur les résultats de cette surveillance.

Il formule des recommandations sur les orientations à donner à la surveillance biologique du territoire et alerte l'autorité administrative lorsqu'il considère que certains effets non intentionnels nécessitent des mesures de gestion particulières.

Il est consulté sur le rapport annuel mentionné au I.

Le Comité de surveillance biologique du territoire est composé de personnalités désignées en raison de leurs compétences dans les domaines se rapportant notamment à l'écotoxicologie, aux sciences agronomiques et à la protection de l'environnement et des végétaux.

Un décret précise la composition, les attributions et les règles de fonctionnement de ce comité.

III. - Toute personne qui constate une anomalie ou des effets indésirables susceptibles d'être liés à la dissémination volontaire d'organismes génétiquement modifiés en informe immédiatement le service chargé de la protection des végétaux.

IV. - Le responsable de la dissémination volontaire d'organismes génétiquement modifiés, le distributeur et l'utilisateur de ces organismes doivent participer au dispositif de surveillance biologique du territoire, notamment en communiquant aux agents chargés de la protection des végétaux toutes les informations nécessaires à cette surveillance.

V. - Dans l'intérêt de l'environnement et de la santé publique, l'autorité administrative peut, par arrêté, prendre toutes mesures destinées à collecter les données et informations relatives à la mise sur le marché, la délivrance et l'utilisation des organismes génétiquement modifiés, afin d'en assurer le traitement et la diffusion.

Dans l'intérêt de la protection des appellations d'origine contrôlée, l'Institut national de l'origine et de la qualité peut proposer à l'autorité administrative les mesures prévues à l'alinéa précédent.
