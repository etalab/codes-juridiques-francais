# Section 3 : Le contrôle sanitaire des végétaux.

- [Article L251-12](article-l251-12.md)
- [Article L251-13](article-l251-13.md)
- [Article L251-14](article-l251-14.md)
- [Article L251-15](article-l251-15.md)
- [Article L251-16](article-l251-16.md)
- [Article L251-17](article-l251-17.md)
- [Article L251-17-1](article-l251-17-1.md)
- [Article L251-17-2](article-l251-17-2.md)
