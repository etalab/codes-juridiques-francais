# Article L257-5

Pour l'exercice de leurs missions, les agents mentionnés à l'article L. 250-2 sont, en outre, habilités à prélever des échantillons de végétaux, de produits d'origine végétale ou de sols.
