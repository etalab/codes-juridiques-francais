# Chapitre III : Mise sur le marché et utilisation  des produits phytopharmaceutiques

- [Section 1 : Conditions d'autorisation](section-1)
- [Section 2 : Information et protection des données](section-2)
- [Section 3 : Essais et études](section-3)
- [Section 4 : Emballage, étiquetage et publicité](section-4)
- [Section 5 : Plan d'action national pour une utilisation durable des produits phytopharmaceutiques](section-5)
- [Section 6 : Mesures de précaution et de surveillance](section-6)
- [Section 7 : Elimination des produits dont l'utilisation 
n'est pas autorisée](section-7)
- [Section 8 : Inspection et contrôle](section-8)
- [Section 9 : Dispositions pénales](section-9)
