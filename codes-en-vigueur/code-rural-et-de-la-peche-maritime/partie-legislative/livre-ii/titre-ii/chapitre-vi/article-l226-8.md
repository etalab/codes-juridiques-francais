# Article L226-8

L'élimination des produits transformés issus des matières de la catégorie 3 au sens du règlement (CE) n° 1774 / 2002 du 3 octobre 2002 précité, provenant d'abattoirs ou d'établissements de manipulation ou de préparation de denrées animales ou d'origine animale, ne relève pas du service public de l'équarrissage.

Dans les cas définis par décret, l'Etat peut se substituer aux abattoirs et établissements pour assurer l'élimination des déchets mentionnés ci-dessus. Dans le cas où l'Etat charge par décret l'établissement mentionné à l'article L. 621-1 d'assurer tout ou partie des mesures concourant à l'élimination de ces déchets, ce dernier est substitué de plein droit à l'Etat à compter de la date d'entrée en vigueur du décret dans tous les marchés en cours d'exécution passés en application du présent article. Cette substitution n'entraîne aucun droit à résiliation de ces marchés ou à indemnisation des cocontractants.
