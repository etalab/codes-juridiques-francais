# Chapitre VIII : Dispositions pénales.

- [Article L228-1](article-l228-1.md)
- [Article L228-2](article-l228-2.md)
- [Article L228-3](article-l228-3.md)
- [Article L228-4](article-l228-4.md)
- [Article L228-5](article-l228-5.md)
- [Article L228-6](article-l228-6.md)
- [Article L228-7](article-l228-7.md)
- [Article L228-8](article-l228-8.md)
