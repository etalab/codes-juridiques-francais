# Sous-section 1 : La rage.

- [Article L223-9](article-l223-9.md)
- [Article L223-10](article-l223-10.md)
- [Article L223-11](article-l223-11.md)
- [Article L223-12](article-l223-12.md)
- [Article L223-13](article-l223-13.md)
- [Article L223-14](article-l223-14.md)
- [Article L223-15](article-l223-15.md)
- [Article L223-16](article-l223-16.md)
- [Article L223-17](article-l223-17.md)
