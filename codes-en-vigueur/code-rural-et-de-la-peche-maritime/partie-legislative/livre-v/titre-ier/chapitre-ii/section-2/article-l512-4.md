# Article L512-4

La chambre d'agriculture de région est constituée par fusion d'une ou plusieurs chambres départementales et d'une chambre régionale.

Les articles L. 511-1 à L. 511-12, L. 512-1, L. 512-2 et L. 514-1 sont applicables à la chambre d'agriculture de région.
