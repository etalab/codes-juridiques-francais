# Article L582-7

Les deuxième, troisième et quatrième alinéas de l'article L. 523-1 sont ainsi rédigés :

"En cas d'augmentation du capital, celle-ci sera au maximum égale à l'augmentation de l'indice des prix à la consommation des ménages, établi par l'Institut territorial de la statistique et des études économiques.

"Cette augmentation, qui ne pourra intervenir qu'après présentation à l'assemblée générale extraordinaire d'un rapport spécial de révision établi par un commissaire aux comptes inscrit, est cumulable avec celle prévue à l'article L. 523-7.

"Les deux opérations cumulées ne peuvent toutefois aboutir à une augmentation du capital social supérieure à celle prévue au deuxième alinéa du présent article".
