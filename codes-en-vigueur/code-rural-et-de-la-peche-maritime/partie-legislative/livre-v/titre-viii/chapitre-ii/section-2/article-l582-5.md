# Article L582-5

Au 1° de l'article L. 522-1, après les mots : "de forestier", sont ajoutés les mots : "ou exerçant une activité de pêche".

Au 2° de l'article L. 522-1, après les mots : "des intérêts agricoles", sont ajoutés les mots : "forestiers ou dans le domaine de la pêche".

Le 3° de l'article L. 522-1 ne s'applique pas à la Nouvelle-Calédonie.

Au 4° de l'article L. 522-1, après les mots : "syndicats d'agriculteurs", sont ajoutés les mots : "ou de pêcheurs".
