# Article L523-9

Les sociétés coopératives agricoles et leurs unions peuvent procéder à une offre au public des titres financiers visés aux articles L. 523-8, L. 523-10 et L. 523-11 du présent code, sous réserve de disposer d'un capital dont le montant intégralement libéré ne soit pas inférieur à 37 000 €.
