# Chapitre V : Dispositions pénales.

- [Article L535-1](article-l535-1.md)
- [Article L535-2](article-l535-2.md)
- [Article L535-4](article-l535-4.md)
- [Article L535-5](article-l535-5.md)
