# Article L661-8

Les règles relatives à la sélection, la production, la protection, le traitement, la circulation, la distribution et l'entreposage des semences, des matériels de multiplication des végétaux, des plants et plantes ou parties de plantes destinés à être plantés ou replantés, autres que les matériels de multiplication végétative de la vigne et les matériels forestiers de reproduction, ci-après appelés " matériels ” en vue de leur commercialisation, ainsi que les règles relatives à leur commercialisation, sont fixées par décret en Conseil d'Etat. Ce décret fixe :

1° Les conditions dans lesquelles ces matériels sont sélectionnés, produits, multipliés et, le cas échéant, certifiés, en tenant compte des différents modes de reproduction ;

2° Les conditions d'inscription au Catalogue officiel des différentes catégories de variétés dont les matériels peuvent être commercialisés ;

3° Les règles permettant d'assurer la traçabilité des produits depuis le producteur jusqu'au consommateur.
