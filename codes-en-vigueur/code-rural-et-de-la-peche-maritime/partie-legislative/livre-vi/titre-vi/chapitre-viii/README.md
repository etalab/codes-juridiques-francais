# Chapitre VIII : Les plantes à parfum, aromatiques et médicinales.

- [Article L668-1](article-l668-1.md)
- [Article L668-2](article-l668-2.md)
- [Article L668-3](article-l668-3.md)
