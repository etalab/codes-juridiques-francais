# Article L653-5

Afin de contribuer à l'aménagement du territoire et de préserver la diversité génétique, il est institué un service universel de distribution et de mise en place de la semence des ruminants en monte publique, assuré dans le respect des principes d'égalité, de continuité et d'adaptabilité au bénéfice de tous les éleveurs qui en font la demande.

Le service universel est assuré par des opérateurs agréés par le ministre chargé de l'agriculture à l'issue d'un appel d'offres. Chaque opérateur est agréé pour une ou plusieurs zones géographiques, après évaluation des conditions techniques et tarifaires qu'il propose.

A titre transitoire, lors de la mise en place du service universel, le ministre chargé de l'agriculture peut, sans recourir à l'appel d'offres, accorder cet agrément pour une période maximale de trois ans aux centres de mise en place de la semence antérieurement autorisés.

Les coûts nets imputables aux obligations du service universel sont évalués sur la base d'une comptabilité appropriée tenue par les opérateurs agréés.

Un fonds de compensation assure le financement de ces coûts. Toutefois, quand ces derniers ne représentent pas une charge excessive pour l'opérateur agréé, aucun versement ne lui est dû. L'Etat participe à l'abondement de ce fonds.

Un décret en Conseil d'Etat détermine les modalités d'application du présent article. Il précise notamment les conditions d'attribution et de retrait de l'agrément des opérateurs, les modalités de règlement amiable des différends liés à l'exécution du service universel, ainsi que la définition de la monte publique.
