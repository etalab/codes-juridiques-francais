# Article L653-13-6

<div align="left"> Un décret précise les conditions d'organisation et de fonctionnement de l'établissement, son régime financier et comptable et les modalités d'exercice de la tutelle de l'Etat.<br/>
</div>
