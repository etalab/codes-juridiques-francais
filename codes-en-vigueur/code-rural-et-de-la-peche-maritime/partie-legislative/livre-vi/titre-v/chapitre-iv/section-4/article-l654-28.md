# Article L654-28

I. - Sous réserve de l'application du contrôle des structures des exploitations agricoles, toute constitution d'association ou de personne morale entre producteurs de lait de vache ou toute mise en commun entre eux d'ateliers ou d'autres moyens de production laitière, lorsque le regroupement ne comporte pas la cession, la location ou la mise à disposition des surfaces utilisées pour la production laitière, doit faire l'objet d'une demande d'autorisation préalable auprès du préfet du département où se situe le regroupement de la production.

Dans les trois mois suivant le dépôt de cette demande, l'autorité administrative délivre une autorisation de regroupement conforme au régime du prélèvement supplémentaire dans le secteur du lait et des produits laitiers institué par le règlement (CEE) n° 3950/92 du Conseil du 28 décembre 1992.

II. - En cas d'infraction aux dispositions édictées au I du présent article, notamment :

- lorsqu'un regroupement existant n'a pas fait l'objet d'une demande d'autorisation préalable ;

- lorsqu'un regroupement est constitué en méconnaissance d'une décision de refus d'autorisation ;

- lorsque les conditions effectives de fonctionnement d'un regroupement ont été modifiées après délivrance de l'autorisation,

l'autorité administrative met les intéressés en demeure de régulariser leur situation dans un délai de deux mois.

Si à l'expiration de ce délai l'irrégularité persiste, l'autorité administrative peut prononcer à l'encontre des intéressés une sanction pécuniaire d'un montant égal au prélèvement supplémentaire prévu par le règlement mentionné au I, selon le volume des références en cause. Cette sanction peut être reconduite chaque année, si les intéressés poursuivent le regroupement illicite.

Afin de rechercher et constater ces irrégularités, l'autorité administrative est habilitée à procéder à tous contrôles nécessaires auprès des producteurs et à vérifier sur place le fonctionnement de l'atelier de production.
