# Section 2 : Les contrats de vente de produits agricoles

- [Article L631-24](article-l631-24.md)
- [Article L631-25](article-l631-25.md)
- [Article L631-25-1](article-l631-25-1.md)
- [Article L631-26](article-l631-26.md)
