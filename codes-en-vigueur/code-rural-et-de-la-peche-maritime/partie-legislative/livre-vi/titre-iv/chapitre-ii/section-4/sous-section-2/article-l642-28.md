# Article L642-28

Les organismes certificateurs ont pour mission d'assurer la certification des produits bénéficiant d'un label rouge, d'une indication géographique, d'une spécialité traditionnelle garantie ou du signe "agriculture biologique" et, le cas échéant, celle des produits bénéficiant d'une appellation d'origine.
