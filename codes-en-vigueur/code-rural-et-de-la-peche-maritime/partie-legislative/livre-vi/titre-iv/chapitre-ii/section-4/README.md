# Section 4 : Le contrôle du cahier des charges

- [Sous-section 1 : Dispositions générales.](sous-section-1)
- [Sous-section 2 : Les organismes certificateurs.](sous-section-2)
- [Sous-section 3 : Les organismes d'inspection.](sous-section-3)
- [Sous-section 4 : Evaluation par l'Institut national de l'origine et de la qualité.](sous-section-4)
