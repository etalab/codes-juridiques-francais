# Section 1 : Dispositions générales.

- [Article L642-1](article-l642-1.md)
- [Article L642-2](article-l642-2.md)
- [Article L642-3](article-l642-3.md)
- [Article L642-4](article-l642-4.md)
