# Article L641-16

La dénomination "montagne" ne peut figurer sur l'étiquetage des produits bénéficiant d'une appellation d'origine contrôlée. Elle peut toutefois être autorisée par l'autorité administrative sur proposition de l'organisme de défense et de gestion de l'appellation d'origine contrôlée intéressé, dans le cas où l'intégralité de l'aire de production de l'appellation est située en zone de montagne.
