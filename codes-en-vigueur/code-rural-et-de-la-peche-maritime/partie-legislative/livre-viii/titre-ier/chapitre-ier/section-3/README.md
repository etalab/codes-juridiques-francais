# Section 3 : Dispositions relatives aux établissements d'enseignement et de formation.

- [Article L811-8](article-l811-8.md)
- [Article L811-9](article-l811-9.md)
- [Article L811-9-1](article-l811-9-1.md)
- [Article L811-10](article-l811-10.md)
- [Article L811-11](article-l811-11.md)
