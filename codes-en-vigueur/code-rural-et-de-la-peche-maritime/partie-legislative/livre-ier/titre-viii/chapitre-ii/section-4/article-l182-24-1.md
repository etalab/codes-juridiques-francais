# Article L182-24-1

Les articles L. 181-14-1 et L. 181-14-2 sont applicables à Mayotte. Pour l'application de l'article L. 181-14-2 à Mayotte, la référence : " L. 181-8 " est remplacée par la référence : " L. 182-16 ".
