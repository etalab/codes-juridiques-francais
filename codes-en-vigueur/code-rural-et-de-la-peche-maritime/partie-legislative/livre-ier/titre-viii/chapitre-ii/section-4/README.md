# Section 4 : Préservation et contrôle du morcellement des terres agricoles

- [Article L182-23](article-l182-23.md)
- [Article L182-24](article-l182-24.md)
- [Article L182-24-1](article-l182-24-1.md)
