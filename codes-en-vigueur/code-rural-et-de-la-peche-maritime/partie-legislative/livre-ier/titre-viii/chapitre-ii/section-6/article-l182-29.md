# Article L182-29

Le chapitre Ier du titre VI du présent livre n'est pas applicable à Mayotte.
