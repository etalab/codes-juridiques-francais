# Article L181-25

En Guadeloupe, en Guyane, en Martinique, à La Réunion et à Saint-Martin, le comité d'orientation stratégique et de développement agricole est chargé, en concertation avec les chambres consulaires et les organisations professionnelles agricoles et en tenant compte des orientations arrêtées au sein du conseil d'administration et des comités sectoriels de l'établissement créé en application de l'article L. 681-3, de définir une politique de développement agricole, agro-industriel, halio-industriel et rural commune à l'Etat et aux collectivités territoriales, notamment pour la mise en œuvre des programmes de l'Union européenne.

Il est présidé conjointement par :

1° Le représentant de l'Etat dans le département et le président du conseil régional en Guadeloupe ;

2° Le représentant de l'Etat dans le département et le président du conseil général à La Réunion ;

3° Le représentant de l'Etat dans la collectivité territoriale et le président du conseil régional en Guyane ;

4° Le représentant de l'Etat dans la collectivité territoriale et le président du conseil régional en Martinique ;

5° Le représentant de l'Etat dans la collectivité d'outre-mer et le président du conseil territorial de Saint-Martin à Saint-Martin.

Il comprend des représentants de l'Etat, des collectivités territoriales, des chambres consulaires, des organisations professionnelles agricoles, des associations agréées de protection de l'environnement et, le cas échéant, des organisations représentatives des filières de la pêche et de l'aquaculture, qui participent à l'élaboration de cette politique.

Un décret précise ses compétences, sa composition et ses règles de fonctionnement.
