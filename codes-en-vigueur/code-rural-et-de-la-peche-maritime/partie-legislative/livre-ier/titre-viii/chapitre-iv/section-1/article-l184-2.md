# Article L184-2

La commission territoriale de la consommation des espaces agricoles, mentionnée à l'article L. 184-1, se prononce sur les questions générales relatives à la régression des surfaces agricoles et à leur mise en valeur effective. Elle formule des propositions sur les moyens de contribuer à la limitation de la consommation de l'espace agricole. Elle est consultée sur toute mesure de déclassement de terres classées agricoles.
