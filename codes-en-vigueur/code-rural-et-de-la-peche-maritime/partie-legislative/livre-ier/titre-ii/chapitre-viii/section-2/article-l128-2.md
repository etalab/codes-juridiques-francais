# Article L128-2

Le président de l'office de développement agricole et rural de Corse ou son représentant est membre titulaire des commissions départementales d'aménagement foncier des départements de la Corse-du-Sud et de la Haute-Corse.
