# Chapitre VI : La réglementation et la protection des boisements

- [Section 1 : Réglementation des boisements et actions forestières.](section-1)
- [Section 2 : La protection des formations linéaires boisées.](section-2)
