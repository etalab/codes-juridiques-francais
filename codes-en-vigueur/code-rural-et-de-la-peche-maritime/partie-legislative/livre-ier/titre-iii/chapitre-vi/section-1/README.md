# Section 1 : Dispositions communes.

- [Article L136-1](article-l136-1.md)
- [Article L136-2](article-l136-2.md)
- [Article L136-3](article-l136-3.md)
