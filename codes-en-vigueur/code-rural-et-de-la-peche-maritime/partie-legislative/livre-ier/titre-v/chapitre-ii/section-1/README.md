# Section 1 : Servitude pour l'établissement de canalisations publiques d'eau ou d'assainissement.

- [Article L152-1](article-l152-1.md)
- [Article L152-2](article-l152-2.md)
