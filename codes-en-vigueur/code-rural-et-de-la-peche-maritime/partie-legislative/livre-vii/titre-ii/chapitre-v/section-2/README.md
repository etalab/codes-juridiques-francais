# Section 2 : Sanctions et dispositions diverses.

- [Article L725-13](article-l725-13.md)
- [Article L725-14](article-l725-14.md)
- [Article L725-15](article-l725-15.md)
- [Article L725-16](article-l725-16.md)
- [Article L725-17](article-l725-17.md)
- [Article L725-18](article-l725-18.md)
- [Article L725-19](article-l725-19.md)
- [Article L725-20](article-l725-20.md)
- [Article L725-21](article-l725-21.md)
- [Article L725-22-1](article-l725-22-1.md)
