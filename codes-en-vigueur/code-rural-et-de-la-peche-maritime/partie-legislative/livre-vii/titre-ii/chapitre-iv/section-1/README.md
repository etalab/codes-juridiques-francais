# Section 1 : Contrôle par l'administration et les agents habilités

- [Sous-section 1 : Contrôle par l'administration.](sous-section-1)
- [Sous-section 2 : Contrôle par les agents des caisses de mutualité sociale agricole et les autres agents habilités.](sous-section-2)
- [Sous-section 3 : Dispositions communes aux agents de l'administration et aux autres agents de contrôle.](sous-section-3)
