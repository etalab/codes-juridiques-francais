# Article L724-13

Sont passibles d'un emprisonnement d'un an et d'une amende de 3 750 euros ou de l'une de ces deux peines seulement les oppositions ou obstacles aux visites ou inspections des fonctionnaires et agents de contrôle mentionnés au premier alinéa de l'article L. 724-11.
