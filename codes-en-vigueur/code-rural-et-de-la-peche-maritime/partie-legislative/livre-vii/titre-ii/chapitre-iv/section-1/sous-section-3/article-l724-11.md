# Article L724-11

Les agents de contrôle assermentés des caisses de mutualité sociale agricole peuvent interroger les salariés agricoles pour connaître leurs nom, adresse, emploi, le montant de leur rémunération et celui des retenues effectuées sur leur salaire au titre des assurances sociales.

Les chefs d'exploitation ou d'entreprise agricole, les personnes mentionnées au 5° de l'article L. 722-10, les titulaires d'allocations ou de pension de retraite mentionnés au deuxième alinéa de l'article L. 722-13 ainsi que tous les employeurs de salariés agricoles sont tenus de recevoir, à toute époque, les agents de contrôle assermentés des caisses de mutualité sociale agricole qui se présentent pour assurer l'exercice de leurs missions et de leur présenter tous documents nécessaires à l'exercice de leur contrôle.

Ces dispositions concernent également, pour l'application des dispositions relatives aux accidents du travail et maladies professionnelles des salariés agricoles, les agents chargés du contrôle de la prévention

Les agents de contrôle mentionnés au premier alinéa doivent communiquer, le cas échéant, leurs observations à l'employeur en l'invitant à y répondre dans un délai déterminé.

A l'expiration de ce délai, ils transmettent au directeur de la caisse de mutualité sociale agricole leurs observations accompagnées de la réponse éventuelle de l'intéressé.
