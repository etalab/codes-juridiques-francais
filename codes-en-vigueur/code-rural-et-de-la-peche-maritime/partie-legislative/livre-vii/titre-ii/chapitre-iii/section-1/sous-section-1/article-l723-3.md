# Article L723-3

Les caisses de mutualité sociale agricole comprennent un service du recouvrement, contrôle et contentieux et des sections dont les opérations font l'objet de comptabilités distinctes dans des conditions fixées par décret.

Le service du recouvrement, contrôle et contentieux est notamment chargé du calcul et du recouvrement des cotisations dues par les ressortissants des régimes obligatoires de protection sociale agricole. Il en met le produit à la disposition des sections intéressées.

Les caisses comprennent obligatoirement les sections suivantes :

1° Assurances sociales des salariés ;

2° Prestations familiales ;

3° Assurance vieillesse et assurance veuvage des non-salariés ;

4° Assurance maladie, invalidité et maternité des non-salariés.

5° Assurance contre les accidents du travail et les maladies professionnelles des salariés ;

6° Action sanitaire et sociale ;

6° bis Assurance contre les accidents du travail et les maladies professionnelles des personnes non salariées mentionnées à l'article L. 752-1 ;

6° ter Assurance vieillesse complémentaire obligatoire des non-salariés agricoles ;

7° Le cas échéant, des sections assurances complémentaires facultatives maladie, invalidité et maternité et assurance vieillesse des non-salariés agricoles.

Les caisses de mutualité sociale agricole peuvent créer toute autre section qui s'avérerait nécessaire après autorisation de l'autorité administrative.

Elles peuvent également, sous leur responsabilité, créer des échelons locaux et confier aux délégués cantonaux élus directement des trois collèges qu'elles désignent toutes missions, qu'ils effectuent à titre gratuit.

Les caisses dont la circonscription comporte plusieurs départements peuvent constituer des comités départementaux selon des modalités fixées par leur conseil d'administration. Les conseils d'administration peuvent leur confier des missions, notamment pour animer le réseau des élus locaux de la mutualité sociale agricole. Ces comités départementaux peuvent être consultés sur les demandes individuelles relatives aux cotisations sociales et les aides individuelles relatives à l'action sanitaire et sociale ainsi que sur toutes questions concernant la gestion des régimes agricoles de protection sociale dans le département. Leurs membres peuvent assurer la représentation de la caisse sur mandat du conseil d'administration. Le comité départemental est composé d'administrateurs de la caisse, élus du département concerné, d'un membre désigné par l'union départementale des associations familiales et de délégués cantonaux du même département. Le nombre de membres du comité départemental ne peut excéder le nombre de membres du conseil d'administration d'une caisse départementale. Les dispositions des articles L. 723-36, L. 723-37 et L. 723-42 sont applicables aux membres de ces comités.

Elles contribuent au développement sanitaire et social des territoires ruraux.

Elles proposent au préfet la surface minimale d'assujettissement prévue à l'article L. 722-5-1.
