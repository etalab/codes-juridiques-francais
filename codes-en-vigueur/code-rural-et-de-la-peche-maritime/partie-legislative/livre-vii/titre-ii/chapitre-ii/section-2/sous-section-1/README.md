# Sous-section 1 : Dispositions générales.

- [Article L722-20](article-l722-20.md)
- [Article L722-21](article-l722-21.md)
- [Article L722-22](article-l722-22.md)
- [Article L722-23](article-l722-23.md)
- [Article L722-24](article-l722-24.md)
