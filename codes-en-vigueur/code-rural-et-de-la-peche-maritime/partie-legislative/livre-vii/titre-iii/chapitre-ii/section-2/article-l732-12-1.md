# Article L732-12-1

Le père ainsi que, le cas échéant, le conjoint de la mère ou la personne liée à elle par un pacte civil de solidarité ou vivant maritalement avec elle, lorsqu'ils appartiennent aux catégories mentionnées aux 1° et 2°, au a du 4° et au 5° de l'article L. 722-10 bénéficient, à l'occasion de la naissance d'un enfant, sur leur demande et sous réserve de se faire remplacer par du personnel salarié dans leurs travaux, d'une allocation de remplacement.

Un décret détermine les modalités d'application du présent article et notamment les montants et la durée maximale d'attribution de la prestation.
