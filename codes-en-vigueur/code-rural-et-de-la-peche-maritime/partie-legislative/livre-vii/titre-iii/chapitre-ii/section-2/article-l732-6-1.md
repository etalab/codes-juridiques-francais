# Article L732-6-1

La Caisse centrale de la mutualité sociale agricole conclut, au nom des caisses de mutualité sociale agricole, une convention avec les organismes assureurs mentionnés à l'article L. 731-30, qui précise les relations entre les caisses et lesdits organismes pour organiser la gestion des prestations prévues à l'article L. 732-4. Cette convention et, le cas échéant, ses avenants sont approuvés par arrêté des ministres chargés de l'agriculture et de la sécurité sociale.
