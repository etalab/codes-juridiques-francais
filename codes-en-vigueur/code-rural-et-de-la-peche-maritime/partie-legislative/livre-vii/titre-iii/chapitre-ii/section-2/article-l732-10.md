# Article L732-10

L'assurance prévue à la présente section prend en charge la couverture des frais exposés par les personnes du sexe féminin mentionnées aux 1° et 2°, au a du 4° et au 5° de l'article L. 722-10 pour assurer leur remplacement dans les travaux de l'exploitation agricole lorsque, prenant part de manière constante à ces travaux, elles sont empêchées de les accomplir en raison de la maternité.

L'allocation de remplacement est accordée aux femmes mentionnées au premier alinéa dont il est reconnu que la grossesse pathologique est liée à l'exposition in utero au diéthylstilbestrol à compter du premier jour de leur arrêt de travail dans les conditions fixées par décret.
