# Article L732-58

Le régime d'assurance vieillesse complémentaire obligatoire est financé :

- par le produit des cotisations dues, au titre de ce régime, par les chefs d'exploitation ou d'entreprise agricole pour leurs propres droits et, le cas échéant, pour les droits des bénéficiaires mentionnés au IV de l'article L. 732-56 ;

- par une fraction du droit de consommation sur les tabacs mentionné à l'article 575 du code général des impôts.

Les ressources du régime couvrent les charges de celui-ci telles qu'énumérées ci-après :

- les prestations prévues à l'article L. 732-60 ;

- les frais de gestion.
