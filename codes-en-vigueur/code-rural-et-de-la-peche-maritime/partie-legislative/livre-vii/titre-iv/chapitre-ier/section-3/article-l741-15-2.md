# Article L741-15-2

Les rémunérations et gains, au sens de l'article L. 741-10, versés aux salariés dont le contrat de travail à durée déterminée a été transformé en contrat à durée indéterminée par les employeurs exerçant les activités visées aux 1° et 4° de l'article L. 722-1, sont exonérés des cotisations à la charge de l'employeur au titre des assurances sociales pendant une durée annuelle fixée par décret et pendant deux ans à compter de la transformation du contrat.

Le montant journalier des rémunérations et gains exonérés est limité au produit du salaire minimum de croissance en vigueur lors de leur versement majoré de 50 % par le nombre journalier moyen d'heures rémunérées pendant la durée annuelle de l'exonération.

Ouvrent droit au bénéfice de l'exonération les salariés qui auront été employés, de manière consécutive ou non, pendant une durée minimum de cent vingt jours de travail effectif au cours des vingt-quatre mois ayant précédé la transformation de leur contrat de travail, et sous la condition que l'employeur n'ait procédé au cours des douze derniers mois à aucun licenciement pour motif économique.

Les dispositions du présent article s'appliquent aux contrats de travail à durée déterminée transformés en 2006,2007 et 2008 en contrats à durée indéterminée.

Le bénéfice des dispositions du présent article ne peut être cumulé avec celui d'une autre exonération totale ou partielle de cotisations patronales ou l'application de taux spécifiques, d'assiettes ou montants forfaitaires de cotisations, à l'exception des exonérations prévues par les articles L. 741-4-2 du présent code, ainsi que par l'article L. 241-18 du code de la sécurité sociale.
