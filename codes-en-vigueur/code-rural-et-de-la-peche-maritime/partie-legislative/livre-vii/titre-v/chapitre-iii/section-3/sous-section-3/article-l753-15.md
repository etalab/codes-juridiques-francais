# Article L753-15

Le montant annuel de l'allocation mentionnée à l'article L. 753-14, servie par le Fonds commun des accidents du travail agricole, est calculé selon les modalités fixées au présent titre pour les rentes et majorations de rentes et sur la base du salaire annuel minimum prévu à l'article L. 434-16 du code de la sécurité sociale.
