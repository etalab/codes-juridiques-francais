# Section 3 : Dépenses du fonds commun des accidents du travail agricole

- [Sous-section 1 : Garantie du paiement des rentes.](sous-section-1)
- [Sous-section 2 : Majorations de rentes.](sous-section-2)
- [Sous-section 3 : Allocation pour accidents antérieurs au 1er juillet 1973.](sous-section-3)
- [Sous-section 4 : Dispositions diverses.](sous-section-4)
