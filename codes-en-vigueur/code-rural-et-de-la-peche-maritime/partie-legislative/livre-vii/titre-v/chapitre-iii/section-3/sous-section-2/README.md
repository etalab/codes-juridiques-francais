# Sous-section 2 : Majorations de rentes.

- [Article L753-7](article-l753-7.md)
- [Article L753-8](article-l753-8.md)
- [Article L753-9](article-l753-9.md)
- [Article L753-10](article-l753-10.md)
- [Article L753-11](article-l753-11.md)
- [Article L753-12](article-l753-12.md)
- [Article L753-13](article-l753-13.md)
