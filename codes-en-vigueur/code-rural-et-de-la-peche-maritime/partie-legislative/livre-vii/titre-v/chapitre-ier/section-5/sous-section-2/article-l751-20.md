# Article L751-20

La partie de la rémunération des personnes mentionnées au 1 de l'article L. 322-4-16-3 du code du travail correspondant à une durée d'activité inférieure ou égale à la limite fixée par le décret prévu à l'article L. 241-11 du code de la sécurité sociale donne lieu à versement d'une cotisation forfaitaire d'accidents du travail.
