# Article L762-25

Les caisses générales de sécurité sociale compétentes en Guadeloupe, en Guyane, à la Martinique, à La Réunion, à Saint-Barthélemy et à Saint-Martin et la caisse compétente pour Mayotte sont chargées de promouvoir l'action sociale en faveur des bénéficiaires de la présente section.
