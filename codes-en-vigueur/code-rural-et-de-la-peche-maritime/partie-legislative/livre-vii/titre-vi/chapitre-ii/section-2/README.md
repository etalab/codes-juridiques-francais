# Section 2 : Prestations familiales.

- [Sous-section 1 : Bénéficiaires et prestations.](sous-section-1)
- [Sous-section 2 : Financement.](sous-section-2)
- [Article L762-6](article-l762-6.md)
