# Article L762-9

Les cotisations varient en fonction de la superficie pondérée de l'exploitation ; un décret fixe les modalités de calcul de ces cotisations.

Dans le bail à métayage, le preneur et le bailleur sont tenus l'un et l'autre au paiement de ces cotisations qui sont partagées entre eux selon une proportion fixée par décret.

L'assiette des cotisations dues par les associés exploitants d'une exploitation agricole à responsabilité limitée constituée conformément au chapitre IV du titre II du livre III est répartie entre les associés exploitants dans les conditions prévues à l'article L. 731-29.
