# Article L762-7

Est considérée comme exploitant agricole pour l'application de la présente section toute personne mettant en valeur, en une qualité autre que celle de salarié, une exploitation dont l'importance est au moins égale à un minimum fixé par décret et évaluée en superficie pondérée.

Un décret fixe les critères d'équivalence utilisés pour le calcul de cette superficie pondérée, compte tenu de la nature des productions végétales et animales.

En application de ces critères, un arrêté interministériel détermine les coefficients d'équivalence applicables dans chaque département.

En Guadeloupe, en Guyane, en Martinique, à La Réunion, à Mayotte, à Saint-Barthélemy et à Saint-Martin, une personne est réputée mettre en valeur une exploitation d'une importance égale au minimum mentionné au premier alinéa si elle exerce une activité de production végétale ou animale pour laquelle le coefficient d'équivalence mentionné au troisième alinéa n'est pas prévu et dès lors que cette activité requiert un temps de travail au moins égal à un seuil fixé par décret.

Dans le bail à métayage, le bailleur et le preneur sont considérés, pour l'application du présent article, comme mettant chacun en valeur la totalité de l'exploitation.
