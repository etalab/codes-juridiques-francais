# Article L762-6

Les non-salariés agricoles exerçant leur activité en Guadeloupe, en Guyane, à la Martinique, à La Réunion, à Saint-Barthélemy et à Saint-Martin bénéficient des prestations familiales mentionnées au chapitre V du titre V du livre VII du code de la sécurité sociale, dans les conditions prévues par la présente section et par les articles L. 755-3, L. 755-4, L. 755-11, L. 755-16 à L. 755-22 du code de la sécurité sociale.

Les non-salariés agricoles exerçant leur activité à Mayotte bénéficient des prestations familiales dans les conditions prévues à l'ordonnance n° 2002-149 du 7 février 2002 modifiée relative à l'extension et la généralisation des prestations familiales et à la protection sociale dans la collectivité départementale de Mayotte.
