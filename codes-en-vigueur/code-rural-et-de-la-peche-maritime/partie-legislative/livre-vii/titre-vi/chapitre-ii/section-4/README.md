# Section 4 : Assurance vieillesse et assurance veuvage

- [Sous-section 1 : Bénéficiaires et prestations.](sous-section-1)
- [Sous-section 2 : Financement.](sous-section-2)
- [Article L762-26](article-l762-26.md)
- [Article L762-27](article-l762-27.md)
