# Article L763-4

Pour l'application du titre Ier à Mayotte, les attributions de la chambre d'agriculture sont exercées par la chambre de l'agriculture, de la pêche et de l'aquaculture de Mayotte.
