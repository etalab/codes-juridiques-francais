# Article L761-23

<div align="left">Une contribution couvrant les dépenses supplémentaires engendrées par les départs en retraite à l'âge prévu aux articles L. 351-1-4 du code de la sécurité sociale et L. 732-18-3 du présent code est mise à la charge du régime local d'assurance accidents agricole régi par le code local des assurances sociales du 19 juillet 1911 applicable dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle.<br/>
<br/>
<br/>
<br/>
</div>
