# Article L713-19

Le code du travail s'applique aux salariés agricoles, à l'exception des dispositions pour lesquelles le présent livre a prévu des dispositions particulières.
