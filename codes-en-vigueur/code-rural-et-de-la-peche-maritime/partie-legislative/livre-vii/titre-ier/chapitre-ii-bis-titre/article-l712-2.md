# Article L712-2

<div align="left">Toute entreprise, à l'exception de celles mentionnées aux articles L. 1251-42 et L. 1252-1 du code du travail, dont les salariés relèvent du régime des salariés agricoles et répondent aux conditions fixées à l'article L. 712-3 du présent code peut adhérer à un service d'aide à l'accomplissement de ses obligations en matière sociale, dénommé : “ Titre emploi-service agricole ” et proposé par les caisses de mutualité sociale agricole.<br/>
<br/>
<br/>
<br/>
</div>
