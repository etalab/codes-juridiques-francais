# Chapitre VI : Hébergement et participation des employeurs agricoles à l'effort de construction.

- [Article L716-1](article-l716-1.md)
- [Article L716-2](article-l716-2.md)
- [Article L716-3](article-l716-3.md)
- [Article L716-4](article-l716-4.md)
- [Article L716-5](article-l716-5.md)
