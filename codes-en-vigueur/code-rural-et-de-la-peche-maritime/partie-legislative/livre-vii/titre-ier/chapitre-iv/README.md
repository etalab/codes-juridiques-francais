# Chapitre IV : Repos et congés

- [Section 1 : Repos hebdomadaire.](section-1)
- [Section 2 : Repos quotidien.](section-2)
- [Section 3 : Dispositions applicables aux organismes de mutualité agricole.](section-3)
- [Section 4 : Congé payé annuel](section-4)
