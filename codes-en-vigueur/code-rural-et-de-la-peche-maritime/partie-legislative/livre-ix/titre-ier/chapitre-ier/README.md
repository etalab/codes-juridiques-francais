# Chapitre Ier : Dispositions générales

- [Article L911-1](article-l911-1.md)
- [Article L911-2](article-l911-2.md)
- [Article L911-3](article-l911-3.md)
- [Article L911-4](article-l911-4.md)
