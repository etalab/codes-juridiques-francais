# Article L912-12

Ces organisations de producteurs sont habilitées à prendre, conformément aux règlements communautaires, les mesures propres à assurer l'amélioration des conditions de vente de leur production.

Les règles que les organisations de producteurs reconnues et représentatives au sens des règlements communautaires appliquent à leurs adhérents peuvent être étendues à la demande de ces organisations aux producteurs non adhérents.
