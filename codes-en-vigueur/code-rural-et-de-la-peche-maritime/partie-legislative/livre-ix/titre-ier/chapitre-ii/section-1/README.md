# Section 1 : Organisation professionnelle des pêches maritimes et des élevages marins

- [Article L912-1](article-l912-1.md)
- [Article L912-2](article-l912-2.md)
- [Article L912-3](article-l912-3.md)
- [Article L912-4](article-l912-4.md)
- [Article L912-5](article-l912-5.md)
