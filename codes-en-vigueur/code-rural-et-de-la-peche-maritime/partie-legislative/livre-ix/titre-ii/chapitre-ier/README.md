# Chapitre Ier : Dispositions générales

- [Section 1 : Autorisation des activités de pêche maritime](section-1)
- [Section 2 : Dispositions applicables aux navires 
battant pavillon d'un Etat étranger](section-2)
- [Section 3 : Autres dispositions](section-3)
