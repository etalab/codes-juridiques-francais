# Chapitre IV : Dispositions communes aux collectivités d'outre-mer et à la Nouvelle-Calédonie

- [Article L954-1](article-l954-1.md)
- [Article L954-2](article-l954-2.md)
