# Section 1 : Agents chargés de la recherche 
et la constatation des infractions

- [Article L942-1](article-l942-1.md)
- [Article L942-2](article-l942-2.md)
