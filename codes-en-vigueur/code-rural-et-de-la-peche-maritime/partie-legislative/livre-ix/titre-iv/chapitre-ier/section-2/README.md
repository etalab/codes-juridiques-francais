# Section 2 : Opérations de contrôle

- [Article L941-3](article-l941-3.md)
- [Article L941-4](article-l941-4.md)
- [Article L941-5](article-l941-5.md)
- [Article L941-6](article-l941-6.md)
- [Article L941-7](article-l941-7.md)
- [Article L941-8](article-l941-8.md)
