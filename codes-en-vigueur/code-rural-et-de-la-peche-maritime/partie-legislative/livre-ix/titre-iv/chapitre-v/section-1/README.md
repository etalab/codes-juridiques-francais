# Section 1 : Sanctions des infractions en matière 
de pêche maritime et d'aquaculture marine

- [Article L945-1](article-l945-1.md)
- [Article L945-2](article-l945-2.md)
- [Article L945-3](article-l945-3.md)
- [Article L945-4](article-l945-4.md)
- [Article L945-4-1](article-l945-4-1.md)
