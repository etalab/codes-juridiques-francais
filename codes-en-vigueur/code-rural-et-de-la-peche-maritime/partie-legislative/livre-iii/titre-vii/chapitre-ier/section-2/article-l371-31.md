# Article L371-31

Les 1° et 2° de l'article L. 361-2 et l'article L. 361-5 ne sont pas applicables en Guadeloupe, en Guyane, en Martinique, à La Réunion, à Mayotte, à Saint-Barthélemy, à Saint-Martin et à Saint-Pierre-et-Miquelon.

A la demande du ministre chargé de l'agriculture et du ministre chargé de l'outre-mer, le Comité national de la gestion des risques en agriculture prévu à l'article L. 361-8 peut être mobilisé afin d'utiliser ses compétences et ses moyens à des fins d'expertise dans les départements d'outre-mer.
