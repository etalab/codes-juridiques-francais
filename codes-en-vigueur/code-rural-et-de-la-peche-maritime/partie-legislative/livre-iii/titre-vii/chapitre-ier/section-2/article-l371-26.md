# Article L371-26

Toute personne ayant sciemment fait une fausse déclaration ou participé à l'établissement d'une telle déclaration pour l'application des dispositions prévues au présent chapitre est passible des peines prévues à l'article 161, dernier alinéa, du code pénal.
