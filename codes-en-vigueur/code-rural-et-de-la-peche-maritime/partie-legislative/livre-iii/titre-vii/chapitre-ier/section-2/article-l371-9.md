# Article L371-9

Indépendamment des taxes parafiscales qui pouraient être établies, après avis de chaque conseil général concerné, au profit de la Caisse centrale de réassurance mentionnée à l'article L. 362-5 sur certains produits agricoles et alimentaires originaires des départements d'outre-mer, expédiés hors de chacun de ces départements ou alimentant le marché local du département, l'Etat affecte au fonds de garantie des calamités agricoles des départements d'outre-mer :

1° Une contribution additionnelle aux primes ou cotisations d'assurance afférentes aux conventions d'assurance couvrant à titre exclusif ou principal les dommages aux biens mentionnés à l'article L. 362-6. La contribution additionnelle est assise sur la totalité des primes ou cotisations. Elle est liquidée et recouvrée suivant les mêmes règles, sous les mêmes garanties et les mêmes sanctions que la taxe annuelle sur les conventions d'assurance prévue à l'article 991 du code général des impôts. Son taux est fixé par la loi de finances et ne peut être supérieur à 10 % ;

2° Tout ou partie des bénéfices versés au Trésor, réalisés en métropole sur les importations de bananes en provenance des pays tiers. Le montant des bénéfices affectés au fonds est déterminé par arrêté interministériel ;

3° Une subvention inscrite au budget de l'Etat et dont le montant sera au moins égal aux produits des taxes parafiscales et des recettes prévues ci-dessus.
