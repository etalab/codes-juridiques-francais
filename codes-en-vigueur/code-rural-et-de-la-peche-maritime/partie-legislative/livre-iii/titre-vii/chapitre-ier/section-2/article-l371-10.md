# Article L371-10

La gestion comptable et financière du fonds est assurée selon les dispositions de l'article L. 431-11 du code des assurances ci-après reproduit :

" Art.L. 431-11 : La gestion comptable et financière du fonds national de gestion des risques en agriculture mentionné à l'article L. 442-1 est assurée par la Caisse centrale de réassurance dans un compte distinct de ceux qui retracent les autres opérations pratiquées par cet établissement.

" Les frais exposés par la Caisse centrale de réassurance pour la gestion du fonds lui sont remboursés dans des conditions fixées par décret en Conseil d'Etat ".
