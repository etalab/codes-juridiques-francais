# Section 1 : Les rapports entre les membres de l'exploitation familiale

- [Sous-section 1 : Les rapports entre les époux, les personnes liées par un pacte civil de solidarité et les concubins.](sous-section-1)
- [Sous-section 2 : Les associés d'exploitation.](sous-section-2)
- [Sous-section 3 : Le contrat de travail à salaire différé.](sous-section-3)
