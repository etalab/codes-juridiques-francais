# Section 2 : La transmission de l'exploitation familiale.

- [Article L321-22](article-l321-22.md)
- [Article L321-23](article-l321-23.md)
- [Article L321-24](article-l321-24.md)
- [Article L321-25](article-l321-25.md)
