# Chapitre Ier : Exploitation familiale à responsabilité personnelle

- [Section 1 : Les rapports entre les membres de l'exploitation familiale](section-1)
- [Section 2 : La transmission de l'exploitation familiale.](section-2)
