# Chapitre II : Les groupements fonciers agricoles et les groupements fonciers ruraux.

- [Article L322-1](article-l322-1.md)
- [Article L322-2](article-l322-2.md)
- [Article L322-3](article-l322-3.md)
- [Article L322-4](article-l322-4.md)
- [Article L322-5](article-l322-5.md)
- [Article L322-6](article-l322-6.md)
- [Article L322-7](article-l322-7.md)
- [Article L322-8](article-l322-8.md)
- [Article L322-9](article-l322-9.md)
- [Article L322-10](article-l322-10.md)
- [Article L322-11](article-l322-11.md)
- [Article L322-12](article-l322-12.md)
- [Article L322-13](article-l322-13.md)
- [Article L322-14](article-l322-14.md)
- [Article L322-15](article-l322-15.md)
- [Article L322-16](article-l322-16.md)
- [Article L322-17](article-l322-17.md)
- [Article L322-18](article-l322-18.md)
- [Article L322-19](article-l322-19.md)
- [Article L322-20](article-l322-20.md)
- [Article L322-21](article-l322-21.md)
- [Article L322-22](article-l322-22.md)
- [Article L322-23](article-l322-23.md)
- [Article L322-24](article-l322-24.md)
