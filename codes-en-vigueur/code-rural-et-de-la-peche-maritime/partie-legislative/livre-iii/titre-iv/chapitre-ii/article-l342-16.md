# Article L342-16

Les avis prescrits dans le présent chapitre sont envoyés en la forme et avec la taxe des papiers d'affaires recommandés.
