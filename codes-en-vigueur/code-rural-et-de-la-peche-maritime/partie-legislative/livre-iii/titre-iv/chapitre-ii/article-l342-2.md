# Article L342-2

Le cultivateur, lorsqu'il ne sera pas propriétaire ou usufruitier de son exploitation, devra, avant tout emprunt, sauf ce qui sera dit ci-après, aviser le propriétaire du fonds loué de la nature, de la valeur et de la quantité des marchandises qui doivent servir de gage pour l'emprunt, ainsi que du montant des sommes à emprunter.

Cet avis devra être donné au propriétaire, usufruitier ou à leur mandataire légal désigné par l'intermédiaire du greffier du tribunal d'instance dans le ressort duquel se trouvent les objets warrantés. La lettre d'avis sera remise au greffier, qui devra la viser, l'enregistrer et l'envoyer sous forme de pli d'affaires recommandé avec accusé de réception.

Le propriétaire, l'usufruitier ou le mandataire légal désigné pourront, dans le cas où des termes échus leur seraient dus, dans un délai de huit jours francs à partir de la date de l'accusé de réception, s'opposer au prêt sur lesdits objets par une autre lettre envoyée également sous pli d'affaires recommandé au greffier du tribunal d'instance.

Toutefois, si le prêteur y consent, et sous la condition que l'emprunteur devra conserver la garde des objets warrantés dans les bâtiments ou sur les terres de l'exploitation, aucun avis ne sera donné au propriétaire ou usufruitier, et le consentement donné sera mentionné dans les clauses particulières du warrant ; mais, en ce cas, le privilège du bailleur subsistera dans les termes de droit.

Le bailleur pourra renoncer à son privilège jusqu'à concurrence de la dette contractée, en apposant sa signature sur le warrant.
