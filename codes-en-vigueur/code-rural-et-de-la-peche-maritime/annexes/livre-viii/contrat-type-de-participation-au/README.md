# Contrat type de participation au service public d'éducation et de formation des établissements privés offrant une formation pédagogique mentionnés à l'article L. 813-10 (2°).

- [Article Annexe III à l'article L813-10](article-annexe-iii-a-l-article-l813-10.md)
