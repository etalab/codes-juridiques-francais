# Livre II : Santé publique vétérinaire et protection des végétaux

- [Annexe à la sous-section 1 de la section 2 du chapitre VI du titre III du livre II.](annexe-a)
- [Contrat type applicable aux transports publics routiers d'animaux vivants.](contrat-type-applicable-aux)
