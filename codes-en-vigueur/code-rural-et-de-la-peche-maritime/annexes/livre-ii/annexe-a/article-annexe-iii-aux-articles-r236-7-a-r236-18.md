# Article Annexe III aux articles R236-7 à R236-18

<table>
<thead>
<tr>
<td width="227">
<p align="center">MALADIES-AGENTS PATHOGÈNES</p>
</td>
<td width="227">
<br/>
<p align="center">ESPÈCES SENSIBLES</p>
</td>
</tr>
</thead>
<tbody>
<tr>
<td valign="top">
<br/>
<p>Haplosporidiosis (Haplosporidium nelsoni).</p>
</td>
<td valign="top">
<br/>
<p>Huître de Virginie (Crassostrea virginica).</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p>(Haplosporidium costale).</p>
</td>
<td valign="top">
<br/>
<p>Huître de Virginie (Crassostrea virginica).</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p>Perkinosis (Perkinsus marinus).</p>
</td>
<td valign="top">
<br/>
<p>Huître de Virginie (Crassostrea virginica).</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p>(Perkinsus olseni).</p>
</td>
<td valign="top">
<br/>
<p>Ormeau à lèvres noires (Haliotis rubra).</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<br/>
</td>
<td valign="top">
<br/>
<p>Ormeau lisse d'Australie (Haliotis laevigata).</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p>Mikrokytosis (Mykrokytos mackini).</p>
</td>
<td valign="top">
<br/>
<p>Huître creuse japonaise (Crassostrea gigas).</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<br/>
</td>
<td valign="top">
<br/>
<p>Huître plate (Ostrea edulis).</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<br/>
</td>
<td valign="top">
<br/>
<p>Huître plate d'Argentine (Ostrea puelchana).</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<br/>
</td>
<td valign="top">
<br/>
<p>Huître plate du Pacifique (Ostrea denselamellosa).</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<br/>
</td>
<td valign="top">
<br/>
<p>Huître plate du Chili Tiostrea chilensis).</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p>(Mykrokytos roughleyi).</p>
</td>
<td valign="top">
<br/>
<p>Huître creuse d'Australie (Saccostrea commercialis).</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p>Iridovirosis (Oyster velar virus).</p>
</td>
<td valign="top">
<br/>
<p>Huître creuse japonaise (Crassostrea gigas).</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p>Marteiliosis (Marteilia sidneyi).</p>
</td>
<td valign="top">
<br/>
<p>Huître creuse d'Australie (Saccostrea commercialis).</p>
</td>
</tr>
</tbody>
</table>
