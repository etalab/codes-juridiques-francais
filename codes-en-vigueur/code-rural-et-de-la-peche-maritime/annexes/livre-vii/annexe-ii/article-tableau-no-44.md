# Article Tableau n° 44

Affections cutanées et muqueuses professionnelles de mécanisme allergique

<table>
<tbody>
<tr>
<td width="265">
<p align="center">DÉSIGNATION DES MALADIES</p>
</td>
<td width="113">
<p align="center">DÉLAI DE PRISE<br/>en charge</p>
</td>
<td width="265">
<p align="center">LISTE INDICATIVE DES PRINCIPAUX TRAVAUX susceptibles de provoquer ces maladies</p>
</td>
</tr>
<tr>
<td valign="top" width="265">
<p>Lésions eczématiformes récidivant après nouvelle exposition au risque ou confirmées par un test épicutané positif au produit manipulé.</p>
</td>
<td valign="top" width="113">
<p align="center">15 jours</p>
</td>
<td rowspan="3" valign="top" width="265">
<p>Manipulation ou emploi habituels, dans l'activité professionnelle, de tous produits.</p>
</td>
</tr>
<tr>
<td valign="top" width="265">
<p>Conjonctivite aiguë bilatérale récidivant en cas de nouvelle exposition ou confirmée par un test.</p>
</td>
<td valign="top" width="113">
<p align="center">7 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="265">
<p>Urticaire de contact récidivant en cas de nouvelle exposition et confirmé par un test.</p>
</td>
<td valign="top" width="113">
<p align="center">7 jours</p>
</td>
</tr>
</tbody>
</table>
