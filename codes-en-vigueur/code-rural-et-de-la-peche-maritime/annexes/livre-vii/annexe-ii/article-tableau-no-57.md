# Article Tableau n° 57

**Affections chroniques du rachis lombaire provoquées par des vibrations de basses**

**et moyennes fréquences transmises au corps entier**

<table>
<tbody>
<tr>
<th>
<br/>DÉSIGNATION DES MALADIES <br/>
</th>
<th>
<br/>DÉLAI DE PRISE EN CHARGE <br/>
</th>
<th>
<br/>LISTE LIMITATIVE DES TRAVAUX <p>susceptibles de provoquer ces maladies <br/>
</p>
</th>
</tr>
<tr>
<td align="center">
<p align="left">
<br/>Sciatique par hernie discale L4-L5 ou L5-S1 avec atteinte radiculaire de topographie concordante. </p>
<p align="left">Radiculalgie crurale par hernie discale L2-L3 ou L3-L4 ou L4-L5, avec atteinte radiculaire de topographie concordante. <br/>
</p>
</td>
<td align="center">
<br/>6 mois (sous réserve d'une durée d'exposition de 5 ans) <br/>
</td>
<td align="center">
<p align="left">
<br/>Travaux exposant habituellement aux vibrations de basses et moyennes fréquences, transmises au corps entier : </p>
<p align="left">1. Par l'utilisation ou la conduite : </p>
<p align="left">- de tracteurs ou machines agricoles, y compris les tondeuses autoportées, </p>
<p align="left">- de tracteurs ou engins forestiers, </p>
<p align="left">- d'engins de travaux agricoles ou publics, </p>
<p align="left">- de chariots automoteurs à conducteurs portés ; </p>
<p align="left">2. Par l'utilisation de crible, concasseur, broyeur ; </p>
<p align="left">3. Par la conduite de tracteurs routiers et de camions monoblocs ; </p>
<p align="left">4. Par l'utilisation et la conduite des sulkys de courses et d'entraînement de trot, tractés par des chevaux.<br/>
</p>
</td>
</tr>
</tbody>
</table>
