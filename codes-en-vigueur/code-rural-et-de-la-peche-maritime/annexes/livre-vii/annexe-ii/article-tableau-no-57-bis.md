# Article Tableau n° 57 bis

Affections chroniques du rachis lombaire

provoquées par la manutention manuelle habituelle de charges lourdes

<table>
<tbody>
<tr>
<td width="265">
<p align="center">DÉSIGNATION DES MALADIES</p>
</td>
<td width="113">
<p align="center">DÉLAI DE PRISE<br/>en charge</p>
</td>
<td width="265">
<p align="center">LISTE LIMITATIVE DES TRAVAUX SUSCEPTIBLES<br/>de provoquer ces maladies</p>
</td>
</tr>
<tr>
<td valign="top" width="265">
<p>Sciatique par hernie discale L4-L5 ou L5-S1 avec atteinte radiculaire de topographie concordante. Radiculalgie crurale par hernie discale L2-L3 ou L3-L4 ou L4-L5, avec atteinte radiculaire de topographie concordante.</p>
</td>
<td valign="top" width="113">
<p align="center">6 mois (sous réserve d'une durée d'exposition de 5 ans)</p>
</td>
<td valign="top" width="265">
<p>Travaux de manutention manuelle habituelle de charges lourdes effectués :</p>
<p>- dans les exploitations agricoles et forestières, les scieries ; </p>
<p>- dans les établissements de conchyliculture et de pisciculture ; </p>
<p>- dans les entreprises de travaux agricoles, les entreprises de travaux paysagers ; </p>
<p>- dans les entreprises artisanales rurales ; </p>
<p>- dans les abattoirs et entreprises d'équarrissage ; </p>
<p>- dans le chargement et le déchargement en cours de fabrication, dans la livraison, le stockage et la répartition des produits agricoles et industriels, alimentaires et forestiers.</p>
</td>
</tr>
</tbody>
</table>
