# Article Tableau n° 49

Affections dues aux rickettsies

<table>
<tbody>
<tr>
<td width="295">
<p align="center">DÉSIGNATION DES MALADIES</p>
</td>
<td width="96">
<p align="center">DÉLAI DE PRISE<br/>en charge</p>
</td>
<td width="290">
<p align="center">LISTE LIMITATIVE DES TRAVAUX SUSCEPTIBLES<br/>de provoquer ces maladies</p>
</td>
</tr>
<tr>
<td valign="top" width="295">
<p>A. - Rickettsioses : </p>
<p>Manifestations cliniques aiguës.</p>
</td>
<td rowspan="2" valign="top" width="96">
<p align="center">21 jours</p>
</td>
<td rowspan="2" valign="top" width="290">
<p>A. - Travaux effectués dans les laboratoires spécialisés en matière de rickettsies ou de production de vaccins. Travaux effectués en forêt de manière habituelle.</p>
</td>
</tr>
<tr>
<td valign="top" width="295">
<p>B. - Fièvre Q : </p>
</td>
</tr>
<tr>
<td valign="top" width="295">
<p>- manifestations cliniques aiguës ;</p>
</td>
<td valign="top" width="96">
<p align="center">21 jours</p>
</td>
<td rowspan="2" valign="top" width="290">
<p>B. - Travaux exposant au contact avec des bovins, caprins, ovins, leurs viscères ou leurs déjections. Travaux exécutés dans les laboratoires effectuant le diagnostic de la fièvre Q, ou des recherches biologiques vétérinaires.</p>
</td>
</tr>
<tr>
<td valign="top" width="295">
<p>- manifestations chroniques ; </p>
<p>- endocardite ; </p>
<p>- hépatite granulomateuse. </p>
<p>Dans toutes ces affections, A et B, le diagnostic doit être confirmé par un examen de laboratoire spécifique.</p>
</td>
<td valign="top" width="96">
<p align="center">10 ans</p>
</td>
</tr>
</tbody>
</table>
