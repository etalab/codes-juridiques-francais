# Article Tableau n° 25 bis

Affections cutanées cancéreuses provoquées par les suies de combustion des produits pétroliers

<table>
<tbody>
<tr>
<td width="265">
<p align="center">DÉSIGNATION DE LA MALADIE</p>
</td>
<td width="113">
<p align="center">DÉLAI DE PRISE<br/>en charge</p>
</td>
<td width="265">
<p align="center">LISTE LIMITATIVE DES TRAVAUX SUSCEPTIBLES<br/>de provoquer cette maladie</p>
</td>
</tr>
<tr>
<td valign="top" width="265">
<p>Epithéliomas primitifs de la peau.</p>
</td>
<td valign="top" width="113">
<p align="center">30 ans (sous réserve d'une durée d'exposition minimale de 10 ans)</p>
</td>
<td valign="top" width="265">
<p>Travaux de ramonage et de nettoyage de chaudières et de cheminées exposant aux suies de combustion de produits pétroliers.</p>
</td>
</tr>
</tbody>
</table>
