# Article R331-12

Dans un délai de six mois à compter de sa saisine, la commission des recours notifie à l'auteur du recours, par lettre recommandée avec accusé de réception, une décision motivée, mentionnant la possibilité d'un recours de pleine juridiction devant le tribunal administratif dans un délai de deux mois. Cette décision est également notifiée au préfet qui a infligé la sanction contestée.

Lorsque la commission a décidé qu'il y avait lieu à sanction pécuniaire, le préfet émet le titre exécutoire nécessaire à son recouvrement. Ce recouvrement est effectué selon les règles prévues pour les créances de l'Etat étrangères à l'impôt et au domaine.
