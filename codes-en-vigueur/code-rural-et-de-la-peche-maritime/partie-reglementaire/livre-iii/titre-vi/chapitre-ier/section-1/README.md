# Section 1 : Composition, mission et fonctionnement

- [Sous-section 1 : Gestion comptable et financière du Fonds national de gestion des risques en agriculture](sous-section-1)
- [Sous-section 2 : Comité national de gestion des risques en agriculture](sous-section-2)
- [Sous-section 3 : Comité départemental d'expertise.](sous-section-3)
