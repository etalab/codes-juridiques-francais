# Article D361-13

Le comité départemental d'expertise comprend, sous la présidence du préfet :

1° Le directeur départemental des finances publiques ;

2° Le directeur départemental des territoires ou le directeur départemental des territoires et de la mer ;

3° Le président de la chambre départementale d'agriculture ;

4° Un représentant de chacune des organisations syndicales d'exploitants agricoles à vocation générale habilitées en application de l'article 3 du décret n° 90-187 du 28 février 1990 susmentionné ;

5° Une personnalité désignée par la Fédération française des sociétés d'assurances ;

6° Une personnalité désignée par les caisses de réassurances mutuelles agricoles dans le ressort desquelles se trouve le département ;

7° Un représentant des établissements bancaires présents dans le département.

Les membres du comité mentionnés aux 4° à 7° sont pourvus chacun d'un suppléant.

Les membres du comité départemental d'expertise ainsi que, le cas échéant, leurs suppléants sont nommés, pour une durée de trois ans, par arrêté préfectoral. Le mandat des membres du comité peut être prolongé, dans la limite d'un an, par arrêté préfectoral.

Le comité départemental d'expertise se réunit sur convocation du préfet. Son secrétariat est assuré par les soins du directeur départemental des territoires ou du directeur départemental des territoires et de la mer.

Dans la région d'Ile-de-France, les compétences des comités techniques des départements de Paris et des Hauts-de-Seine, du département de la Seine-Saint-Denis et de celui du Val-de-Marne sont exercées respectivement par les comités départementaux d'expertise des Yvelines, du Val-d'Oise et de l'Essonne.

Les comités départementaux d'expertise fonctionnent dans les conditions prévues par les articles 3 à 15 du décret n° 2006-672 du 8 juin 2006 relatif à la création, à la composition et au fonctionnement de commissions administratives à caractère consultatif, à l'exception des articles 10 et 11.
