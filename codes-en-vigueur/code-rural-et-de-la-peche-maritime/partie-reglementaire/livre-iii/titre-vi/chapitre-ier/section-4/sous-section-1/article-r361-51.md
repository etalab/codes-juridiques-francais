# Article R361-51

Les fonds de mutualisation indemnisent des pertes économiques occasionnées par :

― les maladies animales figurant dans la liste des maladies animales établie par l'Organisation mondiale de la santé animale ou à l'annexe I de la décision 2009/470/ CE du Conseil du 25 mai 2009 relative à certaines dépenses dans le domaine vétérinaire ;

― les organismes nuisibles aux végétaux listés en application de l'article L. 251-3 du code rural et de la pêche maritime, faisant l'objet de mesures de lutte obligatoire ou présentant un caractère anormal ou exceptionnel.
