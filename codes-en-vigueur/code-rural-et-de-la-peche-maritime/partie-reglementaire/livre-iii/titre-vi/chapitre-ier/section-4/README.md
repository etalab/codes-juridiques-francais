# Section 4 : Mutualisation des risques sanitaires 
et environnementaux

- [Sous-section 1 : Conditions d'organisation et de fonctionnement des fonds de mutualisation susceptibles d'être agréés](sous-section-1)
- [Sous-section 2 : Modalités d'agrément des fonds de mutualisation](sous-section-2)
- [Sous-section 3 : Obligation d'affiliation à un fonds de mutualisation](sous-section-3)
- [Sous-section 4 : Conditions de l'intervention publique en faveur des fonds de mutualisation](sous-section-4)
