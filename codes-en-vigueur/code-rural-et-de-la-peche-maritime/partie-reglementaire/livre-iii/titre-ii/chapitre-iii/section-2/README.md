# Section 2 : Fonctionnement des groupements.

- [Article D323-31-1](article-d323-31-1.md)
- [Article R323-24](article-r323-24.md)
- [Article R323-25](article-r323-25.md)
- [Article R323-26](article-r323-26.md)
- [Article R323-27](article-r323-27.md)
- [Article R323-28](article-r323-28.md)
- [Article R323-29](article-r323-29.md)
- [Article R323-30](article-r323-30.md)
- [Article R323-31](article-r323-31.md)
- [Article R323-31-2](article-r323-31-2.md)
- [Article R323-32](article-r323-32.md)
- [Article R323-33](article-r323-33.md)
- [Article R323-34](article-r323-34.md)
- [Article R323-35](article-r323-35.md)
- [Article R323-36](article-r323-36.md)
- [Article R323-37](article-r323-37.md)
- [Article R323-38](article-r323-38.md)
- [Article R323-39](article-r323-39.md)
- [Article R323-40](article-r323-40.md)
- [Article R323-41](article-r323-41.md)
- [Article R323-42](article-r323-42.md)
- [Article R323-43](article-r323-43.md)
- [Article R323-44](article-r323-44.md)
