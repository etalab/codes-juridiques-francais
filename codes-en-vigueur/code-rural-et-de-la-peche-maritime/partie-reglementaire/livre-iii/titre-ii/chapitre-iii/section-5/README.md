# Section 5 : Conditions d'accès des groupements agricoles d'exploitation en commun totaux aux aides de la politique agricole commune

- [Article R323-52](article-r323-52.md)
- [Article R323-53](article-r323-53.md)
- [Article R323-54](article-r323-54.md)
