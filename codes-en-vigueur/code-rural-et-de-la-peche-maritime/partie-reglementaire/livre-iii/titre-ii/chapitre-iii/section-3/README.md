# Section 3 : Statut social et économique des groupements et de leurs membres.

- [Article R323-45](article-r323-45.md)
- [Article R323-46](article-r323-46.md)
- [Article R323-47](article-r323-47.md)
