# Article D372-10

<div align="left">Les articles D. 343-3 à D. 343-9, D. 343-11, D. 343-12 et D. 343-17 à D. 343-24 ne sont pas applicables à Mayotte. Les aides à l'installation sont régies par les articles D. 372-11 à D. 372-17.<br/>
<br/>
</div>
