# Article D372-18

<div align="left">Pour l'application à Mayotte des articles D. 353-4, D. 353-5 et D. 353-9 : <br/>
<br/>1° La référence aux articles L. 6341-5 et L. 6332-1 du code du travail est remplacée par la référence à l'article L. 711-1 du code du travail applicable à Mayotte ; <br/>
<br/>2° La référence à l'article R. 6332-59 du code du travail est remplacée par la référence à l'article L. 721-3 du code du travail applicable à Mayotte ; <br/>
<br/>3° La référence au chapitre II du titre IV du livre III de la sixième partie du code du travail est remplacée par la référence au chapitre II du titre II du livre VII du code du travail applicable à Mayotte ; <br/>
<br/>4° La référence au livre III de la sixième partie du code du travail est remplacée par la référence au livre VII du code du travail applicable à Mayotte.<br/>
<br/>
</div>
