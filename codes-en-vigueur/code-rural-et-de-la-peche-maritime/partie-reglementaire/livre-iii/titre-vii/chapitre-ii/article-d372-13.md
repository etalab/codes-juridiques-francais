# Article D372-13

<div align="left">Le projet d'installation est présenté par le candidat avant son installation et résumé au sein d'un plan de développement de l'exploitation, d'une durée de cinq ans, dans lequel il expose : <br/>
<br/>1° L'état de l'exploitation ; <br/>
<br/>2° Le revenu disponible agricole prévisionnel pour chaque année du plan ; <br/>
<br/>3° La situation financière du candidat ; <br/>
<br/>4° Les besoins de trésorerie ; <br/>
<br/>5° Les objectifs en matière de production, d'investissements, de financement et de commercialisation. <br/>
<br/>Lorsque les engagements sont souscrits sur neuf ans en application du 4° de l'article D. 372-12, le plan de développement de l'exploitation comporte en outre le calendrier et le plan de défrichement et de mise en cultures. <br/>
<br/>Le projet doit identifier les besoins de trésorerie et de financement des investissements. <br/>
<br/>Ce plan est produit sur la base de références techniques et économiques établies, par production et par région agricole, par la direction de l'alimentation, de l'agriculture et de la forêt. Ces références sont agréées par arrêté préfectoral. Le plan fait l'objet d'avenants en cas de modification importante du projet. Le préfet en assure le suivi et contrôle le respect des engagements souscrits.<br/>
<br/>
</div>
