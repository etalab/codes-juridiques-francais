# Article D371-14

I.-       En Guadeloupe, en Guyane, à la Martinique, à La Réunion, à Saint-Barthélemy et à Saint-Martin, par dérogation aux dispositions du dernier alinéa de l'article D. 341-3, la durée des prêts destinés à financer l'acquisition, la construction, l'agrandissement ou l'aménagement de logements à usage d'habitation principale pourra être portée à dix-huit ans.

II.-Ne peuvent bénéficier des prêts mentionnés au présent article que les personnes remplissant les conditions de ressources fixées conformément aux dispositions du premier alinéa de l'article 36 du décret n° 72-66 du 24 janvier 1972 relatif aux primes, aux bonifications d'intérêt et aux prêts à la construction.

III.-Les conditions d'application des dispositions du présent article, et notamment celles relatives au montant et aux taux d'intérêt maximum des prêts seront fixées, après avis du conseil d'administration de la Caisse nationale de crédit agricole, par arrêté conjoint du ministre chargé de l'économie, du ministre chargé de l'équipement et du ministre de l'agriculture.
