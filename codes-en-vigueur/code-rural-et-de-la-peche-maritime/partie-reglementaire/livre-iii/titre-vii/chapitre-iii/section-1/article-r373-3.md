# Article R373-3

Pour son application en Nouvelle-Calédonie, l'  article R. 323-3 est ainsi rédigé :

" Art. R. 323-3. - Les membres du comité prévu à l'article R. 323-1 applicable en Nouvelle-Calédonie sont nommés pour une durée de trois ans par le président du gouvernement de la Nouvelle-Calédonie ; un suppléant est nommé pour chacun d'eux dans les mêmes conditions.

" Le secrétariat de ce comité est assuré par le service de la Nouvelle-Calédonie chargé de l'agriculture. "
