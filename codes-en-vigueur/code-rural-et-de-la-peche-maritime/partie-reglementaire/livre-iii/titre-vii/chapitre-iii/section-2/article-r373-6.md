# Article R373-6

L'article D. 324-4 est applicable en Nouvelle-Calédonie, sous réserve de l'adaptation suivante : le montant de 30 000 euros est remplacé par le montant de 4 000 000 de francs CFP.
