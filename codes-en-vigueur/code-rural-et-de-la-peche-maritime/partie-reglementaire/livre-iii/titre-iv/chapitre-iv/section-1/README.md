# Section 1 : Dispositions générales.

- [Article D*344-2](article-d-344-2.md)
- [Article D*344-3](article-d-344-3.md)
- [Article D*344-4](article-d-344-4.md)
- [Article D*344-5](article-d-344-5.md)
- [Article D*344-6](article-d-344-6.md)
- [Article D*344-7](article-d-344-7.md)
