# Section 2 : Prêts pour l'amélioration de l'habitat rural

- [Sous-section 1 : Prêts à long terme des caisses de crédit agricole mutuel pour l'amélioration de l'habitat rural.](sous-section-1)
- [Sous-section 2 : Prêts bonifiés des caisses de crédit agricole mutuel en matière de logement.](sous-section-2)
