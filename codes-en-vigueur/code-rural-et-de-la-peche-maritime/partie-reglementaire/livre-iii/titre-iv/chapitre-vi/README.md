# Chapitre VI : Aides à l'habitat rural

- [Section 1 : Aides à la restauration de l'habitat rural](section-1)
- [Section 2 : Prêts pour l'amélioration de l'habitat rural](section-2)
