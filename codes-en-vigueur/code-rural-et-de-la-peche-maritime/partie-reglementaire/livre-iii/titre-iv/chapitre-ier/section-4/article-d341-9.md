# Article D341-9

La demande d'engagement agroenvironnemental est déposée auprès de la direction départementale de l'agriculture et de la forêt. Les modalités de présentation et la date limite de dépôt de la demande sont analogues à celles définies en application de l'article D. 615-1 pour le dépôt de la demande unique.

Sauf en cas de force majeure, toute réception d'une demande d'engagement agroenvironnemental après la date limite entraîne une réduction de 1 % par jour ouvrable de retard du montant annuel auquel le demandeur aurait eu droit si la demande avait été déposée dans le délai imparti. Lorsque le retard dépasse vingt-cinq jours, la demande est irrecevable.

A l'issue de l'instruction, le préfet arrête la décision d'engagement.

Lorsque la demande d'engagement agroenvironnemental porte sur un dispositif dit "déconcentré à cahier des charges national" ou "déconcentré zoné" au sens du troisième alinéa de l'article D. 341-7, le préfet prend sa décision après avis de la commission départementale d'orientation agricole.
