# Article D341-19

Lorsque la cession totale ou partielle de l'exploitation ne s'accompagne pas du transfert des engagements agroenvironnementaux correspondants, le remboursement de la totalité des paiements versés depuis le début de l'exécution de ces engagements est demandé au cédant.

Ce remboursement n'est pas demandé lorsque le cédant cesse définitivement ses activités agricoles après avoir rempli ses engagements pendant au moins trois années et s'il justifie que le transfert des engagements au cessionnaire n'est pas réalisable.

Si un cas de force majeure ou des circonstances exceptionnelles telles que définies à l'article D. 341-17 obligent le bénéficiaire à cesser définitivement l'exploitation d'une partie de sa ferme sans pouvoir transférer ses engagements, le remboursement des paiements versés n'est pas demandé.

En cas d'application de l'un des modes d'aménagement foncier défini au titre II du livre Ier du code rural, les engagements prévus sont adaptés à la nouvelle situation de l'exploitation. Si une telle adaptation n'est pas réalisable et que l'importance des obligations qui ne peuvent plus être respectées est telle que la cohérence de l'engagement agroenvironnemental est remise en cause, le préfet peut le résilier. Dans ce cas, le remboursement des paiements versés n'est pas demandé.
