# Article R343-34-1

Le silence gardé pendant plus de six mois par le préfet sur la demande d'aide à la transmission de l'exploitation mentionnée au e du 1° de l'article D. 343-34 vaut décision de rejet.
