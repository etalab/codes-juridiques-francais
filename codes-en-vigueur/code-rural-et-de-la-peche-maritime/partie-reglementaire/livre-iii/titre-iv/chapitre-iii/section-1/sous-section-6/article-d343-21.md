# Article D343-21

Le candidat aux aides à l'installation doit pouvoir disposer, dans son département :

a) Des informations utiles relatives à l'installation.

A cet effet, le préfet de département, après consultation du comité départemental à l'installation, confère pour une durée de trois ans, après appel à candidature et avis de la commission départementale d'orientation de l'agriculture et sur la base d'un cahier des charges national, le label " Point info installation " à une structure départementale chargée :

-d'accueillir toute personne souhaitant s'installer à court ou moyen terme en agriculture ;

-d'informer les candidats sur toutes les questions liées à une première installation et aux différentes formes d'emploi et de formation en agriculture, ainsi que sur les conditions d'éligibilité aux aides à l'installation en agriculture, les conditions de mise en œuvre du plan de professionnalisation personnalisé et les possibilités de prise en charge des actions à réaliser dans le cadre du plan de professionnalisation personnalisé ;

-de proposer aux candidats les organismes techniques ou de formation susceptibles de les accompagner dans l'élaboration de leur projet.

b) D'une assistance pour la réalisation du plan de professionnalisation personnalisé prévu au b du 4° de l'article D. 343-4.

A cet effet, dans chaque département, le préfet, après appel à candidature, confère pour une durée de trois ans, sur la base d'un cahier des charges national, le label " Centre d'élaboration du plan de professionnalisation personnalisé " à un organisme ou un réseau d'organismes chargé de conduire les procédures d'élaboration et de mise en œuvre du plan de professionnalisation personnalisé.

Les candidats aux aides à l'installation relevant des collectivités territoriales peuvent bénéficier de cet accompagnement si les collectivités territoriales le prévoient.
