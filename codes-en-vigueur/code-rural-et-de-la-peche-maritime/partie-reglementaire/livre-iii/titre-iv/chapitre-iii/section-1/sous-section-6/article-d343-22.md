# Article D343-22

Le plan de professionnalisation personnalisé, prévu à l'article D. 343-4, est agréé par le préfet dans des conditions fixées par arrêté du ministre chargé de l'agriculture.
