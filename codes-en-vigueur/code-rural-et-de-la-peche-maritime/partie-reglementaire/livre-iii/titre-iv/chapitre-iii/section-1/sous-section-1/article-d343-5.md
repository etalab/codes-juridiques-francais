# Article D343-5

Le jeune agriculteur, candidat aux aides mentionnées à l'article D. 343-3, doit en outre :

1° Présenter un projet de première installation ;

2° S'installer sur une exploitation constituant une unité économique indépendante et disposant, dans le cas d'une production hors-sol, d'une superficie minimale déterminée par le préfet après avis de la commission départementale d'orientation de l'agriculture.L'exploitation doit être gérée distinctement de toute autre, sous réserve des dispositions propres aux sociétés, et comporter ses propres bâtiments d'exploitation et des moyens de production suffisants ;

3° Présenter un projet d'installation viable au terme de la cinquième année suivant l'installation sur la base d'un plan de développement de l'exploitation au sens de l'article D. 343-7;

4° S'engager à mettre en œuvre le plan de développement de l'exploitation mentionné au 3° du présent article validé par le préfet ;

5° S'engager à exercer dans un délai d'un an et pendant cinq ans la profession d'agriculteur en qualité de chef d'exploitation sur un fonds répondant aux conditions fixées par la présente section en retirant au moins 50 % de son revenu professionnel global d'activités agricoles au sens de l'article L. 311-1. Le bénéficiaire des aides s'engage à mettre en valeur personnellement son exploitation et à participer effectivement aux travaux pendant cinq ans ;

6° S'engager pendant la même période à tenir une comptabilité de gestion de son exploitation correspondant aux normes du plan comptable général agricole et la transmettre au préfet au terme du plan de développement de l'exploitation et avant le terme de la sixième année suivant l'installation ;

7° S'engager à avoir réalisé les travaux éventuellement exigés par la réglementation relative à la protection de l'environnement en vue de la mise en conformité des équipements repris et à satisfaire aux normes minimales requises en matière d'hygiène et de bien-être des animaux, dans un délai de trois ans ;

8° S'il bénéficie d'un prêt à moyen terme spécial, s'engager à conserver le bien faisant l'objet du prêt pour un usage identique pendant au moins cinq ans.
