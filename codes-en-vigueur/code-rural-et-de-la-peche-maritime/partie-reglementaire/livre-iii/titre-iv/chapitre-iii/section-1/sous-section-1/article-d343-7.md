# Article D343-7

Le plan de développement de l'exploitation mentionnée au 3° de l'article D. 343-5 expose notamment l'état de l'exploitation, sa situation juridique, ses orientations économiques principales, l'ensemble des moyens de production dont l'exploitation dispose et la main-d'œuvre. Le plan de développement de l'exploitation prévoit les étapes de développement des activités. Il précise les prévisions en matière de production et de commercialisation ainsi que les investissements correspondant au développement des activités et, le cas échéant, ceux relatifs à la mise aux normes. Ces investissements sont évalués sur la base de coûts raisonnables.

Le plan de développement de l'exploitation comporte également une simulation du revenu prévisionnel de l'exploitation pendant les cinq premières années d'activité.

Un arrêté du ministre chargé de l'agriculture précise les conditions d'établissement du plan de développement de l'exploitation.
