# Article D352-17

Les agriculteurs, reconnus en difficulté, qui doivent suivre une formation en vue de leur réinsertion professionnelle perçoivent une rémunération égale à la rémunération versée aux travailleurs non salariés en application des dispositions de l'article L. 961-6 du code du travail. Lorsque le stage prévu ne bénéficie pas déjà d'un agrément au titre de l'article L. 961-3 du code du travail, il est agréé par le préfet de région dans les conditions prévues à l'article R. 961-2 et dans les limites d'un volume de mois stagiaires fixé par le ministre de l'agriculture. Une contribution de l'Etat à la prise en charge des frais de formation peut en outre être accordée dans le cadre d'une convention passée à cet effet avec le centre de formation.

La rémunération des stages agréés par le préfet de région est liquidée et payée par           l'Agence de services et de paiement et fait l'objet d'une comptabilité séparée.
