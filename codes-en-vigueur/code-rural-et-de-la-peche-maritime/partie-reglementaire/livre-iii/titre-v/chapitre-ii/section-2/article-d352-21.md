# Article D352-21

Les postulants au bénéfice des dispositions de la présente section doivent :

1° Justifier qu'ils ont exercé à titre principal une activité agricole pendant une durée d'au moins cinq ans précédant immédiatement la date de dépôt de la demande, ou avoir participé effectivement aux travaux de l'exploitation pendant cette durée ;

2° S'engager à ne plus revenir à l'agriculture en qualité de chef d'exploitation. Lorsque le bénéficiaire cesse de remplir cet engagement, il peut être contraint de rembourser la prime de départ qu'il a perçue assortie des intérêts au taux légal.

Les avantages prévus à la présente section ne peuvent se cumuler avec ceux énoncés au décret n° 69-189 du 26 février 1969.
