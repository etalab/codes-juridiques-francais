# Article R352-1

Lorsque l'acte déclaratif d'utilité publique prévoit l'application des dispositions de l'article L. 352-1, le maître de l'ouvrage est tenu, dans les conditions précisées aux articles ci-après, de participer financièrement soit à la réinstallation sur des exploitations nouvelles, soit à la reconversion de l'activité des exploitants agricoles dont les exploitations sont supprimées ou déséquilibrées du fait des expropriations auxquelles il est procédé en vue de la réalisation des aménagement ou ouvrages mentionnés à l'article 2 de la loi n° 76-629 du 10 juillet 1976 relative à la protection de la nature et non dispensés de l'obligation d'une étude d'impact par le décret n° 77-1141 du 12 octobre 1977 pris pour l'application de cette loi.

La procédure d'expropriation et celle organisée par la présente section se déroulent indépendamment l'une de l'autre. La fixation des indemnités d'expropriation, leur paiement ou leur consignation et la prise de possession des biens expropriés interviennent conformément au droit commun, quel que soit l'état, à leur date, de la liquidation et du versement des participations prévues à l'alinéa qui précède.

Doivent être considérés comme exploitants agricoles pour l'application de la présente section les personnes qui satisfont aux conditions mentionnées aux I, II et III de l'article 1003-7-1 du code rural.
