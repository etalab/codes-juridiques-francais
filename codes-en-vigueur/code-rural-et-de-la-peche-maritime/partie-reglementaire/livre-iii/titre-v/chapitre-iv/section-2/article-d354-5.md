# Article D354-5

La situation de l'exploitation fait l'objet d'un diagnostic économique et financier afin d'évaluer sa pérennité et de définir les moyens à mettre en œuvre pour assurer son redressement. Ce diagnostic est réalisé par un expert choisi par l'exploitant sur une liste établie par le préfet et doit comporter :

1° Les éléments permettant d'apprécier la structure financière de l'exploitation et les causes de ses difficultés ;

2° Une analyse des conditions qui pourraient permettre le retour à la viabilité de l'exploitation.
