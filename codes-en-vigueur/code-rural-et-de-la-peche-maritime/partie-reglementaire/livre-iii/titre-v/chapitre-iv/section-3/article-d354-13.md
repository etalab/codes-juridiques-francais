# Article D354-13

Le suivi prévu à l'article D. 354-8 est financé en partie par l'aide prévue au 3° de l'article D. 354-1 dont le montant forfaitaire est fixé par arrêté conjoint du ministre chargé de l'agriculture et du ministre chargé du budget.

Cette aide est versée à l'expert qui a réalisé le suivi technico-économique de l'exploitation.
