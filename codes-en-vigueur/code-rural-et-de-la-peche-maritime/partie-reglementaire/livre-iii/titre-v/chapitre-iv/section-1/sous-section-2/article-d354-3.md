# Article D354-3

Pour bénéficier des aides prévues à l'article D. 354-1, l'exploitation du demandeur doit :

1° Prendre la forme soit d'une exploitation agricole individuelle dont la main-d'œuvre est constituée du chef d'exploitation, du conjoint ou du partenaire concubin ou pacsé ou des aides familiaux, soit d'un groupement agricole d'exploitations en commun (GAEC), soit d'une personne morale dont l'objet est exclusivement agricole à condition que 50 % du capital social soit détenu par des agriculteurs répondant aux conditions fixées à l'article D. 354-2 ;

2° Employer au moins une unité de travail non salariée. Une personne travaillant sur l'exploitation ne peut être prise en compte pour plus d'une unité de travail. Les membres de la famille de l'exploitant ne peuvent être pris en compte que si leur participation aux travaux de l'exploitation représente au moins une demi-unité de travail. Ils sont pris en compte au prorata de leur activité ;

3° Ne pas employer annuellement une main-d'œuvre salariée permanente ou saisonnière supérieure à dix unités de travail équivalent temps plein ;

4° Avoir dégagé, sur la moyenne des trois derniers exercices, par unité de travail non salariée, un revenu inférieur à un SMIC net annuel déterminé au 1er janvier de l'année du dépôt du dossier ou, s'il est différent, au revenu d'objectif fixé au niveau départemental pour reconnaître la viabilité des projets d'installation ;

5° Justifier de difficultés économiques et financières ne lui permettant pas d'assurer son redressement avec ses propres ressources.
