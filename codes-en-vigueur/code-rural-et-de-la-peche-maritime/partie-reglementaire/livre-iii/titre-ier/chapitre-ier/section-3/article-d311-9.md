# Article D311-9

I. ― Les demandes d'immatriculation sont déposées auprès de la chambre d'agriculture dans le ressort de laquelle se trouve le siège de l'exploitation, en deux exemplaires et accompagnées des pièces dont la liste est définie par arrêté du ministre chargé de l'agriculture.

II. ― Toutefois, lorsque l'intéressé a sollicité auprès d'un centre de formalités des entreprises autre que celui mentionné au 6° de l'article R. 123-3 du code de commerce son immatriculation au registre du commerce et des sociétés ou au répertoire des métiers et de l'artisanat, il demande à ce centre, en vue de son immatriculation au registre de l'agriculture, de transmettre les pièces mentionnées au I à la chambre de l'agriculture dans le ressort de laquelle est situé le siège de l'exploitation.
