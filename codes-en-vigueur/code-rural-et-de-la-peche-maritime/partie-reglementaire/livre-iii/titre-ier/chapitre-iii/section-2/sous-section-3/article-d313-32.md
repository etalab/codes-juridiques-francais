# Article D313-32

La  comptabilité budgétaire de l'agence retrace, le cas échéant, dans la  partie II du budget, les engagements pris au profit des bénéficiaires  des transferts par les mandants de l'agence, sous leur seule  responsabilité. En cas de carence de la part des mandants dans la  notification des engagements pris, l'ordonnateur enregistre par défaut  un montant d'engagements correspondant aux dépenses ordonnancées.

Le système d'information de l'agence doit  garantir la traçabilité de tous les engagements notifiés par les  mandants jusqu'au bénéficiaire final, notamment celle des autorisations  d'engagement ouvertes au budget de l'Etat et notifiées à l'agence par  l'Etat.
