# Section 3 : Commission régionale de l'économie agricole et du monde rural.

- [Article R313-45](article-r313-45.md)
- [Article R313-46](article-r313-46.md)
- [Article R313-47](article-r313-47.md)
