# Chapitre III : Instruments

- [Section 1 : Commission départementale d'orientation de l'agriculture.](section-1)
- [Section 2 : L'Agence de services et de paiement.](section-2)
- [Section 3 : Commission régionale de l'économie agricole et du monde rural.](section-3)
