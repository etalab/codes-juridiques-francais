# Article R313-2

La commission départementale d'orientation de l'agriculture est placée sous la présidence du préfet ou de son représentant et comprend :

1° Le président du conseil régional ou son représentant ;

2° Le président du conseil départemental ou son représentant ;

3° Un président d'établissement public de coopération inter-communale ayant son siège dans le département ou son représentant ou, le cas échéant, le représentant d'un syndicat mixte de gestion d'un parc naturel régional ou de pays ;

4° Le directeur départemental de l'agriculture et de la forêt ou son représentant ;

5° Le trésorier-payeur général ou son représentant ;

6° Trois représentants de la chambre d'agriculture, dont un au titre des sociétés coopératives agricoles autres que celles mentionnées au 8° ;

7° Le président de la caisse de mutualité sociale agricole ou son représentant ou, dans les départements d'outre-mer, le président de la caisse générale de sécurité sociale ou son représentant ;

8° Deux représentants des activités de transformation des produits de l'agriculture, dont un au titre des entreprises agroalimentaires non coopératives, l'autre au titre des coopératives ;

9° Huit représentants des organisations syndicales d'exploitants agricoles à vocation générale habilitées en application de l'article 1er du décret n° 90-187 du 28 février 1990 relatif à la représentation des organisations syndicales d'exploitants agricoles au sein de certains organismes ou commissions, dont au moins un représentant de chacune d'elles ;

10° Un représentant des salariés agricoles présenté par l'organisation syndicale de salariés des exploitations agricoles la plus représentative au niveau départemental ;

11° Deux représentants de la distribution des produits agroalimentaires, dont un au titre du commerce indépendant de l'alimentation ;

12° Un représentant du financement de l'agriculture ;

13° Un représentant des fermiers-métayers ;

14° Un représentant des propriétaires agricoles ;

15° Un représentant de la propriété forestière ;

16° Deux représentants des associations agréées pour la protection de l'environnement ;

17° Un représentant de l'artisanat ;

18° Un représentant des consommateurs ;

19° Deux personnes qualifiées ;

20° S'il y a lieu, un représentant de l'établissement public du parc national situé pour tout ou partie dans le département.

Les membres de la commission pour lesquels la possibilité de se faire représenter n'est pas prévue sont pourvus chacun de deux suppléants.
