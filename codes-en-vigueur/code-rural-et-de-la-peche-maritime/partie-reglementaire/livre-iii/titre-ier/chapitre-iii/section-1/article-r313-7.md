# Article R313-7

En Corse, les sections sont placées sous la coprésidence du représentant de l'Etat dans la collectivité territoriale de Corse et du président du conseil exécutif ou leurs représentants.

Sont membres de toutes les sections :

-les présidents des conseils départementaux ou leurs représentants ;

-les directeurs départementaux de l'agriculture et de la forêt ou leurs représentants ;

-les trésoriers-payeurs généraux ou leurs représentants ;

-le président de l'Assemblée de Corse ou son représentant ;

-le président de l'ODARC ou son représentant ;

-les présidents des chambres d'agriculture ou leurs représentants ;

-les six représentants des organisations syndicales agricoles à vocation générale mentionnées à l'article 1er du décret n° 90-187 du 28 février 1990.

Le représentant de l'Etat dans la collectivité territoriale de Corse désigne les autres membres de la Commission appelés à siéger dans chaque section en fonction de son objet, conformément à l'avis de la commission territoriale d'orientation agricole.
