# Article D315-6

<div align="left">Si des modifications substantielles interviennent dans le projet mentionné à l'article D. 315-2, la personne morale reconnue comme groupement d'intérêt économique et environnemental en informe immédiatement le préfet de région.<br/>
<br/>
<br/>
<br/>
</div>
