# Article D315-3

<div align="left">Le préfet de région recueille l'avis de la commission régionale de l'économie agricole et du monde rural mentionnée à l'article R. 313-45 sur les projets présentés. <br/>
<br/>La reconnaissance comme groupement d'intérêt économique et environnemental est accordée, après avis du président du conseil régional, par arrêté du préfet de région publié au recueil des actes administratifs de la préfecture de région.<br/>
<br/>
<br/>
<br/>
</div>
