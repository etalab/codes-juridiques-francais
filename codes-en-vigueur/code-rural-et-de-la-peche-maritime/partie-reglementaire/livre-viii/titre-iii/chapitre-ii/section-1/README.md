# Section 1 : Dispositions générales.

- [Article R832-1](article-r832-1.md)
- [Article R832-2](article-r832-2.md)
- [Article R832-3](article-r832-3.md)
