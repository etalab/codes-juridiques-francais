# Article R832-15

Le conseil scientifique et technique est l'instance de réflexion et de proposition de l'institut en matière de politique scientifique et technologique.

Il assiste le président de l'institut qui le consulte sur :

1° Les questions mentionnées aux 1° et 4° de l'article R. 832-6 ;

2° La création, la modification et la suppression des départements de l'institut ;

3° La nomination des responsables des départements, le renouvellement de leurs fonctions ou la décision d'y mettre fin ;

4° La création, la modification et la suppression des unités de recherche de l'institut ;

5° La nomination des responsables des unités de recherche, le renouvellement de leurs fonctions ou la décision d'y mettre fin ;

6° La création, la composition, la modification, la suppression et les modalités de fonctionnement des commissions spécialisées de l'institut ;

7° Les grandes orientations des actions de valorisation, d'information et de formation menées ou organisées par l'institut.

Il peut être consulté par le conseil d'administration ou le président de l'institut sur toute question relevant de la compétence de l'institut et notamment sur la situation et les perspectives de développement de la recherche dans le champ d'intervention de l'institut.

Il comprend des personnalités scientifiques extérieures à l'institut, notamment étrangères, des responsables scientifiques et techniques de la recherche publique, de l'administration, de l'enseignement supérieur et des secteurs économiques et sociaux concernés par les domaines d'activités de l'institut ainsi que des représentants élus du personnel.

Il se réunit au moins deux fois par an sur convocation du président de l'institut ou à la demande écrite et motivée des deux tiers de ses membres.

Sa composition, les modalités d'élection et de désignation de ses membres et de son président, la durée des mandats et les règles de son fonctionnement sont fixées par arrêté conjoint du ministre chargé de la recherche, du ministre chargé de l'agriculture et du ministre chargé de l'environnement.
