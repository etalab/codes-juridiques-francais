# Article R832-11

Les unités de recherche et les unités de service de l'établissement sont créées, modifiées ou supprimées par décision du président de l'institut, après avis du conseil scientifique et technique pour les unités de recherche.

Elles relèvent, au plan scientifique et technique, de départements et sont regroupées géographiquement en centres.

Elles reçoivent, sous forme de dotations globales, les crédits qui leur sont alloués au titre du fonctionnement, des missions, du petit et moyen équipement.

Les responsables d'unités de recherche et les responsables d'unités de service de l'institut sont nommés par décision du président de l'institut, après avis du conseil scientifique et technique pour les responsables d'unités de recherche. La durée de leur mandat est de quatre ans, renouvelable deux fois en qualité de responsable de la même unité.
