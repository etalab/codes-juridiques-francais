# Article R831-11

Le conseil scientifique est l'instance de réflexion et de proposition de l'institut en matière de politique scientifique, ainsi que d'évaluation des activités de recherche.

Il étudie la situation et les perspectives de développement dans les domaines de la recherche agronomique, et veille à ce que soit assurée une bonne coordination entre l'institut et les autres organismes de recherche intéressés.

Il donne son avis sur :

1. L'organisation scientifique de l'institut, et notamment la liste des départements de recherche ;

2. Le contenu et l'exécution des programmes de recherche, des études et travaux de l'institut ;

3. La nomination des directeurs scientifiques et des chefs de départements.

Le conseil scientifique peut être assisté par :

a) Des commissions spécialisées ou par discipline, créées par décision du président de l'institut ;

b) Des groupes de travail ou comités restreints, constitués en son sein, en vue de l'aider à remplir les tâches qui lui sont dévolues.

Ces différentes instances doivent rendre compte périodiquement au conseil scientifique des conclusions de leurs travaux.
