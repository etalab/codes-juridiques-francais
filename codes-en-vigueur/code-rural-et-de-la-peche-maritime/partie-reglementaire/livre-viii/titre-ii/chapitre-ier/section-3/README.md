# Section 3 : Dispositions particulières aux départements d'outre-mer.

- [Article D821-15](article-d821-15.md)
- [Article R821-16](article-r821-16.md)
