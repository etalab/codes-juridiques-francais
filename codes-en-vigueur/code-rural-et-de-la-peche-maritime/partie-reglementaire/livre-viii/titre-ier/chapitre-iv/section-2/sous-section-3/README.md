# Sous-section 3 : Fonctionnement.

- [Article R814-25](article-r814-25.md)
- [Article R814-26](article-r814-26.md)
- [Article R814-27](article-r814-27.md)
- [Article R814-28](article-r814-28.md)
- [Article R814-29](article-r814-29.md)
- [Article R814-30](article-r814-30.md)
