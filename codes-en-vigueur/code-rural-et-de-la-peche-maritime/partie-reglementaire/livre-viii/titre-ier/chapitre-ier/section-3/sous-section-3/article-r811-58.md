# Article R811-58

L'agent comptable est nommé par arrêté conjoint du ministre chargé du budget et du ministre de l'agriculture après information de la collectivité de rattachement par le préfet de région. En application de l'             article 14 du décret n° 2012-1246 du 7 novembre 2012 relatif à la gestion budgétaire et comptable publique, il prête serment devant la chambre régionale des comptes.
