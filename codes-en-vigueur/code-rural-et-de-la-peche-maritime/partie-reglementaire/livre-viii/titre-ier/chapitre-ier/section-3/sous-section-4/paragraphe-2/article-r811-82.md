# Article R811-82

Toute atteinte aux personnes ou aux biens peut donner lieu à une sanction disciplinaire.
