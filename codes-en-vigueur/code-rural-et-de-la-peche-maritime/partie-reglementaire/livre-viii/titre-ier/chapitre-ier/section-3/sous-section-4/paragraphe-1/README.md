# Paragraphe 1 : Les droits.

- [Article R811-77](article-r811-77.md)
- [Article R811-78](article-r811-78.md)
- [Article R811-79](article-r811-79.md)
- [Article R811-80](article-r811-80.md)
- [Article R811-81](article-r811-81.md)
