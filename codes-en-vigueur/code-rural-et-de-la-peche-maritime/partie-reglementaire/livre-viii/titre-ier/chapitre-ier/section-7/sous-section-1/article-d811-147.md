# Article D811-147

I. - Les candidats mineurs au 31 décembre de l'année de l'examen ne peuvent postuler le certificat d'aptitude professionnelle agricole que s'ils justifient avoir suivi la préparation, conformément aux II, III, IV et V ci-après.

II. - Le certificat d'aptitude professionnelle agricole est accessible par la voie scolaire :

a) Aux candidats ayant effectué un cycle d'études de deux ans à l'issue d'une classe de troisième.

Pour les établissements privés assurant des formations selon les modalités définies à l'article L. 813-9, le cycle d'études comprend une durée totale d'au moins 800 heures effectuées dans le centre de formation.

Toutefois, le cycle d'études peut être d'un an à l'issue d'une classe de troisième préparatoire au certificat d'aptitude professionnelle du secteur agricole ou d'une classe de troisième préparatoire aux certificats d'aptitude professionnelle agricole selon un rythme approprié lorsque cette disposition est prévue, après avis de la ou des commissions professionnelles consultatives concernées, par l'arrêté cité au II de l'article D. 811-146. Dans ce cas, pour les établissements privés assurant des formations selon les modalités définies à l'article L. 813-9, la durée de la formation en centre ne peut être inférieure à 600 heures ;

b) Aux candidats titulaires d'un certificat d'aptitude professionnelle, d'un brevet d'études professionnelles, d'un diplôme de niveau supérieur ainsi qu'aux candidats justifiant d'un niveau de scolarité de fin de seconde du second cycle de l'enseignement secondaire. Ces candidats sont dispensés de la première année du cycle d'études lorsque celui-ci est de deux ans.

Les formations mentionnées aux a et b du présent article sont dispensées dans :

1° Des établissements publics locaux ou nationaux de l'enseignement professionnel agricole ;

2° Des établissements privés ayant passé, pour la formation considérée, un contrat au titre des articles L. 813-8 et L. 813-9 ;

3° Des établissements relevant d'autres ministères, après avis du Conseil national de l'enseignement agricole, en fonction de critères spécifiques, sur la base d'une convention passée avec le ministre de l'agriculture ;

4° Tout autre établissement privé.
