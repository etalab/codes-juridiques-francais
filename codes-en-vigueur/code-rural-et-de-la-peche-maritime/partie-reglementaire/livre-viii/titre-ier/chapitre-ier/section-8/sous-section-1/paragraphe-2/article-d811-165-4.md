# Article D811-165-4

Le diplôme peut être délivré selon la modalité des unités capitalisables ou sous la forme d'un examen composé d'épreuves terminales.

Lorsque le diplôme est délivré selon la modalité des unités capitalisables, le candidat, pour être déclaré admis, doit avoir obtenu toutes les unités du brevet professionnel. Les modalités de préparation au brevet professionnel et de sa délivrance selon le dispositif des unités capitalisables sont définies par arrêté du ministre de l'agriculture.

Lorsque le diplôme est délivré selon la modalité des épreuves terminales, l'examen conduisant à sa délivrance est organisé à partir du référentiel caractéristique du diplôme. Un arrêté du ministre de l'agriculture fixe pour chaque option la liste, la nature et la durée des épreuves.
