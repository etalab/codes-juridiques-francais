# Article D811-167-3

Le certificat de spécialisation agricole est obtenu :

a) Par la voie de l'apprentissage dans les conditions définies au livre Ier du code du travail.

b) Par la voie de la formation continue dans les conditions définies au livre IX du code du travail.

c) Par la voie de la validation des acquis de l'expérience.

Lorsque le certificat de spécialisation agricole est demandé par la voie de l'apprentissage ou de la formation continue, les candidats doivent justifier :

1. De l'équivalent d'une année d'activité professionnelle à temps plein à la date d'évaluation de la dernière unité capitalisable ou de la première épreuve terminale. Au titre de cette année d'activité peut être pris en compte la durée d'un contrat de travail de type particulier en alternance ou en apprentissage ;

2. Et lors de leur entrée en formation :

a) Soit de la possession de l'un des diplômes ou titres inscrits au répertoire national des certifications professionnelles figurant sur la liste fixée par l'arrêté de création de l'option ;

b) Soit de la possession d'un autre diplôme ou titre inscrit au répertoire national des certifications professionnelles et de niveau au moins équivalent à celui du diplôme de référence, mais dont le contenu n'est pas en rapport avec celui-ci, sous réserve d'une durée de formation plus longue précisée à l'article D. 811-167-4 ;

c) Soit de l'équivalent d'une année d'activité professionnelle à temps plein dans un emploi en rapport direct avec le contenu et le niveau de l'un des diplômes ou titres en permettant l'accès, ou de l'équivalent de trois années à temps plein dans un autre emploi. Ils doivent en outre satisfaire aux évaluations de pré-requis organisées par le centre. Les périodes effectuées lors d'un contrat de travail de type particulier en alternance ou en apprentissage, ou lors d'un stage d'application mentionné à l'article R. 343-4 du code rural et de la pêche maritime peuvent être prises en compte dans cette durée.

Lorsque l'accès au certificat de spécialisation agricole est demandé par la voie de la validation des acquis de l'expérience, les candidats doivent justifier d'une durée totale cumulée équivalente à au moins trois années d'activité professionnelle salariée, non salariée ou bénévole, en rapport direct avec le contenu de l'option du certificat de spécialisation agricole concernée.

Le directeur régional de l'alimentation, de l'agriculture et de la forêt pour les départements d'outre-mer ou le directeur départemental de l'agriculture et de la forêt détermine la recevabilité des justificatifs présentés.

De plus, une décision dérogatoire à l'entrée en formation peut être accordée par le directeur régional de l'alimentation, de l'agriculture et de la forêt pour les départements d'outre-mer ou par le directeur départemental de l'agriculture et de la forêt dans les cas suivants :

a) Aux candidats qui ne possèdent pas l'un des diplômes ou titres figurant sur la liste fixée par l'arrêté de création de l'option du certificat de spécialisation agricole mais qui justifient du suivi de la formation complète y conduisant, après examen de leur dossier intégrant les autres formations suivies ou les activités exercées ;

b) Aux candidats justifiant d'un diplôme ou titre obtenu en France ou à l'étranger autre que ceux figurant sur l'arrêté de création de l'option du certificat de spécialisation agricole, de niveau au moins équivalent et dont le contenu est en rapport avec celui du diplôme de référence.
