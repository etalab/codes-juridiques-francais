# Article D811-166-8

Le jury déclare admis après délibération et, le cas échéant, après examen du dossier individuel de suivi pédagogique ou du dossier de validation des acquis de l'expérience, les candidats ayant satisfait à l'ensemble des conditions d'obtention du diplôme.

Pour l'obtention du diplôme, les unités capitalisables obtenues ont une durée de validité limitée à cinq ans à compter de leur date de délivrance.

L'obtention d'une unité capitalisable ou d'un certificat peut faire l'objet de la délivrance d'une attestation de réussite.

Les candidats ajournés à l'issue de la présentation de la totalité des unités capitalisables ou, en cas de dépassement de la limite de validité d'unités capitalisables obtenues, doivent se réinscrire à l'examen pour présenter les unités manquantes.

Les conditions de toute nouvelle présentation à une unité capitalisable après échec sont fixées par arrêté du ministre chargé de l'agriculture.
