# Chapitre II : Dispositions propres à l'enseignement supérieur agricole et vétérinaire public

- [Section 1 : Les établissements d'enseignement supérieur agricole publics](section-1)
- [Section 2 : Formation et recherche](section-2)
- [Section 3 : Enseignement supérieur vétérinaire](section-3)
