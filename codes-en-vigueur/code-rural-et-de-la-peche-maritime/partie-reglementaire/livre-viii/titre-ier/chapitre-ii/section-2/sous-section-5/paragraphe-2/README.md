# Paragraphe 2 : Dispositions particulières aux élèves étrangers.

- [Article R812-42](article-r812-42.md)
- [Article R812-43](article-r812-43.md)
- [Article R812-44](article-r812-44.md)
- [Article R812-45](article-r812-45.md)
- [Article R812-46](article-r812-46.md)
- [Article R812-47](article-r812-47.md)
- [Article R812-48](article-r812-48.md)
