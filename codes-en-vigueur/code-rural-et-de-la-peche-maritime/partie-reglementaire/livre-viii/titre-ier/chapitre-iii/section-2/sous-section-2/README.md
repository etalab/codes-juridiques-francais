# Sous-section 2 : Dispositions particulières aux établissements mentionnés à l'article L. 813-9

- [Paragraphe 1 : Contrats entre l'Etat et les établissements.](paragraphe-1)
- [Paragraphe 2 : Obligations et garanties des formateurs.](paragraphe-2)
- [Paragraphe 3 : Stages et périodes de formation en milieu professionnel](paragraphe-3)
