# Article D840-12

<div align="left">Pour l'application à Mayotte de l'article D. 811-167-3 : <br/>
<br/>1° La référence au titre Ier du livre Ier du code du travail est remplacée par la référence au titre Ier du livre Ier du code du travail applicable à Mayotte ; <br/>
<br/>2° La référence au livre IX du code du travail est remplacée par la référence au livre VII du code du travail applicable à Mayotte.<br/>
<br/>
</div>
