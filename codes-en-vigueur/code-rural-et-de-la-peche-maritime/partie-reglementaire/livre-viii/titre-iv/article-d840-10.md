# Article D840-10

<div align="left">Pour l'application à Mayotte des articles D. 811-166-3 et D. 811-166-4 : <br/>
<br/>1° La référence au titre Ier du livre Ier du code du travail est remplacée par la référence au titre Ier du livre Ier du code du travail applicable à Mayotte ; <br/>
<br/>2° La référence au livre IX du code du travail est remplacée par la référence au livre VII du code du travail applicable à Mayotte.<br/>
<br/>
</div>
