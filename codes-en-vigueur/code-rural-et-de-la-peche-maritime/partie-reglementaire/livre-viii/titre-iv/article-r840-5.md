# Article R840-5

<div align="left">Pour l'application à Mayotte de l'article R. 811-157, la référence au livre IX du code du travail est remplacée par la référence au livre VII du code du travail applicable à Mayotte.<br/>
<br/>
</div>
