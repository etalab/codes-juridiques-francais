# Article D840-6

<div align="left">Pour l'application à Mayotte de l'article D. 811-158, les mots : " L. 920-1 du livre IX du code du travail ” et les mots : " L. 980-2 du livre IX du code du travail ” sont remplacés respectivement par les mots : " L. 733-1 du code du travail applicable à Mayotte ” et " L. 711-5 du même code ”, et la référence au titre Ier du livre Ier du code du travail est remplacée par la référence au titre Ier du livre Ier du code du travail applicable à Mayotte.<br/>
<br/>
</div>
