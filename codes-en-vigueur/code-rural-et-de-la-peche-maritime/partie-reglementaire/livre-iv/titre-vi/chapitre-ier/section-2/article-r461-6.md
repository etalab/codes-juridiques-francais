# Article R461-6

La décision administrative prévue à l'article L. 461-4 est prise par arrêté du préfet du département, ou à Saint-Barthélemy, Saint-Martin et Saint-Pierre-et-Miquelon, du représentant de l'Etat dans la collectivité après avis de la commission consultative des baux ruraux.

Au cas où cette commission consultative n'a pas émis l'avis dans les deux mois qui suivent la demande du préfet du département, ou à Saint-Barthélemy, Saint-Martin et Saint-Pierre-et-Miquelon, du représentant de l'Etat dans la collectivité, celui-ci prend l'arrêté prévu à l'alinéa précédent.

En tout état de cause, les parties doivent avoir le choix entre plusieurs denrées représentant une production du fonds donné à bail, sauf en cas de monoculture.
