# Article R462-10

Le droit de préemption prévu à l'article L. 462-15 ne peut être invoqué par le preneur :

1° En cas d'aliénation faite au profit du conjoint ou d'un parent du bailleur, jusqu'au troisième degré inclus, à moins que le preneur ne soit lui-même parent du bailleur au même degré ou à un degré plus rapproché que l'acquéreur ;

2° En cas de ventes effectuées en vue de créer ou d'étendre sur le fonds une entreprise industrielle ou de construire des immeubles.
