# Section 2 : Droits et obligations du preneur en matière d'exploitation

- [Article R411-9-11-1](article-r411-9-11-1.md)
- [Article R411-9-11-2](article-r411-9-11-2.md)
- [Article R411-9-11-3](article-r411-9-11-3.md)
- [Article R411-9-11-4](article-r411-9-11-4.md)
