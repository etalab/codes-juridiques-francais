# Article R414-4-1

La commission consultative paritaire interdépartementale des baux ruraux de la région d'Ile-de-France (Essonne, Paris, Hauts-de-Seine, Seine-Saint-Denis, Val-de-Marne, Val-d'Oise et Yvelines) comprend, outre le préfet de région d'Ile-de-France, préfet de Paris, ou son représentant, président :

a) Les préfets de département de l'Essonne, du Val-d'Oise et des Yvelines, ou leurs représentants ;

b) Le directeur régional et interdépartemental de l'alimentation, de l'agriculture et de la forêt d'Ile-de-France, ou son représentant ;

c) Le président de la chambre interdépartementale d'agriculture d'Ile-de-France, ou son représentant ;

d) Un représentant de chacune des organisations syndicales d'exploitants agricoles à vocation générale habilitées en application de l'article 1er du décret n° 90-187 du 28 février 1990 ;

e) Le président de l'organisation interdépartementale des bailleurs de baux ruraux d'Ile-de-France affiliée à l'organisation nationale la plus représentative ou, si celui-ci renonce à faire partie de la commission, le président de l'organisation interdépartementale de la propriété agricole d'Ile-de-France affiliée à l'organisation nationale la plus représentative, ou leur représentant ;

f) Le président de l'organisation interdépartementale des fermiers et des métayers d'Ile-de-France affiliée à l'organisation nationale la plus représentative, ou son représentant ;

g) Les présidents :

-de la chambre interdépartementale des notaires de Paris, de la Seine-Saint-Denis et du Val-de-Marne ;

-de la chambre interdépartementale de notaires des Yvelines et du Val-d'Oise ;

-de la chambre départementale des notaires de l'Essonne ;

-de la chambre départementale des notaires des Hauts-de-Seine,

ou leurs représentants ;

h) Trois représentants titulaires des bailleurs non preneurs et trois représentants titulaires des preneurs non bailleurs, désignés par le préfet de la région sur proposition de leurs organisations respectives et selon les modalités prévues au dernier alinéa de l'article R. 414-3. Il est désigné autant de suppléants que de titulaires. Seuls les membres ainsi désignés ont voix délibérative.

Le préfet de région arrête la composition de la commission.
