# Article R492-32

La réclamation est instruite et jugée en application des articles R. 119, alinéas 4 et 5, R. 120, alinéas 1er, 2 et 3, R. 121, R. 122 et R. 123 du code électoral.
