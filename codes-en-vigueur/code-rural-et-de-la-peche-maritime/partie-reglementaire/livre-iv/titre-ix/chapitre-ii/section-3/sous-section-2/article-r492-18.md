# Article R492-18

Une commission d'organisation des élections est instituée par arrêté du préfet publié au recueil des actes administratifs de la préfecture et installée au plus tard quatre semaines avant la date d'ouverture du scrutin.L'arrêté préfectoral fixe également le jour, l'heure et le lieu des opérations de dépouillement, au plus tôt cinq jours après la date de clôture du scrutin.

La commission comprend :

― le préfet ou son représentant, président ;

― le maire de la commune du siège du tribunal paritaire ou son représentant ;

― un fonctionnaire des services déconcentrés de l'Etat compétent en matière agricole ;

― avec voix consultative, un représentant des preneurs et un représentant des bailleurs siégeant dans l'une des commissions de préparation des listes électorales mentionnées à l'article R. 492-5 désignés par le préfet.

Le secrétariat de la commission est assuré par un fonctionnaire désigné par le préfet.

La commission statue à la majorité. En cas de partage égal de voix, celle du président est prépondérante.
