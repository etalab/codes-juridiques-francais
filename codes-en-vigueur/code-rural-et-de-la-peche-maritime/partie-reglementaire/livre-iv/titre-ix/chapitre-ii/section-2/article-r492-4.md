# Article R492-4

En vue de pourvoir à l'élection des membres assesseurs des tribunaux paritaires des baux ruraux prévue à l'article L. 492-1, il est dressé, dans le ressort de chaque tribunal, deux listes distinctes, s'il y a lieu, des bailleurs à ferme et à métayage, et deux listes distinctes, s'il y a lieu également, des preneurs à ferme et à métayage.

Avant le 1er juillet de l'année précédant celle des élections, le préfet du département du siège du tribunal fait afficher dans les communes du ressort du tribunal un avis annonçant l'établissement de ces listes.

Nul ne peut être inscrit sur plusieurs listes dans le ressort d'un même tribunal paritaire des baux ruraux. Les personnes réunissant les qualités leur permettant de s'inscrire sur plusieurs listes sont inscrites sur la liste correspondant à leur qualité prédominante appréciée en fonction de la superficie qui lui est afférente.
