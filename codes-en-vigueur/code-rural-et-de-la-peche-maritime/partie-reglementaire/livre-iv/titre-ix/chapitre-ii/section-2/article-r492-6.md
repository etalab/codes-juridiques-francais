# Article R492-6

Les demandes d'inscription sur une liste électorale sont adressées au maire de la commune où sont situés les biens immobiliers jusqu'au 31 août de l'année précédant celle de l'élection.

Dans un délai de quarante-huit heures au plus à compter de cette date, les maires de chaque commune du ressort du tribunal transmettent à la commission de préparation des listes électorales les listes provisoires des électeurs, établies sur la base des demandes d'inscription et des précédentes listes révisées en raison de décès, de départ du ressort du tribunal, ou de changement de qualité.

Les électeurs doivent communiquer toute pièce justifiant de leur qualité pour être inscrit sur une liste électorale.

La commission tient un registre des décisions d'inscription, de radiation ou de refus d'inscription ou de radiation sur les listes électorales provisoires. Elle transmet au plus tard le 15 octobre de l'année précédant celle de l'élection les listes provisoires des électeurs au préfet qui procède à l'établissement des listes par ressort de chacun des tribunaux paritaires des baux ruraux du département.
