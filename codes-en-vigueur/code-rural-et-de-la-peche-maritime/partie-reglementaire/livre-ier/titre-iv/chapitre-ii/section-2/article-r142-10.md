# Article R142-10

Les cessions à l'amiable aux sociétés d'aménagement foncier et d'établissement rural d'immeubles appartenant à l'Etat sont régies par l'article R. 147-1 du code du domaine de l'Etat ci-après reproduit :

" Art.R. 147-1 : Par dérogation aux dispositions de l'article R. 129 ci-dessus, les immeubles appartenant à l'Etat peuvent, quelle que soit leur valeur, être cédés à l'amiable, dans les conditions prévues à l'article R. 130, aux sociétés d'aménagement foncier et d'établissement rural agréées et, lorsqu'il s'agit de fonds incultes, aux organismes mentionnés à l'article 9 de la loi n° 51-592 du 24 mai 1951 modifiée ".
