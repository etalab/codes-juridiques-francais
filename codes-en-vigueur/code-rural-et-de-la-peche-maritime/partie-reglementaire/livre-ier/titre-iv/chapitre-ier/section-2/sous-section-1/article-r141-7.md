# Article R141-7

Les sociétés d'aménagement foncier et d'établissement rural soumettent à l'approbation des ministres chargés de l'agriculture et des finances leur programme pluriannuel d'activité et leur communiquent un compte tendu annuel d'activité.
