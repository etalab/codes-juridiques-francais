# Article R182-7

Le préfet arrête la liste des terres incultes ou manifestement sous-exploitées et le cahier des charges correspondant à chacune d'elles. Il est alors valablement procédé par le préfet, quels que soient les propriétaires et titulaires de droits d'exploitation, aux mesures prévues aux articles          L. 182-3 à L. 182-6, dès lors que ces mesures sont prises à l'encontre tant des propriétaires et des titulaires de droits d'exploitation identifiés par l'administration en application de l'article R. 182-3 auxquels les communications prévues au premier alinéa de l'article R. 182-4 ont été adressées que des propriétaires et titulaires de droit d'exploitation qui se sont révélés dans le mois de la publication en mairie prévue au deuxième alinéa de l'article R. 182-4.

Le projet de mise en valeur agricole du fonds, prévu à l'article L. 181-6, est cohérent avec le cahier des charges mentionné au premier alinéa.
