# Section 6 : Aménagement foncier et opérateur foncier

- [Article D182-30](article-d182-30.md)
- [Article R182-26](article-r182-26.md)
- [Article R182-27](article-r182-27.md)
- [Article R182-28](article-r182-28.md)
- [Article R182-29](article-r182-29.md)
- [Article R182-31](article-r182-31.md)
- [Article R182-32](article-r182-32.md)
