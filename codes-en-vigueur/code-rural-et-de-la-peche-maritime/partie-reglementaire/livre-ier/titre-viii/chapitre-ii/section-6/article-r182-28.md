# Article R182-28

<div align="left">I. ― Le chapitre Ier du titre IV du présent livre n'est pas applicable à Mayotte. <br/>
<br/>II. ― L'opérateur foncier mentionné à l'article L. 182-25 exerce ses missions en matière d'opérations immobilières ou d'exercice du droit de préemption conformément aux dispositions des chapitres II et III du même titre relatives aux sociétés d'aménagement foncier et d'établissement rural. <br/>
<br/>III. ― Pour l'application des dispositions du II, les références aux commissaires du Gouvernement auprès de la société d'aménagement foncier et d'établissement rural sont remplacées par la référence au commissaire du Gouvernement auprès de l'Agence de services et de paiement ou à son représentant désigné à cet effet.</div>
