# Article R151-48

Lorsque les travaux entrent dans les catégories suivantes figurant à l'article L. 151-36 :

a) Défense des rives et du fond des rivières non domaniales ;

b) Curage, approfondissement, redressement et régularisation des canaux et cours d'eau non domaniaux ;

c) Aménagement d'un cours d'eau non domanial ou d'une section de celui-ci,

les missions confiées dans les articles précédents au directeur départemental de l'agriculture et de la forêt sont exercées par le chef du service chargé de la police du cours d'eau ou de la section de cours d'eau concerné.
