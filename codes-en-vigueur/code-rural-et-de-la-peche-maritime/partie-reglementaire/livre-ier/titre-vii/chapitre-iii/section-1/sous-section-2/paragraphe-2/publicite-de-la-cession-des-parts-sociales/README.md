# Publicité de la cession des parts sociales.

- [Article R173-30](article-r173-30.md)
- [Article R173-31](article-r173-31.md)
