# Paragraphe 4 : Retrait d'un associé

- [Article R173-35](article-r173-35.md)
- [Article R173-36](article-r173-36.md)
- [Article R173-37](article-r173-37.md)
