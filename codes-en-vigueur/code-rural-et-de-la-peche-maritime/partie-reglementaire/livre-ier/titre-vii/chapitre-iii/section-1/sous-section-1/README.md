# Sous-section 1 : Constitution de la société

- [Paragraphe 1 : Dispositions générales](paragraphe-1)
- [Paragraphe 2 : Statuts, capital, parts sociales, parts en industrie](paragraphe-2)
- [Paragraphe 3 : Immatriculation de la société et publicité de sa constitution](paragraphe-3)
