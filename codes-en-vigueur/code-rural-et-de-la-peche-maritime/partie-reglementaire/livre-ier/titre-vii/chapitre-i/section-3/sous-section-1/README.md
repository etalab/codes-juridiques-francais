# Sous-section 1 : Le comité siégeant en matière disciplinaire.

- [Article R171-18](article-r171-18.md)
- [Article R171-19](article-r171-19.md)
- [Article R171-20](article-r171-20.md)
- [Article R171-21](article-r171-21.md)
