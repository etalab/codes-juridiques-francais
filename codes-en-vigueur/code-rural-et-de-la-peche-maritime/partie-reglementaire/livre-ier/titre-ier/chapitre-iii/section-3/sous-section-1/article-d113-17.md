# Article D113-17

Les délimitations prévues aux articles D. 113-14 à D. 113-16 sont effectuées par arrêté conjoint des ministres chargés de l'agriculture et de l'économie et des finances.

Toutefois les rectifications de délimitation d'importance secondaire et, en tout état de cause, limitées à 0,5 p. 100 de la superficie agricole utile nationale, peuvent être décidées par arrêté du ministre de l'agriculture.

Par dérogation aux dispositions prévues au premier alinéa du présent article, les régions de piedmont mentionnées à l'article D. 113-16, a, peuvent être délimitées par arrêtés préfectoraux. Un arrêté conjoint des ministres chargés de l'agriculture, de l'économie et des finances, du budget et des départements et territoires d'outre-mer précisera les conditions d'application de cette mesure.
