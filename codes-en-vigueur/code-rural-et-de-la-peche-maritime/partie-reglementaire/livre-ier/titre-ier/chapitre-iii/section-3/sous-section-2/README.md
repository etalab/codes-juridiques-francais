# Sous-section 2 : Aides compensatoires des handicaps naturels permanents.

- [Article D113-18](article-d113-18.md)
- [Article D113-19](article-d113-19.md)
- [Article D113-20](article-d113-20.md)
- [Article D113-21](article-d113-21.md)
- [Article D113-22](article-d113-22.md)
- [Article D113-23](article-d113-23.md)
- [Article D113-24](article-d113-24.md)
- [Article D113-25](article-d113-25.md)
- [Article R113-26](article-r113-26.md)
