# Sous-section 1 : Sociétés d'aménagement régional.

- [Article R112-6](article-r112-6.md)
- [Article R112-7](article-r112-7.md)
- [Article R112-8](article-r112-8.md)
- [Article R112-9](article-r112-9.md)
- [Article R112-10](article-r112-10.md)
- [Article R112-11](article-r112-11.md)
- [Article R112-12](article-r112-12.md)
- [Article R112-13](article-r112-13.md)
