# Sous-section 4 : Observatoire national de la consommation des espaces agricoles

- [Article D112-1-12](article-d112-1-12.md)
- [Article D112-1-13](article-d112-1-13.md)
- [Article D112-1-14](article-d112-1-14.md)
- [Article D112-1-15](article-d112-1-15.md)
- [Article D112-1-16](article-d112-1-16.md)
- [Article D112-1-17](article-d112-1-17.md)
