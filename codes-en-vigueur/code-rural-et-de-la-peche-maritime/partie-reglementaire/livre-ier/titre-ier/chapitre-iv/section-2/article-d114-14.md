# Article D114-14

Peuvent conclure un contrat de protection de l'environnement dans les espaces ruraux :

1° Toute personne physique exerçant une activité agricole au sens de l'article L. 311-1 et âgée de soixante-sept ans au plus au 1er janvier de l'année de la demande ;

2° Les sociétés ayant pour objet statutaire la mise en valeur d'une exploitation agricole, sous réserve qu'au moins un associé exploitant remplisse les conditions prévues au 1° du présent article ;

3° Les fondations, associations sans but lucratif et les établissements d'enseignement et de recherche agricoles, lorsqu'ils mettent directement en valeur une exploitation agricole ;

4° Les personnes morales qui mettent des terres à disposition d'exploitants agricoles de manière indivise.

Pour chaque opération, des conditions d'éligibilité plus restrictives peuvent être arrêtées par le ministre chargé de l'agriculture.
