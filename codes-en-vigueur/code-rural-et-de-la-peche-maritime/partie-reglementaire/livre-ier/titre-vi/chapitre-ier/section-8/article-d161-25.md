# Article D161-25

Les délibérations des conseils municipaux portant sur l'aliénation de tout ou partie d'un ou plusieurs chemins ruraux appartenant à plusieurs communes ou constituant un même itinéraire s'étendant sur le territoire de plusieurs communes doivent être précédées d'une enquête publique unique, conduite par un même commissaire enquêteur, effectuée dans les conditions de forme et de procédure prévues aux articles R. 141-4 à R. 141-9 du code de la voirie routière.
