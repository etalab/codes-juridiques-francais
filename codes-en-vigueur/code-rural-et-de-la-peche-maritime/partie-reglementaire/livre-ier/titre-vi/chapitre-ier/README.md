# Chapitre Ier : Chemins ruraux

- [Section 1 : Chemins incorporés à la voirie rurale.](section-1)
- [Section 2 : Acceptation et exécution des souscriptions volontaires.](section-2)
- [Section 3 : Caractéristiques techniques.](section-3)
- [Section 4 : Mesures générales de police.](section-4)
- [Section 5 : Bornage.](section-5)
- [Section 6 : Conservation et surveillance.](section-6)
- [Section 7 : Dispositions relatives à l'écoulement des eaux, aux plantations, à l'élagage et au curage des fossés.](section-7)
- [Section 8 : Aliénation des chemins ruraux dans les cas prévus à l'article L. 161-10-1 du code rural et de la pêche maritime.](section-8)
- [Section 9 : Dispositions diverses.](section-9)
