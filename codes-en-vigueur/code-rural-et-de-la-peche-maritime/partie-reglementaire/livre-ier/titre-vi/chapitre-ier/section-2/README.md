# Section 2 : Acceptation et exécution des souscriptions volontaires.

- [Article D161-5](article-d161-5.md)
- [Article D161-6](article-d161-6.md)
- [Article D161-7](article-d161-7.md)
