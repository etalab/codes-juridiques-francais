# Section 4 : Dispositions particulières

- [Sous-section 1 : Dispositions particulières aux zones forestières.](sous-section-1)
- [Sous-section 2 : Opérations liées à la réalisation de grands ouvrages publics](sous-section-2)
- [Sous-section 4 : Dispositions particulières aux aires d'appellation d'origine contrôlée.](sous-section-4)
