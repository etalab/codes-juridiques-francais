# Article R732-7

Pour l'ouverture du droit à pension d'invalidité, les dispositions du troisième alinéa de l'article R. 732-2 sont applicables.

Le ministre chargé de l'agriculture fixe les conditions dans lesquelles doivent être constitués par les assurés les dossiers afférents à leur demande de pension.
