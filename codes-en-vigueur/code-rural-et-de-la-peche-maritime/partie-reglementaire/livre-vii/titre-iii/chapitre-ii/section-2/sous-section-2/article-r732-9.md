# Article R732-9

Lorsque l'invalide ne répond pas à la convocation du service de contrôle médical fait par lettre recommandée, en vue de son examen, ou s'oppose à la visite du médecin désigné à cet effet, aux jour et heure notifiés par lettre recommandée de celui-ci, la date de la convocation ou de la visite est reportée d'office à quinzaine.

Lorsque l'invalide ne se présente pas à l'issue de ce délai ou s'oppose à nouveau à la visite, la pension peut être supprimée.

Les lettres recommandées prévues au premier alinéa doivent être adressées avec demande d'avis de réception.
