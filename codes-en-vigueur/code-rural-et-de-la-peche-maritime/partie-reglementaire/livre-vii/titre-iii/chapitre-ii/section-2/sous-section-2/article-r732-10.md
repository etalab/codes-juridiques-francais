# Article R732-10

Les organismes assureurs sont tenus de notifier à l'assuré, par lettre recommandée avec demande d'avis de réception, les décisions prises par eux en application de la présente section.
