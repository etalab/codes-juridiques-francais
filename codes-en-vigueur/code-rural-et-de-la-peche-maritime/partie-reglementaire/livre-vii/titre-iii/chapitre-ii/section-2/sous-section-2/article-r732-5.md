# Article R732-5

Pour apprécier si, en fonction des ressources du titulaire d'une pension d'invalidité, cette pension doit être supprimée ou suspendue, le revenu de référence est fixé à 2028 fois le salaire minimum de croissance en vigueur au 1er janvier de l'année du contrôle.

La pension d'invalidité est supprimée dès l'instant où l'intéressé est en état de reprendre d'une manière permanente dans une profession quelconque un emploi lui assurant un revenu annuel au moins égal à la moitié du revenu de référence.

Elle est suspendue en tout ou partie, sauf pendant les périodes de rééducation fonctionnelle ou professionnelle, lorsqu'il est constaté que, durant les deux premiers trimestres de service de la pension, le montant cumulé de la pension d'invalidité et des salaires ou revenus professionnels excède la moitié du revenu de référence. Dans ce cas, le montant des arrérages mensuels suivants est réduit du sixième du dépassement constaté au cours des deux trimestres de référence.

Il est ensuite procédé à des contrôles annuels. Si, à l'occasion de ces contrôles, ces mêmes ressources, appréciées au 1er janvier, dépassent le montant du revenu de référence, le montant des arrérages des douze mois suivants est réduit du douzième du dépassement constaté au cours de l'année précédente. Toute modification de la situation de l'invalide intervenant avant le 1er janvier suivant donne lieu à une révision de sa situation qui prend effet dès la date de la modification.
