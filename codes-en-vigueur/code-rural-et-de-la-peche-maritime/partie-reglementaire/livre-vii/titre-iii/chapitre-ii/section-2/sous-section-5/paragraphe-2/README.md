# Paragraphe 2 : Allocation de remplacement pour congé de paternité et d'accueil de l'enfant prévue à l'article L. 732-12-1.

- [Article D732-27](article-d732-27.md)
- [Article D732-28](article-d732-28.md)
- [Article D732-29](article-d732-29.md)
