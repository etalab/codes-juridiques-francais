# Article D732-2-1

<div align="left">I. ― Pour bénéficier des indemnités journalières prévues à l'article L. 732-4, l'assuré doit : <br/>
<br/>1° Etre affilié au régime d'assurance maladie, invalidité, maternité des non-salariés agricoles depuis au moins un an. Lorsque l'intéressé est affilié depuis moins d'un an, il est fait application des dispositions de l'article L. 172-1-A du code de la sécurité sociale ; <br/>
<br/>2° Etre à jour de la cotisation mentionnée à l'article L. 731-35-1 au 1er janvier de l'année civile au cours de laquelle l'incapacité de travail a été médicalement constatée. <br/>
<br/>II. ― En cas de paiement tardif de la cotisation mentionnée à l'article L. 731-35-1, l'assuré peut faire valoir ses droits aux indemnités journalières à condition d'avoir réglé la totalité de la cotisation restant due au 1er janvier de l'année au cours de laquelle est constatée l'incapacité de travail. Dans ce cas, il bénéficie des indemnités journalières à compter de la date de règlement de cette cotisation.<br/>
<br/>
</div>
