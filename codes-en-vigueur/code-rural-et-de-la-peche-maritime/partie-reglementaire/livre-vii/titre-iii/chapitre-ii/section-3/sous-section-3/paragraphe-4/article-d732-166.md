# Article D732-166

La valeur de service du point de retraite complémentaire obligatoire mentionnée à l'article L. 732-60 est fixée pour l'année 2014 à 0,336 2 euros.
