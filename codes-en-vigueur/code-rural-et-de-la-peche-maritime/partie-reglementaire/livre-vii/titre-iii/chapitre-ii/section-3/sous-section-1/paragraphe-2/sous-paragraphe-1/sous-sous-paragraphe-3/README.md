# Sous-sous-paragraphe 3 : Condition de cessation d'activité.

- [Article D732-53](article-d732-53.md)
- [Article D732-54](article-d732-54.md)
- [Article D732-55](article-d732-55.md)
- [Article D732-56](article-d732-56.md)
