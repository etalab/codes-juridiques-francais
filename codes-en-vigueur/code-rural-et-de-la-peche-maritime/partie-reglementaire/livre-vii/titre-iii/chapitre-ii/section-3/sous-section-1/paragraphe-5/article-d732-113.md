# Article D732-113

Le montant annuel du plafond prévu à l'article L. 732-54-3 est fixé à 9 600 euros.

Le montant du plafond est revalorisé aux mêmes dates et dans les mêmes conditions que celles prévues pour les pensions de vieillesse de base par l'article L. 161-23-1 du code de la sécurité sociale.
