# Sous-sous-paragraphe 2 : Dispositions relatives au nombre de points acquis au titre de certaines années.

- [Article D732-76](article-d732-76.md)
- [Article D732-77](article-d732-77.md)
