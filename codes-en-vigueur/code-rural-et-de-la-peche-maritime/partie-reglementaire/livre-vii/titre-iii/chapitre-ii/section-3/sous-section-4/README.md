# Sous-section 4 : Retraite progressive

- [Paragraphe 1 : Conditions et modalités de liquidation et de calcul de la fraction de pension ainsi que de la pension définitive.](paragraphe-1)
- [Paragraphe 2 : Le plan de cession progressive de l'exploitation ou de l'entreprise agricole.](paragraphe-2)
