# Article R731-111

Les organismes assureurs peuvent, avec l'accord du groupement dont ils relèvent, passer des conventions avec les caisses de mutualité sociale agricole en vue de transférer à celles-ci tout ou partie de la gestion de l'assurance.
