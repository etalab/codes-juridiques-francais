# Article D731-97

Le montant de la cotisation forfaitaire définie à l'article L. 731-35 et due pour la couverture des prestations d'invalidité prévues à l'article L. 732-8 par les chefs d'exploitation ou d'entreprise agricole pour les collaborateurs d'exploitation ou d'entreprise agricole mentionnés à l'article L. 321-5 est constaté chaque année par arrêté des ministres chargés de l'agriculture, de la sécurité sociale et du budget.

Le montant de cette cotisation est revalorisé annuellement en fonction de l'évolution du SMIC à partir du montant de la cotisation applicable au cours de l'année précédente.

La valeur du SMIC en fonction de laquelle le montant de la cotisation est revalorisé est celle fixée au 1er janvier de l'année au titre de laquelle les cotisations sont dues.

Le montant de cette cotisation est arrondi au demi-euro le plus proche.
