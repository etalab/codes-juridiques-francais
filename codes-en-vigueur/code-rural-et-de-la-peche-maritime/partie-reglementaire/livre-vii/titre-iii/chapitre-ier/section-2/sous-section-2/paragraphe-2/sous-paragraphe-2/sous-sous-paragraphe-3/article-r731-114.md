# Article R731-114

Les opérations de l'assurance font l'objet, dans chacun des organismes assureurs et dans le groupement dont il relèvent, d'une comptabilité spéciale conforme aux prescriptions du plan comptable unique des organismes de sécurité sociale. Les livres, registres, documents comptables et pièces justificatives sont conservés dans les conditions et les délais applicables aux caisses de mutualité sociale agricole. Les dépenses et recettes liées aux prestations prévues à l'article L. 732-4 sont enregistrées dans des comptes distincts.
