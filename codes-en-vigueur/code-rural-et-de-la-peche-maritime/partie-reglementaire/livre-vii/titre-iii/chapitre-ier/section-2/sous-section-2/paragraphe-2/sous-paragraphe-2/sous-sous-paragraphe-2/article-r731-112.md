# Article R731-112

Les organismes assureurs relevant du code de la mutualité admis à la gestion de l'assurance peuvent passer des conventions avec d'autres organismes mutualistes en vue de leur confier la mission d'exécuter pour leur compte des opérations leur incombant au titre de cette gestion. Ces conventions sont soumises à l'approbation du ministre chargé de la sécurité sociale.
