# Article R731-116

Les services de l'Etat mentionnés à l'article R. 724-3 peuvent contrôler, dans les bureaux départementaux ainsi que chez les organismes assureurs et le groupement dont ils relèvent, l'ensemble des opérations de l'assurance.
