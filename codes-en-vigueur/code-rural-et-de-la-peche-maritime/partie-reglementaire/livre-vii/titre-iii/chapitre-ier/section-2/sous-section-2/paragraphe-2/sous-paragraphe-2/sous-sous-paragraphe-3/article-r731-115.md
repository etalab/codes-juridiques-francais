# Article R731-115

Les comptes annuels relatifs aux opérations de l'assurance établis par des organismes assureurs ou par le groupement dont ils relèvent sont communiqués, dans le délai prescrit par l'article D. 723-219,                       au ministre chargé de l'agriculture.
