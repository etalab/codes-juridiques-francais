# Article R731-108

En vue d'assurer le contrôle des opérations prévues par les articles R. 731-101 à R. 731-119, les organismes assureurs relevant du même groupement en application du 3° de l'article R. 731-105 constituent un bureau départemental pour l'ensemble de leurs assurés relevant de la circonscription territoriale de chaque caisse de mutualité sociale agricole.

Sauf dérogation accordée par décision du ministre chargé de l'agriculture, ces bureaux doivent être situés dans la ville du siège de la caisse de mutualité sociale agricole intéressée.

Faute de constitution du bureau départemental par les organismes assureurs intéressés, la caisse de mutualité sociale agricole assure la tenue de ce bureau, à charge par le groupement dont relèvent ces organismes de l'indemniser des frais de gestion y afférents dans les conditions fixées par le ministre chargé de l'agriculture.

Les organismes assureurs sont tenus d'effectuer, par l'intermédiaire des groupements dont ils relèvent et dans le délai de dix jours du règlement ou du rejet des prestations, pour l'ensemble de leurs assurés compris dans la circonscription territoriale de chaque caisse de mutualité sociale agricole, le dépôt au bureau départemental correspondant à celle-ci des feuilles de soins et documents y annexés, des feuilles de décompte afférentes aux prestations payées ou rejetées des assurances maladie et maternité ainsi que des pièces émanant du service du contrôle médical.

Les prestations des assurances maladie et maternité doivent faire l'objet d'une fiche récapitulative tenue à jour et conservée par les bureaux départementaux.
