# Article R731-21

La production de déclarations de revenus professionnels incomplètes ou inexactes entraîne l'application d'une majoration de 10 % des cotisations dues.
