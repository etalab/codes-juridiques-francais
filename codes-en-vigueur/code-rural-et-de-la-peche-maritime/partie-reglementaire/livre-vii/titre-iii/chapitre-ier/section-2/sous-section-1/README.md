# Sous-section 1 : Dispositions générales.

- [Paragraphe 1 : Assiette des cotisations](paragraphe-1)
- [Paragraphe 2 : Cotisations de solidarité](paragraphe-2)
- [Paragraphe 3 : Exonération partielle des cotisations en début d'activité.](paragraphe-3)
- [Paragraphe 4 : Périodicité et recouvrement des cotisations](paragraphe-4)
- [Article D731-14](article-d731-14.md)
- [Article D731-15](article-d731-15.md)
- [Article R731-16](article-r731-16.md)
