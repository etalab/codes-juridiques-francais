# Article R731-75

I.-Les conseils d'administration des caisses de mutualité sociale agricole ou les commissions de recours amiable prévues à l'article R. 142-1 du code de la sécurité sociale ayant reçu délégation à cet effet peuvent, sur demande écrite des intéressés, accorder, en cas de bonne foi dûment prouvée, la remise des majorations et des pénalités prévues aux articles L. 731-22, R. 731-20, R. 731-21, R. 731-68, premier alinéa et D. 731-41, dans des conditions fixées au présent article.

La majoration de 0, 4 % mentionnée au deuxième alinéa de l'article R. 731-68 peut faire l'objet d'une remise lorsque les cotisations ont été acquittées dans le délai de trente jours qui suit la date limite d'exigibilité ou en raison de circonstances exceptionnelles ou dans les cas de force majeure.

Aucune remise ne peut être accordée sur les majorations portant sur des cotisations dues à titre personnel à la suite du constat de l'infraction relative au travail dissimulé par dissimulation d'activité défini à l'article L. 8221-3 du code du travail.

II.-La demande n'est recevable qu'après paiement de la totalité des cotisations ayant donné lieu aux majorations de retard. Dès paiement de la totalité des cotisations, y compris en cas de recouvrement forcé, la caisse de mutualité sociale agricole informe les intéressés de la possibilité de formuler cette demande de remise ainsi que du délai dans lequel cette demande doit être présentée sous peine de forclusion.

La conclusion d'un échéancier de paiement vaut pour le débiteur demande de remise des pénalités et majorations de retard prévue au premier alinéa. Toutefois, lorsque l'échéancier n'est pas respecté, une demande de remise doit être formulée conformément à ce même alinéa.

Les décisions sont motivées. Elles sont notifiées au demandeur de la remise. Le silence gardé pendant plus de trois mois par l'organisme créancier sur une demande de remise vaut décision de rejet.

Pour les contestations relatives aux décisions de remise des pénalités et des majorations de retard, le délai d'un mois mentionné à l'article R. 142-18 du code de la sécurité sociale est porté à trois mois.

III.-Les conditions dans lesquelles doit être présentée la demande de remise des pénalités et majorations de retard ainsi que le montant de la remise au-delà duquel les décisions de remise totale ou partielle doivent être approuvées par le responsable du service mentionné à l'article R. 155-1 du code de la sécurité sociale sont fixés par arrêté du ministre chargé de l'agriculture, du ministre chargé de la sécurité sociale et du ministre chargé du budget.
