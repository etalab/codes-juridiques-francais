# Paragraphe 3 : Exonération partielle des cotisations en début d'activité.

- [Article D731-51](article-d731-51.md)
- [Article D731-52](article-d731-52.md)
- [Article D731-53](article-d731-53.md)
- [Article D731-54](article-d731-54.md)
- [Article D731-55](article-d731-55.md)
- [Article D731-56](article-d731-56.md)
