# Sous-paragraphe 2 : Déduction du revenu implicite du capital foncier.

- [Article D731-22](article-d731-22.md)
- [Article D731-23](article-d731-23.md)
- [Article D731-24](article-d731-24.md)
- [Article D731-25](article-d731-25.md)
