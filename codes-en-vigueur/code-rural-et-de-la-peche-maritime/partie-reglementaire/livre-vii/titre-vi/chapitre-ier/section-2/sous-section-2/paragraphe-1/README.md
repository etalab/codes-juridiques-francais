# Paragraphe 1 : Bénéficiaires

- [Sous-paragraphe 1 : Elèves des établissements d'enseignement technique et de formation professionnelle agricoles.](sous-paragraphe-1)
- [Sous-paragraphe 2 : Membres bénévoles des organismes sociaux créés au profit des professions agricoles.](sous-paragraphe-2)
- [Sous-paragraphe 3 : Salariés agricoles siégeant dans des organismes paritaires.](sous-paragraphe-3)
- [Sous-paragraphe 4 : Assurés bénéficiaires d'allocations de conversion.](sous-paragraphe-4)
- [Sous-paragraphe 5 : Salariés bénéficiaires d'un congé de représentation.](sous-paragraphe-5)
