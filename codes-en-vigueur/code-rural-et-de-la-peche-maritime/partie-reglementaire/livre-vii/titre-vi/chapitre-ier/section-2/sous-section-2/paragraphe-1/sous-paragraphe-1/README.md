# Sous-paragraphe 1 : Elèves des établissements d'enseignement technique et de formation professionnelle agricoles.

- [Article D761-39](article-d761-39.md)
- [Article D761-40](article-d761-40.md)
- [Article D761-41](article-d761-41.md)
