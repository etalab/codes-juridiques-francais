# Paragraphe 1 : Régime de base obligatoire des assurances sociales agricoles.

- [Article D761-2](article-d761-2.md)
- [Article R761-1](article-r761-1.md)
- [Article R761-3](article-r761-3.md)
