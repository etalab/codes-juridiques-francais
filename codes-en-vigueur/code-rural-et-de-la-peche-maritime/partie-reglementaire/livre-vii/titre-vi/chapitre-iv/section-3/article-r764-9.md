# Article R764-9

Les dispositions des chapitres III et VI du titre VI du livre VII du code de la sécurité sociale (deuxième partie : Décrets en Conseil d'Etat et troisième partie : Décrets) sont applicables aux ressortissants français, mentionnés à l'article L. 764-6, qui exercent dans un pays étranger une activité agricole non salariée.
