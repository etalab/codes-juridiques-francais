# Sous-paragraphe 1 : Dispositions communes à la retraite forfaitaire et à la retraite proportionnelle.

- [Article D762-53](article-d762-53.md)
- [Article D762-54](article-d762-54.md)
- [Article D762-55-1](article-d762-55-1.md)
- [Article R762-51](article-r762-51.md)
- [Article R762-52](article-r762-52.md)
- [Article R762-55](article-r762-55.md)
