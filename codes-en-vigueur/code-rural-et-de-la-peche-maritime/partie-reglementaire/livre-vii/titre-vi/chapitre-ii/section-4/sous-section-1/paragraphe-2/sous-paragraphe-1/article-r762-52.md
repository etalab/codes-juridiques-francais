# Article R762-52

L'âge et la durée minimale d'assurance ou de périodes reconnues équivalentes mentionnés à l'article L. 762-30 en deçà desquels s'applique un coefficient de minoration au montant de la pension de retraite sont ceux fixés à l'article R. 732-39.
