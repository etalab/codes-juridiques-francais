# Article D762-5

Dans le cas de métayage ou de colonat partiaire, le montant des cotisations prévues aux articles L. 762-9, L. 762-11, L. 762-33 (2e et 4e alinéas) et L. 762-21 est réparti à raison des trois quarts pour le preneur et d'un quart pour le bailleur ; toutefois, lorsque le preneur et le bailleur en font la demande, la répartition est faite selon la proportion retenue pour le partage des fruits. Le preneur et le bailleur sont tenus au paiement de la fraction mise à la charge de chacun.
