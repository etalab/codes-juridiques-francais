# Article D762-7-3

Le comité de gestion mentionné à l'article D. 762-76 fixe le jour du mois où le prélèvement mensuel des cotisations mentionnées à l'article D. 762-4 sera effectué sans que celui-ci puisse être postérieur au dixième jour. Cette échéance ne peut être modifiée au cours d'une année civile.
