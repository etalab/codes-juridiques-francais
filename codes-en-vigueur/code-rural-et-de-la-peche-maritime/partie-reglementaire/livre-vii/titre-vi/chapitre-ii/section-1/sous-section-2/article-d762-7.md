# Article D762-7

Les cotisations sont notifiées aux cotisants par les caisses générales de sécurité sociale au plus tard aux dates d'exigibilité fixées conformément au deuxième alinéa de l'article D. 762-6.
