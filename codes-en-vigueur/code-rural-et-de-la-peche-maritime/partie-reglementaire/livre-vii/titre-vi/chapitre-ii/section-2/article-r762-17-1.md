# Article R762-17-1

<div align="left">A Mayotte, le régime des prestations familiales des non-salariés agricoles est régi par les dispositions suivantes : <br/>
<br/>1° En matière de calcul de cotisations, les modalités prévues à la sous-section 2 de la section 2 du présent chapitre ainsi que, en matière de recouvrement, les dispositions réglementaires relatives à la sécurité sociale dans les départements d'outre-mer ; <br/>
<br/>2° En matière de prestations, les dispositions réglementaires prises pour l'application de l'ordonnance n° 2002-149 du 7 février 2002 relative à l'extension et à la généralisation des prestations familiales et à la protection sociale dans la collectivité départementale de Mayotte ; <br/>
<br/>3° En matière de contentieux, les dispositions réglementaires du chapitre IV du titre IV du livre II du code de la sécurité sociale et les articles R. 752-10 à R. 752-15 du même code. <br/>
<br/>4° La section " prestations familiales des exploitants agricoles ” de la caisse d'allocations familiales de La Réunion est compétente pour le service des prestations familiales aux non-salariés agricoles de Mayotte. La Caisse centrale de la mutualité sociale agricole est chargée de mettre à la disposition de cette caisse les fonds nécessaires au règlement des prestations légales de ce régime. Les cotisations sont calculées et recouvrées par la caisse de sécurité sociale de Mayotte.<br/>
</div>
