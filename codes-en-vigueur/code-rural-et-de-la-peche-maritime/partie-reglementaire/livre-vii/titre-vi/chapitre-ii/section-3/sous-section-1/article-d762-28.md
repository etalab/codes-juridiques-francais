# Article D762-28

Les agents assermentés des caisses générales de sécurité sociale sont chargés de vérifier l'exécution des prescriptions de l'article D. 762-27.
