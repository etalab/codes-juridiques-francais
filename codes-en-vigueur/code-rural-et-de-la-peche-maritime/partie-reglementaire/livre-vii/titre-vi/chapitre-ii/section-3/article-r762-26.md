# Article R762-26

Les dispositions réglementaires prises pour l'application des dispositions de la partie législative du code de la sécurité sociale mentionnées à l'article L. 762-14 sont applicables aux non-salariés agricoles exerçant leur activité en Guadeloupe, en Guyane, à la Martinique, à La Réunion, à Mayotte, à Saint-Barthélemy et à Saint-Martin.

Les dispositions réglementaires prises pour l'application des articles L. 762-13 et L. 762-15 sont applicables aux non-salariés agricoles exerçant leur activité en Guadeloupe, en Guyane, à la Martinique, à La Réunion, à Mayotte, à Saint-Barthélemy et à Saint-Martin dans les conditions et sous les réserves précisées à la présente section.
