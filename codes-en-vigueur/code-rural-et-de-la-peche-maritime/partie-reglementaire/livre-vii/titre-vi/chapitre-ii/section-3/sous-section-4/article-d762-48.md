# Article D762-48

La comptabilité des caisses générales de sécurité sociale fait apparaître de manière distincte les opérations relatives aux recettes et dépenses de la section des assurances maladie, invalidité et maternité des exploitants agricoles.
