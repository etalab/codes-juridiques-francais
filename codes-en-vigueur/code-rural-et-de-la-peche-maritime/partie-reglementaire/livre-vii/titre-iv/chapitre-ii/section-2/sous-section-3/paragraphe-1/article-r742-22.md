# Article R742-22

Pour l'application de l'article R. 351-11 du code de la sécurité sociale aux salariés agricoles :

1° Au II, la référence aux articles "R. 243-16 et R. 243-18" est remplacée respectivement par la référence aux articles "R. 741-22 et R. 741-23 du code rural" ;

2° Le IV est complété par les mots suivants : "ainsi que les cotisations émises par la caisse après déclaration par l'employeur des salaires payés".
