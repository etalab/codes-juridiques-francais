# Sous-paragraphe 3 : Périodes de détention provisoire.

- [Article D742-32](article-d742-32.md)
- [Article D742-33](article-d742-33.md)
- [Article D742-36](article-d742-36.md)
- [Article D742-37](article-d742-37.md)
- [Article D742-38](article-d742-38.md)
