# Sous-paragraphe 2 : Rapatriés ayant exercé une activité salariée agricole.

- [Article D742-26](article-d742-26.md)
- [Article D742-27](article-d742-27.md)
- [Article D742-30](article-d742-30.md)
- [Article D742-31](article-d742-31.md)
