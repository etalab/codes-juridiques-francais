# Article R741-8-1

L'employeur dont l'exploitation ou l'entreprise agricoles répond aux conditions fixées pour bénéficier de la réduction d'impôt mentionnée à l'article 220 decies du code général des impôts peut, de plein droit, limiter le paiement des cotisations patronales de sécurité sociale dues au titre de chaque échéance au montant de celles dont il était redevable l'année précédente lors de la même échéance. Le solde des cotisations patronales de sécurité sociale restant dû est acquitté lors de l'échéance correspondante de l'année suivante.

Les cotisations dont le paiement peut être partiellement différé sont celles dues au titre de la période de douze mois qui suit l'exercice au titre duquel la réduction d'impôt dont bénéficie l'exploitation ou l'entreprise agricole a été calculée.

En cas de changement de périodicité de versement des cotisations en raison soit de la modification de l'effectif des salariés de l'exploitation ou de l'entreprise agricole calculé au 31 décembre de chaque année, soit en raison de l'exercice ou de la dénonciation de l'option prévue à l'article R. 741-7, l'employeur bénéficie du paiement partiellement différé des cotisations patronales de sécurité sociale uniquement dans le cadre des échéances trimestrielles. Le solde des cotisations patronales de sécurité sociale restant dû est acquitté lors de l'échéance trimestrielle correspondante de l'année suivante.

II.-Les employeurs mentionnés aux articles R. 741-3 et R. 741-7 doivent informer la caisse de mutualité sociale agricole dont ils relèvent de l'application du paiement partiellement différé des cotisations patronales de sécurité sociale au plus tard à la date de retour du bordereau mentionné à l'article R. 741-5 afférent au mois d'activité au titre duquel le différé de paiement est appliqué.

L'employeur soumis à l'obligation du paiement trimestriel des cotisations prévue à l'article R. 741-6 doit présenter une demande de paiement partiellement différé des cotisations patronales de sécurité sociale par écrit auprès de la caisse de mutualité sociale agricole dont il relève, au plus tard à la date de retour du bordereau ou de transmission de la déclaration de données sociales mentionnés à l'article R. 741-2 afférents au trimestre d'activité au titre duquel le différé de paiement est sollicité.

Dans tous les cas, l'employeur doit fournir, dans les douze mois suivant la clôture de l'exercice pour lequel la réduction d'impôt dont bénéficie l'exploitation ou l'entreprise agricole a été calculée, copie de la déclaration spéciale prévue à l'article 46 quater-0 YW de l'annexe III du code général des impôts.

III.-Sous réserve que l'employeur s'acquitte des cotisations salariales et patronales dues aux échéances prévues, le paiement régulièrement différé de ces cotisations patronales ne donne pas lieu à l'application des majorations de retard mentionnées à l'article R. 741-23.
