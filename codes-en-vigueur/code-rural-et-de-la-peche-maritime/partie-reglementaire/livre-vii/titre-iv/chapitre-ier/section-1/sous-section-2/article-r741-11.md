# Article R741-11

Les compléments de cotisations devant être réclamés ou les sommes perçues à tort devant être remboursées à la suite d'ajustements opérés par la caisse dans les décomptes relatifs à un trimestre déterminé peuvent :

1° Soit être mis en recouvrement avec les cotisations trimestrielles venant à échéance ou déduites de ces cotisations, suivant le cas ;

2° Soit faire l'objet d'une mise en recouvrement ou d'un remboursement séparé, selon le cas.

Lorsque les compléments de cotisations font l'objet d'une mise en recouvrement séparée, ils doivent être versés dans les dix jours suivant la mise en recouvrement.

En cas d'ajustements opérés par l'employeur qui a exercé l'option prévue à l'article R. 741-1-2 dans les décomptes relatifs à un trimestre déterminé, les compléments de cotisations devant être recouvrés ou les sommes perçues à tort devant être remboursées font l'objet d'une régularisation par l'employeur lors de l'établissement de la déclaration de données sociales correspondant à la prochaine échéance trimestrielle.
