# Article R741-28-1

La majoration prévue à l'article L. 243-7-6 du code de la sécurité sociale est appliquée si les observations effectuées à l'occasion d'un précédent contrôle ont été notifiées moins de cinq ans avant la date de la notification des nouvelles observations constatant le manquement aux mêmes obligations.

Cette majoration est appliquée à la part du montant du redressement résultant du manquement réitéré aux obligations en cause.
