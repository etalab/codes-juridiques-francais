# Article R741-23

Il est appliqué une majoration de retard de 5 % au montant des cotisations ou contributions qui n'ont pas été versées aux dates limites d'exigibilité fixées aux articles R. 741-3,
R. 741-6, R. 741-7, R. 741-9,
R. 741-10, R. 741-11 et R. 741-15.

A cette majoration s'ajoute une majoration complémentaire de 0, 4 % du montant des cotisations ou contributions dues, par mois ou fraction de mois écoulé, à compter de la date limite d'exigibilité des cotisations ou contributions.

Pour les redressements d'assiette et de taux faisant suite aux contrôles mentionnés aux articles L. 724-7 et L. 724-11, la majoration complémentaire n'est décomptée qu'à partir du 1er février de l'année qui suit celle au titre de laquelle les régularisations sont effectuées.
