# Article R741-30

En cas de défaillance de l'entreprise de travail temporaire et d'insuffisance de la garantie financière exigée, les cotisations réclamées à l'utilisateur en application du deuxième alinéa de l'article R. 124-22 du code du travail font l'objet de majorations de retard calculées dans les conditions fixées par les articles R. 741-23, R. 741-25 et R. 741-26 du présent code dès lors qu'elles n'ont pas été acquittées dans un délai d'un mois à compter de la notification de la mise en demeure à l'utilisateur.
