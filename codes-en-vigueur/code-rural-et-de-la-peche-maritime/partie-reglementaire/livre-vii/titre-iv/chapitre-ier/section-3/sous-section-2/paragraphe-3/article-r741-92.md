# Article R741-92

Les dispositions des articles R. 741-22 et R. 741-23 sont applicables aux organismes redevables des cotisations prévues à l'article R. 741-90 ci-dessus.
