# Paragraphe 2 : Cotisations assises sur les avantages de retraite.

- [Article R741-80](article-r741-80.md)
- [Article R741-81](article-r741-81.md)
- [Article R741-82](article-r741-82.md)
- [Article R741-83](article-r741-83.md)
- [Article R741-84](article-r741-84.md)
- [Article R741-85](article-r741-85.md)
- [Article R741-86](article-r741-86.md)
- [Article R741-87](article-r741-87.md)
- [Article R741-88](article-r741-88.md)
- [Article R741-89](article-r741-89.md)
