# Article R741-96

Les cotisations assises sur les indemnités, allocations et revenus de remplacement mentionnés à l'article L. 131-2 du code de la sécurité sociale servis par l'employeur sont versées par celui-ci à l'organisme de recouvrement dont il relève, dans les conditions prévues aux articles R. 741-2 à R. 741-11 et R. 741-22 à R. 741-24. Pour l'application de ces dispositions, les avantages précités sont assimilés à des rémunérations. L'employeur indique dans la déclaration annuelle des salaires le montant global de ces avantages versés dans l'année et soumis à cotisation.
