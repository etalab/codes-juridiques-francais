# Paragraphe 3 : Cotisations assises sur les revenus de remplacement.

- [Article D741-76](article-d741-76.md)
- [Article D741-77](article-d741-77.md)
- [Article D741-78](article-d741-78.md)
