# Article D741-70-2

Pour bénéficier de l'exonération prévue à l'article L. 741-15-1, les groupements d'employeurs mentionnés à l'article précédent doivent en formuler la demande auprès de la caisse de mutualité sociale agricole d'affiliation de leurs salariés lors de la déclaration préalable à l'embauche prévue à l'article L. 320 du code du travail.

Cette déclaration doit alors être accompagnée d'une attestation précisant qu'ils ne bénéficient pas déjà d'une exonération totale de cotisations en application de l'article L. 741-16.
