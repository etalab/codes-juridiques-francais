# Article D741-51

Pour l'application des dispositions de l'article L. 741-24 du présent code, les dispositions de l'article D. 241-1-1 du code de la sécurité sociale sont applicables aux salariés du régime agricole sous réserve des adaptations suivantes :

1° Les dispositions du 2° de l'article D. 241-1-1 sont applicables aux salariés agricoles ne remplissant pas les conditions mentionnées au 1° de cet article ;

2° Les dispositions du 3° ne sont pas applicables ;

3° La référence à l'article L. 241-3-1 du code de la sécurité sociale est remplacée par la référence à l'article L. 741-24 du présent code et la référence à l'article L. 242-1 du code de la sécurité sociale est remplacée par la référence à l'article L. 741-10 du présent code.
