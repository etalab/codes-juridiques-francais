# Section 4 : Dispositions diverses.

- [Article D741-98](article-d741-98.md)
- [Article D741-102](article-d741-102.md)
- [Article D741-104](article-d741-104.md)
