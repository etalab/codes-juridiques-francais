# Article D741-33

La cotisation         de prestations familiales prévue à l'article L. 741-2 est assise sur les gains et rémunérations déterminés selon les modalités prévues aux articles L. 741-10 et R. 741-37, ou sur une assiette forfaitaire ou réduite lorsqu'elle est prévue par voie législative ou réglementaire.
