# Article D741-34

Le recouvrement de la cotisation prévue à l'article L. 741-2 s'effectue dans les conditions prévues par les articles R. 741-2 à R. 741-31.
