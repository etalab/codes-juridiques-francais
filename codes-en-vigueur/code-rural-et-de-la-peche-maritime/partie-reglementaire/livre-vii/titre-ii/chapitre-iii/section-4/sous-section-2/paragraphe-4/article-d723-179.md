# Article D723-179

Dans les cas fixés à l'article D. 723-201 le directeur peut, sous sa responsabilité, requérir par écrit qu'il soit passé outre au refus de visa et de paiement opposé par l'agent comptable à l'encontre d'un ordre de dépense émis par lui.

La réquisition de paiement a pour effet d'engager la responsabilité personnelle et pécuniaire du directeur. La responsabilité du directeur est mise en cause dans les conditions fixées par les articles D. 122-11 à D. 122-18 du code de la sécurité sociale et par l'article D. 723-210-1 du présent code.
