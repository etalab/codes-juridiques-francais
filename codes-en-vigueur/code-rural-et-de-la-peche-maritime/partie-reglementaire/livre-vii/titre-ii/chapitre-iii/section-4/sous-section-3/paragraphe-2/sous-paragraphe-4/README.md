# Sous-paragraphe 4 : Garde des fonds et valeurs.

- [Article D723-204](article-d723-204.md)
- [Article D723-205](article-d723-205.md)
- [Article D723-206](article-d723-206.md)
- [Article D723-207](article-d723-207.md)
- [Article D723-208](article-d723-208.md)
