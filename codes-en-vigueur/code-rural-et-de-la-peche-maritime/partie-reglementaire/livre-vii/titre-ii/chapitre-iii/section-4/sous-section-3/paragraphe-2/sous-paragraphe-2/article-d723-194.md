# Article D723-194

La prise en charge de l'ordre de recette est datée et signée par l'agent comptable ou son délégué.

L'agent comptable vérifie, dans les conditions définies à l'article D. 723-191, la régularité des ordres de recettes établis et signés par le directeur.
