# Sous-paragraphe 2 : Encaissement.

- [Article D723-193](article-d723-193.md)
- [Article D723-194](article-d723-194.md)
- [Article D723-197](article-d723-197.md)
