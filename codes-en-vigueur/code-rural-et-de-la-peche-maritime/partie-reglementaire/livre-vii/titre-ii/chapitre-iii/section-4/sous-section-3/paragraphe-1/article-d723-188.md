# Article D723-188

L'agent comptable rend compte de ses actes devant le conseil d'administration ainsi que devant les autorités qui l'ont agréé.
