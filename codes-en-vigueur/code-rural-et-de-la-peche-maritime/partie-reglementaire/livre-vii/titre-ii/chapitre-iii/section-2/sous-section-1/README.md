# Sous-section 1 : Elections des délégués cantonaux

- [Paragraphe 1 : Dispositions générales.](paragraphe-1)
- [Paragraphe 2 : Etablissement des listes électorales et contentieux.](paragraphe-2)
- [Paragraphe 3 : Opérations préparatoires au scrutin et déclarations de candidatures](paragraphe-3)
- [Paragraphe 4 : Déroulement des opérations électorales](paragraphe-4)
- [Paragraphe 5 : Recensement des votes et proclamation des résultats](paragraphe-5)
- [Paragraphe 6 : Contentieux.](paragraphe-6)
