# Sous-paragraphe 4 : Dispositions communes aux trois collèges.

- [Article R723-58](article-r723-58.md)
- [Article R723-59](article-r723-59.md)
- [Article R723-60](article-r723-60.md)
