# Sous-paragraphe 1 : Opérations préliminaires.

- [Article R723-42](article-r723-42.md)
- [Article R723-43](article-r723-43.md)
- [Article R723-44](article-r723-44.md)
