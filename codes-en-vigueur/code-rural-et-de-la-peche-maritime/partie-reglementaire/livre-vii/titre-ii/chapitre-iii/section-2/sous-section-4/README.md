# Sous-section 4 : Assemblées générales

- [Paragraphe 1 : Caisses départementales et pluridépartementales.](paragraphe-1)
- [Paragraphe 2 : Caisse centrale de la mutualité sociale agricole.](paragraphe-2)
