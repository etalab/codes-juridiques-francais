# Sous-section 1 : Caisses départementales et pluridépartementales de mutualité sociale agricole

- [Paragraphe 1 : Statuts et règlements intérieurs.](paragraphe-1)
- [Paragraphe 2 : Fusion des caisses de mutualité sociale agricole.](paragraphe-2)
- [Paragraphe 3 : Regroupement des caisses de mutualité sociale agricole.](paragraphe-3)
- [Paragraphe 4 : Participation des caisses de mutualité sociale agricole aux organismes mentionnés à l'article L. 723-7.](paragraphe-4)
