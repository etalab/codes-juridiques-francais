# Paragraphe 2 : Fusion des caisses de mutualité sociale agricole.

- [Article D723-4](article-d723-4.md)
- [Article D723-5](article-d723-5.md)
- [Article D723-6](article-d723-6.md)
- [Article D723-7](article-d723-7.md)
- [Article D723-8](article-d723-8.md)
- [Article D723-9](article-d723-9.md)
- [Article D723-10](article-d723-10.md)
- [Article D723-11](article-d723-11.md)
- [Article D723-12](article-d723-12.md)
- [Article D723-13](article-d723-13.md)
