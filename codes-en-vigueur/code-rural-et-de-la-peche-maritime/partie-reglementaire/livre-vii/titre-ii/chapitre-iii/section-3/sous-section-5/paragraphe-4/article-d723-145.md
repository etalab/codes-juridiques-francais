# Article D723-145

Les médecins-conseils chefs de service ne peuvent être nommés par le conseil d'administration de la caisse de mutualité sociale agricole intéressée que s'ils figurent sur une liste d'aptitude. Un arrêté fixe les conditions d'inscription sur cette liste d'aptitude. Cette liste d'aptitude est arrêtée, après consultation des groupements d'organismes assureurs mentionnés à l'article L. 731-30, par le ministre chargé de l'agriculture.
