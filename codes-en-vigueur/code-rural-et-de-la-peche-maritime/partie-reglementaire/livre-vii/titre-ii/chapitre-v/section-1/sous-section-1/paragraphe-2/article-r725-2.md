# Article R725-2

En application de l'article L. 725-2, toute personne physique ou morale doit, pour obtenir le bénéfice des subventions en vue de favoriser les investissements de modernisation matériels et immatériels dans les exploitations et entreprises agricoles, être quitte, au 1er janvier de l'année au titre de laquelle l'aide est sollicitée, de ses obligations concernant le paiement des cotisations et contributions légalement exigibles aux régimes de protection sociale agricole. Les personnes bénéficiant d'un échéancier de paiements sont réputées s'être acquittées de leurs obligations.

La justification de l'acquittement est apportée par la Caisse centrale de la mutualité sociale agricole et les organismes mentionnés aux articles L. 731-31 et L. 752-14 selon les modalités définies par les articles R. 723-116 à R. 723-118.
