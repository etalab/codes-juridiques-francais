# Sous-section 1 : Règles de recouvrement

- [Paragraphe 1 : Imputation des cotisations sur les prestations sociales.](paragraphe-1)
- [Paragraphe 2 : Condition posée par l'article L. 725-2 pour l'attribution de certains avantages d'ordre économique.](paragraphe-2)
- [Paragraphe 3 : Admission en non-valeur et réduction des créances des organismes de mutualité sociale agricole et des autres organismes mentionnés à l'article L. 731-30.](paragraphe-3)
- [Paragraphe 4 :  Vérification des déclarations](paragraphe-4)
- [Paragraphe 5 : Dispositions diverses](paragraphe-5)
