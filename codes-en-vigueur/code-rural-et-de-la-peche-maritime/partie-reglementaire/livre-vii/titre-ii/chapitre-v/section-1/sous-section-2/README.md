# Sous-section 2 : Procédures de recouvrement.

- [Paragraphe 1 : Mise en demeure.](paragraphe-1)
- [Paragraphe 2 : Contrainte.](paragraphe-2)
- [Paragraphe 3 : Opposition entre les mains de tiers détenteurs](paragraphe-3)
- [Paragraphe 4 : Procédure sommaire.](paragraphe-4)
- [Paragraphe 5 : Dispositions communes.](paragraphe-5)
- [Article R725-5](article-r725-5.md)
