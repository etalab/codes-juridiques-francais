# Article R724-7

Sauf s'il est diligenté par un fonctionnaire cité à l'article L. 724-2 du présent code ou s'il est effectué pour rechercher des infractions aux interdictions mentionnées à l'article L. 8221-1 du code du travail, tout contrôle effectué en application de l'article L. 724-11 du présent code est précédé de l'envoi par la caisse de mutualité sociale agricole d'un avis adressé par par tout moyen permettant de rapporter la preuve de sa réception, à l'employeur, au chef d'exploitation ou au titulaire d'allocation de vieillesse agricole ou de pension de retraite intéressé.
