# Article D724-13

La caisse centrale de la mutualité sociale agricole est soumise au contrôle budgétaire de l'Etat. Un arrêté du ministre chargé de l'économie et des finances fixe les modalités d'exercice de ce contrôle.
