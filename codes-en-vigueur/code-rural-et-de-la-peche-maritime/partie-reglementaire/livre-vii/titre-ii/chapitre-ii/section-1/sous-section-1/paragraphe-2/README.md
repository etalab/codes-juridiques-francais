# Paragraphe 2 : Condition d'assujettissement prévue à l'article L. 722-5 et relative à l'importance minimale de l'exploitation ou de l'entreprise

- [Sous-paragraphe 1 : Affiliation dérogatoire  prévue au second alinéa de l'article L. 722-6.](sous-paragraphe-1)
- [Sous-paragraphe 2 : Conditions du maintien de l'affiliation prévu à l'article L. 722-7.](sous-paragraphe-2)
