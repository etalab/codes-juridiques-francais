# Sous-paragraphe 2 : Maintien des prestations en nature au bénéfice de certains agriculteurs cessant leur activité.

- [Article D722-23](article-d722-23.md)
- [Article D722-24](article-d722-24.md)
