# Article R722-18

A partir du 1er janvier de chaque année, les affiliations d'office sont effectuées dans chaque circonscription des caisses de mutualité sociale agricole proportionnellement aux effectifs recueillis dans cette circonscription par chacun des organismes assureurs au 1er octobre précédent.

Afin de permettre le recensement des effectifs, chaque organisme assureur doit communiquer à la caisse de mutualité sociale agricole les bulletins d'adhésion recueillis dans la circonscription de celle-ci en vue de l'affiliation des intéressés. Ces bulletins doivent être conformes au modèle approuvé par le ministre chargé de l'agriculture.
