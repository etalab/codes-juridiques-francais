# Paragraphe 2 : Assurance maladie, invalidité et maternité

- [Sous-paragraphe 1 : Immatriculation, affiliation d'office, radiation, dénonciation d'affiliation.](sous-paragraphe-1)
- [Sous-paragraphe 2 : Maintien des prestations en nature au bénéfice de certains agriculteurs cessant leur activité.](sous-paragraphe-2)
