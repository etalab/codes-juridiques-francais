# Article R726-16

Les dépenses du fonds social de l'assurance maladie des exploitants agricoles sont incluses dans les autorisations budgétaires fixées par la convention d'objectifs et de gestion mentionnée à l'article L. 723-12.
