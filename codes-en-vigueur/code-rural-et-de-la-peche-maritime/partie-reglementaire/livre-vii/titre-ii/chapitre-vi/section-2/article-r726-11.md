# Article R726-11

Sur convocation de son président, le comité national se réunit au moins une fois par semestre au siège de la Caisse centrale de la mutualité sociale agricole, qui assure le secrétariat du comité.

Le directeur chargé de la protection sociale agricole au ministère de l'agriculture ou son représentant peut assister aux réunions du comité national.
