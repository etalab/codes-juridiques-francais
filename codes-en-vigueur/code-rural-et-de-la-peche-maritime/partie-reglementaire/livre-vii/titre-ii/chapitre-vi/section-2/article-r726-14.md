# Article R726-14

Le comité départemental ou pluridépartemental :

1° Attribue, sur proposition des organismes assureurs, les prestations supplémentaires dans les conditions prévues par son règlement intérieur ;

2° Propose à l'agrément du comité national les actions prévues aux 3° et 4° de l'article R. 726-7 ou, à la demande du comité national, formule son avis sur ces actions.

Les décisions du comité sont soumises à la procédure de communication et aux fins prescrites par les articles R. 152-2 et suivants du code de la sécurité sociale. Toutefois, ne sont pas soumises à la procédure de communication :

1° Les décisions individuelles prises en application d'un barème local approuvé par le préfet de région ;

2° Les décisions d'octroi de secours urgents, lorsque l'aide n'excède pas un seuil par foyer et par an de 38 % de la valeur mensuelle du plafond prévu par l'article L. 241-3 du code de la sécurité sociale.
