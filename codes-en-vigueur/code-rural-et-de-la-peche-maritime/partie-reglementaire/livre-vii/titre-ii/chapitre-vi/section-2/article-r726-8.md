# Article R726-8

Les décisions du comité national et des comités départementaux ou pluridépartementaux, intervenues dans l'exercice des attributions définies à l'article L. 726-2 et à la présente section, sont prises pour le compte de la Caisse centrale de la mutualité sociale agricole, chargée de la gestion du fonds social.
