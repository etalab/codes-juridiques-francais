# Article D752-62

La gestion du fonds de prévention prévu à l'article L. 752-29 est confiée à la Caisse centrale de mutualité sociale agricole.
