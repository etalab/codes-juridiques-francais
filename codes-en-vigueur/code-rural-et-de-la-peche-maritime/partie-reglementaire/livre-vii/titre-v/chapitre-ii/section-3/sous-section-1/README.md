# Sous-section 1 : Organisation

- [Paragraphe 1 : Dispositions relatives aux caisses de mutualité sociale agricole.](paragraphe-1)
- [Paragraphe 2 : Dispositions relatives aux organismes assureurs autres que les caisses de mutualité sociale agricole.](paragraphe-2)
- [Paragraphe 3 : Dispositions communes aux caisses de mutualité sociale agricole et aux autres organismes assureurs.](paragraphe-3)
- [Paragraphe 4 : Sanctions.](paragraphe-4)
