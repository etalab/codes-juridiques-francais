# Article R752-39

Pour l'application de l'article L. 752-14, les organismes assureurs sont autorisés à participer à la gestion de l'assurance prévue au présent chapitre par arrêté du ministre chargé de l'agriculture. Cette autorisation est subordonnée à :

1° La détention de l'agrément du ministre chargé de l'économie et des finances mentionné aux articles L. 321-1, L. 321-7 et L. 321-9 du code des assurances, ou de l'attestation délivrée par le ministre chargé de l'économie et des finances certifiant que les informations prévues aux articles L. 362-1 et L. 362-2 du code des assurances lui ont été transmises, ou de l'agrément prévu à l'article L. 211-7 du code de la mutualité ;

2° L'adhésion au groupement mentionné à l'article L. 752-14 et à la délégation à celui-ci de toutes les opérations relatives au fonctionnement du régime.
