# Paragraphe 2 : Dispositions relatives aux organismes assureurs autres que les caisses de mutualité sociale agricole.

- [Article R752-39](article-r752-39.md)
- [Article R752-40](article-r752-40.md)
- [Article R752-41](article-r752-41.md)
- [Article R752-42](article-r752-42.md)
- [Article R752-43](article-r752-43.md)
