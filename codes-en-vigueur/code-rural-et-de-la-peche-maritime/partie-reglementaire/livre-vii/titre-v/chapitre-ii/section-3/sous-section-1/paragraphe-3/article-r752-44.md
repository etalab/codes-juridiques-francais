# Article R752-44

Les caisses de mutualité sociale agricole et le groupement mentionné à l'article L. 752-14, chacun en ce qui le concerne, sont chargés pour leurs assurés :

1° De l'enregistrement des affiliations ;

2° De la tenue du fichier de leurs assurés ;

3° Du calcul des cotisations sur la base de l'arrêté prévu à l'article L. 752-16 ;

4° De l'appel, de l'encaissement et du recouvrement des cotisations ;

5° De l'enquête éventuelle sur les circonstances de l'accident ;

6° De prendre la décision de prise en charge ou de refus de prise en charge prévue à l'article L. 752-25 ;

7° De la fixation de la date de guérison ou de consolidation de la blessure, dans les conditions prévues à l'article L. 752-24 ;

8° De la liquidation et du paiement des prestations en nature et en espèces ;

9° De la gestion du contentieux relatif notamment aux cotisations, prestations, recours contre tiers, récupération des indus.
