# Article R752-48

Sans préjudice des dispositions législatives autorisant le transfert de données, les informations transmises en application du présent article ne peuvent être utilisées à d'autres fins que la gestion du régime.
