# Article R752-5-1

L'autorité administrative compétente mentionnée au deuxième alinéa de l'article L. 752-13 est le directeur régional de l'alimentation, de l'agriculture et de la forêt.
