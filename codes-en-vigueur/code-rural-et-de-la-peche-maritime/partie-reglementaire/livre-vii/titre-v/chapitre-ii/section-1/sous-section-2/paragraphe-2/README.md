# Paragraphe 2 : Consultation des comités régionaux de reconnaissance des maladies professionnelles.

- [Article D752-8](article-d752-8.md)
- [Article D752-9](article-d752-9.md)
- [Article D752-10](article-d752-10.md)
- [Article D752-11](article-d752-11.md)
- [Article D752-12](article-d752-12.md)
- [Article D752-13](article-d752-13.md)
- [Article D752-14](article-d752-14.md)
