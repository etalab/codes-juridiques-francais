# Article D752-79

Les décisions prises en application de l'article L. 752-24 par la caisse de mutualité sociale agricole ou par le groupement, après avis du service du contrôle médical, doivent être médicalement motivées.

Ces décisions, ainsi que celles prises en application de l'article D. 752-30, doivent être notifiées à la victime par lettre recommandée avec avis de réception. Il en est de même des propositions et notifications établies conformément aux dispositions de l'article D. 752-29.
