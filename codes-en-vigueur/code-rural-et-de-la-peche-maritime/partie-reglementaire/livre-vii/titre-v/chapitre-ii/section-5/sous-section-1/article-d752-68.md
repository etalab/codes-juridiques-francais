# Article D752-68

Le praticien, l'auxiliaire médical, le pharmacien, le fournisseur ou l'établissement hospitalier adresse sa note d'honoraires ou sa facture à la caisse de mutualité sociale agricole ou au groupement tel qu'il est mentionné sur la feuille d'accident présentée par la victime.
