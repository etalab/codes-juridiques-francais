# Sous-section 1 : Déclaration de l'accident du travail ou de la maladie professionnelle.

- [Article D752-65](article-d752-65.md)
- [Article D752-66](article-d752-66.md)
- [Article D752-67](article-d752-67.md)
- [Article D752-68](article-d752-68.md)
