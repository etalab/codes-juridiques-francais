# Paragraphe 2 : Rentes

- [Sous-paragraphe 1 : Rentes dues à la victime.](sous-paragraphe-1)
- [Sous-paragraphe 2 : Rentes d'ayants droit.](sous-paragraphe-2)
