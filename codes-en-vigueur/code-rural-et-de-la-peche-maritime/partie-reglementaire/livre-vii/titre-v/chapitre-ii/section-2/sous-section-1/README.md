# Sous-section 1 : Dispositions générales.

- [Paragraphe 1 : Service des prestations en cas de changement d'organisme assureur.](paragraphe-1)
- [Paragraphe 2 : Prescription.](paragraphe-2)
- [Article D752-17](article-d752-17.md)
