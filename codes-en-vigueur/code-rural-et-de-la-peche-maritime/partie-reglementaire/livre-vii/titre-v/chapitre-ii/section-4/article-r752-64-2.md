# Article R752-64-2

Devant les juridictions civiles, le greffe du tribunal informe l'organisme assureur de la victime de la date de l'audience, dès que celle-ci est fixée.
