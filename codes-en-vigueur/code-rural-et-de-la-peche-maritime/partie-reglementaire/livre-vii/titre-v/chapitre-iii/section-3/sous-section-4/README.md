# Sous-section 4 : Dispositions diverses.

- [Article D753-13](article-d753-13.md)
- [Article D753-14](article-d753-14.md)
