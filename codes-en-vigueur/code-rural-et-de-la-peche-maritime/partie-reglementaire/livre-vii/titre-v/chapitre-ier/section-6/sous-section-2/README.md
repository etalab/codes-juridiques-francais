# Sous-section 2 : Contrôle et procédure postérieurs à l'accident

- [Paragraphe 1 : Contrôle médical.](paragraphe-1)
- [Paragraphe 2 : Contrôle administratif.](paragraphe-2)
