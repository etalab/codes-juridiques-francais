# Article R751-136

Les honoraires et frais de déplacement dus au médecin traitant ou au médecin spécialiste du fait du contrôle médical ou au médecin chargé du nouvel examen médical dans les conditions prévues aux articles R. 751-133 à R. 751-135 sont supportés dans les mêmes conditions qu'en matière d'assurances sociales agricoles et selon un tarif fixé par arrêté conjoint du ministre chargé de l'agriculture, du ministre chargé de la santé, du ministre chargé de la sécurité sociale et du ministre chargé du budget.
