# Article D751-114

Les contestations auxquelles donneraient lieu la fixation ou la liquidation des frais d'enquête (à l'exception des frais d'autopsie) devront être soumises au tribunal des affaires de sécurité sociale ou, le cas échéant, à la section de cette juridiction compétente en matière agricole.
