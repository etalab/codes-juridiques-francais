# Article D751-91

La caisse de mutualité sociale agricole peut décider le retrait de l'autorisation de tenue d'un registre pour l'une des raisons suivantes :

1° Tenue incorrecte du registre ;

2° Disparition des conditions d'octroi ;

3° Refus de présentation du registre :

a) Aux agents de contrôle des caisses de mutualité sociale agricole et aux agents chargés du contrôle de la prévention ;

b) Aux agents de l'inspection du travail ;

c) A la victime d'un accident consigné au registre.

La caisse notifie sa décision motivée de retrait de l'autorisation.
