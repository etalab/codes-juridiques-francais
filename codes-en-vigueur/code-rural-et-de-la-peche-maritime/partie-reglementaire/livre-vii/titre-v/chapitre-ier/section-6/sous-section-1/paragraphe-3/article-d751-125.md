# Article D751-125

Lorsque la commission des rentes instituée à l'article R. 751-62 n'est pas en mesure d'établir des propositions relatives au taux d'incapacité dès la fixation de la date de consolidation de la blessure, la caisse de mutualité sociale agricole précise dans la notification de sa décision relative à la fixation de cette date les délais qui paraissent nécessaires pour faire connaître lesdites propositions.
