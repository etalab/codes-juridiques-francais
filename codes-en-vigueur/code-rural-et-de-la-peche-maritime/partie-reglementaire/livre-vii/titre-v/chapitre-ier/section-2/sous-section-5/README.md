# Sous-section 5 : Déclaration médicale de certaines maladies professionnelles.

- [Article D751-29](article-d751-29.md)
- [Article R751-28](article-r751-28.md)
