# Sous-section 6 : Déclaration de la maladie par la victime.

- [Article R751-30](article-r751-30.md)
- [Article R751-31](article-r751-31.md)
