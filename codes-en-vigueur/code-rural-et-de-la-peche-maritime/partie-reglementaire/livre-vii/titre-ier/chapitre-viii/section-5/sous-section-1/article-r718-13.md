# Article R718-13

Les membres de la commission nationale de conciliation des professions agricoles mentionnée à l'article R. 718-10 sont nommés par arrêté conjoint du ministre chargé de l'agriculture et du ministre chargé du travail, dans les conditions prévues au premier alinéa de l'article R. 2522-14 et à l'article R. 2522-16 du code du travail.

Les articles R. 2522-17 à R. 2522-21 du code du travail s'appliquent à la Commission nationale de conciliation agricole.
