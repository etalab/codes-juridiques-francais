# Article D718-16

Pour les chefs d'exploitation ou d'entreprise agricoles, la contribution prévue à l'article L. 718-2-1 ne peut être :

- ni inférieure à 0,17 %, ni supérieure à 0,89 % du montant annuel du plafond de la sécurité sociale prévu à l'article L. 241-3 du code de la sécurité sociale, à compter du 1er janvier 2014.

Pour les personnes mentionnées au deuxième alinéa de l'article L. 718-2-1, la contribution est égale à :

0,10 % du montant annuel du plafond de la sécurité sociale prévu à l'article L. 241-3 du code de la sécurité sociale, pour la période allant du 1er janvier au 31 décembre 2009 ;

0,137 % de ce même plafond à compter du 1er janvier 2010.

Pour les nouveaux chefs d'exploitation ou d'entreprise agricoles dont la durée d'assujettissement ne permet pas de connaître les revenus professionnels, le montant de l'assiette forfaitaire servant au calcul de cette contribution, à titre provisionnel, est celui mentionné à l'article D. 731-31 du code rural et de la pêche maritime pour le calcul des cotisations d'assurance maladie, invalidité et maternité.
