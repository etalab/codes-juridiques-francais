# Article D718-5

Le contrat de travail prévu à l'article précédent peut être conclu pour une durée maximale de vingt-quatre mois. Il ne peut pas être renouvelé.
