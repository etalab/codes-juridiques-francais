# Section 1 : Hébergement en résidence fixe

- [Sous-section 1 : Dispositions communes.](sous-section-1)
- [Sous-section 2 : Hébergement en logement individuel.](sous-section-2)
- [Sous-section 3 : Hébergement collectif des travailleurs saisonniers.](sous-section-3)
- [Sous-section 4 : Mesures d'application.](sous-section-4)
- [Sous-section 5 : Dérogations.](sous-section-5)
