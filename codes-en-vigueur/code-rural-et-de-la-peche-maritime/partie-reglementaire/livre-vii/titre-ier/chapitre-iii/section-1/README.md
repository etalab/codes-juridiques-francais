# Section 1 : Dispositions générales.

- [Article D713-5](article-d713-5.md)
- [Article D713-7](article-d713-7.md)
- [Article D713-8](article-d713-8.md)
- [Article R713-1](article-r713-1.md)
- [Article R713-2](article-r713-2.md)
- [Article R713-3](article-r713-3.md)
- [Article R713-4](article-r713-4.md)
- [Article R713-6](article-r713-6.md)
