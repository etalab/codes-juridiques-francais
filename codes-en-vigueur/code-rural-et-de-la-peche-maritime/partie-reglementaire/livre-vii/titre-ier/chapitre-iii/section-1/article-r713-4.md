# Article R713-4

Les heures perdues en dessous de la durée légale du travail à la suite d'une interruption collective résultant d'une cause prévue à l'article L. 713-4 peuvent être récupérées dans les conditions suivantes :

1° La récupération ne peut concerner que les salariés présents lors de l'interruption. Elle est effectuée dans la période de vingt-six semaines qui suit la semaine au cours de laquelle a eu lieu l'interruption. Le nombre d'heures de récupération ne peut excéder huit par semaine ;

2° Lorsque l'interruption est consécutive à l'une des causes prévues au 1° de l'article L. 713-4, l'employeur, qui se réserve la possibilité de faire récupérer les heures perdues, en informe l'inspecteur du travail ; lorsque l'interruption concerne l'ensemble des entreprises relevant d'un même type d'activité, il peut être procédé à cette information par l'organisation patronale intéressée ;

3° Les heures qui ont donné lieu au paiement des allocations légales pour privation partielle d'emploi ne peuvent être récupérées.

Il ne peut être dérogé aux dispositions des 2° et 3° du présent article par des conventions ou accords étendus ou par des accords d'entreprise ou d'établissement.
