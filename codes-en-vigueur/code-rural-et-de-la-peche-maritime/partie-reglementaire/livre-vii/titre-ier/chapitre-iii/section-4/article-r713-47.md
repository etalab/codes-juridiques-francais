# Article R713-47

L'employeur enregistre, pour chaque salarié, sur un document prévu à cet effet :

1° Lorsque l'employeur entre dans le champ d'application des deuxième et troisième alinéas de l'article L. 713-9 et en l'absence, sur les deux points mentionnés ci-après, de toute disposition dans un accord conclu entre des organisations syndicales d'employeurs et de salariés les plus représentatives au plan national :

a) Le nombre d'heures de repos compensateur porté au crédit du salarié ;

b) Lorsque le droit à ce repos compensateur est ouvert, une mention rappelant le délai dans lequel il doit être pris ;

2° Lorsque l'employeur entre dans le champ d'application de l'article L. 713-10 et lorsque le droit du salarié est ouvert :

a) Le nombre de journées ou de demi-journées de congé porté à son crédit ;

b) Le cas échéant, le délai dans lequel ces journées ou demi-journées doivent être prises ;

3° Lorsque des heures supplémentaires donnent lieu à la bonification sous forme de repos prévue au I de l'article L. 713-6 ou lorsque des droits à repos compensateur sont acquis en application de l'article L. 713-7 :

a) Le nombre d'heures de repos porté au crédit du salarié ;

b) Le cas échéant, une mention précisant l'ouverture du droit à repos et le délai dans lequel ce repos doit être pris ;

4° Lorsque l'employeur applique l'organisation du travail prévue à l'article L. 713-14 : le résultat de la compensation effectuée depuis le début de la période mentionnée à cet article entre les heures accomplies au-delà de la durée hebdomadaire moyenne fixée par la convention ou l'accord et les heures non travaillées en deçà de cette durée ;

5° Lorsque l'employeur fait application de l'article L. 212-9 du code du travail : le nombre de journées ou de demi-journées de repos attribuées à ce titre ;

6° La nature et la durée des repos pris chaque mois en application des dispositions mentionnées aux 1°, 2°, 3° et 5° ci-dessus et des autres périodes d'absence en précisant si elles ont été ou non rémunérées.

L'employeur remet au salarié, dans les conditions et avec les effets prévus au deuxième alinéa de l'article R. 713-36, une copie des informations mentionnées aux alinéas 1 à 4 du présent article.

Le document mentionné au dernier alinéa du III de l'article L. 212-15-3 du code du travail doit comporter la récapitulation pour chaque année du nombre de journées et de demi-journées travaillées par chaque salarié.
