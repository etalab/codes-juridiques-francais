# Article R713-50

Le support informatique mentionné au deuxième alinéa de l'article L. 620-7 du code du travail doit permettre d'obtenir, sans difficulté d'utilisation et de compréhension et sans risque d'altération, toutes les mentions obligatoires des documents prévus aux articles R. 713-36 et R. 713-47. Il doit être présenté dans les mêmes conditions et conservé dans le même délai que le document auquel il se substitue.

En cas de traitement automatisé de données nominatives, le chef d'établissement ou le responsable du traitement doit justifier à l'inspecteur du travail de la délivrance du récépissé attestant qu'il a effectué la déclaration préalable prévue par la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés.

Les délégués du personnel peuvent consulter les documents et autres supports mentionnés aux articles R. 713-35 à R. 713-37 et R. 713-47.
