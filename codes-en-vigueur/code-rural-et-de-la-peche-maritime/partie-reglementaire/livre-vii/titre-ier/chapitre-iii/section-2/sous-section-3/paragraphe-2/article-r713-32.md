# Article R713-32

Les demandes sont adressées au directeur régional des entreprises, de la concurrence, de la consommation, du travail et  de l'emploi. Celui-ci examine si les circonstances invoquées sont de nature à justifier l'octroi de la dérogation et, dans le cas où cette dernière doit intéresser la totalité ou plusieurs des entreprises relevant d'un même type d'activités dans une région déterminée, procède à la consultation des organisations syndicales d'employeurs et de salariés les plus représentatives dans ce type d'activités et dans cette région.

La décision du directeur régional des entreprises, de la concurrence, de la consommation, du travail et  de l'emploi  précise les modalités de la dérogation ainsi que la durée pour laquelle elle est accordée.

Lorsqu'une dérogation a été accordée par application des alinéas précédents pour un type d'activités et une région déterminée, les employeurs concernés qui désirent en user doivent consulter le comité d'entreprise ou, à défaut, les délégués du personnel sur cette intention et transmettre l'avis ainsi recueilli au directeur régional des entreprises, de la concurrence, de la consommation, du travail et  de l'emploi.
