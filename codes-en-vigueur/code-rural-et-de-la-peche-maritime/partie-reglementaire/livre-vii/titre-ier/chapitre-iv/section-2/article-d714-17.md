# Article D714-17

Une convention ou un accord collectif étendu ou un accord collectif d'entreprise ou d'établissement peut prévoir une réduction de la durée du repos quotidien en cas de surcroît d'activité.
