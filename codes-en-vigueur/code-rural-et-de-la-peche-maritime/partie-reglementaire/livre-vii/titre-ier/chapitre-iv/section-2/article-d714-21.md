# Article D714-21

La mise en oeuvre des dérogations prévues aux articles D. 714-16, D. 714-17, D. 714-19 et D. 714-20 est soumise à la condition que des périodes au moins équivalentes de repos soient accordées aux salariés concernés. Lorsque l'octroi de ce repos n'est pas possible, une contrepartie équivalente doit être prévue par accord collectif.
