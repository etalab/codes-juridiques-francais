# Article R712-52-9

Dans le respect des règles liées à l'exercice de la profession d'infirmier déterminées en application de l'article L. 4311-1 du code de la santé publique, l'infirmier exerce ses missions propres et celles définies par le médecin du travail, sur la base des protocoles écrits mentionnés à l'article R. 717-52-3.
