# Paragraphe 3 : Interne en médecine du travail

- [Article R717-52-5](article-r717-52-5.md)
- [Article R717-52-6](article-r717-52-6.md)
- [Article R717-52-7](article-r717-52-7.md)
