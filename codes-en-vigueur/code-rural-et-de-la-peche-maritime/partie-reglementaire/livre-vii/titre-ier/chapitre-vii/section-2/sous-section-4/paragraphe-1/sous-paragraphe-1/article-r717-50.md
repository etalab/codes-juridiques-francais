# Article R717-50

Seul un médecin remplissant l'une des conditions suivantes peut pratiquer la médecine du travail :

1° Remplir les conditions mentionnées à l'article R. 4623-2 du code du travail ;

2° Etre titulaire du diplôme délivré par l'Institut national de médecine agricole.

Ces dispositions ne sont pas applicables aux médecins qui exerçaient la médecine du travail en agriculture antérieurement au 12 juillet 1968.
