# Article D717-26-1

Les dispositions de la présente section sont applicables au service de santé au travail dont bénéficient les salariés des entreprises de travail temporaire ou de groupements d'employeurs, exclusivement occupés dans une ou plusieurs entreprises agricoles, sous réserve des modalités particulières prévues par le présent sous-paragraphe.
