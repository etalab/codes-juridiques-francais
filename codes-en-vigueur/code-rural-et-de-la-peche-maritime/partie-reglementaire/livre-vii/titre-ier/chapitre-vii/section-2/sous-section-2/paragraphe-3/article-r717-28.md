# Article R717-28

A l'issue de chacun des examens médicaux prévus aux articles R. 717-14 à R. 717-17, et à l'article R. 717-22 le médecin du travail établit une fiche d'aptitude en double exemplaire.

Il en remet un exemplaire au salarié et transmet l'autre à l'employeur.

La remise d'une attestation d'entretien infirmier, établie en double exemplaire, permet d'attester la présence du salarié à l'examen médical. Un exemplaire est remis au salarié, l'autre est transmis à l'employeur.

Lorsque le salarié en fait la demande, ou lorsqu'il quitte une entreprise dotée d'un service autonome d'entreprise, le médecin du travail établit une fiche médicale en double exemplaire. Il en remet un exemplaire au salarié et conserve le second dans le dossier médical de l'intéressé.

Lorsque le service de santé au travail est assuré dans les conditions prévues par les articles R. 717-34 et R. 717-35, le conseil d'administration de la caisse de mutualité sociale agricole ou de l'association spécialisée peut, notamment pour l'application de l'article R. 717-15 et dans tout ou partie des entreprises assujetties aux dispositions des sous-sections 1 à 6 de la présente section, substituer aux fiches d'aptitude mentionnées ci-dessus un registre à feuillets fixes ou mobiles sur lesquels l'employeur portera, avant l'examen, l'indication de l'identité et du numéro d'immatriculation de chaque salarié ainsi que la mention du poste occupé par celui-ci. Le médecin du travail complétera les feuillets et remettra en outre au salarié la fiche mentionnée au premier alinéa.

Le modèle de la fiche d'aptitude et des fiches médicales est fixé par arrêté du ministre chargé de l'agriculture.
