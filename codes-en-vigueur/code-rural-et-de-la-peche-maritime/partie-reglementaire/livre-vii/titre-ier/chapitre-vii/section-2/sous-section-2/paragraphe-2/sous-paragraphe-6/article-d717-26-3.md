# Article D717-26-3

Les conditions dans lesquelles le médecin de l'entreprise de travail temporaire ou le groupement d'employeurs a accès aux postes de travail occupés ou susceptibles d'être occupés par des travailleurs temporaires sont fixées entre l'entreprise utilisatrice et l'entreprise de travail temporaire ou le groupement d'employeurs, après avis des médecins du travail concernés.
