# Article R717-3-1

Les actions sur le milieu de travail concernent notamment :

1° La visite de lieux de travail ;

2° L'étude de postes en vue de l'amélioration des conditions de travail, de leur adaptation dans certaines situations ou du maintien dans l'emploi ;

3° L'identification et l'analyse des risques professionnels ;

4° L'élaboration et la mise à jour de la fiche d'entreprise prévue à l'article D. 717-31  (1);

5° La délivrance de conseils en matière d'organisation des secours et des services d'urgence ;

6° La participation aux réunions du comité d'hygiène, de sécurité et des conditions de travail ou de la commission paritaire d'hygiène, de sécurité et des conditions de travail ;

7° La réalisation des mesures métrologiques ;

8° L'animation de campagnes d'information et de sensibilisation aux questions de santé publique ;

9° Les enquêtes épidémiologiques ;

10° La formation aux risques spécifiques ;

11° L'étude de toute nouvelle technique de production ;

12° L'élaboration des actions de formation à la sécurité prévue à l'article L. 4141-2 du code du travail et à celle des secouristes prévues à l'article D. 717-57 (2).
