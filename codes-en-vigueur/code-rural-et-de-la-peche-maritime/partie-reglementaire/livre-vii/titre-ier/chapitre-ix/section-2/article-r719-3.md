# Article R719-3

Est puni de l'amende prévue pour les contraventions de la quatrième classe le fait de contrevenir à l'une des dispositions prévues par :

1° Les décrets pris pour l'application des articles L. 713-2 et L. 713-3 relatifs à la durée légale et à la durée quotidienne du travail effectif des salariés ;

2° L'article L. 713-6 fixant les modalités de décompte et de majoration des heures supplémentaires effectuées au-delà de la durée hebdomadaire du travail prévue à l'article L. 713-2 ou de la durée considérée comme équivalente ;

3° Les articles L. 713-7, L. 713-9 et L. 713-10 relatifs aux modalités d'octroi d'un repos compensateur en remplacement de tout ou partie du paiement des heures supplémentaires et de leurs majorations ;

4° Les articles L. 713-11 et L. 713-12 relatifs aux règles de fixation du contingent d'heures supplémentaires ;

5° L'article L. 713-13 limitant l'exécution d'heures supplémentaires en fonction de la durée hebdomadaire de travail ;

6° L'article L. 713-20 relatif aux obligations mises à la charge de l'employeur pour permettre le contrôle de l'application des dispositions légales et conventionnelles relatives à la durée et à l'aménagement du temps de travail, ainsi qu'aux décrets pris pour son application.

Est puni de l'amende prévue pour les contraventions de la quatrième classe le fait pour l'employeur :

1° De ne pas accorder les compensations prévues au deuxième alinéa du III de l'article L. 713-5 ;

2° De ne pas remettre à chaque salarié concerné, ou de ne pas conserver à la disposition des agents de contrôle de l'inspection du travail, le document mentionné au troisième alinéa du III de l'article L. 713-5 ;

3° De ne pas accorder le bénéfice du repos quotidien prévu à l'article L. 714-5.
