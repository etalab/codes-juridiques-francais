# Article D654-60

I. - L'acheteur déclare à FranceAgriMer, dans les 30 jours suivant la fin de la campagne, l'identité des producteurs qui demandent un ajustement temporaire de leurs quotas individuels, ainsi que le montant de cet ajustement.

II. - L'acheteur déclare à FranceAgriMer, avant le 15 décembre, l'identité des producteurs qui demandent un ajustement définitif de leurs quotas individuels, ainsi que le montant de cet ajustement.
