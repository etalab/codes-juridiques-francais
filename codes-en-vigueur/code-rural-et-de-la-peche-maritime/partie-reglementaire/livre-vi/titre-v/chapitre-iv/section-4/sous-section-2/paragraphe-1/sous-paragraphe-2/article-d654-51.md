# Article D654-51

Après la fin de la campagne, FranceAgriMer fait connaître à chaque acheteur de lait le montant du prélèvement éventuellement dû.

L'acheteur, redevable du prélèvement, verse le montant du prélèvement à l'agent comptable de FranceAgriMer dans le mois suivant cette notification et au plus tard le 1er octobre suivant la fin de la campagne.
