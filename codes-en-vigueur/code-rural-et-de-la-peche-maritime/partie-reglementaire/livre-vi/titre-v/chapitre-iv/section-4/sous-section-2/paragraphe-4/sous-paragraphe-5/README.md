# Sous-paragraphe 5 : Recouvrement du prélèvement  en cas de dépassement du quota national.

- [Article D654-89](article-d654-89.md)
- [Article D654-90](article-d654-90.md)
- [Article D654-91](article-d654-91.md)
