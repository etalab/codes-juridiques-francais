# Article D654-64

L'acheteur déclare à FranceAgriMer, dans le mois qui suit cette prise en charge, les producteurs nouvellement pris en charge, la date de la première livraison ainsi que, le cas échéant, le quota individuel et le taux de référence de matière grasse dont ils disposaient auprès de l'acheteur précédent.
