# Article D654-46

Après une période de six mois minimum, si un contrôle approfondi donne des résultats satisfaisants et montre que les obligations réglementaires sont à nouveau remplies, l'agrément peut être rétabli à la demande de l'acheteur.
