# Article D654-67

Le prélèvement mentionné à l'article D. 654-39, est dû par tout producteur effectuant des ventes directes du lait ou d'autres produits laitiers, sur la quantité de lait ou d'équivalent-lait cédée en dépassement du quota individuel attribué par FranceAgriMer après répartition, le cas échéant, des quotas individuels inutilisés, en application de l'arrêté mentionné à l'article D. 654-40.
