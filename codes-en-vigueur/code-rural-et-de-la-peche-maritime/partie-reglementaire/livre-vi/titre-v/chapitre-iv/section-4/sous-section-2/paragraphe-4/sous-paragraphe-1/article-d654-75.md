# Article D654-75

Dans les cas de transferts régis par la sous-section 3 de la présente section, le directeur général de FranceAgriMer après avis du conseil spécialisé compétent, fixe la date limite de déclaration par le cessionnaire au préfet du département, qui donne droit à un ajustement des quotas individuels des producteurs concernés au cours de la campagne pendant laquelle ce transfert a eu lieu. Passé ce délai, la déclaration est prise en compte le 1er avril suivant la campagne pendant laquelle elle a été effectuée.

Une dérogation peut toutefois être accordée après cette date par le préfet en cas d'installation, de constitution de société ou de changement de forme sociétaire.
