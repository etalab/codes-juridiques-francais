# Article D654-66-1

I. - Le producteur s'assure auprès de la direction départementale des territoires ou de la direction départementale des territoires et de la mer dont il dépend que l'acheteur auquel il livre son lait est agréé.

II. - Le producteur effectuant des livraisons conserve un relevé des quantités de lait livrées à son acheteur. Il présente ce relevé aux autorités de contrôle, au cours de la campagne à laquelle il se rapporte et pendant les trois années civiles suivant la fin de celle-ci. Ce relevé peut consister en tout document permettant la mesure exhaustive des quantités de lait livrées à chaque ramassage.
