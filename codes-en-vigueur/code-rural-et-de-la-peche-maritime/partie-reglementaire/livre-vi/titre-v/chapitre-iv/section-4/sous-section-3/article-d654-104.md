# Article D654-104

Lorsque la cession ou l'apport porte sur une ou plusieurs parties d'une exploitation laitière, la quantité de référence correspondant à cette exploitation est répartie entre les producteurs, personnes physiques ou morales, qui reprennent les parcelles en cause, en fonction de leur superficie respective, à l'exclusion des bois, landes improductives, friches, étangs et cultures pérennes.

Il n'y a toutefois pas transfert lorsqu'il s'agit d'une première cession portant sur une superficie inférieure à 3 hectares ou lorsque le cédant peut établir que les terres cédées ont été prises dans le cadre d'un agrandissement sans quantité de référence laitière, à l'exception des terres acquises après application du deuxième alinéa de l'article D. 654-109.

Dans le cas de la transmission par héritage de la propriété d'une exploitation laitière, s'il y a accord établi par acte authentique entre les héritiers et, le cas échéant, le conjoint survivant, sur la répartition de la quantité de référence, le transfert est opéré suivant cet accord.
