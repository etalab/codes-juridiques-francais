# Paragraphe 2 : Dispositions communes au paiement du lait de vache, de brebis et de chèvre en fonction de sa composition et de sa qualité

- [Article D654-32](article-d654-32.md)
- [Article D654-33](article-d654-33.md)
- [Article D654-34](article-d654-34.md)
- [Article D654-35](article-d654-35.md)
- [Article D654-36](article-d654-36.md)
