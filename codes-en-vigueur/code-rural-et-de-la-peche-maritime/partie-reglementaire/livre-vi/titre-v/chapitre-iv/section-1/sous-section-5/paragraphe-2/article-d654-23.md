# Article D654-23

Les modalités de perception de la redevance sanitaire d'abattage et de découpage mentionnée à l'article L. 654-20 sont fixées par les articles 111 quater L à 111 quater R de l'annexe III du code général des impôts.
