# Section 8 : Conditions zootechniques et généalogiques applicables aux importations en provenance de pays tiers.

- [Article D653-106](article-d653-106.md)
- [Article D653-107](article-d653-107.md)
- [Article D653-108](article-d653-108.md)
- [Article D653-109](article-d653-109.md)
- [Article D653-110](article-d653-110.md)
- [Article D653-111](article-d653-111.md)
- [Article D653-112](article-d653-112.md)
- [Article D653-113](article-d653-113.md)
- [Article D653-114](article-d653-114.md)
