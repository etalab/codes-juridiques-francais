# Section 2 : Les systèmes nationaux d'information génétique

- [Article D653-6](article-d653-6.md)
- [Article D653-7](article-d653-7.md)
- [Article D653-8](article-d653-8.md)
