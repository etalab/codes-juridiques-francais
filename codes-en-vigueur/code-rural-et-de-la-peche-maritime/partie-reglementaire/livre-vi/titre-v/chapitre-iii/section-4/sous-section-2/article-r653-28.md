# Article R653-28

I. - Des comptables secondaires peuvent être désignés par le directeur général après avis de l'agent comptable et avec l'agrément du ministre chargé du budget.

II. - L'établissement est soumis aux dispositions des titres Ier et III du décret n° 2012-1246 du 7 novembre 2012 relatif à la gestion budgétaire et comptable publique. En particulier des régies d'avances et de recettes peuvent être ouvertes dans les conditions fixées par le décret du 20 juillet 1992 relatif aux régies de recettes et aux régies d'avances des organismes publics.

III. - L'établissement peut recourir à l'emprunt ou à des lignes de trésorerie, avec l'autorisation des ministres de tutelle et des ministres chargés de l'économie et du budget.

IV. - La comptabilité analytique est tenue par l'agent comptable ou sous son contrôle, selon un plan établi par le directeur général et approuvé par les ministres de tutelle et le ministre chargé du budget. Un état retraçant les résultats de la comptabilité analytique est joint au compte financier adressé aux ministres de tutelle.
