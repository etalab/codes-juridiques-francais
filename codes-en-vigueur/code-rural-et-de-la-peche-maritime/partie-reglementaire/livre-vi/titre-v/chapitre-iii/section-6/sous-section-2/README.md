# Sous-section 2 : Le service public d'enregistrement et de contrôle des performances des ruminants

- [Article R653-63](article-r653-63.md)
- [Article R653-64](article-r653-64.md)
- [Article R653-65](article-r653-65.md)
- [Article R653-66](article-r653-66.md)
- [Article R653-67](article-r653-67.md)
- [Article R653-68](article-r653-68.md)
- [Article R653-69](article-r653-69.md)
- [Article R653-70](article-r653-70.md)
- [Article R653-71](article-r653-71.md)
- [Article R653-72](article-r653-72.md)
- [Article R653-73](article-r653-73.md)
- [Article R653-74](article-r653-74.md)
