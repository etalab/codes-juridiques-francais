# Article D653-1

La Commission nationale d'amélioration génétique est consultée par le ministre chargé de l'agriculture dans les conditions prévues à l'article D. 653-2 et peut faire toutes propositions sur l'ensemble des questions concernant les méthodes et moyens d'amélioration de la qualité génétique du cheptel des espèces bovine, ovine, caprine, porcine, canine, féline, équine et asine, des lapins, des volailles et des espèces élevées dans des exploitations aquacoles.

La commission nationale comprend une commission générale et cinq comités consultatifs :

1° Le comité consultatif pour l'espèce bovine ;

2° Le comité consultatif pour les espèces ovine et caprine ;

3° Le comité consultatif pour les espèces porcine, les lapins, les volailles et les espèces élevées dans des exploitations aquacoles ;

4° Le comité consultatif pour les espèces canine et féline ;

5° Le comité consultatif pour les espèces équine et asine.

Le ministre chargé de l'agriculture peut créer, par arrêtés pris après consultation de la commission générale ou des comités consultatifs intéressés, des commissions permanentes composées de membres désignés selon le cas au sein de la commission générale ou des comités consultatifs, auxquelles lesdits commission ou comités peuvent déléguer certaines de leurs missions.

Le fonctionnement de la Commission nationale d'amélioration génétique est régi par les dispositions du décret n° 2006-672 du 8 juin 2006 relatif à la composition et au fonctionnement de commissions administratives à caractère consultatif.
