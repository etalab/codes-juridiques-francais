# Article R653-87-2

Les professionnels ressortissants d'un Etat membre de la Communauté européenne ou d'un Etat partie à l'accord sur l'Espace économique européen qui effectuent à titre temporaire et occasionnel des prestations de service sont réputés remplir les conditions définies à l'article R. 653-87 sous réserve d'être légalement établis dans un de ces Etats pour y exercer cette activité et, lorsque ni l'activité ni la formation y conduisant n'y sont réglementées, de l'avoir exercée, dans cet Etat, pendant au moins deux ans au cours des dix années qui précèdent la prestation.

Lorsqu'ils effectuent pour la première fois leur prestation en France, ils doivent en informer au préalable l'autorité administrative par une déclaration écrite.

La déclaration doit être adressée au centre d'évaluation habilité à la délivrance du certificat d'aptitude aux fonctions de technicien d'insémination dans les espèces bovine, caprine et ovine. Elle comporte les éléments prévus par l'article R. 204-1.

Lorsque la vérification des qualifications professionnelles de l'intéressé fait apparaître une différence substantielle entre ces qualifications et la formation exigée, de nature à nuire à la santé ou à la sécurité des bénéficiaires du service, l'intéressé est mis à même de démontrer qu'il a acquis les connaissances et compétences manquantes par une épreuve d'aptitude dont les modalités sont précisées par arrêté du ministre chargé de l'agriculture.
