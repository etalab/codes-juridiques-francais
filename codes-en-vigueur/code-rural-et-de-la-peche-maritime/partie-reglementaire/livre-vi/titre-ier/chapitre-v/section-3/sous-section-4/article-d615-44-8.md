# Article D615-44-8

Le plafond mentionné au b du 2 de l'article 111 du règlement (CE) n° 73/2009 du Conseil du 19 janvier 2009 susmentionné n'est pas appliqué pour l'octroi de la prime.
