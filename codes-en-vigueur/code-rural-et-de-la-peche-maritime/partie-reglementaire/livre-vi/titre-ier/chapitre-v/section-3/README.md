# Section 3 : Régimes de soutien aux productions animales

- [Sous-section 4 : Prime à la vache allaitante.](sous-section-4)
- [Sous-section 7 : Utilisation des droits à prime dans le secteur bovin.](sous-section-7)
- [Sous-section 8 : Transfert des droits à prime dans le secteur bovin.](sous-section-8)
- [Sous-section 9 : Soutiens spécifiques aux productions animales](sous-section-9)
