# Sous-section 6 : Détermination des superficies.

- [Article D615-11](article-d615-11.md)
- [Article D615-12](article-d615-12.md)
- [Article D615-12-1](article-d615-12-1.md)
- [Article D615-12-2](article-d615-12-2.md)
