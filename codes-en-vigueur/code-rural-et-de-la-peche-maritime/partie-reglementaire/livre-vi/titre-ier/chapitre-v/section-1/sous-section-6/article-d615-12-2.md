# Article D615-12-2

La liste des essences forestières convenant à l'usage de taillis à courte rotation mentionnée au n de l'article 2 du règlement (CE) n° 1120/2009 de la Commission du 29 octobre 2009 susmentionné et leur cycle maximal de récolte sont fixés par arrêté du ministre chargé de l'agriculture.
