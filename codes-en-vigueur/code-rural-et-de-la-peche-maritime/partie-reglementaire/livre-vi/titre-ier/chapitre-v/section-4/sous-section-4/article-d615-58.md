# Article D615-58

Lorsque, pour un ou plusieurs des domaines mentionnés à l'article D. 615-57, des cas de non-conformité sont constatés lors du contrôle du respect des bonnes conditions agricoles et environnementales mentionnées à la sous-section 2 et du respect des exigences réglementaires mentionnées au II de l'article D. 615-57, il est déterminé, pour chaque domaine, un pourcentage de réduction.

Lorsque, pour un domaine donné, plusieurs cas de non-conformité sont constatés, le pourcentage de réduction applicable correspond à celui des pourcentages affectés à ces cas dont la valeur est la plus élevée.

Toutefois, pour un même domaine de contrôle, lorsque tous les cas de non-conformité affectés du pourcentage le plus élevé et pertinents pour l'exploitation sont constatés, le pourcentage de réduction applicable à ce domaine est fixé à 5 %.
