# Article D615-61

Le directeur départemental de l'agriculture et de la forêt ou, dans les départements d'outre-mer, le directeur de l'agriculture et de la forêt recueille, sous l'autorité du préfet, les observations de l'agriculteur sur les cas de non-conformité constatés à l'occasion des contrôles effectués et sur le taux de réduction susceptible d'en résulter.

Il transmet aux organismes payeurs la liste des cas de non-conformité qui entraînent une réduction des paiements directs en application de la présente section, et le taux de cette réduction.
