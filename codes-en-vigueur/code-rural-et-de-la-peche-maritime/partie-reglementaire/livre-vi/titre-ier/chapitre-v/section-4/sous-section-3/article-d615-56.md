# Article D615-56

Le directeur départemental de l'agriculture et de la forêt ou, dans les départements d'outre-mer, le directeur de l'agriculture et de la forêt, calcule, sous l'autorité du préfet, la taille des échantillons de contrôles.

Il veille à la coordination dans le temps des contrôles effectués au titre de la présente section ainsi que de ceux réalisés au titre des réglementations visées à l'annexe II du règlement (CE) n° 73 / 2009 du 19 janvier 2009 susmentionné, de manière à ce que le nombre de missions de contrôle sur une même exploitation soit aussi limité que possible.

Il est régulièrement informé par les organismes de contrôle mentionnés à l'article D. 615-52 des exploitations contrôlées ou qu'ils envisagent de contrôler et, dans ce cas, des dates prévisionnelles de ces contrôles.

Il conserve une copie des rapports de ces contrôles ainsi que de toutes les informations relatives aux suites autres que celles liées à l'application des dispositions de la présente sous-section qui leur sont données par les organismes précités.
