# Article D615-46

I.-Les agriculteurs qui demandent les aides soumises aux règles de conditionnalité prévues par la politique agricole commune  et qui disposent de terres agricoles localisées à moins de cinq mètres de la bordure d'un des cours d'eau définis par arrêté du ministre chargé de l'agriculture sont tenus de conserver une bande tampon pérenne le long de ces cours d'eau, de sorte qu'une largeur de cinq mètres au minimum soit maintenue entre eux et la partie cultivée des terres agricoles susmentionnées.

L'utilisation de fertilisants minéraux ou organiques sur les surfaces consacrées à la bande tampon est interdite. Sauf dans les cas prévus par l'article L. 251-8, l'utilisation de traitements phytopharmaceutiques est également interdite sur ces surfaces.

II.-Un arrêté du ministre chargé de l'agriculture précise la liste des couverts autorisés, les éléments pris en compte pour la détermination de la largeur mentionnée au I et les conditions d'entretien des bandes tampons.
