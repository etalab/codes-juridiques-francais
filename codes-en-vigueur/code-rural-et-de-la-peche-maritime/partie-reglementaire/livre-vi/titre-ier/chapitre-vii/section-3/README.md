# Section 3 : Délivrance et contrôle de la certification environnementale

- [Sous-section 1 : Principes généraux](sous-section-1)
- [Sous-section 2 : Certification individuelle](sous-section-2)
- [Sous-section 3 : Certification gérée dans un cadre collectif](sous-section-3)
