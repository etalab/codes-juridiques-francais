# Article D617-2

Pour pouvoir demander une certification environnementale, l'exploitation agricole doit atteindre un premier niveau d'exigence environnementale. Ce niveau est regardé comme atteint dès lors que les conditions suivantes sont réunies :

1° L'exploitant a réalisé un bilan démontrant que son exploitation satisfait aux exigences relatives à l'environnement et à la santé des végétaux mentionnées à l'article 5 du règlement (CE) n° 73/2009 du Conseil du 19 janvier 2009 établissant des règles communes pour les régimes de soutien direct en faveur des agriculteurs dans le cadre de la politique agricole commune et établissant certains régimes de soutien en faveur des agriculteurs ainsi que, si elle y est soumise, aux bonnes conditions agricoles et environnementales définies aux articles D. 615-46 à D. 615-51.

Ce bilan a été vérifié par un organisme habilité dans le cadre du système de conseil agricole, conformément à l'article 12 du règlement (CE) n° 73/2009 du Conseil du 19 janvier 2009, qui en a attesté la pertinence en se fondant sur un entretien avec l'exploitant, sur ses connaissances de l'exploitation et des pratiques de cet exploitant et, le cas échéant, sur une visite de l'exploitation.

2° L'exploitant a réalisé une évaluation de l'exploitation au regard du référentiel de deuxième niveau mentionné à l'article D. 617-3 ou au regard des seuils de performance environnementale de troisième niveau mentionnés à l'article D. 617-4.
