# Article D611-5

I.-La Commission nationale technique est présidée par le directeur général de la performance économique et environnementale des entreprises au ministère de l'agriculture et de la pêche ou par son représentant.

II.-La Commission nationale technique comprend :

1° Au titre du Conseil supérieur d'orientation et de coordination de l'économie agricole et alimentaire :

a) Les membres mentionnés aux 2°, 3°, 6° et 7° du I de l'article R. 611-1 ;

b) Parmi les membres mentionnés au 1° du I de l'article R. 611-1, le représentant du ministre chargé de la concurrence ;

c) Deux représentants de la transformation des produits agricoles parmi les membres mentionnés au 9° du I de l'article R. 611-1 du code rural et de la pêche maritime.

2° Au titre des personnalités extérieures au Conseil supérieur d'orientation et de coordination de l'économie agricole et alimentaire :

a) Trois représentants des établissements mentionnés à l'article D. 621-1 ;

b) Un représentant de la coopération agricole ;

c) Quatre représentants des organisations spécialisées de producteurs de la filière des fruits et légumes ;

d) Quatre représentants des organisations spécialisées de producteurs des filières de productions animales ;

e) Deux représentants des organisations spécialisées de producteurs de la filière vitivinicole ;

f) Deux représentants des organisations spécialisées de producteurs de la filière forestière.

III.-Les membres de la Commission nationale technique autres que ceux mentionnés aux a et b du 1° du II sont nommés pour une durée de trois ans, renouvelable, par arrêté du ministre chargé de l'agriculture.
