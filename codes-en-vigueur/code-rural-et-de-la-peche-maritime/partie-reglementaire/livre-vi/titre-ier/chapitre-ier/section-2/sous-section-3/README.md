# Sous-section 3 : La commission technique spécialisée du "développement agricole et rural"

- [Article D611-14](article-d611-14.md)
- [Article D611-15](article-d611-15.md)
- [Article D611-16](article-d611-16.md)
- [Article D611-17](article-d611-17.md)
