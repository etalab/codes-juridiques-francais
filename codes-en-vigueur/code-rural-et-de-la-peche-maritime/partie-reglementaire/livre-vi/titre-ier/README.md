# Titre Ier : Dispositions générales

- [Chapitre Ier : Le Conseil supérieur d'orientation et de coordination de l'économie agricole et alimentaire](chapitre-ier)
- [Chapitre II : Les aides de l'Etat à l'orientation des productions.](chapitre-ii)
- [Chapitre IV : Le fonds de promotion des produits agricoles et alimentaires.](chapitre-iv)
- [Chapitre V : Régimes de soutien direct dans le cadre de la politique agricole commune](chapitre-v)
- [Chapitre VI : Prévention et gestion des situations de crise conjoncturelle.](chapitre-vi)
- [Chapitre VII : Certification environnementale des exploitations agricoles](chapitre-vii)
- [Chapitre VIII :  Assistance en matière de recouvrement international](chapitre-viii)
