# Sous-section 1 : Conditions de reconnaissance et de retrait de reconnaissance des organisations interprofessionnelles agricoles.

- [Article R632-1](article-r632-1.md)
- [Article R632-2](article-r632-2.md)
- [Article R632-3](article-r632-3.md)
- [Article R632-4](article-r632-4.md)
- [Article R632-4-1](article-r632-4-1.md)
