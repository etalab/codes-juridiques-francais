# Article D631-2

Le médiateur peut prendre toutes initiatives de nature à favoriser la conciliation des positions des parties aux contrats mentionnés à l'article L. 631-24.

Il peut, avec l'accord des parties, confier aux personnes qui l'assistent le traitement de demandes de médiation.

La médiation est conduite par le médiateur ou les personnes qui l'assistent selon les règles applicables aux médiations conventionnelles.
