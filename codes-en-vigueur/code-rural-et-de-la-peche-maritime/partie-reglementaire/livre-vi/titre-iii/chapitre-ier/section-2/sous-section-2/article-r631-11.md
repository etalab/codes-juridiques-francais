# Article R631-11

On entend par fruits et légumes, au sens de la présente sous-section, les produits mentionnés à la partie IX de l'annexe I au règlement (CE) n° 1234/2007 du Conseil du 22 octobre 2007 portant organisation commune des marchés dans le secteur agricole et dispositions spécifiques en ce qui concerne certains produits de ce secteur (règlement OCM unique).

On entend par producteur toute personne qui exerce une activité agricole au sens de l'article L. 311-1 et qui vend les fruits ou les légumes qu'elle a produits dans le cadre de cette activité.
