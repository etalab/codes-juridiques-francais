# Section 2 : Les contrats de vente de produits agricoles.

- [Sous-section 1 : Les contrats de vente de lait de vache.](sous-section-1)
- [Sous-Section 2 : Les contrats de vente de fruits et légumes frais](sous-section-2)
