# Article R671-3

Est puni des peines prévues pour les contraventions de la troisième classe, le fait pour tout producteur, transformateur ou responsable de la mise sur le marché d'une denrée alimentaire ou d'un produit agricole mentionné à l'article R. 641-32, d'utiliser le terme " montagne " en l'absence de l'autorisation prévue à l'article R. 641-35 ou en méconnaissance des prescriptions du cahier des charges mentionné au 3° de l'article R. 641-37 ou des dispositions prévues au 5° de l'article R. 641-38.
