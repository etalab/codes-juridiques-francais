# Article R622-46

Des agents placés sous l'autorité du ministre chargé de l'économie contrôlent la réalité et la régularité des opérations faisant directement ou indirectement partie du système de financement par les fonds européens de financement de la politique agricole commune. Ils sont assermentés à cet effet dans les conditions prévues à l'article R. 622-47.

Ce contrôle peut porter sur toutes opérations pour lesquelles la poursuite d'éventuelles irrégularités n'est pas prescrite en application des dispositions de l'article 3 du règlement (CE) du Conseil du 18 décembre 1995 susvisé.

Il s'exerce auprès des bénéficiaires et des redevables des fonds communautaires. Des justifications peuvent être demandées à toute personne détenant des informations utiles au contrôle.
