# Article D681-4-2

Les agriculteurs qui demandent les aides mentionnées à l'article D. 615-45 sont tenus de maintenir les particularités topographiques des surfaces agricoles de leur exploitation, qui constituent des éléments pérennes du paysage. Cette obligation ne s'applique pas aux agriculteurs dont la surface agricole utile est inférieure à quinze hectares.

La surface totale de ces particularités topographiques, convertie en " surface équivalente topographique " (SET), doit être au moins égale au pourcentage de la surface agricole utile (SAU) de l'exploitation déterminé en application de l'article D. 615-50-1.

Un arrêté préfectoral fixe, conformément aux dispositions de l'arrêté du ministre chargé de l'agriculture pris en application de l'article D. 615-50-1, la liste des particularités topographiques qui peuvent être retenues ainsi que la " surface équivalente topographique " (SET) correspondant à chacune d'elles. Il fixe également les règles d'entretien des particularités topographiques.

Le préfet peut compléter ou adapter la liste des particularités topographiques, la SET correspondant à chacune d'elles ainsi que leurs règles d'entretien, après approbation du ministre chargé de l'agriculture.
