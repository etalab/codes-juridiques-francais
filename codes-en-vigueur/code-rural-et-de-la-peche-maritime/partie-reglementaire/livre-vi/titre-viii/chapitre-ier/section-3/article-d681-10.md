# Article D681-10

<div align="left">Les dispositions du chapitre VII du titre Ier, l'article R. 651-1 et les articles D. 654-29 à D. 654-114-7 ne sont pas applicables à Mayotte.<br/>
</div>
