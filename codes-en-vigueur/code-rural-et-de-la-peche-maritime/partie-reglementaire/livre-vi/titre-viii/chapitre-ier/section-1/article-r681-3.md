# Article R681-3

Pour les opérations d'identification des ovins et des caprins mentionnées à l'article D. 212-27, des délais spécifiques peuvent être définis en Guadeloupe, en Guyane, à la Martinique, à La Réunion ou à Saint-Martin par arrêté conjoint des ministres chargés de l'agriculture et de l'outre-mer.
