# Chapitre IV : L'Office de développement de l'économie agricole dans les départements d'outre-mer

- [Section 1 : Missions.](section-1)
- [Section 2 : Conseil d'administration et comités.](section-2)
- [Section 3 : Direction.](section-3)
- [Section 4 : Régime financier et comptable.](section-4)
