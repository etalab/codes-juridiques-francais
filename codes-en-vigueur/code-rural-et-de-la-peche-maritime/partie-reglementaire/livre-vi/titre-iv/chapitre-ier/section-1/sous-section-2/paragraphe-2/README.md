# Paragraphe 2 : Dispositions applicables à la procédure d'opposition aux demandes émanant d'un autre Etat membre de la Communauté européenne ou d'un pays tiers.

- [Article R641-22](article-r641-22.md)
- [Article R641-23](article-r641-23.md)
- [Article R641-24](article-r641-24.md)
- [Article R641-25](article-r641-25.md)
