# Article R641-42

En cas d'infraction aux dispositions du présent chapitre ou aux mesures prises pour son application, le préfet de région peut retirer l'autorisation d'utiliser la dénomination "montagne" après avoir mis le titulaire de l'autorisation à même de présenter ses observations dans un délai d'un mois.
