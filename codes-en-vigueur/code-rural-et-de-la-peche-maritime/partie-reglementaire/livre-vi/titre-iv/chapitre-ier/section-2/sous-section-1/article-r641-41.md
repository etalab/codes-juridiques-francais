# Article R641-41

L'autorisation d'exploiter une source d'eau minérale naturelle accordée en application de l'article L. 1322-1 du code de la santé publique, si elle prévoit l'utilisation de la dénomination "montagne", tient lieu de l'autorisation prévue par le présent chapitre.
