# Article R641-53

En cas de non-respect des conditions fixées pour l'utilisation des termes "produits pays", le préfet de région peut mettre le titulaire de l'autorisation en demeure de procéder, dans un délai qu'il fixe, à des actions correctives.

Lorsqu'à l'expiration du délai imparti, il est constaté que la mise en demeure est restée sans effet ou n'a été que partiellement prise en compte, le préfet de région peut, dans les formes prévues à l'article R. 641-54, suspendre, pour une durée qu'il détermine et qui ne peut excéder douze mois, l'autorisation d'utiliser les termes "produits pays". A l'issue de cette période, le préfet de région met fin à la suspension s'il a été remédié aux irrégularités constatées, ou procède au retrait de l'autorisation dans les formes prévues à l'article R. 641-54.

En cas d'urgence ou si les faits constatés sont d'une gravité suffisante, le préfet de région peut, sans mise en demeure préalable, prononcer la suspension de l'autorisation ou procéder à son retrait dans les formes prévues à l'article R. 641-54.
