# Section 2 : Dispositions relatives au contrôle des vins, eaux-de-vie et autres boissons alcoolisées bénéficiant d'une appellation d'origine.

- [Sous-section 1 : Dispositions communes.](sous-section-1)
- [Sous-section 2 : Dispositions applicables aux vins à appellation d'origine contrôlée.](sous-section-2)
- [Sous-section 3 : Dispositions applicables aux eaux-de-vie bénéficiant d'une appellation d'origine contrôlée.](sous-section-3)
- [Sous-section 4 : Dispositions applicables aux produits cidricoles bénéficiant d'une appellation d'origine contrôlée.](sous-section-4)
