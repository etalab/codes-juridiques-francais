# Section 3 : Dispositions applicables aux appellations d'origine laitières, agroalimentaires et forestières.

- [Article D644-16](article-d644-16.md)
- [Article D644-17](article-d644-17.md)
- [Article D644-18](article-d644-18.md)
