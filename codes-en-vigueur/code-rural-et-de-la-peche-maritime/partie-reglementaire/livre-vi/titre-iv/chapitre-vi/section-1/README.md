# Section 1 : Dispositions relatives au contrôle

- [Article D646-2](article-d646-2.md)
- [Article D646-3](article-d646-3.md)
- [Article D646-4](article-d646-4.md)
- [Article D646-5](article-d646-5.md)
- [Article D646-6](article-d646-6.md)
- [Article D646-7](article-d646-7.md)
- [Article D646-8](article-d646-8.md)
- [Article D646-9](article-d646-9.md)
- [Article D646-10](article-d646-10.md)
