# Section 1 : Protection des aires d'appellations d'origine et d'indications géographiques protégées

- [Article R643-1](article-r643-1.md)
- [Article R643-2](article-r643-2.md)
