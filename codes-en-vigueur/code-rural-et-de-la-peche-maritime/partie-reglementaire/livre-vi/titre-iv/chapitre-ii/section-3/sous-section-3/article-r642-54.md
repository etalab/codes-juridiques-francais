# Article R642-54

L'organisme certificateur élabore pour chaque cahier des charges, en concertation avec l'organisme de défense et de gestion intéressé, le plan de contrôle prévu à l'article R. 642-39, qu'il transmet à l'Institut national de l'origine et de la qualité avec l'avis de cet organisme.

Le plan de contrôle approuvé par le conseil chargé des agréments et contrôles est adressé par l'organisme certificateur à l'organisme de défense et de gestion qui le communique aux opérateurs.

Les contrôles sont réalisés sur la base du plan de contrôle approuvé.
