# Article R642-57

L'Institut national de l'origine et de la qualité confie à des organismes d'inspection les tâches de contrôle du respect du cahier des charges des produits bénéficiant d'une appellation d'origine ou des vins bénéficiant d'une indication géographique protégée au sens du règlement (CE) n° 1234 / 2007 du Conseil du 22 octobre 2007 portant organisation commune des marchés dans le secteur agricole et dispositions spécifiques en ce qui concerne certains produits de ce secteur (règlement " OCM unique "), si ce contrôle n'est pas effectué par un organisme certificateur.
