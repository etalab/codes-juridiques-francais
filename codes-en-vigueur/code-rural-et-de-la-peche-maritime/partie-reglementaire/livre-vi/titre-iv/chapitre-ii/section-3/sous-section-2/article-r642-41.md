# Article R642-41

Un organisme de contrôle doit obtenir l'agrément de l'Institut national de l'origine et de la qualité lorsqu'il entend exercer :

- soit une activité de certification de produits bénéficiant d'un label rouge, d'une appellation d'origine, d'une spécialité traditionnelle garantie ou d'une indication géographique protégée ainsi que la certification du mode de production biologique ;

- soit une activité d'inspection des produits bénéficiant d'une appellation d'origine ou des vins bénéficiant d'une indication géographique protégée ayant opté pour ce mode de contrôle.
