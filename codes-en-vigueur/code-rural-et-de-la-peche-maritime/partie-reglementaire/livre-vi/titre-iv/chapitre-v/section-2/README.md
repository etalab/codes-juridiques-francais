# Section 2 : Dispositions générales applicables aux eaux-de-vie.

- [Sous-section 1](sous-section-1)
- [Sous-section 2 : Dispositions applicables aux eaux-de-vie de vin.](sous-section-2)
