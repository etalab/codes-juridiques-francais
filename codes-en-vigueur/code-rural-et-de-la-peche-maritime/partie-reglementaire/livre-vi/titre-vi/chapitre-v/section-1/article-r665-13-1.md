# Article R665-13-1

Un arrêté conjoint des ministres chargés de l'agriculture et du budget précise les modalités de dépôt des dossiers de demandes d'autorisation mentionnées aux articles R. 665-7 à R. 665-13 ainsi que les modalités de publication et de notification des décisions correspondantes.
