# Article D664-1

I.-La Commission nationale des fonds opérationnels peut, à la demande du ministre de l'agriculture, émettre des avis sur les actions transnationales ou interprofessionnelles prévues dans les programmes opérationnels des organisations de producteurs ou, plus généralement, sur les orientations générales des programmes opérationnels et des fonds opérationnels.

II.-La commission comprend :

1° Le directeur général de la performance économique et environnementale des entreprises ou son représentant ;

2° Un représentant de l'assemblée permanente des chambres d'agriculture nommé sur proposition de cette assemblée ;

3° Un représentant de l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer) et un représentant de l'Office de développement de l'économie agricole d'outre-mer ;

4° Un représentant du centre technique interprofessionnel des fruits et légumes ;

5° Un représentant de chacune des organisations syndicales d'exploitants agricoles à vocation générale habilitées en application de l'article 3 du décret n° 90-187 du 28 février 1990 relatif à la représentation des organisations syndicales d'exploitants agricoles au sein de certains organismes ou commissions, nommé sur proposition de l'organisation intéressée ;

6° Un représentant de la coopération agricole ;

7° Dix représentants des organisations spécialisées de producteurs de la filière des fruits et légumes.

Les membres de la commission sont nommés par arrêté du ministre chargé de l'agriculture. A l'exception du représentant de l'Etat, nommé sans condition de durée, leur mandat est de cinq ans.

III.-La commission est présidée par le directeur général de la performance économique et environnementale des entreprises ou son représentant. Le secrétariat de la commission est assuré par l'établissement créé en application de l'article L. 621-1, compétent en matière de fruits et légumes.
