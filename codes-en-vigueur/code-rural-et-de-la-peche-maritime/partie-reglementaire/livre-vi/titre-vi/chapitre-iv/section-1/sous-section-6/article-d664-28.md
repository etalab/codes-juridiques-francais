# Article D664-28

Pour l'application de l'article 99 du règlement d'exécution (UE) n° 543/2011 de la Commission du 7 juin 2011, un arrêté du ministre chargé de l'agriculture fixe les modalités du système d'identification unique, conforme au système national d'identification et du répertoire des entreprises et de leurs établissements prévu aux articles R. 123-220 à R. 123-234 du code de commerce, appliqué pour toutes les demandes d'aide présentées par une même organisation de producteurs ou une association d'organisations de producteurs.
