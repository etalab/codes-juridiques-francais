# Article D664-26

Les organisations de producteurs notifient à FranceAgriMer les opérations de non-récolte et de récolte en vert définies à l'article 84 du règlement d'exécution (UE) n° 543/2011 susmentionné, dans les conditions définies à l'article 85 du même règlement.

Après chaque opération, elles renseignent un certificat de récolte en vert et de non-récolte.

Un arrêté du ministre de l'agriculture fixe la liste des produits concernés, le montant des paiements à l'hectare versés en compensation, ainsi que le contenu et les modalités de notification et d'envoi des certificat de récolte en vert et de non-récolte.
