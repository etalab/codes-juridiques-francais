# Article D664-5

Le directeur général de FranceAgriMer instruit les demandes d'approbation des programmes opérationnels en s'assurant qu'ils comportent l'ensemble des éléments mentionnés à l'article 59 du règlement d'exécution (UE) n° 543/2011  susmentionné. Il vérifie en particulier la régularité des modalités de calcul de la valeur de la production commercialisée retenue par l'organisation de producteurs ainsi que la conformité des objectifs et mesures figurant dans le programme avec la stratégie nationale.

Le directeur général de FranceAgriMer approuve ou rejette tout ou partie du programme opérationnel dans les délais et selon les modalités mentionnées à l'article 64 du règlement d'exécution (UE) n° 543/2011 susmentionné. Il peut approuver un programme opérationnel sous réserve de l'acceptation par l'organisation de producteurs de certaines modifications.

Lorsque le programme opérationnel prévoit la réalisation d'actions dans un autre Etat membre, ou la réalisation d'actions à caractère interprofessionnel, le directeur général de FranceAgriMer soumet le programme à l'avis du ministre chargé de l'agriculture qui consulte, le cas échéant, la Commission nationale des fonds opérationnels mentionnée à l'article D. 664-1.
