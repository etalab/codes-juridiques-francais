# Article D664-21

I.-En application du 4 de l'article 103 quinquies du règlement (CE) n° 1234 / 2007 susmentionné, l'aide financière communautaire peut atteindre 100 % du montant des dépenses réelles effectuées dans le cas de retrait du marché de fruits et légumes délivrés gratuitement aux organismes suivants :

1° Œuvres de bienfaisance ou fondations charitables ;

2° Institutions pénitentiaires, écoles et établissements d'enseignement public, colonies de vacances, hôpitaux et hospices de personnes âgées.

II.-Les organismes mentionnés au I qui souhaitent bénéficier gratuitement des fruits et légumes retirés du marché doivent demander au préalable l'agrément du directeur général de FranceAgriMer. Celui-ci s'assure que l'organisme demandeur est en mesure de satisfaire aux obligations mentionnées au 1 de l'article 83 du règlement d'exécution (UE) n° 543/2011 susmentionné.

Un arrêté du ministre chargé de l'agriculture fixe la liste des pièces justificatives qui doivent être jointes à la demande d'agrément, ainsi que la date limite de dépôt de celle-ci.

III.-Les organismes mentionnés au 1° du I sont agréés pour assurer la distribution gratuite des produits, selon les cas, sur le territoire national, dans l'ensemble de l'Union européenne, ou dans les pays tiers. Avant de les distribuer, ils peuvent faire transformer les produits retirés qui leur ont été délivrés. Dans ce cas, ces organismes sont autorisés à demander une contribution symbolique aux bénéficiaires finaux des produits concernés.

IV.-Lorsque l'organisme mentionné au I. souhaite transférer les produits qui lui ont été fournis à d'autres organismes de même nature, la liste complète de ces organismes est annexée à la décision d'agrément. Dans ce cas, les organismes destinataires des produits sont eux-mêmes soumis aux obligations définies au 1 de l'article 83 du règlement d'exécution (UE) n° 543/2011 susmentionné.
