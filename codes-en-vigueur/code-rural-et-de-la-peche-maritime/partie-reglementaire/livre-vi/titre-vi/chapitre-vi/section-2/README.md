# Section 2 : L'aval.

- [Article D666-10](article-d666-10.md)
- [Article D666-11](article-d666-11.md)
- [Article D666-12](article-d666-12.md)
- [Article D666-13](article-d666-13.md)
- [Article D666-14](article-d666-14.md)
