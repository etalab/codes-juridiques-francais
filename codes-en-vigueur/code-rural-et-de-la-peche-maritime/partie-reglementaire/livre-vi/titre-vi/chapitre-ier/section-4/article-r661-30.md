# Article R661-30

I. - La production et la distribution des matériels de multiplication de base ne peuvent être conduites que par des établissements spécialisés dits :

1° Etablissements de sélection pour les matériels de base initiaux nécessaires à la prémultiplication ;

2° Etablissements de prémultiplication pour les matériels de base nécessaires à la plantation des vignes-mères de porte-greffe ou de greffons destinées à la production des matériels certifiés.

II. - Ces établissements spécialisés doivent être agréés par le ministre chargé de l'agriculture et être titulaires de la carte de contrôle prévue à l'article 29 du décret n° 53-977 du 30 septembre 1953 relatif à l'organisation et à l'assainissement du marché du vin et à l'orientation de la production viticole.

III. - Un arrêté du ministre chargé de l'agriculture définit les conditions et la procédure de l'agrément ainsi que les règles de fonctionnement des établissements.

IV. - L'agrément peut être retiré lorsque les conditions nécessaires ne se trouvent plus remplies ou en cas de manquement grave aux prescriptions de la présente section et des arrêtés pris pour son application.
