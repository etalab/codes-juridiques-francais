# Article R661-31

Les matériels commercialisés doivent être accompagnés d'un bulletin de transport extrait d'un carnet numéroté.

Les carnets de bulletins de transport sont délivrés par l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer).

Ce bulletin doit être présenté à toute réquisition par le transporteur ou le détenteur des produits mentionnés. Il tient lieu, pour les viticulteurs procédant à des plantations de vigne, de l'attestation prévue à l'article R. 665-16.

Les entreprises de production et de distribution sont tenues de présenter à toute réquisition des agents habilités au contrôle, les souches des carnets utilisés ou en cours d'utilisation.

Ces mêmes entreprises sont astreintes à tenir une comptabilité matière séparée, pour chaque catégorie de matériels précisant, pour toute entrée ou sortie, l'origine, la quantité, la nature des marchandises et les dates de l'opération.

Ces comptabilités matières, ainsi que les pièces justificatives de l'origine des matériels, doivent être présentées à toute réquisition des agents habilités au contrôle.
