# Section 2 : La création de zones protégées pour la production de semences ou plants.

- [Article R661-12](article-r661-12.md)
- [Article R661-13](article-r661-13.md)
- [Article R661-14](article-r661-14.md)
- [Article R661-15](article-r661-15.md)
- [Article R661-16](article-r661-16.md)
- [Article R661-17](article-r661-17.md)
- [Article R661-18](article-r661-18.md)
- [Article R661-19](article-r661-19.md)
- [Article R661-20](article-r661-20.md)
- [Article R661-21](article-r661-21.md)
- [Article R661-22](article-r661-22.md)
- [Article R661-23](article-r661-23.md)
