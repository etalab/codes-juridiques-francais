# Sous-section 6 : Modalités d'organisation et de tenue des élections aux comités régionaux, départementaux ou interdépartementaux des pêches maritimes et des élevages marins

- [Paragraphe 1 : Dispositions communes](paragraphe-1)
- [Paragraphe 2 : Listes électorales](paragraphe-2)
- [Paragraphe 3 : Conditions d'éligibilité et déclarations de candidature](paragraphe-3)
- [Paragraphe 4 : Préparation et déroulement des opérations électorales](paragraphe-4)
- [Paragraphe 5 : Elections partielles](paragraphe-5)
- [Paragraphe 6 : Contentieux](paragraphe-6)
