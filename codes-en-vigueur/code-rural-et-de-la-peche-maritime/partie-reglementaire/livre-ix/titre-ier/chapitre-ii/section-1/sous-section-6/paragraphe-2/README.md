# Paragraphe 2 : Listes électorales

- [Article R912-71](article-r912-71.md)
- [Article R912-72](article-r912-72.md)
- [Article R912-73](article-r912-73.md)
- [Article R912-74](article-r912-74.md)
- [Article R912-75](article-r912-75.md)
- [Article R912-76](article-r912-76.md)
- [Article R912-77](article-r912-77.md)
- [Article R912-78](article-r912-78.md)
- [Article R912-79](article-r912-79.md)
