# Article R912-67

La présente section est applicable à l'élection des membres des conseils des comités départementaux, interdépartementaux et régionaux des pêches maritimes et des élevages marins, représentant :

1° Les équipages et salariés des entreprises de pêche maritime et d'élevage marin, composant le premier collège ;

2° Les chefs d'entreprise de pêche maritime et d'élevage marin, composant le deuxième collège, divisé en quatre catégories regroupant respectivement les chefs d'entreprise de pêche maritime embarqués, les chefs d'entreprise de pêche maritime non embarqués armant un ou plusieurs navires titulaires d'un rôle d'équipage de pêche, les chefs d'entreprise de pêche maritime à pied et les chefs d'entreprise d'élevage marin.
