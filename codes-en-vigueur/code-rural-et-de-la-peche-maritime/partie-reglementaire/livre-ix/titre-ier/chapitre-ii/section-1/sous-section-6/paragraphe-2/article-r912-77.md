# Article R912-77

Sont électeurs dans le collège des chefs d'entreprise de pêche maritime et d'élevage marin et dans leurs catégories respectives en vue de l'élection des membres des conseils des comités départementaux, interdépartementaux et régionaux des pêches maritimes et des élevages marins :

1° Les chefs d'entreprise de pêche maritime embarqués, armant un ou plusieurs navires titulaires d'un rôle d'équipage de pêche, ayant accompli au moins trois mois d'embarquement à la pêche au cours des douze mois précédant la date mentionnée à l'article R. 912-75 ;

2° Les chefs d'entreprise de pêche maritime non embarqués, armant un ou plusieurs navires titulaires d'un rôle d'équipage de pêche ;

3° Les chefs d'entreprise d'élevage marin ;

4° Les chefs d'entreprise de pêche maritime à pied.

Aux fins du présent article, les chefs d'entreprise sont les chefs d'une entreprise de pêche ou d'élevage marin immatriculée au registre du commerce et des sociétés ainsi que les personnes pratiquant individuellement leur activité sur des navires d'une longueur inférieure ou égale à douze mètres ou effectuant habituellement des sorties de moins de vingt-quatre heures.
