# Article R912-84

Pour être inscrits sur la liste des candidats mentionnée à l'article R. 912-82, les personnes mentionnées à l'article R. 912-83 effectuent une demande d'inscription sur la liste des candidats auprès de la commission électorale. Le demandeur précise :

1° Ses nom et prénoms ;

2° Ses date et lieu de naissance ;

3° Son adresse ;

4° Le collège, et éventuellement la catégorie, au titre duquel il demande son inscription.

Cette demande est accompagnée des pièces justificatives nécessaires à son examen.

L'éligibilité d'un candidat est limitée au collège auquel il appartient ou au titre duquel il a demandé son inscription sur la liste des candidats et, dans le cas du collège des chefs d'entreprise, à la catégorie dans laquelle le candidat exerce son droit de vote ou, à défaut, au titre de laquelle il a demandé son inscription sur la liste des candidats.
