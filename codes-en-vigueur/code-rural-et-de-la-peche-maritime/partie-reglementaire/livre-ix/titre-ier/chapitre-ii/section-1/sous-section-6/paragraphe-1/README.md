# Paragraphe 1 : Dispositions communes

- [Article R912-67](article-r912-67.md)
- [Article R912-68](article-r912-68.md)
- [Article R912-69](article-r912-69.md)
- [Article R912-70](article-r912-70.md)
