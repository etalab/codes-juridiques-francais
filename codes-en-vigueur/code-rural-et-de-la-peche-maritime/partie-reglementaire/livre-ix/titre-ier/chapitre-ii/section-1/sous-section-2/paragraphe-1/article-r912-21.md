# Article R912-21

Le comité régional des pêches maritimes et des élevages marins peut recruter et rémunérer des gardes-jurés chargés de veiller au respect de l'application des réglementations en matière de gestion des ressources halieutiques dans les conditions prévues par l'article L. 942-2.
