# Sous-section 2 : Comités régionaux des pêches maritimes et des élevages marins

- [Paragraphe 1 : Compétence géographique et missions](paragraphe-1)
- [Paragraphe 2 : Composition et organisation du conseil et du bureau](paragraphe-2)
- [Paragraphe 3 : Fonctionnement du conseil et du bureau](paragraphe-3)
- [Paragraphe 4 : Compétences du président](paragraphe-4)
