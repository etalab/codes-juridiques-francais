# Paragraphe 1 : Compétence géographique et missions

- [Article R912-18](article-r912-18.md)
- [Article R912-19](article-r912-19.md)
- [Article R912-20](article-r912-20.md)
- [Article R912-21](article-r912-21.md)
