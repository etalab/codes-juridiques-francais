# Section 3 : Organisations de producteurs

- [Sous-section 1 : Reconnaissance et contrôle](sous-section-1)
- [Sous-section 2 : Extension de certaines règles des organisations de producteurs](sous-section-2)
