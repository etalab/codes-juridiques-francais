# Sous-section 4 : Modalités d'organisation et de tenue des élections aux comités régionaux de la conchyliculture

- [Paragraphe 1 : Dispositions générales](paragraphe-1)
- [Paragraphe 2 : Listes électorales](paragraphe-2)
- [Paragraphe 3 : Conditions d'éligibilité et déclarations de candidature](paragraphe-3)
- [Paragraphe 4 : Déroulement des opérations électorales](paragraphe-4)
