# Article R921-18

Outre les cas résultant de l'application de l'article L. 946-1, la licence de pêche européenne est suspendue par l'autorité qui l'a délivrée dans les cas suivants :

1° Lorsque le navire est soumis à un arrêt temporaire d'activité en application de la réglementation européenne ou nationale ;

2° Lorsque le navire ne dispose pas d'un permis de navigation valide pour l'activité de pêche professionnelle, jusqu'à l'obtention éventuelle d'un permis de navigation ;

3° Lorsque le navire ne respecte pas les limites de capacité déclarées dans le fichier des navires de pêche ou figurant dans le permis de mise en exploitation ;

4° Lorsque le navire ne respecte pas les conditions d'activité minimale définie par l'article R. 921-7.

La suspension d'activité de la capacité de pêche correspondante est immédiatement déclarée dans le fichier de la flotte de pêche européenne par l'autorité mentionnée au premier alinéa.
