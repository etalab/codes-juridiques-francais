# Article R921-7

Les navires immatriculés ou destinés à être immatriculés en France métropolitaine ou dans une collectivité territoriale d'outre-mer ayant le statut de région ultrapériphérique de l'Union européenne, et armés ou devant être armés à la pêche professionnelle, sont soumis à l'obligation de disposer d'un permis de mise en exploitation, délivré dans les conditions fixées par la présente sous-section.

Ce permis est exigé avant :

1° La construction ;

2° L'importation ;

3° L'armement à la pêche d'un navire antérieurement affecté à une autre activité ;

4° La modification de la capacité par augmentation de la jauge ou de la puissance du navire ;

5° Le réarmement à la pêche d'un navire qui a cessé d'être actif ;

6° Le passage d'un navire d'un segment à un autre, au sens de la réglementation européenne.

Est considéré comme actif au sens du 5°, un navire dont l'effectif porté au rôle, pendant une période de six mois au moins, correspond à celui prévu pour son exploitation et dont l'activité de pêche est attestée par le débarquement régulier de ressources halieutiques et par la remise régulière des documents statistiques correspondants prévue par la réglementation en vigueur. Cette période peut être portée à neuf mois pour les navires exerçant une activité de pêche saisonnière.

Est considéré comme inactif au sens du 5°, un navire qui ne remplit pas au moins un des critères mentionnés à l'alinéa précédent.
