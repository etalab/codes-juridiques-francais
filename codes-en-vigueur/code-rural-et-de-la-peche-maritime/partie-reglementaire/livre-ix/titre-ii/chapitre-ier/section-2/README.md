# Section 2 : Gestion de la flotte de pêche et accès aux ressources

- [Sous-section 1 : Permis de mise en exploitation des navires de pêche](sous-section-1)
- [Sous-section 2 : Licence de pêche européenne](sous-section-2)
