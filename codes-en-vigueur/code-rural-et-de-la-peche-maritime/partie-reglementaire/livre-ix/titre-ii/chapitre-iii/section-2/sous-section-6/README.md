# Sous-section 6 : Modification, suspension, retrait et vacance des concessions

- [Article R923-40](article-r923-40.md)
- [Article R923-41](article-r923-41.md)
- [Article R923-42](article-r923-42.md)
- [Article R923-43](article-r923-43.md)
- [Article R923-44](article-r923-44.md)
