# Sous-section 2 : Schémas des structures des exploitations de cultures marines

- [Article D923-6](article-d923-6.md)
- [Article D923-7](article-d923-7.md)
- [Article D923-8](article-d923-8.md)
