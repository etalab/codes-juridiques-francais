# Section 1 : Dispositions spécifiques aux Terres australes et antarctiques françaises

- [Sous-section 1 : Dispositions générales](sous-section-1)
- [Sous-section 2 : Dispositions particulières aux Terres australes](sous-section-2)
- [Sous-section 3 : Dispositions particulières aux îles Eparses](sous-section-3)
- [Sous-section 4 : Dispositions particulières aux navires battant pavillon d'un Etat étranger](sous-section-4)
