# Article R202-4

Les laboratoires nationaux de référence désignés pour la première fois dans un domaine de compétence donné disposent d'un délai de vingt-quatre mois pour obtenir les accréditations nécessaires à l'exercice de leurs missions.
