# Chapitre préliminaire : Comités consultatifs d'orientation de la politique sanitaire animale et végétale

- [Section 1 : Conseil national d'orientation de la politique sanitaire animale et végétale](section-1)
- [Section 2 : Conseils régionaux d'orientation 
de la politique sanitaire animale et végétale](section-2)
