# Article D200-2

Le Conseil national d'orientation de la politique sanitaire animale et végétale, placé auprès du ministre chargé de l'agriculture, est consulté sur :

― la liste des dangers sanitaires de première et deuxième catégorie ;

― les programmes collectifs volontaires de prévention, de surveillance et de lutte contre certains dangers sanitaires soumis à approbation dans un objectif de cohérence nationale ;

― les dispositions du code de déontologie vétérinaire ;

― la liste des programmes collectifs volontaires approuvés pour lesquels l'adhésion est une condition préalable à une qualification sanitaire ou à une certification sanitaire en vue des échanges et des exportations vers les pays tiers ;

― la liste des dangers sanitaires de deuxième catégorie donnant lieu à transmission d'informations en application du quatrième alinéa de l'article L. 201-7 ;

― le plan national d'intervention sanitaire d'urgence en santé animale et végétale.

Il est consulté sur les orientations en matière de politique sanitaire animale et végétale. Il peut être consulté sur les projets de mesure réglementaire en matière de protection et de santé des animaux et des végétaux ou sur toute autre question relative à la santé et à la protection des animaux et des végétaux.
