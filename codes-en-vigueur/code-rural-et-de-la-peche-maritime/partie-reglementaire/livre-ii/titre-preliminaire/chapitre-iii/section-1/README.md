# Section 1 : Le vétérinaire sanitaire

- [Sous-section 1 : Désignation](sous-section-1)
- [Sous-section 2 : Conditions de délivrance et portée de l'habilitation](sous-section-2)
- [Sous-section 3 : Conditions d'exercice de leurs missions par les vétérinaires sanitaires](sous-section-3)
- [Sous-section 4 : Suspension et retrait de l'habilitation](sous-section-4)
