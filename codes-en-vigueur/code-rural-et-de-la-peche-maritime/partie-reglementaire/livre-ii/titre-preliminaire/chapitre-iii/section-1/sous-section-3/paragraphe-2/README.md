# Paragraphe 2 : Conditions de remplacement ou d'assistance des vétérinaires sanitaires

- [Article R203-9](article-r203-9.md)
- [Article R203-10](article-r203-10.md)
