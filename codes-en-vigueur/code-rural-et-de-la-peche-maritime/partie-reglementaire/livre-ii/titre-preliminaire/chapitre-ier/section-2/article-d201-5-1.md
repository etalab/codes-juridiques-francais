# Article D201-5-1

Le ministre chargé de l'agriculture arrête le plan national d'intervention sanitaire d'urgence mentionné à l'article L. 201-5, après avis du Conseil national d'orientation de la politique sanitaire animale et végétale et de l'Agence nationale de sécurité sanitaire de l'alimentation, de l'environnement et du travail en ce qui concerne les mesures de maîtrise des dangers sanitaires.

L'adaptation et la mise en œuvre de ce plan sanitaire au niveau départemental s'inscrit dans le dispositif opérationnel ORSEC défini par le décret n° 2005-1157 du 13 septembre 2005 relatif au plan ORSEC et pris pour application des articles L741-1 à L741-5 du code de la sécurié intérieure.
