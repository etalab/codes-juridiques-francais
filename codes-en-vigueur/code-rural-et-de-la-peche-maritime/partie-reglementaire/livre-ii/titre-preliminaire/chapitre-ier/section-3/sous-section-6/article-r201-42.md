# Article R201-42

Les organismes délégataires :

1° Garantissent l'indépendance et l'impartialité des personnels en s'assurant, notamment, de l'absence d'intérêt commercial ou de participation financière aux exploitations et établissements contrôlés. A ce titre, l'organisme délégataire interdit que la rémunération des personnes chargées d'effectuer les activités déléguées ne dépende du nombre d'inspections d'effectuées ni de leurs résultats ;

2° Attestent de moyens en personnels suffisants à l'exercice des missions déléguées ;

3° Garantissent l'égalité de traitement des usagers du service.

Il leur est interdit de subdéléguer les missions qui leur sont confiées.
