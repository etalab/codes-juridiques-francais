# Article R201-26

La demande de reconnaissance est adressée par l'association au préfet de région compétent, accompagnée d'un dossier dont le contenu est précisé par arrêté du ministre chargé de l'agriculture.

Après instruction, le préfet transmet au ministre chargé de l'agriculture la demande assortie du dossier et accompagnée de son avis. A défaut d'intervention d'un arrêté ministériel dans les six mois suivant le dépôt de la demande de reconnaissance, celle-ci est réputée refusée.

La reconnaissance d'association sanitaire régionale est délivrée pour une durée de cinq ans.
