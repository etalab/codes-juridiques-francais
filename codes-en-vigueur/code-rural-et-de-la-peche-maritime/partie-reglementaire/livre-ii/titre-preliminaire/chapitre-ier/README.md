# Chapitre Ier : Dispositions générales relatives à la prévention, à la surveillance et à la lutte contre les dangers sanitaires concernant les animaux et les végétaux

- [Section 1 : Définitions et champ d'application](section-1)
- [Section 2 : Modalités communes de prévention, de surveillance et de lutte contre les dangers sanitaires de première et de deuxième catégorie](section-2)
- [Section 3 : Rôle des personnes autres que l'Etat dans la surveillance, la prévention et la lutte contre les dangers sanitaires](section-3)
- [Section 4 : Dispositions pénales](section-4)
