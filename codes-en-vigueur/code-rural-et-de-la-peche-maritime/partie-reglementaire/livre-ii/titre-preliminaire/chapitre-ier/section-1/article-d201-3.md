# Article D201-3

L'arrêté publiant la liste des dangers de deuxième catégorie précise les régions dans lesquelles ceux-ci font, le cas échéant, l'objet d'un programme collectif volontaire approuvé. Il mentionne également les dangers donnant lieu aux obligations d'information ainsi que le destinataire de l'information conformément aux II et III de l'article D. 201-7.
