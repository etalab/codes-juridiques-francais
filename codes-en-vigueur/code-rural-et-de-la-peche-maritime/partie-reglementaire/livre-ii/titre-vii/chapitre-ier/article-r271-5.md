# Article R271-5

Les dispositions des articles R. 271-3 et R. 271-4 ne font pas obstacle à l'application des prescriptions relatives à la surveillance des animaux mordeurs ou griffeurs prévues par l'article R. 223-35.
