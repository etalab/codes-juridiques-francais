# Article R231-1-1

Le préfet de département peut mandater, dans les conditions prévues par l'article L. 203-9, des vétérinaires pour effectuer, dans des lieux d'élevage destinés à la production de denrées alimentaires :

― des contrôles visant à s'assurer du respect, par les exploitants, des normes sanitaires mentionnées aux II et III de l'article R. 231-13 ;

― des inspections ante mortem des porcs, des volailles et du gibier d'élevage, prévues respectivement aux chapitres IV, V et VI de la section IV de l'annexe I du règlement (CE) n° 854/2004 du 29 avril 2004 du Parlement européen et du Conseil fixant les règles spécifiques d'organisation des contrôles officiels concernant les produits d'origine animale destinés à la consommation humaine et les dispositions prises pour son application ;

― des inspections ante mortem des volailles dans les salles d'abattage agréées pour contrôler le respect des dispositions de la section II du chapitre VI de l'annexe III du règlement n° 853/2004 du Parlement et du Conseil du 29 avril 2004 fixant des règles spécifiques applicables aux denrées alimentaires d'origine animale.

Lorsque le vétérinaire mandaté ou, le cas échéant, le vétérinaire sanitaire auquel le préfet a demandé de concourir à l'exécution d'opérations de police sanitaire en application de l'article L. 203-7 constate une non-conformité aux dispositions dont il contrôle le respect, il peut prononcer l'une des mesures mentionnées aux alinéas 2°, 3° et 4° du I et au II de l'article L. 231-2-2.
