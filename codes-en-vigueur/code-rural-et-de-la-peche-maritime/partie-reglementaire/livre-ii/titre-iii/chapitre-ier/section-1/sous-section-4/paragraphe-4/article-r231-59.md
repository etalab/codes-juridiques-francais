# Article R231-59

Des arrêtés conjoints du ministre chargé de l'agriculture, du ministre chargé des pêches maritimes et des cultures marines et du ministre chargé de la consommation définissent :

1° Les prescriptions relatives à la nature des colis ou conditionnements autorisés pour la mise sur le marché des coquillages ;

2° Les caractéristiques et les conditions d'utilisation et de contrôle de la marque sanitaire ;

3° Les noms français officiels des coquillages.
