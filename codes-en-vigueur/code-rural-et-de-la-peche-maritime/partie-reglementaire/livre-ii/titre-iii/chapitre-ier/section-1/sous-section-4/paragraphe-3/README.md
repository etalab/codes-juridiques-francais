# Paragraphe 3 : Reparcage et purification des coquillages vivants.

- [Article R231-47](article-r231-47.md)
- [Article R231-48](article-r231-48.md)
- [Article R231-49](article-r231-49.md)
- [Article R231-50](article-r231-50.md)
- [Article R231-51](article-r231-51.md)
- [Article R231-52](article-r231-52.md)
