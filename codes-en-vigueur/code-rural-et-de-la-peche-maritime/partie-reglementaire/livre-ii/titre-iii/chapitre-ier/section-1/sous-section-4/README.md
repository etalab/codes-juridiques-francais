# Sous-section 4 : Dispositions particulières aux produits de la mer et d'eau douce

- [Paragraphe 1 : Conditions sanitaires de production et de mise sur le marché des coquillages vivants.](paragraphe-1)
- [Paragraphe 2 : Pêche non professionnelle de coquillages vivants.](paragraphe-2)
- [Paragraphe 3 : Reparcage et purification des coquillages vivants.](paragraphe-3)
- [Paragraphe 4 : Mise sur le marché des coquillages vivants.](paragraphe-4)
