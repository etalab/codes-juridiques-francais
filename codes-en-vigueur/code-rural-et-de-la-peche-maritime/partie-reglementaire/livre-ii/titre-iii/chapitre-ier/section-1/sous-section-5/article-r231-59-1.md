# Article R231-59-1

Les denrées périssables, c'est-à-dire les denrées alimentaires qui peuvent devenir dangereuses du fait de leur instabilité microbiologique lorsque la température d'entreposage n'est pas maîtrisée, doivent être transportées dans les conditions fixées par la présente sous-section.
