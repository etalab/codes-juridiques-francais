# Article R231-59-5

Sauf lorsque le recours à un engin spécial n'est pas nécessaire en application de l'article R. 231-59-4, l'utilisateur de l'engin de transport doit disposer d'une attestation officielle de conformité de celui-ci aux règles techniques qui lui sont applicables, délivrée à l'issue d'un examen technique :

― dans les conditions et pour la durée prévues par l'accord du 1er septembre 1970 susvisé, dans les cas mentionnés à l'article R. 231-59-2 ;

― selon des modalités et une périodicité prévues par arrêté du ministre chargé de l'agriculture, pour les engins utilisés uniquement sur le territoire national, mentionnés à l'article R. 231-59-3.

Pour les engins de transport neufs construits en série d'après un type déterminé, l'attestation officielle de conformité peut être délivrée au vu de l'examen technique de l'engin type et d'un contrôle par échantillonnage d'engins de la série.

Ces attestations sont délivrées par le préfet du département d'immatriculation ou de mise en service de l'engin.
