# Sous-section 2 : Conditions d'hygiène applicables aux animaux, aux produits d'origine animale, aux denrées alimentaires en contenant et aux aliments pour animaux d'origine animale ou contenant des produits d'origine animale

- [Paragraphe 1 : Champ d'application.](paragraphe-1)
- [Paragraphe 2 : Conditions d'abattage et de préparation.](paragraphe-2)
- [Paragraphe 3 : Conditions d'hygiène applicables aux transports](paragraphe-3)
- [Paragraphe 4 : Etat de santé du personnel](paragraphe-4)
- [Paragraphe 5 : Mesures d'exécution](paragraphe-5)
