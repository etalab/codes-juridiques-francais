# Sous-Section 1 : Certification officielle en matière d'échanges d'animaux vivants, de semences, ovules et embryons par les vétérinaires mandatés

- [Article D236-6](article-d236-6.md)
- [Article D236-7](article-d236-7.md)
- [Article D236-8](article-d236-8.md)
- [Article D236-9](article-d236-9.md)
