# Chapitre III : Dispositions relatives aux établissements

- [Section 2 : Agrément des établissements](section-2)
- [Section 3 : Déclarations](section-3)
- [Section 4 : Dispositions relatives à la formation](section-4)
- [Section 5 : Contrôle des établissements d'abattage   et des ateliers de traitement du gibier](section-5)
- [Article D233-20](article-d233-20.md)
