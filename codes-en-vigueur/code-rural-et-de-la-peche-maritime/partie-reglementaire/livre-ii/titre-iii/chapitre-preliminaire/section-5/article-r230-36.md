# Article R230-36

Le ministre chargé de l'alimentation peut, à la demande d'organisations interprofessionnelles reconnues en application de l'article R. 632-4, de fédérations professionnelles ou de syndicats professionnels représentants des entreprises de la production, de la transformation ou de la distribution d'une famille de produits agricoles ou alimentaires, ou, le cas échéant, d'une ou plusieurs entreprises qui justifient d'un impact significatif sur une famille de produits, accorder la reconnaissance de l'Etat à des accords collectifs par lesquels les opérateurs concernés s'engagent, par la fixation d'objectifs quantifiés, à l'amélioration de la qualité nutritionnelle et à la promotion des modes de production, de transformation et de distribution durables. Cette reconnaissance est rendue publique sur le site internet du ministère chargé de l'alimentation.

Les engagements sur la qualité nutritionnelle peuvent notamment porter sur :

― l'augmentation de la teneur en fruits et légumes ;

― la réduction de la teneur en sel ;

― la réduction de la teneur en lipides, notamment lipides totaux et acides gras saturés ;

― l'augmentation de la teneur en glucides complexes et en fibres ;

― la réduction de la teneur en glucides simples.

La mise en œuvre de ces engagements doit être conciliée avec la préservation ou l'amélioration de la qualité gustative des produits concernés.

Les engagements sur la promotion de modes de production, de transformation ou de distribution durable peuvent porter notamment sur :

― la sélection de produits agricoles ou agroalimentaires composant la denrée issus de modes de production, de transformation ou de distribution durables ;

― la réduction du gaspillage alimentaire à tous les stades de la chaîne alimentaire et la préservation des ressources naturelles ;

― la promotion du patrimoine alimentaire et culinaire.

Le ministre chargé de l'alimentation ne peut reconnaître les accords collectifs conclus par des opérateurs justifiant d'un impact significatif sur une famille de produits distribués dans les départements et régions d'outre-mer que si ces accords comportent des engagements sur la réduction des teneurs des produits en glucides et lipides visant à les rapprocher de celles de produits similaires distribués en métropole.
