# Sous-section 5 : Mesures de contrôle.

- [Article R234-9](article-r234-9.md)
- [Article R234-10](article-r234-10.md)
- [Article R234-11](article-r234-11.md)
- [Article R234-12](article-r234-12.md)
- [Article R234-13](article-r234-13.md)
- [Article R234-14](article-r234-14.md)
