# Section 2 : Règles spécifiques aux activités relatives à la reproduction des espèces bovine, ovine, caprine et porcine et des carnivores domestiques

- [Sous-section 1 : Monte publique artificielle](sous-section-1)
- [Sous-section 2 : Monte publique naturelle](sous-section-2)
- [Sous-section 3 : Monte privée artificielle](sous-section-3)
