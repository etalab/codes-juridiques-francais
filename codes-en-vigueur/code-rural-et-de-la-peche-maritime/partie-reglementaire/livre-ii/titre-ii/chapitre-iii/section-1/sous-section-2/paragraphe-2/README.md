# Paragraphe 2 : Responsabilités spécifiques à certaines collectivités ou administrations.

- [Article R223-9](article-r223-9.md)
- [Article R223-11](article-r223-11.md)
