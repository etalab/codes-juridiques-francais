# Article R223-11

Les écoles nationales vétérinaires déclarent au préfet du département d'origine les maladies réglementées constatées sur les animaux amenés à la consultation.

Dans l'intérieur de ces établissements, les mesures de police sanitaire sont appliquées par les directeurs, qui font la déclaration prévue à l'article L. 223-5.
