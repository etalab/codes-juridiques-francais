# Paragraphe 3 : Classification et mesures départementales.

- [Article R223-26](article-r223-26.md)
- [Article R223-27](article-r223-27.md)
- [Article R223-28](article-r223-28.md)
- [Article R223-29](article-r223-29.md)
- [Article R223-30](article-r223-30.md)
