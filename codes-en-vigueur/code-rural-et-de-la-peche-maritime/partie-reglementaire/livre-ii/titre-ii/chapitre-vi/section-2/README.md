# Section 2 : Dispositions relatives au service public de l'équarrissage.

- [Article R226-7](article-r226-7.md)
- [Article R226-8](article-r226-8.md)
- [Article R226-11](article-r226-11.md)
- [Article R226-12](article-r226-12.md)
- [Article R226-13](article-r226-13.md)
- [Article R226-14](article-r226-14.md)
- [Article R226-15](article-r226-15.md)
