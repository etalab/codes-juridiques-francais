# Chapitre VII : Pharmacie vétérinaire et réactifs

- [Section 1 : Pharmacovigilance.](section-1)
- [Section 2 : Programmes sanitaires d'élevage et commissions d'agrément des groupements visés aux articles L. 5143-6 et L. 5143-7 du code de la santé publique.](section-2)
