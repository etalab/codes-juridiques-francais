# Article R214-25

Le dossier de demande du certificat de capacité mentionné au 3° du IV de l'article L. 214-6 est adressé au préfet du département du lieu où s'exerce l'activité pour laquelle le postulant demande le certificat de capacité.

Le préfet peut délivrer le certificat de capacité aux postulants qui justifient :

1° Soit de la possession d'un diplôme, titre ou certificat figurant sur une liste publiée par arrêté du ministre chargé de l'agriculture ;

2° Soit de connaissances suffisantes attestées par le         directeur régional de l'alimentation, de l'agriculture et de la forêt  ou par le directeur de l'agriculture et de la forêt pour les départements d'outre-mer. Le contenu, les modalités d'évaluation des connaissances ainsi que la liste des établissements habilités à participer à cette évaluation sont définis par arrêté du ministre chargé de l'agriculture.

Les pièces constituant le dossier de demande du certificat de capacité ainsi que les modalités de présentation de ce dossier et de délivrance du certificat sont définies par arrêté du ministre chargé de l'agriculture.
