# Article D214-32-2

I.-Le certificat mentionné à l'article L. 214-8, que doit faire établir toute personne qui cède un chien, à titre gratuit ou onéreux, est délivré par un vétérinaire compte tenu, d'une part, des informations portées à sa connaissance et, d'autre part, d'un examen du chien.

II.-Les informations mentionnées au I sont :

1° L'identité, l'adresse, le cas échéant, la raison sociale du cédant ;

2° Le document justifiant de l'identification de l'animal ;

3° Le cas échéant, le numéro du passeport européen pour animal de compagnie ;

4° Le cas échéant, un certificat vétérinaire de stérilisation ;

5° Les vaccinations réalisées ;

6° Pour les chiens de race, le document délivré par une fédération nationale agréée conformément à l'article D. 214-11 ;

7° La date et le résultat de la dernière évaluation comportementale si elle a été réalisée.

III.-Le vétérinaire procède à un diagnostic de l'état de santé du chien. Il vérifie la cohérence entre la morphologie du chien et le type racial figurant dans le document justifiant de l'identification de l'animal et, le cas échéant, détermine la catégorie à laquelle le chien appartient, au sens de l'article L. 211-12.

Lorsque le document mentionné au 6° du II n'est pas produit, le vétérinaire indique sur le certificat que le chien n'appartient pas à une race. La mention " d'apparence " suivie d'un nom de race peut être inscrite sur la base des informations données par le cédant.

Dans le cas où le vétérinaire ne peut pas établir que le chien n'appartient pas à la première catégorie, il mentionne qu'une détermination morphologique devra être réalisée lorsque le chien aura entre 8 et 12 mois.

IV.-Le vétérinaire reporte sur le certificat vétérinaire les informations mentionnées au II et au III, il y précise éventuellement la race du chien sur la base du document mentionné au 6° du II. Il mentionne la date d'examen du chien et y appose son cachet.

Dans le cas où le type racial n'est pas cohérent avec celui précisé sur le document d'identification, le vétérinaire l'indique sur le certificat.

V.-Le cédant garde une copie du certificat qui doit être produite à la demande des autorités de contrôle.
