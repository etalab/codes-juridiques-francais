# Article R214-27-1

Le titulaire d'un certificat de capacité doit procéder à l'actualisation de ses connaissances dans des conditions précisées par arrêté du ministre chargé de l'agriculture. Lorsque un titulaire du certificat de capacité n'a pas satisfait à cette obligation, son certificat de capacité peut être suspendu par le préfet pour une durée de trois mois ou retiré.
