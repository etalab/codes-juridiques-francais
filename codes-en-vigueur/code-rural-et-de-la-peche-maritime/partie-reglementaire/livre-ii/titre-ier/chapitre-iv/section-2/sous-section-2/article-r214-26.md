# Article R214-26

Les frais de l'évaluation mentionnée au 3° de l'article R. 214-26 sont supportés par le candidat. Ils donnent lieu à la perception par l'Etat d'une redevance pour services rendus qui est exigible à l'occasion de chaque demande.

Le montant et les modalités de perception de cette redevance sont précisés par arrêté conjoint du ministre chargé de l'agriculture et du ministre chargé du budget.
