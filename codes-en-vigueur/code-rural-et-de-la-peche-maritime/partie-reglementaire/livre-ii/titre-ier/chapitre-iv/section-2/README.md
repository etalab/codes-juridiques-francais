# Section 2 : L'élevage, le parcage, la garde, le transit

- [Sous-section 1 : Dispositions générales.](sous-section-1)
- [Sous-section 2 : Dispositions relatives aux animaux de compagnie](sous-section-2)
- [Sous-section 3 : Dispositions particulières](sous-section-3)
