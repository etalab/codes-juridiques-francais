# Chapitre IV : La protection des animaux

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : L'élevage, le parcage, la garde, le transit](section-2)
- [Section 3 : Le transport.](section-3)
- [Section 4 : L'abattage](section-4)
- [Section 5 : Activités diverses soumises à autorisation](section-5)
- [Section 6 : Utilisation d'animaux vivants à des fins scientifiques](section-6)
