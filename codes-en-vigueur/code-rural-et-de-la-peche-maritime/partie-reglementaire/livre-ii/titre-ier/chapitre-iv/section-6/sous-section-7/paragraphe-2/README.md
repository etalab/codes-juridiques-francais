# Paragraphe 2 : Comité national de réflexion éthique sur l'expérimentation animale

- [Article R214-134](article-r214-134.md)
- [Article R214-135](article-r214-135.md)
- [Article R214-136](article-r214-136.md)
