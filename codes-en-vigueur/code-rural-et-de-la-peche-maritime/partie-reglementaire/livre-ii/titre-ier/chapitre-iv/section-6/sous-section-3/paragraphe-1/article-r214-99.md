# Article R214-99

Tout établissement éleveur, fournisseur ou utilisateur doit être agréé. A cet effet, sous réserve des dispositions de l'article R. 214-127, une demande d'agrément est adressée par le responsable de l'établissement au préfet du département du lieu d'implantation de l'établissement.

Cette demande est accompagnée d'un dossier dont les éléments sont précisés par arrêté conjoint des ministres chargés de l'agriculture et de la recherche et du ministre de la défense.
