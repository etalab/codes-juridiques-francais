# Article D212-52

Conformément à la dérogation prévue à l'article 12 du règlement (CE) n° 504/2008 de la Commission du 6 juin 2008, les chevaux issus d'une saillie déclarée d'un étalon de race de trait peuvent ne pas faire l'objet du marquage actif par l'implantation d'un transpondeur.

En ce cas, ces équidés font, au moment de leur première identification, l'objet d'un marquage actif par l'application de deux marques auriculaires agréées, la première étant un repère visuel et la seconde comportant, dans la partie femelle, un transpondeur électronique. L'apposition des marques auriculaires est réalisée dans un délai de huit jours à compter de la date de naissance de l'équidé. Le document d'identification indique dans la partie A du chapitre premier la présence de marques auriculaires ainsi que le numéro unique du repère visuel et le code du transpondeur qu'elles contiennent. Ces informations sont enregistrées dans le fichier central.

Lorsque les chevaux issus d'une saillie déclarée d'un étalon de race de trait font uniquement l'objet d'un marquage actif réalisé par leur propriétaire ou détenteur par la pose de marques auriculaires, le document d'identification ne mentionne pas les informations relatives au signalement, à l'exception de celle relative à la robe.
