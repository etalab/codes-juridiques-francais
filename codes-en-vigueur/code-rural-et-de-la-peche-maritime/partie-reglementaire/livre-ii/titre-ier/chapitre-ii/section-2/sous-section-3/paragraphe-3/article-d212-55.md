# Article D212-55

Pour les équidés d'élevage et de rente, l'Institut français du cheval et de l'équitation délivre le document d'identification dans un délai de deux mois suivant la transmission du formulaire de demande d'identification par le détenteur.

Pour les équidés enregistrés, le formulaire de demande d'identification est transmis par le détenteur à l'Institut français du cheval et de l'équitation ou à un autre organisme émetteur au sens du 1 de l'article 4 du règlement (CE) n° 504/2008 de la Commission du 6 juin 2008. Dans ce cas, le détenteur en informe le gestionnaire du fichier central dans un délai de deux mois.

Si l'identification de terrain est réalisée dans un autre Etat membre, la demande d'identification comporte les mêmes pièces et informations que celles requises pour établir le formulaire de demande d'identification mentionné à l'article D. 212-53.

Le document d'identification est délivré dans un délai de deux mois suivant la transmission de la demande d'identification. Ce délai peut être suspendu dans l'attente de la validation du certificat d'origine prévue au chapitre II du document d'identification par un autre organisme reconnu.

Pour les équidés importés, lorsque la demande d'enregistrement d'un document d'identification existant comporte l'ensemble des informations nécessaires à l'établissement du document d'identification ou son enregistrement, l'Institut français du cheval et de l'équitation délivre le document d'identification ou enregistre le document d'identification existant dans un délai de deux mois.
