# Sous-paragraphe 2 : Identification des porcins.

- [Article D212-37](article-d212-37.md)
- [Article D212-38](article-d212-38.md)
- [Article D212-39](article-d212-39.md)
- [Article R212-40](article-r212-40.md)
