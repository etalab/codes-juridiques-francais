# Paragraphe 2 : Dispositions spécifiques au cheptel bovin

- [Article D212-17](article-d212-17.md)
- [Article D212-18](article-d212-18.md)
- [Article D212-19](article-d212-19.md)
- [Article D212-20](article-d212-20.md)
- [Article D212-21](article-d212-21.md)
- [Article D212-23](article-d212-23.md)
- [Article R212-22](article-r212-22.md)
