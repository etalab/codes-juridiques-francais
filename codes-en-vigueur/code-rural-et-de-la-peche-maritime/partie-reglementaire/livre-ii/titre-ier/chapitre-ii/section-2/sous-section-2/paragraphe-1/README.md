# Paragraphe 1 : Dispositions communes

- [Article D212-16-1](article-d212-16-1.md)
- [Article R212-15](article-r212-15.md)
- [Article R212-16](article-r212-16.md)
- [Article R212-16-2](article-r212-16-2.md)
