# Article D212-13-1

La commission nationale d'identification comprend, en nombre égal, d'une part des représentants de l'administration et des établissements publics placés sous la tutelle du ministre chargé de l'agriculture et d'autre part des représentants des organisations professionnelles concernées.

Le président de la commission peut inviter des personnes choisies en raison de leur compétence à participer, sans voix délibérative, aux travaux de la commission.

La composition et le fonctionnement de la commission nationale d'identification sont fixés par arrêté du ministre chargé de l'agriculture.
