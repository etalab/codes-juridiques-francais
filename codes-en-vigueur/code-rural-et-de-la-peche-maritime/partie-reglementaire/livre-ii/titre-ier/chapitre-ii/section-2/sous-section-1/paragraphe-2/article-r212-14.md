# Article R212-14

L'agrément mentionné à l'article L. 212-12-1 est délivré, après avis de la commission nationale d'identification mentionnée à l'article D. 212-13, à des personnes répondant aux conditions d'aptitude, d'expérience et de compétences techniques nécessaires à la tenue d'un fichier nominatif, à l'issue d'un appel à candidatures.
