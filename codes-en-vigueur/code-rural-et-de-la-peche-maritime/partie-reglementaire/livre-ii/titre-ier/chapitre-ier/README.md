# Chapitre Ier : La garde des animaux domestiques et sauvages apprivoisés ou tenus en captivité

- [Section 1 : Les animaux de rente.](section-1)
- [Section 2 : Les animaux dangereux et errants](section-2)
- [Section 3 : Colombiers ― Colombophilie civile](section-3)
