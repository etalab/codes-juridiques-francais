# Sous-section 3 : Détention des chiens de la 1re et de la 2e catégorie.

- [Article D211-5-2](article-d211-5-2.md)
- [Article R211-5](article-r211-5.md)
- [Article R211-5-1](article-r211-5-1.md)
- [Article R211-5-3](article-r211-5-3.md)
- [Article R211-5-4](article-r211-5-4.md)
- [Article R211-5-5](article-r211-5-5.md)
- [Article R211-5-6](article-r211-5-6.md)
- [Article R211-6](article-r211-6.md)
- [Article R211-7](article-r211-7.md)
