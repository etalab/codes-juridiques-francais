# Article R213-7

Les délais prévus aux articles R. 213-5 et R. 213-6 courent à compter de la livraison de l'animal. La mention de cette date est portée sur la facture ou sur l'avis de livraison remis à l'acheteur.

Les délais mentionnés aux articles R. 213-5 à R. 213-8 sont comptés conformément aux articles 640, 641 et 642 du code de procédure civile ci-après reproduits :

" Art. 640-Lorsqu'un acte ou une formalité doit être accompli avant l'expiration d'un délai, celui-ci a pour origine la date de l'acte, de l'événement, de la décision ou de la notification qui le fait courir.

" Art. 641-Lorsqu'un délai est exprimé en jours, celui de l'acte, de l'événement, de la décision ou de la notification qui le fait courir ne compte pas.

" Lorsqu'un délai est exprimé en mois ou en années, ce délai expire le jour du dernier mois ou de la dernière année qui porte le même quantième que le jour de l'acte, de l'événement, de la décision ou de la notification qui fait courir le délai. A défaut d'un quantième identique, le délai expire le dernier jour du mois.

" Lorsqu'un délai est exprimé en mois et en jours, les mois sont d'abord décomptés, puis les jours.

" Art. 642-Tout délai expire le dernier jour à vingt-quatre heures.

" Le délai qui expirerait normalement un samedi, un dimanche ou un jour férié ou chômé, est prorogé jusqu'au premier jour ouvrable suivant ".
