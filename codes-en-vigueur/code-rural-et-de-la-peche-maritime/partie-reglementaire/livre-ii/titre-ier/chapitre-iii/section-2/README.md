# Section 2 : Action en garantie et expertise

- [Sous-section 1 : Introduction de l'action et nomination des experts.](sous-section-1)
- [Sous-section 2 : Délais pour introduire les actions.](sous-section-2)
- [Sous-section 3 : Procédure relative à l'expertise.](sous-section-3)
