# Chapitre III : Les cessions d'animaux et de produits animaux

- [Section 1 : Les vices rédhibitoires](section-1)
- [Section 2 : Action en garantie et expertise](section-2)
