# Paragraphe 2 : Exercice dans les établissements pharmaceutiques mentionnés à l'article R. 5145-2 du code de la santé publique.

- [Article R242-78](article-r242-78.md)
- [Article R242-79](article-r242-79.md)
