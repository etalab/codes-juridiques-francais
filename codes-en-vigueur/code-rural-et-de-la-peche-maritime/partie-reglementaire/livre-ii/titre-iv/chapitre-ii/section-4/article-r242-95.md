# Article R242-95

Le rapporteur a qualité pour procéder à l'audition du vétérinaire poursuivi et, d'une façon générale, recueillir tous les témoignages et procéder à toutes constatations nécessaires à la manifestation de la vérité.

Les dépositions consignées sur des procès-verbaux d'audition sont signées par les personnes entendues ainsi que par le rapporteur. Elles sont communiquées au vétérinaire concerné.

Lorsqu'il a achevé son instruction, le rapporteur transmet le dossier accompagné de son rapport écrit au président du conseil régional qui l'a désigné. Le rapport précise les faits dénoncés et les diligences accomplies.
