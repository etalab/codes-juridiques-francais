# Article R242-100

La chambre régionale de discipline ne peut valablement statuer que si la majorité des membres composant la formation de jugement appelée à délibérer sont présents.
