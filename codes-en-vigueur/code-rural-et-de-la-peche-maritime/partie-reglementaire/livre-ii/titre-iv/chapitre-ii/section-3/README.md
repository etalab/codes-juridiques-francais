# Section 3 : Inscription au tableau de l'ordre des vétérinaires.

- [Article R242-85](article-r242-85.md)
- [Article R242-86](article-r242-86.md)
- [Article R242-87](article-r242-87.md)
- [Article R242-88](article-r242-88.md)
- [Article R242-89](article-r242-89.md)
- [Article R242-90](article-r242-90.md)
- [Article R242-91](article-r242-91.md)
