# Sous-section 2 : Organisation générale.

- [Article R242-4](article-r242-4.md)
- [Article R242-5](article-r242-5.md)
- [Article R242-6](article-r242-6.md)
