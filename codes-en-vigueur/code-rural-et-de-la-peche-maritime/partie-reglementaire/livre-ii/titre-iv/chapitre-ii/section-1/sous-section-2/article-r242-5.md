# Article R242-5

Le Conseil supérieur de l'ordre des vétérinaires est composé de douze membres élus pour six ans par les membres des conseils régionaux selon les modalités prévues à la sous-section 4 de la présente section.

Les membres du conseil supérieur sont renouvelables par moitié tous les trois ans. Ils sont rééligibles.

Le conseil supérieur élit en son sein un président, un vice-président, un secrétaire général et un trésorier pour un mandat de trois ans.

Les élections du bureau ont lieu à bulletin secret, à la majorité absolue au premier tour, à la majorité relative au second tour. En cas d'égalité de suffrages entre deux candidats, le plus âgé est proclamé élu.

Les délibérations du conseil sont prises à la majorité des voix. En cas d'égalité, le président a voix prépondérante.

En cas de décès, démission ou cessation de fonctions d'un membre du bureau, il est procédé immédiatement à son remplacement dans les conditions prévues pour son élection. Le nouvel élu achève le mandat de son prédécesseur.
