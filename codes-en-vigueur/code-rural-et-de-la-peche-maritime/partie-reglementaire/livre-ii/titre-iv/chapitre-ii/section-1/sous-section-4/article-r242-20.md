# Article R242-20

Chaque associé d'une société d'exercice en commun est individuellement électeur et éligible au Conseil supérieur de l'ordre sans que la société soit elle-même électrice ou éligible.
