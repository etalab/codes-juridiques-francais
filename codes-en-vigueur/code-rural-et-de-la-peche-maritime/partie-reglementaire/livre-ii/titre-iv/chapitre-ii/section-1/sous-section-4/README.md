# Sous-section 4 : Election des membres du conseil supérieur de l'ordre.

- [Article R242-20](article-r242-20.md)
- [Article R242-21](article-r242-21.md)
- [Article R242-27](article-r242-27.md)
