# Article R242-1

Dans l'étendue de son ressort, le conseil régional de l'ordre surveille l'exercice de la médecine et de la chirurgie des animaux.

Il veille sur la moralité et l'honneur de la profession vétérinaire et maintient la discipline au sein de l'ordre.

Il veille au respect des dispositions législatives et réglementaires qui régissent la profession.

Il étudie les problèmes qui s'y rapportent et peut en saisir le conseil supérieur.
