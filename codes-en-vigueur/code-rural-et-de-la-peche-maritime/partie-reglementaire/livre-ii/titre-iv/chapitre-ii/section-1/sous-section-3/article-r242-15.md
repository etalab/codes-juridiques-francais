# Article R242-15

I. - Dès la clôture du scrutin, le contenu de l'urne et les listes d'émargement gérés par les serveurs sont figés et scellés automatiquement sur l'ensemble des serveurs, dans des conditions garantissant la conservation des données.

Avant l'ouverture du dépouillement, le président du bureau de vote et ses assesseurs utilisent leurs clés de déchiffrement dont l'utilisation conjointe permet d'accéder aux données du fichier dénommé " contenu de l'urne électronique  "    ;

Le président dispose des éléments permettant de vérifier l'intégrité du système.

Après la vérification de l'intégrité du fichier dénommé " contenu de l'urne électronique ", le président du bureau de vote et ses assesseurs procèdent publiquement à l'ouverture de l'urne électronique.

II. - Les données suivantes apparaissent de manière lisible à l'écran et font l'objet d'une édition sécurisée permettant leur transposition sur le procès-verbal :

- le nombre d'électeurs ;

- les listes d'émargement définitives ;

- le décompte des électeurs ayant validé leur vote ;

- le nombre de bulletins blancs ou nuls ;

- le nombre de suffrages valablement exprimés ;

- le décompte du nombre de voix obtenues par candidat.

Le bureau de vote vérifie que le nombre total de suffrages reçus par voie électronique correspond au nombre de votants de la liste d'émargement électronique.

Le système de vote électronique par internet est verrouillé après le dépouillement de sorte qu'il soit impossible de reprendre ou modifier le résultat après la décision de clôture du dépouillement prise par le bureau de vote.
