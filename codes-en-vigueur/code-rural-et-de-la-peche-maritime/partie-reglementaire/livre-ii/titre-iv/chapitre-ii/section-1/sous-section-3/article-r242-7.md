# Article R242-7

Les membres du conseil régional de l'ordre sont élus par les vétérinaires mentionnés à l'article L. 241-1 et inscrits au tableau de l'ordre du conseil régional concerné par les élections.

Chaque associé d'une société d'exercice en commun est individuellement électeur et éligible au conseil régional de l'ordre sans que la société soit elle-même électrice ou éligible.
