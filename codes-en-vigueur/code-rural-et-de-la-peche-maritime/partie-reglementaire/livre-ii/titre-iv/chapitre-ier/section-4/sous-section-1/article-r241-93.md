# Article R241-93

En cas de fusion ou de scission de société civile professionnelle dans les conditions prévues par l'article 2-1 de la loi n° 66-879 du 29 novembre 1966, la nouvelle société créée ou les sociétés scissionnaires devront accomplir les formalités d'inscription, d'immatriculation et de publicité prévues aux articles R. 241-31 à R. 241-33 et R. 241-36.
