# Section 4 : Dispositions relative à l'exercice en commun de la profession vétérinaire en France

- [Sous-section 1 : Sociétés civiles professionnelles de vétérinaires.](sous-section-1)
- [Sous-section 2 : Sociétés d'exercice libéral de vétérinaires.](sous-section-2)
- [Sous-section 3 : Dispositions communes aux différentes sociétés d'exercice vétérinaire](sous-section-3)
