# Article D241-1

Les épreuves pour l'obtention du diplôme d'Etat de docteur vétérinaire, créé par la loi du 31 juillet 1923, consistent dans la rédaction et la soutenance d'une thèse.

Les élèves des écoles mentionnées aux 5°, 6°, 7° et 8° de l'article D. 812-1 qui se destinent à la profession de vétérinaire soutiennent leur thèse devant, respectivement, les universités de Lyon-I, Nantes, Paris-XII et Toulouse.
