# Article D241-5

Les élèves reçus au concours d'entrée dans les écoles nationales vétérinaires ne peuvent recevoir comme sanction de leurs études au sein de ces écoles que le diplôme d'Etat de docteur vétérinaire.
