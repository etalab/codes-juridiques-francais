# Sous-section 3 : Ressortissants français ou ressortissants d'un autre Etat membre de la Communauté européenne ou d'un autre Etat partie à l'accord sur l'Espace économique européen, titulaires d'un diplôme, certificat ou titre de vétérinaire émanant d'un pays tiers.

- [Article R241-25](article-r241-25.md)
- [Article R241-26](article-r241-26.md)
- [Article R241-27](article-r241-27.md)
