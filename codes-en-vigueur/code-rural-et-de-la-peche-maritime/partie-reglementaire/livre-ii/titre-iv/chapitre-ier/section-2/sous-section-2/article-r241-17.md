# Article R241-17

Le ministre chargé de l'agriculture transmet aux autorités compétentes des Etats mentionnés à l'article R. 241-16 et reçoit de leur part les informations relatives aux mesures ou sanctions de caractère professionnel, administratif ou pénal prononcées à l'encontre des vétérinaires migrant au sein de la Communauté européenne et de l'Espace économique européen.
