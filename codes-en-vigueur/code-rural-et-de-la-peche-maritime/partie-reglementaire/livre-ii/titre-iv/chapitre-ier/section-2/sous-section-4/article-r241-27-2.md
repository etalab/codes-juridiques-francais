# Article R241-27-2

Tout vétérinaire qui sollicite son enregistrement doit adresser sa demande au président du conseil régional de l'ordre dans la région où il se propose de fixer son domicile personnel ou professionnel administratif tel que mentionné à l'article R. 242-52.

La demande d'enregistrement doit être accompagnée des pièces suivantes :

- la présentation de l'original ou la production ou l'envoi d'une photocopie lisible d'un passeport ou d'une carte nationale d'identité en cours de validité ;

- la copie du diplôme d'Etat de docteur vétérinaire ou diplôme, certificat ou titre de vétérinaire et, pour les vétérinaires d'origine étrangère et naturalisés français, de l'arrêté ministériel les habilitant à exercer en France ou, s'ils sont originaires de la Communauté européenne ou des autres Etats partie, à l'accord sur l'Espace économique européen, de l'un des titres mentionnés à l'article L. 241-2.
