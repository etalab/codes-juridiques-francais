# Section 2 : Contrôle périodique obligatoire

- [Sous-section 1 : Modalités du contrôle des pulvérisateurs](sous-section-1)
- [Sous-section 2 : Les organismes d'inspection](sous-section-2)
- [Sous-section 3 : Les centres de formation des inspecteurs](sous-section-3)
- [Sous-section 4 : Le groupement d'intérêt public](sous-section-4)
- [Sous-section 5 : Agrément des organismes d'inspection et des centres de formation](sous-section-5)
- [Sous-section 6 : Dispositions pénales](sous-section-6)
