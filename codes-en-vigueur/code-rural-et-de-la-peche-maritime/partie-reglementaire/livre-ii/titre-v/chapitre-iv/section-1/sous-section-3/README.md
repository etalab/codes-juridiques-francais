# Sous-section 3 : Certificats individuels

- [Article R254-8](article-r254-8.md)
- [Article R254-9](article-r254-9.md)
- [Article R254-10](article-r254-10.md)
- [Article R254-11](article-r254-11.md)
- [Article R254-12](article-r254-12.md)
- [Article R254-13](article-r254-13.md)
- [Article R254-14](article-r254-14.md)
