# Article R254-10

Pour les titulaires d'un certificat individuel, son renouvellement ou l'obtention d'un certificat dans une autre spécialité professionnelle ou une autre catégorie peut être obtenu dans les conditions prévues à l'article R. 254-9. Le contenu et la durée des programmes de formation sont fixés par arrêté du ministre chargé de l'agriculture.
