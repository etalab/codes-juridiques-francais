# Article R257-3

Est puni de l'amende prévue pour les contraventions de la troisième classe le fait, pour les exploitants mentionnés à l'article L. 257-1 :

1° De ne pas tenir le registre mentionné à l'article L. 257-3 dans les conditions prévues par l'arrêté ministériel pris pour l'application de cet article ;

2° De ne pas utiliser une eau conforme aux prescriptions du 5° (c) de l'annexe I, partie A, du règlement (CE) n° 852/2004 du 29 avril 2004 relatif à l'hygiène des denrées alimentaires.
