# Section 4 : Emballage et étiquetage

- [Article D253-43-1](article-d253-43-1.md)
- [Article R253-41](article-r253-41.md)
- [Article R253-42](article-r253-42.md)
- [Article R253-43](article-r253-43.md)
