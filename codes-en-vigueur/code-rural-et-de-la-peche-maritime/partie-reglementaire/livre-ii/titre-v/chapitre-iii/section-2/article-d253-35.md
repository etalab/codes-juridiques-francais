# Article D253-35

I. ― Le détenteur d'une autorisation de mise sur le marché ou tout  bénéficiaire d'une extension d'autorisation pour un usage mineur d'un  produit mentionné à l'article 51 du règlement (CE) n° 1107/2009  communique au ministre chargé de l'agriculture et à l'Agence les  informations mentionnées à l'article 56 du règlement.

Si la  première autorisation d'un produit au sein de la zone sud a été délivrée  sur le territoire national, l'Agence évalue ces informations et  transmet le résultat de cette évaluation au ministre chargé de  l'agriculture, qui informe la Commission européenne et les autres Etats  membres de la zone, dans les conditions prévues par le 3 de l'article 56  du règlement précité.

II. ― Le détenteur d'un permis de commerce  parallèle communique au ministre chargé de l'agriculture et à l'Agence  les informations mentionnées au 4 de l'article 56 du règlement précité  et une copie de l'étiquette du produit mise à jour suite aux  modifications intervenues sur le produit de référence, après la  délivrance du permis.
