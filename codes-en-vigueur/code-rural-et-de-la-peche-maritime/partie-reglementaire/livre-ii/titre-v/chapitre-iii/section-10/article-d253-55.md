# Article D253-55

La commission des produits phytopharmaceutiques, des matières fertilisantes et des supports de culture est chargée :

1° De proposer au ministre chargé de l'agriculture toutes les mesures susceptibles de contribuer à la définition et à la normalisation des conditions d'emploi des produits mentionnés à l'article L. 253-1 et à l'article L. 255-1, eu égard à leur degré d'efficacité et à leurs effets indésirables de tous ordres, notamment écologiques et sanitaires ;

2° De donner son avis sur toutes les questions que lui soumettent les ministres intéressés et de formuler toutes recommandations relevant de sa compétence et concernant les produits mentionnés aux articles L. 253-1 et L. 255-1.

Cette commission comprend notamment :

1° Des représentants des services publics ;

2° Des représentants des organismes professionnels intéressés ;

3° Des représentants des organisations agréées de consommateurs ;

4° Des représentants d'associations de protection de l'environnement agréées au niveau national conformément aux dispositions de l'article L. 141-1 du code de l'environnement ;

5° Des représentants des organisations syndicales de salariés les plus représentatifs du secteur ;

6° Des personnalités qualifiées désignées.

Un arrêté conjoint des ministres chargés de l'agriculture, de la santé, de la consommation, de l'industrie et de l'environnement fixe la composition et les modalités de fonctionnement de la commission.
