# Article D253-25

Le ministre chargé de l'agriculture notifie sa décision au demandeur  dans un délai de quinze jours à compter de la réception de l'avis de  l'Agence, à qui il adresse copie de la décision.

Si l'Agence n'a pas émis son avis dans les délais qui lui sont impartis,  le ministre chargé de l'agriculture peut prendre sa décision, après  avoir procédé à un examen d'identité du produit. L'absence de décision à  l'issue des délais fixés par le présent article vaut décision de rejet.
