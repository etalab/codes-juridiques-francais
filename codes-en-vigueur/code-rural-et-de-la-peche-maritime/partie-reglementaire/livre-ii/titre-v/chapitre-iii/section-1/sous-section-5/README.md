# Sous-section 5 : Permis de commerce parallèle

- [Article D253-24](article-d253-24.md)
- [Article D253-25](article-d253-25.md)
- [Article R253-23](article-r253-23.md)
- [Article R253-26](article-r253-26.md)
- [Article R253-27](article-r253-27.md)
- [Article R253-28](article-r253-28.md)
- [Article R253-29](article-r253-29.md)
