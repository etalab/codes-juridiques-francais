# Article D253-17

-Les décisions relatives à la mise sur le marché des produits visés à  l'article L. 253-1 sont rendues publiques par voie électronique par  l'Agence, dans les conditions prévues à l'article 57 du règlement (CE) n° 1107/2009.
