# Article R253-6

L'avis de l'Agence n'est pas requis pour les demandes suivantes :

1° Pour les demandes d'autorisation de mise sur le marché mentionnées à l'article 53 du règlement (CE) n° 1107/2009 du 21 octobre 2009 ;

2° Pour les demandes d'autorisation d'une préparation naturelle peu  préoccupante pour laquelle une autorisation a déjà été accordée dans un  Etat membre de l'Union européenne selon les principes uniformes visés à  l'article 29 du règlement (CE) n° 1107/2009 ou pour laquelle un produit phytopharmaceutique de composition équivalente a été autorisé en France depuis plus de dix ans.
