# Sous-section 1 : Approbation des substances actives, phytoprotecteurs et synergistes

- [Article D253-2](article-d253-2.md)
- [Article D253-3](article-d253-3.md)
- [Article D253-4](article-d253-4.md)
