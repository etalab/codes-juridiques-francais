# Article R253-30

I. ― Le permis mentionné à l'article 54 du règlement (CE) n° 1107/2009 pour  effectuer des essais, expériences ou études de produits  phytopharmaceutiques et adjuvants ainsi que les décisions faisant suite à  une demande de modification, de renouvellement ou de retrait de ce  permis sont délivrés par le ministre chargé de l'agriculture pour une  durée qui ne peut excéder trois ans, dans les conditions prévues par cet  article et par la section 1 du présent chapitre. Les productions  végétales issues des essais, expériences ou études et susceptibles  d'être consommées par l'homme ou l'animal sont détruites, sauf si le  permis prévoit une dérogation à l'obligation de destruction des  récoltes.

II. ― Si les essais, expériences, ou  études sont susceptibles de présenter des effets nocifs pour la santé  humaine ou animale ou inacceptables pour l'environnement et si aucune  mesure de gestion des risques ne permet de les atténuer, le ministre  chargé de l'agriculture peut refuser d'accorder le permis et s'opposer à  la réalisation des essais officiellement reconnus mentionnés au II de  l'article R. 253-38 ou dans les conditions mentionnées au II de  l'article R. 253-39.

III. ― Le permis peut être  retiré ou modifié par le ministre chargé de l'agriculture s'il apparaît  que les conditions exigées pour sa délivrance cessent d'être réunies.
