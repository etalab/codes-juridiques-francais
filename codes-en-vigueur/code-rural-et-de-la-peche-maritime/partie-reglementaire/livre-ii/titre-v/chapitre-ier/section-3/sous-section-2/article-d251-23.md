# Article D251-23

Si les résultats des contrôles sont satisfaisants, un passeport phytosanitaire est délivré s'il y a lieu par les agents chargés de la protection des végétaux.

Lorsque les végétaux, produits végétaux ou autres objets importés sont accompagnés d'un passeport phytosanitaire, ils peuvent faire l'objet des contrôles prévus à l'article D. 251-21.
