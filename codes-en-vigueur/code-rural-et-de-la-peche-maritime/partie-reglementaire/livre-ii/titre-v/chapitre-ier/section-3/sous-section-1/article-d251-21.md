# Article D251-21

I.-Lorsque les végétaux, produits végétaux et autres objets figurant sur la liste mentionnée au A du V de l'article D. 251-2 doivent être accompagnés d'un passeport phytosanitaire, les agents mentionnés à l'article L. 250-2 vérifient que :

1° Le passeport phytosanitaire accompagne les végétaux, produits végétaux ou autres objets et qu'il est fixé, de façon qu'il ne puisse être réutilisé ;

2° Les rubriques d'informations du passeport phytosanitaire ou du passeport phytosanitaire de remplacement qui accompagnent les végétaux, produits végétaux ou autres objets sont dûment remplies, en application de l'article D. 251-17 ;

3° Le passeport phytosanitaire comporte la marque " ZP " lorsque les végétaux, produits végétaux et autres objets sont autorisés pour une ou plusieurs zones spécifiques protégées ;

4° Le passeport phytosanitaire, qui en remplace un autre, comporte la marque " RP " ;

5° Le passeport phytosanitaire comporte l'indication du nom du pays d'origine ou du pays d'expédition lorsqu'il est délivré pour des végétaux, produits végétaux ou autres objets originaires de pays tiers à la Communauté européenne.

II.-Les contrôles portent sur la conformité du passeport phytosanitaire et le respect des exigences mentionnées au IV de l'article D. 251-2. Ils sont réalisés de manière aléatoire et sans discrimination en ce qui concerne l'origine des végétaux, produits végétaux ou autres objets originaires de pays tiers à la Communauté européenne.

Ils sont :

1° Soit occasionnels à tout moment et en tout lieu, lorsque les végétaux, produits végétaux et autres objets sont déplacés ;

2° Soit occasionnels dans les établissements où les végétaux, produits végétaux et autres objets sont stockés ou mis en vente, ainsi que dans les établissements des acheteurs, lesquels doivent conserver, en tant qu'utilisateurs finals engagés professionnellement dans la production de végétaux, les passeports phytosanitaires pendant un an et en consignent les références dans leurs livres ;

3° Soit réalisés simultanément, à tout contrôle de documents effectué pour des raisons autres que phytosanitaires.

Ces contrôles peuvent devenir réguliers et peuvent être sélectifs si des indices donnent à penser que les exigences phytosanitaires ne sont pas respectées.
