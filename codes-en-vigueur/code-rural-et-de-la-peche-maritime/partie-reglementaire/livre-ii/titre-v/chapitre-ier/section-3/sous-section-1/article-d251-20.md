# Article D251-20

Le passeport phytosanitaire est délivré par les agents chargés de la protection des végétaux.
