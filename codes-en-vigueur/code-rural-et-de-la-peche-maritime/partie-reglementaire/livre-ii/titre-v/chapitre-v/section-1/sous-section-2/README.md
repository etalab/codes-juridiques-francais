# Sous-section 2 : Contrôle des matières fertilisantes et des supports de culture composés en tout ou partie d'organismes génétiquement modifiés

- [Paragraphe 1 : Autorisation de dissémination volontaire à toute autre fin que la mise sur le marché.](paragraphe-1)
- [Paragraphe 2 : Autorisation de mise sur le marché.](paragraphe-2)
