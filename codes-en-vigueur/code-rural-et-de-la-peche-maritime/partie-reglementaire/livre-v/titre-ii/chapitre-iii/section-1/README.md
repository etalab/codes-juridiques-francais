# Section 1 : Capital social.

- [Article R523-1](article-r523-1.md)
- [Article R523-1-1](article-r523-1-1.md)
- [Article R523-2](article-r523-2.md)
- [Article R523-3](article-r523-3.md)
- [Article R523-4](article-r523-4.md)
- [Article R523-5](article-r523-5.md)
- [Article R523-5-1](article-r523-5-1.md)
