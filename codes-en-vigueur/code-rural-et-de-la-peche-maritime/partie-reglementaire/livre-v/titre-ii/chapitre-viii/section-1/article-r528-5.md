# Article R528-5

Deux commissaires du Gouvernement auprès du haut conseil sont désignés, l'un par le ministre chargé de l'agriculture, l'autre par le ministre chargé de l'économie sociale. Ils siègent avec voix consultative au comité directeur.

Ils assurent l'information des ministres sur l'activité permanente du haut conseil.

Ils veillent :

- au respect des textes, règles et principes de la coopération agricole par le haut conseil et au fonctionnement régulier de ses instances ;

- au respect des normes et principes de la révision.

Ils peuvent présenter des observations au comité directeur.
