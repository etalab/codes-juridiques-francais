# Article R528-6

Il est créé, au sein du haut conseil, trois sections, la section juridique, la section révision et la section économique et financière, chargées de formuler des propositions et des avis au comité directeur dans leur domaine de compétence.

Le comité directeur désigne pour chaque section un conseil de section, présidé par un membre du comité directeur et composé de membres du comité directeur et de membres désignés par ce dernier.

Les statuts du haut conseil prévoient les attributions de chacune des sections et les modalités de désignation de leurs membres.
