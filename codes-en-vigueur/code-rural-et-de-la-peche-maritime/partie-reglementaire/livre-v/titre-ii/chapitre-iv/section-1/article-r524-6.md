# Article R524-6

Les copies ou extraits des procès-verbaux des délibérations du conseil d'administration ou des procès-verbaux des assemblées générales sont valablement certifiés par le président du conseil d'administration ou un ou plusieurs administrateurs habilités à cet effet par le conseil d'administration.

Au cours de la liquidation de la société, ces copies ou extraits sont valablement certifiés par un seul liquidateur.
