# Chapitre IV : Administration

- [Section 1 : Conseil d'administration.](section-1)
- [Section 2 : Assemblée générale.](section-2)
- [Section 3 : Comptes sociaux, consolidés ou combinés et commissariat aux comptes](section-3)
- [Section 4 : Dispositions concernant les unions de coopératives.](section-4)
- [Section 5 : Directoire et conseil de surveillance.](section-5)
