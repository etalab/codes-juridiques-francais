# Article R521-3

Les sociétés coopératives agricoles peuvent fournir à l'union à laquelle elles adhèrent les services nécessaires à la réalisation de son objet statutaire.

Les sociétés coopératives et leurs unions peuvent fournir à une société d'intérêt collectif agricole dont elles sont membres les services nécessaires à la réalisation de son objet statutaire. Une société coopérative agricole peut également mettre des immeubles, du matériel ou de l'outillage, notamment des moyens de transport, à la disposition de cette coopérative, d'une société d'intérêt collectif agricole, associé coopérateur, ou d'une autre société coopérative.
