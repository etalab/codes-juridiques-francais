# Section 2 : Fusion ― Scission ― Apports partiel d'actifs

- [Article R526-4](article-r526-4.md)
- [Article R526-5](article-r526-5.md)
- [Article R526-6](article-r526-6.md)
- [Article R526-7](article-r526-7.md)
- [Article R526-8](article-r526-8.md)
- [Article R526-9](article-r526-9.md)
- [Article R526-10](article-r526-10.md)
- [Article R526-11](article-r526-11.md)
