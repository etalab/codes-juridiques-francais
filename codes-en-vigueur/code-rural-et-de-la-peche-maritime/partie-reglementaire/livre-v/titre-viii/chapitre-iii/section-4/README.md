# Section 4 : Transformation, dissolution, liquidation.

- [Article R583-21](article-r583-21.md)
- [Article R583-22](article-r583-22.md)
- [Article R583-23](article-r583-23.md)
