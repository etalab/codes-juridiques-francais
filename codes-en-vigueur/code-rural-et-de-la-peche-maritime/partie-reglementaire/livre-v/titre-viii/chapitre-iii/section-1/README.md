# Section 1 : Constitution.

- [Article R583-2](article-r583-2.md)
- [Article R583-3](article-r583-3.md)
- [Article R583-4](article-r583-4.md)
- [Article R583-5](article-r583-5.md)
- [Article R583-6](article-r583-6.md)
- [Article R583-7](article-r583-7.md)
- [Article R583-8](article-r583-8.md)
- [Article R583-9](article-r583-9.md)
- [Article R583-10](article-r583-10.md)
- [Article R583-11](article-r583-11.md)
- [Article R583-12](article-r583-12.md)
- [Article R583-13](article-r583-13.md)
