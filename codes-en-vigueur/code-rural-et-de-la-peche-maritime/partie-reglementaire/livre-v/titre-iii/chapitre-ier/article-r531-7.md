# Article R531-7

Les sociétés d'intérêt collectif agricole qui n'ont pas la forme commerciale établissent des comptes annuels suivant les méthodes et principes fixés par les articles 8 à 16 du code de commerce et le décret n° 83-1020 du 29 novembre 1983, sous réserve des règles posées par un plan comptable approuvé par le ministre de l'agriculture après avis de l'Autorité des normes comptables.
