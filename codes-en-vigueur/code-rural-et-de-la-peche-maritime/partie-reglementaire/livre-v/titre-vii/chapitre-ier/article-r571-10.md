# Article R571-10

Les quatrième et cinquième alinéas de l'article R. 511-11 ne sont pas applicables à Mayotte.

Pour l'application du troisième alinéa du même article, l'avant-dernière phrase est supprimée.
