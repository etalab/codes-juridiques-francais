# Article R571-23

Pour l'application à Mayotte de l'article R. 511-85, la référence à l'article L. 511-5 ainsi que le mot : "ou" qui la suit sont supprimés et les mots : "des deux collèges" sont remplacés par les mots : "du collège".
