# Article D511-97

Une chambre interdépartementale d'agriculture de l'Ile-de-France, dont le siège est fixé dans le département des Yvelines, a pour circonscription Paris et les départements des Yvelines, de l'Essonne, du Val-d'Oise, du Val-de-Marne, de la Seine-Saint-Denis et des Hauts-de-Seine.

Elle est soumise à toutes les dispositions concernant les chambres départementales d'agriculture, sous réserve des dispositions de la présente section.
