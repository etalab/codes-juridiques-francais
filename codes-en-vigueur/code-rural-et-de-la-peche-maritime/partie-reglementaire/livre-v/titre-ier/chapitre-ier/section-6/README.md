# Section 6 : Chambres interdépartementales

- [Sous-section 1 : Dispositions générales](sous-section-1)
- [Sous-section 2 : Dispositions particulières à l'Ile-de-France](sous-section-2)
