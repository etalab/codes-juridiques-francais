# Sous-section 1 : Conditions requises pour être électeur

- [Paragraphe 1 : Electeurs votant individuellement.](paragraphe-1)
- [Paragraphe 2 : Groupements électeurs.](paragraphe-2)
