# Section 3 : Elections

- [Sous-section 1 : Conditions requises pour être électeur](sous-section-1)
- [Sous-section 2 : Listes électorales](sous-section-2)
- [Sous-section 3 : Eligibilité et candidatures.](sous-section-3)
- [Sous-section 4 : Propagande.](sous-section-4)
- [Sous-section 5 : Mode de scrutin.](sous-section-5)
- [Sous-section 6 : Opérations de vote](sous-section-6)
- [Sous-section 7 : Contentieux.](sous-section-7)
- [Sous-section 8 : Cessation de mandat.](sous-section-8)
- [Sous-section 9 : Elections partielles.](sous-section-9)
