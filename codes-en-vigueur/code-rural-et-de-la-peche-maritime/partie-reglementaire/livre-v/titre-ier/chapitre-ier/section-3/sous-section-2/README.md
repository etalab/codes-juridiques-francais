# Sous-section 2 : Listes électorales

- [Paragraphe 1 : Electeurs votant individuellement.](paragraphe-1)
- [Paragraphe 2 : Groupements électeurs.](paragraphe-2)
