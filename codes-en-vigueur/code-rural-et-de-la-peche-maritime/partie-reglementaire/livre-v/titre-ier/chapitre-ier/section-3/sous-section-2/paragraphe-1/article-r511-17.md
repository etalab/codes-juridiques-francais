# Article R511-17

Cette commission prépare avant le 1er octobre, commune par commune et pour chaque collège d'électeurs individuels, la liste provisoire des électeurs en prenant pour base la dernière liste établie. Elle peut demander à chaque maire de lui indiquer les noms qu'il convient de retirer de l'ancienne liste en raison de décès ou de départ de la commune. La commission met également à jour la liste des demandes d'inscription transmises en application de l'article R. 511-12. Pour les collèges mentionnés aux 1,3 et 4 de l'article R. 511-6, la commission peut se faire communiquer par les caisses départementales ou pluridépartementales de la mutualité sociale agricole, dans les conditions fixées en application du I de l'article 77 de la loi n° 95-95 du 1er février 1995 modifiée de modernisation de l'agriculture, la liste de leurs assujettis remplissant les conditions définies par l'article R. 511-8. Elle peut également utiliser toutes autres sources d'information dont elle pourrait disposer.

Elle inscrit d'office les électeurs dont la capacité électorale lui est connue, même s'ils n'ont pas demandé leur inscription et procède aux radiations. Elle inscrit également sur cette liste les personnes qui rempliront les conditions requises avant la clôture définitive de la liste. Elle peut exiger des intéressés toute pièce de nature à justifier de leur qualité pour être inscrits sur la liste électorale.

La commission tient un registre de toutes ses décisions et y mentionne les motifs et pièces à l'appui.

Au plus tard le 1er octobre, le président de la commission transmet à chaque mairie un exemplaire de la liste provisoire des électeurs de la commune pour chacun des collèges.

La liste provisoire est également transmise à la chambre départementale d'agriculture qui en assure la mise à disposition du public pour consultation.
