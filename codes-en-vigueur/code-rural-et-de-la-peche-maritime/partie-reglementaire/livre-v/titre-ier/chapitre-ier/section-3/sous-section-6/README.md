# Sous-section 6 : Opérations de vote

- [Paragraphe 1 : Date du scrutin.](paragraphe-1)
- [Paragraphe 2 : Electeurs votant individuellement.](paragraphe-2)
- [Paragraphe 3 : Groupements électeurs.](paragraphe-3)
- [Paragraphe 4 : Recensement des votes.](paragraphe-4)
