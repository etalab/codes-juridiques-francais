# Paragraphe 2 : Electeurs votant individuellement.

- [Article R511-45](article-r511-45.md)
- [Article R511-46](article-r511-46.md)
- [Article R511-47](article-r511-47.md)
