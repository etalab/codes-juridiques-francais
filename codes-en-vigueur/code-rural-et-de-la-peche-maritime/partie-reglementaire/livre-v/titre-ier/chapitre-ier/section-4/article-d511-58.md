# Article D511-58

Le commissaire de la République et le président du conseil général du département où la chambre d'agriculture a son siège peuvent assister aux séances de la chambre. Ils sont entendus chaque fois qu'ils le demandent. Ils peuvent se faire assister ou représenter.

Le directeur départemental de l'agriculture assiste à titre consultatif aux réunions de la chambre d'agriculture. Il peut se faire suppléer et se faire accompagner par tout fonctionnaire qualifié pour l'assister dans l'exercice de ses fonctions.

Les chambres d'agriculture peuvent aussi entendre les personnes qu'il leur paraît utile de consulter.
