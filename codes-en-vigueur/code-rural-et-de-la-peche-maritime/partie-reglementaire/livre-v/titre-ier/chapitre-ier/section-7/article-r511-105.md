# Article R511-105

Pour l'application des dispositions de l'article R. 511-17 aux chambres d'agriculture des départements d'outre-mer, les mots : "par les caisses départementales ou pluridépartementales de la mutualité sociale agricole" sont remplacés par les mots : "par les caisses générales de sécurité sociale".
