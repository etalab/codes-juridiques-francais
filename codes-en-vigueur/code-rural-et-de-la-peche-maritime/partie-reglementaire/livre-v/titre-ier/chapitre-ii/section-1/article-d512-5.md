# Article D512-5

Les dispositions des articles L. 511-3 (alinéa 1), L. 511-10, L. 511-11, R. 511-1, R. 511-2, R. 511-3, R. 511-5, R. 511-7, R. 511-51, R. 511-52, R. 511-54 à R. 511-57, R. 511-59, R. 511-60, R. 511-61, R. 511-63 à R. 511-68, R. 511-69 (alinéas 2 et suivants), R. 511-70, R. 511-74 à R. 511-83, R. 511-85, R. 511-91 à R. 511-96 sont applicables aux chambres régionales d'agriculture.

Toutefois, pour l'application de ces dispositions aux chambres régionales d'agriculture, le préfet de région est le commissaire de la République de région.

Pour l'application du premier alinéa, dernière phrase de l'article R. 511-61, la délégation spéciale est choisie parmi les membres des chambres départementales.
