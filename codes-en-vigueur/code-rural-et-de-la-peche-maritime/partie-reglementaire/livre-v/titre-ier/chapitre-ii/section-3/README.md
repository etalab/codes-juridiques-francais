# Section 3 : Dispositions relatives aux chambres interrégionales d'agriculture et aux chambres d'agriculture de région

- [Article D512-12](article-d512-12.md)
- [Article R512-13](article-r512-13.md)
