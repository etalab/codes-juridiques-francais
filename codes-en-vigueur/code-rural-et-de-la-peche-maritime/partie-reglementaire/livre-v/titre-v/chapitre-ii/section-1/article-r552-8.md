# Article R552-8

La circonscription d'un comité économique agricole agréé ne peut être modifiée qu'avec l'approbation du ministre de l'agriculture qui se prononce après instruction de la demande par le préfet et avis du conseil supérieur d'orientation et de coordination de l'économie agricole et alimentaire. L'arrêté du ministre est publié conformément aux dispositions de l'article R. 552-5.
