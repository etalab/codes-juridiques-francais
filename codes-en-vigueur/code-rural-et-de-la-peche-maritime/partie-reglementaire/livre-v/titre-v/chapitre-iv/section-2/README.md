# Section 2 : Procédure d'extension des règles

- [Sous-section 1 : Dispositions générales.](sous-section-1)
- [Sous-section 2 : Procédure de consultation des producteurs](sous-section-2)
