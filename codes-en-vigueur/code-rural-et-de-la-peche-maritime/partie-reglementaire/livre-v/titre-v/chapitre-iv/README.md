# Chapitre IV : Extension des règles édictées par les comités économiques agricoles

- [Section 1 : Catégories de règles pouvant être étendues à l'ensemble des producteurs de la circonscription d'un comité économique agricole.](section-1)
- [Section 2 : Procédure d'extension des règles](section-2)
- [Section 3 : Recherche et constatation des infractions](section-3)
