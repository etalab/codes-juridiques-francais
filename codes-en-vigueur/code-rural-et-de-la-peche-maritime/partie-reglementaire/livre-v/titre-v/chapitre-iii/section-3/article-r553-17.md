# Article R553-17

Le ministre de l'agriculture peut autoriser des dérogations au caractère obligatoire des règles édictées conformément aux dispositions des chapitres Ier et II du présent titre, de la section II du présent chapitre et des articles R. 554-1 à R. 554-6 qui pourraient mettre obstacle aux recherches et essais effectués en vue de la production de végétaux et d'animaux, d'espèces ou de variétés nouvelles.
