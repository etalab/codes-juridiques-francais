# Article D551-24

L'organisation de producteurs dite non commerciale est constituée d'éleveurs et d'un collège associé d'acheteurs comprenant au moins une entreprise d'abattage ou un exportateur, selon les types de production.

Tout adhérent doit s'acquitter du paiement des droits d'inscription ou des cotisations fixées par l'organisation de producteurs.

L'organisation de producteurs dite non commerciale organise la mise en marché des animaux provenant de ses producteurs adhérents auprès des acheteurs adhérents, sans en être propriétaire ni en assurer la vente, dans le cadre d'une commission de mise en marché dont le rôle est d'analyser les prévisions de vente et d'achat et de proposer les modalités d'adaptation de l'offre à la demande.

Elle peut agir comme mandataire pour la commercialisation des produits de ses adhérents en application d'un mandat écrit et non cessible qui est donné par chaque producteur portant sur tout ou partie de sa production. Ce mandat est établi sur la base d'un mandat type qui figure dans le règlement intérieur de l'organisation et comporte au moins les clauses prévues par arrêté du ministre chargé de l'agriculture.

Les dispositions des deux alinéas précédents ne peuvent conduire à un accord collectif sur le prix des produits concernés.
