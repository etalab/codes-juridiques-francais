# Sous-Section 3 : Dispositions particulières aux organisations de producteurs dites non commerciales

- [Article D551-24](article-d551-24.md)
- [Article D551-25](article-d551-25.md)
- [Article D551-26](article-d551-26.md)
- [Article D551-27](article-d551-27.md)
- [Article D551-28](article-d551-28.md)
- [Article D551-29](article-d551-29.md)
