# Article D551-18

Peuvent adhérer, en qualité de producteurs, à une organisation de producteurs dans le secteur de l'élevage bovin et ovin, toutes personnes physiques ou morales se livrant à l'élevage d'animaux bovins ou ovins et qui ne sont pas liées par des contrats d'intégration prévus aux articles L. 326-1 et suivants.

Les exploitations constituées sous une forme sociétaire qui ont pour objet la production des animaux sont considérées comme un seul producteur et comptées pour un adhérent dans le décompte des membres de l'organisation de producteurs.

Lorsqu'une organisation de producteurs regroupe des personnes morales collectant et/ou vendant la production de leurs adhérents, chacun des producteurs associés dans ces personnes morales est pris en compte individuellement dans le décompte des membres de l'organisation de producteurs.

Un adhérent ne peut changer d'organisation de producteurs qu'après avoir été régulièrement libéré de ses engagements statutaires dans sa précédente structure ou en cas de cessation d'activité.
