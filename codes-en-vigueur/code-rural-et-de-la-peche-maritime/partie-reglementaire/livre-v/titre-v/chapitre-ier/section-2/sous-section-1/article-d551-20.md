# Article D551-20

L'organisation de producteurs met en place :

- un encadrement technique de la production, par un personnel qualifié, pour l'adapter aux débouchés, en quantité et en qualité, par des pratiques d'élevage respectueuses de l'environnement ;

- des instruments lui permettant de connaître le cheptel des adhérents et d'établir, au moins une fois par an, un calendrier prévisionnel des sorties adapté à la campagne de commercialisation et mis à jour selon une fréquence appropriée ;

- un dispositif de traitement des informations provenant de ses adhérents afin de déterminer la totalité des volumes à commercialiser, de structurer cette offre par catégorie d'animaux et de la segmenter en vue de sa commercialisation ;

- un dispositif destiné à recueillir systématiquement les informations relatives aux transactions commerciales, notamment les prix de vente des animaux par catégorie afin d'assurer à leurs adhérents, suivant une fréquence appropriée, un retour d'information sur les débouchés des produits et les prix obtenus ; elles informent leurs adhérents des coûts des services rendus dans le cadre de leur activité.

Elle doit également être en mesure d'offrir à chaque producteur qui le souhaite un dispositif de garantie de paiement.
