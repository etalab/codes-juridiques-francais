# Article D551-42

L'organisation de producteurs contrôle les produits de ses membres pour déterminer leurs caractéristiques en vue de leur commercialisation. Pour ce faire, elle dispose de grilles d'agréage et d'un cahier des charges pour chaque produit.

Par dérogation au paragraphe précédent, lorsque la fonction d'agréage est réalisée par le producteur, l'organisation de producteurs, qui en reste responsable, s'assure de sa maîtrise notamment en mettant en place un dispositif contrôlé par elle comprenant la formation et l'information des producteurs, ainsi que la mise en place d'un contrôle physique de second niveau, s'appuyant sur un échantillon représentatif de l'ensemble des opérations d'agréage, réalisé par un agent de l'organisation de producteurs ou par un organisme extérieur.

L'organisation de producteurs contrôle, par échantillonnage le cas échéant, l'agréage réalisé par ses acheteurs ou prestataires. Ce contrôle peut être opéré par un producteur membre de l'organisation.
