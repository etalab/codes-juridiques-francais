# Article D551-47

Les personnes physiques ou morales n'ayant pas la qualité de producteurs peuvent être membres d'une organisation de producteurs dans le secteur des fruits et légumes. Dans ce cas, les statuts de cette organisation de producteurs prévoient que les membres producteurs détiennent au moins 75 % des voix à l'assemblée générale et, lorsque l'organisation de producteurs est constituée sous forme de société, 75 % des parts sociales.

Ces membres non producteurs n'ont pas accès au vote pour les décisions ayant trait aux fonds opérationnels.
