# Article D551-59

I.-Une seule demande de modification des plans de reconnaissance peut être sollicitée par période de douze mois dans des conditions et selon des modalités définies par arrêté du ministre chargé de l'agriculture.

II.-Cette demande est examinée selon la procédure décrite aux articles D. 551-57 et D. 551-58.
