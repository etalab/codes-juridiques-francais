# Article D551-139

Un arrêté du ministre chargé de l'agriculture définit la nature et les modalités de transmission des informations que les organisations de producteurs et les associations d'organisations de producteurs doivent adresser aux autorités compétentes pour l'application de l'article 126 quater du règlement (CE) n° 1234/2007 du 22 octobre 2007 susmentionné et de ses règlements d'exécution.
