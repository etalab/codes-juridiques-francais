# Article D551-7

Une organisation de producteurs précédemment reconnue qui a été l'objet d'une mesure de retrait de reconnaissance peut, après avoir tenu compte des motifs de la mesure prise, former une nouvelle demande de reconnaissance qui sera présentée et instruite suivant la procédure fixée par les articles D. 551-1, D. 551-3 et R. 551-4.
