# Section 1 : Dispositions générales.

- [Article D551-1](article-d551-1.md)
- [Article D551-2](article-d551-2.md)
- [Article D551-3](article-d551-3.md)
- [Article D551-5](article-d551-5.md)
- [Article D551-6](article-d551-6.md)
- [Article D551-7](article-d551-7.md)
- [Article D551-8](article-d551-8.md)
- [Article R551-4](article-r551-4.md)
- [Article R551-9](article-r551-9.md)
- [Article R551-11](article-r551-11.md)
- [Article R551-12](article-r551-12.md)
