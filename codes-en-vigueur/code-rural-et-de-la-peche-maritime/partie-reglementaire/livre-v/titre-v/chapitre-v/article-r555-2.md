# Article R555-2

L'agrément des comités économiques agricoles prévu à l'article L. 552-1 est prononcé, dans les départements ou collectivités mentionnés à l'article R. 555-1, par arrêté conjoint du ministre de l'agriculture et du ministre         chargé de l'outre-mer.
