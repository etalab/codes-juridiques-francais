# Paragraphe 3 : Mobilisation des créances hypothécaires et assimilées.

- [Article R313-20](article-r313-20.md)
- [Article R313-21](article-r313-21.md)
- [Article R313-22](article-r313-22.md)
- [Article R313-24](article-r313-24.md)
- [Article R313-25](article-r313-25.md)
- [Article R313-25-1](article-r313-25-1.md)
