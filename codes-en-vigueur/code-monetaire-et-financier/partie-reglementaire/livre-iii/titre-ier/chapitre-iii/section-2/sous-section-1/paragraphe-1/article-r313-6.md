# Article R313-6

Toute modification affectant les renseignements mentionnés à l'article R. 313-3 est publiée en marge de l'inscription existante au registre mentionné à l'article R. 313-4.

Dans le cas où cette modification implique un changement du tribunal territorialement compétent, l'entreprise de crédit-bail doit en outre faire reporter l'inscription modifiée sur le registre du greffe du nouveau tribunal.
