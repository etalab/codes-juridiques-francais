# Paragraphe 1 : Publicité des opérations de crédit-bail en matière mobilière.

- [Article R313-4](article-r313-4.md)
- [Article R313-5](article-r313-5.md)
- [Article R313-6](article-r313-6.md)
- [Article R313-7](article-r313-7.md)
- [Article R313-8](article-r313-8.md)
- [Article R313-9](article-r313-9.md)
- [Article R313-10](article-r313-10.md)
- [Article R313-11](article-r313-11.md)
