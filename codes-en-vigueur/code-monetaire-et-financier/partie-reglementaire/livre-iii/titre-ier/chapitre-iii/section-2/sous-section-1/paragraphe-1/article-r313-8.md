# Article R313-8

Les inscriptions sont radiées, soit sur justification de l'accord des parties, soit en vertu d'une décision juridictionnelle passée en force de chose jugée.
