# Section 2 : Catégories de crédits et opérations assimilées.

- [Sous-section 1 : Crédit-bail.](sous-section-1)
- [Sous-section 2 : Crédits aux entreprises.](sous-section-2)
