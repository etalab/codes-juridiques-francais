# Section 1 : Droit au compte et relations avec le client

- [Sous-section 1 : Dispositions de droit commun.](sous-section-1)
- [Sous-section 2 : Services bancaires de base.](sous-section-2)
- [Sous-section 3 : Droit au compte](sous-section-3)
- [Sous-section 4 : Observatoire de l'inclusion bancaire](sous-section-4)
