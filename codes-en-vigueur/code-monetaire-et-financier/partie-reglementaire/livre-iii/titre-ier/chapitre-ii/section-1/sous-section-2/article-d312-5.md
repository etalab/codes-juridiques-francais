# Article D312-5

Les services bancaires de base mentionnés aux troisième et quatrième alinéas de l'article L. 312-1 comprennent :

1° L'ouverture, la tenue et la clôture du compte ;

2° Un changement d'adresse par an ;

3° La délivrance à la demande de relevés d'identité bancaire ;

4° La domiciliation de virements bancaires ;

5° L'envoi mensuel d'un relevé des opérations effectuées sur le compte ;

6° La réalisation des opérations de caisse ;

7° L'encaissement de chèques et de virements bancaires ;

8° Les dépôts et les retraits d'espèces au guichet de l'organisme teneur de compte ;

9° Les paiements par prélèvement, titre interbancaire de paiement ou virement bancaire ;

10° Des moyens de consultation à distance du solde du compte ;

11° Une carte de paiement dont chaque utilisation est autorisée par l'établissement de crédit qui l'a émise ;

12° Deux formules de chèques de banque par mois ou moyens de paiement équivalents offrant les mêmes services.
