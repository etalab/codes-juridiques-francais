# Paragraphe 2 : Autres opérations

- [Article D712-10-2](article-d712-10-2.md)
- [Article R712-7](article-r712-7.md)
- [Article R712-8](article-r712-8.md)
- [Article R712-9](article-r712-9.md)
- [Article R712-10](article-r712-10.md)
- [Article R712-10-1](article-r712-10-1.md)
