# Article R712-18

En Nouvelle-Calédonie, en Polynésie française et dans les îles Wallis et Futuna, l'Institut d'émission d'outre-mer :

1° Reçoit de la Banque de France les informations qu'elle recueille au titre des dispositions des articles R. 131-26 à R. 131-31 et R. 131-33 à R. 131-37 ;

2° Exerce les missions dévolues à la Banque de France par les articles R. 131-40 à R. 131-42, dans les conditions précisées aux articles R. 712-19 et R. 712-20.
