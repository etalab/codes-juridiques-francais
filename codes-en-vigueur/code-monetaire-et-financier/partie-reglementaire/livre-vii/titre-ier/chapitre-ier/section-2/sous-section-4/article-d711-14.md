# Article D711-14

Les missions de l'institut d'émission des départements d'outre-mer relatives au taux de l'usure sont définies par l'article D. 313-9 du code de la consommation, ci-après reproduit :

Art.D. 313-9.-L'Institut d'émission des départements d'outre-mer est chargé, dans ces départements, d'effectuer les missions confiées à la Banque de France par les articles D. 313-6 et D. 313-7.
