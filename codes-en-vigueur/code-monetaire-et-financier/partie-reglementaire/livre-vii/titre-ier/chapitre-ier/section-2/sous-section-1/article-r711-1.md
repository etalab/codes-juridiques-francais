# Article R711-1

L'Institut d'émission des départements d'outre-mer est doté de la personnalité civile et de l'autonomie financière.
