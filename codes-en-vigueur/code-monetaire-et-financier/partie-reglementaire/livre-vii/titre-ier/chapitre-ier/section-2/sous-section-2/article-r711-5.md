# Article R711-5

Le conseil de surveillance se réunit au moins deux fois par an et aussi souvent qu'il est nécessaire sur convocation de son président. Il doit être réuni à la demande de la moitié de ses membres.

Le conseil ne délibère valablement qu'en la présence effective d'au moins trois membres titulaires ou suppléants avec voix délibérative. Les délibérations sont prises à la majorité des voix des membres présents ou représentés.

Le directeur général de l'institut assiste aux réunions du conseil.

Lorsque le conseil de surveillance délibère par voie de consultation écrite en application du dernier alinéa de l'article L. 711-5, son président recueille, dans un délai qu'il fixe mais qui ne peut être inférieur à deux jours ouvrables, les votes des membres du conseil sur une proposition de décision. Toutefois, si un membre en fait la demande écrite dans ce délai, le président réunit le conseil dans les formes et conditions prévues au deuxième alinéa ci-dessus. Pour que ses résultats puissent être pris en compte, la consultation doit avoir permis de recueillir la moitié au moins des votes des membres du conseil dans le délai fixé par le président. Celui-ci informe, dans les meilleurs délais, les membres du conseil de la décision résultant de cette consultation.
