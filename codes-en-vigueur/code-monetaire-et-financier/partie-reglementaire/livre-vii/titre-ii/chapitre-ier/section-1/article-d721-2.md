# Article D721-2

Les dispositions de l'article D. 721-1 ne s'appliquent pas à l'Institut d'émission des départements d'outre-mer et au Trésor public.
