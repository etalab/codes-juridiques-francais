# Article R742-1

Les articles R. 211-1 à R. 211-8 et R. 213-16 sont applicables en Nouvelle-Calédonie.
