# Article D743-8

Les articles D. 341-1 à D. 341-8 sont applicables en Nouvelle-Calédonie, sous réserve de supprimer à l'article D. 341-4, au premier et au second alinéa, les mots : "et les sociétés de capital-risque".

Pour l'application de l'article D. 341-2 en Nouvelle-Calédonie, au 3°, le membre de phrase : "ou aux 3° à 5° de l'article L. 310-18 du code des assurances" est supprimé.
