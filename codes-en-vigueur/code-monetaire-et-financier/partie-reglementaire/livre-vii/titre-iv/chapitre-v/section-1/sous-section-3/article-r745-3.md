# Article R745-3

L'Agence française de développement mentionnée aux articles R. 513-23 et suivants exerce également ses attributions en faveur de la Nouvelle-Calédonie.
