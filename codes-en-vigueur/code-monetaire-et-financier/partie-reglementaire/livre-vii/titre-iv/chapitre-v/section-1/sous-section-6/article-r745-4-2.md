# Article R745-4-2

I. ― Les articles R. 519-1 à R. 519-11 et R. 519-13 à R. 519-31 sont applicables en Nouvelle-Calédonie, à l'exception du troisième alinéa de l'article R. 519-4 et sous réserve des adaptations prévues au II.

II. ― 1° Au troisième alinéa de l'article R. 519-7, les mots : " au titre du troisième alinéa de l'article L. 311-8 du code de la consommation ” sont supprimés ;

2° Au 1° de l'article R. 519-8, les mots : " d'un diplôme sanctionnant des études supérieures d'un niveau de formation II ” sont remplacés par les mots : " d'une certification professionnelle de niveau II enregistrée au répertoire national des certifications professionnelles ou au registre de la certification professionnelle de la Nouvelle-Calédonie ”.

3° Pour l'application des articles R. 519-8 et R. 519-9, les mots : " dans les conditions prévues à l'article R. 519-12 ” sont supprimés ;

4° Au 1° des articles R. 519-9 et R. 519-10, les mots : " d'un diplôme sanctionnant un premier cycle d'études supérieures d'un niveau de formation III ” sont remplacés par les mots : " d'une certification professionnelle de niveau III enregistrée au répertoire national des certifications professionnelles ou au registre de la certification professionnelle de la Nouvelle-Calédonie ” ;

5° A l'article R. 519-11, après les mots : " certifications professionnelles ”, il est ajouté les mots : " de la Nouvelle-Calédonie ” ;

6° Au b de l'article R. 519-14, les mots : " et livret ” sont supprimés.
