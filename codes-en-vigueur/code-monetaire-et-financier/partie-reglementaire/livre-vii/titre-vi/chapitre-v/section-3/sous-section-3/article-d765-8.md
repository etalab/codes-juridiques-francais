# Article D765-8

Les articles D. 533-2-1, D. 533-3 à D. 533-7, D. 533-11 à D. 533-14 sont applicables dans les îles Wallis et Futuna.
