# Article R765-9-4

Les articles R. 548-2 à R. 548-10 et l'article R. 571-3 sont applicables dans les îles Wallis et Futuna, sous la réserve suivante :

Pour l'application du a du 1° de l'article R. 548-7, la référence au numéro SIREN est remplacée par la référence au numéro du répertoire des entreprises applicable localement.
