# Article R766-2

I.-Le chapitre II du titre Ier du livre VI à l'exception de l'article D. 612-23 est applicable dans les îles Wallis et Futuna, sous réserve des adaptations prévues au II.

II.-Pour son application dans les îles Wallis et Futuna :

1° Le II de l'article R. 612-18 est ainsi rédigé : " Le recouvrement de la contribution, des astreintes et des sanctions prévues aux articles L. 612-20, L. 612-25 et L. 612-39 à L. 612-41 est effectué par un comptable de l'Etat dans les conditions fixées par la convention prévue au III de l'article R. 612-18. " ;

2° A l'article R. 612-20, les 2°, 4° et 5° du I ne sont pas applicables ;

3° Au III de l'article R. 612-24, les mots : " des articles L. 613-20-2 et L. 613-5 " sont remplacés par les mots : " à l'article L. 613-20-2 ".
