# Sous-section 1 : Définitions et règles générales

- [Article D762-1-1](article-d762-1-1.md)
- [Article R762-1](article-r762-1.md)
