# Sous-section 4 : Les placements collectifs

- [Article D762-5](article-d762-5.md)
- [Article R762-4](article-r762-4.md)
- [Article R762-6](article-r762-6.md)
- [Article R762-7](article-r762-7.md)
