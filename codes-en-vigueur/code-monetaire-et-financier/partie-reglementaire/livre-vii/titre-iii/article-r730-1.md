# Article R730-1

Pour son application à Mayotte, les références faites par des dispositions du présent code à des dispositions du code du travail sont remplacées par les références aux dispositions ayant le même effet, applicables localement.
