# Article R752-1

Les articles R. 211-1 à R. 211-8 et R. 211-16 sont applicables en Polynésie française.
