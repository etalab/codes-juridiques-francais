# Article R755-4-2

I.-Les articles R. 519-1 à R. 519-11 et R. 519-13 à R. 519-31 sont applicables en Polynésie française, à l'exception du troisième alinéa de l'article R. 519-4 et sous réserve des adaptations prévues au II.

II.-1° Pour l'application du 1° du I de l'article R. 519-4, la référence à l'immatriculation au registre du commerce et des sociétés est remplacée par des dispositions applicables localement ayant le même objet ;

2° Pour l'application des articles R. 519-7 à R. 519-11 et de l'article R. 519-14, les références à la formation professionnelle sont remplacées par des dispositions applicables localement ayant le même objet ;

3° Au troisième alinéa de l'article R. 519-7, les mots : " au titre du troisième alinéa de l'article L. 311-8 du code de la consommation " sont supprimés ;

4° Pour l'application des articles R. 519-8 et R. 519-9, les mots : " dans les conditions prévues à l'article R. 519-12 " sont supprimés ;

5° Au b de l'article R. 519-14, les mots : " et livret " sont supprimés.
