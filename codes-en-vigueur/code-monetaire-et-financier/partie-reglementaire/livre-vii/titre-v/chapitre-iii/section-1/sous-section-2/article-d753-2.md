# Article D753-2

Les articles D. 312-5, D. 312-6 et R. 312-18 sont applicables en Polynésie française.

Pour l'application de l'article R. 312-18, la référence au code de commerce est remplacée par une référence aux dispositions applicables localement ayant le même objet.
