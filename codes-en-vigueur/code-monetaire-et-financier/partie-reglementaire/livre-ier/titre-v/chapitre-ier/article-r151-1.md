# Article R151-1

Pour l'application du présent titre :

1° Le territoire dénommé "France" s'entend : de la France métropolitaine, de la Guadeloupe, de la Guyane, de la Martinique, de la Réunion, de Mayotte, de Saint-Barthélemy, de Saint-Martin, de Saint-Pierre-et-Miquelon et des îles Wallis et Futuna ainsi que la Principauté de Monaco. Toutefois, pour les besoins statistiques liés à l'établissement de la balance des paiements, les îles Wallis et Futuna sont considérées comme l'étranger ;

2° Sont considérés comme résidents : les personnes physiques ayant leur principal centre d'intérêt en France, les fonctionnaires et autres agents publics français en poste à l'étranger dès leur prise de fonctions, ainsi que les personnes morales françaises ou étrangères pour leurs établissements en France ;

3° Sont considérés comme non-résidents : les personnes physiques ayant leur principal centre d'intérêt à l'étranger, les fonctionnaires et autres agents publics étrangers en poste en France dès leur prise de fonctions, et les personnes morales françaises ou étrangères pour leurs établissements à l'étranger ;

4° Pour les besoins statistiques mentionnés aux articles R. 152-1, R. 152-2, R. 152-3 et R. 152-4, sont considérées comme des investissements directs étrangers en France ou français à l'étranger les opérations par lesquelles des non-résidents ou des résidents acquièrent au moins 10 % du capital ou des droits de vote, ou franchissent le seuil de 10 %, d'une entreprise résidente ou non résidente respectivement. Relèvent aussi de la définition statistique des investissements directs toutes les opérations entre entreprises apparentées, de quelque nature qu'elles soient, telles que prêts, emprunts ou dépôts, ainsi que les investissements immobiliers ;

5° Sont qualifiées d'investissements directs étrangers, pour l'application de l'article R. 152-5 :

a) La création d'une entreprise nouvelle par une entreprise de droit étranger ou une personne physique non résidente ;

b) L'acquisition de tout ou partie d'une branche d'activité d'une entreprise de droit français par une entreprise de droit étranger ou une personne physique non résidente ;

c) Toutes opérations effectuées dans le capital d'une entreprise de droit français par une entreprise de droit étranger ou une personne physique non résidente dès lors que, après l'opération, la somme cumulée du capital ou des droits de vote détenus par des entreprises étrangères ou des personnes physiques non résidentes excède 33,33 % du capital ou des droits de vote de l'entreprise française ;

d) Les mêmes opérations effectuées par une entreprise de droit français dont le capital ou les droits de vote sont détenus à plus de 33,33 % par une ou des entreprises de droit étranger ou une ou des personnes physiques non résidentes ;

6° Sont également qualifiées d'investissements étrangers, pour l'application de l'article R. 152-5, des opérations telles que l'octroi de prêts ou de garanties substantielles ou l'achat de brevets ou de licences, l'acquisition de contrats commerciaux ou l'apport d'assistance technique qui entraînent la prise de contrôle de fait d'une entreprise de droit français par une entreprise de droit étranger ou une personne physique non résidente ;

7° Sont qualifiées d'investissements indirects étrangers, pour l'application de l'article R. 152-5, les opérations effectuées à l'étranger ayant pour effet de modifier le contrôle d'une entreprise non résidente, elle-même détentrice d'une participation ou de droits de vote dans une entreprise de droit français dont le capital ou les droits de vote sont détenus à plus de 33,33 % par une ou des entreprises de droit étranger ou des personnes physiques non résidentes.
