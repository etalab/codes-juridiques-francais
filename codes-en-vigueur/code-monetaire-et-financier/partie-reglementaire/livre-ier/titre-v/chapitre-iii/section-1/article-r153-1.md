# Article R153-1

Constitue un investissement au sens de la présente section le fait pour un investisseur :

1° Soit d'acquérir le contrôle, au sens de l'article L. 233-3 du code de commerce, d'une entreprise dont le siège social est établi en France ;

2° Soit d'acquérir tout ou partie d'une branche d'activité d'une entreprise dont le siège social est établi en France ;

3° Soit de franchir le seuil de 33,33 % de détention du capital ou des droits de vote d'une entreprise dont le siège social est établi en France.
