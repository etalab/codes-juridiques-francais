# Article D112-4

Le montant prévu au troisième alinéa de l'article L. 112-6 relatif à l'achat au détail de métaux ferreux et non ferreux est fixé à 500 €.
