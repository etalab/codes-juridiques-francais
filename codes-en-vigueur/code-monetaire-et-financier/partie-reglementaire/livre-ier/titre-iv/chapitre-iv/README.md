# Chapitre IV : Dispositions diverses

- [Section 1 : Dispositions budgétaires et financières](section-1)
- [Section 2 : Comptabilité de la Banque de France](section-2)
- [Section 3 : Dispositions diverses.](section-3)
