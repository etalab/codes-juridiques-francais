# Article R165-2

Quiconque aura contrevenu à l'obligation de déclaration administrative prévue au premier alinéa de l'article R. 152-5 est passible d'une amende égale au montant maximum applicable aux contraventions de 4e classe.
