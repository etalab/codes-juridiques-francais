# Section 6 bis : Le plan d'épargne en actions destiné au financement des petites et moyennes entreprises et des entreprises de taille intermédiaire

- [Article D221-113-1](article-d221-113-1.md)
- [Article D221-113-2](article-d221-113-2.md)
- [Article D221-113-3](article-d221-113-3.md)
- [Article D221-113-4](article-d221-113-4.md)
- [Article D221-113-5](article-d221-113-5.md)
- [Article D221-113-6](article-d221-113-6.md)
- [Article D221-113-7](article-d221-113-7.md)
