# Article D221-9

Les règles d'emploi des ressources collectées par les établissements distribuant le livret A ou le livret de développement durable et non centralisées en application de l'article L. 221-5 sont précisées par arrêté du ministre chargé de l'économie.
