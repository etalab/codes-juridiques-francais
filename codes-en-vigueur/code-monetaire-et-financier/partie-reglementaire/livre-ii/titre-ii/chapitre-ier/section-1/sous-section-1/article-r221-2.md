# Article R221-2

Le plafond prévu à l'article L. 221-4 est fixé à  22 950 euros pour les personnes physiques et 76 500 euros pour les associations mentionnées au premier alinéa de l'article L. 221-3. La capitalisation des intérêts peut porter le solde du livret A au-delà de ce plafond.

Les organismes d'habitation à loyer modéré sont autorisés à effectuer des dépôts sur leur livret A sans être soumis à un plafond.
