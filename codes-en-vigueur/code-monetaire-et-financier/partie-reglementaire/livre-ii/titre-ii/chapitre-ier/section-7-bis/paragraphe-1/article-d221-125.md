# Article D221-125

Le titulaire du compte apporte à l'établissement habilité à recevoir des dépôts, dans un délai maximal de quarante-cinq jours fin de mois ou soixante jours à compter de leurs dates d'émission, les factures permettant un tel retrait sur le compte et justifiant que les travaux engagés sont conformes aux opérations décrites à l'article D. 221-124.
