# Section 3 : Le livret jeune.

- [Sous-section 1 : Ouverture et clôture du livret jeune.](sous-section-1)
- [Sous-section 2 : Opérations effectuées sur le livret jeune et rémunération.](sous-section-2)
- [Sous-section 3 : Relations entre l'Etat et les établissements ou organismes collecteurs.](sous-section-3)
