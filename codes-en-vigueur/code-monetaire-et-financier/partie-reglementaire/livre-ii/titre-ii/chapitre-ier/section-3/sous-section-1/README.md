# Sous-section 1 : Ouverture et clôture du livret jeune.

- [Article R221-76](article-r221-76.md)
- [Article R221-77](article-r221-77.md)
- [Article R221-78](article-r221-78.md)
- [Article R221-79](article-r221-79.md)
- [Article R221-80](article-r221-80.md)
- [Article R221-81](article-r221-81.md)
- [Article R*221-82](article-r-221-82.md)
