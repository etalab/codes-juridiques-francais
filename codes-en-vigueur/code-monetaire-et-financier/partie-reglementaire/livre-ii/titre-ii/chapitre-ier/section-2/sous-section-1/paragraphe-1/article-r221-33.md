# Article R221-33

Les comptes sur livret d'épargne populaire peuvent être ouverts au nom des personnes physiques qui justifient soit qu'elles remplissent personnellement les conditions fixées par l'article L. 221-15, soit qu'elles sont le conjoint d'un contribuable remplissant ces conditions.

Ils restent ouverts aussi longtemps que leurs titulaires justifient, par la production annuelle du document exigé à l'article R. 221-34, qu'ils continuent à remplir ces conditions.
