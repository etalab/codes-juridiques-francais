# Article R221-47

Aucune opération de retrait ne peut avoir pour effet de rendre le compte débiteur.
