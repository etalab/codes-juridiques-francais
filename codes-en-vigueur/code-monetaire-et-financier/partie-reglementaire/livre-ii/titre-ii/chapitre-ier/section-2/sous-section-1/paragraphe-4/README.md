# Paragraphe 4 : Dispositions relatives aux relations entre l'Etat, la Caisse des dépôts et consignations et les établissements ou organismes collecteurs.

- [Article R221-61](article-r221-61.md)
- [Article R221-62](article-r221-62.md)
- [Article R221-64](article-r221-64.md)
