# Paragraphe 1 : Procédure de commercialisation de FIA.

- [Article D214-32](article-d214-32.md)
- [Article D214-32-1](article-d214-32-1.md)
- [Article D214-32-2](article-d214-32-2.md)
- [Article D214-32-3](article-d214-32-3.md)
- [Article D214-32-4](article-d214-32-4.md)
- [Article D214-32-4-1](article-d214-32-4-1.md)
