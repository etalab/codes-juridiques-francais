# Article R214-233

Le passif d'un fonds commun de titrisation comprend à tout moment un nombre minimal de deux parts.
