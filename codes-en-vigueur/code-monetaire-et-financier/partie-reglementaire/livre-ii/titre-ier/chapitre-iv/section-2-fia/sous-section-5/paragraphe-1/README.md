# Paragraphe 1 : Dispositions communes aux organismes de titrisation.

- [Sous-paragraphe 1 : Règlement ou statuts de l'organisme de titrisation.](sous-paragraphe-1)
- [Sous-paragraphe 2 : Règles générales de composition de l'actif et du passif de l'organisme de titrisation.](sous-paragraphe-2)
- [Sous-paragraphe 3 : Règles applicables aux instruments financiers à terme et à la cession de créances avant leur terme.](sous-paragraphe-3)
- [Sous-paragraphe 4 : Règles applicables à la cession et au recouvrement des créances ainsi qu'à la conservation des actifs.](sous-paragraphe-4)
- [Sous-paragraphe 5 : Obligations d'information.](sous-paragraphe-5)
- [Sous-paragraphe 6 : Dispositions particulières aux organismes de titrisation à compartiments.](sous-paragraphe-6)
