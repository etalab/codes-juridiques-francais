# Sous-paragraphe 2 : Organismes professionnels de placement collectif immobilier.

- [Article D214-195](article-d214-195.md)
- [Article R214-194](article-r214-194.md)
- [Article R214-196](article-r214-196.md)
- [Article R214-197](article-r214-197.md)
- [Article R214-198](article-r214-198.md)
- [Article R214-199](article-r214-199.md)
- [Article R214-200](article-r214-200.md)
- [Article R214-201](article-r214-201.md)
