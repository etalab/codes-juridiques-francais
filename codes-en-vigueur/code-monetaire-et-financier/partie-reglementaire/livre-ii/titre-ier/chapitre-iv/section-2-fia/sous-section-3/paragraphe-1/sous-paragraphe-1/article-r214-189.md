# Article R214-189

Les fonds professionnels à vocation générale ne sont pas pris en compte pour l'application du I de l'article R. 214-32-35.
