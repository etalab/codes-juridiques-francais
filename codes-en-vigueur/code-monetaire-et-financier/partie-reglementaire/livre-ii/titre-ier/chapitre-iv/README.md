# Chapitre IV : Placements collectifs.

- [Section 1 : OPCVM.](section-1)
- [Section 2 : FIA.](section-2-fia)
- [Section 3 : Autres placements collectifs.](section-3)
- [Article D214-0](article-d214-0.md)
