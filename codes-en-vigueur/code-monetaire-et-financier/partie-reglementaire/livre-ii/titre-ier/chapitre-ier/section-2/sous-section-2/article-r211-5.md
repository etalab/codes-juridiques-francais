# Article R211-5

Les titres financiers à forme obligatoirement nominative ne peuvent être négociés sur un marché réglementé ou sur un système multilatéral de négociation qu'après avoir été placés en compte d'administration.

Les titres financiers qui ne revêtent pas la forme obligatoirement nominative ne peuvent être négociés sur un marché réglementé ou sur un système multilatéral de négociation que sous la forme au porteur.
