# Article D213-14

Les émetteurs de titres de créance négociables communiquent à la Banque de France des informations statistiques sur leurs titres, dans les conditions définies par les arrêtés mentionnés à l'article D. 213-7.

La Banque de France assure régulièrement la diffusion de ces informations.

Les émetteurs de titres de créance négociables rendent compte à la Banque de France des remboursements anticipés de leurs titres. La Banque de France fixe la fréquence de la fourniture de ces informations.
