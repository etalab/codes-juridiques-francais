# Article D213-6

Les titres de créance négociables peuvent être émis en toute devise étrangère. La Banque de France peut toutefois décider de la suspension temporaire des émissions de titres libellés dans une devise déterminée si les circonstances le justifient.
