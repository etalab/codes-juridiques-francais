# Article R550-3

Le commissaire aux comptes mentionné à l'article L. 550-2 est désigné par ordonnance sur requête du président du tribunal de commerce du lieu du domicile ou du siège social du gestionnaire, après avis de l'Autorité des marchés financiers. Ce tribunal est compétent pour relever les commissaires aux comptes de leurs fonctions dans les cas prévus à l'article L. 550-5.

Pour l'exercice de sa mission, le commissaire aux comptes est soumis aux dispositions des articles R. 821-1 à R. 823-21 du code de commerce.
