# Paragraphe 2 : Fonctionnement.

- [Article R512-9](article-r512-9.md)
- [Article R512-10](article-r512-10.md)
- [Article R512-11](article-r512-11.md)
- [Article R512-12](article-r512-12.md)
- [Article R512-13](article-r512-13.md)
