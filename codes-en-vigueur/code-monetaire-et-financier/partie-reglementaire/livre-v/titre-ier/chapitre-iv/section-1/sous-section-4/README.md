# Sous-section 4 : Ventes aux enchères.

- [Article D514-16](article-d514-16.md)
- [Article D514-17](article-d514-17.md)
- [Article D514-18](article-d514-18.md)
- [Article D514-19](article-d514-19.md)
- [Article D514-20](article-d514-20.md)
