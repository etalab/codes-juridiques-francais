# Article R518-22

Le directeur général de la Caisse des dépôts et consignations fixe, après avis de la commission de surveillance, les modalités de fonctionnement des contrôles mentionnés à l'article R. 518-19.
