# Article R518-18

Tous les mois, le caissier général communique au chef de la comptabilité, pour être vérifiés, les relevés des recettes et des dépenses en numéraire et des entrées et sorties de valeurs du mois précédent.

La situation de sa caisse est vérifiée par le directeur général au moins une fois par mois, indépendamment des vérifications que la commission de surveillance peut faire toutes les fois qu'elle le juge utile.
