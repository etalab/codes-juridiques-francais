# Article R518-5

Les nominations aux emplois de chef de service, de directeur adjoint, de sous-directeur et d'expert de haut niveau ou directeur de projet sont prononcées par arrêté conjoint du Premier ministre et du ministre chargé de l'économie pris sur la proposition du directeur général après avis du ministre chargé de la fonction publique.

Peuvent être nommés aux emplois de chef de service, directeur adjoint et sous-directeur les fonctionnaires appartenant au corps des administrateurs civils ainsi que, dans les proportions fixées à l'article 2 du décret du 19 septembre 1955 relatif aux conditions de nomination et d'avancement dans les emplois de chef de service, de directeur adjoint et de sous-directeur des administrations centrales de l'Etat, les autres fonctionnaires de catégorie A de la fonction publique de l'Etat, de la fonction publique territoriale ou de la fonction publique hospitalière répondant aux conditions posées aux a et b du même article. Ils doivent remplir les conditions fixées à l'article 3 du décret du 19 septembre 1955 susmentionné.

Seuls peuvent bénéficier d'une nomination en qualité d'expert de haut niveau ou directeur de projet les fonctionnaires qui remplissent les conditions fixées à l'article 3 du décret n° 2000-449 du 23 mai 2000 modifié relatif aux emplois de expert de haut niveau ou directeur de projet.
