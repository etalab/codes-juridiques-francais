# Article R518-7

Les directeurs, les contrôleurs généraux, chefs de service et directeurs adjoints prêtent serment devant la commission de surveillance.
