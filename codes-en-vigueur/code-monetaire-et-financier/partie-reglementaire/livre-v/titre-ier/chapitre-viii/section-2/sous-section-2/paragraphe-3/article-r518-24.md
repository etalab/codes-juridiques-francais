# Article R518-24

Les comptables publics de l'Etat mentionnés à l'article L. 518-14 sont des comptables de la direction générale des finances publiques.

Lorsqu'ils traitent les consignations et les dépôts des clientèles dont le compte est ouvert dans les livres de la Caisse des dépôts et consignations, ces comptables sont ses préposés.
