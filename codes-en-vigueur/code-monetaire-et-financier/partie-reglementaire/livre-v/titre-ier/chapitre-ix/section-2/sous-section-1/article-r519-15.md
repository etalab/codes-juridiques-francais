# Article R519-15

Toute personne mentionnée au I de l'article R. 519-4 veille à ce que ses salariés qui exercent l'activité d'intermédiation en opérations de banque et en services de paiement remplissent les conditions de compétence professionnelle prévues aux articles R. 519-8, R. 519-9 et R. 519-10 et qui lui sont applicables à elle-même.
