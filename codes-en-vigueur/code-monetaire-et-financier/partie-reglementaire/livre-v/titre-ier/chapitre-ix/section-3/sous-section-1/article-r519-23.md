# Article R519-23

Toute information fournie par l'intermédiaire en opérations de banque et en services de paiement en application de la présente section est communiquée avec clarté et exactitude. La communication est faite sur support durable à la disposition du client, y compris du client potentiel, et auquel celui-ci a facilement accès.

En cas de commercialisation d'un contrat à distance, les informations précontractuelles fournies au client, y compris au client potentiel, en sus de celles indiquées aux articles R. 519-25 et R. 519-26, sont conformes aux dispositions de l'article L. 121-20-8 à L. 121-20-16 du code de la consommation.
