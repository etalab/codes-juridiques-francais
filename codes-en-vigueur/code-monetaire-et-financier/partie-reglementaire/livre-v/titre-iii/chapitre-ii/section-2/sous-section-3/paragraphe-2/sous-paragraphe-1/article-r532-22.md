# Article R532-22

Lorsque l'Autorité des marchés financiers, s'agissant de l'exercice du service mentionné au 4° de l'article L. 321-1, refuse que soient transmises à l'autorité compétente de l'Etat d'accueil mentionné au 1° de l'article R. 532-20, qui a été désignée comme point de contact, les éléments d'information mentionnés à l'article R. 532-21, elle doit faire connaître les raisons de ce refus à l'Autorité de contrôle prudentiel et de résolution ainsi qu'au prestataire concerné dans le délai de trois mois prévu à l'article R. 532-21.

Lorsque l'Autorité de contrôle prudentiel et de résolution refuse de transmettre les informations mentionnées à l'article R. 532-21, il doit en informer l'Autorité des marchés financiers ainsi que l'entreprise concernée dans les conditions et le délai mentionnés à l'alinéa précédent.
