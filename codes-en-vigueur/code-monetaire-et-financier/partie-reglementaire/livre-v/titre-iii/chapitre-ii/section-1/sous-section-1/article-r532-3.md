# Article R532-3

Dès réception d'une demande, l'Autorité de contrôle prudentiel et de résolution vérifie qu'elle est conforme au dossier type prévu au deuxième alinéa de l'article R. 532-1 et, dans l'affirmative, procède à son instruction.

L'Autorité de contrôle prudentiel et de résolution communique à l'Autorité des marchés financiers le dossier dans un délai de cinq jours ouvrés à compter de la date de réception de la demande. L'Autorité de contrôle prudentiel et de résolution, à sa propre initiative ou sur demande de l'Autorité des marchés financiers, demander au requérant tous éléments d'information complémentaires nécessaires pour l'instruction du dossier. Le délai imparti à ces autorités est suspendu jusqu'à réception des éléments complémentaires.

Dans le cas où la demande comprend les services mentionnés aux 4 ou 5 de l'article L. 321-1, l'Autorité des marchés financiers peut également demander au requérant tous éléments d'information complémentaires nécessaires pour l'instruction du programme d'activité. Le délai qui lui est imparti est suspendu jusqu'à réception des éléments complémentaires.
