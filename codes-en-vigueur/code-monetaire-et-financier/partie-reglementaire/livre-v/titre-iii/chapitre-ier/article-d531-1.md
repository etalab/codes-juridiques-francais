# Article D531-1

Les contrats à terme sur marchandises et autres contrats à terme mentionnés au j du 2° de l'article L. 531-2 sont ceux cités aux 2, 3, 4, 7 et 8 du I de l'article D. 211-1 A.
