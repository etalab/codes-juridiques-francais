# Article R533-18-1

Lorsqu'elles disposent d'un site internet, les entreprises d'investissement autres que les sociétés de gestion de portefeuille y présentent les dispositifs mis en œuvre pour assurer le respect des exigences prévues par l'article L. 511-45, par la présente section, ainsi que par les dispositions réglementaires prises pour leur application.
