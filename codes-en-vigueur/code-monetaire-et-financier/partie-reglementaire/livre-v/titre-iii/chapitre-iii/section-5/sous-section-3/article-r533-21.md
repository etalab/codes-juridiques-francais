# Article R533-21

Les entreprises d'investissement autres que les sociétés de gestion de portefeuille sont tenues aux obligations prévues aux articles R. 511-20 à R. 511-25.
