# Section 3 : Règles de bonne conduite.

- [Sous-section 1 : Dispositions applicables aux membres du personnel des entreprises d'investissement](sous-section-1)
- [Sous-section 2 : Clients professionnels](sous-section-2)
- [Sous-section 3 : Contreparties éligibles](sous-section-3)
- [Sous-Section 4 : Conventions entre producteurs   et distributeurs d'instruments financiers](sous-section-4)
- [Sous-Section 5 : Modalités de prise en compte des critères relatifs au respect d'objectifs sociaux, environnementaux et de qualité de gouvernance](sous-section-5)
