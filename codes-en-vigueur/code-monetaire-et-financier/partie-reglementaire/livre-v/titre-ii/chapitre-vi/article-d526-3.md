# Article D526-3

<div align="left">Le montant prévu au quatrième alinéa de l'article L. 526-19 est fixé à 250 euros.</div>
