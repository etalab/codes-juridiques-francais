# Article D525-2

<div align="left">L'Autorité de contrôle prudentiel et de résolution effectue la notification prévue au deuxième alinéa de l'article L. 525-6 dans un délai de trois mois.<br/>
</div>
