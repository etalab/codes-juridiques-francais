# Section 1 : Dispositions concernant la surveillance, les contrôles et les enquêtes

- [Sous-section 1 : Coopération et échanges d'informations avec les autorités d'autres Etats membres de l'Union européenne ou d'autres Etats parties à l'accord sur l'Espace économique européen](sous-section-1)
- [Sous-section 2 : Coopération et échanges d'informations avec les autorités des Etats non membres de l'Union européenne et non parties à l'accord sur l'Espace économique européen](sous-section-2)
