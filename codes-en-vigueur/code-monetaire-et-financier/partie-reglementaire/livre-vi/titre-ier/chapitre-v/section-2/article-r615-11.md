# Article R615-11

Le secrétariat du Comité de la médiation bancaire est assuré par la Banque de France.
