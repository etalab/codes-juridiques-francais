# Section 3 : Moyens de fonctionnement

- [Article R612-10](article-r612-10.md)
- [Article R612-11](article-r612-11.md)
- [Article R612-12](article-r612-12.md)
- [Article R612-13](article-r612-13.md)
- [Article R612-14](article-r612-14.md)
- [Article R612-15](article-r612-15.md)
- [Article R612-16](article-r612-16.md)
- [Article R612-17](article-r612-17.md)
- [Article R612-18](article-r612-18.md)
- [Article R612-19](article-r612-19.md)
