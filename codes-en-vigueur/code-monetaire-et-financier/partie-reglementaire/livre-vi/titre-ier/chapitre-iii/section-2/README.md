# Section 2 : Dispositions relatives au traitement des établissements de crédit, des sociétés de financement, des établissements de monnaie électronique, des établissements de paiement et des entreprises d'investissement en difficulté

- [Sous-section 1 : Mesures spécifiques à la sauvegarde, au redressement ou à la liquidation judiciaire des établissements de crédit, des sociétés de financement, des établissements de monnaie électronique, des établissements de paiement et des entreprises d'investissement](sous-section-1)
- [Sous-section 2 : Mesures d'assainissement et de liquidation des établissements de crédit communautaires](sous-section-2)
- [Sous-section 3 :  Dispositions relatives à la résolution des crises bancaires](sous-section-3)
