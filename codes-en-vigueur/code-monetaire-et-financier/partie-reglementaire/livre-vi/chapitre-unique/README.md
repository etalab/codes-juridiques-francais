# Chapitre unique : Dispositions applicables aux autorités  compétentes en matière de réglementation et de contrôle

- [Article R641-1](article-r641-1.md)
- [Article R641-2](article-r641-2.md)
- [Article R641-3](article-r641-3.md)
