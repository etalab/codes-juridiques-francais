# Sous-section 7 : Dispositions diverses

- [Article L513-25](article-l513-25.md)
- [Article L513-26](article-l513-26.md)
- [Article L513-27](article-l513-27.md)
