# Article L513-22

L'Autorité de contrôle prudentiel veille au respect par les sociétés de crédit foncier des obligations leur incombant en application de la présente section et sanctionne, dans les conditions prévues au chapitre II et aux sections 1 et 2 du chapitre III du titre Ier du livre VI, les manquements constatés.
