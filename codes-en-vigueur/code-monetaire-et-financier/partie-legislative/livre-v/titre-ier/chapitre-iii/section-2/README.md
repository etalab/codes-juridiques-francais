# Section 2 : Les sociétés de crédit foncier

- [Sous-section 1 : Statut et objet](sous-section-1)
- [Sous-section 2 : Opérations](sous-section-2)
- [Sous-section 3 : Privilège des créances nées des opérations](sous-section-3)
- [Sous-section 4 : Règles régissant les opérations des sociétés de crédit foncier](sous-section-4)
- [Sous-section 5 : Sauvegarde, redressement et liquidation judiciaires](sous-section-5)
- [Sous-section 6 : Contrôles](sous-section-6)
- [Sous-section 7 : Dispositions diverses](sous-section-7)
