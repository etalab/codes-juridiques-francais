# Sous-section 4 : Règles régissant les opérations des sociétés de crédit foncier

- [Article L513-12](article-l513-12.md)
- [Article L513-13](article-l513-13.md)
- [Article L513-14](article-l513-14.md)
- [Article L513-15](article-l513-15.md)
- [Article L513-16](article-l513-16.md)
- [Article L513-17](article-l513-17.md)
