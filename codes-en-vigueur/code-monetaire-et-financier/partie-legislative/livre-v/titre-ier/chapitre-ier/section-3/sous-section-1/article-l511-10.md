# Article L511-10

Avant d'exercer leur activité, les établissements de crédit et les sociétés de financement doivent obtenir un agrément. En application des articles 4 et 14 du règlement (UE) n° 1024/2013 du Conseil du 15 octobre 2013, l'agrément d'établissement de crédit est délivré par la Banque centrale européenne, sur proposition de l'Autorité de contrôle prudentiel et de résolution. L'agrément de société de financement est délivré par l'Autorité de contrôle prudentiel et de résolution en application du 1° du II de l'article L. 612-1.

L'Autorité de contrôle prudentiel et de résolution vérifie si l'entreprise satisfait aux obligations prévues aux articles L. 511-11, L. 511-13, L. 515-1-1 ou 93 du règlement (UE) n° 575/2013 du Parlement européen et du Conseil du 26 juin 2013 et l'adéquation de la forme juridique de l'entreprise à l'activité d'établissement de crédit ou de société de financement, selon les cas. Elle prend en compte le programme d'activités de cette entreprise, son organisation, les moyens techniques et financiers qu'elle prévoit de mettre en œuvre ainsi que, dans les conditions définies par arrêté du ministre chargé de l'économie, l'identité des apporteurs de capitaux et le montant de leur participation.

L'Autorité apprécie également l'aptitude de l'entreprise requérante à réaliser ses objectifs de développement dans des conditions compatibles avec le bon fonctionnement du système bancaire et qui assurent à la clientèle une sécurité satisfaisante.

Pour fixer les conditions de l'agrément, l'Autorité de contrôle prudentiel et de résolution peut prendre en compte la spécificité de certains établissements de crédit ou sociétés de financement appartenant au secteur de l'économie sociale et solidaire. Elle apprécie notamment l'intérêt de leur action au regard des missions d'intérêt général relevant de la lutte contre les exclusions ou de la reconnaissance effective d'un droit au crédit.

L'Autorité peut, selon les cas, limiter ou proposer à la Banque centrale européenne de limiter l'agrément à l'exercice de certaines opérations définies par l'objet social du demandeur.

L'Autorité peut, selon les cas, assortir ou proposer à la Banque centrale européenne d'assortir l'agrément de conditions particulières visant à préserver l'équilibre de la structure financière de l'entreprise et le bon fonctionnement du système bancaire en tenant compte, le cas échéant, des objectifs de la surveillance complémentaire prévue par le chapitre VII du titre Ier du livre V du présent code. Elle peut aussi subordonner ou proposer à la Banque centrale européenne de subordonner l'octroi de l'agrément au respect d'engagements souscrits par l'entreprise requérante.

L'Autorité refuse l'agrément lorsque l'exercice de la mission de surveillance de l'entreprise requérante est susceptible d'être entravé soit par l'existence de liens de capital ou de contrôle directs ou indirects entre l'entreprise et d'autres personnes physiques ou morales, soit par l'existence de dispositions législatives ou réglementaires d'un Etat qui n'est pas partie à l'accord sur l'Espace économique européen et dont relèvent une ou plusieurs de ces personnes.

L'Autorité refuse l'agrément si les dispositions des articles L. 511-51 et L. 511-52 ne sont pas respectées.

L'Autorité refuse l'agrément s'il existe, au regard des critères d'appréciation prévus au I de l'article L. 511-12-1, des motifs raisonnables de penser que la qualité des apporteurs de capitaux ne permet pas de garantir une gestion saine et prudente ou si les informations communiquées sont incomplètes.

L'établissement de crédit ou la société de financement doit satisfaire à tout moment aux conditions de son agrément.
