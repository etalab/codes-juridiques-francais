# Article L511-13

L'administration centrale de tout établissement de crédit ou société de financement soumis au présent agrément doit être située sur le même territoire national que son siège statutaire.

La direction effective de l'activité des établissements de crédit ou des sociétés de financement doit être assurée par deux personnes au moins.

Les établissements de crédit dont le siège social est à l'étranger désignent deux personnes au moins auxquelles ils confient la direction effective de l'activité de leur succursale en France.
