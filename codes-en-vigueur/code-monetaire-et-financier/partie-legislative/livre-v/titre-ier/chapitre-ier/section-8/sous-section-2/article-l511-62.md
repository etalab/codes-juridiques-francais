# Article L511-62

En vue de lui permettre d'assurer la mission prévue à l'article L. 511-60, le conseil d'administration, le conseil de surveillance ou tout autre organe exerçant des fonctions de surveillance équivalentes est informé, par les personnes mentionnées à l'article L. 511-13, de l'ensemble des risques significatifs, des politiques de gestion des risques et des modifications apportées à celles-ci.
