# Paragraphe 1 : Dispositions communes

- [Article L511-89](article-l511-89.md)
- [Article L511-90](article-l511-90.md)
- [Article L511-91](article-l511-91.md)
