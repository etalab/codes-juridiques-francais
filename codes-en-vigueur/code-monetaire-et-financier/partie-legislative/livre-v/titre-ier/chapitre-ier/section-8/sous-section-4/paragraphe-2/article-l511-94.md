# Article L511-94

Le comité des risques examine, dans le cadre de sa mission, si les prix des produits et services mentionnés aux livres II et III proposés aux clients sont compatibles avec la stratégie en matière de risques de l'établissement de crédit ou de la société de financement.

Lorsque ces prix ne reflètent pas correctement les risques, il présente au conseil d'administration, au conseil de surveillance ou à tout autre organe exerçant des fonctions de surveillance équivalentes un plan d'action pour y remédier.
