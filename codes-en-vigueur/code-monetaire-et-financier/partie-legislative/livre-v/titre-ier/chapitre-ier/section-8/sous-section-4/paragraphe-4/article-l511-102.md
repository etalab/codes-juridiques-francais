# Article L511-102

Le comité des rémunérations prépare les décisions que le conseil d'administration, le conseil de surveillance ou tout autre organe exerçant des fonctions de surveillance équivalentes arrête concernant les rémunérations, notamment celles qui ont une incidence sur le risque et la gestion des risques dans l'établissement de crédit ou la société de financement.

Ce comité ou, à défaut, le conseil d'administration, le conseil de surveillance ou tout autre organe exerçant des fonctions de surveillance équivalentes procède à un examen annuel :

1° Des principes de la politique de rémunération de l'entreprise ;

2° Des rémunérations, indemnités et avantages de toute nature accordés aux mandataires sociaux de l'entreprise ;

3° De la politique de rémunération des salariés qui gèrent des OPCVM, des FIA relevant des paragraphes 1,2,3,5 et 6 de la sous-section 2, des sous-sections 3,4 et 5 de la section 2 du chapitre IV du titre Ier du livre II et des catégories de personnel, incluant les personnes mentionnées à l'article L. 511-13, les preneurs de risques, les personnes exerçant une fonction de contrôle ainsi que tout salarié qui, au vu de ses revenus globaux, se trouve dans la même tranche de rémunération, dont les activités professionnelles ont une incidence significative sur le profil de risque de l'entreprise ou du groupe.

Ce comité ou, à défaut, le conseil d'administration, le conseil de surveillance ou tout autre organe exerçant des fonctions de surveillance équivalentes contrôle directement la rémunération du responsable de la fonction de gestion des risques mentionné à l'article L. 511-64 et, le cas échéant, du responsable de la conformité.

Le comité peut être assisté par les services de contrôle interne ou des experts extérieurs. Il rend régulièrement compte de ses travaux au conseil d'administration, au conseil de surveillance ou à tout autre organe exerçant des fonctions de surveillance équivalentes.

Les établissements de crédit et les sociétés de financement intègrent dans le rapport présenté à l'assemblée générale les informations relatives à la politique et aux pratiques de rémunération fixées par arrêté du ministre chargé de l'économie.

Le conseil d'administration, le conseil de surveillance ou tout autre organe exerçant des fonctions de surveillance équivalentes des établissements de crédit et des sociétés de financement faisant partie d'un groupe peut décider d'appliquer la politique de rémunération de l'entreprise qui le contrôle au sens de l'article L. 233-16 du code de commerce.

Les dispositions du présent article s'appliquent aux sociétés de capital-risque mentionnées à l'article 1-1 de la loi n° 85-695 du 11 juillet 1985 portant diverses dispositions d'ordre économique et financier.
