# Article L511-93

Le comité des risques conseille le conseil d'administration, le conseil de surveillance ou tout autre organe exerçant des fonctions de surveillance équivalentes sur la stratégie globale de l'établissement de crédit ou de la société de financement et l'appétence en matière de risques, tant actuels que futurs.

Il assiste le conseil d'administration, le conseil de surveillance ou tout autre organe exerçant des fonctions de surveillance équivalentes lorsque celui-ci contrôle la mise en œuvre de cette stratégie par les personnes mentionnées à l'article L. 511-13 et par le responsable de la fonction de gestion des risques.
