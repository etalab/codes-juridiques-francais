# Article L511-89

Au sein des établissements de crédit et des sociétés de financement d'importance significative au regard de leur taille et de leur organisation interne ainsi que de la nature, de l'échelle et de la complexité de leurs activités, le conseil d'administration, le conseil de surveillance ou tout autre organe exerçant des fonctions de surveillance équivalentes constitue un comité des risques, un comité des nominations et un comité des rémunérations.

Les critères des établissements d'importance significative selon lesquels sont créés les comités sont précisés par arrêté du ministre chargé de l'économie.
