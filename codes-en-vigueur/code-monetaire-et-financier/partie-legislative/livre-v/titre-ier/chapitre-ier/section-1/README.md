# Section 1 : Définitions et activités

- [Article L511-1](article-l511-1.md)
- [Article L511-2](article-l511-2.md)
- [Article L511-3](article-l511-3.md)
- [Article L511-4](article-l511-4.md)
- [Article L511-4-1](article-l511-4-1.md)
- [Article L511-4-2](article-l511-4-2.md)
