# Paragraphe 3 : Ressources

- [Article L512-44](article-l512-44.md)
- [Article L512-45](article-l512-45.md)
- [Article L512-46](article-l512-46.md)
