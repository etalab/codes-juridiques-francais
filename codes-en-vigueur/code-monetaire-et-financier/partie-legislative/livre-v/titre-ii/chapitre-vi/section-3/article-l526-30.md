# Article L526-30

Les établissements de monnaie électronique sont tenus de respecter les articles L. 522-14 à L. 522-18 lorsqu'ils fournissent des services de paiement, au sens du 1° de l'article L. 526-2.
