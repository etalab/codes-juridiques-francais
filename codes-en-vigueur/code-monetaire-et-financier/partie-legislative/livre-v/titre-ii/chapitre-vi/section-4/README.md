# Section 4 : Secret professionnel, comptabilité et contrôle légal des comptes

- [Article L526-35](article-l526-35.md)
- [Article L526-36](article-l526-36.md)
- [Article L526-37](article-l526-37.md)
- [Article L526-38](article-l526-38.md)
- [Article L526-39](article-l526-39.md)
- [Article L526-40](article-l526-40.md)
