# Chapitre III : Dispositions relatives aux prestataires de services d'investissement , aux conseillers en investissements financiers, aux conseillers en investissements participatifs et aux intermédiaires en financement participatif

- [Section 1 : Dispositions relatives aux prestataires de services d'investissement](section-1)
- [Section 2 : Dispositions relatives aux conseillers en investissements financiers](section-2)
- [Section 3 : Dispositions relatives aux conseillers   en investissements participatifs](section-3)
- [Section 4 : Dispositions relatives aux intermédiaires en financement participatif](section-4)
