# Article L533-15

Les prestataires de services d'investissement rendent compte à leurs clients des services fournis à ceux-ci. Le compte rendu inclut, lorsqu'il y a lieu, les coûts liés aux transactions effectuées et aux services fournis pour le compte du client.
