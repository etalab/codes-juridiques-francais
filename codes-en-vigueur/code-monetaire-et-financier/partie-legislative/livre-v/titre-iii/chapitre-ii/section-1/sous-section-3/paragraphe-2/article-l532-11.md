# Article L532-11

Toute société de gestion de portefeuille ayant décidé sa dissolution anticipée avant le terme de cette période demeure soumise, jusqu'à la clôture de sa liquidation, au contrôle de l'Autorité des marchés financiers qui peut prononcer les sanctions prévues à l'article L. 621-15, y compris la radiation. Elle ne peut faire état de sa qualité de société de gestion de portefeuille qu'en précisant qu'elle est en liquidation.
