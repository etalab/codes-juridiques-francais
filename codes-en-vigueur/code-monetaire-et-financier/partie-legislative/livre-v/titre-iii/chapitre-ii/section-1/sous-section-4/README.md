# Sous-section 4 : Bureaux de représentation

- [Article L532-14](article-l532-14.md)
- [Article L532-15](article-l532-15.md)
