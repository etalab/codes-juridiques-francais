# Article L532-3-1

Toute modification des conditions auxquelles était subordonné l'agrément délivré à une entreprise d'investissement ou à un établissement de crédit fournissant un ou plusieurs services d'investissement doit faire l'objet, selon les cas, d'une autorisation préalable de l'Autorité de contrôle prudentiel et de résolution, d'une déclaration ou d'une notification, dans les conditions fixées par un arrêté du ministre chargé de l'économie.

Dans les cas où une autorisation doit être délivrée, elle peut, elle-même, être assortie de conditions particulières répondant à la finalité mentionnée au huitième alinéa de l'article L. 532-2 et au cinquième alinéa de l'article L. 532-3 ou subordonnée au respect d'engagements pris par l'entreprise ou l'établissement.
