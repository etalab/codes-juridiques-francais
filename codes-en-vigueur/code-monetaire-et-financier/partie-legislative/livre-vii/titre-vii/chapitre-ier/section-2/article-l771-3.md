# Article L771-3

Les dispositions prévues aux articles L. 771-1 et L. 771-2 ne s'appliquent pas aux relations financières entre, d'une part, Saint-Barthélemy et, d'autre part, le territoire métropolitain, la Guadeloupe, la Guyane, la Martinique, La Réunion, Saint-Martin, Saint-Pierre-et-Miquelon, Mayotte, les îles Wallis et Futuna, la Nouvelle-Calédonie et la Polynésie française.
