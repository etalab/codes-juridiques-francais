# Article L772-1

La sous-section 2 de la section 3 du chapitre Ier du titre Ier du livre V n'est pas applicable à Saint-Barthélemy.
