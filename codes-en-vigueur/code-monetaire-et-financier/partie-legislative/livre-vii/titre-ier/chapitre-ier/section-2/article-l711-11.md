# Article L711-11

Le personnel détaché par l'agence française de développement auprès de l'institut d'émission des départements d'outre-mer reste régi par les dispositions qui lui sont applicables dans son établissement d'origine. Le personnel de l'institut non détaché par ladite agence est soumis à la législation du travail de droit commun.
