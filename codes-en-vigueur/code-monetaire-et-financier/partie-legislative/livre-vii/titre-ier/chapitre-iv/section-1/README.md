# Section 1 : Mesures de gel des avoirs décidées dans les cas autres que ceux prévus aux articles L. 562-1 et L. 562-2 du code monétaire et financier

- [Article L714-1](article-l714-1.md)
- [Article L714-2](article-l714-2.md)
- [Article L714-3](article-l714-3.md)
- [Article L714-4](article-l714-4.md)
