# Article L714-1

I. ― Le ministre chargé de l'économie peut décider le gel, pour une durée de six mois, renouvelable, à Saint-Barthélemy, à Saint-Pierre-et-Miquelon, en Nouvelle-Calédonie, en Polynésie française et dans les îles Wallis et Futuna, de tout ou partie des fonds, instruments financiers et ressources économiques appartenant à des personnes, organismes ou entités à l'encontre desquels de telles mesures sont en vigueur en France métropolitaine, en vertu de règlements adoptés par la Commission européenne ou le Conseil.

Les fruits produits par ces fonds, instruments financiers ou ressources économiques sont également gelés.

II. ― Le ministre chargé de l'économie peut décider d'interdire dans les collectivités mentionnées au I, pour une durée de six mois, renouvelable, tout mouvement ou transfert de fonds, instruments financiers et ressources économiques au bénéfice des personnes, organismes ou entités mentionnés au I.
