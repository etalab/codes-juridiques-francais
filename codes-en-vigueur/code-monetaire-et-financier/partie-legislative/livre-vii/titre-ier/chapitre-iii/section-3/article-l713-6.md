# Article L713-6

Le prestataire de services de paiement du bénéficiaire vérifie que les champs relatifs aux informations concernant le donneur d'ordre, prévus dans le système de messagerie ou de paiement et de règlement utilisé pour effectuer un virement de fonds, ont été complétés à l'aide de caractères ou d'éléments compatibles avec ce système.

Il dispose de procédures permettant de déceler :

a) Lorsque le prestataire de services de paiement du donneur d'ordre est situé en France, l'absence des informations mentionnées au I de l'article L. 713-5 ;

b) Lorsque le prestataire de services de paiement du donneur d'ordre est situé hors de France, l'absence des informations mentionnées au 1° du I de l'article L. 713-4 ou, le cas échéant, au 3° de l'article L. 713-9 ;

c) Dans le cas de virements par lots, lorsque le prestataire de services de paiement du donneur d'ordre est situé hors de France, l'absence des informations mentionnées à l'article L. 713-4. L'absence de ces informations est recherchée dans le virement par lots mais non dans les virements individuels regroupés dans les lots.
