# Section 3 : Obligations du prestataire de services de paiement du bénéficiaire

- [Article L713-6](article-l713-6.md)
- [Article L713-7](article-l713-7.md)
