# Article L713-1

Pour l'application du présent chapitre, on entend par :

1° " Donneur d'ordre ” : soit la personne qui est titulaire d'un compte ouvert chez les prestataires de services de paiement définis au 3° et qui autorise un virement de fonds à partir de ce compte, soit, en l'absence de compte, la personne qui donne l'ordre d'effectuer un virement de fonds ;

2° " Bénéficiaire ” : la personne qui est le destinataire final prévu des fonds virés ;

3° " Prestataire de services de paiement ” : les établissements autres que les sociétés de financement régis par le titre Ier et les chapitres Ier à III, V et VI du titre II du livre V ainsi que les offices des postes et télécommunications en Nouvelle-Calédonie et en Polynésie française, lorsqu'ils effectuent des virements de fonds :

a) Le prestataire de services de paiement du donneur d'ordre est le prestataire de services de paiement qui reçoit d'un donneur d'ordre instruction de procéder à un virement de fonds en faveur d'un bénéficiaire ;

b) Le prestataire de services de paiement du bénéficiaire est le prestataire de services de paiement chargé de mettre les fonds à la disposition du bénéficiaire ;

c) Le prestataire de services de paiement intermédiaire est un prestataire de services de paiement, distinct de ceux mentionnés aux a et b, qui participe à l'exécution du virement de fonds ;

4° " Virement de fonds ” : toute opération effectuée par voie électronique pour le compte d'un donneur d'ordre par l'intermédiaire d'un prestataire de services de paiement en vue de mettre des fonds à la disposition d'un bénéficiaire titulaire d'un compte ouvert chez un prestataire de services de paiement, le donneur d'ordre et le bénéficiaire pouvant être ou non la même personne ;

5° " Virement par lots ” : plusieurs virements de fonds individuels qui sont groupés en vue de leur transmission ;

6° " Identifiant unique ” : une combinaison de lettres, de numéros ou de symboles déterminée par le prestataire de services de paiement conformément aux protocoles du système de paiement et de règlement ou du système de messagerie utilisé pour effectuer le virement de fonds, permettant d'identifier le donneur d'ordre.
