# Article L713-2

Les dispositions du présent chapitre sont applicables aux virements de fonds en toutes monnaies émis ou reçus par tout prestataire de services de paiement domicilié à Saint-Barthélemy, à Saint-Pierre-et-Miquelon, en Nouvelle-Calédonie, en Polynésie française ou dans les îles Wallis et Futuna, à l'exception :

1° Des virements de fonds effectués à l'aide d'une carte de crédit ou de débit, en vertu d'un contrat conclu avec un prestataire de services de paiement, permettant le paiement de la fourniture de biens et de services, lorsqu'il a été attribué un identifiant unique au donneur d'ordre ;

2° Des virements de fonds effectués au moyen de monnaie électronique lorsque le montant de la transaction est inférieur ou égal à 1 000 € ou la contre-valeur en monnaie locale ;

3° Des virements de fonds effectués au moyen d'un téléphone portable ou d'un autre dispositif numérique ou lié aux technologies de l'information, lorsque ces virements sont effectués à partir d'un prépaiement et n'excèdent pas 150 € ou la contre-valeur en monnaie locale ;

4° Des virements de fonds postpayés, exécutés au moyen d'un téléphone portable ou d'un autre dispositif numérique ou lié aux technologies de l'information en vertu d'un contrat conclu avec un prestataire de services de paiement, permettant le paiement de la fourniture de biens et de services, lorsqu'il a été attribué un identifiant unique au donneur d'ordre ;

5° Des virements de fonds effectués dans la collectivité et en provenance ou en direction de la France métropolitaine, des collectivités territoriales régies par les articles 73 et des autres collectivités régies par l'article 74 de la Constitution et de la Nouvelle-Calédonie à condition que le prestataire de services de paiement du bénéficiaire puisse, au moyen d'un numéro de référence unique, identifier, par l'intermédiaire du bénéficiaire, la personne physique ou morale qui a effectué le virement et que le montant de la transaction soit inférieur ou égal à 1 000 € ou la contre-valeur en monnaie locale ;

6° Des retraits d'espèces effectués par le donneur d'ordre pour son propre compte ;

7° Des virements de fonds effectués en vertu d'une autorisation de prélèvement automatique dès lors qu'il a été attribué un identifiant unique au donneur d'ordre ;

8° Des virements de fonds effectués au moyen de chèques sous forme d'images-chèques ;

9° Des virements de fonds destinés au paiement de taxes, d'amendes ou autres impôts aux autorités publiques, en France ;

10° Des virements de fonds pour lesquels le donneur d'ordre et le bénéficiaire sont tous deux des prestataires de services de paiement opérant pour leur propre compte.
