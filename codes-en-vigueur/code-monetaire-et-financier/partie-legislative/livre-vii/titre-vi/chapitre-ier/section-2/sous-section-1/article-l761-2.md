# Article L761-2

Les articles L. 151-1 à L. 151-4 ainsi que l'article L. 165-1 sont applicables dans les îles Wallis-et-Futuna.

L'article L. 165-1 est modifié comme suit :

" Art. L. 165-1.-Les articles du code des douanes en vigueur dans les îles Wallis-et-Futuna correspondant au titre II et XII du code des douanes métropolitain sont applicables aux infractions aux obligations édictées par l'article L. 152-1. "

Des décrets pris sur le rapport du ministre chargé de l'outre-mer et du ministre chargé de l'économie fixent les conditions d'application de l'article L. 151-2.
