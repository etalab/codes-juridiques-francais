# Article L762-3

Les articles L. 213-1 à L. 213-4-1 sont applicables dans les îles Wallis-et-Futuna, à l'exception des 5 et 13 de l'article L. 213-3.
