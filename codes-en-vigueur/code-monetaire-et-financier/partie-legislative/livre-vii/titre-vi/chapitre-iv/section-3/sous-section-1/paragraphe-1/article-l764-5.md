# Article L764-5

Les articles L. 211-17 à L. 211-19 sont applicables dans les îles Wallis-et-Futuna.
