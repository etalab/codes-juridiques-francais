# Article L765-11-2-1

L'article L. 543-1 est applicable dans les îles Wallis et Futuna, sous réserve de supprimer la mention : " les sociétés de gestion des sociétés d'épargne forestière ".
