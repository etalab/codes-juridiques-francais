# Article L741-6

Les dispositions prévues aux articles L. 741-4 et L. 741-5 ne s'appliquent pas aux relations financières entre, d'une part, la Nouvelle-Calédonie et, d'autre part, le territoire métropolitain, la Guadeloupe, la Guyane, la Martinique, la Réunion, Saint-Barthélemy, Saint-Martin, Mayotte, Saint-Pierre-et-Miquelon, la Polynésie française et les îles Wallis et Futuna.
