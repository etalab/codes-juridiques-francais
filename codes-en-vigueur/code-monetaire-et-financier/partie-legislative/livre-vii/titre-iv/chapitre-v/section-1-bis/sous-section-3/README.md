# Sous-section 3 : Les envois contre remboursement

- [Article L745-7-12](article-l745-7-12.md)
- [Article L745-7-13](article-l745-7-13.md)
- [Article L745-7-14](article-l745-7-14.md)
- [Article L745-7-15](article-l745-7-15.md)
