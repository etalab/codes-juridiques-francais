# Sous-section 1 : Le chèque postal et les cartes de paiement

- [Article L745-7-3](article-l745-7-3.md)
- [Article L745-7-4](article-l745-7-4.md)
- [Article L745-7-5](article-l745-7-5.md)
- [Article L745-7-6](article-l745-7-6.md)
- [Article L745-7-7](article-l745-7-7.md)
- [Article L745-7-8](article-l745-7-8.md)
