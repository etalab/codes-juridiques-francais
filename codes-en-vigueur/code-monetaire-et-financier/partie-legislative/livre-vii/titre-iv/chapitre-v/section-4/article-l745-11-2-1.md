# Article L745-11-2-1

L'article L. 543-1 est applicable en Nouvelle-Calédonie, sous réserve de supprimer la mention : " les sociétés de gestion des sociétés d'épargne forestière ".
