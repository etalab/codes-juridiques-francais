# Article L745-11-6

I.-Les articles du chapitre VII du titre IV du livre V sont applicables en Nouvelle-Calédonie, à l'exception de l'article L. 547-8 et sous réserve des dispositions suivantes :

1° Au deuxième alinéa du II de l'article L. 547-1, les mots : " articles 54, 55 et 60 de la loi n° 71-1130 du 31 décembre 1971 portant réforme de certaines professions judiciaires et juridiques " sont remplacés par les mots : " dispositions applicables localement ayant un effet équivalent aux articles 54, 55 et 60 de la loi n° 71-1130 du 31 décembre 1971 portant réforme de certaines professions judiciaires et juridiques. " ;

2° Au dernier alinéa de l'article L. 547-4, les mots : " mentionné à l'article L. 512-1 du code des assurances " sont remplacés par les mots : " qui assure la tenue du registre unique mentionné à l'article L. 745-11-5 " ;

3° Pour l'application de l'article L. 547-5 en Nouvelle-Calédonie :

a) Au II, après les mots : " un contrat d'assurances le couvrant contre les conséquences pécuniaires de sa responsabilité civile professionnelle ", sont ajoutés les mots : " tel que prévu par les dispositions applicables localement en matière d'assurance, " ;

b) Le décret en Conseil d'Etat mentionné au III est complété, le cas échéant, par des dispositions applicables localement ;

4° Pour l'application en Nouvelle-Calédonie du 8° de l'article L. 547-9, la référence au code de commerce est remplacée par les dispositions applicables localement ayant le même objet.

II.-Les articles L. 573-12 à L. 573-14 y sont également applicables.
