# Article L745-1-1

Le chapitre Ier du titre Ier du livre V est applicable en Nouvelle-Calédonie, à l'exception des articles L. 511-8-1, L. 511-12, L. 511-21 à L. 511-28, des 1°, 3° et 4° de l'article L. 511-34, des articles L. 511-41-1, L. 511-41-2, L. 511-45 ainsi que du dernier alinéa de l'article L. 511-102. Les articles L. 571-1 à L. 571-9 y sont également applicables.

Pour l'application de ses dispositions, le premier alinéa de l'article L. 511-46 est ainsi rédigé :

" Au sein des établissements de crédit et des sociétés de financement mentionnés à l'article L. 511-1, le comité mentionné à l'article L. 823-19 du code de commerce assure également le suivi de la politique, des procédures et des systèmes de gestion des risques. "

A l'article L. 511-36, les mots : " règlement de la Commission européenne " sont remplacés par les mots : " arrêté du ministre chargé de l'économie ".

Pour l'application de l'article L. 511-6 :

-au premier alinéa, les mots : " ni les institutions et services énumérés à l'article L. 518-1, ni les entreprises régies par le code des assurances, ni les sociétés de réassurance, ni les organismes agréés soumis aux dispositions du livre II du code de la mutualité pour les opérations visées au e du 1° de l'article L. 111-1 dudit code " sont remplacés par les mots : " ni l'institut d'émission d'outre-mer " ;

-le quatrième et l'avant-dernier alinéa de cet article sont supprimés ;

-au septième alinéa, les mots : " et des institutions ou services mentionnés l'article L. 518-1 " sont supprimés ;

-au huitième alinéa, les mots : " répondant à la définition visée au III de l'article 80 de la loi n° 2005-32 du 18 janvier 2005 de programmation pour la cohésion sociale et bénéficiant à ce titre de garanties publiques " sont supprimés ;

Pour l'application de l'article L. 511-32, les mots : " européennes directement applicables, " sont supprimés.

Pour l'application de l'article L. 511-34, au deuxième alinéa, les mots : " entités réglementées ou " sont supprimés.

Pour l'application de l'article L. 511-48, au 1° du II, les mots : " taxables au titre de l'article 235 ter ZD bis du code général des impôts " sont remplacés par les mots : " constituées par le fait d'adresser à titre habituel des ordres, en ayant recours à un dispositif de traitement automatisé, caractérisé par l'envoi, la modification ou l'annulation d'ordres successifs sur un titre donné, séparés d'un délai inférieur à une seconde ".

Pour l'application de l'article L. 511-52, les références au code de commerce sont remplacées par des références à des dispositions applicables localement ayant le même effet.

Pour l'application de l'article L. 511-86, au second alinéa, les mots : " Sous réserve du respect des dispositions du V de l'article 4 de la loi n° 2011-1416 du 2 novembre 2011 de finances rectificative pour 2011, " sont supprimés.

Pour l'application de l'article L. 511-97, les mots : " au comité spécialisé mentionné à l'article L. 823-19 du code de commerce " sont remplacés par les dispositions suivantes : " à un comité spécialisé créé par l'organe délibérant et agissant sous la responsabilité de celui-ci pour assurer le suivi des questions relatives à l'élaboration et au contrôle des informations comptables et financières. Le comité ne peut comprendre que des membres de l'organe délibérant en fonctions dans la société. Un membre au moins du comité doit présenter des compétences particulières en matière financière ou comptable et être indépendant au regard de critères rendus publics par l'organe délibérant. "

Sans préjudice des compétences de l'organe délibérant, ce comité est notamment chargé d'assurer le suivi du processus d'élaboration de l'information financière, de l'efficacité des systèmes de contrôle interne et de gestion des risques, du contrôle légal des comptes annuels et, le cas échéant, des comptes consolidés par les commissaires aux comptes, et de l'indépendance des commissaires aux comptes.

Il émet une recommandation sur les commissaires aux comptes proposés à la désignation par l'assemblée générale ou l'organe exerçant une fonction analogue. Il rend compte régulièrement à l'organe collégial délibérant de l'exercice de ses missions et l'informe sans délai de toute difficulté rencontrée. Ce comité assure également le suivi de la politique, des procédures et des systèmes de gestion des risques.

Le dernier alinéa de l'article L. 571-4 est applicable à l'office des postes et télécommunications.
