# Article L752-3

Les articles L. 213-1 à L. 213-4-1 sont applicables en Polynésie française, à l'exception des 5 et 13 de l'article L. 213-3.
