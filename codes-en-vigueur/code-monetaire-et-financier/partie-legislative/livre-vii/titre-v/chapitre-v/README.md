# Chapitre V : Les prestataires de services

- [Section 1 : Prestataires de services bancaires](section-1)
- [Section 1 bis : Les services financiers de l'office des postes et télécommunications](section-1-bis)
- [Section 2 : Prestataires de services de paiement, changeurs manuels et émetteurs de monnaie électronique](section-2)
- [Section 3 : Les prestataires de services d'investissement](section-3)
- [Section 6 : Obligations relatives à la lutte contre le blanchiment de capitaux](section-6)
- [Article L755-0](article-l755-0.md)
- [Article L755-1](article-l755-1.md)
