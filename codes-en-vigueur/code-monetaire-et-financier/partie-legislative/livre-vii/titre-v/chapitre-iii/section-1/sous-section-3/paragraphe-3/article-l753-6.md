# Article L753-6

Les articles L. 313-23 à    L. 313-49-1 sont applicables en Polynésie française.

Pour l'application des articles L. 313-42 et    L. 313-49-1, les références au code de commerce sont remplacées par les références à des dispositions applicables localement ayant le même objet.
