# Livre VII : Régime de l'outre-mer

- [Titre Ier : Dispositions communes à plusieurs collectivités territoriales](titre-ier)
- [Titre II : Dispositions spécifiques à Saint-Pierre-et-Miquelon](titre-ii)
- [Titre III : Dispositions particulières applicables au département de Mayotte](titre-iii)
- [Titre IV : Dispositions applicables en Nouvelle-Calédonie](titre-iv)
- [Titre V : Dispositions applicables en Polynésie française](titre-v)
- [Titre VI : Dispositions applicables dans les îles Wallis-et-Futuna](titre-vi)
- [Titre VII : Dispositions spécifiques à Saint-Barthélemy](titre-vii)
