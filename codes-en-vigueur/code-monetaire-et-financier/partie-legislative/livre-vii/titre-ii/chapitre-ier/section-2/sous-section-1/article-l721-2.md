# Article L721-2

A Saint-Pierre-et-Miquelon, les personnes physiques doivent déclarer les sommes, titres ou valeurs qu'elles transfèrent en provenance ou à destination de l'étranger, sans l'intermédiaire              d'un établissement de crédit, d'un établissement de monnaie électronique, d'un établissement de paiement ou d'un organisme ou service mentionné à l'article L. 518-1.

Une déclaration est établie pour chaque transfert à l'exclusion des transferts dont le montant est inférieur à 10 000 euros.

Les modalités d'application du présent article sont fixées par décret en Conseil d'Etat.
