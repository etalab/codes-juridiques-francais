# Article L211-17

Le transfert de propriété de titres financiers résulte de l'inscription de ces titres au compte-titres de l'acquéreur.

Lorsque les titres financiers sont admis aux opérations d'un dépositaire central ou livrés dans un système de règlement et de livraison d'instruments financiers mentionné à l'article L. 330-1, le transfert de propriété résulte de l'inscription des titres au compte-titres de l'acquéreur, à la date et dans les conditions définies par le règlement général de l'Autorité des marchés financiers.

Par dérogation aux alinéas précédents, lorsque le système de règlement et de livraison assure la livraison des titres financiers en prévoyant un dénouement irrévocable en continu, le transfert n'intervient au profit de l'acquéreur que lorsque celui-ci a réglé le prix. Tant que l'acquéreur n'a pas réglé le prix, l'intermédiaire qui a reçu les titres financiers en est le propriétaire. Le règlement général de l'Autorité des marchés financiers précise les modalités particulières de transfert de propriété applicables dans le cas prévu au présent alinéa.
