# Paragraphe 1 : Négociabilité

- [Article L211-14](article-l211-14.md)
- [Article L211-15](article-l211-15.md)
- [Article L211-16](article-l211-16.md)
