# Article L211-15

Les titres financiers se transmettent par virement de compte à compte.
