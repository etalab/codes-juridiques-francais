# Article L211-25

Les titres financiers empruntés et la dette représentative de l'obligation de restitution de ces titres sont inscrits distinctement au bilan de l'emprunteur au prix du marché au jour du prêt.
