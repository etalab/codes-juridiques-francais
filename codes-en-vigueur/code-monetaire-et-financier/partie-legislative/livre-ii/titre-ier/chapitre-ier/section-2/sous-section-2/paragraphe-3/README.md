# Paragraphe 3 : Protection du titulaire du compte

- [Article L211-9](article-l211-9.md)
- [Article L211-10](article-l211-10.md)
- [Article L211-11](article-l211-11.md)
- [Article L211-12](article-l211-12.md)
