# Article L211-3

Les titres financiers, émis en territoire français et soumis à la législation française, sont inscrits dans un compte-titres tenu soit par l'émetteur, soit par l'un des intermédiaires mentionnés aux 2° à 7° de l'article L. 542-1.
