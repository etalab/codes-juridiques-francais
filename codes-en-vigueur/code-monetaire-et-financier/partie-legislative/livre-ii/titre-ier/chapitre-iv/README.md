# Chapitre IV : Placements collectifs

- [Section 1 : OPCVM](section-1)
- [Section 2 : FIA](section-2-fia)
- [Section 3 : Autres placements collectifs](section-3)
- [Article L214-1](article-l214-1.md)
- [Article L214-1-1](article-l214-1-1.md)
