# Article L214-154

Un fonds professionnel spécialisé prend la forme d'une SICAV ou d'un fonds commun de placement. Selon le cas, sa dénomination est alors respectivement celle de " société d'investissement professionnelle spécialisée " ou de " fonds d'investissement professionnel spécialisé ".

Par dérogation aux articles L. 214-24-29, L. 214-24-34 et L. 214-24-55, un fonds professionnel spécialisé peut investir dans des biens s'ils satisfont aux règles suivantes :

1° La propriété du bien est fondée soit sur une inscription, soit sur un acte authentique, soit sur un acte sous seing privé dont la valeur probante est reconnue par la loi française ;

2° Le bien ne fait l'objet d'aucune sûreté autre que celles éventuellement constituées pour la réalisation de l'objectif de gestion du fonds professionnel spécialisé ;

3° Le bien fait l'objet d'une valorisation fiable sous forme d'un prix calculé de façon précise et établi régulièrement, qui est soit un prix de marché, soit un prix fourni par un système de valorisation permettant de déterminer la valeur à laquelle l'actif pourrait être échangé entre des parties avisées et contractant en connaissance de cause dans le cadre d'une transaction effectuée dans des conditions normales de concurrence ;

4° La liquidité du bien permet au fonds professionnel spécialisé de respecter ses obligations en matière d'exécution des rachats vis-à-vis de ses porteurs et actionnaires définies par ses statuts ou son règlement.
