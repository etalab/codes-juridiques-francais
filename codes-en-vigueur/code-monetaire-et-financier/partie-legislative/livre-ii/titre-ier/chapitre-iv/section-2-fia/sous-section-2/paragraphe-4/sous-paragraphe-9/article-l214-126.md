# Article L214-126

Un décret en Conseil d'Etat fixe les modalités d'application des sections 1, 2, 3 et 4 du présent chapitre.
