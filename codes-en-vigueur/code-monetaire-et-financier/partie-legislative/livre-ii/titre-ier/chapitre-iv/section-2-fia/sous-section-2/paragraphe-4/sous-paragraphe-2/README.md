# Sous-paragraphe 2 : Souscription des parts

- [Article L214-93](article-l214-93.md)
- [Article L214-94](article-l214-94.md)
- [Article L214-95](article-l214-95.md)
- [Article L214-96](article-l214-96.md)
- [Article L214-97](article-l214-97.md)
