# Article L214-24-50

Le résultat net d'un fonds d'investissement à vocation générale est égal au montant des intérêts, arrérages, primes et lots, dividendes, jetons de présence et tous autres produits relatifs aux titres constituant le portefeuille, majoré du produit des sommes momentanément disponibles et diminué du montant des frais de gestion et de la charge des emprunts.
