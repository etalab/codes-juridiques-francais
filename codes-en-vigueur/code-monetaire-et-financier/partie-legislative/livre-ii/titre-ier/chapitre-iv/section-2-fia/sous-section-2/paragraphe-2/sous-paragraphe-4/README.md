# Sous-paragraphe 4 : Fonds d'investissement de proximité

- [Article L214-31](article-l214-31.md)
- [Article L214-32](article-l214-32.md)
- [Article L214-32-1](article-l214-32-1.md)
