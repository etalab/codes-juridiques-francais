# Paragraphe 1 : Dispositions communes aux organismes de titrisation

- [Sous-paragraphe 1 : Dispositions particulières aux sociétés de titrisation](sous-paragraphe-1)
- [Sous-paragraphe 2 : Dispositions particulières aux fonds communs de titrisation](sous-paragraphe-2)
- [Article L214-167](article-l214-167.md)
- [Article L214-168](article-l214-168.md)
- [Article L214-169](article-l214-169.md)
- [Article L214-170](article-l214-170.md)
- [Article L214-171](article-l214-171.md)
- [Article L214-172](article-l214-172.md)
- [Article L214-173](article-l214-173.md)
- [Article L214-174](article-l214-174.md)
- [Article L214-175](article-l214-175.md)
