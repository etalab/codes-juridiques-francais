# Paragraphe 2 : Dispositions particulières aux organismes de titrisation ou aux compartiments d'organismes de titrisation supportant des risques d'assurance

- [Article L214-187](article-l214-187.md)
- [Article L214-188](article-l214-188.md)
- [Article L214-189](article-l214-189.md)
- [Article L214-190](article-l214-190.md)
