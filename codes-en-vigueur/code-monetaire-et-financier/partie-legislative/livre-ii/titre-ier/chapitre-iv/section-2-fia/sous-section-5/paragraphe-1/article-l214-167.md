# Article L214-167

I.-La présente section ne s'applique pas aux organismes de titrisation, à l'exception de la présente sous-section et des I et II de l'article L. 214-24.

II.-Par dérogation au I, les organismes de titrisation qui répondent à des caractéristiques définies par décret sont soumis à la présente section, à l'exception des sous-sections 2 à 4.
