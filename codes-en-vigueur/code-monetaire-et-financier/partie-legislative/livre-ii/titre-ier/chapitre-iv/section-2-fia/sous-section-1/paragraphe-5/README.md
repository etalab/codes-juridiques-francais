# Paragraphe 5 : Participation et contrôle

- [Article L214-24-21](article-l214-24-21.md)
- [Article L214-24-22](article-l214-24-22.md)
- [Article L214-24-23](article-l214-24-23.md)
