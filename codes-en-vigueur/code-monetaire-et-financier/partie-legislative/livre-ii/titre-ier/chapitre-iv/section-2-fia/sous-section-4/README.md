# Sous-section 4 : Fonds d'épargne salariale

- [Paragraphe 1 : Fonds communs de placement d'entreprise](paragraphe-1)
- [Paragraphe 2 : Sociétés d'investissement à capital variable d'actionnariat salarié](paragraphe-2)
- [Article L214-163](article-l214-163.md)
