# Article L214-7

La SICAV est une société anonyme ou une société par actions simplifiée qui a pour seul objet la gestion d'un portefeuille d'instruments financiers et de dépôts.

Le siège social et l'administration centrale de la SICAV sont situés en France.

Sous réserve des dispositions de l'article L. 214-7-4, les actions de la SICAV sont émises et rachetées par la société à la demande, selon le cas, des souscripteurs ou des actionnaires et à la valeur liquidative majorée ou diminuée, selon le cas, des frais et commissions.

Lorsque la SICAV est une société anonyme, ses actions peuvent être admises aux négociations sur un marché réglementé dans des conditions fixées par décret.

Le montant du capital est égal à tout moment à la valeur de l'actif net de la société, déduction faite des sommes distribuables définies à l'article L. 214-17-2.

Le capital initial d'une SICAV ne peut être inférieur à un montant fixé par décret.
