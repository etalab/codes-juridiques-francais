# Sous-section 3 : Obligations de la société de gestion, du dépositaire et de l'entité responsable de la centralisation et du commissaire aux comptes

- [Article L214-9](article-l214-9.md)
- [Article L214-10](article-l214-10.md)
- [Article L214-10-1](article-l214-10-1.md)
- [Article L214-11](article-l214-11.md)
- [Article L214-12](article-l214-12.md)
- [Article L214-13](article-l214-13.md)
- [Article L214-14](article-l214-14.md)
