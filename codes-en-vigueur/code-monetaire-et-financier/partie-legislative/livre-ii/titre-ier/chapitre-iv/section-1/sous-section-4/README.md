# Sous-section 4 : Règles de fonctionnement

- [Article L214-15](article-l214-15.md)
- [Article L214-16](article-l214-16.md)
- [Article L214-17](article-l214-17.md)
- [Article L214-17-1](article-l214-17-1.md)
- [Article L214-17-2](article-l214-17-2.md)
- [Article L214-17-3](article-l214-17-3.md)
- [Article L214-18](article-l214-18.md)
- [Article L214-19](article-l214-19.md)
