# Article L213-12

L'émission d'obligations par les associations mentionnées à l'article L. 213-8 peut être effectuée par offre au public. Elle est alors soumise au contrôle de l'Autorité des marchés financiers dans les conditions prévues par le présent code.
