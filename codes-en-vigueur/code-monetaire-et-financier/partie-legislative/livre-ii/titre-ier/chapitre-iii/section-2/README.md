# Section 2 : Les obligations

- [Sous-section 1 : Règles générales.](sous-section-1)
- [Sous-section 2 : Obligations émises par les groupements d'intérêt économique.](sous-section-2)
- [Sous-section 3 : Obligations émises par les associations.](sous-section-3)
- [Sous-section 4 : Obligations émises par les fondations](sous-section-4)
