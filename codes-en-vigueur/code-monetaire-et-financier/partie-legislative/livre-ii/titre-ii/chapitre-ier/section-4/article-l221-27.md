# Article L221-27

Le livret de développement durable est ouvert par les personnes physiques ayant leur domicile fiscal en France dans les établissements et organismes autorisés à recevoir des dépôts. Les sommes déposées sur ce livret sont employées conformément à l'article L. 221-5.

Les versements effectués sur un livret de développement durable ne peuvent porter le montant inscrit sur le livret au-delà d'un plafond fixé par voie réglementaire.

Il ne peut être ouvert qu'un livret par contribuable ou un livret pour chacun des époux ou partenaires liés par un pacte civil de solidarité, soumis à une imposition commune.

Les modalités d'ouverture et de fonctionnement du livret de développement durable, ainsi que la nature des travaux d'économies d'énergie auxquels sont affectées les sommes déposées sur ce livret, sont fixées par voie réglementaire.

Les opérations relatives au livret de développement durable sont soumises au contrôle sur pièces et sur place de l'inspection générale des finances.
