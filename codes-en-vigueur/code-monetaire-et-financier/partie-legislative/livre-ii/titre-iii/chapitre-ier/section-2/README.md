# Section 2 : Infractions relatives aux placements collectifs

- [Sous-section 1 : Dispositions relatives aux OPCVM, aux fonds communs de créances et aux organismes de placement collectif immobilier](sous-section-1)
- [Sous-section 2 : Dispositions relatives aux sociétés civiles de placement immobilier](sous-section-2)
