# Article L232-1

Est puni des peines prévues par les articles 313-1,313-7 et 313-8 du code pénal le fait, pour l'émetteur, de reproduire un bilan inexact et faussement certifié sincère dans le cas prévu par le deuxième alinéa de l'article L. 223-2.
