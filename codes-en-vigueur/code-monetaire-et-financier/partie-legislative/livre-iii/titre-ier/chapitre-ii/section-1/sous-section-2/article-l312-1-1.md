# Article L312-1-1

I.-Les établissements de crédit sont tenus d'informer leur clientèle et le public sur les conditions générales et tarifaires applicables aux opérations relatives à la gestion d'un compte de dépôt, selon des modalités fixées par un arrêté du ministre chargé de l'économie.

La gestion d'un compte de dépôt des personnes physiques n'agissant pas pour des besoins professionnels est réglée par une convention écrite passée entre le client et son établissement de crédit ou les services financiers de La Poste.

Lorsqu'un relevé de compte est diffusé en application des stipulations de la convention visée à l'alinéa précédent et que celui-ci indique, à titre d'information, qu'un montant de découvert est autorisé, il mentionne immédiatement après, dans les mêmes caractères, le taux annuel effectif global au sens de l'article L. 313-1 du code de la consommation, quelle que soit la durée du découvert autorisé considéré.

Jusqu'au 31 décembre 2009, les établissements de crédit et les services financiers de La Poste sont tenus d'informer au moins une fois par an les clients n'ayant pas de convention de compte de dépôt de la possibilité d'en signer une.

Les principales stipulations que la convention de compte de dépôt doit comporter, notamment les conditions générales et tarifaires d'ouverture, de fonctionnement et de clôture, sont précisées par un arrêté du ministre chargé de l'économie.

Avant que le client ne soit lié par cette convention, l'établissement de crédit l'informe desdites conditions sur support papier ou sur un autre support durable. L'établissement de crédit peut s'acquitter de cette obligation en fournissant au client une copie du projet de convention de compte de dépôt.

Si, à la demande du client, cette convention est conclue par un moyen de communication à distance ne permettant pas à l'établissement de crédit de se conformer au précédent alinéa, ce dernier satisfait à ses obligations aussitôt après la conclusion de la convention de compte de dépôt.

L'acceptation de la convention de compte de dépôt est formalisée par la signature du ou des titulaires du compte.

Lorsque l'établissement de crédit est amené à proposer à son client de nouvelles prestations de services de paiement dont il n'était pas fait mention dans la convention de compte de dépôt, les informations relatives à ces nouvelles prestations font l'objet d'un contrat-cadre de services de paiement régi par les dispositions des sections 2 à 4 du chapitre IV du présent titre relatives au contrat-cadre de services de paiement ou d'une modification de la convention de compte de dépôt dans les conditions mentionnées au II du présent article.

II.-Tout projet de modification de la convention de compte de dépôt est communiqué sur support papier ou sur un autre support durable au client au plus tard deux mois avant la date d'application envisagée. Selon les modalités prévues dans la convention de compte de dépôt, l'établissement de crédit informe le client qu'il est réputé avoir accepté la modification s'il ne lui a pas notifié, avant la date d'entrée en vigueur proposée de cette modification, qu'il ne l'acceptait pas ; dans ce cas, l'établissement de crédit précise également que, si le client refuse la modification proposée, il peut résilier la convention de compte de dépôt sans frais, avant la date d'entrée en vigueur proposée de la modification.

III.-Le client peut résilier la convention de compte de dépôt à tout moment, sauf stipulation contractuelle d'un préavis qui ne peut dépasser trente jours.

Au-delà de douze mois, la convention de compte de dépôt peut être résiliée sans frais. Dans les autres cas, les frais de résiliation doivent être proportionnés aux coûts induits par cette résiliation.

L'établissement de crédit résilie une convention de compte de dépôt conclue pour une durée indéterminée moyennant un préavis d'au moins deux mois. Les frais régulièrement imputés pour la prestation de services de paiement ne sont dus par le client qu'au prorata de la période échue à la date de résiliation de la convention de compte de dépôt. S'ils ont été payés à l'avance, ces frais sont remboursés au prorata.

Avec l'accord du client, la convention de compte peut être adaptée avant l'expiration du délai de deux mois mentionné au II lorsqu'il bénéficie de la procédure de surendettement afin de faciliter l'exécution des mesures de traitement prévue au titre III du livre III du code de la consommation. L'Association française des établissements de crédit et des entreprises d'investissement, mentionnée à l'article L. 511-29 du présent code, adopte des normes professionnelles qui précisent les modalités et la durée du maintien du compte de dépôt et les adaptations, en particulier des moyens de paiement, de nature à en faciliter le fonctionnement et à éviter les incidents.

Ces normes, homologuées par le ministre de l'économie, après avis du comité consultatif du secteur financier et du comité consultatif de la législation et de la réglementation financières, sont applicables par tout établissement de crédit. Le contrôle du respect de ces normes est assuré par l'Autorité de contrôle prudentiel et de résolution et relève de la procédure prévue à l'article L. 612-31.

IV.-A tout moment de la relation contractuelle, l'établissement de crédit fournit à la demande de l'utilisateur les termes de la convention de compte de dépôt sur support papier ou sur un autre support durable.

L'établissement de crédit ne peut refuser la fourniture au client d'une convention établie sur support papier.

V.-Pour chaque opération de paiement mentionnée à l'article L. 314-2 relevant d'une convention de compte de dépôt et ordonnée par le payeur, le prestataire de services de paiement fournit à celui-ci, à sa demande, des informations sur le délai d'exécution maximal de cette opération spécifique, sur les frais qu'il doit payer et, le cas échéant, sur le détail de ces frais.
