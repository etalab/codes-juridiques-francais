# Sous-section 2 : Crédits aux entreprises

- [Paragraphe 1 : Crédit d'exploitation](paragraphe-1)
- [Paragraphe 2 : Prêts participatifs](paragraphe-2)
- [Paragraphe 3 : Garanties des crédits aux entrepreneurs individuels](paragraphe-3)
- [Paragraphe 4 : Régime des engagements de garantie](paragraphe-4)
