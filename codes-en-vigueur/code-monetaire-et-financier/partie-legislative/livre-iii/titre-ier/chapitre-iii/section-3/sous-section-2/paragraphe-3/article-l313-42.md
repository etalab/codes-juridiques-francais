# Article L313-42

Sont soumis aux dispositions du présent paragraphe les billets à ordre émis par les établissements de crédit ou les sociétés de financement pour mobiliser des créances à long terme destinées au financement d'un bien immobilier situé en France ou dans les autres Etats de l'Espace économique européen et garanties :

-par une hypothèque de premier rang ou une sûreté immobilière conférant une garantie au moins équivalente ;

-ou par un cautionnement consenti par un établissement de crédit ou une société de financement ou une entreprise d'assurance n'entrant pas dans le périmètre de consolidation défini à l'article L. 233-16 du code de commerce dont relève l'établissement de crédit ou la société de financement émetteur du billet à ordre.

Sont assimilées aux créances mentionnées ci-dessus les parts ou titres de créances émis par des organismes de titrisation, dès lors que l'actif de ces fonds est composé, à hauteur de 90 % au moins, de créances de même nature, à l'exclusion des parts spécifiques ou titres de créances supportant le risque de défaillance des débiteurs des créances.

Les créances mobilisées par des billets à ordre doivent respecter, à compter du 1er janvier 2002, les conditions prévues au I de l'article L. 513-3 selon des modalités déterminées par un décret en Conseil d'Etat. Ce décret précise les conditions dans lesquelles la quotité peut être dépassée si le montant desdites créances excède celui des billets à ordre qu'elles garantissent.
