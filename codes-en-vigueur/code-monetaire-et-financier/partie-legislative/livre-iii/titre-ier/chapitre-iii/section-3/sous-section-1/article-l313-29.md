# Article L313-29

Sur la demande du bénéficiaire du bordereau, le débiteur peut s'engager à le payer directement : cet engagement est constaté, à peine de nullité, par un écrit intitulé : " Acte d'acceptation de la cession ou du nantissement d'une créance professionnelle ".

Dans ce cas, le débiteur ne peut opposer à l'établissement de crédit ou à la société de financement les exceptions fondées sur ses rapports personnels avec le signataire du bordereau, à moins que l'établissement de crédit ou la société de financement, en acquérant ou en recevant la créance, n'ait agi sciemment au détriment du débiteur.
