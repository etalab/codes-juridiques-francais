# Article L313-27

La cession ou le nantissement prend effet entre les parties et devient opposable aux tiers à la date apposée sur le bordereau lors de sa remise, quelle que soit la date de naissance, d'échéance ou d'exigibilité des créances, sans qu'il soit besoin d'autre formalité, et ce quelle que soit la loi applicable aux créances et la loi du pays de résidence des débiteurs.

A compter de cette date, le client de l'établissement de crédit ou de la société de financement bénéficiaire du bordereau ne peut, sans l'accord de cet établissement ou de cette société, modifier l'étendue des droits attachés aux créances représentées par ce bordereau.

La remise du bordereau entraîne de plein droit le transfert des sûretés, des garanties et des accessoires attachés à chaque créance, y compris les sûretés hypothécaires, et son opposabilité aux tiers sans qu'il soit besoin d'autre formalité.

En cas de contestation de la date portée sur le bordereau, l'établissement de crédit ou la société de financement rapporte, par tous moyens, l'exactitude de celle-ci.
