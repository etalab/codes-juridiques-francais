# Article L313-26

Le bordereau n'est transmissible qu'à un autre établissement de crédit ou une autre société de financement.
