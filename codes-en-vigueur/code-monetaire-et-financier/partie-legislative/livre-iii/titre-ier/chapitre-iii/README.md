# Chapitre III : Crédits

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Catégories de crédits et opérations assimilées](section-2)
- [Section 3 : Procédures de mobilisation des créances professionnelles](section-3)
- [Section 4 : Garantie des cautions](section-4)
