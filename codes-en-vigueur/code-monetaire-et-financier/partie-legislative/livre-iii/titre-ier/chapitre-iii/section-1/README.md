# Section 1 : Dispositions générales

- [Sous-section 1 : Définition](sous-section-1)
- [Sous-section 2 : Taux d'intérêt](sous-section-2)
- [Sous-section 3 : Fichier des incidents de paiement caractérisés](sous-section-3)
