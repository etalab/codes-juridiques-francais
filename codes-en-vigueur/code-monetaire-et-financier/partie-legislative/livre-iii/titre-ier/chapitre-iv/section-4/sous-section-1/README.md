# Sous-section 1 : Opérations de paiement isolées

- [Article L314-9](article-l314-9.md)
- [Article L314-10](article-l314-10.md)
- [Article L314-11](article-l314-11.md)
