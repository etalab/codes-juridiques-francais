# Sous-section 4 : Obligations d'information lorsqu'un des prestataires de services de paiement impliqué dans l'opération est situé à Saint-Pierre-et-Miquelon ou en dehors de l'Espace économique européen

- [Article L314-15](article-l314-15.md)
