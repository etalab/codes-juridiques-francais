# Article L322-3

Un arrêté du ministre chargé de l'économie, pris sur avis conforme de L'Autorité des marchés financiers, détermine notamment :

1. Le plafond d'indemnisation par investisseur, les modalités et délais d'indemnisation ainsi que les règles relatives à l'information de la clientèle ;

2. Les caractéristiques des certificats d'association, ainsi que les conditions de leur rémunération et de leur remboursement en cas de retrait de l'agrément, après imputation, le cas échéant, des pertes subies par le mécanisme ;

3. Le montant global et la formule de répartition des cotisations annuelles dues par les établissements mentionnés à l'article L. 322-1 dont l'assiette est constituée de la valeur des dépôts et des instruments financiers qui sont couverts par la garantie en vertu de l'article L. 322-1 pondérée par les cotisations déjà versées ainsi que par des indicateurs de la situation financière de chacun des établissements concernés, reflétant les risques objectifs que l'adhérent fait courir au fonds ;

4. Les conditions dans lesquelles une partie de ces contributions peut ne pas être versée au fonds de garantie moyennant la constitution de garanties appropriées.

Les cotisations dues par les établissements affiliés à un des organes centraux mentionnés à l'article L. 511-30 sont directement versées au fonds de garantie par cet organe central.
