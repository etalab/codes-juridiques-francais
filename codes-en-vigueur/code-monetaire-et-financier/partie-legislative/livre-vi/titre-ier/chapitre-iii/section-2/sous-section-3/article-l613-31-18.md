# Article L613-31-18

L'annulation d'une décision du collège de l'Autorité de contrôle prudentiel et de résolution n'affecte pas la validité des actes pris pour son application lorsque leur remise en cause est de nature à porter atteinte aux intérêts des tiers, sauf en cas de fraude de ceux-ci.
