# Article L613-21-7

Un décret en Conseil d'Etat précise les conditions d'application de la présente sous-section.
