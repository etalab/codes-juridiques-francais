# Article L612-39

Sous réserve des dispositions de l'article L. 612-40, si l'une des personnes mentionnées au I de l'article L. 612-2, à l'exception de celles mentionnées aux 4 bis, 5° et 11° du A et au 4° du B, a enfreint une disposition européenne, législative ou réglementaire au respect de laquelle l'Autorité a pour mission de veiller ou des codes de conduite homologués applicables à sa profession, n'a pas remis à l'Autorité le programme de rétablissement demandé ou le programme de formation mentionné au V de l'article L. 612-23-1, n'a pas tenu compte d'une mise en garde, n'a pas déféré à une mise en demeure ou n'a pas respecté les conditions particulières posées ou les engagements pris à l'occasion d'une demande d'agrément, d'autorisation ou de dérogation prévue par les dispositions législatives ou réglementaires applicables, la commission des sanctions peut prononcer l'une ou plusieurs des sanctions disciplinaires suivantes, en fonction de la gravité du manquement :

1° L'avertissement ;

2° Le blâme ;

3° L'interdiction d'effectuer certaines opérations et toutes autres limitations dans l'exercice de l'activité ;

4° La suspension temporaire d'un ou plusieurs dirigeants ou de toute autre personne mentionnée à l'article L. 612-23-1 ou, dans le cas d'un établissement de paiement ou d'un établissement de monnaie électronique exerçant des activités hybrides, des personnes déclarées responsables, respectivement, de la gestion des activités de services de paiement ou des activités d'émission et de gestion de monnaie électronique, avec ou sans nomination d'administrateur provisoire ;

5° La démission d'office d'un ou plusieurs dirigeants ou de toute autre personne mentionnée à l'article L. 612-23-1 ou, dans le cas d'un établissement de paiement ou d'un établissement de monnaie électronique exerçant des activités hybrides, des personnes déclarées responsables, respectivement, de la gestion des activités de services de paiement ou des activités d'émission et de gestion de monnaie électronique, avec ou sans nomination d'administrateur provisoire ;

6° Le retrait partiel d'agrément ;

7° Le retrait total d'agrément ou la radiation de la liste des personnes agréées, avec ou sans nomination d'un liquidateur.

Les sanctions mentionnées aux 3° et 4° ne peuvent, dans leur durée, excéder dix ans.

Pour les établissements de crédit, la sanction prévue au 6° ne peut être prononcée que pour les services ne relevant pas de l'agrément délivré par la Banque centrale européenne. Pour ces mêmes établissements et pour les activités qui entrent dans le champ de l'agrément délivré par la Banque centrale européenne, les sanctions prévues au 6° et au 7° prennent la forme respectivement d'une interdiction partielle ou totale d'activité prononcée à titre conservatoire.

Lorsque la commission des sanctions prononce l'interdiction totale d'activité d'un établissement de crédit, l'Autorité de contrôle prudentiel et de résolution propose à la Banque centrale européenne de prononcer le retrait de l'agrément. Dans le cas où la Banque centrale européenne ne prononce pas le retrait d'agrément, la commission des sanctions peut délibérer à nouveau et infliger une autre sanction parmi celles prévues au présent article.

Lorsque la procédure de sanction engagée peut conduire à l'application de sanctions à des dirigeants, la formation de l'Autorité qui a décidé de l'engagement de la procédure indique expressément, dans la notification de griefs, que les sanctions mentionnées aux 4° et 5° sont susceptibles d'être prononcées à l'encontre des dirigeants qu'elle désigne, en précisant les éléments susceptibles de fonder leur responsabilité directe et personnelle dans les manquements ou infractions en cause, et la commission des sanctions veille au respect à leur égard du caractère contradictoire de la procédure.

La commission des sanctions peut prononcer, soit à la place, soit en sus de ces sanctions, une sanction pécuniaire au plus égale à cent millions d'euros.

La commission des sanctions peut assortir la sanction d'une astreinte, dont elle fixe le montant et la date d'effet. Un décret en Conseil d'Etat fixe la procédure applicable, le montant journalier maximum de l'astreinte et les modalités selon lesquelles, en cas d'inexécution totale ou partielle ou de retard d'exécution, il est procédé à la liquidation de l'astreinte.

La commission des sanctions peut également prononcer les sanctions mentionnées au présent article s'il n'a pas été déféré aux injonctions prévues aux articles L. 511-41-3, L. 522-15-1 et L. 526-29 et aux exigences complémentaires prévues au deuxième alinéa de l'article L. 334-1 du code des assurances, ou au premier alinéa de l'article L. 352-3 du même code.

La décision de la commission des sanctions est rendue publique dans les publications, journaux ou supports qu'elle désigne, dans un format proportionné à la faute commise et à la sanction infligée. Les frais sont supportés par les personnes sanctionnées. Toutefois, lorsque la publication risque de perturber gravement les marchés financiers ou de causer un préjudice disproportionné aux parties en cause, la décision de la commission peut prévoir qu'elle ne sera pas publiée.
