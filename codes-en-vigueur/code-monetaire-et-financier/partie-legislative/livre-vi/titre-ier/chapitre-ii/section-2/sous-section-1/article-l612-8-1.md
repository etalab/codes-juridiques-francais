# Article L612-8-1

Le collège de résolution de l'Autorité de contrôle prudentiel et de résolution est composé de six membres :

1° Le gouverneur de la Banque de France ou son représentant, président ;

2° Le directeur général du Trésor ou son représentant ;

3° Le président de l'Autorité des marchés financiers ou son représentant ;

4° Le sous-gouverneur désigné par le gouverneur de la Banque de France, ou son représentant ;

5° Le président de la chambre commerciale, financière et économique de la Cour de cassation, ou son représentant ;

6° Le président du directoire du fonds de garantie des dépôts et de résolution ou son représentant.

Par dérogation aux dispositions des articles L. 612-12, L. 612-23 et L. 612-26, du second alinéa du I de l'article L. 612-19 et aux règles relatives à la direction des services prévues à l'article L. 612-15, un décret en Conseil d'Etat fixe les conditions d'organisation et de fonctionnement des services chargés de préparer les travaux du collège de résolution. Le directeur chargé de ces services est nommé par arrêté du ministre chargé de l'économie, sur proposition du président du collège de résolution. Il rapporte au collège de résolution.

Le collège de résolution ne peut délibérer que si la majorité de ses membres sont présents.

Ses décisions sont prises à la majorité des voix. En cas de partage égal des voix, celle du président est prépondérante.

Les décisions pouvant entraîner, immédiatement ou à terme, l'appel à des concours publics, quelle que soit la forme de ces concours, ne peuvent être adoptées qu'avec la voix du directeur général du Trésor ou de son représentant.

Les membres du collège de résolution et les services chargés de la préparation de ses travaux ont accès, pour l'exercice de leurs missions au sein de l'Autorité de contrôle prudentiel et de résolution, aux informations détenues par cette autorité pour l'exercice de ses missions de contrôle prudentiel.
