# Section 9 : Coopération

- [Sous-section 1 : Coopération avec les fonds de garantie](sous-section-1)
- [Sous-section 2 : Coordination en matière de supervision des relations entre les professions assujetties et leurs clientèles](sous-section-2)
