# Article L612-46

Les fonds de garantie mentionnés aux articles L. 312-4, L. 313-50 et L. 322-2 du présent code, L. 421-1 et L. 423-1 du code des assurances, L. 431-1 du code de la mutualité et L. 931-35 du code de la sécurité sociale sont consultés par l'Autorité de contrôle prudentiel et de résolution pour les décisions d'agrément des personnes relevant de leur champ d'intervention.
