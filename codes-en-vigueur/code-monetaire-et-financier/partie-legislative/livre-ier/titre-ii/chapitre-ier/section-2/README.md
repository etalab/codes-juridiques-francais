# Section 2 : La Monnaie de Paris

- [Article L121-3](article-l121-3.md)
- [Article L121-4](article-l121-4.md)
- [Article L121-5](article-l121-5.md)
- [Article L121-6](article-l121-6.md)
