# Article L142-9

Les agents de la Banque de France sont tenus au secret professionnel.

Ils ne peuvent prendre ou recevoir une participation ou quelque intérêt ou rémunération que ce soit par travail ou conseil dans une entreprise publique ou privée, industrielle, commerciale ou financière, sauf dérogation accordée par le gouverneur. Ces dispositions ne s'appliquent pas à la production des oeuvres scientifiques, littéraires ou artistiques.

Le conseil général de la Banque de France détermine, dans les conditions prévues par le troisième alinéa de l'article L. 142-2, les règles applicables aux agents de la Banque de France dans les domaines où les dispositions du code du travail sont incompatibles avec le statut ou avec les missions de service public dont elle est chargée.

Les articles L. 2323-19 et L. 2323-21 à L. 2323-24 et L. 2323-26 du code du travail et les articles L. 2323-78 à L2323-82 et L. 2323-86 du même code ne sont pas applicables à la Banque de France. L'article L. 2323-86 du code du travail ne s'applique pas aux personnes morales de droit privé sur lesquelles la Banque de France exerce une influence dominante au sens de l'article L. 2331-1 du même code.

Les dispositions du chapitre II du titre III du livre IV du même code autres que celles énumérées à l'alinéa précédent sont applicables à la Banque de France uniquement pour les missions et autres activités qui, en application de l'article L. 142-2 du présent code, relèvent de la compétence du conseil général.

Le comité d'entreprise et, le cas échéant, les comités d'établissement de la Banque de France ne peuvent faire appel à l'expert visé à l'article L. 2325-35 du code du travail que lorsque la procédure prévue aux articles L. 1233-29 et L. 1233-30 du même code est mise en oeuvre.

Les conditions dans lesquelles s'applique à la Banque de France les articles L. 2323-83 et L. 2323-87 du même code sont fixées par un décret en Conseil d'Etat.
