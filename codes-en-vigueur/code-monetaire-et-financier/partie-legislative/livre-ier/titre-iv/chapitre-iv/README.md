# Chapitre IV : Dispositions diverses

- [Article L144-1](article-l144-1.md)
- [Article L144-2](article-l144-2.md)
- [Article L144-2-1](article-l144-2-1.md)
- [Article L144-3](article-l144-3.md)
- [Article L144-4](article-l144-4.md)
- [Article L144-5](article-l144-5.md)
