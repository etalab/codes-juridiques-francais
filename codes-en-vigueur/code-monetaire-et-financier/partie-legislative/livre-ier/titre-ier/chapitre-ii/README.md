# Chapitre II : Règles d'usage de la monnaie

- [Section 1 : L'indexation](section-1)
- [Section 3 : Interdiction du paiement en espèces de certaines créances](section-3)
- [Section 4 : Mode de paiement du salaire](section-4)
- [Section 5 : Frais ou réduction pour l'usage d'un instrument de paiement donné](section-5)
