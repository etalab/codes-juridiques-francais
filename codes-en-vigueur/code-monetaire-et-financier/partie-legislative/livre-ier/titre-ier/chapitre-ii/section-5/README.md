# Section 5 : Frais ou réduction pour l'usage d'un instrument de paiement donné

- [Article L112-11](article-l112-11.md)
- [Article L112-12](article-l112-12.md)
