# Article L133-1

I.-Les dispositions du présent chapitre s'appliquent aux opérations de paiement réalisées par les prestataires de services de paiement mentionnés au livre V dans le cadre des activités définies au II de l'article L. 314-1.

II.-A l'exception de celles du I de l'article L. 133-14, les dispositions du présent chapitre s'appliquent si le prestataire de services de paiement du bénéficiaire et celui du payeur sont situés sur le territoire de la France métropolitaine, dans les départements d'outre-mer, dans le Département de Mayotte, à Saint-Martin, à Saint-Barthélemy ou à Saint-Pierre-et-Miquelon et que l'opération est réalisée en euros.

A l'exception de celles du I de l'article L. 133-14, les dispositions du présent chapitre s'appliquent également si le prestataire de services de paiement du bénéficiaire et celui du payeur sont situés, l'un sur le territoire de la France métropolitaine, dans les départements d'outre-mer, dans le Département de Mayotte ou à Saint-Martin, l'autre sur le territoire de la France métropolitaine, dans les départements d'outre-mer, dans le Département de Mayotte, à Saint-Martin ou dans un autre Etat membre de  l'Union européenne ou dans un autre Etat partie à l'accord sur l'Espace économique européen, et que l'opération est réalisée en euros ou dans la devise d'un Etat membre de  l'Union européenne ou d'un autre Etat partie à l'accord sur l'Espace économique européen qui n'appartient pas à la zone euro.

III.-Les dispositions du présent chapitre ne s'appliquent pas aux opérations de paiement effectuées entre prestataires de services de paiement pour leur propre compte.

IV.-Sans préjudice de l'application de la section 12, le présent chapitre s'applique à l'émission et la gestion de monnaie électronique.
