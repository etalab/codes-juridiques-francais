# Chapitre II : Dispositions générales

- [Section 1 : Obligations de publicité](section-1)
- [Section 2 : Interdictions et sanctions](section-2)
