# Article L451-3

Les opérations de rachat d'actions prévues par l'article L. 225-209 du code de commerce ne sont pas soumises aux dispositions du VII de l'article L. 621-8 du présent code.

Dans les conditions et selon les modalités fixées par le règlement général de l'Autorité des marchés financiers, toute société dont des actions sont admises aux négociations sur un marché réglementé qui souhaite procéder au rachat de ses propres titres de capital informe préalablement le marché.
