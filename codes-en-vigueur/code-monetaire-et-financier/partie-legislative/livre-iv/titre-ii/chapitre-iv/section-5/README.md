# Section 5 : Obligations de transparence avant et après négociation

- [Article L424-7](article-l424-7.md)
- [Article L424-8](article-l424-8.md)
