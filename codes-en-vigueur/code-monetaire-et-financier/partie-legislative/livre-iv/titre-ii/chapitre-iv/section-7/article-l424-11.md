# Article L424-11

Tout système existant à la date du 1er novembre 2007 relevant de la définition d'un système multilatéral de négociation, géré par une entreprise de marché, est réputé autorisé, à condition qu'il soit conforme aux dispositions du présent code et du règlement général de l'Autorité des marchés financiers et que l'entreprise de marché en fasse la demande à l'Autorité des marchés financiers au plus tard le 30 avril 2009.
