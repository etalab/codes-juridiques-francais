# Sous-section 1 : Obligations des dirigeants et des actionnaires d'entreprises de marché

- [Article L421-7](article-l421-7.md)
- [Article L421-8](article-l421-8.md)
- [Article L421-9](article-l421-9.md)
