# Article L421-19

Les entreprises de marché ne peuvent limiter le nombre de prestataires de services d'investissement sur le marché dont elles ont la charge.

L'Autorité des marchés financiers veille à ce que les entreprises de marché adaptent, en tant que de besoin, leur capacité technique aux demandes d'accès dont elles font l'objet.
