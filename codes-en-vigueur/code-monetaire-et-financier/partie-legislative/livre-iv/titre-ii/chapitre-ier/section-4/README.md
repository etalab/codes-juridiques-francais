# Section 4 : Admission aux négociations, suspension, radiation et retrait des instruments financiers

- [Article L421-14](article-l421-14.md)
- [Article L421-15](article-l421-15.md)
- [Article L421-16](article-l421-16.md)
- [Article L421-16-1](article-l421-16-1.md)
- [Article L421-16-2](article-l421-16-2.md)
