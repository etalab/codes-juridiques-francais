# Article L433-5

Les articles L. 433-1 à L. 433-4 sont applicables aux sociétés dont les instruments financiers ont cessé d'être admis aux négociations sur un marché réglementé pour être admis aux négociations sur un système multilatéral de négociation qui se soumet aux dispositions législatives ou réglementaires visant à protéger les investisseurs contre les opérations d'initiés, les manipulations de cours et la diffusion de fausses informations pendant une durée de trois ans à compter de la date à laquelle ces instruments financiers ont cessé d'être admis aux négociations sur un marché réglementé.

L'alinéa précédent est applicable aux sociétés dont la capitalisation boursière est inférieure à un milliard d'euros.
