# Chapitre III : Opérations spécifiques aux marchés réglementés

- [Section 1 : Offres publiques d'achat et d'échange](section-1)
- [Section 2 : Obligation de déposer un projet d'offre publique](section-2)
- [Section 3 : Offres publiques de retrait et retrait obligatoire](section-3)
- [Section 4 : Dispositions applicables aux sociétés dont les instruments financiers ont cessé d'être négociés sur un marché réglementé](section-4)
