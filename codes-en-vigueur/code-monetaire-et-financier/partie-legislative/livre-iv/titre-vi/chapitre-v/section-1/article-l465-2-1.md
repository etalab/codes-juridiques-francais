# Article L465-2-1

Est puni des peines prévues au premier alinéa de l'article L. 465-1 le fait :

-pour toute personne de transmettre des données ou des informations fausses ou trompeuses utilisées pour calculer un indice défini au dernier alinéa du présent article ou de nature à fausser le cours d'un instrument ou d'un actif auquel serait lié cet indice, lorsque la personne ayant transmis les données ou les informations savait ou aurait dû savoir qu'elles étaient fausses ou trompeuses ;

-pour toute personne d'adopter tout autre comportement aboutissant à la manipulation du calcul d'un indice.

Constitue un indice toute donnée diffusée calculée à partir de la valeur ou du prix, constaté ou estimé, d'un ou plusieurs sous-jacents, d'un ou plusieurs taux d'intérêt constatés ou estimés, ou de toute autre valeur ou mesure, et par référence à laquelle est déterminé le montant payable au titre d'un instrument financier ou la valeur d'un instrument financier.
