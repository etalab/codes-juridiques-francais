# Titre VIII : Dispositions d'ordre et diverses.

- [Paragraphe Ier : Concession et révision de la pension.](paragraphe-ier)
- [Paragraphe II : Dispositions diverses.](paragraphe-ii)
