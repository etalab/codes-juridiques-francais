# Article L33

Le fonctionnaire dont la mise à la retraite a été prononcée en vertu des articles L. 27 ou L. 29 et qui est reconnu, après avis de la commission de réforme prévue à l'article L. 31, apte à reprendre l'exercice de ses fonctions, peut être réintégré dans un emploi de son grade s'il existe une vacance. La pension et, le cas échéant, la rente viagère d'invalidité prévue à l'article L. 28 sont annulées à compter de la date d'effet de la réintégration.
