# Article D39 bis

La pension dont le montant total mensuel brut est inférieur au douzième de la somme prévue à l'article R. 351-26 du code de la sécurité sociale et revalorisée selon les modalités fixées par cet article est payée annuellement et à terme échu.

Le titulaire d'une pension mentionnée au premier alinéa peut toutefois opter de manière irrévocable, dans un délai d'un an à compter de la date de la liquidation de la pension, pour le versement d'un capital égal à quinze fois le montant annuel de cette pension. Ce capital est réduit, le cas échéant, de la somme des pensions déjà payées à la date de son versement.
