# Article L252-3

L'admission à l'aide médicale de l'Etat des personnes relevant du premier alinéa de l'article L. 251-1 est prononcée, dans des conditions définies par décret, par le représentant de l'Etat dans le département, qui peut déléguer ce pouvoir au directeur de la caisse primaire d'assurance maladie des travailleurs salariés.

Cette admission est accordée pour une période d'un an. Toutefois le service des prestations est conditionné au respect de la stabilité de la résidence en France, dans des conditions fixées par décret en Conseil d'Etat.

Les organismes mentionnés aux articles L. 211-1 et L. 752-4 du code de la sécurité sociale peuvent obtenir le remboursement des prestations qu'ils ont versées à tort. En cas de précarité de la situation du demandeur, la dette peut être remise ou réduite.
