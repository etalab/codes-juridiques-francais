# Chapitre IV : Prise en charge des soins urgents

- [Article L254-1](article-l254-1.md)
- [Article L254-2](article-l254-2.md)
