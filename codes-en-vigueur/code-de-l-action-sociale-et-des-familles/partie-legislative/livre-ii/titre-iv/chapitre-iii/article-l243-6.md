# Article L243-6

L'Etat assure aux organismes gestionnaires des établissements et services d'aide par le travail, dans des conditions fixées par décret, la compensation totale des charges et des cotisations afférentes à la partie de la rémunération garantie égale à l'aide au poste mentionnée à l'article L. 243-4.
