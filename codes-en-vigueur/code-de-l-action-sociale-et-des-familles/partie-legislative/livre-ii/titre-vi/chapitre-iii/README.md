# Chapitre III : Actions d'insertion

- [Section 1 : Organisation départementale du dispositif d'insertion.](section-1)
- [Section 2 : Fonds d'aide aux jeunes en difficulté.](section-2)
- [Section 3 : Dispositions communes.](section-3)
