# Section 2 : Prestation de revenu de solidarité active

- [Sous-section 1 : Conditions d'ouverture du droit](sous-section-1)
- [Sous-section 2 : Attribution et service de la prestation](sous-section-2)
- [Sous-section 3 : Financement du revenu de solidarité active](sous-section-3)
