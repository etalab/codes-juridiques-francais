# Article L224-5

Lorsqu'un enfant est recueilli par le service de l'aide sociale à l'enfance dans les cas mentionnés aux 1°, 2°, 3° et 4° de l'article L. 224-4, un procès-verbal est établi.

Il doit être mentionné au procès-verbal que les parents à l'égard de qui la filiation de l'enfant est établie, la mère ou le père de naissance de l'enfant ou la personne qui remet l'enfant ont été informés :

1° Des mesures instituées, notamment par l'Etat, les collectivités territoriales et les organismes de sécurité sociale pour aider les parents à élever eux-mêmes leurs enfants ;

2° Des dispositions du régime de la tutelle des pupilles de l'Etat suivant le présent chapitre ;

3° Des délais et conditions suivant lesquels l'enfant pourra être repris par ses père ou mère ainsi que des modalités d'admission en qualité de pupille de l'Etat mentionnées à l'article L. 224-8 ;

4° De la possibilité de laisser tous renseignements concernant la santé des père et mère, les origines de l'enfant, les raisons et les circonstances de sa remise au service de l'aide sociale à l'enfance.

De plus, lorsque l'enfant est remis au service par ses père ou mère, selon les 2° ou 3° de l'article L. 224-4, ceux-ci doivent être invités à consentir à son adoption ; le consentement est porté sur le procès-verbal ; celui-ci doit également mentionner que les parents ont été informés des délais et conditions dans lesquels ils peuvent rétracter leur consentement, selon les deuxième et troisième alinéas de l'article 348-3 du code civil.
