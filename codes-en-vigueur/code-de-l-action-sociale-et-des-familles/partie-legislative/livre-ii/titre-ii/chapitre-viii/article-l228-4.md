# Article L228-4

Sous réserve des dispositions du deuxième alinéa du présent article, les prestations d'aide sociale à l'enfance mentionnées au chapitre II du présent titre sont à la charge du département qui a prononcé l'admission dans le service de l'aide sociale à l'enfance.

Les dépenses mentionnées à l'article L. 228-3 sont prises en charge par le département du ressort de la juridiction qui a prononcé la mesure en première instance, nonobstant tous recours éventuels contre les décisions correspondantes, dans les conditions suivantes :

1° Les dépenses mentionnées au 2° de l'article L. 228-3 sont prises en charge par le département auquel le mineur est confié par l'autorité judiciaire ;

2° Les autres dépenses mentionnées à l'article L. 228-3 résultant de mesures prononcées en première instance par l'autorité judiciaire sont prises en charge par le département sur le territoire duquel le mineur est domicilié ou sur le territoire duquel sa résidence a été fixée.

Lorsque, pendant l'exécution de la mesure, la juridiction décide de se dessaisir du dossier au profit d'une autre juridiction, elle porte cette décision à la connaissance des présidents des conseils départementaux concernés. Le département du ressort de la juridiction désormais saisie prend en charge les frais afférents à l'exécution de la mesure dans les conditions fixées par le deuxième alinéa du présent article.

Le département chargé de la prise en charge financière d'une mesure, en application des deuxième et troisième alinéas ci-dessus, assure celle-ci selon le tarif en vigueur dans le département où se trouve le lieu de placement de l'enfant.
