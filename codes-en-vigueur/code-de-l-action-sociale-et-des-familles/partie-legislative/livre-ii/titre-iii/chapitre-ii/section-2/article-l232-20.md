# Article L232-20

Les recours contre les décisions relatives à l'allocation personnalisée d'autonomie sont formés devant les commissions départementales mentionnées à l'article L. 134-6, dans des conditions et selon les modalités prévues aux articles L. 134-1 à L. 134-10.

Lorsque le recours est relatif à l'appréciation du degré de perte d'autonomie, la commission départementale mentionnée à l'article L. 134-6 recueille l'avis d'un médecin titulaire d'un diplôme universitaire de gériatrie ou d'une capacité en gérontologie et gériatrie, choisi par son président sur une liste établie par le conseil départemental de l'ordre des médecins.
