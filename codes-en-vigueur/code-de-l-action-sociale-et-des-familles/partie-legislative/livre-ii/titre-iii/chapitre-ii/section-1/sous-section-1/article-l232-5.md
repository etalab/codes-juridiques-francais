# Article L232-5

Pour l'application de l'article L. 232-3, sont considérées comme résidant à domicile les personnes accueillies dans les conditions fixées par les articles L. 441-1 à L. 443-10 ou hébergées dans un établissement visé au II de l'article L. 313-12.
