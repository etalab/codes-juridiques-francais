# Article L542-3

Pour l'application du titre III du livre II :

I. ― L'article L. 231-1 est ainsi modifié :

1° Au deuxième alinéa, les mots : " l'allocation simple et " sont supprimés ;

2° Au quatrième alinéa, les mots : " le taux de l'allocation simple " sont supprimés.

II. ― Pour l'application du chapitre II :

1° A l'article L. 232-1, les mots : " dans des conditions identiques sur l'ensemble du territoire national " sont complétés par les mots : " sous réserve des adaptations prévues au présent chapitre " ;

2° Le deuxième alinéa de l'article L. 232-6 est ainsi modifié :

a) Après les mots : " sauf refus exprès du bénéficiaires ", sont ajoutés les mots : " ou absence d'offre de service organisée, " ;

b) Les mots : " agréé dans les conditions fixées à l'article L. 129-1 du code du travail " sont remplacés par les mots : " autorisé au titre du 1° de l'article L. 313-1-2 " ;

3° Les deuxième et troisième alinéas de l'article L. 232-7 ne sont pas applicables ;

4° A l'article L. 232-11, les mots : " au livre Ier " sont remplacés par les mots : " au chapitre Ier du titre IV du livre V " ;

5° A l'article L. 232-13, les mots : " agréés dans les conditions prévues à l'article L. 129-1 du code du travail " sont remplacés par les mots : " autorisé au titre du 1° de l'article L. 313-1-2 " ;

6° A l'article L. 232-15 :

a) Le premier alinéa est remplacé par les dispositions suivantes :

La part de l'allocation personnalisée d'autonomie destinée à rémunérer un service d'aide à domicile peut être versée directement au service d'aide à domicile choisi par le bénéficiaire, qui demeure libre de modifier son choix à tout moment ;

b) Le troisième alinéa n'est pas applicable ;

7° A l'article L. 232-20, les mots : " le conseil départemental de l'ordre des médecins " sont remplacés par les mots : " le conseil de l'ordre des médecins de Mayotte " ;

8° A l'article L. 232-23, les mots : " ni avec la majoration pour aide constante d'une tierce personne prévue à l'article L. 355-1 du code de la sécurité sociale, ni avec la prestation complémentaire pour recours à tierce personne prévue à l'article L. 434-2 du même code " ne sont pas applicables.
