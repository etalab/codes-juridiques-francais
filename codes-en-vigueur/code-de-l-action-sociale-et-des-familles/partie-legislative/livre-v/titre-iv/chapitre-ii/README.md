# Chapitre II : Adaptations du livre II

- [Section 1 : Famille](section-1)
- [Section 2 : Enfance](section-2)
- [Section 3 : Personnes âgées](section-3)
- [Section 4 : Personnes handicapées](section-4)
- [Section 5 : Accès aux soins des personnes étrangères en situation irrégulière](section-5)
- [Section 6 : Lutte contre la pauvreté et les exclusions](section-6)
