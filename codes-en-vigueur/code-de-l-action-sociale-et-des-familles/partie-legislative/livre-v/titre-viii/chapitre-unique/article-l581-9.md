# Article L581-9

Les dispositions des articles L. 522-12, L. 522-14 et L. 522-16 sont applicables à Saint-Barthélemy et à Saint-Martin.
