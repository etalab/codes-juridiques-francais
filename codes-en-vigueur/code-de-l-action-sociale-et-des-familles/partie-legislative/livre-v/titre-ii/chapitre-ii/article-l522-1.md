# Article L522-1

Dans chaque département d'outre-mer, une agence d'insertion, établissement public départemental à caractère administratif, assure les missions suivantes :

1° Elle exerce les compétences relatives aux décisions individuelles concernant le revenu de solidarité active, ainsi qu'au contrat d'engagements réciproques en matière d'insertion sociale ou professionnelle mentionné à l'article L. 262-36 ;

2° Elle concourt à l'élaboration du programme départemental d'insertion prévu à l'article L. 263-1 et le met en œuvre ;

3° Elle est associée à l'élaboration du pacte territorial d'insertion prévu à l'article L. 263-2 et participe à sa mise en œuvre ;

4° Elle conclut les contrats d'insertion par l'activité mentionnés à l'article L. 522-8 et établit le programme annuel de tâches d'utilité sociale auxquelles les titulaires de ces contrats sont affectés.

Toutefois, le conseil général peut décider d'exercer tout ou partie des compétences mentionnées aux alinéas précédents, le cas échéant dans le cadre de délégations à d'autres organismes, dans les conditions définies par l'article L. 121-6 et le chapitre II du titre VI du livre II du présent code et par l'article L. 5134-19-2 du code du travail. Lorsque le conseil général décide d'exercer la totalité de ces compétences, l'agence d'insertion est supprimée.
