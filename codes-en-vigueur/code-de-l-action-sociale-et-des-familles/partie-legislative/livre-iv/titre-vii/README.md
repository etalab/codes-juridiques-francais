# Titre VII : Mandataires judiciaires à la protection des majeurs et délégués aux prestations familiales

- [Chapitre Ier : Dispositions communes aux mandataires judiciaires à la protection des majeurs](chapitre-ier)
- [Chapitre II : Personnes physiques mandataires judiciaires à la protection des majeurs](chapitre-ii)
- [Chapitre III : Dispositions pénales communes aux mandataires judiciaires à la protection des majeurs](chapitre-iii)
- [Chapitre IV : Délégués aux prestations familiales](chapitre-iv)
