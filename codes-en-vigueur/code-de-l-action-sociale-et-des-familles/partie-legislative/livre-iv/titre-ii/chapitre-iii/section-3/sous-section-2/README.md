# Sous-section 2 : Dispositions applicables aux seuls assistants maternels employés par des particuliers

- [Article L423-23](article-l423-23.md)
- [Article L423-24](article-l423-24.md)
- [Article L423-25](article-l423-25.md)
- [Article L423-26](article-l423-26.md)
- [Article L423-27](article-l423-27.md)
