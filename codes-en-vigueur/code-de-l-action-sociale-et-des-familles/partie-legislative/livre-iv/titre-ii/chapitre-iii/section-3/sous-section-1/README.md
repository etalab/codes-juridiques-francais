# Sous-section 1 : Dispositions communes à tous les assistants maternels

- [Article L423-17](article-l423-17.md)
- [Article L423-18](article-l423-18.md)
- [Article L423-19](article-l423-19.md)
- [Article L423-20](article-l423-20.md)
- [Article L423-21](article-l423-21.md)
- [Article L423-22](article-l423-22.md)
