# Section 3 : Assistants maternels

- [Sous-section 1 : Dispositions communes à tous les assistants maternels](sous-section-1)
- [Sous-section 2 : Dispositions applicables aux seuls assistants maternels employés par des particuliers](sous-section-2)
- [Sous-section 3 : Dispositions applicables aux seuls assistants maternels employés par des personnes morales de droit privé](sous-section-3)
