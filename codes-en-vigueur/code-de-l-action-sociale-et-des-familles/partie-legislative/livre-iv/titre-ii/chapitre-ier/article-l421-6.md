# Article L421-6

Lorsque la demande d'agrément concerne l'exercice de la profession d'assistant maternel, la décision du président du conseil départemental est notifiée dans un délai de trois mois à compter de cette demande. A défaut de notification d'une décision dans ce délai, l'agrément est réputé acquis.

Lorsque la demande d'agrément concerne l'exercice de la profession d'assistant familial, la décision du président du conseil départemental est notifiée dans un délai de quatre mois à compter de cette demande. A défaut de notification d'une décision dans ce délai, l'agrément est réputé acquis, ce délai pouvant être prolongé de deux mois suite à une décision motivée du président du conseil départemental.

Si les conditions de l'agrément cessent d'être remplies, le président du conseil départemental peut, après avis d'une commission consultative paritaire départementale, modifier le contenu de l'agrément ou procéder à son retrait. En cas d'urgence, le président du conseil départemental peut suspendre l'agrément. Tant que l'agrément reste suspendu, aucun enfant ne peut être confié.

Toute décision de retrait de l'agrément, de suspension de l'agrément ou de modification de son contenu doit être dûment motivée et transmise sans délai aux intéressés.

La composition, les attributions et les modalités de fonctionnement de la commission présidée par le président du conseil départemental ou son représentant, mentionnée au troisième alinéa, sont définies par voie réglementaire.

La commission est notamment consultée chaque année sur le programme de formation des assistants maternels et des assistants familiaux ainsi que sur le bilan de fonctionnement de l'agrément.
