# Article L424-6

Le ménage ou la personne qui emploie un assistant maternel assurant l'accueil d'un mineur dans une maison d'assistants maternels perçoit le complément de libre choix du mode de garde dans les conditions prévues à l'article L. 531-5 du code de la sécurité sociale.
