# Article L424-1

Par dérogation à l'article L. 421-1, l'assistant maternel peut accueillir des mineurs au sein d'une maison d'assistants maternels.

Le nombre d'assistants maternels pouvant exercer dans une même maison ne peut excéder quatre.
