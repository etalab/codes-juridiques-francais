# Article L14-10-4

Les produits affectés à la Caisse nationale de solidarité pour l'autonomie sont constitués par :

1° Une contribution au taux de 0,3 % due par les employeurs privés et publics. Cette contribution a la même assiette que les cotisations patronales d'assurance maladie affectées au financement des régimes de base de l'assurance maladie. Elle est recouvrée dans les mêmes conditions et sous les mêmes garanties que lesdites cotisations ;

1° bis Une contribution au taux de 0,3 % due sur les avantages de retraite et d'invalidité ainsi que sur les allocations de préretraite qui ne sont pas assujetties à la contribution mentionnée au 2° et sont perçues par les personnes dont le montant des revenus de l'avant-dernière année, définis au IV de l'article 1417 du code général des impôts, est supérieur ou égal au seuil mentionné au 2° du III de l'article L. 136-8 du code de la sécurité sociale. Elle est recouvrée et contrôlée selon les règles, garanties et sanctions mentionnées pour les mêmes revenus à l'article L. 136-5 du même code.

Sont exonérées de la contribution mentionnée au premier alinéa du présent 1° bis les pensions mentionnées au a du 4° et aux 12°, 14° et 14° bis de l'article 81 du code général des impôts et les personnes titulaires d'un avantage de vieillesse ou d'invalidité non contributif attribué par le service mentionné au deuxième alinéa de l'article L. 815-7 du code de la sécurité sociale ou par un régime de base de sécurité sociale sous les conditions de ressources mentionnées à l'article L. 815-9 du même code ;

2° Une contribution additionnelle au prélèvement social mentionné à l'article L. 245-14 du code de la sécurité sociale et une contribution additionnelle au prélèvement social mentionné à l'article L. 245-15 du même code. Ces contributions additionnelles sont assises, contrôlées, recouvrées et exigibles dans les mêmes conditions et sous les mêmes sanctions que celles applicables à ces prélèvements sociaux. Leur taux est fixé à 0,3 % ;

3° Une fraction de 0,1 point du produit des contributions sociales mentionnées aux articles L. 136-1,
L. 136-6, L. 136-7 et L. 136-7-1 du même code ;

4° Une participation des régimes obligatoires de base de l'assurance vieillesse, représentative d'une fraction identique pour tous les régimes, déterminée par voie réglementaire, des sommes consacrées par chacun de ceux-ci en 2000 aux dépenses d'aide ménagère à domicile au bénéfice des personnes âgées dépendantes remplissant la condition de perte d'autonomie mentionnée à l'article L. 232-2 du code de l'action sociale et des familles ; cette fraction ne peut être inférieure à la moitié ni supérieure aux trois quarts des sommes en cause. Le montant de cette participation est revalorisé chaque année, par arrêté du ministre chargé de la sécurité sociale, conformément à l'évolution prévisionnelle des prix à la consommation hors tabac prévue dans le rapport économique et financier annexé à la loi de finances pour l'année considérée ;

5° La contribution des régimes d'assurance maladie mentionnée au deuxième alinéa de l'article L. 314-3. Cette contribution est répartie entre les régimes au prorata des charges qui leur sont imputables au titre du I de l'article L. 14-10-5 ;

6° Une part, fixée à l'article L. 131-8 du code de la sécurité sociale, du produit du droit de consommation sur les tabacs. La répartition de cette part entre les sections mentionnées à l'article L. 14-10-5 du présent code est fixée par un arrêté conjoint des ministres chargés des personnes âgées, des personnes handicapées et du budget.
