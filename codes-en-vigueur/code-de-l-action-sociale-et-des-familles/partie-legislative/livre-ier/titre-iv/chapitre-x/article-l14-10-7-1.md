# Article L14-10-7-1

Pour l'application des articles L. 14-10-6 et L. 14-10-7, le potentiel fiscal utilisé est majoré ou, le cas échéant, minoré de la fraction de correction prévue au 4 du III de l'article L. 3335-3 du code général des collectivités territoriales.
