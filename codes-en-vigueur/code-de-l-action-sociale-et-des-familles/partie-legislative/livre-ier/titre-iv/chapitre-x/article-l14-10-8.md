# Article L14-10-8

I. ― Les crédits affectés, au titre d'un exercice, aux sections et sous-sections mentionnées à l'article L. 14-10-5, qui n'ont pas été consommés à la clôture de l'exercice, donnent lieu à report automatique sur les exercices suivants.

II. ― Les produits résultant du placement, dans des conditions définies par décret en Conseil d'Etat, des disponibilités qui excèdent les besoins de trésorerie de la caisse sont affectés au financement des charges mentionnées aux II et III de l'article L. 14-10-5.
