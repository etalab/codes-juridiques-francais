# Article L148-2

Il est institué une Autorité centrale pour l'adoption chargée d'orienter et de coordonner l'action des administrations et des autorités compétentes en matière d'adoption internationale.

Un décret en Conseil d'Etat précise les conditions d'application du présent article.
