# Article L144-1

L'Observatoire national de la pauvreté et de l'exclusion sociale placé auprès du ministre chargé des affaires sociales est chargé de rassembler, analyser et diffuser les informations et données relatives aux situations de précarité, de pauvreté et d'exclusion sociale ainsi qu'aux politiques menées en ce domaine.

Il fait réaliser des travaux d'études, de recherche et d'évaluation quantitatives et qualitatives en lien étroit avec le Conseil national des politiques de lutte contre la pauvreté et l'exclusion sociale. Ces travaux mentionnent la proportion d'hommes et de femmes respectivement touchés par la pauvreté et l'exclusion. Les administrations de l'Etat, des collectivités territoriales et des établissements publics sont tenues de communiquer à l'observatoire les éléments qui lui sont nécessaires pour la poursuite de ses buts sous réserve de l'application des dispositions législatives imposant une obligation de secret.

Il contribue au développement de la connaissance et des systèmes d'information dans les domaines mal couverts, en liaison notamment avec les banques de données et organismes régionaux, nationaux et internationaux.

Il élabore chaque année, à destination du Premier ministre et du Parlement, un rapport synthétisant les travaux d'études, de recherche et d'évaluation réalisés aux niveaux national et régionaux. Ce rapport est rendu public.

Un décret en Conseil d'Etat détermine la composition, les missions et les modalités de fonctionnement de l'observatoire.
