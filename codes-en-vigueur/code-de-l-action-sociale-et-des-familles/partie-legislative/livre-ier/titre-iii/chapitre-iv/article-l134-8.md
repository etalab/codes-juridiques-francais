# Article L134-8

L'appel contre la décision de la commission départementale est suspensif, dans les cas où cette décision prononce l'admission au bénéfice de l'aide sociale aux personnes âgées ou aux personnes handicapées d'une personne à laquelle cette admission aurait été refusée par suite d'une décision de la commission centrale d'aide sociale.
