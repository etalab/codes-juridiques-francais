# Article Annexe 2-1

GUIDE D'ÉVALUATION DE LA PERSONNE ÂGÉE EN PERTE D'AUTONOMIE

<table>
<tbody>
<tr>
<td valign="top">
<p>Nom de naissance (en majuscules) :</p>
</td>
<td valign="top">
<p>Prénom :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>Nom marital (s'il y a lieu) :</p>
</td>
<td valign="top">
<p>Date de naissance :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>Adresse :</p>
</td>
<td valign="top"/>
</tr>
<tr>
<td valign="top">
<p>Numéro de téléphone :</p>
</td>
<td valign="top">
<p>Numéro de digicode :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>- 1<sup>ère</sup> demande</p>
</td>
<td valign="top">
<p>- renouvellement</p>
</td>
</tr>
<tr>
<td valign="top">
<p>Date de la visite :</p>
</td>
<td valign="top"/>
</tr>
<tr>
<td valign="top">
<p>Effectuée par :</p>
</td>
<td valign="top"/>
</tr>
<tr>
<td valign="top">
<p>Médecin traitant :</p>
</td>
<td valign="top"/>
</tr>
</tbody>
</table>

.

<table>
<thead>
<tr>
<td colspan="4" width="455">
<p align="center">SOINS ET AIDES EXISTANTS</p>
</td>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">hébergement en établissement</p>
</td>
<td valign="top">
<br/>
<p align="center">avec convention tripartite</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
<td valign="top">
<br/>
<p align="center">sans convention tripartite</p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">soins par des infirmiers libéraux</p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">service de soins infirmiers à domicile</p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">aide-ménagère (en heures par mois)</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">garde à domicile (en heures par mois)</p>
</td>
<td valign="top">
<br/>
<p align="center">jour</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
<td valign="top">
<br/>
<p align="center">nuit</p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">portage de repas (en nombre par mois)</p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">téléalarme</p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">dépannage</p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">autre service (précisez)</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">aides techniques</p>
</td>
<td colspan="2" valign="top">
<br/>
<p align="center">fauteuil roulant</p>
</td>
</tr>
<tr>
<td rowspan="6" valign="top">
<br/>
<br/>
</td>
<td colspan="2" valign="top">
<br/>
<p align="center">cannes</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">déambulateur</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">lit médicalisé</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">lève-malade</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">matériel à usage unique pour incontinence</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">autres (précisez)</p>
</td>
</tr>
</tbody>
</table>

GRILLE NATIONALE AGGIR

<table>
<tbody>
<tr>
<td colspan="2" width="455">
<p align="center">VARIABLES DISCRIMINANTES</p>
</td>
</tr>
</tbody>
<tbody>
<tr>
<td valign="top">
<p>COHERENCE :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>converser et/ou se comporter de façon sensée</p>
</td>
</tr>
<tr>
<td valign="top">
<p>ORIENTATION :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>se repérer dans le temps, les moments de la journée et dans les lieux</p>
</td>
</tr>
<tr>
<td valign="top">
<p>TOILETTE :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>concerne l'hygiène corporelle</p>
</td>
</tr>
<tr>
<td valign="top">
<p>HABILLAGE :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>s'habiller, se déshabiller, se présenter</p>
</td>
</tr>
<tr>
<td valign="top">
<p>ALIMENTATION :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>manger les aliments préparés</p>
</td>
</tr>
<tr>
<td valign="top">
<p>ELIMINATION :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>assumer l'hygiène de l'élimination urinaire et fécale</p>
</td>
</tr>
<tr>
<td valign="top">
<p>TRANSFERTS :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>se lever, se coucher, s'asseoir</p>
</td>
</tr>
<tr>
<td valign="top">
<p>DEPLACEMENT A L'INTERIEUR :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>avec ou sans canne, déambulateur, fauteuil roulant...</p>
</td>
</tr>
<tr>
<td valign="top">
<p>DEPLACEMENT A L'EXTERIEUR :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>à partir de la porte d'entrée sans moyen de transport</p>
</td>
</tr>
<tr>
<td valign="top">
<p>COMMUNICATION A DISTANCE :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>utiliser les moyens de communication, téléphone, sonnette, alarme...</p>
</td>
</tr>
</tbody>
</table>

.

<table>
<thead>
<tr>
<td colspan="2" width="455">
<p align="center">VARIABLES ILLUSTRATIVES</p>
</td>
</tr>
<tr>
<td colspan="2" width="455">
<br/>
<p align="center">(PERTE D'AUTONOMIE DOMESTIQUE ET SOCIALE)</p>
</td>
</tr>
</thead>
<tbody>
<tr>
<td valign="top">
<p>GESTION :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>gérer ses propres affaires, son budget, ses biens</p>
</td>
</tr>
<tr>
<td valign="top">
<p>CUISINE :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>préparer ses repas et les conditionner pour être servis</p>
</td>
</tr>
<tr>
<td valign="top">
<p>MENAGE :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>effectuer l'ensemble des travaux ménagers</p>
</td>
</tr>
<tr>
<td valign="top">
<p>TRANSPORT :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>prendre et/ou commander un moyen de transport</p>
</td>
</tr>
<tr>
<td valign="top">
<p>ACHATS :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>acquisition directe ou par correspondance</p>
</td>
</tr>
<tr>
<td valign="top">
<p>SUIVI DU TRAITEMENT :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>se conformer à l'ordonnance du médecin</p>
</td>
</tr>
<tr>
<td valign="top">
<p>ACTIVITES DE TEMPS LIBRE :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>activités sportives, culturelles, sociales, de loisir ou de passe-temps</p>
</td>
</tr>
</tbody>
</table>

GROUPE ISO-RESSOURCES

<table>
<tbody>
<tr>
<td colspan="3" valign="top">
<p align="center">ENTOURAGE</p>
</td>
</tr>
<tr>
<td rowspan="8" valign="top">
<br/>
<p align="center">réseaux personnels</p>
</td>
<td colspan="2" valign="top">
<br/>
<p align="center">vit seule</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">vit avec une personne dépendante</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">vit avec une personne autonome</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">sans contact avec sa famille</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">sans voisinage ou voisinage indifférent</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">commerces inaccessibles</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">absence de réseaux personnels</p>
</td>
<td valign="top">
<br/>
<p align="center">le week-end</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<br/>
</td>
<td valign="top">
<br/>
<p align="center">les vacances</p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top">
<br/>
<p align="center">absence de réseaux professionnels</p>
</td>
<td colspan="2" valign="top">
<br/>
<p align="center">le week-end</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">les vacances</p>
</td>
</tr>
</tbody>
</table>

HABITAT

<table>
<tbody>
<tr>
<td rowspan="5" valign="top">
<p align="center">type</p>
</td>
<td colspan="2" valign="top">
<br/>
<p align="center">rural dispersé</p>
</td>
</tr>
<tr>
<td rowspan="3" valign="top">
<br/>
<p align="center">appartement</p>
</td>
<td valign="top">
<br/>
<p align="center">ordinaire</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">en logement-foyer</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">autre</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">maison individuelle</p>
</td>
</tr>
<tr>
<td rowspan="3" valign="top">
<br/>
<p align="center">accessibilité</p>
</td>
<td colspan="2" valign="top">
<br/>
<p align="center">étage sans ascenseur</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">présence de marches ou de niveaux différents</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">sols défectueux</p>
</td>
</tr>
<tr>
<td rowspan="5" valign="top">
<br/>
<p align="center">sanitaires</p>
</td>
<td colspan="2" valign="top">
<br/>
<p align="center">absence eau courante</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">absence eau chaude</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">WC extérieurs ou absents, WC non adaptés</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">absence de salle de bains</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">baignoire ou douche inadaptée</p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top">
<br/>
<p align="center">chauffage</p>
</td>
<td colspan="2" valign="top">
<br/>
<p align="center">problème approvisionnement</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">chauffage défectueux ou inexistant</p>
</td>
</tr>
<tr>
<td rowspan="5" valign="top">
<br/>
<p align="center">équipement</p>
</td>
<td colspan="2" valign="top">
<br/>
<p align="center">électro-ménager insuffisant</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">mobilier insuffisant</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">absence de téléphone</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">logement trop petit</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">logement insalubre</p>
</td>
</tr>
</tbody>
</table>

.

<table>
<thead>
<tr>
<td colspan="3" width="338">
<p align="center">AIDES PROPOSEES</p>
</td>
<td width="117">
<br/>
<p align="center">FREQUENCE</p>
</td>
</tr>
</thead>
<tbody>
<tr>
<td valign="top">
<br/>
<p align="center">aide à domicile</p>
</td>
<td colspan="2" valign="top">
<br/>
<p align="center">aide-ménagère (en heures par mois)</p>
</td>
<td valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td rowspan="2" valign="top">
<br/>
<p align="center">type :</p>
</td>
<td valign="top">
<br/>
<p align="center">garde à domicile</p>
</td>
<td valign="top">
<br/>
<p align="center">jour</p>
</td>
<td valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">(en heures par mois)</p>
</td>
<td valign="top">
<br/>
<p align="center">nuit</p>
</td>
<td valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">accueil temporaire</p>
</td>
<td valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">portage de repas (en nombre par mois)</p>
</td>
<td valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">téléalarme</p>
</td>
<td valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">adaptation du logement</p>
</td>
<td valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">blanchisserie à domicile</p>
</td>
<td valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">transport</p>
</td>
<td valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">dépannage, petits travaux</p>
</td>
<td valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">autre service (précisez)</p>
</td>
<td valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td rowspan="7" valign="top">
<br/>
<p align="center">aides techniques</p>
</td>
<td colspan="2" valign="top">
<br/>
<p align="center">fauteuil roulant</p>
</td>
<td valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">cannes</p>
</td>
<td valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">déambulateur</p>
</td>
<td valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">lit médicalisé</p>
</td>
<td valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">lève-malade</p>
</td>
<td valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">matériel à usage unique pour incontinence</p>
</td>
<td valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="2" valign="top">
<br/>
<p align="center">autres (précisez)</p>
</td>
<td valign="top">
<br/>
</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
<td valign="top">
<p align="justify">OBSERVATIONS DE L'EQUIPE MEDICO-SOCIALE</p>
</td>
</tr>
</tbody>
</table>

Guide de remplissage de la grille nationale AGGIR

(Guide non reproduit, consulter le fac-similé)
