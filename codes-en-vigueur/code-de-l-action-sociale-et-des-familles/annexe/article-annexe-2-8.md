# Article Annexe 2-8

LISTE DES INFORMATIONS ANONYMISÉES

I. - Informations préalables

I.-1. NUMDEP : numéro du département

<table>
<tbody>
<tr>
<td align="center">CODE</td>
<td align="center">LIBELLÉ</td>
</tr>
<tr>
<td align="center">
<br/>1</td>
<td align="center">
<br/>AIN</td>
</tr>
<tr>
<td align="center">
<br/>2</td>
<td align="center">
<br/>AISNE</td>
</tr>
<tr>
<td align="center">
<br/>3</td>
<td align="center">
<br/>ALLIER</td>
</tr>
<tr>
<td align="center">
<br/>4</td>
<td align="center">
<br/>ALPES-DE-HAUTE-PROVENCE</td>
</tr>
<tr>
<td align="center">
<br/>5</td>
<td align="center">
<br/>HAUTES-ALPES</td>
</tr>
<tr>
<td align="center">
<br/>6</td>
<td align="center">
<br/>ALPES-MARITIMES</td>
</tr>
<tr>
<td align="center">
<br/>7</td>
<td align="center">
<br/>ARDÈCHE</td>
</tr>
<tr>
<td align="center">
<br/>8</td>
<td align="center">
<br/>ARDENNES</td>
</tr>
<tr>
<td align="center">
<br/>9</td>
<td align="center">
<br/>ARIÈGE</td>
</tr>
<tr>
<td align="center">
<br/>10</td>
<td align="center">
<br/>AUBE</td>
</tr>
<tr>
<td align="center">
<br/>11</td>
<td align="center">
<br/>AUDE</td>
</tr>
<tr>
<td align="center">
<br/>12</td>
<td align="center">
<br/>AVEYRON</td>
</tr>
<tr>
<td align="center">
<br/>13</td>
<td align="center">
<br/>BOUCHES-DU-RHÔNE</td>
</tr>
<tr>
<td align="center">
<br/>14</td>
<td align="center">
<br/>CALVADOS</td>
</tr>
<tr>
<td align="center">
<br/>15</td>
<td align="center">
<br/>CANTAL</td>
</tr>
<tr>
<td align="center">
<br/>16</td>
<td align="center">
<br/>CHARENTE</td>
</tr>
<tr>
<td align="center">
<br/>17</td>
<td align="center">
<br/>CHARENTE-MARITIME</td>
</tr>
<tr>
<td align="center">
<br/>18</td>
<td align="center">
<br/>CHER</td>
</tr>
<tr>
<td align="center">
<br/>19</td>
<td align="center">
<br/>CORRÈZE</td>
</tr>
<tr>
<td align="center">
<br/>2A</td>
<td align="center">
<br/>CORSE-DU-SUD</td>
</tr>
<tr>
<td align="center">
<br/>2B</td>
<td align="center">
<br/>HAUTE-CORSE</td>
</tr>
<tr>
<td align="center">
<br/>21</td>
<td align="center">
<br/>CÔTE-D'OR</td>
</tr>
<tr>
<td align="center">
<br/>22</td>
<td align="center">
<br/>CÔTES-D'ARMOR</td>
</tr>
<tr>
<td align="center">
<br/>23</td>
<td align="center">
<br/>CREUSE</td>
</tr>
<tr>
<td align="center">
<br/>24</td>
<td align="center">
<br/>DORDOGNE</td>
</tr>
<tr>
<td align="center">
<br/>25</td>
<td align="center">
<br/>DOUBS</td>
</tr>
<tr>
<td align="center">
<br/>26</td>
<td align="center">
<br/>DRÔME</td>
</tr>
<tr>
<td align="center">
<br/>27</td>
<td align="center">
<br/>EURE</td>
</tr>
<tr>
<td align="center">
<br/>28</td>
<td align="center">
<br/>EURE-ET-LOIRE</td>
</tr>
<tr>
<td align="center">
<br/>29</td>
<td align="center">
<br/>FINISTÈRE</td>
</tr>
<tr>
<td align="center">
<br/>30</td>
<td align="center">
<br/>GARD</td>
</tr>
<tr>
<td align="center">
<br/>31</td>
<td align="center">
<br/>HAUTE-GARONNE</td>
</tr>
<tr>
<td align="center">
<br/>32</td>
<td align="center">
<br/>GERS</td>
</tr>
<tr>
<td align="center">
<br/>33</td>
<td align="center">
<br/>GIRONDE</td>
</tr>
<tr>
<td align="center">
<br/>34</td>
<td align="center">
<br/>HÉRAULT</td>
</tr>
<tr>
<td align="center">
<br/>35</td>
<td align="center">
<br/>ILLE-ET-VILAINE</td>
</tr>
<tr>
<td align="center">
<br/>36</td>
<td align="center">
<br/>INDRE</td>
</tr>
<tr>
<td align="center">
<br/>37</td>
<td align="center">
<br/>INDRE-ET-LOIRE</td>
</tr>
<tr>
<td align="center">
<br/>38</td>
<td align="center">
<br/>ISÈRE</td>
</tr>
<tr>
<td align="center">
<br/>39</td>
<td align="center">
<br/>JURA</td>
</tr>
<tr>
<td align="center">
<br/>40</td>
<td align="center">
<br/>LANDES</td>
</tr>
<tr>
<td align="center">
<br/>41</td>
<td align="center">
<br/>LOIR-ET-CHER</td>
</tr>
<tr>
<td align="center">
<br/>42</td>
<td align="center">
<br/>LOIRE</td>
</tr>
<tr>
<td align="center">
<br/>43</td>
<td align="center">
<br/>HAUTE-LOIRE</td>
</tr>
<tr>
<td align="center">
<br/>44</td>
<td align="center">
<br/>LOIRE-ATLANTIQUE</td>
</tr>
<tr>
<td align="center">
<br/>45 </td>
<td align="center">
<br/>LOIRET</td>
</tr>
<tr>
<td align="center">
<br/>46</td>
<td align="center">
<br/>LOT</td>
</tr>
<tr>
<td align="center">
<br/>47</td>
<td align="center">
<br/>LOT-ET-GARONNE</td>
</tr>
<tr>
<td align="center">
<br/>48</td>
<td align="center">
<br/>LOZÈRE</td>
</tr>
<tr>
<td align="center">
<br/>49</td>
<td align="center">
<br/>MAINE-ET-LOIRE</td>
</tr>
<tr>
<td align="center">
<br/>50</td>
<td align="center">
<br/>MANCHE</td>
</tr>
<tr>
<td align="center">
<br/>51</td>
<td align="center">
<br/>MARNE</td>
</tr>
<tr>
<td align="center">
<br/>52</td>
<td align="center">
<br/>HAUTE-MARNE</td>
</tr>
<tr>
<td align="center">
<br/>53</td>
<td align="center">
<br/>MAYENNE</td>
</tr>
<tr>
<td align="center">
<br/>54</td>
<td align="center">
<br/>MEURTHE-ET-MOSELLE</td>
</tr>
<tr>
<td align="center">
<br/>55</td>
<td align="center">
<br/>MEUSE</td>
</tr>
<tr>
<td align="center">
<br/>56</td>
<td align="center">
<br/>MORBIHAN</td>
</tr>
<tr>
<td align="center">
<br/>57</td>
<td align="center">
<br/>MOSELLE</td>
</tr>
<tr>
<td align="center">
<br/>58</td>
<td align="center">
<br/>NIÈVRE</td>
</tr>
<tr>
<td align="center">
<br/>59</td>
<td align="center">
<br/>NORD</td>
</tr>
<tr>
<td align="center">
<br/>60</td>
<td align="center">
<br/>OISE</td>
</tr>
<tr>
<td align="center">
<br/>61</td>
<td align="center">
<br/>ORNE</td>
</tr>
<tr>
<td align="center">
<br/>62</td>
<td align="center">
<br/>PAS-DE-CALAIS</td>
</tr>
<tr>
<td align="center">
<br/>63</td>
<td align="center">
<br/>PUY-DE-DÔME</td>
</tr>
<tr>
<td align="center">
<br/>64</td>
<td align="center">
<br/>PYRÉNÉES-ATLANTIQUES</td>
</tr>
<tr>
<td align="center">
<br/>65</td>
<td align="center">
<br/>HAUTES-PYRÉNÉES</td>
</tr>
<tr>
<td align="center">
<br/>66</td>
<td align="center">
<br/>PYRÉNÉES-ORIENTALES</td>
</tr>
<tr>
<td align="center">
<br/>67</td>
<td align="center">
<br/>BAS-RHIN</td>
</tr>
<tr>
<td align="center">
<br/>68</td>
<td align="center">
<br/>HAUT-RHIN</td>
</tr>
<tr>
<td align="center">
<br/>69</td>
<td align="center">
<br/>RHÔNE</td>
</tr>
<tr>
<td align="center">
<br/>70</td>
<td align="center">
<br/>HAUTE-SAÔNE</td>
</tr>
<tr>
<td align="center">
<br/>71</td>
<td align="center">
<br/>SAÔNE-ET-LOIRE</td>
</tr>
<tr>
<td align="center">
<br/>72</td>
<td align="center">
<br/>SARTHE</td>
</tr>
<tr>
<td align="center">
<br/>73</td>
<td align="center">
<br/>SAVOIE</td>
</tr>
<tr>
<td align="center">
<br/>74</td>
<td align="center">
<br/>HAUTE-SAVOIE</td>
</tr>
<tr>
<td align="center">
<br/>75</td>
<td align="center">
<br/>PARIS</td>
</tr>
<tr>
<td align="center">
<br/>76</td>
<td align="center">
<br/>SEINE-MARITIME</td>
</tr>
<tr>
<td align="center">
<br/>77</td>
<td align="center">
<br/>SEINE-ET-MARNE</td>
</tr>
<tr>
<td align="center">
<br/>78</td>
<td align="center">
<br/>YVELINES</td>
</tr>
<tr>
<td align="center">
<br/>79</td>
<td align="center">
<br/>DEUX-SÈVRES</td>
</tr>
<tr>
<td align="center">
<br/>80</td>
<td align="center">
<br/>SOMME</td>
</tr>
<tr>
<td align="center">
<br/>81</td>
<td align="center">
<br/>TARN</td>
</tr>
<tr>
<td align="center">
<br/>82</td>
<td align="center">
<br/>TARN-ET-GARONNE</td>
</tr>
<tr>
<td align="center">
<br/>83</td>
<td align="center">
<br/>VAR</td>
</tr>
<tr>
<td align="center">
<br/>84</td>
<td align="center">
<br/>VAUCLUSE</td>
</tr>
<tr>
<td align="center">
<br/>85</td>
<td align="center">
<br/>VENDÉE</td>
</tr>
<tr>
<td align="center">
<br/>86</td>
<td align="center">
<br/>VIENNE</td>
</tr>
<tr>
<td align="center">
<br/>87</td>
<td align="center">
<br/>HAUTE-VIENNE</td>
</tr>
<tr>
<td align="center">
<br/>88</td>
<td align="center">
<br/>VOSGES</td>
</tr>
<tr>
<td align="center">
<br/>89</td>
<td align="center">
<br/>YONNE</td>
</tr>
<tr>
<td align="center">
<br/>90</td>
<td align="center">
<br/>TERRITOIRE DE BELFORT</td>
</tr>
<tr>
<td align="center">
<br/>91</td>
<td align="center">
<br/>ESSONNE</td>
</tr>
<tr>
<td align="center">
<br/>92</td>
<td align="center">
<br/>HAUTS-DE-SEINE</td>
</tr>
<tr>
<td align="center">
<br/>93</td>
<td align="center">
<br/>SEINE-SAINT-DENIS</td>
</tr>
<tr>
<td align="center">
<br/>94</td>
<td align="center">
<br/>VAL-DE-MARNE</td>
</tr>
<tr>
<td align="center">
<br/>95</td>
<td align="center">
<br/>VAL-D'OISE</td>
</tr>
<tr>
<td align="center">
<br/>971</td>
<td align="center">
<br/>GUADELOUPE</td>
</tr>
<tr>
<td align="center">
<br/>972</td>
<td align="center">
<br/>MARTINIQUE</td>
</tr>
<tr>
<td align="center">
<br/>973</td>
<td align="center">
<br/>GUYANE</td>
</tr>
<tr>
<td align="center">
<br/>974</td>
<td align="center">
<br/>LA RÉUNION</td>
</tr>
<tr>
<td align="center">
<br/>976</td>
<td align="center">
<br/>MAYOTTE</td>
</tr>
</tbody>
</table>

I. 2. - NUMANONYM : numéro d'anonymat du mineur

I. 3. - CODEV : codification du type d'événement

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Evaluation de l'information préoccupante donnant lieu à une entrée <p>ou à un maintien dans la protection de l'enfance <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Signalement direct donnant lieu à une entrée <p>ou à un maintien dans la protection de l'enfance <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Saisine directe du juge des enfants <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Mesure de protection de l'enfance <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>5 <br/>
</td>
<td align="center">
<br/>Renouvellement ou fin de l'intervention en protection de l'enfance <br/>
</td>
</tr>
</tbody>
</table>

II. - Informations concernant le mineur

II. 1. - SEXE : sexe du mineur

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Garçon <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Fille <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Non connu à ce jour <br/>
</td>
</tr>
</tbody>
</table>

II. 2. - MNAIS et ANAIS : mois et année de naissance du mineur

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>mm <br/>
</td>
<td align="center">
<br/>Mois de naissance <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année de naissance <br/>
</td>
</tr>
</tbody>
</table>

II. 3. - MODACC : mode d'accueil pour les mineurs de moins de 6 ans

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Gardé par un des parents au domicile <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Gardé par un autre adulte au domicile <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Accueilli en crèche <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Accueilli par une assistante maternelle <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>5 <br/>
</td>
<td align="center">
<br/>Accueilli par un membre de la famille <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>6 <br/>
</td>
<td align="center">
<br/>Autre mode de garde <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

II. 4. - Situation scolaire ou professionnelle du mineur

II. 4a. SCODTCOM : le mineur est scolarisé en droit commun

<table>
<tbody>
<tr>
<td colspan="2">SCOLARISÉ EN DROIT COMMUN </td>
</tr>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

II. 4. b. NIVSCO : si le mineur est scolarisé en droit commun, préciser le niveau selon la nomenclature de l'éducation nationale

<table>
<tbody>
<tr>
<td colspan="2">NIVEAU SCOLAIRE (nomenclature de l'éducation nationale) </td>
</tr>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>200 <br/>
</td>
<td align="center">
<br/>Ecole maternelle <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>310 <br/>
</td>
<td align="center">
<br/>CP <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>320 <br/>
</td>
<td align="center">
<br/>CE1 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>330 <br/>
</td>
<td align="center">
<br/>CE2 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>340 <br/>
</td>
<td align="center">
<br/>CM1 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>350 <br/>
</td>
<td align="center">
<br/>CM2 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>360 <br/>
</td>
<td align="center">
<br/>Autre, dont scolarisation élémentaire spécifique <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>399 <br/>
</td>
<td align="center">
<br/>Ecole élémentaire sans distinction supplémentaire <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>410 <br/>
</td>
<td align="center">
<br/>6e <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>420 <br/>
</td>
<td align="center">
<br/>5e <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>430 <br/>
</td>
<td align="center">
<br/>4e <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>440 <br/>
</td>
<td align="center">
<br/>3e <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>450 <br/>
</td>
<td align="center">
<br/>Autre, dont scolarisation spécifique au collège <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>499 <br/>
</td>
<td align="center">
<br/>Collège, sans distinction supplémentaire <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>510 <br/>
</td>
<td align="center">
<br/>Seconde générale et technologique <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>520 <br/>
</td>
<td align="center">
<br/>Première générale <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>530 <br/>
</td>
<td align="center">
<br/>Première technologique <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>540 <br/>
</td>
<td align="center">
<br/>Première d'adaptation <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>550 <br/>
</td>
<td align="center">
<br/>Terminale générale <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>560 <br/>
</td>
<td align="center">
<br/>Terminale technologique <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>570 <br/>
</td>
<td align="center">
<br/>Autre, dont scolarisation spécifique au lycée <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>599 <br/>
</td>
<td align="center">
<br/>Lycée d'enseignement général et technologique, sans distinction supplémentaire <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>610 <br/>
</td>
<td align="center">
<br/>CAP <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>620 <br/>
</td>
<td align="center">
<br/>BEP <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>630 <br/>
</td>
<td align="center">
<br/>Bac professionnel <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>640 <br/>
</td>
<td align="center">
<br/>Autre, dont scolarisation spécifique au lycée professionnel <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>699 <br/>
</td>
<td align="center">
<br/>Lycée professionnel, sans distinction supplémentaire <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>710 <br/>
</td>
<td align="center">
<br/>CAP en apprentissage <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>720 <br/>
</td>
<td align="center">
<br/>BEP en apprentissage <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>730 <br/>
</td>
<td align="center">
<br/>Brevet professionnel <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>740 <br/>
</td>
<td align="center">
<br/>Bac professionnel en apprentissage <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>750 <br/>
</td>
<td align="center">
<br/>Autre, dont scolarisation spécifique <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>769 <br/>
</td>
<td align="center">
<br/>Apprentissage sans distinction supplémentaire <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>770 <br/>
</td>
<td align="center">
<br/>Etudes supérieures <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>999 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

II. 4. c. - SCOCLASPE : le mineur est scolarisé en classe spécialisée

<table>
<tbody>
<tr>
<td colspan="2">SCOLARISÉ EN CLASSE SPÉCIALISÉE </td>
</tr>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

II. 4. d. - TYPCLASSPE : si le mineur est scolarisé en classe spécialisée, préciser le type de classe

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Classe pour l'inclusion scolaire <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Unité pédagogique d'intégration <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Section d'enseignement général et professionnel adapté <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Classe d'intégration <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>5 <br/>
</td>
<td align="center">
<br/>Autre classe spécialisée <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>6 <br/>
</td>
<td align="center">
<br/>Classe spécialisée sans distinction supplémentaire <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

II. 4. e. - ETABSCOSPE : le mineur est scolarisé en établissement spécialisé

<table>
<tbody>
<tr>
<td colspan="2">SCOLARISÉ EN ÉTABLISSEMENT SPÉCIALISÉ </td>
</tr>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

II. 4. f. - TYPETABSPE : si le mineur est scolarisé en établissement spécialisé, préciser le type d'établissement

<table>
<tbody>
<tr>
<td colspan="2">TYPE D'ÉTABLISSEMENT SPÉCIALISÉ </td>
</tr>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>810 <br/>
</td>
<td align="center">
<br/>Institut médico-pédagogique <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>820 <br/>
</td>
<td align="center">
<br/>Institut médico-professionnel <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>830 <br/>
</td>
<td align="center">
<br/>Institut médico-éducatif <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>840 <br/>
</td>
<td align="center">
<br/>Etablissement spécialisé de l'éducation nationale <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>890 <br/>
</td>
<td align="center">
<br/>Etablissement spécialisé sans distinction supplémentaire <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>999 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

II. 4. g. - NONSCO : si le mineur est non scolarisé, préciser la situation

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>981 <br/>
</td>
<td align="center">
<br/>Non scolarisé, sans formation, insertion ou activité professionnelle <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>982 <br/>
</td>
<td align="center">
<br/>Non scolarisé et dispositif de formation ou d'insertion <p>ou activité professionnelle, dont stage <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<br/>990 <br/>
</td>
<td align="center">
<br/>Autre (instruit à domicile, CNED, etc.) <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>999 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

II. 5. - FREQSCO : fréquentation de l'établissement scolaire

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non inscrit <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Inscrit et fréquentation régulière <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Inscrit et fréquentation irrégulière <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Inscrit mais déscolarisé <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>5 <br/>
</td>
<td align="center">
<br/>Inscrit mais en situation d'exclusion temporaire <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

II. 6. - Handicap

II. 6. a. - HANDICAP : prise en charge spécifique suite à une décision de la commission des droits et de l'autonomie rendue au nom de la Maison départementale des personnes handicapées

<table>
<tbody>
<tr>
<td colspan="2">PRISE EN CHARGE SPÉCIFIQUE </td>
</tr>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

II. 6. b. - DATDECMDPH : si prise en charge spécifique, date de la décision de la commission des droits et de l'autonomie

<table>
<tbody>
<tr>
<td colspan="2">DATE DE LA DÉCISION</td>
</tr>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>jj <br/>
</td>
<td align="center">
<br/>Jour de la décision <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>mm <br/>
</td>
<td align="center">
<br/>Mois de la décision <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année de décision <br/>
</td>
</tr>
</tbody>
</table>

II. 6. c. - DATEXDECMDPH : si prise en charge spécifique, date d'exécution de la décision de la commission des droits et de l'autonomie

<table>
<tbody>
<tr>
<td colspan="2">EXÉCUTION DE LA DÉCISION </td>
</tr>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>jj <br/>
</td>
<td align="center">
<br/>Jour de la décision <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>mm <br/>
</td>
<td align="center">
<br/>Mois de la décision <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année de décision <br/>
</td>
</tr>
</tbody>
</table>

III. - Information préoccupante ou signalement direct donnant lieu à une mesure de protection de l'enfance

III. 1. - DATIP : date de réception de l'information préoccupante

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>jj <br/>
</td>
<td align="center">
<br/>Jour <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>mm <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
</tr>
</tbody>
</table>

III. 2. - DATSIGN : date du signalement direct auprès du procureur de la République

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>jj <br/>
</td>
<td align="center">
<br/>Jour <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>mm <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
</tr>
</tbody>
</table>

III. 3. - DATJE : date de la saisine directe du juge des enfants

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>jj <br/>
</td>
<td align="center">
<br/>Jour <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>mm <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
</tr>
</tbody>
</table>

III. 4. - ORIGIP : qualité de la personne à l'origine de l'information préoccupante ou du signalement direct

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>10 <br/>
</td>
<td align="center">
<br/>Le mineur lui-même <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>20 <br/>
</td>
<td align="center">
<br/>Parents du mineur <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>31 <br/>
</td>
<td align="center">
<br/>Personnel social <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>32 <br/>
</td>
<td align="center">
<br/>Personnel de santé <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>33 <br/>
</td>
<td align="center">
<br/>Elu <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>34 <br/>
</td>
<td align="center">
<br/>Autre intervenant institutionnel <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>40 <br/>
</td>
<td align="center">
<br/>Autre particulier <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>99 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

III. 5. - TRANSIP : institution ou qualité de la personne ayant transmis l'information préoccupante à la cellule ou ayant saisi directement le procureur de la République ou le juge des enfants

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>100 <br/>
</td>
<td align="center">
<br/>Le mineur lui-même <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>200 <br/>
</td>
<td align="center">
<br/>Parents du mineur <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>300 <br/>
</td>
<td align="center">
<br/>Autre membre de la famille <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>400 <br/>
</td>
<td align="center">
<br/>Autre particulier <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>510 <br/>
</td>
<td align="center">
<br/>Service national d'accueil téléphonique de l'enfance en danger <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>520 <br/>
</td>
<td align="center">
<br/>Conseil général <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>530 <br/>
</td>
<td align="center">
<br/>Service de milieu ouvert ou de placement <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>541 <br/>
</td>
<td align="center">
<br/>Etablissement d'enseignement public <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>542 <br/>
</td>
<td align="center">
<br/>Etablissement d'enseignement privé <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>543 <br/>
</td>
<td align="center">
<br/>Education nationale, sans distinction supplémentaire <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>550 <br/>
</td>
<td align="center">
<br/>Hôpital <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>560 <br/>
</td>
<td align="center">
<br/>Médecine libérale <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>570 <br/>
</td>
<td align="center">
<br/>Autre institution sanitaire et sociale <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>580 <br/>
</td>
<td align="center">
<br/>Accueil extrascolaire du mineur <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>590 <br/>
</td>
<td align="center">
<br/>Autre service social et association <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>600 <br/>
</td>
<td align="center">
<br/>Police ou gendarmerie <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>611 <br/>
</td>
<td align="center">
<br/>Procureur de la République <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>612 <br/>
</td>
<td align="center">
<br/>Juge des enfants <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>613 <br/>
</td>
<td align="center">
<br/>Justice sans distinction supplémentaire <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>620 <br/>
</td>
<td align="center">
<br/>Mairie ou commune <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>630 <br/>
</td>
<td align="center">
<br/>Autre <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>640 <br/>
</td>
<td align="center">
<br/>Voie institutionnelle sans distinction supplémentaire <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>999 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

III. 6. - Suite donnée au signalement direct auprès du procureur de la République

III. 6. a. - SUITSIGNCG/ SUITSIGOPP/ SUITSIGJE/ SUITSIGSS :

type de suite donnée au signalement

<table>
<tbody>
<tr>
<td align="center" colspan="3">CODE </td>
<td align="center" rowspan="2">LIBELLÉ </td>
</tr>
<tr>
<td align="center">Non </td>
<td align="center">Oui </td>
<td align="center">Ne sait pas <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>SUITSIGNCG Renvoi au conseil général pour compétence fondée sur l'article L. 226-4 du CASF <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>SUITSIGOPP Ordonnance de placement provisoire <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>SUITSIGJE Saisine du juge des enfants <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>SUITSIGSS Sans suite du procureur de la République <br/>
</td>
</tr>
</tbody>
</table>

III. 6. b. - DATAVIS : en cas d'ouverture directe d'une procédure auprès du juge des enfants : date d'avis d'ouverture de la procédure

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>jj <br/>
</td>
<td align="center">
<br/>Jour <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>mm <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
</tr>
</tbody>
</table>

III. 6. c. - ENQPENAL : le cas échéant, préciser s'il y a une enquête pénale

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

III. 6. d. - SAISJUR : le cas échéant, préciser s'il y a une saisine de la juridiction pénale

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

IV. - Informations concernant le cadre de vie social et familial du mineur

IV. 1 - Caractéristiques du ménage au sein de la résidence principale du mineur

IV. 1. a. - COMPOMENAG : composition du ménage

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Mineur autonome <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Parents vivant ensemble <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Mineur vivant avec sa mère seule <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Mineur vivant avec son père seul <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>5 <br/>
</td>
<td align="center">
<br/>Résidence alternée <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>6 <br/>
</td>
<td align="center">
<br/>Mineur vivant avec sa mère dans une famille recomposée <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>7 <br/>
</td>
<td align="center">
<br/>Mineur vivant avec son père dans une famille recomposée <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>8 <br/>
</td>
<td align="center">
<br/>Mineur vivant chez un autre membre de la famille <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Mineur vivant chez un particulier <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>10 <br/>
</td>
<td align="center">
<br/>Autre <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>99 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

IV. 1. b. - AUTREHEBER : autre hébergement régulier du mineur le cas échéant

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

IV. 1. c. - NBPER : nombre total de personnes dans le lieu de résidence

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>nn <br/>
</td>
<td align="center">
<br/>Nombre de personnes <br/>
</td>
</tr>
</tbody>
</table>

IV. 1. d. - NBFRAT : nombre total de frères et sœurs

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>nn <br/>
</td>
<td align="center">
<br/>Nombre de frères et sœurs (inclus demi-frères et demi-sœurs) <br/>
</td>
</tr>
</tbody>
</table>

IV. 1. e. - STATOCLOG : statut d'occupation du logement

(selon la nomenclature INSEE)

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Propriétaire <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Locataire, sous-locataire <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Logé gratuitement <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Fermier-métayer <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>5 <br/>
</td>
<td align="center">
<br/>Autre <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

IV. 2. - Exercice de l'autorité parentale

IV. 2. a. - TITAP : titulaire de l'autorité parentale

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>10 <br/>
</td>
<td align="center">
<br/>Exercice conjoint, par les parents vivant ensemble <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>20 <br/>
</td>
<td align="center">
<br/>Exercice conjoint, par les parents vivant séparément <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>30 <br/>
</td>
<td align="center">
<br/>Exclusivement par le père <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>40 <br/>
</td>
<td align="center">
<br/>Exclusivement par la mère <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>50 <br/>
</td>
<td align="center">
<br/>Autre membre de la famille <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>60 <br/>
</td>
<td align="center">
<br/>Autre particulier sans lien familial <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>70 <br/>
</td>
<td align="center">
<br/>Président du conseil général <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>80 <br/>
</td>
<td align="center">
<br/>Préfet <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>90 <br/>
</td>
<td align="center">
<br/>Etablissement <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>99 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

IV. 2. b. - DECAP : décision relative à l'autorité parentale

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Décision du juge aux affaires familiales sur l'exercice de l'autorité parentale <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Délégation de l'autorité parentale <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Retrait <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Tutelle <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

IV. 2. c. - DATDECAP : date de la décision relative à l'autorité parentale

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>jj <br/>
</td>
<td align="center">
<br/>Jour <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>mm <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
</tr>
</tbody>
</table>

IV. 2. d. - CONTMERE/ CONTPERE : fréquence des contacts de la mère/ du père avec le mineur

<table>
<tbody>
<tr>
<td align="center" rowspan="2">CODE </td>
<td align="center">CONTMÈRE </td>
<td align="center">CONTPÈRE </td>
</tr>
<tr>
<td align="center">LIBELLÉ </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Au moins une fois par semaine <br/>
</td>
<td align="center">
<br/>Au moins une fois par semaine <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Au moins une fois par mois <br/>
</td>
<td align="center">
<br/>Au moins une fois par mois <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Au moins une fois tous les 6 mois <br/>
</td>
<td align="center">
<br/>Au moins une fois tous les 6 mois <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Au moins une fois par an <br/>
</td>
<td align="center">
<br/>Au moins une fois par an <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>5 <br/>
</td>
<td align="center">
<br/>Aucun contact <br/>
</td>
<td align="center">
<br/>Aucun contact <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

IV. 3. - Situation sociodémographique des parents ou des adultes qui s'occupent principalement du mineur dans sa résidence principale

IV. 3. a. - LIENA1/ LIENA2 : lien de l'adulte 1 et de l'adulte 2 avec le mineur

<table>
<tbody>
<tr>
<td align="center" rowspan="2">CODE </td>
<td align="center">LIENA1 </td>
<td align="center">LIENA2 </td>
</tr>
<tr>
<td align="center">LIBELLÉ </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Père ou mère <br/>
</td>
<td align="center">
<br/>Père ou mère <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Conjoint du père ou de la mère <br/>
</td>
<td align="center">
<br/>Conjoint du père ou de la mère <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Grand-père ou grand-mère <br/>
</td>
<td align="center">
<br/>Grand-père ou grand-mère <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Frère, demi-frère, sœur ou demi-sœur <br/>
</td>
<td align="center">
<br/>Frère, demi-frère, sœur ou demi-sœur <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>5 <br/>
</td>
<td align="center">
<br/>Oncle ou tante <br/>
</td>
<td align="center">
<br/>Oncle ou tante <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>6 <br/>
</td>
<td align="center">
<br/>Autre membre de la famille <br/>
</td>
<td align="center">
<br/>Autre membre de la famille <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>7 <br/>
</td>
<td align="center">
<br/>Autre particulier sans lien familial <br/>
</td>
<td align="center">
<br/>Autre particulier sans lien familial <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

IV. 3. b. - SEXA1/ SEXA2 : sexe de l'adulte 1 et de l'adulte 2

<table>
<tbody>
<tr>
<td align="center" rowspan="2">CODE </td>
<td align="center">SEXA1 </td>
<td align="center">SEXA2 </td>
</tr>
<tr>
<td align="center">LIBELLÉ </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Homme <br/>
</td>
<td align="center">
<br/>Homme <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Femme <br/>
</td>
<td align="center">
<br/>Femme <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

IV. 3. c. - ANSA1/ ANSA2 : année de naissance de l'adulte 1 et de l'adulte 2

<table>
<tbody>
<tr>
<td align="center" rowspan="2">CODE </td>
<td align="center">ANSA1 </td>
<td align="center">ANSA2 </td>
</tr>
<tr>
<td align="center">LIBELLÉ </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année de naissance <br/>
</td>
<td align="center">
<br/>Année de naissance <br/>
</td>
</tr>
</tbody>
</table>

IV. 3. d. - EMPLA1/ EMPLA2 : situation face à l'emploi de l'adulte 1 et de l'adulte 2

(selon la nomenclature INSEE)

<table>
<tbody>
<tr>
<td align="center" rowspan="2">CODE </td>
<td align="center">EMPLA 1 </td>
<td align="center">EMPLA 2 </td>
</tr>
<tr>
<td align="center">LIBELLÉ </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>10 <br/>
</td>
<td align="center">
<br/>Salarié contrat à durée indéterminée <br/>
</td>
<td align="center">
<br/>Salarié contrat à durée indéterminée <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>11 <br/>
</td>
<td align="center">
<br/>A son compte ou aidant un membre de sa famille dans son travail <br/>
</td>
<td align="center">
<br/>A son compte ou aidant un membre de sa famille dans son travail <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>12 <br/>
</td>
<td align="center">
<br/>Contrat à durée déterminée, mission d'intérim, apprentissage, travail saisonnier <br/>
</td>
<td align="center">
<br/>Contrat à durée déterminée, mission d'intérim, apprentissage, travail saisonnier <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>13 <br/>
</td>
<td align="center">
<br/>Stage de la formation professionnelle, ou contrat d'aide à l'emploi <br/>
</td>
<td align="center">
<br/>Stage de la formation professionnelle, ou contrat d'aide à l'emploi <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>20 <br/>
</td>
<td align="center">
<br/>Au chômage <br/>
</td>
<td align="center">
<br/>Au chômage <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>30 <br/>
</td>
<td align="center">
<br/>Elève, étudiant, stagiaire non rémunéré <br/>
</td>
<td align="center">
<br/>Elève, étudiant, stagiaire non rémunéré <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>40 <br/>
</td>
<td align="center">
<br/>Militaire du contingent <br/>
</td>
<td align="center">
<br/>Militaire du contingent <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>50 <br/>
</td>
<td align="center">
<br/>Autre situation : retraité, préretraité, femme au foyer, autre... <br/>
</td>
<td align="center">
<br/>Autre situation : retraité, préretraité, femme au foyer, autre... <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>99 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

IV. 3. e. - CSPA1/ CSPA2 : catégorie socioprofessionnelle de l'adulte 1 et de l'adulte 2

(selon la nomenclature INSEE)

<table>
<tbody>
<tr>
<td align="center" rowspan="2">CODE </td>
<td align="center">CSPA1 </td>
<td align="center">CSPA2 </td>
</tr>
<tr>
<td align="center">LIBELLÉ </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Agriculteurs exploitants <br/>
</td>
<td align="center">
<br/>Agriculteurs exploitants <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Artisans, commerçants et chefs d'entreprise <br/>
</td>
<td align="center">
<br/>Artisans, commerçants et chefs d'entreprise <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Cadres et professions intellectuelles supérieures <br/>
</td>
<td align="center">
<br/>Cadres et professions intellectuelles supérieures <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Professions Intermédiaires <br/>
</td>
<td align="center">
<br/>Professions Intermédiaires <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>5 <br/>
</td>
<td align="center">
<br/>Employés <br/>
</td>
<td align="center">
<br/>Employés <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>6 <br/>
</td>
<td align="center">
<br/>Ouvriers <br/>
</td>
<td align="center">
<br/>Ouvriers <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>7 <br/>
</td>
<td align="center">
<br/>Retraités <br/>
</td>
<td align="center">
<br/>Retraités <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>8 <br/>
</td>
<td align="center">
<br/>Autres personnes sans activité professionnelle <br/>
</td>
<td align="center">
<br/>Autres personnes sans activité professionnelle <br/>
</td>
</tr>
</tbody>
</table>

IV. 3. f. - RESMENAG : ressources mensuelles du ménage

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>0-999 euros <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>1 000-1 999 euros <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>2 000-2 999 euros <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>3 000 et plus <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

IV. 3. g. - REVTRAV/ MINIMA/ ALLOC/ AUTRE : nature des ressources du ménage

<table>
<tbody>
<tr>
<td colspan="4">CODE </td>
</tr>
<tr>
<td align="center">Non </td>
<td align="center">Oui </td>
<td align="center">Ne sait pas </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>REVTRAV Revenus du travail <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>MINIMA Minimas sociaux <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>ALLOC Allocations ou pensions <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>AUTRE Autres sources de revenus <br/>
</td>
</tr>
</tbody>
</table>

IV. 4. - Caractéristiques sociodémographiques du père et/ ou de la mère si non cohabitant avec le mineur

IV. 4. a. MEREINC/ PEREINC : mère/ père inconnu

<table>
<tbody>
<tr>
<td rowspan="2">CODE <br/>
</td>
<td>MERINC </td>
<td>PEREINC </td>
</tr>
<tr>
<td align="center">LIBELLÉ <br/>
</td>
<td align="center">LIBELLÉ <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

IV. 4. b. - ANSMERE/ ANSPERE : année de naissance de la mère/ du père

<table>
<tbody>
<tr>
<td rowspan="2">
<br/>CODE <br/>
</td>
<td>ANSMERE </td>
<td>ANSPERE </td>
</tr>
<tr>
<td align="center">
<br/>LIBELLÉ <br/>
</td>
<td align="center">
<br/>LIBELLÉ <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
</tr>
</tbody>
</table>

IV. 4. c. - DCMERE/ DCPERE : mère décédée/ père décédé

<table>
<tbody>
<tr>
<td align="center" rowspan="2">CODE <br/>
</td>
<td align="center">DCMERE </td>
<td align="center">DCPERE</td>
</tr>
<tr>
<td align="center">
<p>LIBELLÉ</p>
</td>
<td align="center">LIBELLÉ</td>
</tr>
<tr>
<td align="center">1 </td>
<td align="center">Non </td>
<td align="center">Non </td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

IV. 4. d. - DATDCMERE/ DATDCPERE : si décès de la mère/ du père, mois et année du décès

<table>
<tbody>
<tr>
<td rowspan="2">CODE <br/>
</td>
<td>DATDCMERE </td>
<td>DATCPERE </td>
</tr>
<tr>
<td align="center">LIBELLE <br/>
</td>
<td align="center">LIBELLE <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Mm <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
</tr>
</tbody>
</table>

V. - Informations relatives au mineur recueillies au titre de l'évaluation de sa situation, ou au titre du signalement direct

V. 1. - Evaluation

V. 1. a. - NOTIFEVAL : date de notification de la demande d'évaluation

<table>
<tbody>
<tr>
<td>CODE </td>
<td>LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>jj <br/>
</td>
<td align="center">
<br/>Jour <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>mm <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
</tr>
</tbody>
</table>

V. 1. b. - FINEVAL : date de fin de l'évaluation

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>jj <br/>
</td>
<td align="center">
<br/>Jour <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>mm <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
</tr>
</tbody>
</table>

V. 1. c. - MESANT : existence d'une prestation ou mesure de protection de l'enfance en cours ou antérieure pour un membre de la fratrie

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

V. 1. d. - ACCFAM : accompagnement social ou médico-social en cours d'au moins un membre de la famille

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

V. 1. e. - SUITEVAL : suite donnée à l'évaluation

<table>
<tbody>
<tr>
<td>
<p align="center"> CODE </p>
</td>
<td>
<p align="center"> LIBELLÉ</p>
</td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Mise en place d'une mesure administrative de protection de l'enfance <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Signalement judiciaire <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Poursuite de la prise en charge en protection de l'enfance (administrative ou judiciaire) <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Nouvelle décision en protection de l'enfance <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

V. 1. f. - MOTIFSIG : en cas de signalement judiciaire après l'évaluation, motif du signalement judiciaire

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>L'action ou les actions mises en œuvre précédemment n'ont pas permis de remédier à la situation <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Refus explicite ou implicite de la famille d'accepter l'intervention proposée <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Impossibilité de collaboration avec la famille <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Impossibilité d'évaluer cette situation <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

V. 2. - Problématiques familiales observées ou prises en compte dans le cadre de l'évaluation ou des bilans

V. 2. a. - CONDADD : conduite addictive (alcool ou drogue) d'un ou des adultes ayant en charge le mineur dans le lieu de résidence principale

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui, avec prise en charge spécialisée connue <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Oui, sans prise en charge connue <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

V. 2. b. - DEFINTEL : déficience intellectuelle ou mentale reconnue par la maison départementale des personnes handicapées d'un ou des adultes ayant en charge le mineur dans le lieu de sa résidence principale

<table>
<tbody>
<tr>
<td align="center">CODE  <br/>
</td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

V. 2. c. - CONFL : exposition du mineur à un conflit de couple

<table>
<tbody>
<tr>
<td align="center">CODE  <br/>
</td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

V. 2. d. - VIOLFAM : exposition du mineur à un climat de violence au sein de la famille

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

V. 2. e. - VIOLPERS : en cas de climat de violence au sein de la famille, personnes les plus concernées par ces violences

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Violences entre conjoints ou ex-conjoints <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Violences au sein de la fratrie <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Violences intergénérationnelles <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Violences concernant d'autres personnes <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

V. 2. f. - VIOLFAMPHYS : en cas de climat de violence au sein de la famille, existence de violences physiques

<table>
<tbody>
<tr>
<td align="center">
<br/>CODE <br/>
</td>
<td align="center">
<br/>LIBELLE <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

V. 2. g. - SOUTSOC : manque de soutien social et/ ou familial, isolement

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VI. - Informations sur la nature du danger ou du risque de danger justifiant une prise en charge en protection de l'enfance

VI. 1. - SANTÉ/ SÉCURITÉ/ MORALITÉ/ CONDEDUC/ CONDEDEV : nature du danger ou du risque de danger

<table>
<tbody>
<tr>
<td colspan="4">  CODE </td>
</tr>
<tr>
<td align="center"> Non </td>
<td align="center">Oui  <br/>
</td>
<td align="center"> Ne sait pas </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>SANTÉ : santé du mineur en danger ou en risque de danger <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>SECURITÉ : sécurité du mineur en danger ou en risque de danger <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>MORALITÉ : moralité du mineur en danger ou en risque de danger <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>CONDEDUC : conditions d'éducations gravement compromises ou en risque de l'être <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>CONDEDEV : conditions de développement physique, intellectuel, affectif ou social gravement compromises ou en risque de l'être <br/>
</td>
</tr>
</tbody>
</table>

VI. 2. - En cas de maltraitance associée, type de mauvais traitement

VI. 2. a. - VIOLSEX : violences sexuelles envers le mineur

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Procédure, ou enquête en cours <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Oui, avec allégations du mineur ou d'un tiers <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Oui, avec décision de justice <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VI. 2. b. - VIOLPHYS : violences physiques envers le mineur

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Procédure, ou enquête en cours <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Oui, avec allégations du mineur ou d'un tiers <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Oui, avec décision de justice <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VI. 2. c. - NEGLIG : négligences lourdes envers le mineur

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VI. 2. d. - VIOLPSY : violences psychologiques envers le mineur

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VI. 3. - En cas de maltraitance associée, caractéristiques de la ou des deux personnes principales à l'origine du mauvais traitement

VI. 3. a. - LIENAUT1/ LIENAUT2 : lien de la ou des deux personnes principales à l'origine du mauvais traitement avec le mineur

<table>
<tbody>
<tr>
<td rowspan="2">  CODE </td>
<td align="center"> LIENAUT1 </td>
<td align="center"> LIENAUT2 </td>
</tr>
<tr>
<td align="center"> LIBELLÉ </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>10 <br/>
</td>
<td align="center">
<br/>Membre de la famille vivant avec le mineur <br/>
</td>
<td align="center">
<br/>Membre de la famille vivant avec le mineur <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>20 <br/>
</td>
<td align="center">
<br/>Autre personne vivant avec le mineur <br/>
</td>
<td align="center">
<br/>Autre personne vivant avec le mineur <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>30 <br/>
</td>
<td align="center">
<br/>Membre du lieu de prise en charge en hébergement <p>du mineur <br/>
</p>
</td>
<td align="center">
<br/>Membre du lieu de prise en charge en hébergement <p>du mineur <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<br/>40 <br/>
</td>
<td align="center">
<br/>Membre de la famille ne vivant pas avec le mineur <br/>
</td>
<td align="center">
<br/>Membre de la famille ne vivant pas avec le mineur <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>50 <br/>
</td>
<td align="center">
<br/>Professionnel et assimilé ayant autorité sur le mineur <br/>
</td>
<td align="center">
<br/>Professionnel et assimilé ayant autorité sur le mineur <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>60 <br/>
</td>
<td align="center">
<br/>Autre personne connue du mineur <br/>
</td>
<td align="center">
<br/>Autre personne connue du mineur <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>70 <br/>
</td>
<td align="center">
<br/>Autre personne inconnue du mineur <br/>
</td>
<td align="center">
<br/>Autre personne inconnue du mineur <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>80 <br/>
</td>
<td align="center">
<br/>Personne ne vivant pas avec le mineur sans distinction <p>supplémentaire <br/>
</p>
</td>
<td align="center">
<br/>Personne ne vivant pas avec le mineur sans distinction <p>supplémentaire <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<br/>99 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VI. 3. b. - SEXAUT1/ SEXAUT2 : sexe de la ou des deux personnes principales à l'origine du mauvais traitement

<table>
<tbody>
<tr>
<td align="center" rowspan="2">CODE </td>
<td> SEXAUT1 </td>
<td> SEXAUT2 </td>
</tr>
<tr>
<td> LIBELLÉ </td>
<td> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Masculin <br/>
</td>
<td align="center">
<br/>Masculin <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Féminin <br/>
</td>
<td align="center">
<br/>Féminin <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VI. 3. c. - MINAUT1/ MINAUT2 : statut de majorité ou minorité de la ou des deux personnes principales à l'origine du mauvais traitement

<table>
<tbody>
<tr>
<td align="center" rowspan="2">CODE </td>
<td align="center"> MINAUT1 </td>
<td align="center"> MINAUT2 </td>
</tr>
<tr>
<td align="center"> LIBELLÉ </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Mineur <br/>
</td>
<td align="center">
<br/>Mineur <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Majeur <br/>
</td>
<td align="center">
<br/>Majeur <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. - Informations sur les décisions, mesures et interventions en protection de l'enfance

VII. 1. - DATDECPE : date de la décision de protection de l'enfance

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>jj <br/>
</td>
<td align="center">
<br/>Jour <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>mm <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
</tr>
</tbody>
</table>

VII. 2. - INTERANT : existence d'une intervention antérieure en protection de l'enfance ou en assistance éducative

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 3. - Projet pour l'enfant

VII. 3. a. - PROJET : existence d'un projet pour l'enfant

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 3. b. - SIGNPAR/ SIGNMIN : signature du projet pour l'enfant par les parents et par le mineur

<table>
<tbody>
<tr>
<td rowspan="2">CODE <br/>
</td>
<td> SIGNPAR </td>
<td> SIGNMIN </td>
</tr>
<tr>
<td align="center">
<br/>LIBELLÉ <br/>
</td>
<td align="center">
<br/>LIBELLÉ <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 3. c. - DATSIGNPROJ : date de la signature du projet pour l'enfant

<table>
<tbody>
<tr>
<td align="center">CODE  <br/>
</td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>jj <br/>
</td>
<td align="center">
<br/>Jour <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>mm <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
</tr>
</tbody>
</table>

VII. 4. - Nature de la décision

VII. 4. a. - DECISION : nature de la décision de protection de l'enfance

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Décision administrative en protection de l'enfance <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Décision judiciaire en assistance éducative <br/>
</td>
</tr>
</tbody>
</table>

VII. 4. b. - NATPDECADM : si décision administrative de protection de l'enfance, préciser la nature de la décision

<table>
<tbody>
<tr>
<td align="center">CODE  <br/>
</td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>10 <br/>
</td>
<td align="center">
<br/>Aide à domicile, hors aides financières <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>11 <br/>
</td>
<td align="center">
<br/>Accueil de jour <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>12 <br/>
</td>
<td align="center">
<br/>Accueil 72 heures <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>13 <br/>
</td>
<td align="center">
<br/>Accueil 5 jours <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>14 <br/>
</td>
<td align="center">
<br/>Accueil provisoire du mineur <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>15 <br/>
</td>
<td align="center">
<br/>Pupille de l'Etat <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>16 <br/>
</td>
<td align="center">
<br/>Accueil parent-enfant (moins de 3 ans) <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>17 <br/>
</td>
<td align="center">
<br/>Contrat responsabilité parentale <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>18 <br/>
</td>
<td align="center">
<br/>Autre décision administrative, à préciser <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>99 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 4. c. - AUTREDA : si autre décision administrative de protection de l'enfance, préciser la décision : champ ouvert

VII. 4. d. - NATDECASSED : si décision judiciaire en assistance éducative, préciser la nature de la décision

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>10 <br/>
</td>
<td align="center">
<br/>Non-lieu du juge des enfants <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>11 <br/>
</td>
<td align="center">
<br/>Mesure d'expertise <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>12 <br/>
</td>
<td align="center">
<br/>Enquête sociale <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>13 <br/>
</td>
<td align="center">
<br/>Investigation d'orientation éducative <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>14 <br/>
</td>
<td align="center">
<br/>Mesure judiciaire d'investigation éducative <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>15 <br/>
</td>
<td align="center">
<br/>Assistance éducative en milieu ouvert <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>16 <br/>
</td>
<td align="center">
<br/>Assistance éducative en milieu ouvert avec hébergement <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>17 <br/>
</td>
<td align="center">
<br/>Décision judiciaire de placement à l'aide sociale à l'enfance <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>18 <br/>
</td>
<td align="center">
<br/>Placement direct <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>19 <br/>
</td>
<td align="center">
<br/>Mesure judiciaire d'aide à la gestion du budget familial <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>20 <br/>
</td>
<td align="center">
<br/>Sursis à statuer <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>21 <br/>
</td>
<td align="center">
<br/>Autre mesure d'assistance éducative, à préciser <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>99 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 4. e. - AUTREDJ : si autre décision judiciaire en assistance éducative, préciser la décision : champ ouvert

VII. 4. f. - NATDECPLAC : si décision judiciaire de placement, nature de la décision

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Ordonnance de placement provisoire du juge des enfants <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Jugement du juge des enfants <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 4. g. - INSTITPLAC : personne ou institution à qui le mineur est confié

<table>
<tbody>
<tr>
<td align="center">CODE  <br/>
</td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>A l'autre parent <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>A un autre membre de la famille ou à un tiers digne de confiance <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>A un service départemental d'aide sociale à l'enfance <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>A un service ou à un établissement habilité pour l'accueil de mineurs à la journée <p>ou suivant toute autre modalité de prise en charge <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<br/>5 <br/>
</td>
<td align="center">
<br/>A un service ou à un établissement sanitaire ou d'éducation, ordinaire ou spécialisé <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>6 <br/>
</td>
<td align="center">
<br/>Auprès d'un établissement recevant des personnes hospitalisées <p>en raison de troubles mentaux <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 5. - Si intervention administrative d'aide à domicile mise en œuvre

VII. 5. a. - TYPINTERDOM : type d'intervention mise en œuvre

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Action d'un technicien de l'intervention sociale et familiale <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Mesure d'action éducative à domicile <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Mesure d'action éducative à domicile intensive ou renforcée <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Mesure d'action éducative à domicile avec hébergement périodique <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>5 <br/>
</td>
<td align="center">
<br/>Mesure d'action éducative à domicile avec hébergement exceptionnel <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>6 <br/>
</td>
<td align="center">
<br/>Mesure d'accompagnement en économie sociale et familiale <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>7 <br/>
</td>
<td align="center">
<br/>Autre <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>8 <br/>
</td>
<td align="center">
<br/>Intervention décidée mais pas mise en œuvre <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 5. b. - DATDEBAD/ DATFINAD : date de début et date de fin d'intervention

<table>
<tbody>
<tr>
<td align="center" rowspan="2">CODE </td>
<td align="center"> DATDEBAD </td>
<td align="center">DATFINAD  <br/>
</td>
</tr>
<tr>
<td align="center"> LIBELLÉ </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>jj <br/>
</td>
<td align="center">
<br/>Jour <br/>
</td>
<td align="center">
<br/>Jour <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>mm <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
</tr>
</tbody>
</table>

VII. 6. - Si décision administrative d'accueil provisoire

VII. 6. a. - LIEUACC : principal lieu d'accueil du mineur

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Assistant familial <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Etablissement <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Pouponnière <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Accueil mère-enfant <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>5 <br/>
</td>
<td align="center">
<br/>Chez un particulier <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>6 <br/>
</td>
<td align="center">
<br/>Hébergement autonome <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>7 <br/>
</td>
<td align="center">
<br/>Parrainage <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>8 <br/>
</td>
<td align="center">
<br/>Village d'enfant <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Lieu de vie <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>10 <br/>
</td>
<td align="center">
<br/>Etablissement médico-social <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>11 <br/>
</td>
<td align="center">
<br/>Accueil de jour <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>12 <br/>
</td>
<td align="center">
<br/>Accueil avec hébergement chez les parents (service d'accueil progressif en milieu naturel) <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>13 <br/>
</td>
<td align="center">
<br/>Accueil en internat ordinaire <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>14 <br/>
</td>
<td align="center">
<br/>Autre <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>15 <br/>
</td>
<td align="center">
<br/>Intervention décidée mais non mise en œuvre <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>99 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 6. b. - ACCMOD : caractère modulable de l'accueil

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 6. c. - AUTRLIEUACC : existence d'un autre lieu d'accueil régulier du mineur

<table>
<tbody>
<tr>
<td align="center"> CODE </td>
<td align="center"> LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 6. d. - DATDEBACC/ DATFINACC : date de début et de fin d'intervention

<table>
<tbody>
<tr>
<td rowspan="2">CODE <br/>
</td>
<td>DATDEBACC </td>
<td>DATEFINACC </td>
</tr>
<tr>
<td align="center">LIBELLÉ <br/>
</td>
<td align="center">LIBELLÉ <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>jj <br/>
</td>
<td align="center">
<br/>Jour <br/>
</td>
<td align="center">
<br/>Jour <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>mm <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
</tr>
</tbody>
</table>

VII. 7. - Si décision judiciaire d'action éducative en milieu ouvert ou d'investigation

VII. 7. a. - TYPDECJUD : type d'intervention mise en œuvre

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Enquête sociale <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Investigation d'orientation éducative <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Mesure judiciaire d'investigation éducative <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Expertise <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>5 <br/>
</td>
<td align="center">
<br/>Assistance éducative en milieu ouvert <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>6 <br/>
</td>
<td align="center">
<br/>Assistance éducative en milieu ouvert intensive ou renforcée <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>7 <br/>
</td>
<td align="center">
<br/>Assistance éducative en milieu ouvert avec hébergement périodique <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>8 <br/>
</td>
<td align="center">
<br/>Assistance éducative en milieu ouvert avec hébergement exceptionnel <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Mesure judiciaire d'aide à la gestion du budget familial <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>10 <br/>
</td>
<td align="center">
<br/>Autre <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>11 <br/>
</td>
<td align="center">
<br/>Intervention décidée mais non mise en œuvre <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>99 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 7. b. - DATDEBINTER/ DATFININTER : date de début et date de fin d'intervention

<table>
<tbody>
<tr>
<td align="center" rowspan="2">CODE </td>
<td align="center">DATBEBINTER </td>
<td align="center">DATFININTER </td>
</tr>
<tr>
<td align="center">LIBELLÉ </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>jj <br/>
</td>
<td align="center">
<br/>Jour <br/>
</td>
<td align="center">
<br/>Jour <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>mm <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
</tr>
</tbody>
</table>

VII. 8. - Si décision judiciaire de placement, personne ou structure à qui le mineur est confié

VII. 8. a. - LIEUPLAC : principal lieu de placement du mineur

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Assistant familial <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Etablissement <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Pouponnière <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Accueil mère-enfant <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>5 <br/>
</td>
<td align="center">
<br/>Chez un particulier <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>6 <br/>
</td>
<td align="center">
<br/>Hébergement autonome <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>7 <br/>
</td>
<td align="center">
<br/>Parrainage <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>8 <br/>
</td>
<td align="center">
<br/>Village d'enfant <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Lieu de vie <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>10 <br/>
</td>
<td align="center">
<br/>Etablissement médico-social <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>11 <br/>
</td>
<td align="center">
<br/>Accueil de jour <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>12 <br/>
</td>
<td align="center">
<br/>Accueil avec hébergement chez les parents <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>13 <br/>
</td>
<td align="center">
<br/>Accueil en internat ordinaire (internat scolaire, foyer de jeunes travailleurs, centre de formation des apprentis, etc.) <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>14 <br/>
</td>
<td align="center">
<br/>Hébergement collectif traditionnel <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>15 <br/>
</td>
<td align="center">
<br/>Hébergement collectif en centre de placement immédiat <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>16 <br/>
</td>
<td align="center">
<br/>Hébergement collectif <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>17 <br/>
</td>
<td align="center">
<br/>Hébergement individualisé de la protection judiciaire de la jeunesse <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>18 <br/>
</td>
<td align="center">
<br/>Famille d'accueil de la protection judiciaire de la jeunesse <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>19 <br/>
</td>
<td align="center">
<br/>Autre <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>20 <br/>
</td>
<td align="center">
<br/>Intervention décidée mais non mise en œuvre <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>99 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 8. b. - PLACMOD : caractère modulable de l'accueil

<table>
<tbody>
<tr>
<td align="center">CODE <br/>
</td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 8. c. - AUTRLIEUAR : existence d'un autre lieu d'accueil régulier du mineur

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 8. d. - DATDEBPLAC/ DATFINPLAC : date de début et date de fin d'intervention

<table>
<tbody>
<tr>
<td align="center" rowspan="2">CODE </td>
<td align="center">DATDEBPLAC </td>
<td align="center">DATFINPLAC </td>
</tr>
<tr>
<td align="center">LIBELLÉ </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>jj <br/>
</td>
<td align="center">
<br/>Jour <br/>
</td>
<td align="center">
<br/>Jour <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>mm <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
</tr>
</tbody>
</table>

VII. 9. - Renouvellement ou fin de l'intervention en protection de l'enfance

VII. 9. a. - MOTFININT : en cas de fin de l'intervention en protection de l'enfance, préciser le motif

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Mesure ou prestation arrivée à échéance <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Mainlevée <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Transfert du dossier dans un autre département avec maintien de la mesure <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Motif non connu <br/>
</td>
</tr>
</tbody>
</table>

VII. 9. b. - NOUVDECPE : si mesure ou prestation arrivée à échéance, nouvelle décision de protection de l'enfance

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Non <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Oui <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 9. c - NATNOUVDECPE : si nouvelle décision de protection de l'enfance, préciser la nature de la décision

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Décision administrative et prise en charge par l'aide sociale à l'enfance <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Décision judiciaire et prise en charge par l'aide sociale à l'enfance <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Décision judiciaire et prise en charge par la protection judiciaire de la jeunesse <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Nouvelle mesure de protection sans distinction supplémentaire <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 9. d. - MOTIFML : si mainlevée, motif de la mainlevée

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>10 <br/>
</td>
<td align="center">
<br/>Absence de motif de protection de l'enfance justifiant la mesure, ou absence de danger <p>ou risque de danger <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<br/>11 <br/>
</td>
<td align="center">
<br/>Impossibilité d'exercer la mesure <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>12 <br/>
</td>
<td align="center">
<br/>Déménagement annoncé de la famille dans un autre département avec arrêt de la mesure <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>13 <br/>
</td>
<td align="center">
<br/>Déménagement sans laisser d'adresse <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>14 <br/>
</td>
<td align="center">
<br/>Adoption <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>15 <br/>
</td>
<td align="center">
<br/>Majorité <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>16 <br/>
</td>
<td align="center">
<br/>Emancipation <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>17 <br/>
</td>
<td align="center">
<br/>Mesure jeune majeur <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>18 <br/>
</td>
<td align="center">
<br/>Décès du mineur <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>19 <br/>
</td>
<td align="center">
<br/>Autre <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>99 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 9. e. - SITAPML : situation du mineur après la mainlevée

<table>
<tbody>
<tr>
<td align="center">CODE <br/>
</td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Retour en milieu familial <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Autonomie <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Incarcération <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Autre type d'intervention <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 9. f. - TYPINTERV : si autre type d'intervention après la mainlevée

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Prise en charge médicale ou médico-sociale avec orientation de la commission des droits et de l'autonomie <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Prise en charge médicale ou médico-sociale sans orientation de la commission des droits et de l'autonomie <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Mesure pénale de placement <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Mesure pénale en milieu ouvert <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>5 <br/>
</td>
<td align="center">
<br/>Autre <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas <br/>
</td>
</tr>
</tbody>
</table>

VII. 9. g. - DATDECMIN : si décès du mineur, mois et année du décès

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>mm <br/>
</td>
<td align="center">
<br/>Mois <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>aaaa <br/>
</td>
<td align="center">
<br/>Année <br/>
</td>
</tr>
</tbody>
</table>

VII. 9. h. - DIPLOME : à la fin de l'intervention en protection de l'enfance, dernier diplôme obtenu par le mineur

<table>
<tbody>
<tr>
<td align="center">CODE </td>
<td align="center">LIBELLÉ </td>
</tr>
<tr>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>Aucun diplôme <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Brevet <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>CAP, BEP ou équivalent <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>4 <br/>
</td>
<td align="center">
<br/>Baccalauréat ou équivalent <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>5 <br/>
</td>
<td align="center">
<br/>Diplôme du supérieur <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>9 <br/>
</td>
<td align="center">
<br/>Ne sait pas<br/>
</td>
</tr>
</tbody>
</table>
