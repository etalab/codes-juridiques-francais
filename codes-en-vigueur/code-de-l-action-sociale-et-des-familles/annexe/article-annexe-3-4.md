# Article Annexe 3-4

Tableau de détermination et d'affectation des résultats de chaque section d'imputation tarifaire

d'un établissement hébergeant des personnes âgées dépendantes

<table>
<tbody>
<tr>
<td colspan="2" width="53">
<br/>
</td>
<td colspan="2" width="89">
<p align="center">EXERCICE <br/>DE L'ANNEE</p>
</td>
<td colspan="15" width="538">
<p align="center">SECTIONS D'IMPUTATION</p>
</td>
</tr>
<tr>
<td colspan="2" rowspan="2" width="53">
<p align="center">Rappel n° de Compte</p>
</td>
<td colspan="2" rowspan="2" width="89">
<p align="center">Intitulé</p>
</td>
<td colspan="4" width="153">
<p align="center">Hébergement</p>
</td>
<td colspan="4" width="143">
<p align="center">Dépendance</p>
</td>
<td colspan="4" width="137">
<p align="center">Soins</p>
</td>
<td colspan="3" width="107">
<p align="center">Total</p>
</td>
</tr>
<tr>
<td width="54">
<p align="center">Exé-cutoire</p>
</td>
<td width="42">
<p align="center">Réa-lisé</p>
</td>
<td width="43">
<p align="center">Rete-nu</p>
</td>
<td colspan="2" width="55">
<p align="center">Exé-cutoire</p>
</td>
<td width="43">
<p align="center">Réa-lisé</p>
</td>
<td width="38">
<p align="center">Rete-nu</p>
</td>
<td colspan="2" width="53">
<p align="center">Exé-cutoire</p>
</td>
<td width="41">
<p align="center">Réa-lisé</p>
</td>
<td width="38">
<p align="center">Rete-nu</p>
</td>
<td colspan="2" width="53">
<p align="center">Exé-cutoire</p>
</td>
<td width="38">
<p align="center">Réa-lisé</p>
</td>
<td width="39">
<p align="center">Rete-nu</p>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">602</p>
</td>
<td colspan="2" valign="top" width="89">
<p>ACHATS STOCKES ; AUTRES APPROVISIONNEMENTS sauf 6021et 60226 </p>
</td>
<td width="54">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">6021</p>
</td>
<td colspan="2" valign="top" width="89">
<p>Produits pharmaceutiques et produits à usage médical </p>
</td>
<td width="54">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">60226</p>
</td>
<td colspan="2" valign="top" width="89">
<p>Fournitures hôtelières sauf 602261 </p>
</td>
<td width="54">
<p align="center">70%</p>
</td>
<td width="42">
<p align="center">70%</p>
</td>
<td width="43">
<p align="center">70%</p>
</td>
<td colspan="2" width="55">
<p align="center">30%</p>
</td>
<td width="43">
<p align="center">30%</p>
</td>
<td width="38">
<p align="center">30%</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">602261</p>
</td>
<td colspan="2" valign="top" width="89">
<p>Couches, alèses et produits absorbants </p>
</td>
<td width="54">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">100%</p>
</td>
<td width="43">
<p align="center">100%</p>
</td>
<td width="38">
<p align="center">100%</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">603</p>
</td>
<td colspan="2" valign="top" width="89">
<p>VARIATION DES STOCKS sauf 60321 et 603226 </p>
</td>
<td width="54">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">60321</p>
</td>
<td colspan="2" valign="top" width="89">
<p>Produits pharmaceutiques et produits à usage médical </p>
</td>
<td width="54">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">603226</p>
</td>
<td colspan="2" valign="top" width="89">
<p>Fournitures hôtelières sauf 6032261 </p>
</td>
<td width="54">
<p align="center">70%</p>
</td>
<td width="42">
<p align="center">70%</p>
</td>
<td width="43">
<p align="center">70%</p>
</td>
<td colspan="2" width="55">
<p align="center">30%</p>
</td>
<td width="43">
<p align="center">30%</p>
</td>
<td width="38">
<p align="center">30%</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">6032261</p>
</td>
<td colspan="2" valign="top" width="89">
<p>Couches, alèses et produits absorbants </p>
</td>
<td width="54">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">100%</p>
</td>
<td width="43">
<p align="center">100%</p>
</td>
<td width="38">
<p align="center">100%</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">606</p>
</td>
<td colspan="2" valign="top" width="89">
<p>ACHATS NON STOCKES DE MATIERES ET FOURNITURES sauf 60622, 60626 et 6066 </p>
</td>
<td width="54">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">60622</p>
</td>
<td colspan="2" valign="top" width="89">
<p>Produits d'entretien </p>
</td>
<td width="54">
<p align="center">70%</p>
</td>
<td width="42">
<p align="center">70%</p>
</td>
<td width="43">
<p align="center">70%</p>
</td>
<td colspan="2" width="55">
<p align="center">30%</p>
</td>
<td width="43">
<p align="center">30%</p>
</td>
<td width="38">
<p align="center">30%</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">60626</p>
</td>
<td colspan="2" valign="top" width="89">
<p>Fournitures hôtelières sauf compte 606261 </p>
</td>
<td width="54">
<p align="center">70%</p>
</td>
<td width="42">
<p align="center">70%</p>
</td>
<td width="43">
<p align="center">70%</p>
</td>
<td colspan="2" width="55">
<p align="center">30%</p>
</td>
<td width="43">
<p align="center">30%</p>
</td>
<td width="38">
<p align="center">30%</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">606261</p>
</td>
<td colspan="2" valign="top" width="89">
<p>Couches, alèses et produits absorbants </p>
</td>
<td width="54">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">100%</p>
</td>
<td width="43">
<p align="center">100%</p>
</td>
<td width="38">
<p align="center">100%</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">6066</p>
</td>
<td colspan="2" valign="top" width="89">
<p>Fournitures médicales </p>
</td>
<td width="54">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">61</p>
</td>
<td colspan="2" valign="top" width="89">
<p>SERVICES EXTERIEUR sauf 6111, 61121, 61551, 61562 et 61681 </p>
</td>
<td width="54">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">6111</p>
</td>
<td colspan="2" valign="top" width="89">
<p>Prestations à caractère médical </p>
</td>
<td width="54">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">61121</p>
</td>
<td colspan="2" valign="top" width="89">
<p>Ergothérapie </p>
</td>
<td width="54">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">61551</p>
</td>
<td colspan="2" valign="top" width="89">
<p>Entretien et réparation sur biens mobiliers : matériel et outillage médicaux </p>
</td>
<td width="54">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">61562</p>
</td>
<td colspan="2" valign="top" width="89">
<p>Maintenance du matériel médical </p>
</td>
<td width="54">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">61681</p>
</td>
<td colspan="2" valign="top" width="89">
<p>Primes d'assurance maladie, maternité, accident du travail </p>
</td>
<td width="54">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">62</p>
</td>
<td colspan="2" valign="top" width="89">
<p>AUTRES SERVICES EXTERIEUR sauf 621, 62113, 6223, 6281, 6283 et 6287 pour les budgets annexes hospitaliers </p>
</td>
<td width="54">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">621</p>
</td>
<td colspan="2" valign="top" width="89">
<p>PERSONNEL EXTERIEUR A L'ETABLISSEMENT </p>
</td>
<td width="54">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">62113</p>
</td>
<td colspan="2" valign="top" width="89">
<p>Personnel intérimaire : personnel médical et para­médical </p>
</td>
<td width="54">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="2" width="53">
<p align="center">6223</p>
</td>
<td colspan="2" valign="top" width="89">
<p>Médecins </p>
</td>
<td width="54">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="4" width="142">
<p>628 sauf 6281 et 6282</p>
</td>
<td width="54">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td width="52">
<p align="center">6281</p>
</td>
<td colspan="3" width="90">
<p>Blanchissage à l'extérieur</p>
</td>
<td width="54">
<p align="center">70%</p>
</td>
<td width="42">
<p align="center">70%</p>
</td>
<td width="43">
<p align="center">70%</p>
</td>
<td colspan="2" width="55">
<p align="center">30%</p>
</td>
<td width="43">
<p align="center">30%</p>
</td>
<td width="38">
<p align="center">30%</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td width="52">
<p align="center">6283</p>
</td>
<td colspan="3" width="90">
<p>Nettoyage à l'extérieur</p>
</td>
<td width="54">
<p align="center">70%</p>
</td>
<td width="42">
<p align="center">70%</p>
</td>
<td width="43">
<p align="center">70%</p>
</td>
<td colspan="2" width="55">
<p align="center">30%</p>
</td>
<td width="43">
<p align="center">30%</p>
</td>
<td width="38">
<p align="center">30%</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td width="52">
<p align="center">631</p>
</td>
<td colspan="3" width="90">
<p>IMPÔTS, TAXES ET VERSEMENTS ASSIMILES SUR REMUNERATIONS (administrations des impôts)</p>
</td>
<td width="54">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td width="52">
<p align="center">633</p>
</td>
<td colspan="3" width="90">
<p>IMPÔTS, TAXES ET VERSEMENTS ASSIMILES SUR REMUNERATIONS (autres organismes)</p>
</td>
<td width="54">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td width="52">
<p align="center">635</p>
</td>
<td colspan="3" width="90">
<p>AUTRES IMPÔTS, TAXES ET VERSEMENTS ASSIMILES (administrations des impôts)</p>
</td>
<td width="54">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td width="52">
<p align="center">637</p>
</td>
<td colspan="3" width="90">
<p>Autres impôts, taxes et versements assimilés (autres organismes)</p>
</td>
<td width="54">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td width="52">
<p align="center">64</p>
</td>
<td colspan="3" width="90">
<p>CHARGES DE PERSONNEL</p>
</td>
<td width="54">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td width="52">
<p align="center">65</p>
</td>
<td colspan="3" width="90">
<p>AUTRES CHARGES DE GESTION COURANTE</p>
</td>
<td width="54">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td width="52">
<p align="center">66</p>
</td>
<td colspan="3" width="90">
<p>CHARGES FINANCIERES</p>
</td>
<td width="54">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td width="52">
<p align="center">67</p>
</td>
<td colspan="3" width="90">
<p>CHARGES EXCEPTIONNELLES</p>
</td>
<td valign="top" width="54"/>
<td valign="top" width="42">
<br/>
</td>
<td valign="top" width="43">
<br/>
</td>
<td colspan="2" valign="top" width="55"/>
<td valign="top" width="43">
<br/>
</td>
<td valign="top" width="38">
<br/>
</td>
<td colspan="2" valign="top" width="53"/>
<td valign="top" width="41"/>
<td valign="top" width="38"/>
<td colspan="2" valign="top" width="53"/>
<td valign="top" width="38"/>
<td valign="top" width="39"/>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">68: Dotations aux amortissements et aux provisions sauf matériel médical</p>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td valign="top" width="42">
<br/>
</td>
<td valign="top" width="43"/>
<td colspan="2" valign="top" width="55">
<br/>
</td>
<td valign="top" width="43">
<br/>
</td>
<td valign="top" width="38"/>
<td colspan="2" valign="top" width="53">
<br/>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td valign="top" width="38"/>
<td valign="top" width="39"/>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">Dotations aux amortissements du matériel médical</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">681518 : Dotations aux provisions: autres provisions pour risques (pathologies lourdes)</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">CHARGES BRUTES = A</p>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">PRODUITS AUTRES QUE CEUX RELATIFS A LA TARIFICATION sauf 734-4 = B 1</p>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">734-3 : Contribution de l'assurance maladie au titre de l'article R. 314-188 = B 2</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">PRODUITS DE LA TARIFICATION = C</p>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">NOMBRE DE POINTS GIR DANS L'ETABLISSEMENT AU 30 septembre = D</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">VALEUR NETTE DU POINT GIR "dépendance" = (A - B) / D = E</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td width="39">
<p align="center">●</p>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">QUOTE PART DES CHARGES RELATIVES AUX AIDES­ SOIGNANTES ET AUX AMP IMPUTEE SUR LA SECTION TARIFAIRE "SOINS" = F (cf: annexes)</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td width="39">
<p align="center">●</p>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">VALEUR NETTE DU POINT GIR "Aides soignantes et AMP" = F / (GMP multiplié par la capacité occupée)</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td width="39">
<p align="center">●</p>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">NOMBRE DE JOURNEES REALISEES = G</p>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">CALCUL DE LA VALEUR NETTE DU POINT GIR "SOINS" charges de la section "soins" - produits autres que ceux de la tarification / GMP X capacité moyenne de l'exercice</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td colspan="2" width="53">
<p align="center">●</p>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td width="39">
<p align="center">●</p>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">RESULTATS DE L'EXERCICE = <br/>(B 1 + B 2 + C ) - A = H</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">REPRISE DES RESULTATS EXERCICES ANTERIEURS : I</p>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">RESULTATS A AFFECTER = H + I</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">Résultat affecté au compte 1064 : réserve des plus values nettes</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">Résultat affecté au compte 10682 : excédent affecté à l'investissement</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">Résultat affecté au compte 10685 : excédent affecté à la réserve de trésorerie</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">Résultat affecté au compte 10686 : excédent affecté à la réserve de compensation</p>
</td>
<td colspan="2" width="55">
<p align="center">●</p>
</td>
<td width="42">
<p align="center">●</p>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<p align="center">●</p>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<p align="center">●</p>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<p align="center">●</p>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">Résultat affecté au compte 10 687 : excédent affecté à la réserve de compensation des charges d'amortissement </p>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">Résultat affecté au compte 110 : excédent affecté à la réduction des charges d'exploitation (établissement public) ;</p>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="3" width="142">
<p align="center">Résultat affecté au compte 111 : excédent affecté au financement de mesures d'exploitation non reconductibles (établissement public)</p>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="42">
<br/>
</td>
<td width="43">
<br/>
</td>
<td colspan="2" width="55">
<br/>
</td>
<td width="43">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="41">
<br/>
</td>
<td width="38">
<br/>
</td>
<td colspan="2" width="53">
<br/>
</td>
<td width="38">
<br/>
</td>
<td width="39">
<br/>
</td>
</tr>
<tr>
<td colspan="3">
<p align="center">  Dépenses refusées par l'autorité de tarification en application de l'article R. 314-52 (compte 114) </p>
</td>
<td colspan="2"/>
<td/>
<td/>
<td colspan="2"/>
<td/>
<td/>
<td colspan="2"/>
<td/>
<td/>
<td colspan="2"/>
<td/>
<td/>
</tr>
<tr>
<td colspan="3">
<p align="center"> Résultat affecté au compte 119 : report à nouveau déficitaire (établissement public)</p>
</td>
<td colspan="2"/>
<td/>
<td/>
<td colspan="2"/>
<td/>
<td/>
<td colspan="2"/>
<td/>
<td/>
<td colspan="2"/>
<td/>
<td/>
</tr>
<tr>
<td colspan="3">
<p align="center"> Résultat affecté au compte 11 510 : excédent affecté à la réduction des charges d'exploitation (établissement privé) </p>
</td>
<td colspan="2"/>
<td/>
<td/>
<td colspan="2"/>
<td/>
<td/>
<td colspan="2"/>
<td/>
<td/>
<td colspan="2"/>
<td/>
<td/>
</tr>
<tr>
<td colspan="3">
<p align="center"> Résultat affecté au compte 11 511 : excédent affecté au financement de mesures d'exploitation non reconductibles (établissement privé)</p>
</td>
<td colspan="2"/>
<td/>
<td/>
<td colspan="2"/>
<td/>
<td/>
<td colspan="2"/>
<td/>
<td/>
<td colspan="2"/>
<td/>
<td/>
</tr>
<tr>
<td colspan="3">
<p align="center"> Résultat affecté au compte 11 519 : report à nouveau déficitaire (établissement privé).</p>
</td>
<td colspan="2"/>
<td/>
<td/>
<td colspan="2"/>
<td/>
<td/>
<td colspan="2"/>
<td/>
<td/>
<td colspan="2"/>
<td/>
<td/>
</tr>
</tbody>
</table>
