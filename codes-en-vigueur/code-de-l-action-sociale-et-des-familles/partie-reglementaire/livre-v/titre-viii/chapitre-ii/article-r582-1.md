# Article R582-1

<div align="left">Les dispositions du chapitre II du titre II du livre V sont applicables à Saint-Barthélemy et à Saint-Martin, à l'exception des articles R. 522-10 à R. 522-62.<br/>
</div>
