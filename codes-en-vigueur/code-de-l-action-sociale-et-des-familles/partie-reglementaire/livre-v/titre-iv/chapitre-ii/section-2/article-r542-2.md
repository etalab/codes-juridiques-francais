# Article R542-2

Pour l'application du titre II du livre II :

I.-A l'article R. 223-2 :

1° Le premier alinéa est complété par une phrase ainsi rédigée :

" La notification de la décision d'attribution ou de modification peut être remise en mains propres contre accusé de réception. " ;

2° Après les mots : " recours ", sont ajoutés les mots : " selon les modalités prévues à l'article R. 134-10. "

II.-A l'article R. 223-8 et au deuxième alinéa de l'article R. 224-24, après les mots : " avec demande d'avis de réception " sont ajoutés les mots : " ou lettre remise en mains propres contre accusé de réception. "

III.-Au deuxième alinéa de l'article R. 224-10 et au premier alinéa de l'article R. 224-19, les mots : " selon le II de l'article 34 de la loi n° 82-213 du 2 mars 1982 relative aux droits et libertés des communes, des départements et des régions " et les mots : ", conformément au II de l'article 34 de la loi n° 82-213 du 2 mars 1982 relative aux droits et libertés des communes, des départements et des régions " sont supprimés.

IV.-A l'article R. 225-1, les mots : " aux articles L. 225-2 et L. 225-15 " sont remplacés par les mots : " à l'article L. 225-2 ".

V.-A l'article R. 225-2, les 4° et 5° ne sont pas applicables.

VI.-Au troisième alinéa de l'article R. 225-4, les mots : " ou d'un enfant étranger " ne sont pas applicables.

VII.-Les dispositions des sections 2 à 4 du chapitre V ne sont pas applicables.

VIII.-A l'article D. 226-3-6, les mots : " recteur d'académie " sont remplacés par les mots : " vice-recteur d'académie ".

IX.-Au second alinéa de l'article R. 227-21 et à l'avant-dernier alinéa de l'article R. 227-22, les mots : " Le directeur régional de la jeunesse, des sports et de la vie associative " sont remplacés par les mots : " Le directeur de la jeunesse, des sports et de la cohésion sociale ".

X.-A l'annexe 2-8 du présent code, les mots : " foyer de jeunes travailleurs " ne sont pas applicables.
