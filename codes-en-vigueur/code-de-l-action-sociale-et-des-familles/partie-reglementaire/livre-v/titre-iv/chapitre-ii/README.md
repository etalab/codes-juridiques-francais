# Chapitre II : Adaptations du livre II

- [Section 1 : Famille](section-1)
- [Section 2 : Enfance](section-2)
- [Section 3 : Personnes âgées](section-3)
- [Section 4 : Personnes handicapées](section-4)
- [Section 5 : Personnes non bénéficiaires de la couverture maladie universelle](section-5)
- [Section 6 : Lutte contre la pauvreté et les exclusions](section-6)
- [Section 7 : Accompagnement de la personne en matière sociale et budgétaire](section-7)
