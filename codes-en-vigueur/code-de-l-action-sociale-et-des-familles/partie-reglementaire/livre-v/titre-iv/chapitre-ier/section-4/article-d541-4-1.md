# Article D541-4-1

Après les mots : " à l'article L. 545-1 " figurant au I et après les mots : " de l'article L. 545-1 " figurant à l'avant dernier-alinéa du II, sont ajoutés les mots : " tel que maintenu en vigueur jusqu'à une date fixée par décret et au plus tard le 1er janvier 2016, en application du second alinéa du 3° de l'article 10 de l'ordonnance n° 2012-785 du 31 mai 2012 portant extension et adaptation du code de l'action sociale et des familles au Département de Mayotte ".
