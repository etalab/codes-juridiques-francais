# Article R522-29-1

En vue de favoriser l'insertion sociale et professionnelle des bénéficiaires du revenu de solidarité active, l'agence d'insertion affecte à l'exécution de tâches d'utilité sociale ceux d'entre eux avec lesquels elle a signé le contrat d'insertion par l'activité institué par l'article L. 522-8.

Elle recense les besoins en tâches d'utilité sociale existant dans le département, en liaison avec les collectivités territoriales ou groupements de communes et        Pôle emploi.

Ces tâches, assurées par l'agence elle-même ou par les collectivités, personnes ou organismes mentionnés à l'article L. 5134-21 du code du travail, doivent répondre à des besoins collectifs non satisfaits dans les conditions économiques locales.
