# Article R522-51

Les frais engagés pour permettre aux salariés recrutés par un contrat d'insertion par l'activité de suivre une formation complémentaire non rémunérée peuvent être pris en charge par l'agence d'insertion.

Cette formation, qui contribue, le cas échéant, à la réalisation du projet d'insertion contenu dans le contrat d'insertion, doit être dispensée dans le cadre d'une convention passée par l'agence avec un organisme de formation.

Lorsque le contrat d'insertion par l'activité est rompu avant le terme de la formation, les sommes déjà versées correspondant aux heures de formation non effectuées font l'objet d'un reversement à l'agence.
