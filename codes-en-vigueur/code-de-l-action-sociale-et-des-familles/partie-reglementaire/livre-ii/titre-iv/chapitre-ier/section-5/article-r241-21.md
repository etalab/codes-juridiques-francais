# Article R241-21

L'usage indu de la carte d'invalidité, de la carte de stationnement pour personnes handicapées ou de la canne blanche est puni de l'amende prévue pour les contraventions de la 5e classe.

La récidive de la contravention prévue au présent article est réprimée conformément à l'article 132-11 du code pénal.
