# Article R243-8

Au plus tard le 30 avril de chaque année, les établissements ou les services d'aide par le travail doivent présenter au directeur départemental des affaires sanitaires et sociales un rapport sur leur politique en faveur des travailleurs handicapés qu'ils accueillent, en particulier en matière de rémunération garantie versée et de mise en oeuvre d'actions de formation.

Sur le fondement de ce rapport, une convention ou, le cas échéant, un avenant à la convention mentionnée à l'article R. 344-7 est signé entre le représentant de l'Etat dans le département et l'organisation gestionnaire.

Cette convention est conclue pour une durée maximale de trois ans et peut être dénoncée chaque année dans des conditions prévues par la convention.

Elle peut fixer un objectif d'augmentation du taux moyen de financement de la rémunération garantie par l'établissement ou le service d'aide par le travail, en prenant en compte notamment l'amélioration constatée de la productivité moyenne des personnes accueillies et l'accroissement de la valeur ajoutée dégagée par l'exploitation. Elle définit des orientations en matière de formation des travailleurs handicapés.

Cet objectif d'augmentation doit demeurer compatible avec le projet de l'établissement ou du service d'aide par le travail. Il ne peut avoir pour effet de remettre en cause des investissements nécessaires à l'accomplissement de la mission qui lui est assignée par l'article L. 344-2.
