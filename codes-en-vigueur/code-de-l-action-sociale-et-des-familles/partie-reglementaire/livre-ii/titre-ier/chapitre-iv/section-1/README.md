# Section 1 : Commission départementale de l'accueil des jeunes enfants

- [Article D214-1](article-d214-1.md)
- [Article D214-2](article-d214-2.md)
- [Article D214-3](article-d214-3.md)
- [Article D214-4](article-d214-4.md)
- [Article D214-5](article-d214-5.md)
- [Article D214-6](article-d214-6.md)
