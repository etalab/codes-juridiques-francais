# Article R232-10

Les tarifs nationaux mentionnés à l'article L. 232-3 sont fixés de la manière suivante :

1° Pour les personnes classées dans le groupe 1 de la grille nationale mentionnée à l'article R. 232-3 à 1,19 fois le montant de la majoration pour aide constante d'une tierce personne mentionnée à l'article L. 355-1 du code de la sécurité sociale ;

2° Pour les personnes classées dans le groupe 2 de la grille nationale à 1,02 fois le montant de la majoration pour aide constante d'une tierce personne précitée ;

3° Pour les personnes classées dans le groupe 3 de la grille nationale à 0,765 fois le montant de la majoration pour aide constante d'une tierce personne précitée ;

4° Pour les personnes classées dans le groupe 4 de la grille nationale à 0,51 fois le montant de la majoration pour aide constante d'une tierce personne précitée.

Les coefficients susmentionnés sont, le cas échéant, automatiquement majorés de façon à ce que la revalorisation annuelle des tarifs nationaux mentionnés au premier alinéa ne soit pas inférieure à l'évolution des prix à la consommation hors tabac prévue à l'article L. 232-3.
