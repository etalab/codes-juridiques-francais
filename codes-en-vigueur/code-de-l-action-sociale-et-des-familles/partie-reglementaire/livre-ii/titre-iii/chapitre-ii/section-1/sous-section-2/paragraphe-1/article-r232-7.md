# Article R232-7

La demande d'allocation personnalisée d'autonomie est instruite par une équipe médico-sociale qui comprend au moins un médecin et un travailleur social.

Au cours de la visite à domicile effectuée par l'un au moins des membres de l'équipe médico-sociale, l'intéressé et, le cas échéant, son tuteur ou ses proches reçoivent tous conseils et informations en rapport avec le besoin d'aide du postulant à l'allocation personnalisée d'autonomie. Ils sont notamment informés que l'équipe médico-sociale doit avoir connaissance de tout changement dans la situation de l'intéressé.

Au cours de son instruction, l'équipe médico-sociale consulte le médecin désigné, le cas échéant, par le demandeur. Si l'intéressé le souhaite, ce médecin assiste à la visite à domicile prévue à l'alinéa précédent. L'équipe médico-sociale procède à la même consultation à l'occasion de la révision de l'allocation personnalisée d'autonomie.

Dans un délai de trente jours à compter de la date du dépôt du dossier de demande complet, l'équipe médico-sociale adresse une proposition de plan d'aide à l'intéressé, assortie de l'indication du taux de sa participation financière. Celui-ci dispose d'un délai de dix jours, à compter de la date de réception de la proposition, pour présenter ses observations et en demander la modification ; dans ce cas, une proposition définitive lui est adressée dans les huit jours. En cas de refus exprès ou d'absence de réponse de l'intéressé à cette proposition dans le délai de dix jours, la demande d'allocation personnalisée d'autonomie est alors réputée refusée.

Lorsque le degré de perte d'autonomie de l'intéressé ne justifie pas l'établissement d'un plan d'aide, un compte-rendu de visite est établi.
