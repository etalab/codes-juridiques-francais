# Sous-section 3 : Allocation personnalisée d'autonomie en établissement

- [Paragraphe 1 : Dispositions générales](paragraphe-1)
- [Paragraphe 2 : Dispositions particulières relatives à certains établissements](paragraphe-2)
