# Article R232-56

Les disponibilités excédant les besoins de trésorerie du fonds peuvent faire l'objet de placements dans les conditions prévues par le décret n° 2012-1246 du 7 novembre 2012 relatif à la gestion budgétaire et comptable publique. Le produit de ces placements est affecté au financement des dépenses incombant au fonds en application de l'article L. 232-21.
