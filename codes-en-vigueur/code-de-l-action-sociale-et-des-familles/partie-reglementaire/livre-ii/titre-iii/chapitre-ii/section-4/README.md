# Section 4 : Dispositions communes

- [Article R232-58](article-r232-58.md)
- [Article R232-59](article-r232-59.md)
- [Article R232-61](article-r232-61.md)
