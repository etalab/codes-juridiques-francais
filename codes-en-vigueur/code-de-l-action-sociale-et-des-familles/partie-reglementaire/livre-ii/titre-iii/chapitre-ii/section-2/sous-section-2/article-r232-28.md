# Article R232-28

La décision déterminant le montant de l'allocation personnalisée d'autonomie fait l'objet d'une révision périodique dans le délai qu'elle détermine en fonction de l'état du bénéficiaire. Elle peut aussi être révisée à tout moment à la demande de l'intéressé, ou le cas échéant de son représentant légal, ou à l'initiative du président du conseil départemental si des éléments nouveaux modifient la situation personnelle du bénéficiaire au vu de laquelle cette décision est intervenue.
