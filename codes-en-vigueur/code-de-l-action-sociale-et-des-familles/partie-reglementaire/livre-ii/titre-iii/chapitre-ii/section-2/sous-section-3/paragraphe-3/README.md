# Paragraphe 3 : Dispositions particulières à l'allocation en établissement

- [Article D232-35](article-d232-35.md)
- [Article R232-34](article-r232-34.md)
