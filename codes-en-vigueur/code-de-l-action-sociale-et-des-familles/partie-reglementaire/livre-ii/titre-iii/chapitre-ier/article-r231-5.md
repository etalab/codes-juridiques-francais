# Article R231-5

Le placement dans un établissement comporte, soit le logement seulement, soit l'ensemble de l'entretien.
