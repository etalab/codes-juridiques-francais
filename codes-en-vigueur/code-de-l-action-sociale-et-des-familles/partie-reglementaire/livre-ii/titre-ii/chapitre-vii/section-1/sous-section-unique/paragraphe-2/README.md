# Paragraphe 2 : Dispositions relatives à la qualification des personnes encadrant les mineurs dans les accueils collectifs de mineurs à caractère éducatif.

- [Article R227-12](article-r227-12.md)
- [Article R227-13](article-r227-13.md)
- [Article R227-14](article-r227-14.md)
- [Article R227-15](article-r227-15.md)
- [Article R227-16](article-r227-16.md)
- [Article R227-17](article-r227-17.md)
- [Article R227-18](article-r227-18.md)
- [Article R227-19](article-r227-19.md)
- [Article R227-20](article-r227-20.md)
- [Article R227-21](article-r227-21.md)
- [Article R227-22](article-r227-22.md)
