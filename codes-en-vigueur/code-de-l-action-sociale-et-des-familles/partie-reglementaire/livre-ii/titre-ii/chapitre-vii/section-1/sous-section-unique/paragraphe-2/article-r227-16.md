# Article R227-16

Pour l'encadrement des enfants en accueils de loisirs périscolaires, lorsqu'il relève des dispositions de l'article L. 227-4, l'effectif minimum des personnes exerçant des fonctions d'animation est fixé comme suit :

1° Un animateur pour dix mineurs âgés de moins de six ans ;

2° Un animateur pour quatorze mineurs âgés de six ans ou plus.
