# Section 3 : Transmission d'informations sous forme anonyme aux observatoires départementaux de la protection de l'enfance et à l'Observatoire national de l'enfance en danger

- [Article D226-3-1](article-d226-3-1.md)
- [Article D226-3-2](article-d226-3-2.md)
- [Article D226-3-3](article-d226-3-3.md)
- [Article D226-3-4](article-d226-3-4.md)
- [Article D226-3-5](article-d226-3-5.md)
- [Article D226-3-6](article-d226-3-6.md)
- [Article D226-3-7](article-d226-3-7.md)
