# Article D226-3-7

Le recueil et l'enregistrement des informations mentionnées à l'article D. 226-3-3, en vue de leur transmission à l'Observatoire national de l'enfance en danger, prennent fin à la majorité des mineurs.

Aux fins d'exploitation statistique, l'Observatoire national de l'enfance en danger conserve pendant une durée de trois ans après la majorité des mineurs les données anonymisées qu'il détient. Au-delà de cette durée, l'Observatoire national de l'enfance en danger conserve un échantillon représentatif de 20 % de chaque tranche d'âge, aux fins d'études et de recherches.
