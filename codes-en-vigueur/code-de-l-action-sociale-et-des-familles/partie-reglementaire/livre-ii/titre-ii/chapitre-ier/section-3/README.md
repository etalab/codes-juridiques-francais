# Section 3 : Transmission d'informations entre départements

- [Article R221-5](article-r221-5.md)
- [Article R221-5-1](article-r221-5-1.md)
- [Article R221-5-2](article-r221-5-2.md)
- [Article R221-5-3](article-r221-5-3.md)
- [Article R221-6](article-r221-6.md)
- [Article R221-7](article-r221-7.md)
- [Article R221-8](article-r221-8.md)
- [Article R221-9](article-r221-9.md)
- [Article R221-10](article-r221-10.md)
