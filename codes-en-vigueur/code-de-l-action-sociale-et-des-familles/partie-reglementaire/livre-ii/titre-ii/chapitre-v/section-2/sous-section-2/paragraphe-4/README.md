# Paragraphe 4 : Retrait d'autorisation et interdiction de fonctionnement.

- [Article R225-30](article-r225-30.md)
- [Article R225-31](article-r225-31.md)
- [Article R225-32](article-r225-32.md)
