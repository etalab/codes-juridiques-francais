# Chapitre II : Revenu de solidarité active

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Prestation de revenu de solidarité active](section-2)
- [Section 3 : Droits et devoirs des bénéficiaires du revenu de solidarité active](section-3)
- [Section 4 : Contrôle, contentieux et lutte contre la fraude](section-4)
- [Section 5 : Recours et récupération](section-5)
- [Section 6 : Echanges d'informations et suivi statistique](section-6)
