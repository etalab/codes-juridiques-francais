# Section 3 : Droits et devoirs des bénéficiaires du revenu de solidarité active

- [Article D262-65](article-d262-65.md)
- [Article D262-73](article-d262-73.md)
- [Article R262-65-1](article-r262-65-1.md)
- [Article R262-65-2](article-r262-65-2.md)
- [Article R262-65-3](article-r262-65-3.md)
- [Article R262-66](article-r262-66.md)
- [Article R262-67](article-r262-67.md)
- [Article R262-68](article-r262-68.md)
- [Article R262-69](article-r262-69.md)
- [Article R262-70](article-r262-70.md)
- [Article R262-71](article-r262-71.md)
- [Article R262-72](article-r262-72.md)
