# Article D262-56

Chaque année, sur proposition du président, le conseil de gestion adopte, avant le 31 mars :

1° Pour l'exercice à venir,        le budget afférent aux obligations de toute nature incombant au fonds ;

2° Le bilan, le compte de résultat et le rapport d'activité concernant l'exercice écoulé.
