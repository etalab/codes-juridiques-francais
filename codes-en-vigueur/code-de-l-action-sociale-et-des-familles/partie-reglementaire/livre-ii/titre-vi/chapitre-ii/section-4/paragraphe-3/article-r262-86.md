# Article R262-86

La procédure contradictoire applicable pour prononcer la sanction mentionnée à l'article L. 262-53 est celle applicable au titre de l'article L. 262-52.
