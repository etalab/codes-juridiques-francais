# Chapitre V : Statut des personnes accueillies par des organismes d'accueil communautaire et d'activités solidaires.

- [Section 1 : Agrément des organismes mentionnés à l'article L. 265-1.](section-1)
- [Section 2 :  Suivi, renouvellement et retrait de l'agrément.](section-2)
