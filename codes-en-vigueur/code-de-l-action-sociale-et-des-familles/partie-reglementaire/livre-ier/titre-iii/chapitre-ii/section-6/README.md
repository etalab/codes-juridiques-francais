# Section 6 : Hypothèque légale.

- [Article R132-13](article-r132-13.md)
- [Article R132-14](article-r132-14.md)
- [Article R132-15](article-r132-15.md)
- [Article R132-16](article-r132-16.md)
