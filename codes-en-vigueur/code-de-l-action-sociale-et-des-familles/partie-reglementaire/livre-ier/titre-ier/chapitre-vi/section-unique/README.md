# Section unique : Comité national pour la bientraitance et les droits des personnes âgées et des personnes handicapées

- [Article D116-1](article-d116-1.md)
- [Article D116-2](article-d116-2.md)
- [Article D116-3](article-d116-3.md)
