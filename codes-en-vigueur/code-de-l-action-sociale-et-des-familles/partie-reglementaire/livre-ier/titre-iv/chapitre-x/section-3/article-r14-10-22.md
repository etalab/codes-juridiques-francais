# Article R14-10-22

Sous réserve des dispositions de l'article R. 14-10-48, la Caisse nationale de solidarité pour l'autonomie est soumise aux dispositions des titres Ier et III du décret n° 2012-1246 du 7 novembre 2012 relatif à la gestion budgétaire et comptable publique.
