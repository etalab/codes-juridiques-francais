# Article R14-10-41

A l'issue de l'exercice, les départements communiquent à la Caisse nationale de solidarité pour l'autonomie, au plus tard le 30 juin, un état récapitulatif visé par le comptable du département du chapitre individualisé relatif à la dépense d'allocation personnalisée d'autonomie, faisant apparaître, par article budgétaire, pour l'exercice clos, les montants des mandats et des titres émis, diminués des mandats et titres d'annulation, ainsi que le nombre de bénéficiaires de l'allocation personnalisée d'autonomie au 31 décembre de l'année écoulée.

Sur demande de la caisse qui aurait constaté une incohérence dans les données transmises, le département lui transmet des données corrigées au plus tard le 31 août.

Les départements communiquent également à la caisse, à sa demande, toute information complémentaire relative à l'allocation personnalisée d'autonomie nécessaire à l'exercice de sa mission.
