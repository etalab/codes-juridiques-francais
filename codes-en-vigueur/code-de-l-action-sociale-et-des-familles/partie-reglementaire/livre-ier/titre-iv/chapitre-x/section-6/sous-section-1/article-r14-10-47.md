# Article R14-10-47

Pour l'application des dispositions prévues aux b des 1 et 2 du I de l'article L. 14-10-5, les régimes d'assurance maladie notifient à la caisse les charges afférentes à l'accueil de leurs affiliés dans les établissements et services mentionnés aux 1 et 2 du même article, dans des conditions définies dans les conventions mentionnées à l'article R. 14-10-46.
