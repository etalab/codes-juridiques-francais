# Section 6 : Ressources et charges

- [Sous-section 1 : Dispositions financières générales](sous-section-1)
- [Sous-section 2 : Modernisation des services d'aide à domicile, promotion des actions innovantes et professionnalisation des métiers de service concernant les personnes âgées et les personnes handicapées](sous-section-2)
