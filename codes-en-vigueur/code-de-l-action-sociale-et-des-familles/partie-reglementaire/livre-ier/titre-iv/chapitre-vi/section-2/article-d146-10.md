# Article D146-10

Le conseil départemental consultatif des personnes handicapées, prévu à l'article L. 146-2, comprend trente membres titulaires au maximum, dont :

1° Pour un tiers, des représentants des services déconcentrés de l'Etat, des collectivités territoriales et des principaux organismes qui, par leurs interventions ou leurs concours financiers, apportent une contribution significative à l'action en faveur des personnes handicapées du département, dans tous les domaines de leur vie sociale et professionnelle, nommés par le préfet.

Les représentants de l'Etat et des collectivités territoriales sont en nombre égal.

Les représentants du département et des communes sont nommés respectivement sur proposition du président du conseil général et de l'association départementale des maires ou, à Paris, du maire de Paris. Les représentants des organismes mentionnés ci-dessus sont nommés sur proposition de ceux-ci ;

2° Pour un tiers, des représentants dans le département des associations de personnes handicapées et de leurs familles, nommés par le préfet sur proposition des associations concernées ;

3° Pour un tiers, des personnes en activité au sein des principales professions de l'action sanitaire et sociale et de l'insertion professionnelle en direction des personnes handicapées et de personnalités qualifiées. Les représentants des professions sont nommés par le préfet, sur proposition des organisations syndicales représentatives du secteur concerné, de salariés et d'employeurs. Les personnes qualifiées sont nommées par le préfet, après avis du président du conseil général.

Un nombre égal de membres suppléants est nommé dans les mêmes conditions.
