# Article R146-39

Les catégories d'informations enregistrées dans le traitement sont les suivantes :

1° Informations portant sur la personne handicapée :

a) Numéro d'inscription au répertoire national d'identification des personnes physiques ;

b) Nom de famille, prénoms et, le cas échéant, nom d'usage ;

c) Date et lieu de naissance, sexe ;

d) Nationalité, selon l'une des catégories suivantes : Français, ressortissant de l'Union européenne, ressortissant d'un pays tiers ;

e) Adresse du domicile et, s'il y a lieu, de résidence ;

f) Nature du diagnostic médical, des déficiences et des limitations d'activité, désignées par référence aux classifications reconnues en matière de maladies et de handicaps ainsi qu'aux nomenclatures de limitation d'activité, recensées par arrêté du ministre chargé des personnes handicapées ;

g) Le cas échéant, régime de protection juridique ;

h) Situation familiale, composition de la famille, existence d'aidants familiaux et, dans le cas des mineurs, situation au regard de l'emploi des parents ou du représentant légal et, le cas échéant, des aidants familiaux ;

i) Niveau de formation et situation professionnelle du demandeur ;

j) Dans le cas où la demande porte sur l'une des prestations mentionnées aux articles L. 541-1, L. 821-1 et L. 821-2 du code de la sécurité sociale et à l'article L. 245-1 du code de l'action sociale et des familles, ressources prises en compte pour l'attribution de ces prestations et domiciliation bancaire ;

2° Informations portant sur le représentant légal du demandeur lorsque celui-ci est un mineur ou un majeur protégé :

a) Numéro d'inscription au répertoire national d'identification des personnes physiques ;

b) Nom de famille, prénoms et, le cas échéant, nom d'usage ;

c) Adresses ;

d) Date et lieu de naissance, sexe ;

e) Nature du mandat au titre duquel est exercée la fonction de représentant légal ;

3° Informations relatives à la nature des demandes et à la suite qui leur est donnée :

a) Nature et objet de la demande ;

b) Dates des différentes étapes de l'instruction et de l'examen de la demande par la commission des droits et de l'autonomie des personnes handicapées ;

c) Composition de l'équipe pluridisciplinaire ;

d) Résultats de l'évaluation de l'incapacité permanente et des besoins de compensation de la personne handicapée, exprimés par référence aux nomenclatures de limitation d'activité fixées par arrêté du ministre chargé des personnes handicapées ;

e) Contenu du plan personnalisé de compensation du handicap ;

f) Nature, objet, date, durée de validité et contenu des décisions rendues par la commission des droits et de l'autonomie des personnes handicapées ;

g) Le cas échéant, dates et nature des recours et suite qui leur est donnée ;

4° Informations relatives à l'équipe pluridisciplinaire et aux agents d'instruction :

a) Nom de famille, prénoms et, le cas échéant, nom d'usage ;

b) Adresse professionnelle ;

c) Qualité ;

5° Informations relatives aux membres de la commission des droits et de l'autonomie des personnes handicapées :

a) Nom de famille, prénoms et, le cas échéant, nom d'usage ;

b) Adresses ;

c) Qualité ;

d) Date de nomination.
