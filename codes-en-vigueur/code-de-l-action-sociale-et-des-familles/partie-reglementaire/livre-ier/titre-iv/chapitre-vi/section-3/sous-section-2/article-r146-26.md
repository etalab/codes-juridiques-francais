# Article R146-26

La demande est accompagnée d'un certificat médical de moins de trois mois et, le cas échéant, des éléments d'un projet de vie.

Les modèles de formulaires de demande ainsi que la liste des pièces justificatives à fournir sont fixés par arrêté du ministre chargé des personnes handicapées.

Lorsque la demande est accompagnée de l'ensemble des documents prévus aux deux alinéas précédents, elle est recevable.

Le formulaire de demande doit être accessible aux personnes handicapées ; à défaut, la maison départementale des personnes handicapées assure à ces personnes, par tout moyen, une aide à la formulation de leur demande.
