# Article D146-7

Une commission permanente, présidée par le président du conseil national et composée d'au plus de vingt membres du conseil, nommés par le ministre chargé des personnes handicapées après consultation du conseil national, est chargée, avec le concours de la      direction générale de la cohésion sociale , de la préparation et du suivi des travaux du conseil.
