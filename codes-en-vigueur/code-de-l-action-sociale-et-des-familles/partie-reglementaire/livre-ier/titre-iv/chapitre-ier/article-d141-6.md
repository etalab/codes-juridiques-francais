# Article D141-6

Le haut conseil est assisté d'un secrétaire général nommé par le Premier ministre. Le secrétaire général assure, sous l'autorité du président, l'organisation des travaux du conseil ainsi que l'établissement des rapports.
