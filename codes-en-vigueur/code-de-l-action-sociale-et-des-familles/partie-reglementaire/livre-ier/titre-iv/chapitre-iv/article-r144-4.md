# Article R144-4

L'observatoire se réunit au moins deux fois par an sur convocation de son président ou à la demande du ministre chargé des affaires sociales. Il peut également être réuni sur demande du tiers de ses membres. La direction de la recherche, des études, de l'évaluation et des statistiques du ministère des affaires sociales assure le secrétariat de l'observatoire.
