# Sous-section 2 : Dispositions relatives au centre communal d'action sociale

- [Paragraphe 1 : Composition du conseil d'administration.](paragraphe-1)
- [Paragraphe 2 : Fonctionnement du conseil d'administration.](paragraphe-2)
