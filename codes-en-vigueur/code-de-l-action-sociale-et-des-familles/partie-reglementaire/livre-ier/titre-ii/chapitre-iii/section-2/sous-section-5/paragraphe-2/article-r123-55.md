# Article R123-55

En cas d'extrême urgence, le directeur de section peut accorder à une personne en difficulté les aides relevant normalement du comité de gestion ou de la commission permanente qui en sont informés lors de leur prochaine séance. La somme des dépenses effectuées à ce titre ne peut pas dépasser la limite fixée pour chaque arrondissement par le conseil d'administration du centre.
