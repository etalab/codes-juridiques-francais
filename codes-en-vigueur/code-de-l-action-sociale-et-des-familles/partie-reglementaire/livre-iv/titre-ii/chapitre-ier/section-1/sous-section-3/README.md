# Sous-section 3 : Commission consultative paritaire départementale.

- [Article R421-27](article-r421-27.md)
- [Article R421-28](article-r421-28.md)
- [Article R421-29](article-r421-29.md)
- [Article R421-30](article-r421-30.md)
- [Article R421-31](article-r421-31.md)
- [Article R421-32](article-r421-32.md)
- [Article R421-33](article-r421-33.md)
- [Article R421-34](article-r421-34.md)
- [Article R421-35](article-r421-35.md)
