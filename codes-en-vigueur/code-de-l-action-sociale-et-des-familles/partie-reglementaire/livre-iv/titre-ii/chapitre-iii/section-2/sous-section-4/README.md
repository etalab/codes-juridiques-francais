# Sous-section 4 : Dispositions applicables aux seuls assistants maternels employés par des personnes morales de droit privé.

- [Article D423-20](article-d423-20.md)
