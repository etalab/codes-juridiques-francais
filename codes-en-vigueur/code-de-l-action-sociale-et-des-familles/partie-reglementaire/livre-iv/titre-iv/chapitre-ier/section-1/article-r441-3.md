# Article R441-3

La demande est adressée au président du conseil départemental du département de résidence du demandeur par lettre recommandée avec demande d'avis de réception.

Cette autorité dispose d'un délai de dix jours pour en accuser réception ou, si la demande est incomplète, pour indiquer, dans les conditions prévues par le décret du 6 juin 2001, les pièces manquantes dont la production est indispensable à l'instruction de la demande et fixer un délai pour la production de ces pièces.
