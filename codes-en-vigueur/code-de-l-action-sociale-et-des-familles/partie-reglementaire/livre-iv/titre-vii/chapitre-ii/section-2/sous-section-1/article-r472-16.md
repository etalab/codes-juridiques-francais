# Article R472-16

La déclaration est accompagnée :

1° Concernant l'agent de l'établissement désigné pour exercer l'activité de mandataire judiciaire à la protection des majeurs en qualité de préposé d'établissement hébergeant des majeurs, d'un acte de naissance, d'un extrait de casier judiciaire et du certificat national de compétence mentionné à l'article D. 471-4 ;

2° Du projet de notice d'information mentionnée à l'article L. 471-6 ;

3° D'une copie des conventions et de leurs avenants passés en application du dernier alinéa de l'article L. 472-5.
