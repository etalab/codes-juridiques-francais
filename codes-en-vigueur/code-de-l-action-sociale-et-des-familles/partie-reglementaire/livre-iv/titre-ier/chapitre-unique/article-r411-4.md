# Article R411-4

L'épreuve d'aptitude a pour objet de vérifier au moyen d'épreuves écrites et orales que l'intéressé a une connaissance appropriée des matières figurant au programme du titre de formation permettant l'exercice de la profession en France qui ne lui ont pas été enseignées ou qu'il n'a pas acquises par l'expérience professionnelle. Elle porte sur des matières à choisir parmi celles dont la connaissance est essentielle à l'exercice de la profession.
