# Article R411-3

Les ressortissants d'un Etat membre de la Communauté européenne d'un autre Etat partie à l'accord sur l'Espace économique européen, à une convention internationale ou un arrangement en matière de reconnaissance mutuelle des qualifications professionnelles non titulaires d'un diplôme d'Etat français d'assistant de service social qui souhaitent exercer en France la profession d'assistant de service social doivent obtenir une attestation de capacité à exercer délivrée par le ministre chargé des affaires sociales.

Conformément au septième alinéa de l'article L. 411-1, l'intéressé doit se soumettre préalablement à une mesure de compensation, après examen de ses qualifications professionnelles sous l'égide du préfet de région, lorsque son titre ou ensemble de titres de formation atteste d'une formation inférieure d'au moins un an à celle du diplôme d'Etat français ou comporte des différences importantes, en termes de durée ou de contenu, sur des matières dont la connaissance est essentielle à l'exercice des activités professionnelles d'assistant de service social, sauf s'il a acquis ces connaissances au cours de son expérience professionnelle sous réserve que cette expérience soit licite.

Sont considérées comme essentielles à l'exercice de la profession en France les connaissances correspondant à la formation requise pour l'exercice de la profession relatives aux théories et pratiques de l'intervention en service social y compris l'éthique, aux politiques sociales, à la législation et à la réglementation relatives à l'accès aux droits.

La vérification des qualifications professionnelles et les mesures de compensation sont organisées par le préfet de région.

En cas de doute sur les connaissances linguistiques nécessaires à l'exercice de la profession, le représentant de l'Etat dans la région vérifie le caractère suffisant de la maîtrise de la langue française.

La décision relative à la capacité à exercer est prise par le ministre chargé des affaires sociales. Elle est motivée s'il s'agit d'un refus ou si l'exercice est subordonné à une épreuve d'aptitude ou à un stage d'adaptation. Elle intervient au plus tard dans le délai de quatre mois à compter de la date de réception du dossier complet.

En cas de succès à l'épreuve d'aptitude ou de validation du stage d'adaptation, le ministre chargé des affaires sociales délivre l'attestation de capacité à exercer.

Sont fixées par arrêté du ministre chargé des affaires sociales :

1° Les modalités de présentation de la demande d'attestation de capacité à exercer, et notamment la composition du dossier accompagnant cette demande ;

2° Les conditions d'organisation et les modalités de notation de l'épreuve d'aptitude ainsi que la composition du jury chargé de l'évaluer ;

3° Les conditions de validation du stage d'adaptation.
