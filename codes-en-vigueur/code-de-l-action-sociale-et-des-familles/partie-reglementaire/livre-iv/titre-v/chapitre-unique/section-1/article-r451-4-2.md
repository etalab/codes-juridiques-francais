# Article R451-4-2

Le représentant de l'Etat dans la région contrôle, sur pièces ou sur place, pendant la durée de la formation, le respect des conditions énoncées aux articles R. 451-2 et R. 451-3.
