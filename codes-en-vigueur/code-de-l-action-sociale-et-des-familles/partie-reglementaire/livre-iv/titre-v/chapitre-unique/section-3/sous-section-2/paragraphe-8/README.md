# Paragraphe 8 : Diplôme d'Etat de moniteur éducateur.

- [Article D451-73](article-d451-73.md)
- [Article D451-74](article-d451-74.md)
- [Article D451-75](article-d451-75.md)
- [Article D451-76](article-d451-76.md)
- [Article D451-77](article-d451-77.md)
- [Article D451-78](article-d451-78.md)
