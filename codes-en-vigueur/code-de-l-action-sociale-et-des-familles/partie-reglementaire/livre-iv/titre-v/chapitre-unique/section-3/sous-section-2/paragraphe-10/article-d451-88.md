# Article D451-88

Le diplôme d'Etat d'auxiliaire de vie sociale atteste des compétences nécessaires pour effectuer un accompagnement social et un soutien auprès des personnes âgées, des personnes handicapées, des personnes en difficulté sociale, des familles ou des enfants, dans leur vie quotidienne.
