# Article D451-41

Le diplôme d'Etat d'éducateur spécialisé atteste des compétences nécessaires pour accompagner, dans une démarche éducative et sociale globale, des personnes, des groupes ou des familles en difficulté dans le développement de leurs capacités de socialisation, d'autonomie, d'intégration ou d'insertion.

Il est structuré en domaines de compétences et peut être obtenu, en tout ou partie, par la voie de l'examen à l'issue d'une formation ou par la validation des acquis de l'expérience.

Il est délivré par le recteur d'académie.
