# Article D451-89

Le diplôme d'Etat d'auxiliaire de vie sociale est structuré en domaines de compétences et peut être obtenu par la voie de la formation ou, en tout ou partie, par la validation des acquis de l'expérience.

Il est délivré par le représentant de l'Etat dans la région.
