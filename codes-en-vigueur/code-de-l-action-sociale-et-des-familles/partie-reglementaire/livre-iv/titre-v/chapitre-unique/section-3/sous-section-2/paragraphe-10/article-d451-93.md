# Article D451-93

Les titulaires du certificat d'aptitude aux fonctions d'aide à domicile ou de la mention complémentaire aide à domicile sont, de droit, titulaires du diplôme d'Etat d'auxiliaire de vie sociale.
