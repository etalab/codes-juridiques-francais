# Paragraphe 4 : Diplôme d'Etat aux fonctions d'éducateur technique spécialisé.

- [Article D451-52](article-d451-52.md)
- [Article D451-53](article-d451-53.md)
- [Article D451-54](article-d451-54.md)
- [Article D451-55](article-d451-55.md)
- [Article D451-56](article-d451-56.md)
