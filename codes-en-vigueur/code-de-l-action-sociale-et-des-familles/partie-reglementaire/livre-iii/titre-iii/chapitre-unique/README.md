# Chapitre unique

- [Section 1 : Conseil départemental de l'enfance.](section-1)
- [Section 2 : Surveillance des établissements.](section-2)
- [Section 3 : Administration provisoire et fermeture des établissements et services.](section-3)
