# Titre II : Etablissements soumis à déclaration

- [Chapitre Ier : Accueil de mineurs.](chapitre-ier)
- [Chapitre II : Accueil d'adultes.](chapitre-ii)
