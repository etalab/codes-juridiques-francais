# Article D313-15-4

Les dispositions de l'article R. 314-170 relatives au classement des résidents selon leur niveau de dépendance s'appliquent aux établissements mentionnés aux articles D. 313-15-1 et D. 313-15-2 selon les modalités suivantes :

1° Ce classement est réalisé par l'équipe médico-sociale mentionnée à l'article R. 232-7. Il est communiqué, à leur demande, au directeur général de l'agence régionale de santé et au président du conseil général.

2° Sa révision est opérée tous les ans.
