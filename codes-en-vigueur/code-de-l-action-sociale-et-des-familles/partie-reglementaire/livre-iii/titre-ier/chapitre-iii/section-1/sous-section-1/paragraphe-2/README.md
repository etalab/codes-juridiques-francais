# Paragraphe 2 :  Compétence et fonctionnement de la commission de sélection d'appel à projet social ou médico-social

- [Article D313-2](article-d313-2.md)
- [Article R313-2-1](article-r313-2-1.md)
- [Article R313-2-2](article-r313-2-2.md)
- [Article R313-2-3](article-r313-2-3.md)
- [Article R313-2-4](article-r313-2-4.md)
- [Article R313-2-5](article-r313-2-5.md)
