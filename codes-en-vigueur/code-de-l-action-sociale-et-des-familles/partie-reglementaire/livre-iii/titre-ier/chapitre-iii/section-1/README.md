# Section 1 : Procédure d'autorisation des établissements et services sociaux et médico-sociaux et des lieux de vie et d'accueil

- [Sous-section 1 : Projets de création, de transformation et d'extension d'établissements, services et lieux de vie et d'accueil requérant des financements publics](sous-section-1)
- [Sous-section 1 bis : Projets de création, de transformation et d'extension d'établissements et services ne requérant aucun financement public](sous-section-1-bis)
- [Sous-section 1 ter : Projets de création et d'extension d'établissements et services mentionnés au dernier alinéa de l'article L. 315-2](sous-section-1-ter)
- [Sous-section 1 quater : Dispositions particulières aux projets de création, de transformation et d'extension de services relevant des 14° et 15° du I de l'article L. 312-1](sous-section-1-quater)
- [Sous-section 1 quinquies : Procédure de demande de renouvellement d'autorisation](sous-section-1-quinquies)
- [Sous-section 2 : Contrôle de conformité des établissements.](sous-section-2)
