# Sous-section 1 bis : Projets de création, de transformation et d'extension d'établissements et services ne requérant aucun financement public

- [Article D313-8-2](article-d313-8-2.md)
- [Article D313-8-3](article-d313-8-3.md)
- [Article R313-8](article-r313-8.md)
- [Article R313-8-1](article-r313-8-1.md)
