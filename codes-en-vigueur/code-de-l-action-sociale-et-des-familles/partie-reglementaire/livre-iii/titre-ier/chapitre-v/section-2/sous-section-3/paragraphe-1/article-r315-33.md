# Article R315-33

Sont électeurs les fonctionnaires titulaires et stagiaires ainsi que les agents contractuels mentionnés au premier alinéa de l'article 1er du décret n° 91-155 du 6 février 1991 relatif aux dispositions générales applicables aux agents contractuels des établissements mentionnés à l'article 2 de la loi n° 86-33 du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière, les contractuels de droit public n'occupant pas un emploi permanent et les contractuels de droit privé.

Toutefois, les fonctionnaires appartenant à un corps de catégorie A géré et recruté au niveau national en application de l'avant-dernier alinéa de l'article 4 de la loi n° 86-33 du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière n'ont pas la qualité d'électeur.
