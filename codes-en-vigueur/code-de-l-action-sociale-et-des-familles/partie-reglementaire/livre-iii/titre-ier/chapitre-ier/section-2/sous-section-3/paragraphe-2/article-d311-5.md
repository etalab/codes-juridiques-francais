# Article D311-5

Le conseil de la vie sociale comprend au moins :

1° Deux représentants des personnes accueillies ou prises en charge ;

2° S'il y a lieu, un représentant des familles ou des représentants légaux ;

3° Un représentant du personnel ;

4° Un représentant de l'organisme gestionnaire.

Le nombre des représentants des personnes accueillies, d'une part, et de leur famille ou de leurs représentants légaux, d'autre part, doit être supérieur à la moitié du nombre total des membres du conseil.
