# Article D311-10

Sous réserve des dispositions de l'article D. 311-30, les représentants des personnes accueillies et les représentants des familles ou des représentants légaux sont élus par vote à bulletin secret à la majorité des votants respectivement par l'ensemble des personnes accueillies ou prises en charge et par l'ensemble des familles ou des représentants légaux, au sens du 2° de l'article D. 311-11. Des suppléants sont élus dans les mêmes conditions.

Sont élus le ou les candidats ayant obtenu le plus grand nombre de voix. A égalité de voix, il est procédé par tirage au sort entre les intéressés.

Dans les établissements et services relevant des 8°, 9° et 13° de l'article L. 312-1, les représentants des personnes accueillies peuvent être désignés avec leur accord sans qu'il y ait lieu de procéder à des élections. Les modalités de désignation sont précisées par le règlement de fonctionnement.
