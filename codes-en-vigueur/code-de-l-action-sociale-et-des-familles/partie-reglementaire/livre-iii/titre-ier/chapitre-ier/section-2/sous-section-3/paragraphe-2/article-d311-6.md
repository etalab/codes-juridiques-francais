# Article D311-6

L'absence de désignation de titulaires et suppléants ne fait pas obstacle à la mise en place du conseil sous réserve que le nombre de représentants des personnes accueillies et de leurs familles ou de leurs représentants légaux soit supérieur à la moitié du nombre total des membres du conseil désignés.
