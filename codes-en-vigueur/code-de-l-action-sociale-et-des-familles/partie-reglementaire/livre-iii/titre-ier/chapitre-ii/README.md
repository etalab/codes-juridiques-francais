# Chapitre II : Organisation de l'action sociale et médico-sociale

- [Section 1 : Etablissements et services sociaux et médico-sociaux](section-1)
- [Section 2 : Organismes consultatifs](section-2)
- [Section 3 : Schémas d'organisation sociale et médico-sociale](section-3)
- [Section 4 : Coordination des interventions](section-4)
- [Section 5 : Evaluation et systèmes d'information](section-5)
