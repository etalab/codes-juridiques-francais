# Paragraphe 2 : Evaluation des activités et de la qualité des prestations des établissements et services sociaux et médico-sociaux

- [Article D312-197](article-d312-197.md)
- [Article D312-198](article-d312-198.md)
- [Article D312-199](article-d312-199.md)
- [Article D312-200](article-d312-200.md)
- [Article D312-201](article-d312-201.md)
- [Article D312-202](article-d312-202.md)
- [Article D312-203](article-d312-203.md)
- [Article D312-204](article-d312-204.md)
- [Article D312-205](article-d312-205.md)
- [Article D312-206](article-d312-206.md)
