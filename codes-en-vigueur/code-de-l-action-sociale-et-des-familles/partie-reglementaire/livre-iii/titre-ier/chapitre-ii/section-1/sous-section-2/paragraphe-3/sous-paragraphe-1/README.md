# Sous-paragraphe 1 : Dispositions générales.

- [Article D312-83](article-d312-83.md)
- [Article D312-84](article-d312-84.md)
- [Article D312-85](article-d312-85.md)
