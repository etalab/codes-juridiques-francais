# Sous-paragraphe 5 : Fonctionnement de l'établissement.

- [Article D312-34](article-d312-34.md)
- [Article D312-35](article-d312-35.md)
- [Article D312-36](article-d312-36.md)
- [Article D312-37](article-d312-37.md)
- [Article D312-38](article-d312-38.md)
- [Article D312-39](article-d312-39.md)
- [Article D312-40](article-d312-40.md)
