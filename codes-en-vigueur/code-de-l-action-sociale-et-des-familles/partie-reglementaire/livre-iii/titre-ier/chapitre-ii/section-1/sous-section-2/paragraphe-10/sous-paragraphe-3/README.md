# Sous-paragraphe 3 : Dispositions communes

- [Article D312-170](article-d312-170.md)
- [Article D312-171](article-d312-171.md)
- [Article D312-172](article-d312-172.md)
- [Article D312-173](article-d312-173.md)
- [Article D312-174](article-d312-174.md)
- [Article D312-175](article-d312-175.md)
- [Article D312-176](article-d312-176.md)
