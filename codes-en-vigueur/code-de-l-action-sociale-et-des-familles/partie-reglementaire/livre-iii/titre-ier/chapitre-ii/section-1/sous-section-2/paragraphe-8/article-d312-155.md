# Article D312-155

Pour assurer leurs missions, les gestionnaires des appartements de coordination thérapeutique ont recours à une équipe pluridisciplinaire. Celle-ci comprend au moins un médecin exerçant le cas échéant à temps partiel.
