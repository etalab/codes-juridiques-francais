# Paragraphe 2 : Etablissements et services prenant en charge des enfants ou adolescents présentant une déficience motrice

- [Sous-paragraphe 1 : Dispositions générales.](sous-paragraphe-1)
- [Sous-paragraphe 2 : Organisation de l'établissement ou du service.](sous-paragraphe-2)
- [Sous-paragraphe 3 : Personnels.](sous-paragraphe-3)
- [Sous-paragraphe 4 : Installation.](sous-paragraphe-4)
- [Sous-paragraphe 5 : Service d'éducation spéciale et de soins à domicile.](sous-paragraphe-5)
- [Sous-paragraphe 6 : Dispositions diverses.](sous-paragraphe-6)
