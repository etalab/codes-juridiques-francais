# Sous-paragraphe 1 : Dispositions générales.

- [Article D312-60](article-d312-60.md)
- [Article D312-61](article-d312-61.md)
- [Article D312-62](article-d312-62.md)
- [Article D312-63](article-d312-63.md)
