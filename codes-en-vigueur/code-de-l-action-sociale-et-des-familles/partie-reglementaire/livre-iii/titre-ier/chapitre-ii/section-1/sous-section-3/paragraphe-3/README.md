# Paragraphe 3 : Dispositions communes

- [Article D312-176-11](article-d312-176-11.md)
- [Article D312-176-12](article-d312-176-12.md)
- [Article D312-176-13](article-d312-176-13.md)
