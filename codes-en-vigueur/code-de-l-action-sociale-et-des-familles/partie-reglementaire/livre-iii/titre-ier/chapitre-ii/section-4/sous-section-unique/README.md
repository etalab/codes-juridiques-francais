# Sous-section unique : Groupements

- [Paragraphe 1 : Dispositions générales](paragraphe-1)
- [Paragraphe 2 : Missions](paragraphe-2)
- [Paragraphe 3 : Constitution](paragraphe-3)
- [Paragraphe 4 : Organisation et administration](paragraphe-4)
- [Paragraphe 5 : Dissolution et liquidation](paragraphe-5)
