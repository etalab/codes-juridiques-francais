# Chapitre IV : Dispositions financières

- [Section 1 : Règles de compétences en matière tarifaire.](section-1)
- [Section 2 : Règles budgétaires de financement](section-2)
- [Section 3 : Dispositions diverses](section-3)
- [Article R314-1](article-r314-1.md)
- [Article R314-2](article-r314-2.md)
