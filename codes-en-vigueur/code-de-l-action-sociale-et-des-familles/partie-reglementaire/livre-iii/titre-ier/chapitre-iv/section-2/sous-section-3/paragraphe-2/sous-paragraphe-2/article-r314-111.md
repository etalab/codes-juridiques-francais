# Article R314-111

Les dotations globales ou les forfaits globaux de soins relevant de l'assurance maladie sont versés :

1° Pour les dotations globales afférentes aux soins dispensés dans les centres de soins, d'accompagnement et de prévention en addictologie mentionnés à l'article L. 314-8, dans les conditions prévues par les articles R. 174-7 et R. 174-8 du code de la sécurité sociale ;

2° Pour dotations globales afférentes aux soins dispensés dans les établissements hébergeant des personnes âgées dépendantes mentionnés au I de l'article L. 313-12, dans les conditions prévues par les articles R. 174-9 à R. 174-16 du code de la sécurité sociale ;

3° Pour les dotations globales ou les forfaits globaux de soins versés aux autres établissements ou services relevant du I de l'article L. 312-1, dans les conditions prévues par les articles R. 174-16-1 à R. 174-16-5 du code de la sécurité sociale.
