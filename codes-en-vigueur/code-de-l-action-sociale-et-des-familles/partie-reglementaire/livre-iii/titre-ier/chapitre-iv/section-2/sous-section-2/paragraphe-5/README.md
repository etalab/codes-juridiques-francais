# Paragraphe 5 : Règles applicables aux établissements et services gérés par des organismes à but lucratif ou non habilités à recevoir des bénéficiaires de l'aide sociale.

- [Article R314-101](article-r314-101.md)
- [Article R314-102](article-r314-102.md)
- [Article R314-103](article-r314-103.md)
- [Article R314-104](article-r314-104.md)
