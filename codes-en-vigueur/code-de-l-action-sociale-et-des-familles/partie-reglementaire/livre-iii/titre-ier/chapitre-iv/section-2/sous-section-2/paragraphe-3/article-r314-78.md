# Article R314-78

Les activités sociales et médico-sociales relevant du I de l'article L. 312-1 qui sont gérées par une collectivité territoriale ou un centre communal ou intercommunal d'action sociale sont retracées dans un budget annexe de cette collectivité ou de cet établissement.

Les règles budgétaires et tarifaires propres aux établissements publics sociaux et médico-sociaux, fixées au paragraphe 1 de la sous-section 2 de la présente section, sont applicables à ce budget annexe.

Il en va de même des activité sociales et médico-sociales relevant du I de l'article L. 312-1 qui sont gérées par un établissement public national ou local, sans constituer son activité principale.
