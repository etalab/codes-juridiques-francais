# Sous-paragraphe 3 : Exécution du budget.

- [Article R314-68](article-r314-68.md)
- [Article R314-69](article-r314-69.md)
- [Article R314-72](article-r314-72.md)
- [Article R314-73](article-r314-73.md)
- [Article R314-74](article-r314-74.md)
