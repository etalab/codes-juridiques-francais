# Article R314-190

Pour les établissements régis par les articles L. 342-1 à L. 342-6 :

1° Les dispositions du 1° de l'article R. 314-158, de l'article R. 314-180 et du 1° de l'article R. 314-181 ne sont pas applicables ;

2° Les modalités de tarification afférentes à la dépendance définies au sous-paragraphe 2 du présent paragraphe ne sont applicables qu'aux contrats mentionnés à l'article L. 342-1, conclus postérieurement à la date de signature de la convention prévue à l'article L. 313-12.
