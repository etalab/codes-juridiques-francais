# Article R314-170-2

<div align="left">Le groupe iso-ressources moyen pondéré (GMP) de l'établissement pris en compte pour la détermination annuelle de la dotation globale ou du forfait global relatif à la dépendance et de la dotation globale ou du forfait global relatif aux soins est celui résultant de l'évaluation prévue au deuxième alinéa de l'article R. 314-170 ou de sa révision ultérieure telle que prévue au troisième alinéa du même article.<br/>
</div>
