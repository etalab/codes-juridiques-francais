# Paragraphe 7 : Foyers d'accueil médicalisés et services d'accompagnement médico-social pour personnes adultes handicapées.

- [Article R314-140](article-r314-140.md)
- [Article R314-141](article-r314-141.md)
- [Article R314-142](article-r314-142.md)
- [Article R314-143](article-r314-143.md)
- [Article R314-144](article-r314-144.md)
- [Article R314-145](article-r314-145.md)
- [Article R314-146](article-r314-146.md)
