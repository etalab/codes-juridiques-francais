# Article R314-183

Pour les établissements relevant du 2° de l'article L. 342-1, le tarif journalier afférent à l'hébergement des personnes qui sont bénéficiaires de l'aide sociale est arrêté par le président du conseil départemental du lieu d'implantation de l'établissement dans le cadre d'une convention d'aide sociale et dans les conditions prévues à l'article L. 342-3-1.
