# Article R314-170

L'évaluation de la perte d'autonomie des personnes hébergées dans l'établissement et l'évaluation de leurs besoins en soins sont réalisées par l'établissement, sous la responsabilité du médecin coordonnateur.

Ces évaluations sont réalisées lors de la conclusion ou du renouvellement de la convention pluriannuelle mentionnée au I de l'article L. 313-12 ou du contrat pluriannuel mentionné à l'article L. 313-11.

Elles sont renouvelées une fois et de façon simultanée en cours de convention ou de contrat. Elles sont utilisées pour le calcul de la dotation globale ou du forfait global relatif à la dépendance et de la dotation globale ou du forfait global relatif aux soins à compter de l'exercice budgétaire de l'année de leur réalisation.
