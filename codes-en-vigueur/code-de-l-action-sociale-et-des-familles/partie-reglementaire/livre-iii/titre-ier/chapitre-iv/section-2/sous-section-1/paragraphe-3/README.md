# Paragraphe 3 : Fixation du tarif

- [Sous-paragraphe 1 : Etablissement des propositions budgétaires.](sous-paragraphe-1)
- [Sous-paragraphe 2 : Transmission des propositions budgétaires et procédure contradictoire.](sous-paragraphe-2)
- [Sous-paragraphe 3 : Dépenses pouvant être prises en charge](sous-paragraphe-3)
- [Sous-paragraphe 4 : Tableaux de bord.](sous-paragraphe-4)
- [Sous-paragraphe 5 : Décision d'autorisation budgétaire et de tarification.](sous-paragraphe-5)
- [Sous-paragraphe 6 : Fixation pluriannuelle du budget.](sous-paragraphe-6)
