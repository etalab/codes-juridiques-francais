# Article R314-40

Les éléments pluriannuels du budget sont fixés dans le cadre, soit du contrat pluriannuel prévu par l'article L. 313-11, soit de la convention pluriannuelle mentionnée au I de l'article L. 313-12.

Le contrat ou la convention comportent alors un volet financier qui fixe, par groupes fonctionnels ou par section tarifaire selon la catégorie d'établissement ou de service, et pour la durée de la convention, les modalités de fixation annuelle de la tarification.

Ces modalités peuvent consister :

1° Soit en l'application directe à l'établissement ou au service du taux d'évolution des dotations régionales limitatives mentionnées aux articles L. 314-3 et L. 314-4 ;

2° Soit en l'application d'une formule fixe d'actualisation ou de revalorisation ;

3° Soit en la conclusion d'avenants annuels d'actualisation ou de revalorisation.
