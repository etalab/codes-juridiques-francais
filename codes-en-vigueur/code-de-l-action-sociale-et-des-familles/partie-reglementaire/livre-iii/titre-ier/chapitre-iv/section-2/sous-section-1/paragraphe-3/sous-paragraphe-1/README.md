# Sous-paragraphe 1 : Etablissement des propositions budgétaires.

- [Article R314-14](article-r314-14.md)
- [Article R314-15](article-r314-15.md)
- [Article R314-16](article-r314-16.md)
- [Article R314-17](article-r314-17.md)
- [Article R314-18](article-r314-18.md)
- [Article R314-19](article-r314-19.md)
- [Article R314-20](article-r314-20.md)
