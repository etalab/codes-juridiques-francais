# Chapitre V : Centres d'hébergement et de réinsertion sociale

- [Section 1 : Activités et organisation](section-1)
- [Section 2 : Accueil et séjour](section-2)
- [Section 3 : Dispositif de veille sociale](section-3)
