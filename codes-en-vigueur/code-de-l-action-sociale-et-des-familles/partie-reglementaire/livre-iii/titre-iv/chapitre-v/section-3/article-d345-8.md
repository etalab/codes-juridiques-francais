# Article D345-8

Pour permettre l'accomplissement des missions définies à l'article L. 345-2, le dispositif de veille sociale comprend un service d'appels téléphoniques pour les sans-abri dénommé " 115 ". En outre, il comprend selon les besoins du département, identifiés par le préfet :

1° Un ou des accueils de jour ;

2° Une ou des équipes mobiles chargées d'aller au contact des personnes sans abri ;

3° Un ou des services d'accueil et d'orientation (SAO).

Ces services fonctionnent de manière coordonnée sous l'autorité du préfet du département, dans le cadre de conventions qui précisent l'activité de chaque service, son mode de financement et les indicateurs d'évaluation de son action.
