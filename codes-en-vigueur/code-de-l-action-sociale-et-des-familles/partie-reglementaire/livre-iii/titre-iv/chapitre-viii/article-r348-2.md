# Article R348-2

Pour l'application du I de l'article L. 348-3, l'autorité administrative compétente de l'Etat est le préfet du département du lieu d'implantation du centre d'accueil pour demandeurs d'asile, compétent pour l'admission à l'aide sociale.

La décision d'admission dans un centre d'accueil pour demandeurs d'asile est prise par le gestionnaire de ce centre.

Si ce centre est situé dans le département dans lequel le demandeur d'asile a été admis au séjour, et a été mentionné par le préfet au titre de l'information fournie en vertu du deuxième alinéa de l'article R. 348-1, l'accord du préfet sur l'admission envisagée par le gestionnaire est réputé acquis.

Dans toute autre hypothèse, l'admission doit recueillir l'accord du préfet mentionné au premier alinéa du présent article. A cette fin, le gestionnaire du centre saisit le préfet sans délai. L'accord du préfet est réputé acquis lorsque le préfet n'a pas fait connaître au gestionnaire sa réponse dans un délai de quinze jours à compter de la saisine.
