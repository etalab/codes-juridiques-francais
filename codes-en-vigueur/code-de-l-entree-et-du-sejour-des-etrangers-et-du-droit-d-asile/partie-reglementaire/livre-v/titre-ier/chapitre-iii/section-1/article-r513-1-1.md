# Article R513-1-1

L'autorité administrative compétente pour prononcer la décision fixant le pays de renvoi dans le cas prévu au deuxième alinéa de l'article L. 214-4 est le ministre de l'intérieur.
