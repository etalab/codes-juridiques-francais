# Article R561-1-1

L'autorité administrative compétente pour assigner un étranger à résidence en application du 6° de l'article L. 561-1 est le ministre de l'intérieur.
