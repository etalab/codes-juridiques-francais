# Chapitre Ier : Placement en rétention.

- [Article R551-1](article-r551-1.md)
- [Article R551-2](article-r551-2.md)
- [Article R551-3](article-r551-3.md)
- [Article R551-4](article-r551-4.md)
