# Section 1 : Première saisine du juge des libertés et de la détention.

- [Article R552-1](article-r552-1.md)
- [Article R552-2](article-r552-2.md)
- [Article R552-3](article-r552-3.md)
- [Article R552-4](article-r552-4.md)
- [Article R552-5](article-r552-5.md)
- [Article R552-6](article-r552-6.md)
- [Article R552-7](article-r552-7.md)
- [Article R552-8](article-r552-8.md)
- [Article R552-9](article-r552-9.md)
- [Article R552-10](article-r552-10.md)
