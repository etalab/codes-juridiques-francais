# Article R552-2

Le juge des libertés et de la détention est saisi par simple requête de l'autorité administrative qui a ordonné le placement en rétention.
