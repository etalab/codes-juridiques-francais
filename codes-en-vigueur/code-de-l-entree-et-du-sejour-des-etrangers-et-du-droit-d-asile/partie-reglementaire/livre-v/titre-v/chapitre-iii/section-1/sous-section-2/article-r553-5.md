# Article R553-5

Les locaux mentionnés à l'article R. 551-3 sont créés, à titre permanent ou pour une durée déterminée, par arrêté préfectoral. Une copie de cet arrêté est transmise sans délai au procureur de la République, au directeur départemental des affaires sanitaires et sociales et au Contrôleur général des lieux de privation de liberté.
