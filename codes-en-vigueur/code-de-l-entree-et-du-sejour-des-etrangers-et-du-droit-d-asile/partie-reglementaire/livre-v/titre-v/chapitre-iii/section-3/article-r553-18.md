# Article R553-18

Les dispositions de la présente section ne sont pas applicables à l'étranger dont la demande d'asile relève du 1° de l'article L. 741-4 et qui est placé en rétention en vue de l'exécution d'une décision de remise en application de l'article L. 531-2.

L'étranger en est informé dans une langue dont il est raisonnable de penser qu'il la comprend.
