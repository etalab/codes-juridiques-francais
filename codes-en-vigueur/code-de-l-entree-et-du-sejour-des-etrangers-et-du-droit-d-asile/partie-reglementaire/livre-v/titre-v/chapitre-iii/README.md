# Chapitre III : Conditions de la rétention

- [Section 1 : Lieux de rétention](section-1)
- [Section 2 : Droits des étrangers retenus.](section-2)
- [Section 2 bis : Intervention des personnes morales](section-2-bis)
- [Section 2 ter : Accès des associations humanitaires aux lieux de rétention](section-2-ter)
- [Section 3 : Demandes d'asile formulées par des étrangers retenus.](section-3)
