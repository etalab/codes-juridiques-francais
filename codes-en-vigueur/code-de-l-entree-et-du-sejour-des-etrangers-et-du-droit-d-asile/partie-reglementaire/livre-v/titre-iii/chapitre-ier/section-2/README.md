# Section 2 : Etrangers ayant fait l'objet d'une mesure d'éloignement prise par un Etat membre de l'Union européenne.

- [Article R531-5](article-r531-5.md)
- [Article R531-6](article-r531-6.md)
- [Article R531-7](article-r531-7.md)
- [Article R531-8](article-r531-8.md)
- [Article R531-9](article-r531-9.md)
