# Section 1 : Autorités administratives compétentes.

- [Article R531-1](article-r531-1.md)
- [Article R*531-2](article-r-531-2.md)
- [Article R531-3](article-r531-3.md)
- [Article R531-3-1](article-r531-3-1.md)
- [Article R531-3-2](article-r531-3-2.md)
- [Article R531-3-3](article-r531-3-3.md)
- [Article R531-4](article-r531-4.md)
