# Article R522-8-1

Si, à l'issue du délai fixé au quatrième alinéa de l'article L. 522-2, éventuellement prolongé dans les conditions prévues à cet article, la commission n'a pas émis son avis, le préfet informe l'étranger que les formalités de consultation de la commission sont réputées remplies.
