# Chapitre IV : Abrogation des arrêtés d'expulsion.

- [Article R524-1](article-r524-1.md)
- [Article R524-2](article-r524-2.md)
