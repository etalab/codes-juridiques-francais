# Article R523-4

L'autorité administrative compétente pour prononcer par arrêté, sur le fondement des articles L. 523-3 à L. 523-5, l'assignation à résidence d'un étranger faisant l'objet d'une mesure d'expulsion est le préfet du département où se situe le lieu d'assignation à résidence, à Paris, le préfet de police, quand la mesure d'expulsion est prise sur le fondement de l'article L. 521-1 après accomplissement des formalités prévues à l'article L. 522-1.
