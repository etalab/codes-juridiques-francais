# Chapitre III : Exécution des arrêtés d'expulsion

- [Section 1 : Décision fixant le pays de renvoi.](section-1)
- [Section 2 : Assignation à résidence.](section-2)
