# Chapitre V : Méconnaissance des obligations incombant aux entreprises de transport

- [Section 1 : Procédure.](section-1)
- [Section 2 : Dispositif agréé de numérisation et de transmission par les entreprises de transport des documents de voyage et des visas.](section-2)
- [Section 3 : Consignation d'une somme par l'entreprise de transport.](section-3)
