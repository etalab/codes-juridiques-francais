# Article R625-4

La procédure prévue par les articles R. 625-1 et R. 625-3 est applicable aux entreprises de transport routier mentionnées à l'article L. 625-6.

Les services compétents pour procéder aux contrôles mentionnés au deuxième alinéa de l'article L. 625-6 sont les services de la police nationale ou, en l'absence de tels services, les services des douanes ou les unités de la gendarmerie nationale situés à l'entrée du territoire français.
