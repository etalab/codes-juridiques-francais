# Article R611-2

Le traitement automatisé de données à caractère personnel mentionné à l'article R. 611-1 comporte les images numérisées de la photographie et des empreintes digitales des dix doigts des étrangers suivants :

1° Etrangers demandeurs ou titulaires d'un titre de séjour, d'un titre de voyage d'une durée de validité supérieure à un an ou de la carte de frontalier mentionnée à l'annexe 6-4 ;

2° Etrangers en situation irrégulière ;

3° Etrangers faisant l'objet d'une mesure d'éloignement.

L'impossibilité de collecte totale ou partielle des empreintes digitales est mentionnée dans le traitement.

Le traitement ne comporte pas de dispositif de reconnaissance faciale à partir de l'image numérisée de la photographie.
