# Article D331-1

Une aide dénommée "aide publique à la réinsertion" peut être accordée, sur leur demande et dans la limite des crédits disponibles, aux travailleurs étrangers majeurs de dix-huit ans qui quittent la France pour regagner leur pays d'origine.
