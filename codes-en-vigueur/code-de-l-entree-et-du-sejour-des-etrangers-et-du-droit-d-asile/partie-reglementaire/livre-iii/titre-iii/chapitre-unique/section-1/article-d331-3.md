# Article D331-3

Pour bénéficier de l'aide publique à la réinsertion, les personnes répondant aux conditions fixées aux articles D. 331-1 et D. 331-2 doivent avoir exercé en France une activité professionnelle salariée à caractère permanent, en vertu d'un titre en cours de validité et non en raison d'un régime de libre circulation ou d'assimilation au national.

Ces personnes ne peuvent prétendre à cette aide si elles ont qualité soit pour obtenir de plein droit une autorisation de travail en raison de leur situation personnelle, soit pour obtenir à nouveau la délivrance d'un titre de séjour au titre du regroupement familial.
