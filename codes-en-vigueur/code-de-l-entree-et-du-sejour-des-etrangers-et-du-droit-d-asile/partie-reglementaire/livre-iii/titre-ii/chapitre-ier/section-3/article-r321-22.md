# Article R321-22

Tout titre de voyage délivré pour une durée supérieure à un an intègre les éléments de sécurité et les éléments biométriques prévus par le règlement (CE) n° 2252/2004 du Conseil du 13 décembre 2004 établissant des normes pour les éléments de sécurité et les éléments biométriques intégrés dans les passeports et les documents de voyage délivrés par les Etats membres et son annexe, modifié par le règlement (CE) n° 444/2009 du Parlement européen et du Conseil du 28 mai 2009.

Il comporte, outre les mentions énumérées au B de la section 2 de l'annexe 6-4 du présent code, un composant électronique contenant les données à caractère personnel énumérées au B de la section 3 de la même annexe.
