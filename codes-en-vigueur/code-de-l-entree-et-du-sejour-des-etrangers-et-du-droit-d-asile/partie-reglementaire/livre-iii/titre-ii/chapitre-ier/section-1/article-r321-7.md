# Article R321-7

L'abrogation du visa mentionnée à l'article R. 321-6 est décidée par le préfet du département où séjourne l'étranger qui en est titulaire ou du département où la situation de cet étranger est contrôlée. Le préfet qui a prononcé l'abrogation en avertit sans délai le ministre des affaires étrangères.
