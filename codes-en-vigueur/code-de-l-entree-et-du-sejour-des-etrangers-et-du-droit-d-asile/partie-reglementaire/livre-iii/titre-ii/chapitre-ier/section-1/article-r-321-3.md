# Article R*321-3

L'autorité administrative compétente pour prendre la décision mentionnée au 1° de l'article R. 321-2 est le ministre de l'intérieur.
