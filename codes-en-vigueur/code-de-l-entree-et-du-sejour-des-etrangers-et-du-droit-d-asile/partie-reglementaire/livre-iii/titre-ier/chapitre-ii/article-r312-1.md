# Article R312-1

Le préfet ou, à Paris, le préfet de police met en place la commission du titre de séjour mentionnée à l'article L. 312-1 par un arrêté :

1° Constatant la désignation des élus locaux mentionnés au a du même article ;

2° Désignant les personnalités qualifiées mentionnées au b du même article ;

3° Désignant le président de la commission.
