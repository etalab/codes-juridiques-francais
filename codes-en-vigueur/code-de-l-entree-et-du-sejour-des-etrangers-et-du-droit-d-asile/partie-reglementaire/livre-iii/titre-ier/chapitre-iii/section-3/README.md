# Section 3 : Renouvellement des cartes de séjour temporaires.

- [Article R313-35](article-r313-35.md)
- [Article R313-36](article-r313-36.md)
- [Article R313-36-1](article-r313-36-1.md)
- [Article R313-37](article-r313-37.md)
- [Article R313-38](article-r313-38.md)
