# Article R313-2

Ne sont pas soumis aux dispositions du 2° de l'article R. 313-1 les étrangers mentionnés à l'article L. 313-4-1, aux 2°, 2° bis, 6° à 11° de l'article L. 313-11, et aux articles L. 313-11-1, L. 313-13, L. 313-14, L. 313-15 et L. 316-1.
