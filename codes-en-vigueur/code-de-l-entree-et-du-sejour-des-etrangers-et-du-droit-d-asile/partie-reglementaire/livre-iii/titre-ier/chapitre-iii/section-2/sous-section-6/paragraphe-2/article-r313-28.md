# Article R313-28

L'étranger convoqué devant la commission médicale régionale en est avisé par une lettre précisant la date, l'heure et le lieu de la séance de la commission lors de laquelle il sera entendu, au moins quinze jours avant cette date.

L'étranger est assisté, le cas échéant, par un interprète et peut demander à se faire assister par un médecin. Lorsque l'étranger est mineur, il est accompagné de son représentant légal.

Si l'étranger ne se présente pas devant la commission médicale régionale, celle-ci peut néanmoins délibérer et rendre un avis.
