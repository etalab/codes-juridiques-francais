# Article R313-16-2

Lorsque l'étranger présente un projet tendant à la création d'une activité ou d'une entreprise, l'autorité diplomatique ou consulaire ou le préfet compétent saisit pour avis le   directeur départemental ou, le cas échéant, régional des finances publiques du département dans lequel l'étranger souhaite réaliser son projet.
