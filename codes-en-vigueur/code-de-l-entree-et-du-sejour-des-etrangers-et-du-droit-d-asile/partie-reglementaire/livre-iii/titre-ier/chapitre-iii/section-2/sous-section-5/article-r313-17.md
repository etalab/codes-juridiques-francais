# Article R313-17

Pour l'application du 3° de l'article L. 313-10, l'étranger qui vient en France pour y exercer une activité professionnelle non soumise à l'autorisation prévue à l'article L. 341-2 du code du travail présente, outre les pièces prévues à l'article R. 313-1, celles justifiant qu'il dispose de ressources d'un niveau au moins équivalent au salaire minimum de croissance correspondant à un emploi à temps plein.

Dans les cas où il envisage d'exercer une activité réglementée, il justifie satisfaire aux conditions d'accès à l'activité en cause.
