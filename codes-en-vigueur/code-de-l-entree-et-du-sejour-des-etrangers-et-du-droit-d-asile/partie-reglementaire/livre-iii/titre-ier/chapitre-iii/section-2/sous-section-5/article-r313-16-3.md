# Article R313-16-3

Lors de la demande de délivrance ou de renouvellement de la carte de séjour prévue au 2° de l'article L. 313-10, le préfet vérifie la compatibilité de l'activité en cause avec la sécurité, la salubrité et la tranquillité publiques ainsi que, le cas échéant, l'absence de condamnation ou de décision emportant en France, l'interdiction d'exercer une activité commerciale.
