# Article R313-15

Pour l'application du 1° de l'article L. 313-10, l'étranger qui demande la carte de séjour mention " salarié " présente, outre les pièces prévues à l'article R. 313-1 à l'exception du certificat médical prévu au 4° de cet article, un contrat de travail conclu pour une durée égale ou supérieure à douze mois avec un employeur établi en France. Ce contrat est conforme au modèle fixé par arrêté du ministre chargé du travail et est revêtu du visa de ses services.

L'étranger qui sollicite la délivrance de la carte de séjour mention " travailleur temporaire " présente un contrat de travail conclu pour une durée inférieure à douze mois.

Ces cartes autorisent l'exercice d'une activité professionnelle dans les conditions définies aux articles R. 341-2-1, R. 341-2-2 et R. 341-2-4 du code du travail.
