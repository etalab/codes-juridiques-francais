# Article R313-34-1

L'étranger titulaire de la carte de résident de longue durée-UE dans un autre Etat membre de l'Union européenne qui sollicite la délivrance d'une carte de séjour temporaire en application de l'article L. 313-4-1 doit présenter les pièces suivantes :

1° La carte de résident de longue durée-UE en cours de validité délivrée par l'Etat membre de l'Union européenne qui lui a accordé ce statut sur son territoire ;

2° La justification qu'il dispose de ressources propres, stables et régulières, suffisant à son entretien et, le cas échéant, à celui de son conjoint et de ses enfants mentionnés aux I et II de l'article L. 313-11-1, indépendamment des prestations familiales et des allocations mentionnées au septième alinéa de l'article L. 313-4-1 ; les ressources mensuelles du demandeur et, le cas échéant, de son conjoint doivent atteindre un montant total au moins égal au salaire minimum de croissance apprécié à la date du dépôt de la demande ; lorsque le niveau des ressources du demandeur n'atteint pas cette somme, une décision favorable peut être prise s'il justifie être propriétaire de son logement ou en jouir à titre gratuit ;

3° La justification qu'il dispose d'un logement approprié, qui peut notamment être apportée par tout document attestant sa qualité de propriétaire ou de locataire du logement ;

4° La justification qu'il bénéficie d'une assurance maladie ;

5° Les pièces exigées pour la délivrance de l'une des cartes de séjour temporaires prévues à l'article L. 313-4-1 selon le motif du séjour invoqué.
