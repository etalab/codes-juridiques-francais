# Article R317-3

L'étranger présente à l'appui de sa demande de renouvellement de la carte de séjour prévue à l'article L. 317-1 :

1° Le document d'identité et de voyage dont il est titulaire et, le cas échéant, celui de son conjoint ;

2° Une attestation sur l'honneur selon laquelle chacun des séjours effectués en France sous le couvert de cette carte n'a pas excédé une année ;

3° La carte de séjour mention "retraité" dont il est titulaire et qui vient à expiration ;

4° Trois photographies de face, tête nue, de format 3,5 cm x 4,5 cm récentes et parfaitement ressemblantes.
