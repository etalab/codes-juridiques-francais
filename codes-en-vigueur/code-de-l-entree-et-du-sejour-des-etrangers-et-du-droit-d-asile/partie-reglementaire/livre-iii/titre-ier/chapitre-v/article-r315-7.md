# Article R315-7

Les autorités diplomatiques et consulaires autorisent la délivrance de la carte " compétences et talents " à l'étranger résidant hors de France et lui délivrent un visa de long séjour portant la mention " compétences et talents ". Le préfet du département où l'étranger établit sa résidence en France ou, à Paris, le préfet de police, remet à l'intéressé la carte de séjour prévue à l'article L. 315-1. Cette carte de séjour est délivrée à l'étranger qui réside en France par le préfet ou, à Paris, par le préfet de police.

L'attribution de cette carte vaut autorisation de travail à compter de sa notification.
