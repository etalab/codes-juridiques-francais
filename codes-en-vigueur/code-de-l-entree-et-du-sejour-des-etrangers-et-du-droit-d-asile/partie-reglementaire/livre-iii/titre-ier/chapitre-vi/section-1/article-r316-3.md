# Article R316-3

Une carte de séjour temporaire portant la mention "vie privée et familiale" d'une durée minimale de six mois est délivrée par le préfet territorialement compétent à l'étranger qui satisfait aux conditions définies à l'article L. 316-1 et qui a rompu tout lien avec les auteurs présumés des infractions mentionnées à cet article.

La même carte de séjour temporaire peut également être délivrée à un mineur âgé d'au moins seize ans, remplissant les conditions mentionnées au présent article et qui déclare vouloir exercer une activité professionnelle salariée ou suivre une formation professionnelle.

La demande de carte de séjour temporaire est accompagnée du récépissé du dépôt de plainte de l'étranger ou fait référence à la procédure pénale comportant son témoignage.

La carte de séjour temporaire est renouvelable pendant toute la durée de la procédure pénale mentionnée à l'alinéa précédent, sous réserve que les conditions prévues pour sa délivrance continuent d'être satisfaites.
