# Section 2 : Protection, accueil et hébergement des étrangers victimes de la traite des êtres humains et du proxénétisme coopérant avec les autorités judiciaires

- [Article R316-6](article-r316-6.md)
- [Article R316-7](article-r316-7.md)
- [Article R316-8](article-r316-8.md)
- [Article R316-9](article-r316-9.md)
- [Article R316-10](article-r316-10.md)
