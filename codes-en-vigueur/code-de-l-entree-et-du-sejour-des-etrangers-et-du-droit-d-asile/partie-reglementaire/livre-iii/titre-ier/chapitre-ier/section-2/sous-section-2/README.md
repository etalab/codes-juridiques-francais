# Sous-section 2 : Dispositions relatives à la préparation de l'intégration dans le pays d'origine

- [Article R311-30-1](article-r311-30-1.md)
- [Article R311-30-2](article-r311-30-2.md)
- [Article R311-30-3](article-r311-30-3.md)
- [Article R311-30-4](article-r311-30-4.md)
- [Article R311-30-5](article-r311-30-5.md)
- [Article R311-30-6](article-r311-30-6.md)
- [Article R311-30-7](article-r311-30-7.md)
- [Article R311-30-8](article-r311-30-8.md)
- [Article R311-30-9](article-r311-30-9.md)
- [Article R311-30-10](article-r311-30-10.md)
- [Article R311-30-11](article-r311-30-11.md)
