# Article R311-30-9

Si, à l'issue de la seconde évaluation, l'étranger atteint le niveau linguistique requis, il est dispensé de formation linguistique à son arrivée en France. Les dispositions du troisième alinéa de l'article R. 311-24 lui sont toutefois applicables. Il peut alors bénéficier, à sa demande, d'un accompagnement à la préparation du diplôme initial de langue française organisé par l'          Office français de l'immigration et de l'intégration .

Dans le cas où l'étranger n'atteint pas le niveau linguistique requis, cette évaluation permet de déterminer les caractéristiques de la formation qui lui est prescrite dans le cadre du contrat d'accueil et d'intégration à son arrivée en France.
