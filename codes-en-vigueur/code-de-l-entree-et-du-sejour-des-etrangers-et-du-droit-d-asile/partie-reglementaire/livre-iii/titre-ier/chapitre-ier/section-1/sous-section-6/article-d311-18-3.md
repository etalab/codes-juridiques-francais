# Article D311-18-3

La taxe prévue à l'article L. 311-15 doit être acquittée par l'employeur dans un délai de trois mois à compter de :

a) La délivrance des documents exigés aux 1° et 3° de l'article L. 211-1 du même code lors de la première entrée en France du travailleur étranger ou du salarié détaché ;

b) La délivrance de l'autorisation de travail mentionnée à l'article R. 5221-18 du code du travail lors de la première admission au séjour en qualité de salarié.
