# Sous-section 3 : Délivrance du titre de séjour.

- [Article R311-10](article-r311-10.md)
- [Article R311-11](article-r311-11.md)
- [Article R311-12](article-r311-12.md)
- [Article R311-13](article-r311-13.md)
