# Article R311-12

Le silence gardé pendant plus de quatre mois sur les demandes de titres de séjour vaut décision implicite de rejet.
