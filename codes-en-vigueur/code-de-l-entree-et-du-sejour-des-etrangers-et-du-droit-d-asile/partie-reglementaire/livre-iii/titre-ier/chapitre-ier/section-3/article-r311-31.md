# Article R311-31

Pour l'application des articles L. 311-10 à L. 311-12, l'étranger présente à l'appui de sa demande :

1° Les indications relatives à son état civil ;

2° Trois photographies de face, tête nue, de format 3,5 x 4,5 cm, récentes et parfaitement ressemblantes.
