# Article R311-32

L'autorisation provisoire de séjour mentionnée à l'article L. 311-10 est prévue pour l'exercice d'une mission d'intérêt général visant soit à promouvoir l'autonomie et la protection des personnes, à renforcer la cohésion sociale, à prévenir les exclusions ou, le cas échéant, à en corriger les effets, soit à mener des actions de solidarité en faveur de personnes défavorisées ou sinistrées résidant sur le territoire français.

Le contrat de volontariat mentionné à l'article L. 311-10 comprend les indications prévues à l'article 12 du décret n° 2006-1205 du 29 septembre 2006 relatif au volontariat associatif.
