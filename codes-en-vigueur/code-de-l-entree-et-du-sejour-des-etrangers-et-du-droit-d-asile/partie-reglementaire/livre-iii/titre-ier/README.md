# TITRE Ier : LES TITRES DE SÉJOUR

- [Chapitre Ier : Dispositions générales](chapitre-ier)
- [Chapitre II : La commission du titre de séjour.](chapitre-ii)
- [Chapitre III : La carte de séjour temporaire](chapitre-iii)
- [Chapitre IV : La carte de résident](chapitre-iv)
- [Chapitre V : La carte de séjour portant la mention "compétences et talents"](chapitre-v)
- [Chapitre VI : DISPOSITIONS APPLICABLES AUX ETRANGERS AYANT DEPOSE PLAINTE POUR CERTAINES INFRACTIONS OU TEMOIGNE DANS UNE PROCEDURE PENALE](chapitre-vi)
- [Chapitre VII : La carte de séjour portant la mention "retraité".](chapitre-vii)
