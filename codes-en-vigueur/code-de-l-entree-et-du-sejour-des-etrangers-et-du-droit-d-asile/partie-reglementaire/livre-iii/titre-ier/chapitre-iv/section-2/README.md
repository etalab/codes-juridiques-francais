# Section 2 : Délivrance de la carte de résident

- [Sous-section 1 : Délivrance subordonnée à une durée de séjour régulier.](sous-section-1)
- [Sous-section 2 : Délivrance de plein droit.](sous-section-2)
