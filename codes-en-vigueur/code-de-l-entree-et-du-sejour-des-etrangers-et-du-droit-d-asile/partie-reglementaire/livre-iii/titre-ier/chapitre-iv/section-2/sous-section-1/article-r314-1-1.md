# Article R314-1-1

L'étranger qui sollicite la délivrance de la carte de résident portant la mention "     résident de longue durée-UE " doit justifier qu'il remplit les conditions prévues à l'article L. 314-8 en présentant :

1° La justification qu'il réside légalement et de manière ininterrompue en France depuis au moins cinq ans, sous couvert de l'une des cartes de séjour mentionnées aux articles L. 314-8 et L. 314-8-2 ou sous couvert d'un des visas mentionnés aux 4°, 5°, 7°, 8°, 9° et 11° de l'article R. 311-3 ; les périodes d'absence du territoire français sont prises en compte dans le calcul des cinq années de résidence régulière ininterrompue lorsque chacune ne dépasse pas six mois consécutifs et qu'elles ne dépassent pas un total de dix mois ; s'agissant d'un étranger qui s'est vu reconnaître par la France la qualité de réfugié ou accorder le bénéfice de la protection subsidiaire, la période entre la date de dépôt de la demande d'asile et celle de la délivrance de l'une des cartes de séjour mentionnées au deuxième alinéa de l'article L. 314-8-2 est également prise en compte ;

2° La justification des raisons pour lesquelles il entend s'établir durablement en France, notamment au regard des conditions de son activité professionnelle et de ses moyens d'existence ;

3° La justification qu'il dispose de ressources propres, stables et régulières, suffisant à son entretien, indépendamment des prestations et des allocations mentionnées au deuxième alinéa de l'article L. 314-8, appréciées sur la période des cinq années précédant sa demande, par référence au montant du salaire minimum de croissance ; lorsque les ressources du demandeur ne sont pas suffisantes ou ne sont pas stables et régulières pour la période des cinq années précédant la demande, une décision favorable peut être prise, soit si le demandeur justifie être propriétaire de son logement ou en jouir à titre gratuit, soit en tenant compte de l'évolution favorable de sa situation quant à la stabilité et à la régularité de ses revenus, y compris après le dépôt de la demande ;

4° La justification qu'il dispose d'un logement approprié ;

5° La justification qu'il bénéficie d'une assurance maladie.

Le maire de la commune de résidence du demandeur émet un avis sur le caractère suffisant des conditions de ressources au regard des conditions de logement dans les conditions prévues aux articles R. 313-34-2 à R. 313-34-4.
