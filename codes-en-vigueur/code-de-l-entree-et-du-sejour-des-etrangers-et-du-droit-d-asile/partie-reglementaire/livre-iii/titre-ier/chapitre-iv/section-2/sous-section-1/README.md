# Sous-section 1 : Délivrance subordonnée à une durée de séjour régulier.

- [Article R314-1](article-r314-1.md)
- [Article R314-1-1](article-r314-1-1.md)
- [Article R314-1-2](article-r314-1-2.md)
- [Article R314-1-3](article-r314-1-3.md)
- [Article R314-1-4](article-r314-1-4.md)
