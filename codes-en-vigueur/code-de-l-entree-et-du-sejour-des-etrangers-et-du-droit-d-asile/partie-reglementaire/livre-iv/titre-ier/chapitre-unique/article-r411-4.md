# Article R411-4

Pour l'application du 1° de l'article L. 411-5, les ressources du demandeur et de son conjoint qui alimenteront de façon stable le budget de la famille sont appréciées sur une période de douze mois par référence à la moyenne mensuelle du salaire minimum de croissance au cours de cette période. Ces ressources sont considérées comme suffisantes lorsqu'elles atteignent un montant équivalent à :

- cette moyenne pour une famille de deux ou trois personnes ;

- cette moyenne majorée d'un dixième pour une famille de quatre ou cinq personnes ;

- cette moyenne majorée d'un cinquième pour une famille de six personnes ou plus.
