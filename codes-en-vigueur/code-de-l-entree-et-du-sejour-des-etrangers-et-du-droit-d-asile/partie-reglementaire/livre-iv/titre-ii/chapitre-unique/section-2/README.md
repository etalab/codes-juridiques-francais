# Section 2 : Réception des demandes.

- [Article R421-7](article-r421-7.md)
- [Article R421-8](article-r421-8.md)
- [Article R421-9](article-r421-9.md)
- [Article R421-10](article-r421-10.md)
