# Article R421-14

Pour procéder à la vérification des conditions de logement mentionnées à l'article R. 411-5, le maire examine les pièces justificatives mentionnées au 4° de l'article R. 421-4.
