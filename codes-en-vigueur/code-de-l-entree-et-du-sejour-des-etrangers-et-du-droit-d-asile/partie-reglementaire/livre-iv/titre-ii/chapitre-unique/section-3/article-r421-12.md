# Article R421-12

Pour procéder à la vérification des conditions de ressources mentionnées à l'article R. 411-4, le maire examine les pièces justificatives mentionnées au 3° de l'article R. 421-4.
