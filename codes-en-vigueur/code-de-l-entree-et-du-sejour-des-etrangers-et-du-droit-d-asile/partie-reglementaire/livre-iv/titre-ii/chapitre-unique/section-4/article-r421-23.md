# Article R421-23

Lorsqu'une décision de refus est motivée par la non-conformité du logement aux normes de superficie, ou de confort et d'habitabilité, ou par le caractère non probant des pièces attestant de la disponibilité du logement à l'arrivée de la famille, le demandeur qui présente, dans un délai de six mois suivant la notification du refus, une nouvelle demande est alors dispensé de la production des pièces mentionnées aux 1°, 2° et 3° de l'article R. 421-4 et à l'article R. 421-5.
