# Article R722-4

Le directeur général de l'office est nommé pour une durée de trois ans, renouvelable.

Les décisions et mesures relevant des compétences dévolues à l'office par les dispositions législatives du présent livre sont prises sous sa responsabilité.

Dans le cadre des fonctions plus spécialement dévolues à l'office par l'article L. 721-3, le directeur général est notamment habilité à :

1° Certifier la situation de famille et l'état civil des intéressés tels qu'ils résultent d'actes passés ou de faits ayant eu lieu dans le pays à l'égard duquel les craintes de persécution du réfugié ont été tenues pour fondées et, le cas échéant, d'événements postérieurs les ayant modifiés ; les actes et documents établis par l'office ont la valeur d'actes authentiques ;

2° Attester la conformité avec les lois du pays mentionné au 1° des actes passés dans ce pays ;

3° Signaler, le cas échéant, les intéressés à l'attention des autorités compétentes, en particulier pour les questions de visa, de titre de séjour, d'admission aux établissements d'enseignement et d'une manière générale pour l'accès aux droits sociaux auxquels peuvent prétendre les bénéficiaires de l'asile ;

4° Signaler aux autorités compétentes les bénéficiaires de la protection subsidiaire auxquels un titre de voyage doit être délivré et indiquer pour chaque cas la liste des pays autorisés.
