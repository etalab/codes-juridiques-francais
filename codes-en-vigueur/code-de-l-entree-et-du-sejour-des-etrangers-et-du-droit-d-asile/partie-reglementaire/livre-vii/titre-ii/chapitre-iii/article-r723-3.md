# Article R723-3

Lorsqu'il est saisi en application de la procédure prioritaire prévue au second alinéa de l'article L. 723-1, l'office statue dans un délai de quinze jours sur la demande d'asile. Ce délai est ramené à 96 heures lorsque le demandeur d'asile est placé en rétention administrative en application de l'article L. 551-1.

Lorsque, à la suite d'une décision de rejet devenue définitive, la personne intéressée entend soumettre à l'office des éléments nouveaux, sa demande de réexamen doit être précédée d'une nouvelle demande d'admission au séjour et être présentée selon la procédure prévue à l'article R. 723-1. Le délai prévu au premier alinéa de cet article est alors limité à huit jours.

La décision du directeur général de l'office sur la demande de réexamen est communiquée par lettre recommandée avec demande d'avis de réception et dans les conditions prévues au deuxième alinéa de l'article R. 213-3.
