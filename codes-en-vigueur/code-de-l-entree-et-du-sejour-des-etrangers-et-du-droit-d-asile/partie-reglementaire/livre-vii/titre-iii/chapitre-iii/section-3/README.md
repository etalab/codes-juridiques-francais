# Section 3 : Procédure d'avis de l'article L. 731-3.

- [Article R733-38](article-r733-38.md)
- [Article R733-39](article-r733-39.md)
- [Article R733-40](article-r733-40.md)
- [Article R733-41](article-r733-41.md)
