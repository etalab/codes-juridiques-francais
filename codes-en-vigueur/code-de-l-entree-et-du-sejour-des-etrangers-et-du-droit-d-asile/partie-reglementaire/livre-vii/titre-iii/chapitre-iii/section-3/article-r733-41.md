# Article R733-41

Le président de la cour et les présidents qu'il désigne à cet effet peuvent rejeter une demande manifestement insusceptible d'être examinée en application de l'article L. 731-3.
