# Article R733-39

Sous réserve de l'application de l'article R. 733-41, la demande est immédiatement communiquée au ministre de l'intérieur et au ministre chargé de l'asile, qui disposent d'un délai d'une semaine pour produire leurs observations.

Ces observations sont, dès leur réception, communiquées, par tout moyen, à l'intéressé.
