# Article R733-13

Le président de la formation de jugement ou, avant enrôlement du dossier, le président de la cour peut fixer la date de clôture de l'instruction écrite par une ordonnance notifiée aux parties quinze jours au moins avant cette date. L'ordonnance n'est pas motivée et ne peut faire l'objet d'aucun recours. L'instruction écrite peut être rouverte dans les mêmes formes.

Dans le cas où les parties sont informées de la date de l'audience deux mois au moins avant celle-ci, l'instruction écrite est close dix jours francs avant la date de l'audience. Cette information, qui indique la date de clôture de l'instruction, est valablement faite à l'avocat constitué à la date de son envoi ou, le cas échéant, à l'avocat désigné au titre de l'aide juridictionnelle à cette même date. Elle ne vaut pas avis d'audience au sens de l'article R. 733-19.

S'il n'a pas été fait application du premier ou du deuxième alinéa, l'instruction écrite est close cinq jours francs avant la date de l'audience.

Lorsque l'instruction écrite est close, seule la production des originaux des documents communiqués préalablement en copie demeure recevable jusqu'à la fin de l'audience.
