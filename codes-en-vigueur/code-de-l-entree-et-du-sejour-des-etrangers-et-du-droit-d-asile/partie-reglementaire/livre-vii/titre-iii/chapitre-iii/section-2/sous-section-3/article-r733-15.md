# Article R733-15

La cour peut prescrire toute mesure d'instruction qu'elle jugera utile.

En cas d'expertise ordonnée par la formation de jugement, le rapport déposé par l'expert désigné par le président de la cour est communiqué aux parties. Le président de la cour fixe également, par ordonnance, les honoraires dus à l'expert et arrête, sur justificatifs, le montant de ses frais et débours. L'ensemble est mis à la charge de la partie perdante sauf si les circonstances particulières de l'affaire justifient qu'il soit mis à la charge de l'autre partie ou partagés entre les parties.
