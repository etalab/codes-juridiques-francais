# Article R733-19

L'avis d'audience est adressé aux parties trente jours au moins avant le jour où l'affaire sera appelée à l'audience.

Le conseil du requérant est informé du jour de l'audience par tout moyen. Cette information a lieu sans délai lorsqu'il se constitue après la convocation adressée au requérant.

L'avis d'audience informe les parties de la clôture de l'instruction écrite prévue par l'article R. 733-13.

En cas d'urgence, y compris s'il a été fait application du deuxième alinéa de l'article R. 733-13, le délai de convocation prévu au premier alinéa peut être réduit, sans pouvoir être inférieur à sept jours. Dans ce cas, l'instruction écrite est close cinq jours francs avant l'audience.

Lorsque le président de la formation de jugement fait droit sur le siège à une demande de report de l'audience présentée par le requérant, il peut convoquer les parties, sans conditions de délai, à une audience ultérieure en remettant à l'intéressé ou à son avocat un nouvel avis d'audience. L'office est avisé sans délai.
