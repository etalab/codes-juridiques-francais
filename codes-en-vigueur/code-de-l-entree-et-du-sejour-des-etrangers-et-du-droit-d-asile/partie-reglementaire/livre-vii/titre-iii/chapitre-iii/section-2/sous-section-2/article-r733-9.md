# Article R733-9

Lorsqu'un recours est entaché d'une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours, la cour ne peut le rejeter en relevant d'office cette irrecevabilité qu'après avoir invité son auteur à le régulariser.

La demande de régularisation mentionne qu'à défaut de régularisation les conclusions pourront être rejetées comme irrecevables dès l'expiration du délai imparti qui, sauf urgence, ne peut être inférieur à quinze jours.
