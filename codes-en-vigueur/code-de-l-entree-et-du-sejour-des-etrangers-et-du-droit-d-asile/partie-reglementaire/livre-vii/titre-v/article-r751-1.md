# Article R751-1

Les administrateurs ad hoc chargés d'assister les mineurs non accompagnés d'un représentant légal qui demandent l'asile, mentionnés à l'article L. 751-1, sont désignés et indemnisés conformément aux dispositions des articles R. 111-13 à R. 111-24.
