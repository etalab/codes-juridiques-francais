# Article R742-5

L'étranger auquel la qualité de réfugié est reconnue par l'Office français de protection des réfugiés et apatrides ou la Cour nationale du droit d'asile est admis à souscrire une demande de délivrance de carte de résident dans les conditions prévues à l'article R. 314-2.

Dans un délai de huit jours à compter de sa demande, il est mis en possession d'un récépissé de la demande de titre de séjour qui vaut autorisation de séjour d'une durée de validité de trois mois renouvelable et qui porte la mention " reconnu réfugié ".

Ce récépissé confère à son titulaire le droit d'exercer la profession de son choix dans les conditions prévues à l'article L. 314-4.
