# Article R742-6

L'étranger qui s'est vu accorder le bénéfice de la protection subsidiaire par l'Office français de protection des réfugiés et apatrides ou la Cour nationale du droit d'asile est admis à souscrire une demande de délivrance de carte de séjour temporaire dans les conditions prévues à l'article R. 313-1.

Dans un délai de huit jours à compter de sa demande, il est mis en possession d'un récépissé de demande de titre de séjour qui vaut autorisation de séjour d'une durée de validité de trois mois renouvelable.

Ce récépissé confère à son titulaire le droit d'exercer la profession de son choix dans les conditions prévues à l'article L. 314-4.

Le bénéficiaire de la protection subsidiaire est ensuite mis en possession de la carte de séjour temporaire prévue à l'article L. 313-13.

La carte de séjour temporaire est renouvelée selon les modalités définies aux articles R. 313-35 et R. 313-36 sous réserve de l'application des dispositions de l'article L. 723-5.
