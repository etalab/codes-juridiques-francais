# Article R211-4

Pour effectuer les vérifications prévues à l'article L. 111-6, et par dérogation aux dispositions du premier alinéa de l'article 21 de la loi n° 2000-321 du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, les autorités diplomatiques et consulaires sursoient à statuer sur la demande de visa présentée par la personne qui se prévaut de l'acte d'état civil litigieux pendant une période maximale de quatre mois.

Lorsque, malgré les diligences accomplies, ces vérifications n'ont pas abouti, la suspension peut être prorogée pour une durée strictement nécessaire et qui ne peut excéder quatre mois.
