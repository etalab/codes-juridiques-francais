# Section 3 : Dispense de produire la déclaration d'entrée sur le territoire français mentionnée à l'article L. 531-2.

- [Article R212-6](article-r212-6.md)
