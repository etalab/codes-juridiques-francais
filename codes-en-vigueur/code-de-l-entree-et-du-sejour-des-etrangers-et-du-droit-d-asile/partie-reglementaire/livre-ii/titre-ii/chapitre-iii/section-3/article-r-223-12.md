# Article R*223-12

L'autorité administrative mentionnée aux articles R. 223-8, R. 223-9 et R. 223-11 est le ministre chargé de l'immigration.
