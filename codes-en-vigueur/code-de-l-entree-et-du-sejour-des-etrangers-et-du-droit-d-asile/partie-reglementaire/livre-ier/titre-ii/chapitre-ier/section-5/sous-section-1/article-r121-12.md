# Article R121-12

Les ressortissants mentionnés au 3° de l'article L. 121-1 qui ont établi leur résidence habituelle en France depuis moins de cinq ans bénéficient, à leur demande, d'un titre de séjour portant la mention " UE-étudiant ". La reconnaissance du droit de séjour n'est pas subordonnée à la détention de ce titre.

Ce titre est d'une durée de validité maximale d'un an renouvelable.

Sa délivrance est subordonnée à la production par le demandeur des justificatifs suivants :

1° Un titre d'identité ou un passeport en cours de validité ;

2° Un justificatif de son inscription dans un établissement d'enseignement pour y suivre à titre principal des études ou, dans ce cadre, une formation professionnelle ;

3° Une attestation de prise en charge par une assurance offrant les prestations mentionnées aux articles L. 321-1 et L. 331-2 du code de la sécurité sociale ;

4° Une déclaration ou tout autre moyen équivalent garantissant qu'il dispose de ressources suffisantes pour lui et, le cas échéant, pour les membres de sa famille.
