# Chapitre unique

- [Section 1 : Interprètes traducteurs.](section-1)
- [Section 2 : Administrateurs ad hoc désignés pour la représentation des mineurs maintenus en zone d'attente ou demandeurs du statut de réfugié.](section-2)
