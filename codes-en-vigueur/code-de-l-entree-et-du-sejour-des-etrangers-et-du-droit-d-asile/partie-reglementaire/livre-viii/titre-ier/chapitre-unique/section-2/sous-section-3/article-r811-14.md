# Article R811-14

Le ministre chargé de l'immigration informe la Commission de l'Union européenne et le Haut-Commissariat des Nations Unies pour les réfugiés des demandes de transfert.
