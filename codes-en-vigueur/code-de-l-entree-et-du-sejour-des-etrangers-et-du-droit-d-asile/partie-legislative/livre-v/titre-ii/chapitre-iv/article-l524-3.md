# Article L524-3

Il ne peut être fait droit à une demande d'abrogation d'un arrêté d'expulsion présentée plus de deux mois après la notification de cet arrêté que si le ressortissant étranger réside hors de France. Toutefois, cette condition ne s'applique pas :

1° Pour la mise en oeuvre de l'article L. 524-2 ;

2° Pendant le temps où le ressortissant étranger subit en France une peine d'emprisonnement ferme ;

3° Lorsque l'étranger fait l'objet d'un arrêté d'assignation à résidence pris en application des articles L. 523-3, L. 523-4 ou L. 523-5.
