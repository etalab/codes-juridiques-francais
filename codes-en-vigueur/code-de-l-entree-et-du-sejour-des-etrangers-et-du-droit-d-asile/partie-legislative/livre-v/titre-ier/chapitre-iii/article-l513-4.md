# Article L513-4

L'étranger auquel un délai de départ volontaire a été accordé en application du II de l'article L. 511-1 peut, dès la notification de l'obligation de quitter le territoire français, être astreint à se présenter à l'autorité administrative ou aux services de police ou aux unités de gendarmerie pour y indiquer ses diligences dans la préparation de son départ.

Un décret en Conseil d'Etat prévoit les modalités d'application du présent article.
