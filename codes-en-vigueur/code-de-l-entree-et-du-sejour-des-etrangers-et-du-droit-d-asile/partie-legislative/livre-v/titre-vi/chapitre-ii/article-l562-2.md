# Article L562-2

L'assignation à résidence avec surveillance électronique emporte, pour l'étranger, interdiction de s'absenter de son domicile ou de tout autre lieu désigné par l'autorité administrative ou le juge des libertés et de la détention en dehors des périodes fixées par ceux-ci.

Le contrôle de l'exécution de la mesure est assuré au moyen d'un procédé permettant de détecter à distance la présence ou l'absence de l'étranger dans le seul lieu désigné par le juge des libertés et de la détention pour chaque période fixée. La mise en œuvre de ce procédé peut conduire à imposer à la personne assignée le port, pendant toute la durée du placement sous surveillance électronique, d'un dispositif intégrant un émetteur.

Le procédé utilisé est homologué à cet effet par le ministre chargé de l'immigration et le ministre de la justice. Sa mise en œuvre doit garantir le respect de la dignité, de l'intégrité et de la vie privée de la personne.

Le contrôle à distance de la mesure est assuré par des fonctionnaires de la police ou de la gendarmerie nationales qui sont autorisés, pour l'exécution de cette mission, à mettre en œuvre un traitement automatisé de données nominatives.

La mise en œuvre du dispositif technique permettant le contrôle à distance peut être confiée à une personne de droit privé habilitée dans des conditions fixées par décret en Conseil d'Etat.

Dans la limite des périodes fixées dans la décision d'assignation à résidence avec surveillance électronique, les agents chargés du contrôle peuvent se rendre sur le lieu de l'assignation pour demander à rencontrer l'étranger. Ils ne peuvent toutefois pénétrer au domicile de la personne chez qui le contrôle est pratiqué sans l'accord de celle-ci.

Le non-respect des prescriptions liées à l'assignation à résidence avec surveillance électronique est sanctionné dans les conditions prévues à l'article L. 624-4.
