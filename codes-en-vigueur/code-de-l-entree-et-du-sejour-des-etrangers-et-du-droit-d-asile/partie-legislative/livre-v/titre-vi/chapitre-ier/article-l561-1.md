# Article L561-1

Lorsque l'étranger justifie être dans l'impossibilité de quitter le territoire français ou ne peut ni regagner son pays d'origine ni se rendre dans aucun autre pays, l'autorité administrative peut, jusqu'à ce qu'existe une perspective raisonnable d'exécution de son obligation, l'autoriser à se maintenir provisoirement sur le territoire français en l'assignant à résidence, par dérogation à l'article L. 551-1, dans les cas suivants :

1° Si l'étranger fait l'objet d'une obligation de quitter le territoire français sans délai ou si le délai de départ volontaire qui lui a été accordé est expiré ;

2° Si l'étranger doit être remis aux autorités d'un Etat membre de l'Union européenne en application des articles L. 531-1 ou L. 531-2 ;

3° Si l'étranger doit être reconduit à la frontière en application de l'article L. 531-3 ;

4° Si l'étranger doit être reconduit à la frontière en exécution d'une interdiction de retour ;

5° Si l'étranger doit être reconduit à la frontière en exécution d'une interdiction du territoire prévue au deuxième alinéa de l'article 131-30 du code pénal ;

6° Si l'étranger doit être reconduit à la frontière en exécution d'une interdiction administrative du territoire.

La décision d'assignation à résidence est motivée. Elle peut être prise pour une durée maximale de six mois, et renouvelée une fois ou plus dans la même limite de durée, par une décision également motivée. Par exception, cette durée ne s'applique ni aux cas mentionnés au 5° du présent article ni à ceux mentionnés aux articles L. 523-3 à L. 523-5 du présent code.

L'étranger astreint à résider dans les lieux qui lui sont fixés par l'autorité administrative doit se présenter périodiquement aux services de police ou aux unités de gendarmerie. L'étranger qui fait l'objet d'un arrêté d'expulsion ou d'une interdiction judiciaire ou administrative du territoire prononcés en tout point du territoire de la République peut, quel que soit l'endroit où il se trouve, être astreint à résider dans des lieux choisis par l'autorité administrative dans l'ensemble du territoire de la République. L'autorité administrative peut prescrire à l'étranger la remise de son passeport ou de tout document justificatif de son identité dans les conditions prévues à l'article L. 611-2. Si l'étranger présente une menace d'une particulière gravité pour l'ordre public, l'autorité administrative peut le faire conduire par les services de police ou de gendarmerie jusqu'aux lieux d'assignation.

Le non-respect des prescriptions liées à l'assignation à résidence est sanctionné dans les conditions prévues à l'article L. 624-4.
