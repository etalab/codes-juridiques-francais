# Article L555-1

L'interdiction du territoire prononcée à titre de peine principale et assortie de l'exécution provisoire entraîne de plein droit le placement de l'étranger dans des lieux ne relevant pas de l'administration pénitentiaire, dans les conditions définies au présent titre, pendant le temps strictement nécessaire à son départ. Le deuxième alinéa de l'article L. 551-2 et l'article L. 553-4 sont applicables. Quand un délai de cinq jours s'est écoulé depuis le prononcé de la peine, il est fait application des dispositions des chapitres II à IV du présent titre.

L'interdiction du territoire prononcée à titre de peine complémentaire peut également donner lieu au placement de l'étranger dans des lieux ne relevant pas de l'administration pénitentiaire, le cas échéant à l'expiration de sa peine d'emprisonnement, dans les conditions définies au présent titre.
