# Article L553-6

Un décret en Conseil d'Etat définit les modalités selon lesquelles les étrangers maintenus en rétention bénéficient d'actions d'accueil, d'information et de soutien, pour permettre l'exercice effectif de leurs droits et préparer leur départ.
