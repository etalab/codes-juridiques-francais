# Article L553-5

Sauf en cas de menace à l'ordre public à l'intérieur ou à l'extérieur du lieu de rétention, ou si la personne ne paraît pas psychologiquement à même de recevoir ces informations, l'étranger est informé par le responsable du lieu de rétention de toutes les prévisions de déplacement le concernant : audiences, présentation au consulat, conditions du départ.

Dans chaque lieu de rétention, un document rédigé dans les langues les plus couramment utilisées, et décrivant les droits de l'étranger au cours de la procédure d'éloignement et de rétention, ainsi que leurs conditions d'exercice, est mis à disposition des personnes retenues.

La méconnaissance des dispositions du présent article est sans conséquence sur la régularité et le bien-fondé des procédures d'éloignement et de rétention.
