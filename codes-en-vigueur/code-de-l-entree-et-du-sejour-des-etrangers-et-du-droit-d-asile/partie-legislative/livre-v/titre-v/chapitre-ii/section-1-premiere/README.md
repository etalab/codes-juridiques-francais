# Section 1 : Première saisine du juge des libertés et de la détention.

- [Article L552-1](article-l552-1.md)
- [Article L552-2](article-l552-2.md)
- [Article L552-3](article-l552-3.md)
- [Article L552-4](article-l552-4.md)
- [Article L552-4-1](article-l552-4-1.md)
- [Article L552-5](article-l552-5.md)
- [Article L552-6](article-l552-6.md)
