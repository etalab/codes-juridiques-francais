# Section 2 : Nouvelle saisine du juge des libertés et de la détention.

- [Article L552-7](article-l552-7.md)
- [Article L552-8](article-l552-8.md)
