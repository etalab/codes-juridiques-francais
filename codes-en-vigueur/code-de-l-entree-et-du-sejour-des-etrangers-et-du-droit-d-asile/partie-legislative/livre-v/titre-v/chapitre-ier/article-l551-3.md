# Article L551-3

A son arrivée au centre de rétention, l'étranger reçoit notification des droits qu'il est susceptible d'exercer en matière de demande d'asile. Il lui est notamment indiqué que sa demande d'asile ne sera plus recevable pendant la période de rétention si elle est formulée plus de cinq jours après cette notification.
