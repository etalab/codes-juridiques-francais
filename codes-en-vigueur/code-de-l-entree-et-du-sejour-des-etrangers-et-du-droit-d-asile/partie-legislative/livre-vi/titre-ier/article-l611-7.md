# Article L611-7

Un décret en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés, fixe les modalités d'application de l'article L. 611-6. Il précise la durée de conservation et les conditions de mise à jour des informations enregistrées, les catégories de personnes pouvant y accéder et les modalités d'habilitation de celles-ci ainsi que, le cas échéant, les conditions dans lesquelles les personnes intéressées peuvent exercer leur droit d'accès.
