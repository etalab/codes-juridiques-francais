# Article L611-9

Lorsqu'il existe une section autoroutière commençant dans la zone mentionnée à l'article L. 611-8 et que le premier péage autoroutier se situe au-delà de la ligne des vingt kilomètres, la visite peut en outre avoir lieu jusqu'à ce premier péage sur les aires de stationnement ainsi que sur le lieu de ce péage et les aires de stationnement attenantes.

Dans l'attente des instructions du procureur de la République, le véhicule peut être immobilisé pour une durée qui ne peut excéder quatre heures.

La visite, dont la durée est limitée au temps strictement nécessaire, se déroule en présence du conducteur et donne lieu à l'établissement d'un procès-verbal mentionnant les dates et heures du début et de la fin des opérations. Un exemplaire de ce procès-verbal est remis au conducteur et un autre transmis sans délai au procureur de la République.
