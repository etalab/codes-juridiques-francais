# Article L214-7

Le second alinéa de l'article L. 214-4 n'est pas applicable à l'étranger mineur.
