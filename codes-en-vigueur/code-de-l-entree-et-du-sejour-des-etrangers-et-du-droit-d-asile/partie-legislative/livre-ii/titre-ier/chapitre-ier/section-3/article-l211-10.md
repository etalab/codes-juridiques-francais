# Article L211-10

Un décret en Conseil d'Etat fixe les conditions d'application de la présente section, notamment les conditions dans lesquelles l'étranger peut être dispensé du justificatif d'hébergement en cas de séjour à caractère humanitaire ou d'échange culturel, ou lorsqu'il demande à se rendre en France pour une cause médicale urgente ou en raison des obsèques ou de la maladie grave d'un proche.
