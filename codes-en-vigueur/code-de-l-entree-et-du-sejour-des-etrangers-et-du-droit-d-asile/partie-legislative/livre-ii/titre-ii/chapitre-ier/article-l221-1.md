# Article L221-1

L'étranger qui arrive en France par la voie ferroviaire, maritime ou aérienne et qui, soit n'est pas autorisé à entrer sur le territoire français, soit demande son admission au titre de l'asile, peut être maintenu dans une zone d'attente située dans une gare ferroviaire ouverte au trafic international figurant sur une liste définie par voie réglementaire, dans un port ou à proximité du lieu de débarquement, ou dans un aéroport, pendant le temps strictement nécessaire à son départ et, s'il est demandeur d'asile, à un examen tendant à déterminer si sa demande n'est pas manifestement infondée.

Les dispositions du présent titre s'appliquent également à l'étranger qui se trouve en transit dans une gare, un port ou un aéroport si l'entreprise de transport qui devait l'acheminer dans le pays de destination ultérieure refuse de l'embarquer ou si les autorités du pays de destination lui ont refusé l'entrée et l'ont renvoyé en France.

Le présent titre s'applique également à l'étranger qui arrive en Guyane par la voie fluviale ou terrestre.
