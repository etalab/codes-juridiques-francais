# Chapitre VI : Dispositions applicables à Saint-Barthélemy et à Saint-Martin

- [Article L766-1](article-l766-1.md)
- [Article L766-2](article-l766-2.md)
