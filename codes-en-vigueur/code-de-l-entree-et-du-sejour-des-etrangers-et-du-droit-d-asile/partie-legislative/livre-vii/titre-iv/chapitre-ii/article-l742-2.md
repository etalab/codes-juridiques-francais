# Article L742-2

Par dérogation aux dispositions de l'article L. 742-1, le document provisoire de séjour peut être retiré ou son renouvellement refusé lorsqu'il apparaît, postérieurement à sa délivrance, que l'étranger se trouve dans un des cas de non-admission prévus aux 1° à 4° de l'article L. 741-4.
