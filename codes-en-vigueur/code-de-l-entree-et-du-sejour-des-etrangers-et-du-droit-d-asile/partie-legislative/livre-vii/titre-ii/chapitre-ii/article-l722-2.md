# Article L722-2

L'office est géré par un directeur général nommé par décret, sur proposition conjointe du ministre des affaires étrangères et du ministre chargé de l'asile.
