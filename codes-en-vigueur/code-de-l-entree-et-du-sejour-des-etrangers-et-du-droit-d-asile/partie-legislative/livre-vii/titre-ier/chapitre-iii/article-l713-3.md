# Article L713-3

Peut être rejetée la demande d'asile d'une personne qui aurait accès à une protection sur une partie du territoire de son pays d'origine si cette personne n'a aucune raison de craindre d'y être persécutée ou d'y être exposée à une atteinte grave et s'il est raisonnable d'estimer qu'elle peut rester dans cette partie du pays. Il est tenu compte des conditions générales prévalant dans cette partie du territoire, de la situation personnelle du demandeur ainsi que de l'auteur de la persécution au moment où il est statué sur la demande d'asile.
