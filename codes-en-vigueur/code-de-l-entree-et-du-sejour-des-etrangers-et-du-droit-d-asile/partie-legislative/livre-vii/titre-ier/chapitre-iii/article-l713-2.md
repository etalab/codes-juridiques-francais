# Article L713-2

Les persécutions prises en compte dans l'octroi de la qualité de réfugié et les menaces graves pouvant donner lieu au bénéfice de la protection subsidiaire peuvent être le fait des autorités de l'Etat, de partis ou d'organisations qui contrôlent l'Etat ou une partie substantielle du territoire de l'Etat, ou d'acteurs non étatiques dans les cas où les autorités définies à l'alinéa suivant refusent ou ne sont pas en mesure d'offrir une protection.

Les autorités susceptibles d'offrir une protection peuvent être les autorités de l'Etat et des organisations internationales et régionales.
