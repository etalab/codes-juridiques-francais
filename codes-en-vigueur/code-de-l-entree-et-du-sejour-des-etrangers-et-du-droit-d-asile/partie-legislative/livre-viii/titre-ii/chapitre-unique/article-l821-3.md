# Article L821-3

Chaque agent concourant à ces missions doit être désigné par l'entreprise attributaire du marché et faire l'objet d'un agrément préalable, dont la durée est limitée, de l'autorité administrative compétente ainsi que du procureur de la République.

Il bénéficie d'une formation adaptée et doit avoir subi avec succès un examen technique.
