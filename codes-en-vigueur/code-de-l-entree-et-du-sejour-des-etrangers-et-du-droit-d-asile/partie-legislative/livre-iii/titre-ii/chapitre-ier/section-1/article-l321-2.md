# Article L321-2

Les conditions de la circulation des étrangers en France sont déterminées par voie réglementaire.
