# Article L311-6

Lorsqu'une demande d'asile a été définitivement rejetée, l'étranger qui sollicite la délivrance d'une carte de séjour doit justifier, pour obtenir ce titre, qu'il remplit l'ensemble des conditions prévues par le présent code.
