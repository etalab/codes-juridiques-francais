# Article L311-1

Sous réserve des dispositions de l'article L. 121-1 ou des stipulations d'un accord international, tout étranger âgé de plus de dix-huit ans qui souhaite séjourner en France doit, après l'expiration d'un délai de trois mois depuis son entrée en France, être muni d'une carte de séjour.

Ce délai de trois mois peut être modifié par décret en Conseil d'Etat.
