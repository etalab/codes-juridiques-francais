# Article L314-3

La carte de résident peut être refusée à tout étranger dont la présence constitue une menace pour l'ordre public.
