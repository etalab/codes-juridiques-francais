# Article L317-1

L'étranger qui, après avoir résidé en France sous couvert d'une carte de résident, a établi ou établit sa résidence habituelle hors de France et qui est titulaire d'une pension contributive de vieillesse, de droit propre ou de droit dérivé, liquidée au titre d'un régime de base français de sécurité sociale, bénéficie, à sa demande, d'une carte de séjour portant la mention "retraité". Cette carte lui permet d'entrer en France à tout moment pour y effectuer des séjours n'excédant pas un an. Elle est valable dix ans et est renouvelée de plein droit. Elle n'ouvre pas droit à l'exercice d'une activité professionnelle.

Le conjoint du titulaire d'une carte de séjour "retraité", ayant résidé régulièrement en France avec lui, bénéficie d'un titre de séjour conférant les mêmes droits.
