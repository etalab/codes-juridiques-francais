# Article L313-1

La durée de validité de la carte de séjour temporaire ne peut être supérieure à un an et ne peut dépasser la durée de validité des documents et visas mentionnés à l'article L. 211-1 du présent code.

L'étranger doit quitter la France à l'expiration de la durée de validité de sa carte à moins qu'il n'en obtienne le renouvellement ou qu'il ne lui soit délivré une carte de résident.
