# Article L313-4

Par dérogation aux articles L. 311-2 et L. 313-1, l'étranger titulaire d'une carte de séjour temporaire au titre des articles L. 313-7 ou L. 313-8 depuis au moins un an ou d'un visa délivré pour un séjour d'une durée supérieure à trois mois octroyant à son titulaire les droits attachés aux cartes de séjour temporaire susmentionnées peut, à l'échéance de la validité de ce titre, en solliciter le renouvellement pour une durée supérieure à un an et ne pouvant excéder quatre ans.

Cette dérogation est accordée à l'étudiant étranger admis à suivre, dans un établissement d'enseignement supérieur habilité au plan national, une formation en vue de l'obtention d'un diplôme au moins équivalent au master.

Elle peut également être accordée au titulaire de la carte de séjour temporaire portant la mention " scientifique-chercheur ” en tenant compte de la durée de ses travaux de recherche.

Un décret en Conseil d'Etat précise les conditions d'application de ces dispositions.
