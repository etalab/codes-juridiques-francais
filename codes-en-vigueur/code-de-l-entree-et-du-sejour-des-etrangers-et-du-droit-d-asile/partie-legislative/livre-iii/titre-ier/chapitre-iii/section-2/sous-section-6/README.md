# Sous-section 6 : La carte de séjour temporaire portant la mention "vie privée et familiale".

- [Article L313-11](article-l313-11.md)
- [Article L313-11-1](article-l313-11-1.md)
- [Article L313-12](article-l313-12.md)
- [Article L313-13](article-l313-13.md)
