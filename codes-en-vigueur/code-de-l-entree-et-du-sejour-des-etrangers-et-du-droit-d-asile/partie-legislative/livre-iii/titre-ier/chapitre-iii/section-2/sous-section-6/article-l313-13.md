# Article L313-13

Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire prévue à l'article L. 313-11 est délivrée de plein droit à l'étranger qui a obtenu le bénéfice de la protection subsidiaire en application de l'article L. 712-1 du présent code, sans que la condition prévue à l'article L. 311-7 soit exigée.

Elle est également délivrée de plein droit au conjoint de cet étranger et à ses enfants dans l'année qui suit leur dix-huitième anniversaire ou entrant dans les prévisions de l'article L. 311-3 lorsque le mariage est antérieur à la date d'obtention de la protection subsidiaire ou, à défaut, lorsqu'il a été célébré depuis au moins un an, sous réserve d'une communauté de vie effective entre époux. La condition prévue à l'article L. 311-7 n'est pas exigée.

La carte délivrée au titre du présent article donne droit à l'exercice d'une activité professionnelle.
