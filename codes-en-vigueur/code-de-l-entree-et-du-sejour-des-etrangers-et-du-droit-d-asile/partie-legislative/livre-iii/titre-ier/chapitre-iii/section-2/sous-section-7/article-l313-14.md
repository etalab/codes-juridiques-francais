# Article L313-14

La carte de séjour temporaire mentionnée à l'article L. 313-11 ou la carte de séjour temporaire mentionnée au 1° de l'article L. 313-10 peut être délivrée, sauf si sa présence constitue une menace pour l'ordre public, à l'étranger ne vivant pas en état de polygamie dont l'admission au séjour répond à des considérations humanitaires ou se justifie au regard des motifs exceptionnels qu'il fait valoir, sans que soit opposable la condition prévue à l'article L. 311-7.

L'autorité administrative est tenue de soumettre pour avis à la commission mentionnée à l'article L. 312-1 la demande d'admission exceptionnelle au séjour formée par l'étranger qui justifie par tout moyen résider en France habituellement depuis plus de dix ans.

Un décret en Conseil d'Etat définit les modalités d'application du présent article.
