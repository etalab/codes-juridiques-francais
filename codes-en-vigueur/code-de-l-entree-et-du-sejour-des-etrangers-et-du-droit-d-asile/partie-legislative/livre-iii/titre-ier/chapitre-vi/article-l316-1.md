# Article L316-1

Sauf si sa présence constitue une menace à l'ordre public, une carte de séjour temporaire portant la mention " vie privée et familiale " peut être délivrée à l'étranger qui dépose plainte contre une personne qu'il accuse d'avoir commis à son encontre les infractions visées aux articles 225-4-1 à 225-4-6 et 225-5 à 225-10 du code pénal ou témoigne dans une procédure pénale concernant une personne poursuivie pour ces mêmes infractions. La condition prévue à l'article L. 311-7 n'est pas exigée. Cette carte de séjour temporaire ouvre droit à l'exercice d'une activité professionnelle. Elle est renouvelée pendant toute la durée de la procédure pénale, sous réserve que les conditions prévues pour sa délivrance continuent d'être satisfaites.

En cas de condamnation définitive de la personne mise en cause, une carte de résident est délivrée de plein droit à l'étranger ayant déposé plainte ou témoigné.
