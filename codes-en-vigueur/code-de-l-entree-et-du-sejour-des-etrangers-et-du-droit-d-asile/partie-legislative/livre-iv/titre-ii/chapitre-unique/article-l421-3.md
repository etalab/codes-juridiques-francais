# Article L421-3

A l'issue de l'instruction, le maire émet un avis motivé. Cet avis est réputé favorable à l'expiration d'un délai de deux mois à compter de la communication du dossier par l'autorité administrative. Le dossier est transmis à l'Office français de l'immigration et de l'intégration qui peut demander à ses agents de procéder, s'ils ne l'ont déjà fait, à des vérifications sur place dans les conditions prévues à l'article L. 421-2.
