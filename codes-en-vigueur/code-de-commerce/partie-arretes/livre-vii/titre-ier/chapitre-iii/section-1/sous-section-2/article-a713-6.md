# Article A713-6

Pour l'application de l'article R. 713-12, les frais de campagne s'entendent du coût du papier, de l'impression des bulletins de vote, des circulaires et des affiches et des frais d'affichage.

Chaque groupement sous l'étiquette duquel des candidatures sont présentées dans la circonscription, chaque candidat isolé peuvent prétendre au remboursement des frais de reproduction d'un seul modèle de circulaire, d'un seul modèle d'affiche et d'un modèle de bulletin de vote par catégorie ou, le cas échéant, sous-catégorie professionnelle.
