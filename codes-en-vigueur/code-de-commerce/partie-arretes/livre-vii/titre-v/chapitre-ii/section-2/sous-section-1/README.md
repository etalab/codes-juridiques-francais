# Sous-section 1  :   De la demande d'autorisation

- [Article A752-1](article-a752-1.md)
- [Article A752-2](article-a752-2.md)
- [Article A752-3](article-a752-3.md)
