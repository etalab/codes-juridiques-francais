# Sous-section 1  :   Dispositions communes à tous les marchés d'intérêt national

- [Article A761-15](article-a761-15.md)
- [Article A761-16](article-a761-16.md)
