# Article A123-11

Les avis rendus par la commission sont communiqués au demandeur. Ils sont transmis au comité de coordination du registre du commerce et des sociétés. La commission décide, le cas échéant, leur publication. Cette publication se fait en ligne, sur le site internet du ministère chargé du commerce et de l'artisanat.

La commission établit un rapport annuel destiné aux ministres mentionnés à l'article A. 123-7. Elle peut formuler toute proposition, y compris de disposition législative ou réglementaire, de nature à remédier aux difficultés ou anomalies dont elle a eu à connaître.
