# Article A123-6

L'accusé de réception prévu à l'article R. 123-25, délivré par les centres de formalités des entreprises, leurs services communs ou les greffiers dans les conditions prévues à l'article R. 123-22, comporte les mentions suivantes :

1° Le nom, l'adresse postale et électronique ainsi que le numéro de téléphone du centre de formalités destinataire ;

2° Les formules suivantes :

« Le dossier de déclaration d'entreprise que vous avez adressé au centre de formalités des entreprises / au greffe de... a été reçu le... à.... » ;

« Le présent accusé de réception vous est adressé automatiquement. Si votre dossier est complet, vous recevrez prochainement un récépissé de dépôt de dossier de création d'entreprise par voie postale ou électronique. Si votre dossier est incomplet, il vous sera demandé de le compléter dans un délai qui vous sera alors précisé. »
