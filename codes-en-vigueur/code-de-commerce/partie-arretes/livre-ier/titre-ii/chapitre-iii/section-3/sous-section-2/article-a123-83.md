# Article A123-83

Dans le cadre des opérations de mise à jour du répertoire, l'INSEE peut procéder à des enquêtes administratives sous réserve des dispositions prévues aux articles R. 123-228 à R. 123-230.
