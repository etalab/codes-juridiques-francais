# Article A123-95

La diffusion des renseignements inscrits dans SIRENE concernant les établissements du ministère de la défense est soumise à un accord préalable du ministre chargé de la défense.
