# Sous-paragraphe 1 : De la communication et de l'inscription des actes

- [Article A123-65](article-a123-65.md)
- [Article A123-66](article-a123-66.md)
- [Article A123-67](article-a123-67.md)
- [Article A123-68](article-a123-68.md)
- [Article A123-69](article-a123-69.md)
- [Article A123-70](article-a123-70.md)
- [Article A123-71](article-a123-71.md)
- [Article A123-72](article-a123-72.md)
- [Article A123-73](article-a123-73.md)
