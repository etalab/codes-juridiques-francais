# Article A123-61-1

<div align="left">Un modèle type de déclaration de confidentialité des comptes annuels prévue à l'article R. 123-111-1 figure à l'annexe 1-5 au présent livre.</div>
