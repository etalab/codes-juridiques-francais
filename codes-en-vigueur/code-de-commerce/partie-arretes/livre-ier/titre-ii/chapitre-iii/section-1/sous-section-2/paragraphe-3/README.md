# Paragraphe 3 : Des dépôts en annexe au registre

- [Sous-paragraphe 1 : Des dépôts incombant aux personnes morales dont le siège est sur le territoire français](sous-paragraphe-1)
- [Sous-paragraphe 4 : Dispositions propres aux personnes physiques](sous-paragraphe-4)
