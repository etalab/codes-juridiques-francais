# Sous-sous-paragraphe 2 : Des déclarations aux fins d'immatriculation secondaire hors du ressort de l'établissement principal

- [Article A123-20](article-a123-20.md)
- [Article A123-21](article-a123-21.md)
