# Chapitre IV : Dispositions communes

- [Section 2 : De la représentation des professions auprès des pouvoirs publics](section-2)
- [Section 3 : De la garantie de la représentation des fonds, de la responsabilité civile professionnelle et de la rémunération](section-3)
- [Section 4 : De la comptabilité, du dépôt de fonds, des contrôles et dispositions diverses](section-4)
- [Section 6 : Des sociétés de participations financières de professions libérales       d'administrateurs judiciaires et de mandataires judiciaires](section-6)
