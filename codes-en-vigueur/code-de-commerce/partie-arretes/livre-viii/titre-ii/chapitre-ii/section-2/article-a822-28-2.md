# Article A822-28-2

La durée de la formation professionnelle est de cent vingt heures au cours de trois années consécutives. Vingt heures au moins sont accomplies au cours d'une même année.
