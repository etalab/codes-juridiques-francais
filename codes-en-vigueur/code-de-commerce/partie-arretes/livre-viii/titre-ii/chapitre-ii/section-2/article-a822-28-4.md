# Article A822-28-4

La compagnie nationale des commissaires aux comptes définit annuellement les orientations générales et les différents domaines sur lesquels l'obligation de formation peut porter.

Le commissaire aux comptes consacre un minimum de soixante heures de formation au cours d'une période de trois années consécutives aux domaines suivants : la déontologie du commissaire aux comptes, les normes d'exercice professionnel, les bonnes pratiques professionnelles identifiées et la doctrine professionnelle, les techniques d'audit et d'évaluation du contrôle interne, le cadre juridique de la mission de commissaire aux comptes et les matières comptables, financières, juridiques et fiscales.
