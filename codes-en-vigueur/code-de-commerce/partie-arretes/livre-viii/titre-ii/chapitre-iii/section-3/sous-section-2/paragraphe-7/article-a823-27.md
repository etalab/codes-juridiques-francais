# Article A823-27

La norme d'exercice professionnel relative à la justification des appréciations, homologuée par le garde des sceaux, ministre de la justice, figure ci-dessous :

NORME D'EXERCICE PROFESSIONNEL RELATIVE

À LA JUSTIFICATION DES APPRÉCIATIONS

Introduction

1. En application des dispositions de l'article L. 823-9, le commissaire aux comptes justifie de ses appréciations pour toutes les personnes ou entités dont les comptes annuels ou consolidés font l'objet d'une certification établie conformément à ce même article.

2. Cette obligation s'applique aux rapports établis par le commissaire aux comptes sur les comptes annuels et les comptes consolidés, et ne s'applique pas aux autres rapports susceptibles d'être émis par le commissaire aux comptes dans le cadre de sa mission.

3. La présente norme a pour objet de définir les principes et de préciser leurs modalités d'application concernant l'obligation légale faite au commissaire aux comptes de « justifier de ses appréciations » dans son rapport général sur les comptes annuels et dans son rapport sur les comptes consolidés.

Concept de justification des appréciations

4. La « justification des appréciations » effectuée par le commissaire aux comptes constitue une explicitation de celles-ci et, ce faisant, une motivation de l'opinion émise. Elle doit permettre au destinataire du rapport de mieux comprendre les raisons pour lesquelles le commissaire aux comptes a émis son opinion sur les comptes.

5.L'explicitation de certaines appréciations ne saurait se substituer à la nécessité de formuler une opinion avec réserve ou un refus de certifier ou d'insérer un paragraphe d'observation dans le rapport.

6. La justification de ses appréciations ne saurait conduire le commissaire aux comptes à être un dispensateur d'informations dont la diffusion relève de la responsabilité des dirigeants.

Appréciations de nature à faire l'objet d'une justification

7. Le commissaire aux comptes justifie de ses appréciations en toutes circonstances. Sur la base de son jugement professionnel et au vu des diligences effectuées tout au long de sa mission, il retient dans son rapport les appréciations qui lui sont apparues importantes.

8. Sans préjudice d'autres appréciations que le commissaire aux comptes jugerait nécessaire de justifier pour répondre à l'obligation posée par la loi, les appréciations de nature à faire l'objet d'une justification se rapportent généralement à des éléments déterminants pour la compréhension des comptes. Entrent dans ce cadre, notamment, les appréciations portant sur :

― les options retenues dans le choix des méthodes comptables ou dans leurs modalités de mise en œuvre lorsqu'elles ont des incidences majeures sur le résultat, la situation financière ou la présentation d'ensemble des comptes de l'entité ;

― les estimations comptables importantes, notamment celles manquant de données objectives et impliquant un jugement professionnel dans leur appréciation ;

― la présentation d'ensemble des comptes annuels et consolidés, qu'il s'agisse du contenu de l'annexe ou de la présentation des états de synthèse.

Le commissaire aux comptes peut également estimer nécessaire de justifier d'appréciations portant sur les procédures de contrôle interne concourant à l'élaboration des comptes, qu'il est conduit à apprécier dans le cadre de la mise en œuvre de sa démarche d'audit.

Formulation de la justification des appréciations

9. Le commissaire aux comptes formule la justification de ses appréciations par référence explicite aux dispositions de l'article L. 823-9 et de manière appropriée au regard des circonstances propres à chaque cas d'espèce.

10. Cette formulation doit être claire et comprendre, pour chaque appréciation devant être justifiée :

― l'identification du sujet et la référence, si elle est possible, à l'annexe aux comptes ;

― un résumé des diligences effectuées par le commissaire aux comptes pour fonder son appréciation ;

― une conclusion, exprimée de façon positive, en cohérence avec l'opinion formulée sur les comptes, et qui ne constitue pas une réserve déguisée.

11. Le commissaire aux comptes précise que les appréciations justifiées s'inscrivent dans le cadre de la démarche d'audit des comptes, pris dans leur ensemble, et ont donc contribué à la formation de l'opinion exprimée sur ces comptes. La formulation retenue ne doit pas conduire à apporter une assurance spécifique sur les éléments isolés des comptes faisant l'objet d'une justification des appréciations du commissaire aux comptes.

12. La justification des appréciations peut éventuellement être formulée de manière moins développée dans les cas où :

― les principes comptables retenus par l'entité ou le groupe ne donnent pas lieu à plusieurs interprétations ou options possibles, y compris dans leurs modalités d'application, pour ce qui concerne les éléments significatifs du bilan et du compte de résultat ;

― il n'existe pas d'événement ou de décision intervenus au cours de l'exercice dont l'incidence sur les comptes ou la compréhension que pourrait en avoir un lecteur est apparue importante au commissaire aux comptes ;

― aucun élément significatif dans les comptes n'est constitué à partir d'estimations fondées sur des données subjectives.

Place de la justification des appréciations dans le rapport

13. La justification des appréciations du commissaire aux comptes figure dans une partie de rapport distincte, placée après celle relative à l'expression de l'opinion du commissaire aux comptes. Le rapport sur les comptes comporte ainsi les trois parties distinctes suivantes, nettement individualisées :

― opinion sur les comptes annuels ou opinion sur les comptes consolidés, y compris, le cas échéant, la motivation des réserves ou du refus de certifier et le paragraphe prévu pour les observations ;

― justification des appréciations ;

― vérifications et informations spécifiques pour les comptes annuels ou vérification spécifique pour les comptes consolidés.

Lien entre la justification des appréciations et les observations

14. Lorsqu'un point concernant les comptes nécessite à la fois une observation et une justification des appréciations, ce point est évoqué respectivement dans la première partie du rapport après l'expression de l'opinion au titre de l'observation et dans la deuxième partie du rapport au titre de la justification des appréciations. Cette situation peut se présenter, par exemple, lorsqu'un changement de méthode comptable est intervenu, ou lorsqu'il existe une incertitude relative à la continuité de l'exploitation.

Précision concernant la certification avec réserve

15.L'exposé des motivations fondant une certification avec réserve constitue une justification des appréciations et trouve sa place avant l'expression de l'opinion émise sur les comptes. Une certification avec réserve ne dispense pas le commissaire aux comptes de devoir justifier de ses appréciations sur d'autres points que ceux ayant motivé la réserve même si ces autres appréciations ne posent pas de difficultés particulières. Ces autres justifications d'appréciations figurent dans la deuxième partie de son rapport.

Cas du refus de certifier

16.L'exposé des motivations conduisant à un refus de certifier est de nature à répondre à l'obligation de justification des appréciations. Dans cette situation, le commissaire aux comptes n'a pas à justifier de ses appréciations sur d'autres points que ceux ayant motivé le refus de certifier. Il précise dans la partie du rapport relative à la justification des appréciations qu'il n'y a pas lieu de justifier d'autres appréciations eu égard à la nature de l'opinion exprimée dans la première partie de son rapport.
