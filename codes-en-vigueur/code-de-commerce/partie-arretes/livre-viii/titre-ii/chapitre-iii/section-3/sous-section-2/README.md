# Sous-section 2 : De la certification des comptes

- [Paragraphe 1 : Des principes généraux](paragraphe-1)
- [Paragraphe 2 : De l'analyse des risques](paragraphe-2)
- [Paragraphe 3 : Des techniques de contrôle](paragraphe-3)
- [Paragraphe 4 : Des contrôles des risques spécifiques au cours de la mission](paragraphe-4)
- [Paragraphe 5 : Des contrôles particuliers](paragraphe-5)
- [Paragraphe 6 : De l'utilisation des travaux d'autres intervenants](paragraphe-6)
- [Paragraphe 7 : De l'élaboration des rapports de certification](paragraphe-7)
- [Paragraphe 8 : De la certification des comptes annuels des entités mentionnées à l'article L. 823-12-1.](paragraphe-8)
- [Paragraphe 9 : De la certification des comptes des organismes nationaux de sécurité sociale.](paragraphe-9)
