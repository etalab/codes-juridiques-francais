# Paragraphe 6 : De l'utilisation des travaux d'autres intervenants

- [Article A823-23](article-a823-23.md)
- [Article A823-24](article-a823-24.md)
- [Article A823-25](article-a823-25.md)
