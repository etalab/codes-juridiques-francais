# Article Annexe 4-4

TABLEAU RÉCAPITULATIF DES DONNÉES FINANCIÈRES POUR LES TROIS DERNIERS EXERCICES À JOINDRE

AU DOSSIER DE NOTIFICATION D'UNE OPÉRATION DE CONCENTRATION

Nom de l'entité : ... N<sup>o</sup> SIREN (dans le cas d'une société française) : ...

Données consolidées : oui non (rayer la mention inutile).

<table>
<thead>
<tr>
<td width="143">
<br/>
<br/>
</td>
<td colspan="2" width="104">
<br/>
<p align="center">Exercice</p>
<p align="center">N</p>
<p align="center">clos le :</p>
</td>
<td colspan="2" width="104">
<br/>
<p align="center">Exercice</p>
<p align="center">N-1</p>
<p align="center">clos le :</p>
</td>
<td colspan="2" width="104">
<br/>
<p align="center">Exercice</p>
<p align="center">N-2</p>
<p align="center">clos le :</p>
</td>
</tr>
</thead>
<tbody>
<tr>
<td valign="top">
<br/>
<p align="center">Comptes de résultat</p>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td valign="top">
<p>Chiffres d'affaires total hors taxes</p>
<p>Chiffres d'affaires hors taxes réalisé auprès des clients situés dans l'Union européenne</p>
<p>Chiffre d'affaire hors taxes réalisé auprès des clients situés en France</p>
<p>Valeur ajoutée brute</p>
<p>Excédent brut d'exploitation</p>
<p>Résultat d'exploitation</p>
<p>Intérêts et charges assimilées sur dette financière</p>
<p>Produits financiers des placements</p>
<p>Produits financiers des immobilisations financières</p>
<p>Résultat financier</p>
<p>Résultat net (1)</p>
<p>Part des actionnaires ou des associés minoritaires</p>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">Bilan</p>
</td>
<td valign="top">
<br/>
<p align="center">Brut</p>
</td>
<td valign="top">
<br/>
<p align="center">Net</p>
</td>
<td valign="top">
<br/>
<p align="center">Brut</p>
</td>
<td valign="top">
<br/>
<p align="center">Net</p>
</td>
<td valign="top">
<br/>
<p align="center">Brut</p>
</td>
<td valign="top">
<br/>
<p align="center">Net</p>
</td>
</tr>
<tr>
<td valign="top">
<p>Total du bilan</p>
<p>Immobilisations incorporelles</p>
<p>Immobilisations corporelles</p>
<p>Immobilisations financières</p>
<p>Créances de l'actif circulant</p>
<p>Disponibilités et valeurs mobilières de placement</p>
</td>
<td valign="top">
<br/>
<br/>
</td>
<td valign="top">
<br/>
<br/>
</td>
<td valign="top">
<br/>
<br/>
</td>
<td valign="top">
<br/>
<br/>
</td>
<td valign="top">
<br/>
<br/>
</td>
<td valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<br/>
</td>
<td colspan="2" valign="top">
<br/>
<p align="center">Exercice</p>
<p align="center">N</p>
<p align="center">clos le :</p>
</td>
<td colspan="2" valign="top">
<br/>
<p align="center">Exercice</p>
<p align="center">N-1</p>
<p align="center">clos le :</p>
</td>
<td colspan="2" valign="top">
<br/>
<p align="center">Exercice</p>
<p align="center">N-2</p>
<p align="center">clos le :</p>
</td>
</tr>
<tr>
<td valign="top">
<p>Fonds propres (2)</p>
<p>Part des actionnaires ou des associés minoritaires</p>
<p>Provisions pour risques et charges</p>
<p>Dettes financières</p>
<p>Autres dettes</p>
<p>Ensemble des dettes à plus d'un an de la clôture</p>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">Investissements et cessions</p>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td valign="top">
<p>Acquisitions d'immobilisations corporelles et incorporelles</p>
<p>Acquisitions ou argumentations d'immobilisations financières</p>
<p>Prix de cession des immobilisations cédées et valeur des autres diminutions d'immobilisations financières</p>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">Autres renseignements</p>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td valign="top">
<p>Dépenses de recherche et développement</p>
<p>Dépenses de publicité</p>
<p>Capitalisation boursière à la clôture (3)</p>
<p>Effectifs moyens</p>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
<td colspan="2" valign="top">
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="7" valign="top">
<p>(1) Dans le cas de données consolidées, il s'agit du résultat de l'ensemble consolidé.</p>
<p>(2) Non compris la part des actionnaires ou associés minoritaires dans le cas de données consolidées.</p>
<p>(3) Dans le cas d'un groupe, donner le nom de la société cotée.</p>
</td>
</tr>
</tbody>
</table>
