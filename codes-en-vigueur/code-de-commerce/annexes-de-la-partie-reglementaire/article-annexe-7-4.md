# Article Annexe 7-4

Nombre d'assesseurs des chambres commerciales des tribunaux de grande instance

<div align="center"/>
<div align="center"/>

des départements du Bas-Rhin, du Haut-Rhin et de la Moselle

<div align="center"/>
<table>
<tbody>
<tr>
<td>
<p align="center">DÉPARTEMENTS</p>
</td>
<td>
<p align="center">SIÈGE DE LA CHAMBRE COMMERCIALE<br/>du tribunal de grande instance</p>
</td>
<td>
<p align="center">NOMBRE D'ASSESSEURS<br/>de la chambre commerciale<br/>du tribunal de grande instance</p>
</td>
</tr>
<tr>
<td>
<br/>
<p align="center">Cour d'appel de Colmar</p>
</td>
<td/>
<td/>
</tr>
<tr>
<td>
<br/>
<p align="left">Bas-Rhin </p>
</td>
<td>
<br/>
<p align="left">Saverne</p>
</td>
<td>
<p align="center">8</p>
</td>
</tr>
<tr>
<td>
<p align="left">
<br/>
</p>
</td>
<td>
<p align="left">Strasbourg</p>
</td>
<td>
<p align="center">32</p>
</td>
</tr>
<tr>
<td>
<p align="left">Haut-Rhin </p>
</td>
<td>
<p align="left">Colmar</p>
</td>
<td>
<p align="center">12</p>
</td>
</tr>
<tr>
<td>
<p align="left">
<br/>
</p>
</td>
<td>
<p align="left">Mulhouse</p>
</td>
<td>
<p align="center">22</p>
</td>
</tr>
<tr>
<td>
<p align="center">Cour d'appel de Metz</p>
</td>
<td/>
<td/>
</tr>
<tr>
<td>
<p align="left">Moselle </p>
</td>
<td>
<p align="left">Metz</p>
</td>
<td>
<p align="center">19</p>
</td>
</tr>
<tr>
<td>
<p align="left">
<br/>
</p>
</td>
<td>
<p align="left">Sarreguemines</p>
</td>
<td>
<p align="center">12</p>
</td>
</tr>
<tr>
<td>
<p align="left">
<br/>
</p>
</td>
<td>
<p align="left">Thionville</p>
</td>
<td>
<p align="center">12</p>
</td>
</tr>
</tbody>
</table>
<div align="center"/>

<div align="center"/>

<div align="center"/>

Nombre de juges élus des tribunaux mixtes de commerce des départements d'outre-mer

<div align="center"/>

<div align="center"/>
<table>
<tbody>
<tr>
<td>
<p align="center">DÉPARTEMENTS</p>
</td>
<td>
<p align="center">SIÈGE DU TRIBUNAL MIXTE<br/>de commerce</p>
</td>
<td>
<p align="center">NOMBRE DE JUGES ÉLUS<br/>du tribunal mixte de commerce</p>
</td>
</tr>
<tr>
<td>
<p align="center">Cour d'appel de Basse-Terre</p>
</td>
<td/>
<td/>
</tr>
<tr>
<td>
<p align="left">Guadeloupe </p>
</td>
<td>
<p align="left">Basse-Terre</p>
</td>
<td>
<p align="center">5</p>
</td>
</tr>
<tr>
<td/>
<td>
<p align="left">Pointe-à-Pitre</p>
</td>
<td>
<p align="center">6</p>
</td>
</tr>
<tr>
<td>
<p align="center">Cour d'appel de Cayenne </p>
</td>
<td/>
<td/>
</tr>
<tr>
<td>Guyane </td>
<td>
<p align="left">Cayenne</p>
</td>
<td>
<p align="center">5</p>
</td>
</tr>
<tr>
<td>
<p align="center">Cour d'appel de Fort-de-France</p>
</td>
<td/>
<td/>
</tr>
<tr>
<td>
<p align="left">Martinique </p>
</td>
<td>
<p align="left">Fort-de-France</p>
</td>
<td>
<p align="center">7</p>
</td>
</tr>
<tr>
<td>
<p align="center">Cour d'appel de Saint-Denis de La Réunion</p>
</td>
<td/>
<td/>
</tr>
<tr>
<td>
<p align="left">Mayotte</p>
</td>
<td>
<p align="left">Mamoudzou </p>
</td>
<td>
<p align="center">5</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">La Réunion</p>
</td>
<td align="center">
<p align="left">Saint-Denis</p>
</td>
<td align="center">
<p align="center">5</p>
</td>
</tr>
<tr>
<td>
<br/>
</td>
<td>
<p align="left">Saint-Pierre</p>
</td>
<td>
<p align="center">5</p>
</td>
</tr>
</tbody>
</table>
<div align="center"/>

<div align="center"/>
