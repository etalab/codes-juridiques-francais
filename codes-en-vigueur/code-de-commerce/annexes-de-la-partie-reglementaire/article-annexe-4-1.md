# Article Annexe 4-1

JURIDICTIONS COMPÉTENTES POUR CONNAÎTRE, EN APPLICATION DE L'ARTICLE L. 420-7,

DES PROCÉDURES APPLICABLES AUX PERSONNES QUI NE SONT NI COMMERÇANTS NI ARTISANS

<table>
<tbody>
<tr>
<td width="227">
<p align="center">SIÈGE DES TRIBUNAUX<br/>de grande instance</p>
</td>
<td width="491">
<p align="center">RESSORT</p>
</td>
</tr>
<tr>
<td valign="top" width="227">
<p align="left">Marseille.</p>
</td>
<td valign="top" width="491">
<p align="left">Le ressort des cours d'appel d'Aix-en-Provence, Bastia, Montpellier et Nîmes.</p>
</td>
</tr>
<tr>
<td valign="top" width="227">
<p align="left">Bordeaux.</p>
</td>
<td valign="top" width="491">
<p align="left">Le ressort des cours d'appel d'Agen, Bordeaux, Limoges, Pau et Toulouse.</p>
</td>
</tr>
<tr>
<td valign="top" width="227">
<p align="left">Lille.</p>
</td>
<td valign="top" width="491">
<p align="left">Le ressort des cours d'appel d'Amiens, Douai, Reims et Rouen.</p>
</td>
</tr>
<tr>
<td valign="top" width="227">
<p align="left">Fort-de-France.</p>
</td>
<td valign="top" width="491">
<p align="left">Le ressort des cours d'appel de Basse-Terre, Cayenne et Fort-de-France. </p>
</td>
</tr>
<tr>
<td valign="top" width="227">
<p align="left">Lyon.</p>
</td>
<td valign="top" width="491">
<p align="left">Le ressort des cours d'appel de Chambéry, Grenoble, Lyon et Riom.</p>
</td>
</tr>
<tr>
<td valign="top" width="227">
<p align="left">Nancy.</p>
</td>
<td valign="top" width="491">
<p align="left">Le ressort des cours d'appel de Besançon, Colmar, Dijon, Metz et Nancy.</p>
</td>
</tr>
<tr>
<td valign="top" width="227">
<p align="left">Paris.</p>
</td>
<td valign="top" width="491">
<p align="left">Le ressort des cours d'appel de Bourges, Paris, Orléans, Saint-Denis-de-la-Réunion et Versailles.</p>
</td>
</tr>
<tr>
<td valign="top" width="227">
<p align="left">Rennes.</p>
</td>
<td valign="top" width="491">
<p align="left">Le ressort des cours d'appel d'Angers, Caen, Poitiers et Rennes.</p>
</td>
</tr>
</tbody>
</table>
