# Article Annexe 6-2

Juridictions compétentes par département en métropole pour connaître, en application de l'article L. 610-1,

des procédures applicables aux personnes qui ne sont ni commerçants ni artisans

<table>
<tbody>
<tr>
<td width="229">
<p align="center">DÉPARTEMENT</p>
</td>
<td width="221">
<p align="center">JURIDICTION</p>
</td>
<td width="220">
<p align="center">RESSORT</p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Ain </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Bourg-en-Bresse </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="3" valign="top" width="229">
<p align="left">Aisne </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Laon </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Saint-Quentin </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Soissons </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Allier </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Cusset </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Montluçon </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Alpes-de-Haute-Provence </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Digne-les-Bains </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Alpes (Hautes-) </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Gap </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Alpes-Maritimes </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Grasse </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Nice </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Ardèche </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Privas </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Ardennes </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Charleville-Mézières </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Ariège </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Foix </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Aube </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Troyes </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Aude </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Carcassonne </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Narbonne </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Aveyron </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Rodez </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="3" valign="top" width="229">
<p align="left">Bouches-du-Rhône </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Aix-en-Provence </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Marseille </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Tarascon </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Calvados </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Caen </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Lisieux </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Cantal </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Aurillac </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Charente </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Angoulême </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Charente-Maritime </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de la Rochelle </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Saintes </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Cher </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Bourges </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Corrèze </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Brive-la-Gaillarde </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Corse-du-Sud </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Ajaccio </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Corse (Haute-) </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Bastia </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Côte-d'Or </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Dijon </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Côtes-d'Armor </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Saint-Brieuc </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Creuse </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Guéret </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Dordogne </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Bergerac </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Périgueux </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Doubs </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Besançon </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Montbéliard </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Drôme </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Valence </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Essonne </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Evry </p>
</td>
<td valign="top" width="220">
<p align="left">Le département à l'exception de l'emprise de l'aérodrome de Paris-Orly </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Eure </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Evreux </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Eure-et-Loir </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Chartres </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Finistère </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Brest </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Quimper </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Gard </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Alès </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Nîmes </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Garonne (Haute-) </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Toulouse </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Gers </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Auch </p>
</td>
<td valign="top" width="220">
<p align="left">Le département, à l'exception de l'emprise de l'aérodrome d'Aire-sur-l'Adour </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Gironde </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Bordeaux </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Libourne </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Hérault </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Béziers </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Montpellier </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Ille-et-Vilaine </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Rennes </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Saint-Malo </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Indre </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Châteauroux </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Indre-et-Loire </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Tours </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Isère </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Grenoble </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Vienne </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Jura </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Lons-le-Saunier </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Landes </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Dax </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Mont-de-Marsan </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI, et l'emprise de l'aérodrome de l'Aire-sur-l'Adour </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Loir-et-Cher </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Blois </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Loire </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Roanne </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Saint-Etienne </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Loire (Haute-) </p>
</td>
<td valign="top" width="221">
<p align="left">TGI du Puy-en-Velay </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Loire-Atlantique </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Nantes </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Saint-Nazaire </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Loiret </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Montargis </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI d'Orléans </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Lot </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Cahors </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Lot-et-Garonne </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Agen </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Lozère </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Mende </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Maine-et-Loire </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Angers </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Manche </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Cherbourg-Octeville </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Coutances </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Marne </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Châlons-en-Champagne </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Reims </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Marne (Haute-)</p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Chaumont</p>
</td>
<td valign="top" width="220">
<p align="left">Le département</p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Mayenne </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Laval </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Meurthe-et-Moselle </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Briey </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Nancy </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Meuse </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Bar-le-Duc </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Verdun </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Morbihan </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Lorient </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Vannes </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td rowspan="3" valign="top" width="229">
<p align="left">Moselle </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Metz </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Sarreguemines </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Thionville </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Nièvre </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Nevers </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="6" valign="top" width="229">
<p align="left">Nord </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Avesnes-sur-Helpe </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Cambrai </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Douai </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Dunkerque </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Lille </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Valenciennes </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td rowspan="3" valign="top" width="229">
<p align="left">Oise </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Beauvais </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Compiègne </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Senlis </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Orne </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Alençon </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI d'Argentan </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Paris </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Paris </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="4" valign="top" width="229">
<p align="left">Pas-de-Calais </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Arras </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Béthune </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Boulogne-sur-Mer </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Saint-Omer </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Puy-de-Dôme </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Clermont-Ferrand </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Pyrénées-Atlantiques </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Bayonne </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Pau </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Pyrénées (Hautes-) </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Tarbes </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Pyrénées-Orientales </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Perpignan </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Rhin (Bas-) </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Saverne </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Strasbourg </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Rhin (Haut-) </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Colmar </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Mulhouse </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Rhône </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Lyon </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Villefranche-sur-Saône </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Saône (Haute-) </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Vesoul </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Saône-et-Loire </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Chalon-sur-Saône </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Mâcon </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Sarthe </p>
</td>
<td valign="top" width="221">
<p align="left">TGI du Mans </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Savoie </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Albertville </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Chambéry </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td rowspan="3" valign="top" width="229">
<p align="left">Savoie (Haute-) </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Annecy </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Bonneville </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Thonon-les-Bains </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Seine (Hauts-de-) </p>
</td>
<td valign="top" width="221">
<p align="left">TGI Nanterre </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="3" valign="top" width="229">
<p align="left">Seine-Maritime </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Dieppe </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI du Havre </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Rouen </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td rowspan="3" valign="top" width="229">
<p align="left">Seine-et-Marne </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Fontainebleau </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Meaux </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI, à l'exception de l'emprise de l'aérodrome de Roissy -Charles-de-Gaulle </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Melun </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Seine-Saint-Denis </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Bobigny </p>
</td>
<td valign="top" width="220">
<p align="left">Le département et l'emprise des aérodromes de Paris-Le Bourget et de Roissy -Charles-de-Gaulle </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Sèvres (Deux-) </p>
</td>
<td valign="top" width="221">
<p align="left">TGI Niort </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Somme </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Amiens </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Tarn </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Albi </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Castres </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Tarn-et-Garonne </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Montauban </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Territoire de Belfort </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Belfort </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Val-de-Marne </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Créteil </p>
</td>
<td valign="top" width="220">
<p align="left">Le département et l'emprise de l'aérodrome de Paris-Orly </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Val-d'Oise </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Pontoise </p>
</td>
<td valign="top" width="220">
<p align="left">Le département, à l'exception de l'emprise des aérodromes de Paris-Le Bourget et de Roissy -Charles-de-Gaulle </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Var </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Draguignan </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Toulon</p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI</p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Vaucluse </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Avignon </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Carpentras</p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI</p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Vendée </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de La Roche-sur-Yon </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<br/>
</td>
<td valign="top" width="221">
<p align="left">TGI des Sables-d'Olonne</p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI</p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Vienne </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Poitiers </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Vienne (Haute-) </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Limoges </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Vosges </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Epinal </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="229">
<p align="left">Yonne </p>
</td>
<td valign="top" width="221">
<p align="left">TGI d'Auxerre </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="221">
<p align="left">TGI de Sens </p>
</td>
<td valign="top" width="220">
<p align="left">Ressort du TGI </p>
</td>
</tr>
<tr>
<td valign="top" width="229">
<p align="left">Yvelines </p>
</td>
<td valign="top" width="221">
<p align="left">TGI de Versailles </p>
</td>
<td valign="top" width="220">
<p align="left">Le département </p>
</td>
</tr>
</tbody>
</table>
