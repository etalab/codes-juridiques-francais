# Article Annexe 7-5

<div align="center">
<p>Emoluments des greffiers des tribunaux de commerce </p>
<p>TABLEAU I ANNEXÉ À L'ARTICLE R. 743-140 <br/>
</p>
</div>

ACTES JUDICIAIRES (1)

<table>
<thead>
<tr>
<td width="91">
<p align="center">NUMÉROS </p>
</td>
<td width="260">
<br/>
<p align="center">NATURE DES ACTES </p>
</td>
<td width="104">
<br/>
<p align="center">ÉMOLUMENTS </p>
<p align="center">taux de base (2) </p>
</td>
</tr>
</thead>
<tbody>
<tr>
<td valign="top">
<p align="center">101 </p>
</td>
<td valign="top">
<p align="left">Acte de greffe. </p>
</td>
<td valign="top">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">102 </p>
</td>
<td valign="top">
<p align="left">Certificat. </p>
</td>
<td valign="top">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">103 </p>
</td>
<td valign="top">
<p>Commission rogatoire : envoi et exécution. </p>
</td>
<td valign="top">
<p align="center">5 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">104 </p>
</td>
<td valign="top">
<p align="left">Contredit sur la compétence. </p>
</td>
<td valign="top">
<p align="center">7 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">105 </p>
</td>
<td valign="top">
<p>Copie. </p>
</td>
<td valign="top">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">106 </p>
</td>
<td valign="top">
<p align="left">Vérification de dépens. </p>
</td>
<td valign="top">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Copie certifiée conforme </p>
<p align="center">(en dehors de toute procédure) </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">107 </p>
</td>
<td valign="top">
<p align="left">Jugement. </p>
</td>
<td valign="top">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">108 </p>
</td>
<td valign="top">
<p>Ordonnance. </p>
</td>
<td valign="top">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">109 </p>
</td>
<td valign="top">
<p align="left">Seconde copie certifiée conforme revêtue de la formule exécutoire. </p>
</td>
<td valign="top">
<p align="center">3 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Injonction de payer (procédure d') </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">110 </p>
</td>
<td valign="top">
<p align="left">Ordonnance d'injonction de payer. </p>
</td>
<td valign="top">
<p align="center">9 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">111 </p>
</td>
<td valign="top">
<p>Forfait de transmission des ordonnances d'injonction de payer. </p>
</td>
<td valign="top">
<p align="center">7 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">112 </p>
</td>
<td valign="top">
<p align="left">Diligences relatives à l'ordonnance, y compris l'extrait d'immatriculation (K bis ou L bis) ou un certificat de non-inscription, réception et conservation de la requête. </p>
</td>
<td valign="top">
<p align="center">9 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">113 </p>
</td>
<td valign="top">
<p align="left">Opposition à injonction de payer. </p>
</td>
<td valign="top">
<p align="center">9 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Jugements </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">114 </p>
</td>
<td valign="top">
<p align="left">Jugement (enrôlement, tenue des audiences, mise en forme, avis aux parties), quel que soit le nombre de renvois, pour deux parties. </p>
</td>
<td valign="top">
<p align="center">25 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">115 </p>
</td>
<td valign="top">
<p align="left">Par partie supplémentaire. </p>
</td>
<td valign="top">
<p align="center">5 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">116 </p>
</td>
<td valign="top">
<p align="left">Jugement : forfait de transmission par partie. </p>
</td>
<td valign="top">
<p align="center">10 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Instruction avant jugement </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">117 </p>
</td>
<td valign="top">
<p align="left">Procédure devant un juge rapporteur. </p>
</td>
<td valign="top">
<p align="center">7 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">118 </p>
</td>
<td valign="top">
<p align="left">Contrat ou calendrier de procédure. </p>
</td>
<td valign="top">
<p align="center">7 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">119 </p>
</td>
<td valign="top">
<p align="left">Ordonnances autres que référés et injonctions de payer. </p>
</td>
<td valign="top">
<p align="center">6 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">120 </p>
</td>
<td valign="top">
<p>Prestation de serment. </p>
</td>
<td valign="top">
<p align="center">3 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Référés </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">121 </p>
</td>
<td valign="top">
<p align="left">Ordonnance de référé (enrôlement, tenue des audiences, mise en forme, avis aux parties), quel que soit le nombre de renvois pour deux parties. </p>
</td>
<td valign="top">
<p align="center">15 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">122 </p>
</td>
<td valign="top">
<p>Par partie supplémentaire. </p>
</td>
<td valign="top">
<p align="center">5 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">123 </p>
</td>
<td valign="top">
<p align="left">Ordonnance de référé : forfait de transmission par partie. </p>
</td>
<td valign="top">
<p align="center">7,70 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">124 </p>
</td>
<td valign="top">
<p>Registres de commerce (saisine en matière de contentieux des). </p>
</td>
<td valign="top">
<p align="center">8 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">125 </p>
</td>
<td valign="top">
<p align="left">Diligences liées à l'expertise. </p>
</td>
<td valign="top">
<p align="center">15 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">126 </p>
</td>
<td valign="top">
<p>Convocation ou avis. </p>
</td>
<td valign="top">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">127 </p>
</td>
<td valign="top">
<p align="left">Visa, cote et paraphe des livres. </p>
</td>
<td valign="top">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Procédures ouvertes après le 1er janvier 2006 en application du livre VI du code de commerce </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">160 </p>
</td>
<td valign="top">
<p align="left">Diligences en matière d'enquête en application des articles L 621-1, alinéa 3, et L 651-4 du code de commerce, non compris le coût de la délivrance des copies ou extraits et des avis, notifications, convocations et communications. </p>
</td>
<td valign="top">
<p align="center">10 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">161 </p>
</td>
<td valign="top">
<p align="left">Réception de la demande de mandat ad hoc, de conciliation, de sauvegarde, de redressement judiciaire et de liquidations judiciaires, conformément aux articles R. 611-18, R. 611-22, R. 621-1, R. 631-1 et R. 640-1, non compris le coût de la délivrance des copies ou extraits. </p>
</td>
<td valign="top">
<p align="center">6 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">162 </p>
</td>
<td valign="top">
<p align="left">Diligences en cas de saisine d'office ou à la requête du procureur de la République, non compris le coût de la délivrance des copies ou extraits. </p>
</td>
<td valign="top">
<p align="center">3 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">163 </p>
</td>
<td valign="top">
<p>Convocation devant le juge-commissaire. </p>
</td>
<td valign="top">
<p align="center">3 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">164 </p>
</td>
<td valign="top">
<p align="left">Convocation devant le président du tribunal (mandat ad hoc, conciliation : R. 611-19, R. 611-23 du code de commerce) ou le tribunal. </p>
</td>
<td valign="top">
<br/>
<p align="center">3 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">165 </p>
</td>
<td valign="top">
<p>Avis au créancier en matière d'admission de créances sans débat contradictoire </p>
</td>
<td valign="top">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">166 </p>
</td>
<td valign="top">
<p align="left">Ordonnances du juge-commissaire après débat contradictoire. </p>
</td>
<td valign="top">
<p align="center">6 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">167 </p>
</td>
<td valign="top">
<p>Diligences relatives à la notification des jugements et des requêtes, aux significations et aux convocations par voie d'huissier. </p>
</td>
<td valign="top">
<p align="center">6 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">168 </p>
</td>
<td valign="top">
<p align="left">Mention sur l'état des créances. </p>
</td>
<td valign="top">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td align="left" valign="top">
<p align="center">169 </p>
</td>
<td align="left" valign="top">
<p align="left">Dépôt et conservation des documents, actes ou pièces, y inclus procès-verbal et certificat de dépôt ou reçus de déclaration. </p>
</td>
<td align="left" valign="top">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">170 </p>
</td>
<td valign="top">
<p>Extrait établi en vue des mesures de publicité. </p>
</td>
<td valign="top">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<p align="left">(1) a) Les émoluments ainsi alloués comprennent le coût d'une copie certifiée conforme, revêtue de la formule exécutoire et d'une copie délivrée à chaque partie, </p>
<p align="left">b) En cas de radiation avant le prononcé d'un jugement ou d'une ordonnance, les deux tiers de l'émolument sont alloués au greffier, sauf dans le cas où un émolument a été spécialement perçu pour la saisine du tribunal, </p>
<p align="left">c) Les redevances perçues en matière commerciale au profit du Trésor par les greffiers en chef des tribunaux de grande instance sont calculées conformément aux dispositions ci-dessus. </p>
<p align="left">(2) Voir l'article 743-142. </p>
</td>
</tr>
</tbody>
</table>

TABLEAU II ANNEXÉ À L'ARTICLE R. 743-140

Registre du commerce et des sociétés

Registre des agents commerciaux

<table>
<thead>
<tr>
<td width="88">
<p align="center">NUMÉROS </p>
</td>
<td width="265">
<p align="center">NATURE DES ACTES </p>
</td>
<td width="101">
<p align="center">ÉMOLUMENTS (taux de base) </p>
</td>
</tr>
</thead>
<tbody>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">A.-Registre du commerce et des sociétés (1) </p>
<p align="center">Immatriculation principale, immatriculation secondaire, inscription complémentaire (2) </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">201 </p>
</td>
<td valign="top">
<p align="left">Personne physique. </p>
</td>
<td valign="top">
<p align="center">36 </p>
</td>
</tr>
<tr>
<td>
<p align="center">201 bis </p>
</td>
<td>
<p align="left">Immatriculation principale par création d'une entreprise, personne physique </p>
</td>
<td>
<p align="center">18</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">202 </p>
</td>
<td valign="top">
<p align="left">Personne morale : groupements d'intérêt économique, sociétés commerciales, sociétés non commerciales, établissements publics. </p>
</td>
<td valign="top">
<p align="center">44 </p>
</td>
</tr>
<tr>
<td>
<p align="center">202 bis</p>
</td>
<td>
<p align="left">Immatriculation principale par création de sociétés commerciales </p>
</td>
<td>
<p align="center">22</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">203 </p>
</td>
<td valign="top">
<p align="left">Inscriptions modificatives (3) : personne physique. </p>
</td>
<td valign="top">
<p align="center">32 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">204 </p>
</td>
<td valign="top">
<p align="left">Inscriptions modificatives et mentions d'office sous réserve des cas prévus par l'article R. 143-145 (3) : personne morale (groupements d'intérêt économique, sociétés commerciales, sociétés non commerciales, établissements publics). </p>
</td>
<td valign="top">
<p align="center">42 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">205 </p>
</td>
<td valign="top">
<p align="left">Diligences spécifiques en cas de transformation de sociétés. </p>
</td>
<td valign="top">
<p align="center">15 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">206 </p>
</td>
<td valign="top">
<p align="left">Mise à jour des renseignements figurant dans les immatriculations principales aux immatriculations secondaires et dans les immatriculations secondaires aux immatriculations principales de personnes physiques. </p>
</td>
<td valign="top">
<p align="center">18 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">207 </p>
</td>
<td valign="top">
<p align="left">Mise à jour des renseignements figurant dans les immatriculations principales aux immatriculations secondaires et dans les immatriculations secondaires aux immatriculations principales des personnes morales. </p>
</td>
<td valign="top">
<p align="center">25 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">208 </p>
</td>
<td valign="top">
<p align="left">Notification des mises à jour des immatriculations principales et secondaires concernant les personnes physiques. </p>
</td>
<td valign="top">
<p align="center">6 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">209 </p>
</td>
<td valign="top">
<p align="left">Notification des mises à jour des immatriculations principales et secondaires concernant les personnes morales. </p>
</td>
<td valign="top">
<p align="center">8 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">210 </p>
</td>
<td valign="top">
<p align="left">Dépôt des comptes annuels. </p>
</td>
<td valign="top">
<p align="center">5 </p>
</td>
</tr>
<tr>
<td align="center">210  bis<br/>
</td>
<td>
<br/>
<p>Dépôt des comptes annuels assortis d'une déclaration de confidentialité </p>
<br/>
</td>
<td align="center"> 5</td>
</tr>
<tr>
<td valign="top">
<p align="center">211 </p>
</td>
<td valign="top">
<p align="left">Dépôt d'actes ou de pièces pour la publicité des sociétés (4), y inclus le certificat de dépôt. </p>
</td>
<td valign="top">
<p align="center">6 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">212 </p>
</td>
<td valign="top">
<p align="left">Certificat négatif d'immatriculation, communication d'actes ou de pièces déposées. </p>
</td>
<td valign="top">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td>
<p align="center"> 212 bis</p>
<br/>
</td>
<td align="center" valign="middle">
<div align="left">Certificat attestant que les comptes annuels ont été déposés </div>
<p align="left">mais ne sont pas rendus publics.</p>
</td>
<td align="center"> 1</td>
</tr>
<tr>
<td valign="top">
<p align="center">213 </p>
</td>
<td valign="top">
<p align="left">Extrait du registre du commerce et des sociétés (5). </p>
</td>
<td valign="top">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">214 </p>
</td>
<td valign="top">
<p align="left">Relevé historique des événements au registre du commerce et des sociétés. </p>
</td>
<td valign="top">
<p align="center">5 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">215 </p>
</td>
<td valign="top">
<p>Copie des comptes et rapports annuels (forfait, quel que soit le nombre de page). </p>
</td>
<td valign="top">
<p align="center">6 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">216 </p>
</td>
<td valign="top">
<p align="left">Copie certifiée conforme (par page). </p>
</td>
<td valign="top">
<p align="center">0,33 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">217 </p>
</td>
<td valign="top">
<p align="left">Copie de statuts, actes ou de pièces déposées (forfait). </p>
</td>
<td valign="top">
<p align="center">6 </p>
</td>
</tr>
<tr>
<td align="center">
<p>217 bis </p>
<br/>
</td>
<td>Copie de la déclaration de confidentialité des comptes annuels </td>
<td align="center"> 1</td>
</tr>
<tr>
<td valign="top">
<p align="center">218 </p>
</td>
<td valign="top">
<p align="left">Diligences de transmission de la formalité à l'INPI. </p>
</td>
<td valign="top">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<p align="center">B.-Registre des agents commerciaux </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">221 </p>
</td>
<td valign="top">
<p align="left">Immatriculation (6) comprenant les émoluments de radiation. </p>
</td>
<td valign="top">
<p align="center">6 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">222 </p>
</td>
<td valign="top">
<p align="left">Inscription modificative (6). </p>
</td>
<td valign="top">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">223 </p>
</td>
<td valign="top">
<p align="left">Extrait d'inscription de la déclaration. </p>
</td>
<td valign="top">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td colspan="3">
<div align="center">C.-Registre du commerce et des sociétés </div>
<p align="center">Dépôt effectué par les entrepreneurs individuels à responsabilité limitée </p>
</td>
</tr>
<tr>
<td align="center">230 </td>
<td>
<p align="left">Dépôt de la déclaration d'affectation du patrimoine, de reprise ou de transfert (7) et mentions RCS (8) (9) (art. L. 526-7, L. 526-16 et L. 526-17-II) <br/>
</p>
</td>
<td align="center">32 </td>
</tr>
<tr>
<td align="center">231 <br/>
</td>
<td>
<p align="left">Dépôt de la déclaration modificative ou complémentaire de la déclaration d'affectation du patrimoine, entraînant mentions au RCS (10) <br/>
</p>
</td>
<td align="center">28 <br/>
</td>
</tr>
<tr>
<td align="center">232 </td>
<td>
<p align="left">Dépôt de la déclaration complémentaire d'affectation du patrimoine (art. L. 526-9, L. 526-10 et L. 526-11) ou des actes ou décisions de modification sans mentions RCS (10) <br/>
</p>
</td>
<td align="center">14 </td>
</tr>
<tr>
<td align="center">233 </td>
<td>
<p align="left">Dépôt des comptes annuels au RCS ou du document comptable simplifié (art. L. 526-14) <br/>
</p>
</td>
<td align="center">5 </td>
</tr>
<tr>
<td align="center">234 </td>
<td>
<p align="left">Notification à un autre registre en cas de double immatriculation ou d'immatriculation secondaire <br/>
</p>
</td>
<td align="center">6 </td>
</tr>
<tr>
<td align="center">235 </td>
<td>
<p align="left">Mise à jour des renseignements figurant dans les immatriculations principales aux immatriculations secondaires ou reçus d'un autre registre ou répertoire aux fins de mentions <br/>
</p>
</td>
<td align="center">18 </td>
</tr>
<tr>
<td align="center">236 </td>
<td>
<p align="left">Avis au BODACC relatif à la cession, y compris la délivrance du certificat (art. L. 526-17) <br/>
</p>
</td>
<td align="center">7 </td>
</tr>
<tr>
<td align="center">237 </td>
<td>
<p align="left">Copie des comptes annuels ou du document comptable simplifié ou de la déclaration d'affectation <br/>
</p>
</td>
<td align="center">6 </td>
</tr>
<tr>
<td colspan="3">
<div align="center">D.-Registre des agents commerciaux </div>
<p align="center">Dépôt effectué par les entrepreneurs individuels à responsabilité limitée <br/>
</p>
</td>
</tr>
<tr>
<td align="center">240 <br/>
</td>
<td>
<p align="left">Dépôt de la déclaration d'affectation du patrimoine, de reprise ou de transfert (7) et mentions au RSAC (8) (art. L. 526-7, L. 526-16 et L. 526-17-II) </p>
</td>
<td align="center">32 <br/>
</td>
</tr>
<tr>
<td align="center">241 <br/>
</td>
<td>
<p align="left">Dépôt de la déclaration modificative ou complémentaire de la déclaration d'affectation du patrimoine entraînant mentions au RSAC (11) </p>
</td>
<td align="center">28 <br/>
</td>
</tr>
<tr>
<td align="center">242 <br/>
</td>
<td>
<p align="left">Dépôt de la déclaration complémentaire d'affectation du patrimoine (art. L. 526-9, L. 526-10 et L. 526-11) ou des actes ou décisions de modification sans mentions au RSAC (11) </p>
</td>
<td align="center">14 <br/>
</td>
</tr>
<tr>
<td align="center">243 </td>
<td>
<p align="left">Dépôt des comptes annuels ou du document comptable simplifié (art. L. 526-14) <br/>
</p>
</td>
<td align="center">5 </td>
</tr>
<tr>
<td align="center">244 </td>
<td>
<p align="left">Avis au BODACC relatif à la cession, y compris la délivrance du certificat (art. L. 526-17) <br/>
</p>
</td>
<td align="center">7 </td>
</tr>
<tr>
<td align="center">245 </td>
<td>
<p align="left">Copie des comptes annuels ou du document comptable simplifié ou de la déclaration d'affectation <br/>
</p>
</td>
<td align="center">6 </td>
</tr>
<tr>
<td colspan="3">
<div align="center">E.-Registre des entrepreneurs individuels </div>
<p align="center">à responsabilité limitée visés au 3° de l'article L. 526-7 </p>
</td>
</tr>
<tr>
<td align="center">250 </td>
<td>
<p align="left">Immatriculation y compris après reprise ou transfert (7) (art. L. 526-7, L. 526-16 et L. 526-17-II) comprenant le dépôt de la déclaration d'affectation du patrimoine et les émoluments de radiation </p>
</td>
<td align="center">36 </td>
</tr>
<tr>
<td align="center">251 </td>
<td>
<p align="left">Dépôt de la déclaration modificative ou complémentaire de la déclaration d'affectation du patrimoine, entraînant mentions au registre (11) <br/>
</p>
</td>
<td align="center">28 </td>
</tr>
<tr>
<td align="center">252 </td>
<td>
<p align="left">Dépôt de la déclaration complémentaire d'affectation du patrimoine (art. L. 526-9, L. 526-10 et L. 526-11) ou des actes ou décisions de modification sans mentions au registre (11) </p>
</td>
<td align="center">14 </td>
</tr>
<tr>
<td align="center">253 </td>
<td>
<p align="left">Dépôt des comptes annuels ou du document comptable simplifié (art. L. 526-14) <br/>
</p>
</td>
<td align="center">5 </td>
</tr>
<tr>
<td align="center">
<p align="center">254 <br/>
</p>
</td>
<td>
<p align="left">Avis au BODACC relatif à la cession, y compris la délivrance du certificat (art. L. 526-17) </p>
</td>
<td align="center">7 <br/>
</td>
</tr>
<tr>
<td align="center">255 </td>
<td>
<p align="left">Copie des comptes annuels ou du document comptable simplifié ou de la déclaration d'affectation <br/>
</p>
</td>
<td align="center">6 </td>
</tr>
<tr>
<td align="center">
<p align="center">256 <br/>
</p>
</td>
<td>
<p align="left">Extrait du registre des entrepreneurs individuels à responsabilité limitée </p>
</td>
<td align="center">2 <br/>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<p align="left">(1) Le greffier réclame distinctement le montant des taxes perçues pour le compte de l'Institut national de la propriété industrielle et des frais d'insertion au BODACC. </p>
<p align="left">(2) Cet émolument rémunère forfaitairement l'ensemble des formalités liées à l'immatriculation principale, l'immatriculation secondaire ou l'inscription complémentaire et inclut le coût de la radiation. Il inclut également le coût de la délivrance au requérant de cinq extraits, en ce qui concerne l'immatriculation principale, l'immatriculation secondaire ou l'inscription complémentaire, et de quatre extraits, en ce qui concerne la radiation, ainsi que celui des frais postaux. Lorsque l'immatriculation est effectuée en application des articles R. 743-162 et R. 743-168 du code de commerce, les émoluments ainsi prévus sont fixés à neuf taux de base, en ce qui concerne l'immatriculation principale et l'immatriculation secondaire, y compris les frais postaux. </p>
<p align="left">(3) Cet émolument rémunère forfaitairement l'ensemble des formalités liées à l'inscription modificative. Il inclut le coût de la délivrance au requérant de quatre extraits et celui des frais postaux. </p>
<p align="left">(4) Pour la publicité des sociétés, il n'est perçu qu'un émolument, quel que soit le nombre des actes et des pièces déposés simultanément par un même intéressé. </p>
<p align="left">(5) Il s'agit des extraits K bis et L bis délivrés aux tiers ou des extraits à délivrer, en plus des extraits compris dans le forfait, à la personne assujettie, sur leur demande écrite. L'ensemble de ces demandes est répertorié au greffe. </p>
<p align="left">(6) Lorsque l'immatriculation ou la radiation est effectuée en application des articles R. 743-162 et R. 743-168 du code de commerce, les émoluments ainsi prévus sont réduits de moitié. </p>
<p align="left">(7) Hors coût de l'insertion au BODACC. </p>
<p align="left">(8) Il n'est perçu aucun émolument pour le dépôt de la déclaration d'affectation du patrimoine lorsqu'il intervient simultanément à la demande d'immatriculation (art. L. 526-19). </p>
<p align="left">(9) Y compris la transmission à l'INPI. </p>
<p align="left">(10) Y compris la transmission des documents visés à l'article R. 123-121-3 au service des impôts et la transmission à l'INPI. </p>
<p align="left">(11) Y compris la transmission des documents visés à l'article R. 123-121-3 au service des impôts. </p>
</td>
</tr>
</tbody>
</table>

TABLEAU III ANNEXÉ À L'ARTICLE R. 743-140

Privilèges et sûretés

<table>
<thead>
<tr>
<td width="91">
<p align="center">NUMÉROS </p>
</td>
<td width="273">
<p align="center">NATURE DES ACTES </p>
</td>
<td width="91">
<p align="center">ÉMOLUMENTS </p>
<p align="center">(taux de base) </p>
</td>
</tr>
</thead>
<tbody>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">A.-Privilège du Trésor en matière fiscale </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">301 </p>
</td>
<td valign="top">
<p align="left">Première inscription, radiation totale ou partielle d'une inscription non périmée. </p>
</td>
<td valign="top">
<p align="center">1,5 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">302 </p>
</td>
<td valign="top">
<p align="left">Inscription suivante, renouvellement d'une inscription ou subrogation. </p>
</td>
<td valign="top">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">303 </p>
</td>
<td valign="top">
<p align="left">Délivrance d'un état d'inscription positif ou négatif, quel que soit le nombre d'inscriptions révélées. </p>
</td>
<td valign="top">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">304 </p>
</td>
<td valign="top">
<p align="left">Mention d'une contestation en marge d'une inscription. </p>
</td>
<td valign="top">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">B.-Privilège de la sécurité sociale et des régimes complémentaires </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Inscription, radiation totale d'une inscription non périmée </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">310 </p>
</td>
<td valign="top">
<p align="left">Montant des sommes privilégiées inférieur à 16 000 taux de base. </p>
</td>
<td valign="top">
<p align="center">7 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">311 </p>
</td>
<td valign="top">
<p align="left">Montant des sommes privilégiées supérieur ou égal à 16 000 taux de base. </p>
</td>
<td valign="top">
<p align="center">31 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Radiation partielle d'une inscription non périmée </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">312 </p>
</td>
<td valign="top">
<p align="left">Montant des sommes privilégiées inférieur à 16 000 taux de base. </p>
</td>
<td valign="top">
<p align="center">7 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">313 </p>
</td>
<td valign="top">
<p align="left">Montant des sommes privilégiées supérieur ou égal à 16 000 taux de base. </p>
</td>
<td valign="top">
<p align="center">31 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Renouvellement d'une inscription, subrogation </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">314 </p>
</td>
<td valign="top">
<p align="left">Montant des sommes privilégiées inférieur à 16 000 taux de base. </p>
</td>
<td valign="top">
<p align="center">5 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">315 </p>
</td>
<td valign="top">
<p align="left">Montant des sommes privilégiées supérieur ou égal à 16 000 taux de base. </p>
</td>
<td valign="top">
<p align="center">16 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">316 </p>
</td>
<td valign="top">
<p align="left">Mention d'une saisie en marge des différentes inscriptions concernant un même débiteur, radiation partielle ou totale de ces inscriptions </p>
</td>
<td valign="top">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">317 </p>
</td>
<td valign="top">
<p align="left">Délivrance d'un état d'inscription positif ou négatif, quel que soit le nombre d'inscriptions révélées. </p>
</td>
<td valign="top">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">318 </p>
</td>
<td valign="top">
<p align="left">Délivrance d'un certificat de subrogation, de mention de saisie, de radiation de cette mention, de radiation d'inscription. </p>
</td>
<td valign="top">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">C.-Vente et nantissement des fonds de commerce </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Inscription, y compris radiation totale d'une inscription non périmée </p>
<p align="center">(montant de la somme garantie) </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">320 </p>
</td>
<td valign="top">
<p align="left">Montant inférieur à 16 000 taux de base. </p>
</td>
<td valign="top">
<p align="center">14 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">321 </p>
</td>
<td valign="top">
<p align="left">Montant supérieur ou égal à 16 000 et inférieur à 32 000 taux de base. </p>
</td>
<td valign="top">
<p align="center">62 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">322 </p>
</td>
<td valign="top">
<p>Montant supérieur ou égal à 32 000 taux de base. </p>
</td>
<td valign="top">
<p align="center">93 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Radiation partielle d'une inscription non périmée </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">323 </p>
</td>
<td valign="top">
<p align="left">Montant inférieur à 16 000 taux de base. </p>
</td>
<td valign="top">
<p align="center">7 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">324 </p>
</td>
<td valign="top">
<p align="left">Montant supérieur ou égal à 16 000 taux de base. </p>
</td>
<td valign="top">
<p align="center">31 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Mention d'antériorité ou de subrogation, renouvellement d'inscription </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">325 </p>
</td>
<td valign="top">
<p align="left">Montant inférieur à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">5 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">326 </p>
</td>
<td valign="top">
<p align="left">Montant supérieur ou égal à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">16 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">327 </p>
</td>
<td valign="top">
<p align="left">Procès-verbal de dépôt, certificat de dépôt et certificat constatant une transcription, une cession d'antériorité ou de radiation. Pour l'ensemble de ces formalités. </p>
</td>
<td valign="top">
<br/>
<p align="center">3 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">328 </p>
</td>
<td valign="top">
<p>Etat d'inscription positif ou négatif (quel que soit le nombre des inscriptions). </p>
</td>
<td valign="top">
<br/>
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">329 </p>
</td>
<td valign="top">
<p align="left">Rédaction de la déclaration de créance et certificat constatant cette déclaration. </p>
</td>
<td valign="top">
<br/>
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">330 </p>
</td>
<td valign="top">
<p align="left">Mention de changement de siège de fonds, certificat d'inscription des ventes, cessions ou nantissements en ce qu'ils s'appliquent aux brevets d'invention et aux licences, aux marques de fabrique et de commerce, aux dessins et modèles industriels. </p>
</td>
<td valign="top">
<br/>
<p align="center">1 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">331 </p>
</td>
<td valign="top">
<p align="left">Délivrance des copies de bordereaux d'inscription et des actes de vente sous seing privé déposés au greffe. </p>
</td>
<td valign="top">
<br/>
<p align="center">1 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">332 </p>
</td>
<td valign="top">
<p align="left">Copie certifiée conforme. </p>
</td>
<td valign="top">
<br/>
<p align="center">2 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">D.-Nantissement d'un fonds agricole ou d'un fonds artisanal </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">340 </p>
</td>
<td valign="top">
<p align="left">Les émoluments alloués aux greffiers sont égaux à ceux prévus pour des actes ou formalités analogues en cas de nantissement de fonds de commerce. </p>
</td>
<td valign="top">
<br/>
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">F.-Nantissement judiciaire </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">350 </p>
</td>
<td valign="top">
<p align="left">Les émoluments alloués aux greffiers sont égaux à ceux prévus pour des actes ou formalités analogues en cas de nantissement de fonds de commerce. </p>
</td>
<td valign="top">
<br/>
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">G.-Gage des stocks </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">360 </p>
</td>
<td valign="top">
<p align="left">Les émoluments alloués aux greffiers sont égaux à ceux prévus pour des actes ou formalités analogues en cas de nantissement de fonds de commerce. </p>
</td>
<td valign="top">
<br/>
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">H.-Nantissement de l'outillage et du matériel </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">370 </p>
</td>
<td valign="top">
<p align="left">Les émoluments alloués aux greffiers sont égaux à ceux prévus pour des actes ou formalités analogues en cas de nantissement de fonds de commerce. </p>
</td>
<td valign="top">
<br/>
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">I.-Gage sur meubles corporels </p>
<p align="center">(article 2338 du code civil) </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Inscription, y compris radiation totale d'une inscription (montant de la somme garantie) </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">380 </p>
</td>
<td valign="top">
<p align="left">Montant inférieur à 6 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">7 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">381 </p>
</td>
<td valign="top">
<p align="left">Montant supérieur ou égal à 6 000 taux de base et inférieur à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">15 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">382 </p>
</td>
<td valign="top">
<p>Montant supérieur ou égal à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">45 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Radiation partielle d'une inscription non périmée </p>
<p align="center">(montant de la somme garantie) </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">383 </p>
</td>
<td valign="top">
<p align="left">Montant inférieur à 6 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">4 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">384 </p>
</td>
<td valign="top">
<p align="left">Montant supérieur ou égal à 6 000 taux de base et inférieur à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">8 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">385 </p>
</td>
<td valign="top">
<p align="left">Montant supérieur ou égal à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">23 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Mention d'antériorité ou de subrogation, renouvellement d'inscription (montant de la somme garantie) </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">386 </p>
</td>
<td valign="top">
<p align="left">Montant inférieur à 6 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">4 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">387 </p>
</td>
<td valign="top">
<p align="left">Montant supérieur ou égal à 6 000 taux de base et inférieur à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">8 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">388 </p>
</td>
<td valign="top">
<p>Montant supérieur ou égal à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">23 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">389 </p>
</td>
<td valign="top">
<p align="left">Procès-verbal de dépôt, certificat de dépôt et certificat constatant une transcription, une cession d'antériorité ou de radiation. Pour l'ensemble de ces formalités. </p>
</td>
<td valign="top">
<br/>
<p align="center">1 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">390 </p>
</td>
<td valign="top">
<p align="left">Etat d'inscription positif ou négatif (quel que soit le nombre des inscriptions). </p>
</td>
<td valign="top">
<br/>
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">391 </p>
</td>
<td valign="top">
<p align="left">Délivrance des copies de bordereaux d'inscription et des actes sous seing privé déposés au greffe. </p>
</td>
<td valign="top">
<br/>
<p align="center">3 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">392 </p>
</td>
<td valign="top">
<p align="left">Copie certifiée conforme. </p>
</td>
<td valign="top">
<br/>
<p align="center">2 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">J.-Warrants (1) (2) </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Etablissement du warrant, y compris radiation </p>
<p align="center">(ensemble le volant, la souche et la transcription du premier endossement) </p>
<p align="center">(montant de la somme prévue dans l'acte) </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">390 </p>
</td>
<td valign="top">
<p align="left">Montant inférieur à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">14 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">391 </p>
</td>
<td valign="top">
<p align="left">Montant supérieur ou égal à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">62 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Radiation partielle </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">392 </p>
</td>
<td valign="top">
<p align="left">Montant inférieur à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">14 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">393 </p>
</td>
<td valign="top">
<p align="left">Montant supérieur ou égal à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">62 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Renouvellement du warrant, inscription d'avis d'escompte </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">394 </p>
</td>
<td valign="top">
<p align="left">Montant inférieur à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">7 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">395 </p>
</td>
<td valign="top">
<p align="left">Montant supérieur ou égal à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">31 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">396 </p>
</td>
<td valign="top">
<p align="left">Délivrance d'un état de transcription, d'un état négatif. </p>
</td>
<td valign="top">
<br/>
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">397 </p>
</td>
<td valign="top">
<p align="left">Certificat de radiation. </p>
</td>
<td valign="top">
<br/>
<p align="center">1 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">398 </p>
</td>
<td valign="top">
<p>Rédaction de lettre recommandée (en cas de formalité obligatoire). </p>
</td>
<td valign="top">
<br/>
<p align="center">0,25 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">K.-Nantissement de parts sociales ou de meubles incorporels </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">399 </p>
</td>
<td valign="top">
<p align="left">Les émoluments alloués aux greffiers sont égaux à ceux qui sont prévus pour des actes ou formalités analogues en cas de nantissement de fonds de commerce. </p>
</td>
<td valign="top">
<br/>
<br/>
<br/>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<p align="left">(1) A l'exclusion des warrants agricoles. </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<p align="left">(2) Il n'est rien dû pour les mentions portées sur le registre des avis et oppositions. </p>
</td>
</tr>
</tbody>
</table>

TABLEAU IV ANNEXÉ À L'ARTICLE R. 743-140

Publicités diverses

<table>
<thead>
<tr>
<td width="91">
<p align="center">NUMÉROS </p>
</td>
<td width="273">
<br/>
<p align="center">NATURE DES ACTES </p>
</td>
<td width="91">
<br/>
<p align="center">ÉMOLUMENTS (taux de base) </p>
</td>
</tr>
</thead>
<tbody>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">A.-Crédit-bail en matière mobilière </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">401 </p>
</td>
<td valign="top">
<p align="left">Inscription principale y compris radiation de cette inscription </p>
</td>
<td valign="top">
<br/>
<p align="center">14 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">402 </p>
</td>
<td valign="top">
<p align="left">Modification de cette inscription. </p>
</td>
<td valign="top">
<br/>
<p align="center">7 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">403 </p>
</td>
<td valign="top">
<p align="left">Report d'inscription (1) (par greffier). </p>
</td>
<td valign="top">
<br/>
<p align="center">3 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">404 </p>
</td>
<td valign="top">
<p align="left">Délivrance de tout état d'inscription (quel que soit le nombre d'inscriptions) positif ou négatif. </p>
</td>
<td valign="top">
<br/>
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">405 </p>
</td>
<td valign="top">
<p align="left">Certificat de radiation. </p>
</td>
<td valign="top">
<br/>
<p align="center">1 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">B.-Contrat de location </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">410 </p>
</td>
<td valign="top">
<p align="left">Inscription principale y compris radiation de cette inscription. </p>
</td>
<td valign="top">
<br/>
<p align="center">14 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">411 </p>
</td>
<td valign="top">
<p align="left">Modification de cette inscription. </p>
</td>
<td valign="top">
<br/>
<p align="center">7 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">412 </p>
</td>
<td valign="top">
<p>Report d'inscription (1) (par greffier). </p>
</td>
<td valign="top">
<br/>
<p align="center">3 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">413 </p>
</td>
<td valign="top">
<p align="left">Délivrance de tout état d'inscription (quel que soit le nombre d'inscriptions) positif ou négatif. </p>
</td>
<td valign="top">
<br/>
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">414 </p>
</td>
<td valign="top">
<p>Certificat de radiation. </p>
</td>
<td valign="top">
<br/>
<p align="center">1 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">C.-Inscription sur le registre spécial des prêts et délais </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">420 </p>
</td>
<td valign="top">
<p align="left">Inscription principale y compris radiation de cette inscription. </p>
</td>
<td valign="top">
<br/>
<p align="center">6 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">421 </p>
</td>
<td valign="top">
<p align="left">Modification de cette inscription. </p>
</td>
<td valign="top">
<br/>
<p align="center">3 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">422 </p>
</td>
<td valign="top">
<p>Report d'inscription (1) (par greffier). </p>
</td>
<td valign="top">
<br/>
<p align="center">3 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">423 </p>
</td>
<td valign="top">
<p align="left">Délivrance de tout état d'inscription (quel que soit le nombre d'inscriptions) positif ou négatif. </p>
</td>
<td valign="top">
<br/>
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">424 </p>
</td>
<td valign="top">
<p align="left">Certificat de radiation. </p>
</td>
<td valign="top">
<br/>
<p align="center">1 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">D.-Clause de réserve de propriété </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">430 </p>
</td>
<td valign="top">
<p align="left">Inscription principale y compris radiation de cette inscription. </p>
</td>
<td valign="top">
<br/>
<p align="center">6 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">431 </p>
</td>
<td valign="top">
<p align="left">Modification de cette inscription. </p>
</td>
<td valign="top">
<br/>
<p align="center">3 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">432 </p>
</td>
<td valign="top">
<p>Report d'inscription (1) (par greffier). </p>
</td>
<td valign="top">
<br/>
<p align="center">3 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">433 </p>
</td>
<td valign="top">
<p align="left">Délivrance de tout état d'inscription (quel que soit le nombre d'inscriptions) positif ou négatif. </p>
</td>
<td valign="top">
<br/>
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">434 </p>
</td>
<td valign="top">
<p align="left">Certificat de radiation. </p>
</td>
<td valign="top">
<br/>
<p align="center">1 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">E.-Clause d'inaliénabilité </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">440 </p>
</td>
<td valign="top">
<p align="left">Inscription principale y compris radiation de cette inscription. </p>
</td>
<td valign="top">
<br/>
<p align="center">15 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">441 </p>
</td>
<td valign="top">
<p align="left">Modification de cette inscription. </p>
</td>
<td valign="top">
<br/>
<p align="center">8 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">442 </p>
</td>
<td valign="top">
<p align="left">Report d'inscription (1) (par greffier). </p>
</td>
<td valign="top">
<br/>
<p align="center">3 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">443 </p>
</td>
<td valign="top">
<p align="left">Délivrance de tout état d'inscription (quel que soit le nombre d'inscriptions) positif ou négatif. </p>
</td>
<td valign="top">
<br/>
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">444 </p>
</td>
<td valign="top">
<p align="left">Certificat de radiation. </p>
</td>
<td valign="top">
<br/>
<p align="center">2 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">F.-Publicité des protêts et des certificats de non-paiement des chèques postaux </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Inscription d'un protêt y compris radiation (2) (montant de la somme inscrit dans l'acte) </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">450 </p>
</td>
<td valign="top">
<p align="left">Montant inférieur à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">7 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">451 </p>
</td>
<td valign="top">
<p align="left">Montant supérieur ou égal à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">31 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">452 </p>
</td>
<td valign="top">
<p align="left">Délivrance d'un extrait de registre des protêts positif ou négatif. </p>
</td>
<td valign="top">
<br/>
<p align="center">2 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">G.-Immatriculation des bateaux de rivière </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Inscription et radiation d'un acte ou jugement translatif, constitutif ou déclaratif de propriété ou de droit réel (3) (montant de la somme inscrit dans l'acte) </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">460 </p>
</td>
<td valign="top">
<p align="left">Montant inférieur à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">7 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">461 </p>
</td>
<td valign="top">
<p align="left">Montant supérieur ou égal à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">31 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Mention de radiation totale ou partielle d'une inscription hypothécaire (montant de la somme inscrit dans l'acte) </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">462 </p>
</td>
<td valign="top">
<p align="left">Montant inférieur à 16 000 taux de base. </p>
</td>
<td valign="top">
<p align="center">
<br/>
</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">463 </p>
</td>
<td valign="top">
<p align="left">Montant supérieur ou égal à 16 000 taux de base. </p>
</td>
<td valign="top">
<p align="center">31 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<p>Mention d'antériorité ou de subrogation, renouvellement d'inscription (sur la valeur de la plus faible inscription faisant l'objet de la subrogation ou du renouvellement) (montant de la somme inscrit dans l'acte) </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">465 </p>
</td>
<td valign="top">
<p align="left">Montant inférieur à 16 000 taux de base. </p>
</td>
<td valign="top">
<p align="center">5 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">466 </p>
</td>
<td valign="top">
<p align="left">Montant supérieur ou égal à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">16 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">467 </p>
</td>
<td valign="top">
<p align="left">Déclarations prévues au troisième alinéa de l'article R. 4124-6 du code des transports, mention des changements de domicile élu. </p>
</td>
<td valign="top">
<br/>
<p align="center">1 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">468 </p>
</td>
<td valign="top">
<p align="left">Acte de déclaration de propriété faite sous serment devant le tribunal de commerce (art. 101 du code du domaine public fluvial et de la navigation intérieure). </p>
</td>
<td valign="top">
<br/>
<p align="center">4 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">469 </p>
</td>
<td valign="top">
<p align="left">Dépôt de procès-verbal de saisie. </p>
</td>
<td valign="top">
<br/>
<p align="center">1 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">470 </p>
</td>
<td valign="top">
<p>Délivrance de tout état d'inscription positif ou négatif (décret du 3 avril 1919). </p>
</td>
<td valign="top">
<br/>
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">471 </p>
</td>
<td valign="top">
<p align="left">Délivrance de tout certificat. </p>
</td>
<td valign="top">
<br/>
<p align="center">1 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">472 </p>
</td>
<td valign="top">
<p>Délivrance des copies de tous actes déposés au greffe (code du domaine public fluvial et de la navigation intérieure). </p>
</td>
<td valign="top">
<br/>
<p align="center">2 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">473 </p>
</td>
<td valign="top">
<p align="left">Formalités consécutives au transfert d'immatriculation au greffier du lieu de l'inscription et au greffier de la nouvelle immatriculation. </p>
</td>
<td valign="top">
<br/>
<p align="center">1 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<p align="left">(1) Il n'est rien perçu, en sus de l'émolument ainsi fixé, pour toute radiation consécutive à un report d'inscription. </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<p align="left">(2) Pour l'ensemble des formalités (réception de la copie du protêt, délivrance d'un récépissé, inscription sur le registre et fichiers, etc.) ainsi que, en ce qui concerne les protêts de chèques, pour la réception et la transmission de la copie destinée au procureur de la République. </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<p align="left">(3) Lorsque l'inscription est requise sur plusieurs bateaux et porte, pour chacun d'eux, sur la totalité de la créance, le droit proportionnel est réduit :</p>
<p align="left">-de moitié pour les deuxième, troisième, quatrième et cinquième bateaux ;</p>
<p>-des deux tiers pour les sixième, septième, huitième, neuvième et dixième bateaux ;</p>
<p align="left">-des trois quarts au-delà du dixième bateau. </p>
</td>
</tr>
</tbody>
</table>

TABLEAU V ANNEXÉ À L'ARTICLE R. 743-140

Propriétés industrielles

<table>
<thead>
<tr>
<td width="91">
<p align="center">NUMÉROS </p>
</td>
<td width="273">
<br/>
<p align="center">NATURE DES ACTES </p>
</td>
<td width="91">
<br/>
<p align="center">ÉMOLUMENTS </p>
</td>
</tr>
</thead>
<tbody>
<tr>
<td valign="top">
<p align="center">501 </p>
</td>
<td valign="top">
<p align="left">Dépôt de dessins et modèles (1). </p>
</td>
<td valign="top">
<p align="center">6 </p>
</td>
</tr>
<tr>
<td colspan="3" valign="top">
<p align="left">(1) Pour l'ensemble des formalités y compris le récépissé de dépôt. </p>
</td>
</tr>
</tbody>
</table>

TABLEAU VI ANNEXÉ À L'ARTICLE R. 743-140

Opérations diverses

<table>
<thead>
<tr>
<td width="91">
<p align="center">NUMÉROS </p>
</td>
<td width="273">
<br/>
<p align="center">NATURE DES ACTES </p>
</td>
<td width="91">
<br/>
<p align="center">ÉMOLUMENTS DACS </p>
</td>
</tr>
</thead>
<tbody>
<tr>
<td colspan="3" valign="top">
<br/>
<p align="center">Séquestre judiciaire </p>
<p align="center">(montant de la somme inscrit dans l'acte) </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">601 </p>
</td>
<td valign="top">
<p align="left">Montant inférieur à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">14 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">602 </p>
</td>
<td valign="top">
<p align="left">Montant supérieur ou égal à 16 000 taux de base. </p>
</td>
<td valign="top">
<br/>
<p align="center">62 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">603 </p>
</td>
<td valign="top">
<p align="left">Rapport de mer. </p>
</td>
<td valign="top">
<br/>
<p align="center">3 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">604 </p>
</td>
<td valign="top">
<p align="left">Avis concernant une déclaration afférente à la vente, à la cession, à l'apport en société, à l'attribution par partage ou par licitation d'un fonds de commerce prévus par l'article R. 123-211, y inclus la délivrance du certificat. </p>
</td>
<td valign="top">
<br/>
<p align="center">7 </p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
<p align="center">605 </p>
</td>
<td valign="top">
<p align="left">Rédaction des avis d'insertion au Bulletin officiel des annonces civiles et commerciales des certificats de dépôt au greffe de comptes annuels et rapport de l'exercice clos. </p>
</td>
<td valign="top">
<br/>
<p align="center">4 </p>
</td>
</tr>
</tbody>
</table>

TABLEAU VII ANNEXÉ À L'ARTICLE R. 743-140

Par exception au principe de la facturation des actes des procédures de sauvegarde et de redressement judiciaire, dont le tarif est fixé par le tableau I, les émoluments et les frais de transmission des procédures de liquidation judiciaire ouvertes hors du cours d'une procédure de sauvegarde ou de redressement judiciaire ou prononcées dans les deux mois de l'ouverture d'une procédure de redressement judiciaire font l'objet d'une tarification forfaitaire fixée dans ce tableau.

Cette tarification forfaitaire ne comprend pas les émoluments, les frais et les débours résultant des actions prévues au titre V du livre VI du code de commerce, dont le tarif est fixé par le tableau I, ainsi que les frais de copies d'actes ou de pièces délivrées aux parties.

Pour l'application des droits forfaitaires, le nombre de salariés et le chiffre d'affaires de l'entreprise concernée sont déterminés conformément aux dispositions de l'article R. 621-11 du code de commerce.

A défaut, ils sont déterminés au vu des données disponibles dans le dossier de la procédure.

Une somme fixée à 200 euros hors taxe à valoir sur les émoluments et frais de transmission est versée dès l'ouverture ou le prononcé des procédures de liquidation judiciaire ci-dessus mentionnées. Le solde est exigible à la date de leur clôture.

Tarification forfaitaire

Emoluments du greffe par débiteur et forfait de transmission

(hors frais d'huissiers, frais relatifs aux journaux d'annonces légales, BODACC)

(Exprimés en taux de base)

<table>
<tbody>
<tr>
<td width="104">
<p align="center">NUMÉROS </p>
</td>
<td width="39">
<br/>
<p align="center">701 </p>
</td>
<td width="39">
<br/>
<p align="center">702 </p>
</td>
<td width="39">
<br/>
<p align="center">703 </p>
</td>
<td width="39">
<br/>
<p align="center">704 </p>
</td>
<td width="39">
<br/>
<p align="center">705 </p>
</td>
<td width="39">
<br/>
<p align="center">706 </p>
</td>
<td width="39">
<br/>
<p align="center">707 </p>
</td>
<td width="39">
<br/>
<p align="center">708 </p>
</td>
<td width="39">
<br/>
<p align="center">709 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="left">Nombre de salariés... </p>
</td>
<td valign="top">
<br/>
<p align="center">Aucun salarié </p>
</td>
<td valign="top">
<br/>
<p align="center">De 1 à 5 salariés </p>
</td>
<td colspan="2" valign="top">
<br/>
<p align="center">De 6 à 19 salariés </p>
</td>
<td colspan="2" valign="top">
<br/>
<p align="center">De 20 à 150 salariés </p>
</td>
<td colspan="3" valign="top">
<br/>
<p align="center">Plus de 150 salariés </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="left">Seuil de CA... </p>
</td>
<td valign="top">
<br/>
<br/>
<br/>
</td>
<td valign="top">
<br/>
<br/>
<br/>
</td>
<td valign="top">
<br/>
<p align="center">CA inférieur à 750 k € </p>
</td>
<td valign="top">
<br/>
<p align="center">CA supérieur à 750 k € </p>
</td>
<td valign="top">
<br/>
<p align="center">CA inférieur à 3 000 k € </p>
</td>
<td valign="top">
<br/>
<p align="center">CA supérieur à 3 000 k € </p>
</td>
<td valign="top">
<br/>
<p align="center">CA inférieur à 20 000 k € </p>
</td>
<td valign="top">
<br/>
<p align="center">CA de 20 000 k € à 50 000 k € </p>
</td>
<td valign="top">
<br/>
<p align="center">CA supérieur à 50 000 k € </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="left">Droit principal.-Nombre de taux de base... </p>
</td>
<td valign="top">
<br/>
<p align="center">480 </p>
</td>
<td valign="top">
<br/>
<p align="center">525 </p>
</td>
<td valign="top">
<br/>
<p align="center">1 100 </p>
</td>
<td valign="top">
<br/>
<p align="center">1 240 </p>
</td>
<td valign="top">
<br/>
<p align="center">2 090 </p>
</td>
<td valign="top">
<br/>
<p align="center">2 580 </p>
</td>
<td valign="top">
<br/>
<p align="center">5 294 </p>
</td>
<td valign="top">
<br/>
<p align="center">7 468 </p>
</td>
<td valign="top">
<br/>
<p align="center">12 520 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="left">Frais de transmission.</p>
<p align="left">-Nombre de taux de base... </p>
</td>
<td valign="top">
<br/>
<p align="center">60 </p>
</td>
<td valign="top">
<br/>
<p align="center">65 </p>
</td>
<td valign="top">
<br/>
<p align="center">120 </p>
</td>
<td valign="top">
<br/>
<p align="center">230 </p>
</td>
<td valign="top">
<br/>
<p align="center">300 </p>
</td>
<td valign="top">
<br/>
<p align="center">380 </p>
</td>
<td valign="top">
<br/>
<p align="center">596 </p>
</td>
<td valign="top">
<br/>
<p align="center">682 </p>
</td>
<td valign="top">
<br/>
<p align="center">760 </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="left">Total... </p>
</td>
<td valign="top">
<br/>
<p align="center">702 € </p>
</td>
<td valign="top">
<br/>
<p align="center">767 € </p>
</td>
<td valign="top">
<br/>
<p align="center">1 586 € </p>
</td>
<td valign="top">
<br/>
<p align="center">1 911 € </p>
</td>
<td valign="top">
<br/>
<p align="center">3 107 € </p>
</td>
<td valign="top">
<br/>
<p align="center">3 848 € </p>
</td>
<td valign="top">
<br/>
<p align="center">7 657 € </p>
</td>
<td valign="top">
<br/>
<p align="center">10 595 € </p>
</td>
<td valign="top">
<br/>
<p align="center">17 264 € </p>
</td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr>
<td valign="top">
<p align="left">719 : Droit accessoire par établissement secondaire (à charge pour le greffier de la procédure principale de reverser la moitié du droit au greffe de l'établissement secondaire)... </p>
</td>
<td valign="top">
<p align="left">150 taux de base. </p>
</td>
</tr>
<tr>
<td valign="top">
<p align="left">720 : Droit accessoire par créancier supplémentaire au-delà de 25 créanciers... </p>
</td>
<td valign="top">
<p align="left">10 taux de base plafonné à 100 taux de base.</p>
</td>
</tr>
</tbody>
</table>

TABLEAU VIII ANNEXÉ À L'ARTICLE R. 743-140

Par exception au principe de la facturation des actes dont le tarif est fixé par le tableau I, les émoluments et les frais de transmission de la procédure de rétablissement professionnel ouverte font l'objet d'une tarification forfaitaire par débiteur.

Cette tarification forfaitaire ne comprend pas les émoluments, les frais et les débours résultant des actions prévues au titre V du livre VI, dont le tarif est fixé par le tableau I, ainsi que les frais de copies d'actes ou de pièces délivrées aux parties.

En cas d'ouverture d'une liquidation judiciaire dans les conditions prévues à l'article L. 645-9, le forfait applicable est celui prévu au tableau VII, déduction faite des sommes dues au titre du droit principal en application du présent tableau.

Tarification forfaitaire applicable à la procédure de rétablissement professionnel

Emoluments du greffe par débiteur et forfait de transmission (hors frais d'huissiers, frais relatifs aux journaux d'annonces légales, BODACC)

<table>
<tbody>
<tr>
<td>
<p align="center">NUMÉROS</p>
</td>
<td>
<p align="center">NATURE DES ACTES</p>
</td>
<td>
<p align="center">ÉMOLUMENTS<br/>(taux de base) <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<br/>801 <br/>
</td>
<td>
<p align="left">Droit principal. <br/>
</p>
</td>
<td align="center">
<br/>300 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>802 <br/>
</td>
<td>
<p align="left">Droit accessoire par procédure devant le juge commis statuant sur une demande de report ou de délai de paiement en application de l'article L. 645-6.<br/>
</p>
</td>
<td align="center">
<br/>50 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>809 <br/>
</td>
<td>
<p align="left">Frais de transmission. <br/>
</p>
</td>
<td align="center">
<br/>50 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>819 <br/>
</td>
<td>
<p align="left">Droit accessoire en cas d'ouverture d'une procédure de liquidation judiciaire en application de l'article L. 645-9.<br/>
</p>
</td>
<td align="center">
<br/>60</td>
</tr>
</tbody>
</table>

<div align="left"/>
<div align="left"/>
<div align="left"/>
