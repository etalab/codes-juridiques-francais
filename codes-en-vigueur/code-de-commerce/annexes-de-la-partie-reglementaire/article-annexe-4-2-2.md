# Article Annexe 4-2-2

JURIDICTIONS COMPÉTENTES POUR CONNAÎTRE, EN APPLICATION DE L'ARTICLE L. 442-6,

DES PROCÉDURES APPLICABLES AUX PERSONNES QUI NE SONT NI COMMERÇANTS NI ARTISANS

<table>
<tbody>
<tr>
<td align="center">
<p align="center">SIÈGE DES TRIBUNAUX<br/>de grande instance</p>
</td>
<td align="center">RESSORT </td>
</tr>
<tr>
<td align="left">
<br/>Marseille. <br/>
</td>
<td align="left">
<br/>Le ressort des cours d'appel d'Aix-en-Provence, Bastia, Montpellier et Nîmes. <br/>
</td>
</tr>
<tr>
<td align="left">
<br/>Bordeaux. <br/>
</td>
<td align="left">
<br/>Le ressort des cours d'appel d'Agen, Bordeaux, Limoges, Pau et Toulouse. <br/>
</td>
</tr>
<tr>
<td align="left">
<br/>Lille. <br/>
</td>
<td align="left">
<br/>Le ressort des cours d'appel d'Amiens, Douai, Reims et Rouen. <br/>
</td>
</tr>
<tr>
<td align="left">
<br/>Fort-de-France. <br/>
</td>
<td align="left">
<br/>Le ressort des cours d'appel de Basse-Terre, de Cayenne et de Fort-de-France. <br/>
</td>
</tr>
<tr>
<td align="left">
<br/>Lyon. <br/>
</td>
<td align="left">
<br/>Le ressort des cours d'appel de Chambéry, Grenoble, Lyon et Riom. <br/>
</td>
</tr>
<tr>
<td align="left">
<br/>Nancy. <br/>
</td>
<td align="left">
<br/>Le ressort des cours d'appel de Besançon, Colmar, Dijon, Metz et Nancy. <br/>
</td>
</tr>
<tr>
<td align="left">
<br/>Paris. <br/>
</td>
<td align="left">
<br/>Le ressort des cours d'appel de Bourges, Paris, Orléans, Saint-Denis de La Réunion et Versailles. <br/>
</td>
</tr>
<tr>
<td align="left">
<br/>Rennes. <br/>
</td>
<td align="left">
<br/>Le ressort des cours d'appel d'Angers, Caen, Poitiers et Rennes.<br/>
</td>
</tr>
</tbody>
</table>
