# TITRE III : Des clauses d'exclusivité.

- [Article L330-1](article-l330-1.md)
- [Article L330-2](article-l330-2.md)
- [Article L330-3](article-l330-3.md)
