# Article L321-28

En cas de manquement aux dispositions du présent chapitre, les ressortissants des Etats membres de l'Union européenne et des Etats parties à l'accord sur l'Espace économique européen sont soumis aux dispositions de l'article L. 321-22. Toutefois, les sanctions de l'interdiction temporaire ou définitive de l'exercice de l'activité sont remplacées par les sanctions de l'interdiction temporaire ou définitive d'exercer en France l'activité de ventes volontaires de meubles aux enchères publiques.

En cas de sanction, le Conseil des ventes volontaires de meubles aux enchères publiques en avise l'autorité compétente de l'Etat d'établissement.
