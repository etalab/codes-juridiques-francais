# Article L321-11

Chaque vente volontaire de meubles aux enchères publiques donne lieu à une publicité sous toute forme appropriée.

Le prix de réserve est le prix minimal arrêté avec le vendeur au-dessous duquel le bien ne peut être vendu. Si le bien a été estimé, ce prix ne peut être fixé à un montant supérieur à l'estimation la plus basse figurant dans la publicité, ou annoncée publiquement par la personne qui procède à la vente et consignée au procès-verbal.

Sous réserve des dispositions de l'article L. 442-4, l'article L. 442-2 est applicable à tout vendeur se livrant à titre habituel à la revente de biens neufs en l'état à un prix inférieur à leur prix d'achat effectif, par le procédé des enchères publiques, dans les conditions prévues à cet article.
