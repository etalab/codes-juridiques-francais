# Section 1 : Dispositions générales.

- [Sous-section 1 : Les opérateurs de ventes volontaires de meubles aux enchères publiques](sous-section-1)
- [Sous-section 2 : Le Conseil des ventes volontaires de meubles aux enchères publiques.](sous-section-2)
- [Article L321-1](article-l321-1.md)
- [Article L321-2](article-l321-2.md)
- [Article L321-3](article-l321-3.md)
