# Article L321-22

Tout manquement aux lois, règlements ou obligations professionnelles applicables aux opérateurs de ventes volontaires de meubles aux enchères publiques mentionnés à l'article L. 321-4 et aux personnes habilitées à diriger les ventes en vertu du premier alinéa de l'article L. 321-9 peut donner lieu à sanction disciplinaire. La prescription est de trois ans à compter du manquement. Toutefois, si l'opérateur est l'auteur de faits ayant donné lieu à une condamnation pénale, l'action se prescrit par deux ans à compter de la date à laquelle cette condamnation est devenue définitive.

Le conseil statue par décision motivée. Aucune sanction ne peut être prononcée sans que les griefs aient été communiqués au représentant légal de l'opérateur ou à la personne habilitée à diriger les ventes, que celui-ci ait été mis à même de prendre connaissance du dossier et qu'il ait été entendu ou dûment appelé.

Aucun membre du Conseil des ventes volontaires de meubles aux enchères publiques ne peut participer à une délibération relative à :

1° Une affaire dans laquelle il a un intérêt direct ou indirect, dans laquelle il a déjà pris parti ou s'il représente ou a représenté l'intéressé ;

2° Un organisme au sein duquel il a, au cours des trois années précédant la délibération, détenu un intérêt direct ou indirect, exercé des fonctions ou détenu un mandat.

Tout membre du conseil doit informer le président des intérêts, directs ou indirects, qu'il détient ou vient à détenir, des fonctions qu'il exerce ou vient à exercer et de tout mandat qu'il détient ou vient à détenir au sein d'une personne morale. Ces informations, ainsi que celles concernant le président, sont tenues à la disposition des membres du conseil.

Les sanctions applicables aux opérateurs de ventes volontaires de meubles aux enchères publiques sont, compte tenu de la gravité des faits reprochés : l'avertissement, le blâme, l'interdiction d'exercer tout ou partie de l'activité de ventes volontaires de meubles aux enchères publiques ou de diriger des ventes à titre temporaire pour une durée qui ne peut excéder trois ans, l'interdiction définitive d'exercer l'activité de ventes volontaires de meubles aux enchères publiques ou l'interdiction définitive de diriger des ventes.

En cas d'urgence et à titre conservatoire, le président du conseil peut prononcer la suspension provisoire de l'exercice de tout ou partie de l'activité de ventes volontaires de meubles aux enchères publiques d'un opérateur ou d'une personne habilitée à diriger les ventes.

Cette mesure peut être ordonnée pour une durée qui ne peut excéder un mois, sauf prolongation décidée par le conseil pour une durée qui ne peut excéder trois mois. Le président en informe sans délai le conseil.

La suspension ne peut être prononcée sans que les griefs aient été communiqués à l'intéressé, qu'il ait été mis à même de prendre connaissance du dossier et qu'il ait été entendu ou dûment appelé par le président du conseil.

Le conseil peut publier ses décisions dans les journaux ou supports qu'il détermine, sauf si cette publication risque de causer un préjudice disproportionné aux parties en cause. Les frais de publication sont à la charge des personnes sanctionnées.
