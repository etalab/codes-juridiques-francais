# Article L441-4

Toute infraction aux dispositions de l'article L. 441-3 est punie d'une amende de 75 000 euros.

L'amende peut être portée à 50 % de la somme facturée ou de celle qui aurait dû être facturée.
