# Article L252-10

Les groupements européens d'intérêt économique ne peuvent, à peine de nullité des contrats conclus ou des titres émis, procéder à une offre au public de titres financiers

Est puni d'un emprisonnement de deux ans et d'une amende de 300 000 euros le fait, pour le ou les gérants d'un groupement européen d'intérêt économique ou le représentant permanent d'une personne morale gérant d'un groupement européen d'intérêt économique de       procéder à une offre au public de titres financiers
