# Sous-section 3 : Dispositions communes aux mandataires sociaux des sociétés anonymes.

- [Article L225-94](article-l225-94.md)
- [Article L225-94-1](article-l225-94-1.md)
- [Article L225-95](article-l225-95.md)
- [Article L225-95-1](article-l225-95-1.md)
