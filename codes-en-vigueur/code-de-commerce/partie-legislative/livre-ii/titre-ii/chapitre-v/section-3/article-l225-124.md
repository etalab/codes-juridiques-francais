# Article L225-124

Toute action convertie au porteur ou transférée en propriété perd le droit de vote double attribué en application de l'article L. 225-123. Néanmoins, le transfert par suite de succession, de liquidation de communauté de biens entre époux ou de donation entre vifs au profit d'un conjoint ou d'un parent au degré successible ne fait pas perdre le droit acquis et n'interrompt pas le délai mentionné aux premier et dernier alinéas de l'article L. 225-123. Il en est de même, sauf stipulation contraire des statuts, en cas de transfert par suite d'une fusion ou d'une scission d'une société actionnaire.

La fusion ou la scission de la société est sans effet sur le droit de vote double qui peut être exercé au sein de la ou des sociétés bénéficiaires, si celles-ci en bénéficient.
