# Article L225-247

Le tribunal de commerce peut, à la demande de tout intéressé, prononcer la dissolution de la société, si le nombre des actionnaires est réduit à moins de sept depuis plus d'un an.

Il peut accorder à la société un délai maximal de six mois pour régulariser la situation. Il ne peut prononcer la dissolution si, le jour où il statue sur le fond, cette régularisation a eu lieu.
