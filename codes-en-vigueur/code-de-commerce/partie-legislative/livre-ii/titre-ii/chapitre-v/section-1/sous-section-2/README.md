# Sous-section 2 : De la constitution sans offre au public.

- [Article L225-12](article-l225-12.md)
- [Article L225-13](article-l225-13.md)
- [Article L225-14](article-l225-14.md)
- [Article L225-15](article-l225-15.md)
- [Article L225-16](article-l225-16.md)
- [Article L225-16-1](article-l225-16-1.md)
