# Article L225-16

Les premiers administrateurs ou les premiers membres du conseil de surveillance et les premiers commissaires aux comptes sont désignés dans les statuts.
