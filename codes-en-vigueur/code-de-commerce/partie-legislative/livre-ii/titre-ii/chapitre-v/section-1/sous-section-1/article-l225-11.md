# Article L225-11

Le retrait des fonds provenant des souscriptions en numéraire ne peut être effectué par le mandataire de la société avant l'immatriculation de celle-ci au registre du commerce et des sociétés.

Si la société n'est pas constituée dans le délai de six mois à compter du dépôt du projet de statuts au greffe, tout souscripteur peut demander en justice la nomination d'un mandataire chargé de retirer les fonds pour les restituer aux souscripteurs, sous déduction des frais de répartition.

Si le ou les fondateurs décident ultérieurement de constituer la société, il doit être procédé à nouveau au dépôt des fonds et à la déclaration prévus aux articles L. 225-5 et L. 225-6.
