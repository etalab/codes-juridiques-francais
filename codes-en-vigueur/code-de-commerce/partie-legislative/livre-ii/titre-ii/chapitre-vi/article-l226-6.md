# Article L226-6

L'assemblée générale ordinaire désigne un ou plusieurs commissaires aux comptes.
