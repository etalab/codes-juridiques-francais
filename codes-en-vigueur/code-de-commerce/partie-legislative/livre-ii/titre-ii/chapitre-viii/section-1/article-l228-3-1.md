# Article L228-3-1

I.-Aussi longtemps que la société émettrice estime que certains détenteurs dont l'identité lui a été communiquée le sont pour le compte de tiers propriétaires des titres, elle est en droit de demander à ces détenteurs de révéler l'identité des propriétaires de ces titres, ainsi que la quantité de titres détenus par chacun d'eux, dans les conditions prévues respectivement au premier alinéa du II de l'article L. 228-2 pour les titres au porteur et au premier alinéa de l'article L. 228-3 pour les titres nominatifs.

II.-A l'issue de ces opérations, et sans préjudice des obligations de déclaration de participations significatives imposées par les articles L. 233-7,
L. 233-12 et L. 233-13, la société émettrice peut demander à toute personne morale propriétaire de ses actions et possédant des participations dépassant le quarantième du capital ou des droits de vote de lui faire connaître l'identité des personnes détenant directement ou indirectement plus du tiers du capital social de cette personne morale ou des droits de vote qui sont exercés aux assemblées générales de celle-ci.
