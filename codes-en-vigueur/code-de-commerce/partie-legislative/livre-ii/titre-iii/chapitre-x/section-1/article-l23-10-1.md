# Article L23-10-1

Dans les sociétés qui n'ont pas l'obligation de mettre en place un comité d'entreprise en application de l'
article L. 2322-1 du code du travail
, lorsque le propriétaire d'une participation représentant plus de 50 % des parts sociales d'une société à responsabilité limitée ou d'actions ou valeurs mobilières donnant accès à la majorité du capital d'une société par actions veut les céder, les salariés en sont informés, et ce au plus tard deux mois avant la cession, afin de permettre à un ou plusieurs salariés de présenter une offre d'achat de cette participation.

Le représentant légal notifie sans délai aux salariés cette information, en leur indiquant qu'ils peuvent présenter au cédant une offre d'achat.

La cession peut intervenir avant l'expiration du délai de deux mois dès lors que chaque salarié a fait connaître au cédant sa décision de ne pas présenter d'offre.

La cession intervenue en méconnaissance du présent article peut être annulée à la demande de tout salarié.

L'action en nullité se prescrit par deux mois à compter de la date de publication de la cession de la participation ou de la date à laquelle tous les salariés en ont été informés.
