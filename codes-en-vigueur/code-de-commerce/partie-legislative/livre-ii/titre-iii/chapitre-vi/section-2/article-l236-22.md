# Article L236-22

La société qui apporte une partie de son actif à une autre société et la société qui bénéficie de cet apport peuvent décider d'un commun accord de soumettre l'opération aux dispositions des articles L. 236-16 à L. 236-21.
