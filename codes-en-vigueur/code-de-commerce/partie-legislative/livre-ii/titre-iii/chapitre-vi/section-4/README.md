# Section 4 : Dispositions particulières aux fusions transfrontalières

- [Article L236-25](article-l236-25.md)
- [Article L236-26](article-l236-26.md)
- [Article L236-27](article-l236-27.md)
- [Article L236-28](article-l236-28.md)
- [Article L236-29](article-l236-29.md)
- [Article L236-30](article-l236-30.md)
- [Article L236-31](article-l236-31.md)
- [Article L236-32](article-l236-32.md)
