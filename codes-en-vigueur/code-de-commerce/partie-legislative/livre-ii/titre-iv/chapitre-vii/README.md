# Chapitre VII : Des infractions communes aux diverses formes de sociétés commerciales

- [Section 1 : Des infractions relatives aux filiales, aux participations et aux sociétés contrôlées](section-1)
- [Section 3 : Des infractions relatives à la liquidation](section-3)
- [Section 4 : Des infractions relatives aux sociétés anonymes comportant un directoire et un conseil de surveillance](section-4)
