# Section 4 : Des infractions relatives aux modifications du capital social

- [Sous-section 1 : De l'augmentation du capital](sous-section-1)
- [Sous-section 3 : De la réduction du capital](sous-section-3)
