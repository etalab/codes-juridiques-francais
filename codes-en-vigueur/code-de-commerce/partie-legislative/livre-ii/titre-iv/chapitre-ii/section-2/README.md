# Section 2 : Des infractions relatives à la direction et à l'administration

- [Article L242-6](article-l242-6.md)
- [Article L242-8](article-l242-8.md)
