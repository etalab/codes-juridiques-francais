# Chapitre VIII : Dispositions concernant les directeurs généraux délégués des sociétés anonymes ou des sociétés européennes.

- [Article L248-1](article-l248-1.md)
