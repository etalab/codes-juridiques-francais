# Section 3 : Des infractions relatives aux obligations

- [Article L245-9](article-l245-9.md)
- [Article L245-11](article-l245-11.md)
- [Article L245-12](article-l245-12.md)
- [Article L245-13](article-l245-13.md)
- [Article L245-15](article-l245-15.md)
