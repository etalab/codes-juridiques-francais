# Article L527-4

Le gage des stocks ne produit effet que s'il est inscrit sur un registre public tenu au greffe du tribunal dans le ressort duquel le débiteur a son siège ou son domicile. L'inscription doit être prise, à peine de nullité du gage, dans le délai de quinze jours à compter de la formation de l'acte constitutif.

Le rang des créanciers gagistes entre eux est déterminé par la date de leur inscription. Les créanciers inscrits le même jour viennent en concurrence.
