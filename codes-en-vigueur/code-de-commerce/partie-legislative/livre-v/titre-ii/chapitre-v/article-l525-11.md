# Article L525-11

L'inscription conserve le privilège pendant cinq années à compter de sa régularisation définitive.

Elle garantit, en même temps que le principal, deux années d'intérêts. Elle cesse d'avoir effet si elle n'a pas été renouvelée avant l'expiration du délai ci-dessus ; elle peut être renouvelée deux fois.
