# Article L525-8

Le privilège du créancier nanti en application des dispositions du présent chapitre subsiste si le bien qui est grevé devient immeuble par destination.

L'article 2133 du code civil n'est pas applicable aux biens nantis.
