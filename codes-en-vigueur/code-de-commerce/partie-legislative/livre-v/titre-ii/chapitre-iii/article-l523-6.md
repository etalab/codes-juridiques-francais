# Article L523-6

Le greffier est tenu de délivrer à tout prêteur qui le requiert, soit un état des warrants, soit un certificat établissant qu'il n'existe pas d'inscription. Il est tenu de faire la même délivrance à tout hôtelier ressortissant de son greffe qui le requiert, mais seulement en ce qui concerne le fonds exploité par lui.

Cet état ne remonte pas à une période antérieure de cinq années.
