# Article L523-3

Il est tenu, dans chaque greffe de tribunal de commerce, un registre à souche, coté et paraphé, dont le volant et la souche portent chacun, d'après les déclarations de l'emprunteur, des mentions dont la liste est fixée par décret.

Le volant contenant ces mentions constitue le warrant hôtelier.
