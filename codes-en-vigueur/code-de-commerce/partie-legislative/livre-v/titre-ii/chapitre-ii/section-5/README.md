# Section 5 : Des sanctions.

- [Article L522-38](article-l522-38.md)
- [Article L522-39](article-l522-39.md)
- [Article L522-40](article-l522-40.md)
