# Chapitre Ier : Dispositions d'adaptation du livre Ier.

- [Article L911-2](article-l911-2.md)
- [Article L911-3](article-l911-3.md)
- [Article L911-4](article-l911-4.md)
- [Article L911-5](article-l911-5.md)
- [Article L911-6](article-l911-6.md)
- [Article L911-7](article-l911-7.md)
- [Article L911-8](article-l911-8.md)
- [Article L911-9](article-l911-9.md)
- [Article L911-11](article-l911-11.md)
- [Article L911-12](article-l911-12.md)
- [Article L911-13](article-l911-13.md)
- [Article L911-14](article-l911-14.md)
