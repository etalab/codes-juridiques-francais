# Chapitre IV : Dispositions d'adaptation du livre IV.

- [Article L924-3](article-l924-3.md)
- [Article L924-4](article-l924-4.md)
- [Article L924-5](article-l924-5.md)
- [Article L924-6](article-l924-6.md)
