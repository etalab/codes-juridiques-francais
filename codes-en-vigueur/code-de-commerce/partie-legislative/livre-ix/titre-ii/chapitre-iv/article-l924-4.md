# Article L924-4

Au second alinéa de l'article L. 442-2, avant les mots :

" taxes sur le chiffre d'affaires ", est ajouté le mot :

" éventuelles " ;
