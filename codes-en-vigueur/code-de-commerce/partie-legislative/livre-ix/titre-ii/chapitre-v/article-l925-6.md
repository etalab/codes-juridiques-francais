# Article L925-6

Au 1° de l'article L. 525-18, la référence au décret n° 53-968 du 30 septembre 1953 est remplacée par la référence au décret n° 55-639 du 20 mai 1955.
