# Chapitre Ier : Dispositions d'adaptation du livre Ier.

- [Article L921-2](article-l921-2.md)
- [Article L921-4](article-l921-4.md)
- [Article L921-6](article-l921-6.md)
- [Article L921-8](article-l921-8.md)
- [Article L921-11](article-l921-11.md)
- [Article L921-13](article-l921-13.md)
- [Article L921-14](article-l921-14.md)
