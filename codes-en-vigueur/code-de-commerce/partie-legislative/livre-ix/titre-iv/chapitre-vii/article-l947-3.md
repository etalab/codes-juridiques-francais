# Article L947-3

Le premier alinéa de l'article L. 722-7 est ainsi rédigé :

"Les juges des tribunaux mixtes de commerce sont élus pour quatre ans. Ils sont rééligibles."
