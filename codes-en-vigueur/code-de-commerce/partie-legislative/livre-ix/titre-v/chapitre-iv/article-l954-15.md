# Article L954-15

Au premier alinéa de l'article L. 464-9, les mots : ", ne concernent pas des faits relevant des articles 81 et 82 du traité instituant la Communauté européenne " sont supprimés.
