# Article L954-12

Le dernier alinéa de l'article L. 462-6 est supprimé.
