# Article L954-5-1

Au I de l'article L. 442-6, le 10° est abrogé et, au 11°, les mots : " aux II et III " sont remplacés par les mots : " au II ".
