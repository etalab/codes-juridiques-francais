# Article L821-1

Il est institué auprès du garde des sceaux, ministre de la justice, une autorité publique indépendante dotée de la personnalité morale, dénommée Haut Conseil du commissariat aux comptes, ayant pour mission :

-d'assurer la surveillance de la profession avec le concours de la Compagnie nationale des commissaires aux comptes instituée par l'article L. 821-6 ;

-de veiller au respect de la déontologie et de l'indépendance des commissaires aux comptes.

Pour l'accomplissement de cette mission, le Haut Conseil du commissariat aux comptes est en particulier chargé :

-d'identifier et de promouvoir les bonnes pratiques professionnelles ;

-d'émettre un avis sur les normes d'exercice professionnel élaborées par la Compagnie nationale des commissaires aux comptes avant leur homologation par arrêté du garde des sceaux, ministre de la justice ;

-d'assurer, comme instance d'appel des décisions des commissions régionales mentionnées à l'article L. 822-2, l'inscription des commissaires aux comptes ;

-d'assurer, comme instance d'appel des décisions prises par les chambres régionales mentionnées à l'article L. 822-6, la discipline des commissaires aux comptes ;

-de définir le cadre et les orientations des contrôles périodiques prévus au b de l'article L. 821-7 qu'il met en œuvre soit directement, soit en en déléguant l'exercice à la Compagnie nationale des commissaires aux comptes et aux compagnies régionales, ou qui sont réalisés par la Compagnie nationale et les compagnies régionales, selon les modalités prévues à l'article L. 821-9 ;

-de superviser les contrôles prévus au b et au c de l'article L. 821-7 et d'émettre des recommandations dans le cadre de leur suivi ;

-de veiller à la bonne exécution des contrôles prévus au b de l'article L. 821-7 et, lorsqu'ils sont effectués à sa demande, au c du même article ;

-d'établir des relations avec les autorités d'autres Etats exerçant des compétences analogues.

Les missions définies aux dixième et onzième alinéas du présent article sont exercées dans des conditions fixées par décret en Conseil d'Etat garantissant l'indépendance des fonctions de contrôle et de sanction.
