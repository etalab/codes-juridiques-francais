# Article L821-11

Les conditions d'application des articles L. 821-3 et L. 821-6 à L. 821-10 sont fixées par décret en Conseil d'Etat.
