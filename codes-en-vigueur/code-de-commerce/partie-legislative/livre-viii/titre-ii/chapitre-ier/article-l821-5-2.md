# Article L821-5-2

Aux fins mentionnées à l'avant-dernier alinéa de l'article L. 821-1, le Haut Conseil du commissariat aux comptes peut communiquer des informations ou des documents qu'il détient ou qu'il recueille aux autorités d'Etats non membres de l'Union européenne exerçant des compétences analogues aux siennes sous réserve de réciprocité et à la condition que l'autorité concernée soit soumise au secret professionnel avec les mêmes garanties qu'en France.

Il peut, sous les mêmes réserve et condition, demander au garde des sceaux, ministre de la justice, de faire diligenter une inspection, conformément aux dispositions de l'article L. 821-8, ou faire diligenter par les contrôleurs mentionnés à l'article L. 821-9 les opérations de contrôle qu'il détermine afin de répondre aux demandes d'assistance des autorités mentionnées au premier alinéa.

Le Haut Conseil du commissariat aux comptes peut, à titre exceptionnel, autoriser les agents des autorités des Etats non membres de l'Union européenne à assister aux contrôles périodiques mentionnés au b de l'article L. 821-7. Lors de ces contrôles, effectués sous la direction du Haut Conseil, les agents de ces autorités ne peuvent solliciter directement du commissaire aux comptes la communication d'informations ou de documents.

Un décret en Conseil d'Etat détermine les conditions d'application du présent article, notamment les modalités de la coopération du Haut Conseil avec ces autorités et les conditions dans lesquelles ces modalités sont précisées par des conventions passées par le Haut Conseil avec ces autorités.
