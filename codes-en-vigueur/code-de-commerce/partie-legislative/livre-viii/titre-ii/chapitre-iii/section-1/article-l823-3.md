# Article L823-3

Les commissaires aux comptes sont nommés pour six exercices. Leurs fonctions expirent après la délibération de l'assemblée générale ou de l'organe compétent qui statue sur les comptes du sixième exercice.

Le commissaire aux comptes nommé en remplacement d'un autre ne demeure en fonction que jusqu'à l'expiration du mandat de son prédécesseur.

Le commissaire aux comptes dont la mission est expirée, qui a été révoqué, relevé de ses fonctions, suspendu, interdit temporairement d'exercer, radié, omis ou a donné sa démission permet au commissaire aux comptes lui succédant d'accéder à toutes les informations et à tous les documents pertinents concernant la personne ou l'entité dont les comptes sont certifiés.
