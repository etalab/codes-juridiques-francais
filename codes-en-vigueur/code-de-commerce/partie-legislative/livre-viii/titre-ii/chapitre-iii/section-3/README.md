# Section 3 : Des modalités d'exercice de la mission.

- [Article L823-12-1](article-l823-12-1.md)
- [Article L823-13](article-l823-13.md)
- [Article L823-14](article-l823-14.md)
- [Article L823-15](article-l823-15.md)
- [Article L823-16](article-l823-16.md)
- [Article L823-16-1](article-l823-16-1.md)
- [Article L823-17](article-l823-17.md)
- [Article L823-18](article-l823-18.md)
- [Article L823-19](article-l823-19.md)
- [Article L823-20](article-l823-20.md)
