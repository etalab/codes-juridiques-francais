# Section 2 : De la mission du commissaire aux comptes.

- [Article L823-9](article-l823-9.md)
- [Article L823-10](article-l823-10.md)
- [Article L823-11](article-l823-11.md)
- [Article L823-12](article-l823-12.md)
