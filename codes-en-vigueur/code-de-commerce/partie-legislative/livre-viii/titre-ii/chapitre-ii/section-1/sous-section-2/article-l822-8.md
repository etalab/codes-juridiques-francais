# Article L822-8

- Les sanctions disciplinaires sont :

1° L'avertissement ;

2° Le blâme ;

3° L'interdiction temporaire pour une durée n'excédant pas cinq ans ;

4° La radiation de la liste.

Il peut être aussi procédé au retrait de l'honorariat.

L'avertissement, le blâme ainsi que l'interdiction temporaire peuvent être assortis de la sanction complémentaire de l'inéligibilité aux organismes professionnels pendant dix ans au plus.

La sanction de l'interdiction temporaire peut être assortie du sursis. La suspension de la peine ne s'étend pas à la sanction complémentaire prise en application de l'alinéa précédent. Si, dans le délai de cinq ans à compter du prononcé de la sanction, le commissaire aux comptes a commis une infraction ou une faute ayant entraîné le prononcé d'une nouvelle sanction disciplinaire, celle-ci entraîne, sauf décision motivée, l'exécution de la première sanction sans confusion possible avec la seconde.

Lorsqu'ils prononcent une sanction disciplinaire, le Haut Conseil et les chambres régionales peuvent décider de mettre à la charge du commissaire aux comptes tout ou partie des frais occasionnés par les inspections ou contrôles ayant permis la constatation des faits sanctionnés.
