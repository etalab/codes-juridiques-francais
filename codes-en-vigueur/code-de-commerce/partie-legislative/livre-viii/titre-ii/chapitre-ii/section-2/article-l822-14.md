# Article L822-14

Le commissaire aux comptes, personne physique, et, dans les sociétés de commissaires aux comptes, le ou les associés signataires ainsi que, le cas échéant, tout autre associé principal au sens du 16 de l'article 2 de la directive 2006 / 43 / CE du Parlement européen et du Conseil, du 17 mai 2006, concernant les contrôles légaux des comptes annuels et des comptes consolidés et modifiant les directives 78 / 660 / CEE et 83 / 349 / CEE, et abrogeant la directive 84 / 253 / CEE du Conseil, ne peuvent certifier durant plus de six exercices consécutifs les comptes des personnes et entités dont les titres financiers sont admis à la négociation sur un marché réglementé.

Ils ne peuvent à nouveau participer à une mission de contrôle légal des comptes de ces personnes ou entités avant l'expiration d'un délai de deux ans à compter de la date de clôture du sixième exercice qu'ils ont certifié.

Cette disposition est également applicable aux personnes et entités visées à l'article L. 612-1 et aux associations visées à l'article L. 612-4 dès lors que ces personnes font appel à la générosité publique au sens de l'article 3 de la loi n° 91-772 du 7 août 1991.
