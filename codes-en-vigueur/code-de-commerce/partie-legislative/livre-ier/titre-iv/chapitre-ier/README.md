# Chapitre Ier : De la vente du fonds de commerce.

- [Section 1 : De l'acte de vente.](section-1)
- [Section 2 : Du privilège du vendeur.](section-2)
- [Section 3 : De l'instauration d'un délai permettant aux salariés de présenter une offre en cas de cession d'un fonds de commerce dans les entreprises de moins de cinquante salariés](section-3)
- [Section 4 : De l'information anticipée des salariés leur permettant de présenter une offre en cas de cession d'un fonds de commerce dans les entreprises employant de cinquante à deux cent quarante-neuf salariés](section-4)
