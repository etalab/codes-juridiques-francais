# Article L141-31

La cession est de nouveau soumise aux articles L. 141-28 à L. 141-30 lorsqu'elle intervient plus de deux ans après l'expiration du délai prévu à l'article L. 141-28.

Si pendant cette période de deux ans le comité d'entreprise est consulté, en application de l'
article L. 2323-19 du code du travail
, sur un projet de cession du fonds de commerce, le cours de ce délai de deux ans est suspendu entre la date de saisine du comité et la date où il rend son avis et, à défaut, jusqu'à la date où expire le délai imparti pour rendre cet avis.
