# Section 3 : De l'instauration d'un délai permettant aux salariés de présenter une offre en cas de cession d'un fonds de commerce dans les entreprises de moins de cinquante salariés

- [Article L141-23](article-l141-23.md)
- [Article L141-24](article-l141-24.md)
- [Article L141-25](article-l141-25.md)
- [Article L141-26](article-l141-26.md)
- [Article L141-27](article-l141-27.md)
