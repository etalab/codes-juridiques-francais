# Article L141-19

Pendant les vingt jours qui suivent la publication au Bulletin officiel des annonces civiles et commerciales prévue à l'article L. 141-12, une copie authentique ou l'un des originaux de l'acte de vente est tenu, au domicile élu, à la disposition de tout créancier opposant ou inscrit pour être consulté sans déplacement.

Pendant le même délai, tout créancier inscrit ou qui a formé opposition dans le délai de dix jours fixé par l'article L. 141-14 peut prendre, au domicile élu, communication de l'acte de vente et des oppositions et, si le prix ne suffit pas à désintéresser les créanciers inscrits et ceux qui se sont révélés par des oppositions, au plus tard dans les dix jours qui suivent la publication au Bulletin officiel des annonces civiles et commerciales prévue à l'article L. 141-12, former, en se conformant aux prescriptions des articles L. 141-14 à L. 141-16 une surenchère du sixième du prix principal du fonds de commerce, non compris le matériel et les marchandises.

La surenchère du sixième n'est pas admise après la vente judiciaire d'un fonds de commerce ou la vente poursuivie à la requête d'un administrateur judiciaire ou d'un mandataire judiciaire, ou de copropriétaires indivis du fonds, faite aux enchères publiques et conformément aux articles L. 143-6 et L. 143-7, ou selon les dispositions de l'article L. 642-5.

L'officier public commis pour procéder à la vente doit n'admettre à enchérir que des personnes dont la solvabilité lui est connue, ou qui ont déposé soit entre ses mains, soit à la Caisse des dépôts et consignations, avec affectation spéciale au paiement du prix, une somme qui ne peut être inférieure à la moitié du prix total de la première vente, ni à la portion du prix de ladite vente stipulée payable comptant, augmentée de la surenchère.

L'adjudication sur surenchère du sixième a lieu aux mêmes conditions et délais que la vente sur laquelle la surenchère est intervenue.

Si l'acquéreur surenchéri est dépossédé par suite de la surenchère, il doit, sous sa responsabilité, remettre les oppositions formées entre ses mains à l'adjudicataire, sur récépissé, dans la huitaine de l'adjudication, s'il ne les a pas fait connaître antérieurement par mention insérée au cahier des charges. L'effet de ces oppositions est reporté sur le prix de l'adjudication.
