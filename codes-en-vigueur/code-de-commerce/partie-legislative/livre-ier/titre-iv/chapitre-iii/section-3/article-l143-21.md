# Article L143-21

Tout tiers détenteur du prix d'acquisition d'un fonds de commerce chez lequel domicile a été élu doit en faire la répartition dans les cinq mois de la date de l'acte de vente.

A l'expiration de ce délai, la partie la plus diligente peut se pourvoir en référé devant la juridiction compétente du lieu de l'élection du domicile, qui ordonne soit le dépôt à la Caisse des dépôts et consignations, soit la nomination d'un séquestre répartiteur.
