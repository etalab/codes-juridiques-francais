# Article L143-3

Tout créancier qui exerce des poursuites de saisie-exécution et le débiteur contre lequel elles sont exercées peuvent demander, devant le tribunal de commerce dans le ressort duquel s'exploite le fonds, la vente du fonds de commerce du saisi avec le matériel et les marchandises qui en dépendent.

Sur la demande du créancier poursuivant, le tribunal de commerce ordonne qu'à défaut de paiement dans le délai imparti au débiteur, la vente du fonds a lieu à la requête dudit créancier, après l'accomplissement des formalités prescrites par l'article L. 143-6.

Il en est de même si, sur l'instance introduite par le débiteur, le créancier demande à poursuivre la vente du fonds.

S'il ne le demande pas, le tribunal de commerce fixe le délai dans lequel la vente du fonds doit avoir lieu à la requête du débiteur, suivant les formalités édictées par l'article L. 143-6, et il ordonne que, faute par le débiteur d'avoir fait procéder à la vente dans ledit délai, les poursuites de saisie-exécution sont reprises et continuées sur les derniers errements.
