# Article L144-7

Jusqu'à la publication du contrat de location-gérance et pendant un délai de six mois à compter de cette publication, le loueur du fonds est solidairement responsable avec le locataire-gérant des dettes contractées par celui-ci à l'occasion de l'exploitation du fonds.
