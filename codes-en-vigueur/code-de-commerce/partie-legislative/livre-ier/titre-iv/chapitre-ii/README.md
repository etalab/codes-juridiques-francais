# Chapitre II : Du nantissement du fonds de commerce.

- [Article L142-1](article-l142-1.md)
- [Article L142-2](article-l142-2.md)
- [Article L142-3](article-l142-3.md)
- [Article L142-4](article-l142-4.md)
- [Article L142-5](article-l142-5.md)
