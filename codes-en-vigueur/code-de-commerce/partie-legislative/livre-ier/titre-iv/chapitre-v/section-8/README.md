# Section 8 : De la déspécialisation.

- [Article L145-47](article-l145-47.md)
- [Article L145-48](article-l145-48.md)
- [Article L145-49](article-l145-49.md)
- [Article L145-50](article-l145-50.md)
- [Article L145-51](article-l145-51.md)
- [Article L145-52](article-l145-52.md)
- [Article L145-53](article-l145-53.md)
- [Article L145-54](article-l145-54.md)
- [Article L145-55](article-l145-55.md)
