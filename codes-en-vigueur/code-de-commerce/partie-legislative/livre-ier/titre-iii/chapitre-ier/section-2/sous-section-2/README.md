# Sous-section 2 : Fonctions des courtiers de marchandises assermentés

- [Article L131-23](article-l131-23.md)
- [Article L131-24](article-l131-24.md)
- [Article L131-25](article-l131-25.md)
- [Article L131-26](article-l131-26.md)
- [Article L131-27](article-l131-27.md)
- [Article L131-28](article-l131-28.md)
- [Article L131-29](article-l131-29.md)
- [Article L131-30](article-l131-30.md)
- [Article L131-31](article-l131-31.md)
