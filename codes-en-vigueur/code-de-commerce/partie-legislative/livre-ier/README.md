# LIVRE Ier : Du commerce en général.

- [TITRE Ier : De l'acte de commerce.](titre-ier)
- [TITRE II : Des commerçants.](titre-ii)
- [TITRE III : Des courtiers, des commissionnaires, des transporteurs, des agents commerciaux et des vendeurs à domicile indépendants.](titre-iii)
- [TITRE IV : Du fonds de commerce.](titre-iv)
