# Chapitre VII : Du contrat d'appui au projet d'entreprise pour la création ou la reprise d'une activité économique.

- [Article L127-1](article-l127-1.md)
- [Article L127-2](article-l127-2.md)
- [Article L127-3](article-l127-3.md)
- [Article L127-4](article-l127-4.md)
- [Article L127-5](article-l127-5.md)
- [Article L127-6](article-l127-6.md)
- [Article L127-7](article-l127-7.md)
