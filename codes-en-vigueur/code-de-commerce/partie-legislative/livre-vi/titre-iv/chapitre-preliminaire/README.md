# Chapitre préliminaire : Des conditions d'ouverture de la liquidation judiciaire.

- [Article L640-1](article-l640-1.md)
- [Article L640-2](article-l640-2.md)
- [Article L640-3](article-l640-3.md)
- [Article L640-3-1](article-l640-3-1.md)
- [Article L640-4](article-l640-4.md)
- [Article L640-5](article-l640-5.md)
- [Article L640-6](article-l640-6.md)
