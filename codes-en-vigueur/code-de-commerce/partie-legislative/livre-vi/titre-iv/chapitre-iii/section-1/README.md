# Section 1 : Du règlement des créanciers.

- [Article L643-1](article-l643-1.md)
- [Article L643-2](article-l643-2.md)
- [Article L643-3](article-l643-3.md)
- [Article L643-4](article-l643-4.md)
- [Article L643-5](article-l643-5.md)
- [Article L643-6](article-l643-6.md)
- [Article L643-7](article-l643-7.md)
- [Article L643-7-1](article-l643-7-1.md)
- [Article L643-8](article-l643-8.md)
