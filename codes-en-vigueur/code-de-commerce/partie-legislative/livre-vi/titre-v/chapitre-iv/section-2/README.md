# Section 2 : Des autres infractions.

- [Article L654-8](article-l654-8.md)
- [Article L654-9](article-l654-9.md)
- [Article L654-10](article-l654-10.md)
- [Article L654-11](article-l654-11.md)
- [Article L654-12](article-l654-12.md)
- [Article L654-13](article-l654-13.md)
- [Article L654-14](article-l654-14.md)
- [Article L654-15](article-l654-15.md)
