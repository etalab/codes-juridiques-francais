# Chapitre III : De la faillite personnelle et des autres mesures d'interdiction.

- [Article L653-1](article-l653-1.md)
- [Article L653-2](article-l653-2.md)
- [Article L653-3](article-l653-3.md)
- [Article L653-4](article-l653-4.md)
- [Article L653-5](article-l653-5.md)
- [Article L653-6](article-l653-6.md)
- [Article L653-7](article-l653-7.md)
- [Article L653-8](article-l653-8.md)
- [Article L653-9](article-l653-9.md)
- [Article L653-10](article-l653-10.md)
- [Article L653-11](article-l653-11.md)
