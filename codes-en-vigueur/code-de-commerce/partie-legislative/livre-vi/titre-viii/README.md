# TITRE VIII : Dispositions particulières à l'entrepreneur individuel à responsabilité limitée.

- [Article L680-1](article-l680-1.md)
- [Article L680-2](article-l680-2.md)
- [Article L680-3](article-l680-3.md)
- [Article L680-4](article-l680-4.md)
- [Article L680-5](article-l680-5.md)
- [Article L680-6](article-l680-6.md)
- [Article L680-7](article-l680-7.md)
