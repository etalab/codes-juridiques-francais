# Chapitre II : De la nullité de certains actes.

- [Article L632-1](article-l632-1.md)
- [Article L632-2](article-l632-2.md)
- [Article L632-3](article-l632-3.md)
- [Article L632-4](article-l632-4.md)
