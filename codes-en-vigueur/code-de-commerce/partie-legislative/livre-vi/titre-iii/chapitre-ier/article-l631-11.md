# Article L631-11

Le juge-commissaire fixe la rémunération afférente aux fonctions exercées par le débiteur s'il est une personne physique ou les dirigeants de la personne morale.

En l'absence de rémunération, les personnes mentionnées à l'alinéa précédent peuvent obtenir sur l'actif, pour eux et leur famille, des subsides fixés par le juge-commissaire. Lorsque le débiteur est un entrepreneur individuel à responsabilité limitée, le juge-commissaire tient compte des revenus éventuellement perçus au titre des patrimoines non visés par la procédure.
