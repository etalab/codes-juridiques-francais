# Article R470-1

Lorsque le ministre chargé de l'économie intervient sur le fondement de l'article L. 470-5, il est dispensé de représentation par un avocat.
