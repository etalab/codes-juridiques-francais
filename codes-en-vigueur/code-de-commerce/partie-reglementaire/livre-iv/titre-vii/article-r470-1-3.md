# Article R470-1-3

Un arrêté du ministre chargé de l'économie désigne les agents des services chargés de la concurrence et de la consommation appelés à remplacer les représentants mentionnés aux articles R. 470-1-1 et R. 470-1-2 en cas d'empêchement de ces derniers.
