# Section 2 : Des recours exercés devant la cour d'appel de Paris contre les décisions de l'Autorité de la concurrence.

- [Sous-section 1 : Des recours prévus à l'article L. 464-8.](sous-section-1)
- [Sous-section 2 : Des recours prévus à l'article L. 464-7.](sous-section-2)
- [Sous-section 3 : Des demandes de sursis à exécution.](sous-section-3)
- [Sous-section 4 : Dispositions communes aux différentes demandes.](sous-section-4)
- [Article R464-10](article-r464-10.md)
- [Article R464-11](article-r464-11.md)
