# Chapitre IV : Des décisions et des voies de recours.

- [Section 1 : Des décisions.](section-1)
- [Section 2 : Des recours exercés devant la cour d'appel de Paris contre les décisions de l'Autorité de la concurrence.](section-2)
