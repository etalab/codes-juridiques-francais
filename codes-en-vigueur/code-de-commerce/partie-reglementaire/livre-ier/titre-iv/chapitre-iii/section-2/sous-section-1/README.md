# Sous-section 1 : De l'inscription.

- [Article R143-6](article-r143-6.md)
- [Article R143-7](article-r143-7.md)
- [Article R143-8](article-r143-8.md)
- [Article R143-9](article-r143-9.md)
- [Article R143-10](article-r143-10.md)
- [Article R143-11](article-r143-11.md)
- [Article R143-12](article-r143-12.md)
- [Article R143-13](article-r143-13.md)
- [Article R143-14](article-r143-14.md)
- [Article R143-15](article-r143-15.md)
- [Article R143-16](article-r143-16.md)
- [Article R143-17](article-r143-17.md)
