# Article R143-20

Le certificat de radiation, délivré par le greffier, en exécution de l'article L. 143-20, contient les mêmes indications que celles qui sont prévues pour le certificat d'inscription mentionné à l'article R. 143-11.
