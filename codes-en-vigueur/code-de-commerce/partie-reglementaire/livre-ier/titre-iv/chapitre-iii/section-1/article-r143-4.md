# Article R143-4

L'officier public commis pour procéder à la vente d'un fonds de commerce peut se faire délivrer par le greffier copie des actes de vente sous seing privé déposés au greffe. Il peut également se faire délivrer expédition des actes authentiques de vente.
