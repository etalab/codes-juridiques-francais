# Section 5 : Des charges locatives, impôts, taxes, redevances et travaux

- [Article R145-35](article-r145-35.md)
- [Article R145-36](article-r145-36.md)
- [Article R145-37](article-r145-37.md)
