# Article R145-23

Les contestations relatives à la fixation du prix du bail révisé ou renouvelé sont portées, quel que soit le montant du loyer, devant le président du tribunal de grande instance ou le juge qui le remplace. Il est statué sur mémoire.

Les autres contestations sont portées devant le tribunal de grande instance qui peut, accessoirement, se prononcer sur les demandes mentionnées à l'alinéa précédent.

La juridiction territorialement compétente est celle du lieu de la situation de l'immeuble.
