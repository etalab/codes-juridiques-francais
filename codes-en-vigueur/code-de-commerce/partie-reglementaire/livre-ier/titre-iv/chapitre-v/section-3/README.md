# Section 3 : De la procédure.

- [Article R145-23](article-r145-23.md)
- [Article R145-24](article-r145-24.md)
- [Article R145-25](article-r145-25.md)
- [Article R145-26](article-r145-26.md)
- [Article R145-27](article-r145-27.md)
- [Article R145-28](article-r145-28.md)
- [Article R145-29](article-r145-29.md)
- [Article R145-30](article-r145-30.md)
- [Article R145-31](article-r145-31.md)
- [Article R145-32](article-r145-32.md)
- [Article R145-33](article-r145-33.md)
