# Sous-section 3 : Des activités commerciales et artisanales ambulantes.

- [Article R123-208-1](article-r123-208-1.md)
- [Article R123-208-2](article-r123-208-2.md)
- [Article R123-208-3](article-r123-208-3.md)
- [Article R123-208-4](article-r123-208-4.md)
- [Article R123-208-5](article-r123-208-5.md)
- [Article R123-208-6](article-r123-208-6.md)
- [Article R123-208-7](article-r123-208-7.md)
- [Article R123-208-8](article-r123-208-8.md)
