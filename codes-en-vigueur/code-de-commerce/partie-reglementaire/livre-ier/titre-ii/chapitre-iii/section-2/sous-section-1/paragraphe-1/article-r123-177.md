# Article R123-177

L'inventaire est un relevé de tous les éléments d'actif et de passif au regard desquels sont mentionnées la quantité et la valeur de chacun d'eux à la date d'inventaire.

Les données d'inventaire sont regroupées sur le livre d'inventaire et distinguées selon la nature et le mode d'évaluation des éléments qu'elles représentent. Le livre d'inventaire est suffisamment détaillé pour justifier le contenu de chacun des postes du bilan.

Les comptes annuels sont transcrits chaque année sur le livre d'inventaire, sauf lorsqu'ils sont publiés en annexe au registre du commerce et des sociétés conformément à l'article R. 123-111.
