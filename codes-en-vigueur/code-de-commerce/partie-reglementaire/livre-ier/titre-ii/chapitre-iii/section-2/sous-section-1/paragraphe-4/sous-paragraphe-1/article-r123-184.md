# Article R123-184

Constituent des participations les droits dans le capital d'autres personnes morales, matérialisés ou non par des titres, qui, en créant un lien durable avec celles-ci, sont destinés à contribuer à l'activité de la société détentrice.
