# Sous-section 1 : Des obligations comptables applicables à tous les commerçants

- [Paragraphe 1 : Des livres, documents et pièces comptables obligatoires.](paragraphe-1)
- [Paragraphe 2 : Des méthodes d'évaluation des éléments chiffrés.](paragraphe-2)
- [Paragraphe 3 : Des amortissements et provisions.](paragraphe-3)
- [Paragraphe 4 : De la constitution des comptes.](paragraphe-4)
- [Paragraphe 5 : De la présentation comptable simplifiée.](paragraphe-5)
