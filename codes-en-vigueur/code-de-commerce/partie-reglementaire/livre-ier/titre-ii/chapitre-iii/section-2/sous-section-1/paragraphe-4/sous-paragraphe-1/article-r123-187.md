# Article R123-187

Les frais d'établissement ainsi que les frais de recherche appliquée et de développement sont amortis selon un plan et dans un délai maximal de cinq ans. A titre exceptionnel et pour des projets particuliers, les frais de recherche appliquée et de développement peuvent être amortis sur une période plus longue qui n'excède pas la durée d'utilisation de ces actifs : il en est justifié à l'annexe.

Tant que ces postes ne sont pas apurés, il ne peut être procédé à aucune distribution de dividendes sauf si le montant des réserves libres est au moins égal à celui des frais non amortis.
