# Sous-section 2 : Des obligations comptables applicables à certains commerçants, personnes physiques.

- [Article D123-205-1](article-d123-205-1.md)
- [Article R123-203](article-r123-203.md)
- [Article R123-204](article-r123-204.md)
- [Article R123-207](article-r123-207.md)
- [Article R123-208](article-r123-208.md)
