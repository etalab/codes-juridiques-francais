# Article D123-235

Le numéro unique d'identification qui seul peut être exigé d'une entreprise dans ses relations avec les administrations, personnes ou organismes énumérés à l'article 1er de la loi n° 94-126 du 11 février 1994 relative à l'initiative et à l'entreprise individuelle est le numéro d'identité qui lui est attribué lors de son inscription au répertoire des entreprises et de leurs établissements en application de la sous-section 2.
