# Sous-section 4 : Des mentions sur les papiers d'affaires.

- [Article R123-237](article-r123-237.md)
- [Article R123-237-1](article-r123-237-1.md)
- [Article R123-238](article-r123-238.md)
