# Article R123-228

La radiation des commerçants, personnes physiques ou morales, soumis à l'immatriculation au registre du commerce et des sociétés, des agents commerciaux, personnes physiques ou morales, soumis à l'immatriculation au registre spécial mentionné à l'article R. 134-6 et des entrepreneurs individuels à responsabilité limitée immatriculés au registre spécial mentionné à l'article R. 526-15, ne peut intervenir que lorsque la radiation de ces registres a été faite.
