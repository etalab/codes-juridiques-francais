# Article R123-230

En cas de pluralité d'immatriculations au registre du commerce et des sociétés, au registre spécial des agents commerciaux, au registre de l'agriculture, au registre spécial des entrepreneurs individuels à responsabilité limitée ou au répertoire des métiers, la radiation ne peut intervenir que postérieurement à la radiation de tous les registres ou du répertoire en cause.
