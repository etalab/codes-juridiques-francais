# Paragraphe 2 : Des déclarations incombant aux personnes tenues à l'immatriculation

- [Sous-paragraphe 1 : Des déclarations incombant aux personnes physiques](sous-paragraphe-1)
- [Sous-paragraphe 2 : Des déclarations incombant aux personnes morales](sous-paragraphe-2)
- [Sous-paragraphe 3 : Des déclarations incombant aux représentations ou agences commerciales des Etats, collectivités ou établissements publics étrangers.](sous-paragraphe-3)
- [Sous-paragraphe 4 : Dispositions communes.](sous-paragraphe-4)
