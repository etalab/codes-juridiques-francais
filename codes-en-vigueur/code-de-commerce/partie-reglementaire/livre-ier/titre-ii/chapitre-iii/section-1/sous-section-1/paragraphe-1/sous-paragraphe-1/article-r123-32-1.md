# Article R123-32-1

Les personnes physiques dispensées, en application de l'article L. 123-1-1, de l'obligation d'immatriculation au registre du commerce et des sociétés peuvent néanmoins, à tout moment, demander à y être immatriculées.

Les personnes qui cessent de remplir les conditions de la dispense doivent demander leur immatriculation dans un délai de deux mois à compter de la date à laquelle elles ont perdu le bénéfice du régime prévu par l'article L. 133-6-8 du code de la sécurité sociale.
