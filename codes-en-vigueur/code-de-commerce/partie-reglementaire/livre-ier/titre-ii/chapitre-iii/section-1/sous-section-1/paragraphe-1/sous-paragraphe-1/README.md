# Sous-paragraphe 1 : De l'obligation d'immatriculation des personnes physiques.

- [Article R123-32](article-r123-32.md)
- [Article R123-32-1](article-r123-32-1.md)
- [Article R123-33](article-r123-33.md)
- [Article R123-34](article-r123-34.md)
