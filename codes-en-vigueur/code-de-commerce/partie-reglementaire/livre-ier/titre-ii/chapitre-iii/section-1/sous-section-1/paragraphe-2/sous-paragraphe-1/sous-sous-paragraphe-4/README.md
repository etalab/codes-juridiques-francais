# Sous-sous-paragraphe 4 : De la déclaration aux fins de radiation.

- [Article R123-51](article-r123-51.md)
- [Article R123-52](article-r123-52.md)
