# Paragraphe 1 : Dispositions générales.

- [Article R123-79](article-r123-79.md)
- [Article R123-80](article-r123-80.md)
- [Article R123-81](article-r123-81.md)
- [Article R123-82](article-r123-82.md)
- [Article R123-83](article-r123-83.md)
