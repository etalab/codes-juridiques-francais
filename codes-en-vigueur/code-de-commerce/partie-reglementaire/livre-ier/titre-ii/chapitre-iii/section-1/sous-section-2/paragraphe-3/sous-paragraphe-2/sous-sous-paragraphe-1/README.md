# Sous-sous-paragraphe 1 : Des sociétés ouvrant un premier établissement en France.

- [Article R123-112](article-r123-112.md)
- [Article R123-113](article-r123-113.md)
- [Article R123-114](article-r123-114.md)
