# Sous-sous-paragraphe 3 : Des sociétés européennes.

- [Article R123-118](article-r123-118.md)
- [Article R123-119](article-r123-119.md)
- [Article R123-120](article-r123-120.md)
