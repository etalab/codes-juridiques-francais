# Sous-paragraphe 2 : Des dépôts incombant aux sociétés dont le siège est à l'étranger

- [Sous-sous-paragraphe 1 : Des sociétés ouvrant un premier établissement en France.](sous-sous-paragraphe-1)
- [Sous-sous-paragraphe 3 : Des sociétés européennes.](sous-sous-paragraphe-3)
- [Sous-sous-paragraphe 4 : De la langue des dépôts.](sous-sous-paragraphe-4)
