# Article R123-30

Les annexes 1-1 et 1-2 au présent livre précisent les déclarations visées au 1° du I de l'article R. 123-1 devant être déposées aux centres de formalités des entreprises et les administrations, personnes ou organismes destinataires de ces formalités selon leur compétence.

Ces annexes peuvent être complétées par arrêté des ministres chargés de la justice, des transports, des affaires sociales, du travail, de l'économie, de l'industrie, de l'agriculture, du commerce et de l'artisanat, de la réforme administrative et du budget.
