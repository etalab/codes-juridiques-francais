# Chapitre Ier : Des groupements d'intérêt économique de droit français.

- [Article R251-1](article-r251-1.md)
- [Article R251-2](article-r251-2.md)
- [Article R251-3](article-r251-3.md)
