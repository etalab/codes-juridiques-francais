# Article R228-10

Pour l'application de la dernière phrase du neuvième alinéa de l'article L. 228-1, l'inscription au compte de l'acheteur est faite à la date fixée par l'accord des parties et notifiée à la société émettrice.
