# Article R228-14

Pour l'application de l'article L. 228-6-3, la vente a lieu, selon les modalités prévues à l'article R. 228-12, à l'expiration d'un délai d'un an après la publicité effectuée dans les conditions prévues à l'article R. 228-11 si, pendant cette période, les personnes au nom desquelles l'inscription a été faite ou leurs ayants droit n'ont pu être atteintes par l'avis mentionné à l'article R. 228-11 adressé par lettre recommandée avec demande d'avis de réception.
