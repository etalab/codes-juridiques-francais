# Sous-section 3 : De la défaillance de l'actionnaire.

- [Article R228-24](article-r228-24.md)
- [Article R228-25](article-r228-25.md)
- [Article R228-26](article-r228-26.md)
