# Sous-section 2 : Des clauses d'agrément de la cession de titres de capital ou de valeurs mobilières donnant accès au capital.

- [Article R228-23](article-r228-23.md)
