# Article R225-104

Les seuils prévus au sixième alinéa de l'article L. 225-102-1 sont fixés à 100 millions d'euros pour le total du bilan, à 100 millions d'euros pour le montant net du chiffre d'affaires et à 500 pour le nombre moyen de salariés permanents employés au cours de l'exercice.

Le total du bilan, le montant net du chiffre d'affaires et le nombre moyen de salariés permanents employés au cours de l'exercice sont déterminés conformément aux quatrième, cinquième et sixième alinéas de l'article R. 123-200.
