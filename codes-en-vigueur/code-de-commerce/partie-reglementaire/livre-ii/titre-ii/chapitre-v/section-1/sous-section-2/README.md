# Sous-section 2 : De la constitution sans offre au public.

- [Article R225-13](article-r225-13.md)
- [Article R225-14](article-r225-14.md)
- [Article R225-14-1](article-r225-14-1.md)
