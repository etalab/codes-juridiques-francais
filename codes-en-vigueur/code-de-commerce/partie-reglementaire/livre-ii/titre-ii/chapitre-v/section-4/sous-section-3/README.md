# Sous-section 3 : De l'amortissement du capital.

- [Article R225-146](article-r225-146.md)
- [Article R225-147](article-r225-147.md)
- [Article R225-148](article-r225-148.md)
- [Article R225-149](article-r225-149.md)
