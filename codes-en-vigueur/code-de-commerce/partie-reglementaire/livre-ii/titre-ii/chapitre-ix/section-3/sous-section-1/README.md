# Sous-section 1 : De la constitution par fusion.

- [Article D229-13](article-d229-13.md)
- [Article D229-13-1](article-d229-13-1.md)
- [Article D229-13-2](article-d229-13-2.md)
- [Article R229-14](article-r229-14.md)
