# Section 3 : De la constitution de la société européenne.

- [Sous-section 1 : De la constitution par fusion.](sous-section-1)
- [Sous-section 2 : De la constitution d'une société européenne holding.](sous-section-2)
- [Sous-section 3 : De la constitution par transformation d'une société anonyme.](sous-section-3)
