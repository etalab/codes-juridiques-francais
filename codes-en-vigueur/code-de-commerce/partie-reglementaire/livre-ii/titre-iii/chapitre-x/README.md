# Chapitre X : De l'information des salariés en cas de cession de leur société

- [Article D23-10-1](article-d23-10-1.md)
- [Article D23-10-2](article-d23-10-2.md)
- [Article D23-10-3](article-d23-10-3.md)
