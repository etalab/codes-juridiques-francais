# Article D23-10-1

Le délai de deux mois mentionné au premier alinéa de l'article L. 23-10-1 du présent code s'apprécie au regard de la date de cession, entendue comme la date à laquelle s'opère le transfert de propriété.
