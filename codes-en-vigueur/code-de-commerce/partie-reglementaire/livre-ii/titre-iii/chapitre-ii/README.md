# Chapitre II : Des comptes sociaux.

- [Section 1 : Des documents comptables.](section-1)
- [Section 2 : Dispositions particulières aux sociétés dont les actions sont admises à la négociation sur un marché réglementé et à certaines de leurs filiales.](section-2)
- [Section 3 : Des bénéfices.](section-3)
- [Section 4 : De la publicité des comptes.](section-4)
