# Section 4 : De la publicité des comptes.

- [Article R232-19](article-r232-19.md)
- [Article R232-19-1](article-r232-19-1.md)
- [Article R232-20](article-r232-20.md)
- [Article R232-20-1](article-r232-20-1.md)
- [Article R232-21](article-r232-21.md)
- [Article R232-21-1](article-r232-21-1.md)
- [Article R232-22](article-r232-22.md)
