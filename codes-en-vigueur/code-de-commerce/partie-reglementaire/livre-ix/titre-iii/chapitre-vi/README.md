# Chapitre VI : Dispositions d'adaptation du livre VI.

- [Article R936-1](article-r936-1.md)
- [Article R936-2](article-r936-2.md)
