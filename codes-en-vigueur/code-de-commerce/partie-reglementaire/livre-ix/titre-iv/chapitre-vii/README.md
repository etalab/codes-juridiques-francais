# Chapitre VII : Dispositions d'adaptation du livre VII.

- [Article D947-2](article-d947-2.md)
- [Article R947-1](article-r947-1.md)
- [Article R947-3](article-r947-3.md)
- [Article R947-4](article-r947-4.md)
- [Article R947-5](article-r947-5.md)
- [Article R947-6](article-r947-6.md)
- [Article R947-7](article-r947-7.md)
- [Article R947-8](article-r947-8.md)
- [Article R947-9](article-r947-9.md)
