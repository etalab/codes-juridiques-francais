# Article R910-1

Ne sont pas applicables à Saint-Pierre-et-Miquelon les dispositions suivantes :

1° Les articles R. 121-3, R. 121-4, R. 123-209 à R. 123-219, D. 145-12 à D. 145-19, D. 146-1 et D. 146-2 ;

2° Les articles R. 229-1 à R. 229-26 et R. 252-1 ;

3° Les articles R. 470-2 à R. 470-7 ;

4° Les articles R. 522-1 à R. 522-25 ;

5° Les articles R. 670-1 à R. 670-7 ;

6° Les articles R. 711-6, R. 711-18 à R. 711-31, R. 712-24, R. 713-31 à R. 713-63, D. 721-2 à R. 721-4 et R. 721-7 à R. 761-26 ainsi que les dispositions relatives aux chambres de commerce et d'industrie de région des chapitres Ier, II et III du titre Ier du livre VII.
