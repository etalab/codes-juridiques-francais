# TITRE Ier : Des liquidations, des ventes au déballage, des soldes et des ventes en magasins d'usine.

- [Section 1 : Des liquidations.](section-1)
- [Section 2 : Des ventes au déballage.](section-2)
- [Section 3 : Des soldes.](section-3)
- [Section 4 : Des ventes en magasins ou dépôts d'usine.](section-4)
- [Section 5 : Des sanctions.](section-5)
