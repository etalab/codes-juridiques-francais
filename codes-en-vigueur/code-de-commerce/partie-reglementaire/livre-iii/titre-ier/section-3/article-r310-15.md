# Article R310-15

La déclaration préalable mentionnée au troisième alinéa de l'article L. 310-3 est faite par établissement.

Elle est adressée par le commerçant au préfet du département, par lettre recommandée avec demande d'avis de réception, un mois au moins avant la date prévue pour le début de la vente ; ce délai commence à courir à compter de la date de son envoi.

La transmission de cette déclaration peut être effectuée par voie électronique. Dans ce cas, la déclaration donne lieu à la délivrance d'un avis de réception électronique. Le préfet veille à ce que cette transmission soit assurée de manière sécurisée, conformément à l'article 1316-1 du code civil.

Un arrêté du ministre chargé du commerce fixe la liste des informations contenues dans la déclaration et les modalités de la déclaration par voie électronique.
