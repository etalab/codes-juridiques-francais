# Article Annexe art. D310-15-3

LISTE DES ZONES MENTIONNÉES À L'ARTICLE D. 310-15-3

<div align="center">

<div align="center">

<br/>

<table>
<tbody>
<tr>
<th>DÉPARTEMENT <br/>
</th>
<th>
<br/>APPLICATION TERRITORIALE <br/>
</th>
<th>
<br/>DATES DE DÉBUT DES PÉRIODES DE SOLDES <br/>
</th>
</tr>
<tr>
<td align="center">
<br/>Alpes-Maritimes (06) <br/>
</td>
<td align="center">
<br/>Tout le département <br/>
</td>
<td align="center">
<br/>Soldes d'été : premier mercredi du mois de juillet <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Corse-du-Sud (2A) <br/>
</td>
<td align="center">
<br/>Tout le département <br/>
</td>
<td align="center">
<br/>Soldes d'été : deuxième mercredi du mois de juillet <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Haute-Corse (2B) <br/>
</td>
<td align="center">
<br/>Tout le département <br/>
</td>
<td align="center">
<br/>Soldes d'été : deuxième mercredi du mois de juillet <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Meurthe-et-Moselle (54) <br/>
</td>
<td align="center">
<br/>Tout le département <br/>
</td>
<td align="center">
<br/>Soldes d'hiver : premier jour ouvré du mois de janvier <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Meuse (55) <br/>
</td>
<td align="center">
<br/>Tout le département <br/>
</td>
<td align="center">
<br/>Soldes d'hiver : premier jour ouvré du mois de janvier <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Moselle (57) <br/>
</td>
<td align="center">
<br/>Tout le département <br/>
</td>
<td align="center">
<br/>Soldes d'hiver : premier jour ouvré du mois de janvier <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Pyrénées-Orientales (66) <br/>
</td>
<td align="center">
<br/>Tout le département <br/>
</td>
<td align="center">
<br/>Soldes d'été : premier mercredi du mois de juillet <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Vosges (88) <br/>
</td>
<td align="center">
<br/>Tout le département <br/>
</td>
<td align="center">
<br/>Soldes d'hiver : premier jour ouvré du mois de janvier <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Guadeloupe (971) <br/>
</td>
<td align="center">
<br/>
<br/>
</td>
<td align="center">
<br/>Soldes d'hiver : premier samedi de janvier <br/>
<br/>Soldes d'été : dernier samedi de septembre <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Martinique (972) <br/>
</td>
<td align="center">
<br/>Tout le département <br/>
</td>
<td align="center">
<br/>Soldes d'été : premier jeudi d'octobre <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Guyane (973) <br/>
</td>
<td align="center">
<br/>Tout le département <br/>
</td>
<td align="center">
<br/>Soldes d'hiver : premier mercredi du mois de janvier <br/>
<br/>Soldes d'été : premier jeudi du mois d'octobre <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>La Réunion (974) <br/>
</td>
<td align="center">
<br/>Tout le département <br/>
</td>
<td align="center">
<br/>Soldes d'hiver : premier samedi du mois de septembre <br/>
<br/>Soldes d'été : premier samedi du mois de février. <br/>
</td>
</tr>
</tbody>
</table>
<br/>
<br/>

<table>
<tbody>
<tr>
<th>
<br/>COLLECTIVITÉ <br/>
<br/>d'outre-mer (COM) <br/>
</th>
<th>
<br/>APPLICATION TERRITORIALE <br/>
</th>
<th>
<br/>DATES DE DÉBUT DES PÉRIODES DE SOLDES <br/>
</th>
</tr>
<tr>
<td align="center">
<br/>Saint-Pierre-et-Miquelon (975) <br/>
</td>
<td align="center">
<br/>Toute la collectivité <br/>
</td>
<td align="center">
<br/>Soldes d'été : premier mercredi après le 14 juillet <br/>
<br/>Soldes d'hiver : premier mercredi après le 15 janvier <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Saint-Barthélemy (977) <br/>
</td>
<td align="center">
<br/>Toute la collectivité <br/>
</td>
<td align="center">
<br/>Soldes d'hiver : premier samedi de mai <br/>
<br/>Soldes d'été : deuxième samedi d'octobre <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Saint-Martin (978) <br/>
</td>
<td align="center">
<br/>Toute la collectivité <br/>
</td>
<td align="center">
<br/>Soldes d'hiver : premier samedi de mai <br/>
<br/>Soldes d'été : deuxième samedi d'octobre<br/>
</td>
</tr>
</tbody>
</table>

<br/>

<br/>

</div>

</div>
