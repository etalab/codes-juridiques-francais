# Paragraphe 3 : Du recours contre les décisions du conseil ou de son président.

- [Article R321-50](article-r321-50.md)
- [Article R321-51](article-r321-51.md)
- [Article R321-52](article-r321-52.md)
- [Article R321-53](article-r321-53.md)
- [Article R321-54](article-r321-54.md)
- [Article R321-55](article-r321-55.md)
