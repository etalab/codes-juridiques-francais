# Article R321-55

La décision de la cour d'appel est notifiée, à la diligence du greffe, par lettre recommandée avec demande d'avis de réception aux parties, au commissaire du Gouvernement et au procureur général.
