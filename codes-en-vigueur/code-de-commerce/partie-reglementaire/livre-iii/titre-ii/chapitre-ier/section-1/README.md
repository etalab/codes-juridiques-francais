# Section 1 : Dispositions générales.

- [Sous-section 1 : Les opérateurs de ventes volontaires de meubles aux enchères publiques.](sous-section-1)
- [Sous-section 2 : Le Conseil des ventes volontaires de meubles aux enchères publiques.](sous-section-2)
