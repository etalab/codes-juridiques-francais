# Article R321-18-1

Pour pouvoir, en application de l'article L. 321-2, diriger des ventes volontaires de meubles aux enchères publiques, les notaires et les huissiers de justice doivent, au préalable, avoir suivi, à leurs frais, une formation d'une durée de soixante heures portant sur la réglementation, la pratique et la déontologie des ventes aux enchères.

Cette formation est organisée par le Conseil des ventes volontaires de meubles aux enchères publiques après avis du Conseil supérieur du notariat et de la Chambre nationale des huissiers de justice.

Les notaires assistants et les huissiers de justice stagiaires sont admis à suivre cette formation.

Au terme de la formation, le Conseil des ventes volontaires de meubles aux enchères publiques délivre aux participants un certificat d'accomplissement de formation.
