# Article R321-28

Le Conseil des ventes volontaires de meubles aux enchères publiques procède à l'affectation des stagiaires. Celle-ci est réalisée sur avis de la Chambre nationale des commissaires-priseurs judiciaires, pour les stages dans les offices de commissaire-priseur judiciaire et sur avis du Conseil national des courtiers de marchandises assermentés, pour ceux effectués chez les courtiers de marchandises assermentés.
