# LIVRE III : De certaines formes de ventes et des clauses d'exclusivité.

- [TITRE Ier : Des liquidations, des ventes au déballage, des soldes et des ventes en magasins d'usine.](titre-ier)
- [TITRE II : Des ventes aux enchères publiques.](titre-ii)
- [TITRE III : Des clauses d'exclusivité.](titre-iii)
