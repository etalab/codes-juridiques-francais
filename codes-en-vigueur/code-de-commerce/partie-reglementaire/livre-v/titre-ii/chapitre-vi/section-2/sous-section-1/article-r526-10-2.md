# Article R526-10-2

Lorsque l'entrepreneur individuel entre dans les prévisions de la dernière phrase du 7° de l'article R. 526-3, la valeur qu'il déclare en application de cette disposition est retenue pour les besoins des obligations comptables prévues aux articles L. 526-13 et L. 526-14.
