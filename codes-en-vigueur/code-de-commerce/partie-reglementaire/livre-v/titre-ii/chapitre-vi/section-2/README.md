# Section 2 : De l'entrepreneur individuel à responsabilité limitée.

- [Sous-section 1 : Dispositions communes.](sous-section-1)
- [Sous-section 2 : Du registre spécial des entrepreneurs individuels à responsabilité limitée.](sous-section-2)
