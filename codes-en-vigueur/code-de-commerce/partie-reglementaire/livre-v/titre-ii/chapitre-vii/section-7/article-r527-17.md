# Article R527-17

La mise en demeure prévue au troisième alinéa de l'article L. 527-7 est faite par lettre recommandée avec demande d'avis de réception adressée par le créancier au constituant. Celui-ci dispose d'un délai de quinze jours pour y satisfaire.
