# Chapitre VII : Du gage des stocks.

- [Section 1 : Des formalités d'inscription.](section-1)
- [Section 2 : Des formalités modificatives.](section-2)
- [Section 3 : Des effets de l'inscription.](section-3)
- [Section 4 : De la radiation de l'inscription.](section-4)
- [Section 5 : Des obligations des greffiers.](section-5)
- [Section 6 : Des recours.](section-6)
- [Section 7 : Dispositions diverses.](section-7)
