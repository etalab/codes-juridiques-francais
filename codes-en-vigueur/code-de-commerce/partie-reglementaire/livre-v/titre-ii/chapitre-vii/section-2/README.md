# Section 2 : Des formalités modificatives.

- [Article R527-6](article-r527-6.md)
- [Article R527-7](article-r527-7.md)
- [Article R527-8](article-r527-8.md)
