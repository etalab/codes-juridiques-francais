# Article R527-2

Le bordereau en deux exemplaires est joint à l'acte mentionné à l'article R. 527-1.

Il comporte :

1° La désignation des parties :

a) Pour l'établissement de crédit ou la société de financement  créancier : sa forme, sa dénomination sociale, l'adresse de son siège social et son numéro unique d'identification complété par la mention RCS suivie du nom de la ville où se trouve le greffe où il est immatriculé ;

b) Pour le constituant :

- s'il s'agit d'une personne physique : ses nom, prénoms, date et lieu de naissance, domicile et l'indication du lieu d'exercice de son activité ou de son exploitation principale, ainsi que, le cas échéant, son numéro unique d'identification complété, s'il y a lieu, par la mention RCS suivie du nom de la ville où se trouve le greffe où elle est immatriculée ;

- s'il s'agit d'une personne morale : sa forme, sa dénomination sociale, l'adresse de son siège social et son numéro unique d'identification complété, le cas échéant, par la mention RCS suivie du nom de la ville où se trouve le greffe où elle est immatriculée ;

2° La date de l'acte constitutif du gage et l'indication qu'il porte sur des stocks ;

3° Le montant de la créance garantie en principal, la date de son exigibilité et l'indication du taux des intérêts ; pour les créances futures, le bordereau mentionne les éléments permettant de les déterminer ;

4° Une description des stocks présents ou futurs engagés, en nature, qualité, quantité et valeur, ainsi que, le cas échéant, la mention que la part des stocks engagés diminue à proportion du désintéressement du créancier ;

5° Le lieu de conservation des stocks engagés et, le cas échéant, la désignation du gardien.
