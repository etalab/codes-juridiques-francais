# Article R527-1

Pour inscrire son gage, le créancier remet ou adresse au greffier du tribunal de commerce dans le ressort duquel le constituant a son siège ou son domicile l'un des originaux de l'acte constitutif du gage ou une expédition s'il est établi sous forme authentique.
