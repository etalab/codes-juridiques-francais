# Article R527-3

Le dépôt de l'acte constitutif du gage est constaté sur un registre spécial tenu par le greffier, qui attribue à l'acte un numéro d'ordre.

Ce registre peut être tenu sous forme électronique. Dans ce cas, il est fait usage d'une signature électronique sécurisée dans les conditions prévues par l'article 1316-4 du code civil et le décret du 30 mars 2001 pris pour son application.
