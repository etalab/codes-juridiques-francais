# Article R521-1

Les dispositions des articles R. 322-3 et R. 322-6 sont applicables aux ventes prévues par l'article L. 521-3, sous réserve des dispositions de l'article R. 521-2.
