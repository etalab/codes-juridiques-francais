# Article R525-3

Le papier sur lequel sont établis les bordereaux est fourni par les greffiers aux frais des requérants. Toutefois, les officiers publics ou ministériels peuvent se le procurer eux-mêmes. Ces bordereaux contiennent :

1° Les nom, prénoms et domicile du créancier et du débiteur, leur profession s'ils en ont une ;

2° La date et la nature du titre ;

3° Le montant de la créance exprimée dans le titre, les conditions relatives aux intérêts et à l'exigibilité ;

4° Le lieu où le matériel est placé et éventuellement la mention que ledit matériel est susceptible d'être déplacé ;

5° Election du domicile par le créancier nanti dans le ressort du tribunal au greffe duquel l'inscription est requise.
