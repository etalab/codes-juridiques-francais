# Chapitre Ier : De la lettre de change.

- [Section 1 : Du paiement.](section-1)
- [Section 2 : Des protêts.](section-2)
