# Chapitre IV : De la discipline des juges des tribunaux de commerce

- [Section 1 : De la Commission nationale de discipline.](section-1)
- [Section 2 : De la procédure disciplinaire.](section-2)
