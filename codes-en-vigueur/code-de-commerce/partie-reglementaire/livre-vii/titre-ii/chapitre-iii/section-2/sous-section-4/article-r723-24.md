# Article R723-24

Les contestations relatives à l'électorat, à l'éligibilité et aux opérations électorales organisées en vue de la désignation des juges des tribunaux de commerce sont de la compétence du tribunal d'instance dans le ressort duquel se trouve situé le siège du tribunal de commerce.

Le tribunal d'instance statue en dernier ressort.
