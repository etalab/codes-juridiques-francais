# Sous-section 3 : Dispositions applicables aux marchés d'intérêt national installés sur le domaine privé d'une collectivité territoriale et à ceux installés sur des immeubles appartenant à des personnes privées.

- [Article R761-25](article-r761-25.md)
- [Article R761-26](article-r761-26.md)
