# Article R742-13

Le stagiaire cesse d'être inscrit sur le registre du stage soit à sa demande, soit après avoir subi avec succès l'examen d'aptitude aux fonctions de greffier de tribunal de commerce.
