# Sous-section 2 : Dispositions applicables aux sociétés civiles professionnelles

- [Paragraphe 1 : De la constitution de la société.](paragraphe-1)
- [Paragraphe 2 : Du fonctionnement de la société.](paragraphe-2)
- [Paragraphe 3 : De l'exercice des fonctions de greffier de tribunal de commerce par la société et les associés.](paragraphe-3)
- [Paragraphe 4 : De la dissolution et de la liquidation de la société.](paragraphe-4)
