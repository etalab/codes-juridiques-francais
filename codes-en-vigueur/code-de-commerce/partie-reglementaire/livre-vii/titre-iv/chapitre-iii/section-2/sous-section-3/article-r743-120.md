# Article R743-120

Les sociétés d'exercice libéral de greffiers de tribunaux de commerce à responsabilité limitée, à forme anonyme, en commandite par actions et par actions simplifiées, sont régies par les dispositions du livre II, sous réserve des dispositions de la présente section.
