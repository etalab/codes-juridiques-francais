# Paragraphe 1er : Dispositions générales

- [Article R743-139-1](article-r743-139-1.md)
- [Article R743-139-2](article-r743-139-2.md)
- [Article R743-139-3](article-r743-139-3.md)
- [Article R743-139-4](article-r743-139-4.md)
- [Article R743-139-5](article-r743-139-5.md)
- [Article R743-139-6](article-r743-139-6.md)
