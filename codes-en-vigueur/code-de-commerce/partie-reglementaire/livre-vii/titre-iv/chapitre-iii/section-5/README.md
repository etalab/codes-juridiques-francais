# Section 5 : Compte affecté aux fonds détenus pour le compte de tiers

- [Article R743-178](article-r743-178.md)
- [Article R743-179](article-r743-179.md)
- [Article R743-180](article-r743-180.md)
- [Article R743-181](article-r743-181.md)
- [Article R743-182](article-r743-182.md)
