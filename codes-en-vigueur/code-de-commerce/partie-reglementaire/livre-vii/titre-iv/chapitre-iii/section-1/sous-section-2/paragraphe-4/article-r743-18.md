# Article R743-18

Le procureur de la République notifie sans délai à l'administrateur la décision qui l'a nommé. Si l'administrateur n'est pas greffier de tribunal de commerce en exercice, il prête serment devant le tribunal de commerce auprès duquel il exercera sa mission.

L'administrateur prend ses fonctions à compter, selon le cas, soit de la notification qui lui est faite de la décision l'ayant nommé, soit de sa prestation de serment.
