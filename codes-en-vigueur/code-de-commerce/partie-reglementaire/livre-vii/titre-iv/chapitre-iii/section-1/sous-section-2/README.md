# Sous-section 2 : De la discipline.

- [Paragraphe 1 : De l'enquête disciplinaire.](paragraphe-1)
- [Paragraphe 2 : De la procédure devant la formation disciplinaire du Conseil national des greffiers des tribunaux de commerce.](paragraphe-2)
- [Paragraphe 3 : De la procédure devant le tribunal de grande instance statuant disciplinairement.](paragraphe-3)
- [Paragraphe 4 : De l'administration provisoire.](paragraphe-4)
- [Paragraphe 5 : De la suspension provisoire.](paragraphe-5)
- [Paragraphe 6 : Des voies de recours.](paragraphe-6)
- [Article R743-5](article-r743-5.md)
