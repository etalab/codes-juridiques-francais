# Sous-section 5 : De la procédure de consultation prévue à l'article L. 752-4

- [Article R752-21](article-r752-21.md)
- [Article R752-22](article-r752-22.md)
- [Article R752-23](article-r752-23.md)
- [Article R752-24](article-r752-24.md)
- [Article R752-25](article-r752-25.md)
- [Article R752-26](article-r752-26.md)
- [Article R752-27](article-r752-27.md)
- [Article R752-28](article-r752-28.md)
- [Article R752-29](article-r752-29.md)
