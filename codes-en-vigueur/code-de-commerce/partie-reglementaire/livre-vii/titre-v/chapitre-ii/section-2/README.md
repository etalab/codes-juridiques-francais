# Section 2 : De la décision ou avis de la commission départementale.

- [Sous-section 1 : De la demande d'autorisation d'exploitation commerciale.](sous-section-1)
- [Sous-section 2 : Du dépôt des demandes de permis de construire valant autorisation d'exploitation commerciale.](sous-section-2)
- [Sous-section 3 : Du dépôt des demandes d'autorisation d'exploitation commerciale ne nécessitant pas un permis de construire.](sous-section-3)
- [Sous-section 4 : De la réunion de la commission départementale d'aménagement commercial](sous-section-4)
- [Sous-section 5 : De la procédure de consultation prévue à l'article L. 752-4](sous-section-5)
