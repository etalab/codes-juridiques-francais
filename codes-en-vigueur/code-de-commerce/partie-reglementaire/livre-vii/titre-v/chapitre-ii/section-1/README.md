# Section 1 : Des projets soumis à autorisation

- [Article R752-1](article-r752-1.md)
- [Article R752-2](article-r752-2.md)
- [Article R752-3](article-r752-3.md)
