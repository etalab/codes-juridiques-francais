# Article R711-33

I.-Les chambres de commerce et d'industrie de région fournissent l'avis demandé par le conseil régional sur tout dispositif d'assistance aux créateurs et repreneurs d'entreprises et aux entreprises dont la région envisage la création.

Elles peuvent être consultées par l'Etat, la région et leurs établissements publics sur toute question relative à l'activité et au développement économique, à la formation professionnelle, à l'aménagement du territoire et à l'environnement de la circonscription régionale.

Elles peuvent, de leur propre initiative, émettre des avis et des vœux sur ces mêmes questions.

Les chambres de commerce et d'industrie territoriales sont informées des avis rendus en application des alinéas qui précèdent par la chambre de commerce et d'industrie de région de rattachement de leur circonscription.

II.-Conformément au 6° de l'article L. 711-8, les chambres de commerce et d'industrie de région assurent des fonctions d'appui et de soutien pour le compte des chambres de leur circonscription. Parmi ces missions figurent au moins les suivantes :

1° Service de paie des agents administratifs ;

2° Services de comptabilité, informatique, juridique ;

3° Outils ou contrats portant sur les frais téléphoniques, l'assurance, la maintenance, l'informatique ;

4° Services de formation mutualisés ;

5° Mise en place d'une politique régionale de communication ;

6° Pôles régionaux spécialisés dans l'action économique, l'intelligence économique, l'innovation, l'environnement et le développement international ;

7° Catégories d'achats définis par l'assemblée générale de la chambre de commerce et d'industrie de région ;

8° Missions d'audit sur un sujet d'intérêt commun à tout ou partie des chambres de la circonscription.

Le champ de ces missions ne couvre pas obligatoirement les services publics industriels et commerciaux gérés par les établissements publics du réseau.

Conformément au I de l'article L. 711-10, elles peuvent déléguer une partie de ces fonctions de soutien, à l'exception de la paie qui est centralisée à leur niveau, à l'une des chambres qui leur sont rattachées, mais sans qu'une fonction de soutien puisse être fractionnée, ou déléguée à plusieurs chambres rattachées.
