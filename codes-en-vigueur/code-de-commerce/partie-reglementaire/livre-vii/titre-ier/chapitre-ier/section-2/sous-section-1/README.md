# Sous-section 1 : Des compétences.

- [Article D711-34-1](article-d711-34-1.md)
- [Article D711-34-2](article-d711-34-2.md)
- [Article D711-34-3](article-d711-34-3.md)
- [Article R711-33](article-r711-33.md)
- [Article R711-34](article-r711-34.md)
