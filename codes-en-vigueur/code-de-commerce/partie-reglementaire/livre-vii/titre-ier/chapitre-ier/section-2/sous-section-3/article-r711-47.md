# Article R711-47

I. - Avant le 20 avril de l'année du renouvellement des chambres, un arrêté du préfet de la région où est situé le siège de la chambre de commerce et d'industrie de région détermine le nombre des membres de cette chambre et le nombre des sièges attribués en son sein aux élus de chacune des chambres de commerce et d'industrie territoriales qui lui sont rattachées ainsi, le cas échéant, des élus des chambres de commerce et d'industrie territoriales qui sont représentées à son assemblée générale en application des dispositions de l'article R. 711-46.

Le nombre des membres de la chambre de commerce et d'industrie de région est de 30 au minimum et de 100 au maximum. Ce nombre est déterminé sur proposition de la chambre de commerce et d'industrie de région en tenant compte des éléments économiques issus de l'étude prévue à l'article R. 713-66.

II. - Au sein de la chambre de commerce et d'industrie de région, la répartition des sièges attribués à chaque chambre de commerce et d'industrie territoriale est établie à la moyenne, arrondie à l'unité la plus proche, des proportions représentées par chacune d'elles au sein de l'ensemble, mesurées par le nombre des ressortissants, leurs bases de cotisation foncière des entreprises et leurs effectifs salariés. Ces proportions sont fondées sur l'étude économique de pondération régie par l'article R. 713-66.

Toutefois, aucune chambre de commerce et d'industrie territoriale ne peut disposer de moins de trois sièges, qui doivent être attribués à des représentants de chacune des catégories.

En outre, aucune chambre de commerce et d'industrie territoriale ne peut disposer de plus de 40 % des sièges, sauf lorsque la chambre de commerce et d'industrie de région ne comporte que deux chambres territoriales.

Les effets des dispositions des deux alinéas précédents sont répercutés sur la représentation des autres chambres au sein de la chambre de commerce et d'industrie de région, en suivant la règle de proportionnalité énoncée au premier alinéa.

III. - Pour tenir compte des particularités locales, le préfet de région peut s'écarter, en ce qui concerne le nombre des sièges attribués aux différentes catégories, de la moyenne des proportions définie au II ci-dessus, dans la limite du dixième des sièges à pourvoir.

Lorsqu'il fait application de l'alinéa précédent, le préfet de région en informe les préfets de département intéressés.

IV. - Au sein de la chambre de commerce et d'industrie de région de Corse, le même nombre de sièges est attribué à chacune des deux chambres de commerce et d'industrie territoriales.
