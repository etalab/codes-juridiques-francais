# Sous-section 3 : De l'organisation et du fonctionnement.

- [Article R711-46](article-r711-46.md)
- [Article R711-47](article-r711-47.md)
- [Article R711-47-1](article-r711-47-1.md)
- [Article R711-47-2](article-r711-47-2.md)
- [Article R711-48](article-r711-48.md)
- [Article R711-49](article-r711-49.md)
- [Article R711-50](article-r711-50.md)
- [Article R711-51](article-r711-51.md)
- [Article R711-52](article-r711-52.md)
