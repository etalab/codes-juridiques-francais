# Article D711-9

Les chambres de commerce et d'industrie territoriales établissent annuellement, dans le cadre de leur rapport d'activité, un relevé des indicateurs d'activité, de qualité et de performance, prévus à l'article D. 711-56-1, qu'elles transmettent à la chambre de commerce et d'industrie de région et à l'assemblée des chambres françaises de commerce et d'industrie.
