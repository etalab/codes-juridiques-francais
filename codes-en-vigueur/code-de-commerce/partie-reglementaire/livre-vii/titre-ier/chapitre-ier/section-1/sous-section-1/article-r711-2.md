# Article R711-2

Les chambres de commerce et d'industrie territoriales sont instituées par décret sur la proposition du ministre chargé de leur tutelle. L'avis du conseil municipal de la commune désignée pour être le siège de la nouvelle chambre ainsi que celui du conseil départemental et des chambres de commerce et d'industrie territoriales du ou des départements sur le territoire desquels s'étend sa circonscription sont préalablement demandés.
