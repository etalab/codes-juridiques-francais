# Chapitre Ier : De l'organisation et des missions du réseau des chambres de commerce et d'industrie.

- [Section 1 : Des chambres de commerce et d'industrie territoriales et départementales d'Ile-de-France](section-1)
- [Section 2 : Des chambres de commerce et d'industrie de région](section-2)
- [Section 3 : De l'assemblée des chambres françaises de commerce et d'industrie.](section-3)
- [Section 4 : Dispositions communes.](section-4)
