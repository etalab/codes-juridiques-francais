# Article R711-55

L'assemblée des chambres françaises de commerce et d'industrie élabore la stratégie nationale du réseau des chambres de commerce et d'industrie et effectue sur le plan national la synthèse des positions adoptées par les chambres de commerce et d'industrie territoriales, départementales d'Ile-de-France et de région.

Elle peut se voir confier la gestion de service à l'usage du commerce et de l'industrie lorsque cette gestion ne peut être convenablement assumée au plan régional ou local.

L'assemblée des chambres françaises de commerce et d'industrie peut confier la maîtrise d'ouvrage de la gestion des projets de portée nationale décidés par son assemblée à un autre établissement du réseau, sans délibération de son assemblée générale, mais après avis conforme de son comité directeur. Ce transfert donne alors lieu à établissement d'une convention avec cet établissement, renouvelable de manière expresse tous les trois ans.

L'assemblée des chambres françaises de commerce et d'industrie coordonne l'action des établissements du réseau en tant qu'autorités compétentes dans le cadre des procédures de coopération administrative mentionnées à l'article D. 711-10-1 ; les ministères concernés sont, le cas échéant, associés à cette coordination.

L'assemblée des chambres françaises de commerce et d'industrie constitue une centrale d'achat au sens du code des marchés publics.
