# Section 3 : Dispositions communes.

- [Article R713-63](article-r713-63.md)
- [Article R713-64](article-r713-64.md)
- [Article R713-65](article-r713-65.md)
- [Article R713-66](article-r713-66.md)
- [Article R713-67](article-r713-67.md)
- [Article R713-70](article-r713-70.md)
- [Article R713-71](article-r713-71.md)
