# Article R713-14

I.-La commission prévue à l'article L. 713-17 est chargée :

1° De vérifier la conformité des bulletins de vote et des circulaires aux dispositions de l'arrêté prévu à l'article R. 713-15 ;

2° D'expédier aux électeurs, au plus tard treize jours avant le dernier jour du scrutin, les circulaires et bulletins de vote des candidats de leur catégorie, ainsi que les instruments nécessaires au vote ;

3° D'organiser la réception des votes ;

4° D'organiser le dépouillement et le recensement des votes ;

5° De proclamer les résultats.

II.-Pour assurer ces opérations, le président de la commission peut solliciter le concours de la chambre de commerce et d'industrie territoriale.

Les envois mentionnés au 2° qui ne sont pas parvenus à leur destinataire sont retournés par les entreprises chargées de l'acheminement du courrier à la préfecture, qui les conserve jusqu'à l'expiration des délais du recours contre les élections ou, le cas échéant, jusqu'à l'intervention d'un jugement définitif sur les contestations.
