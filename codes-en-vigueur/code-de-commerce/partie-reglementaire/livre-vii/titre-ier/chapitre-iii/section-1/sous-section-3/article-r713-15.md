# Article R713-15

Un arrêté du ministre chargé de la tutelle des chambres de commerce et d'industrie fixe le format, le libellé et les modalités d'impression des bulletins et des circulaires, ainsi que les modalités de présentation des candidatures sur les bulletins de vote.
