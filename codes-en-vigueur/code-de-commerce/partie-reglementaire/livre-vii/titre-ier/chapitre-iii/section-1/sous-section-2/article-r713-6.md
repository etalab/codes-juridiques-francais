# Article R713-6

I.-Le 1er septembre au plus tard, un arrêté conjoint du ministre de la justice et du ministre chargé de la tutelle des chambres de commerce et d'industrie fixe la période de dépôt des candidatures, la composition des dossiers de candidature et la date de clôture du scrutin, qui ne peut être postérieure au premier mercredi de novembre, à minuit. Les dates de début de scrutin sont identiques pour le vote par correspondance et pour le vote électronique.

En cas de circonstances particulières, les dates fixées dans l'arrêté mentionné à l'alinéa précédent peuvent être modifiées après le 1er septembre par arrêté conjoint du ministre de la justice, du ministre chargé de la tutelle des chambres de commerce et d'industrie et du ministre de l'intérieur.

II.-Lorsqu'une fusion entre chambres rend nécessaire une élection avant le prochain renouvellement général, le déroulement de l'ensemble des opérations prévues aux articles R. 713-1 à R. 713-6 est fixé par arrêté du ministre chargé de la tutelle des chambres de commerce et d'industrie.
