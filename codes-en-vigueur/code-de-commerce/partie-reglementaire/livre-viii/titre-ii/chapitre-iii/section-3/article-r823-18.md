# Article R823-18

En cas de désaccord entre le ou les commissaires aux comptes et les dirigeants de la personne ou de l'entité contrôlée sur le montant de la rémunération, le président de la compagnie régionale, saisi par écrit par la partie intéressée, s'efforce de concilier les parties.

Lorsque les commissaires aux comptes sont inscrits auprès de compagnies régionales distinctes, la tentative de conciliation est conduite par le président de la compagnie régionale qui a été saisi le premier.

A défaut d'une conciliation intervenue dans le mois de la demande, la partie la plus diligente dispose, à l'expiration de ce délai, d'un délai de quinze jours pour saisir du litige la chambre régionale de discipline par lettre recommandée avec demande d'avis de réception adressée au président de cette chambre.

Le secrétaire de la chambre cite les parties à comparaître devant la chambre régionale quinze jours au moins avant l'audience par lettre recommandée avec demande d'avis de réception. Il avise, le cas échéant, les avocats des parties de la date d'audience par lettre simple.

Dès réception de la citation à comparaître devant la chambre régionale, les parties peuvent prendre connaissance du dossier. Elles peuvent se faire assister ou représenter par un avocat. Les parties et leur avocat peuvent se faire délivrer copie de tout ou partie des pièces du dossier pour l'usage exclusif de la procédure.

Les débats devant la chambre sont publics. Toutefois, la chambre peut décider que les débats ne seront pas publics si les parties en font expressément la demande ou s'il doit résulter de la publicité une atteinte à l'ordre public, à un secret protégé par la loi ou au secret des affaires.

Le secrétaire notifie la décision aux intéressés par lettre recommandée avec demande d'avis de réception et contre émargement ou récépissé au magistrat chargé du ministère public.

Le cas échéant, les avocats des parties reçoivent copie de la décision par lettre simple.
