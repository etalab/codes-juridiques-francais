# Article R823-7

Dans leur rapport à l'assemblée générale ordinaire, les commissaires aux comptes :

1° Déclarent :

a) Soit certifier que les comptes de l'exercice et les comptes consolidés sont réguliers et sincères et qu'ils donnent une image fidèle du résultat des opérations de l'exercice écoulé ainsi que de la situation financière et du patrimoine de la personne ou de l'entité et de l'ensemble des entreprises comprises dans la consolidation à la fin de l'exercice, en formulant, s'il y a lieu, toutes observations utiles ;

b) Soit assortir la certification de réserves ;

c) Soit refuser la certification des comptes.

2° Font état de leurs observations sur la sincérité et la concordance avec les comptes annuels des informations données dans le rapport de gestion de l'exercice et dans les documents adressés aux actionnaires sur la situation financière de la société et de l'ensemble des entreprises comprises dans la consolidation ainsi que sur les comptes annuels et les comptes consolidés.

3° Attestent spécialement l'exactitude et la sincérité des informations mentionnées aux trois premiers alinéas de l'article L. 225-102-1.

Dans les cas mentionnés aux b et c du 1°, les commissaires aux comptes précisent les motifs de leurs réserves ou de leur refus.
