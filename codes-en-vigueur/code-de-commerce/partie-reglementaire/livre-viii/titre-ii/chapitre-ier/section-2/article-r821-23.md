# Article R821-23

Les dossiers et documents établis par le commissaire aux comptes en application de l'article R. 823-10 sont conservés pendant dix ans, même après la cessation des fonctions. Ils sont, pour les besoins des contrôles, inspections et procédures disciplinaires, tenus à la disposition des autorités de contrôle, qui peuvent requérir du commissaire aux comptes les explications et les justifications qu'elles estiment nécessaires concernant ces pièces et les opérations qui doivent y être mentionnées.
