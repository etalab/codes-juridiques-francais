# Chapitre Ier : De l'organisation et du contrôle de la profession

- [Section 1 : Du Haut Conseil du commissariat aux comptes](section-1)
- [Section 2 : Des contrôles et inspections des commissaires aux comptes.](section-2)
- [Section 3 : De l'organisation professionnelle](section-3)
