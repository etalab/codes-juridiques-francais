# Article R821-20

Le Haut Conseil peut, dans les conditions prévues à l'article L. 821-5-1, conclure des conventions de coopération avec des autorités d'Etats non membres de la Communauté européenne exerçant des compétences analogues aux siennes et qui ont été reconnues par la Commission comme répondant aux critères d'adéquation mentionnés au 3 de l'article 47 de la directive 2006/48/CE du 17 mai 2006 du Parlement européen et du Conseil.

Ces conventions ne peuvent porter que sur des échanges d'informations et de documents relatifs au contrôle légal des comptes de personnes ou d'entités émettant des valeurs mobilières sur les marchés de capitaux de l'Etat concerné ou entrant dans le périmètre de consolidation de ces personnes ou entités.

Ces conventions comportent des stipulations assurant le respect, dans les échanges avec les autorités des Etats tiers, des prescriptions fixées par les articles R. 821-17 et R. 821-18. Elles précisent les modalités de la coopération envisagée. Elles garantissent notamment :

a) La communication des informations et documents d'autorité compétente à autorité compétente ;

b) L'exposé par l'autorité requérante des motifs de sa demande de coopération ;

c) Le respect des dispositions relatives à la protection des données personnelles ;

d) L'utilisation des informations et documents communiqués aux seules fins de la supervision publique des personnes en charge de fonctions de contrôle légal des comptes.
