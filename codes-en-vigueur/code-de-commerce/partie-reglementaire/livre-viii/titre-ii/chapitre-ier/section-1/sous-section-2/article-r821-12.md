# Article R821-12

Le Haut Conseil du commissariat aux comptes se prononce sur les inscriptions sur la liste des commissaires aux comptes dans les conditions prévues à la section 1 du chapitre II du présent titre.

Il statue en matière disciplinaire dans les conditions prévues à la sous-section 2 de la section 1 du chapitre II du présent titre.
