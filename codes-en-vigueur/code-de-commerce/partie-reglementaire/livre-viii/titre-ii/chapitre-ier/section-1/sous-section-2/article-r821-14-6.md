# Article R821-14-6

Avant le 31 octobre de chaque année, la Compagnie nationale des commissaires aux comptes informe le secrétaire général du montant prévisionnel des droits et contributions à recouvrer en application de l'article L. 821-5, pour l'année qui suit.

Elle adresse à cette fin au secrétaire général un document de synthèse faisant apparaître, outre le montant prévisionnel mentionné à l'alinéa précédent :

a) Le nombre de personnes inscrites à cette date sur la liste de l'article L. 822-1 ;

b) Le nombre prévisionnel de missions exercées pendant l'année en cours par les personnes inscrites sur la liste de l'article L. 822-1, en indiquant celles qui sont exercées auprès de personnes ou d'entités dont les titres financiers sont admis aux négociations sur un marché réglementé, celles qui sont exercées auprès de personnes ou d'entités dont les titres financiers sont offerts au public sur un système multilatéral de négociation et celles qui sont exercées auprès de personnes ou d'entités n'entrant dans aucune de ces deux catégories ;

c) Le nombre prévisionnel de rapports de certification signés par les mêmes personnes pendant l'année en cours, ventilé selon les trois catégories mentionnées au b.

La Compagnie nationale des commissaires aux comptes communique au secrétaire général, sur sa demande et avant le 30 novembre de chaque année, les éléments justificatifs des informations contenues dans le document de synthèse.
