# Article R821-14-11

Le secrétaire général peut décider, après l'avis conforme de l'agent comptable :

1° En cas de gêne des débiteurs, d'accorder une remise gracieuse des créances du haut conseil, sauf pour le reversement des droits et contributions institué à l'article L. 821-5 et pour le versement de la cotisation instituée à l'article L. 821-6-1 ;

2° Une admission en non-valeur des créances du haut conseil, en cas d'irrécouvrabilité avérée ou d'insolvabilité des débiteurs.

Le haut conseil fixe le montant au-delà duquel la remise mentionnée au 1° est soumise à son approbation.

Lorsque la remise gracieuse, totale ou partielle, concerne une dette de l'agent comptable, l'avis prévu par l'article 9 du décret n° 2008-228 du 5 mars 2008 relatif à la constatation et à l'apurement des débets des comptables publics et assimilés est rendu par le haut conseil.
