# Article R821-65

Le conseil régional tient un registre de ses délibérations. Le procès-verbal de chaque séance est signé par le président et le secrétaire.
