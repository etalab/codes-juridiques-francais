# Article R821-67

Le conseil régional agit dans le cadre des délibérations de l'assemblée de compagnie régionale conformément aux articles R. 821-33 à R. 821-40.
