# Article R821-69

Le conseil régional transmet au Conseil national les informations mentionnées au 2° de l'article R. 821-68.
