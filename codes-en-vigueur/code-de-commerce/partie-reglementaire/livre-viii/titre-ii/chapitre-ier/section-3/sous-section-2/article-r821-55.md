# Article R821-55

Le président élu par le Conseil national représente la Compagnie nationale dans tous les actes de la vie civile et este en justice en son nom. Il porte le titre de président de la Compagnie nationale des commissaires aux comptes.

Il représente la Compagnie nationale auprès des pouvoirs publics.

Il ne peut être membre d'aucune chambre de discipline.

Il cesse d'être délégué du conseil régional qui pourvoit à son remplacement.
