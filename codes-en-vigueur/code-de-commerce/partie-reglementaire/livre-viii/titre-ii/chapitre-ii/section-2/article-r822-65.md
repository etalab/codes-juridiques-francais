# Article R822-65

Tout membre de la compagnie peut demander à cesser d'en faire partie provisoirement.

La demande, adressée au conseil régional par lettre recommandée avec demande d'avis de réception, doit être motivée et indiquer notamment la nouvelle activité que l'intéressé se propose d'exercer ainsi que la date à laquelle il souhaite se retirer provisoirement de la compagnie.

Le conseil régional transmet la demande à la commission d'inscription, qui statue selon la procédure prévue au la section 1 du chapitre II du présent titre.

L'intéressé a la faculté d'entreprendre sa nouvelle activité, même si la décision de la commission d'inscription n'est pas encore intervenue, à la condition d'en informer le conseil régional dans les conditions prévues au deuxième alinéa, au moins huit jours à l'avance, d'être à jour de ses cotisations professionnelles et de cesser préalablement son activité de commissaire aux comptes.
