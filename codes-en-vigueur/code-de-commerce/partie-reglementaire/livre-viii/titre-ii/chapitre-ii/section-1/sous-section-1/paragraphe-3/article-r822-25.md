# Article R822-25

Dans le délai de huit jours à compter de sa réception, le secrétaire du Haut Conseil du commissariat aux comptes notifie à l'intéressé par lettre recommandée avec demande d'avis de réception le recours formé par le procureur général.

La même notification est faite en cas de recours formé par le conseil régional qui dispose alors d'un délai de quinze jours pour prendre connaissance, au greffe de la cour d'appel, du dossier au vu duquel a été prise la décision attaquée et pour présenter des observations complémentaires dont l'intéressé est avisé.
