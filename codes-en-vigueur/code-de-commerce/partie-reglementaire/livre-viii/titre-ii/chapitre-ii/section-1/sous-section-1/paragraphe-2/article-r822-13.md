# Article R822-13

La commission ne peut siéger que si quatre de ses membres au moins sont présents. Elle décide, à la majorité, d'inscrire ou de ne pas inscrire le candidat. Si elle rejette la demande d'inscription, elle motive sa décision. En cas de partage, la voix du président est prépondérante.
