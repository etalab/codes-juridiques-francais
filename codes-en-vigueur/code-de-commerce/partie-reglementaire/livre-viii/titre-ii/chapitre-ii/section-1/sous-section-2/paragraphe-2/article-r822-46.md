# Article R822-46

L'appel contre la décision de la chambre régionale de discipline peut être formé, devant le Haut Conseil du commissariat aux comptes, dans le délai d'un mois à compter de la notification qui leur est faite, par l'une des personnes mentionnées à l'article R. 822-44 et par le président de l'Autorité des marchés financiers lorsqu'il est à l'origine de la poursuite.
