# Article R822-44

La décision de la chambre régionale est prise à la majorité de ses membres. En cas de partage égal des voix, celle du président est prépondérante.

La décision de la chambre régionale est motivée. Elle est prononcée en audience publique ou mise à disposition du public au secrétariat.

Le secrétaire la notifie à l'intéressé, au président de la compagnie nationale et au président de la compagnie régionale, par lettre recommandée avec demande d'avis de réception. Il notifie en outre cette décision au garde des sceaux, ministre de la justice, au procureur général, au magistrat chargé du ministère public et au commissaire du Gouvernement auprès de la chambre nationale de discipline du Conseil supérieur de l'ordre des experts-comptables, lorsque l'intéressé est également inscrit au tableau de l'ordre des experts-comptables, contre émargement ou récépissé.

La lettre de notification fait mention du délai de l'appel prévu à l'article R. 822-46 et des modalités selon lesquelles l'appel peut être exercé.

L'auteur de la plainte ainsi que, le cas échéant, l'avocat du commissaire au compte reçoivent une copie de la décision par lettre simple.

Les diligences incombant au secrétaire de la chambre régionale sont accomplies dans le délai d'un mois à compter du prononcé de la décision.
