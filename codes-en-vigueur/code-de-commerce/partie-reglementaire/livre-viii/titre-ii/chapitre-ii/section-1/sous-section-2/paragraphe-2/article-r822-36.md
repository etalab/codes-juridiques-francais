# Article R822-36

Les plaintes dirigées contre un commissaire aux comptes sont reçues par le procureur général près la cour d'appel ou le conseil régional et transmises au magistrat chargé du ministère public auprès de la chambre régionale de discipline.

A la demande du magistrat chargé du ministère public, le syndic réunit, dans le délai de deux mois, les éléments d'information utiles, et transmet, avec ses observations, le dossier au magistrat chargé du ministère public. Celui-ci peut demander au syndic de lui communiquer le dossier ou de procéder à des mesures d'information complémentaires.

Le procureur général peut également transmettre au magistrat chargé du ministère public auprès de la chambre régionale de discipline tout élément de nature à motiver une action disciplinaire.
