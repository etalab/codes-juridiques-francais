# Article R822-54

Les commissaires aux comptes omis, temporairement interdits ou radiés doivent restituer aux sociétés qu'ils contrôlaient les documents qu'ils détiennent pour le compte de ces sociétés ainsi que les sommes déjà perçues qui ne correspondent pas au remboursement de frais engagés ou à un travail effectivement accompli.
