# Article R822-53

Les décisions des chambres régionales de discipline sont exécutoires après l'expiration des délais d'appel.

Les décisions du Haut Conseil du commissariat aux comptes sont exécutoires à compter de leur notification au commissaire aux comptes.
