# Article R822-50

La décision du haut conseil du commissariat aux comptes est motivée. Elle est prononcée en audience publique ou mise à disposition du public au secrétariat. Elle est notifiée par le secrétaire à l'intéressé, au garde des sceaux, ministre de la justice, au procureur général, au magistrat chargé du ministère public, au président de la Compagnie nationale, au président de la compagnie régionale, au président de l'Autorité des marchés financiers lorsqu'il est à l'origine des poursuites, et au commissaire du Gouvernement auprès de la chambre nationale de discipline de l'ordre des experts-comptables, lorsque l'intéressé est également inscrit au tableau de l'ordre des experts-comptables.

Cette notification est faite dans les conditions prévues à l'article R. 822-44.

L'auteur de la plainte et, le cas échéant, les avocats des parties reçoivent copie de la décision par lettre simple.
