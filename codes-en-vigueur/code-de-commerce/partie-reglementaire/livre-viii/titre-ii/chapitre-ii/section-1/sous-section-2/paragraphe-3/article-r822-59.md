# Article R822-59

L'action disciplinaire se prescrit par dix ans.
