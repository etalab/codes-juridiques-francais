# Paragraphe 1 : De la constitution.

- [Article R822-109](article-r822-109.md)
- [Article R822-110](article-r822-110.md)
- [Article R822-111](article-r822-111.md)
- [Article R822-112](article-r822-112.md)
- [Article R822-113](article-r822-113.md)
- [Article R822-114](article-r822-114.md)
- [Article R822-115](article-r822-115.md)
