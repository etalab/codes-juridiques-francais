# Article R822-120

La modification des statuts et la prorogation de la société sont décidées à la majorité des trois quarts des voix dont dispose l'ensemble des associés présents ou représentés.
