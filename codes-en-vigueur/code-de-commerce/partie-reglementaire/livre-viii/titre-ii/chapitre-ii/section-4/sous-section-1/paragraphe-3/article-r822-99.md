# Article R822-99

Sous réserve des articles R. 822-100 et R. 822-101, les dispositions de la sous-section 2 relative à la discipline des commissaires aux comptes sont applicables à la société et aux actionnaires ou associés.

La société peut faire l'objet de poursuites disciplinaires indépendamment de celles qui seraient intentées contre les actionnaires ou associés.
