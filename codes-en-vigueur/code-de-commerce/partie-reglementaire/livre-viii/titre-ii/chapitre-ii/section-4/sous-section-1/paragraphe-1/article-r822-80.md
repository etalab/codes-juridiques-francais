# Article R822-80

La société qui change de ressort de cour d'appel conserve le bénéfice de la date de son inscription initiale.
