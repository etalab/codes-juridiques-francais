# Article R822-91

Sauf dérogation prévue par le présent titre concernant les élections aux conseils et instances de la compagnie, les sociétés membres de la compagnie bénéficient des mêmes droits et sont soumises aux mêmes obligations que les personnes physiques.
