# Article R822-138

Toute personne physique ou morale peut détenir un quart au plus du capital des sociétés mentionnées au titre Ier de la loi n° 90-1258 du 31 décembre 1990.
