# Article R811-22

L'examen d'aptitude est organisé par le Conseil national des administrateurs judiciaires et des mandataires judiciaires. Ne peuvent être admises à s'y présenter que les personnes titulaires du certificat de fin de stage délivré dans les conditions fixées à l'article R. 811-18.
