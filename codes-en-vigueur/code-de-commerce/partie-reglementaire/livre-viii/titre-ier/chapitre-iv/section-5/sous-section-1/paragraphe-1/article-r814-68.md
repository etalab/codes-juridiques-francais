# Article R814-68

Peuvent faire l'objet d'apports à une société d'administrateurs judiciaires et de mandataires judiciaires, en propriété ou en jouissance :

1° Tous droits incorporels, à l'exclusion de ceux qui, d'une manière directe ou indirecte, auraient pour objet ou effet de conférer une valeur patrimoniale à l'activité de mandataire de justice, tous meubles et immeubles utiles à l'exercice de la profession ;

2° Toutes sommes en numéraire.
