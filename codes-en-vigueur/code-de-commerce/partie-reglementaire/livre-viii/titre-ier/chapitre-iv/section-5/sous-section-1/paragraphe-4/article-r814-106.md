# Article R814-106

Le liquidateur ne peut entrer en fonctions avant d'avoir accompli les formalités de publicité au registre du commerce et des sociétés et informé de la dissolution de la société la commission qui a procédé à l'inscription de celle-ci, en joignant copie de l'acte qui l'a nommé.
