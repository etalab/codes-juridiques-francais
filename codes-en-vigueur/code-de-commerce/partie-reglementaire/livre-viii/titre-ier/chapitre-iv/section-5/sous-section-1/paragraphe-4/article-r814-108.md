# Article R814-108

Le liquidateur informe la Commission nationale des administrateurs judiciaires ou la Commission nationale des mandataires judiciaires de la clôture de la liquidation.
