# Sous-section 4 : Dispositions applicables aux sociétés en participation.

- [Article R814-155](article-r814-155.md)
- [Article R814-156](article-r814-156.md)
- [Article R814-157](article-r814-157.md)
