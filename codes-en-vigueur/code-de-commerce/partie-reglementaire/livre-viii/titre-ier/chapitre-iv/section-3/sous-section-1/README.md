# Sous-section 1 : De la garantie de la représentation des fonds et de la responsabilité civile professionnelle.

- [Article R814-16](article-r814-16.md)
- [Article R814-17](article-r814-17.md)
- [Article R814-18](article-r814-18.md)
- [Article R814-19](article-r814-19.md)
- [Article R814-20](article-r814-20.md)
- [Article R814-21](article-r814-21.md)
- [Article R814-22](article-r814-22.md)
- [Article R814-23](article-r814-23.md)
- [Article R814-24](article-r814-24.md)
- [Article R814-25](article-r814-25.md)
- [Article R814-26](article-r814-26.md)
