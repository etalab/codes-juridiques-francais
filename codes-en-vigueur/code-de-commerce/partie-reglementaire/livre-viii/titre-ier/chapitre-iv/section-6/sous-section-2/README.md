# Sous-section 2 : Du fonctionnement de la société et de son contrôle

- [Article R814-163](article-r814-163.md)
- [Article R814-164](article-r814-164.md)
- [Article R814-165](article-r814-165.md)
- [Article R814-166](article-r814-166.md)
- [Article R814-167](article-r814-167.md)
