# Article R812-13

Les dispositions de l'article R. 811-25 relatives aux demandes de dispense d'une partie du stage sont applicables aux demandes de dispense fondées sur les dispositions du huitième alinéa de l'article L. 812-3.
