# Article R812-7

En application des dispositions de l'article L. 812-3, bénéficient de la dispense de l'examen d'accès au stage :

1° Les administrateurs judiciaires ayant exercé leur profession pendant trois ans au moins ;

2° Les avocats, notaires, commissaires-priseurs judiciaires, huissiers de justice, greffiers des tribunaux de commerce, experts-comptables, commissaires aux comptes ayant exercé leur profession pendant cinq ans au moins.
