# Article R812-18

La commission nationale inscrit les sociétés civiles professionnelles et les sociétés d'exercice libéral de mandataires judiciaires prévues par l'article L. 812-5 sur la liste ainsi que chacun des associés. Le nom de chacun de ceux-ci est suivi de la mention de la raison ou dénomination sociale de la société.

L'appartenance aux autres groupements ou sociétés prévus par l'article L. 812-5 est immédiatement portée à la connaissance de la commission.
