# Article R631-34

Les articles R. 626-1 à R. 626-3, relatifs à la convocation des assemblées, sont applicables à la procédure de redressement judiciaire.

Toutefois, pour l'application de l'article L. 626-3, l'administrateur convoque les assemblées si les dirigeants n'y procèdent pas.
