# Chapitre préliminaire : De l'ouverture et du déroulement de la liquidation judiciaire.

- [Article R640-1](article-r640-1.md)
- [Article R640-1-1](article-r640-1-1.md)
- [Article R640-2](article-r640-2.md)
