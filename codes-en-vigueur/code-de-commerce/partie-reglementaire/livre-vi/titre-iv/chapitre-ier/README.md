# Chapitre Ier : Du jugement de liquidation judiciaire.

- [Section 1 : De la saisine et de la décision du tribunal.](section-1)
- [Section 2 : Des conditions d'application de la liquidation judiciaire simplifiée.](section-2)
- [Section 3 : Des organes de la procédure et des contrôleurs.](section-3)
- [Section 4 : Des mesures conservatoires.](section-4)
- [Section 5 : Du maintien de l'activité.](section-5)
- [Section 6 : Des instances interrompues et des procédures d'ordre en cours.](section-6)
- [Section 7 : De la déclaration des créances.](section-7)
- [Section 8 : De la vérification et de l'admission des créances.](section-8)
- [Section 9 : Des droits du conjoint du débiteur.](section-9)
- [Section 10 : Des droits du vendeur de meubles, des revendications et des restitutions.](section-10)
- [Section 11 : Du règlement des créances résultant du contrat de travail.](section-11)
- [Section 12 : Dispositions diverses.](section-12)
