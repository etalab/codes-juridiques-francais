# Article R645-10

<div align="left">Pour l'application de l'article L. 645-8, le mandataire judiciaire informe par lettre simple les créanciers connus de l'ouverture de la procédure de rétablissement professionnel. Cette lettre reproduit les dispositions des articles L. 645-8, L. 645-11 et R. 645-19 et comprend en annexe copie de l'inventaire des biens du débiteur et de la liste des créances déclarées par ce dernier.<br/>
<br/>
<br/>
</div>
