# Article R645-8

<div align="left">Les actes nécessaires à la conservation des droits du débiteur et à la préservation de ses biens font l'objet d'un compte rendu remis au juge commis dont copie est transmise au ministère public par le mandataire judiciaire. <br/>
<br/>
<br/>
</div>
