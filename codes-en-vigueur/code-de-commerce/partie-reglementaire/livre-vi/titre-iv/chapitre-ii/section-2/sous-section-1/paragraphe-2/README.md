# Paragraphe 2 : Dispositions particulières à la vente par voie d'adjudication judiciaire.

- [Article R642-27](article-r642-27.md)
- [Article R642-28](article-r642-28.md)
- [Article R642-29](article-r642-29.md)
- [Article R642-29-1](article-r642-29-1.md)
- [Article R642-29-2](article-r642-29-2.md)
