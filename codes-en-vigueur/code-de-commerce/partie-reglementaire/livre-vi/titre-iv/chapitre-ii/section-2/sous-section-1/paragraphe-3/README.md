# Paragraphe 3 : Dispositions particulières à la vente par voie d'adjudication amiable.

- [Article R642-30](article-r642-30.md)
- [Article R642-31](article-r642-31.md)
- [Article R642-32](article-r642-32.md)
- [Article R642-33](article-r642-33.md)
- [Article R642-34](article-r642-34.md)
- [Article R642-35](article-r642-35.md)
