# Sous-section 1 : Des ventes des immeubles.

- [Paragraphe 1 : Dispositions communes aux ventes par voie d'adjudication judiciaire et par voie d'adjudication amiable.](paragraphe-1)
- [Paragraphe 2 : Dispositions particulières à la vente par voie d'adjudication judiciaire.](paragraphe-2)
- [Paragraphe 3 : Dispositions particulières à la vente par voie d'adjudication amiable.](paragraphe-3)
- [Paragraphe 4 : Dispositions particulières à la vente de gré à gré.](paragraphe-4)
- [Paragraphe 5 : Dispositions communes à toutes les ventes.](paragraphe-5)
