# Chapitre VIII : De la sauvegarde accélérée

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Dispositions propres à la sauvegarde financière accélérée](section-2)
