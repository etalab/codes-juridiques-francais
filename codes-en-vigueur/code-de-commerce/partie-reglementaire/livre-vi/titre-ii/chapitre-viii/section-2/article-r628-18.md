# Article R628-18

<div align="left">
<p>L'avis prévu par le I de l'article R. 225-73 est publié au Bulletin des annonces légales obligatoires au plus tard vingt et un jours avant la tenue de l'assemblée des actionnaires. <br/>
<br/>La demande d'inscription d'un point ou d'un projet de résolution par les actionnaires à l'ordre du jour de l'assemblée est envoyée au siège social du débiteur quinze jours au moins avant la date de l'assemblée réunie sur première convocation.</p>
<p>
<br/>
</p>
</div>
