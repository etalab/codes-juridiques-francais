# Sous-section 1 : De l'ouverture de la procédure

- [Article D628-3](article-d628-3.md)
- [Article R628-2](article-r628-2.md)
- [Article R628-4](article-r628-4.md)
- [Article R628-5](article-r628-5.md)
- [Article R628-6](article-r628-6.md)
- [Article R628-7](article-r628-7.md)
