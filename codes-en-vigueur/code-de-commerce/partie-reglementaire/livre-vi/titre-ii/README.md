# TITRE II : De la sauvegarde.

- [Chapitre Ier : De l'ouverture de la procédure.](chapitre-ier)
- [Chapitre II : De l'entreprise au cours de la période d'observation.](chapitre-ii)
- [Chapitre III : De l'élaboration du bilan économique, social et environnemental.](chapitre-iii)
- [Chapitre IV : De la détermination du patrimoine du débiteur.](chapitre-iv)
- [Chapitre V : Du règlement des créances résultant du contrat de travail.](chapitre-v)
- [Chapitre VI : Du plan de sauvegarde.](chapitre-vi)
- [Chapitre VII : Dispositions particulières en l'absence d'administrateur judiciaire.](chapitre-vii)
- [Chapitre VIII : De la sauvegarde accélérée](chapitre-viii)
