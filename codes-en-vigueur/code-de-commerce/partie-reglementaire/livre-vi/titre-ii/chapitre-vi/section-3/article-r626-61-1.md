# Article R626-61-1

Huit jours avant la date de réunion de l'assemblée générale, l'administrateur arrête le montant des créances qui ouvrent droit à participer au vote. Il est fait application des dispositions des deux dernières phrases du deuxième alinéa et de celles du troisième alinéa de l'article R. 626-58.
