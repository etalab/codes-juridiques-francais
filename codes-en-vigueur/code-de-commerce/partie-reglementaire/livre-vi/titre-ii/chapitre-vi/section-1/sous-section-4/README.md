# Sous-section 4 : Du règlement des créances publiques.

- [Article D626-9](article-d626-9.md)
- [Article D626-10](article-d626-10.md)
- [Article D626-11](article-d626-11.md)
- [Article D626-12](article-d626-12.md)
- [Article D626-13](article-d626-13.md)
- [Article D626-14](article-d626-14.md)
- [Article D626-15](article-d626-15.md)
