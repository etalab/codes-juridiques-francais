# Article R621-8

Le jugement d'ouverture de la procédure de sauvegarde est mentionné avec l'indication des pouvoirs conférés à l'administrateur, lorsqu'il en a été désigné, au registre du commerce et des sociétés s'il s'agit d'un commerçant ou d'une personne morale immatriculée à ce registre.

A la demande du greffier du tribunal qui a ouvert la procédure, les mêmes mentions sont portées sur le répertoire des métiers ou sur le répertoire des entreprises dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle, s'il s'agit d'une entreprise artisanale.

S'il s'agit d'une personne non immatriculée au registre du commerce et des sociétés ou aux répertoires mentionnés au deuxième alinéa, les mentions sont portées sur un registre ouvert à cet effet au greffe du tribunal de grande instance. Dans ce cas, le greffier indique, selon le cas, le siège ou l'adresse du débiteur, les nom, prénoms et adresse du représentant légal de la personne morale débitrice ou du débiteur personne physique.

Si une déclaration d'affectation a été faite conformément à l'article L. 526-7, mention du jugement d'ouverture est également portée, à la demande du greffier du tribunal qui l'a prononcé, conformément aux 1°, 3° et 4° de cet article, soit sur le registre spécial mentionné à l'article R. 526-15 ou celui mentionné à l'article R. 134-6 du présent code, soit sur le registre prévu par l'article L. 311-2 du code rural et de la pêche maritime.

Un avis du jugement est adressé pour insertion au Bulletin officiel des annonces civiles et commerciales. Cette insertion contient l'indication du nom du débiteur ou, lorsque la procédure est ouverte à raison de l'activité d'un entrepreneur individuel à responsabilité limitée à laquelle un patrimoine est affecté, la dénomination prévue par le dernier alinéa de l'article L. 526-6, selon le cas de son siège ou de son adresse professionnelle, de son numéro unique d'identification ainsi que, s'il y a lieu, du nom de la ville du greffe ou de la chambre de métiers et de l'artisanat de région où il est immatriculé ou, si un patrimoine a été affecté à l'activité en difficulté et selon le cas, de la ville où le greffe tient le registre prévu par l'article L. 526-7 ou, celle où est située la chambre d'agriculture mentionnée par ce texte, de l'activité exercée et de la date du jugement qui a ouvert la procédure. Elle précise également le nom et l'adresse du mandataire judiciaire et de l'administrateur s'il en a été désigné avec, dans ce cas, l'indication des pouvoirs qui lui sont conférés. Elle comporte enfin l'avis aux créanciers d'avoir à déclarer leurs créances entre les mains du mandataire judiciaire et le délai imparti pour cette déclaration.

Le même avis est publié dans un journal d'annonces légales du lieu où le débiteur a son siège ou son adresse professionnelle et, le cas échéant, ses établissements secondaires.

Le greffier procède d'office à ces publicités dans les quinze jours de la date du jugement.
