# Chapitre II : De l'entreprise au cours de la période d'observation.

- [Section 1 : Des mesures conservatoires.](section-1)
- [Section 2 : De la gestion de l'entreprise.](section-2)
- [Section 3 : De la poursuite de l'activité.](section-3)
- [Section 4 : De la déclaration de créances.](section-4)
- [Article R622-1](article-r622-1.md)
