# Chapitre III : De la faillite personnelle et des autres mesures d'interdiction.

- [Article R653-1](article-r653-1.md)
- [Article R653-2](article-r653-2.md)
- [Article R653-3](article-r653-3.md)
- [Article R653-4](article-r653-4.md)
