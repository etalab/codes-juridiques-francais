# Chapitre II : Des dispositions applicables aux personnes morales de droit privé non commerçantes ayant une activité économique.

- [Article D612-5](article-d612-5.md)
- [Article R612-1](article-r612-1.md)
- [Article R612-2](article-r612-2.md)
- [Article R612-3](article-r612-3.md)
- [Article R612-4](article-r612-4.md)
- [Article R612-6](article-r612-6.md)
- [Article R612-7](article-r612-7.md)
