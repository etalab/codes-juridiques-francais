# Chapitre Ier : De la prévention des difficultés des entreprises, du mandat ad hoc et de la procédure de conciliation.

- [Section 1 : Des groupements de prévention agréés.](section-1)
- [Section 2 : De la détection des difficultés des entreprises par le président du tribunal](section-2)
- [Section 3 : Du mandat ad hoc.](section-3)
- [Section 4 : De la procédure de conciliation.](section-4)
- [Section 5 : De la rémunération du mandataire ad hoc, du conciliateur, du mandataire à l'exécution de l'accord et de l'expert.](section-5)
