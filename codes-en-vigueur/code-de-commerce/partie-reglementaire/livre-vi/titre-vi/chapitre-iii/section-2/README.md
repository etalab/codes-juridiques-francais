# Section 2 : De la rémunération de l'administrateur judiciaire, du commissaire à l'exécution du plan, du mandataire judiciaire et du liquidateur

- [Sous-section 1 : De la rémunération de l'administrateur judiciaire.](sous-section-1)
- [Sous-section 2 : De la rémunération du commissaire à l'exécution du plan.](sous-section-2)
- [Sous-section 3 : De la rémunération du mandataire judiciaire et du liquidateur.](sous-section-3)
- [Sous-section 4 : Dispositions communes à la rémunération de l'administrateur judiciaire, du commissaire à l'exécution du plan, du mandataire judiciaire et du liquidateur.](sous-section-4)
- [Sous-section 5 : Du mandataire désigné en application du troisième alinéa de l'article L. 643-9](sous-section-5)
