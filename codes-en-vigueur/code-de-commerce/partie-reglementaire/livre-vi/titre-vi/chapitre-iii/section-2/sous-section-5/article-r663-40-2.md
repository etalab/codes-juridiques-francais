# Article R663-40-2

<div align="left">Le juge-commissaire détermine selon quelles modalités les fonds versés en compte de dépôt à la Caisse des dépôts et consignations seront affectés pour l'exécution de cette mission.<br/>
<br/>
</div>
