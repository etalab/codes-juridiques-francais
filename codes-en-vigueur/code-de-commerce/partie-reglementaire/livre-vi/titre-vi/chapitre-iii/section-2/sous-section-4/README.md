# Sous-section 4 : Dispositions communes à la rémunération de l'administrateur judiciaire, du commissaire à l'exécution du plan, du mandataire judiciaire et du liquidateur.

- [Article R663-32](article-r663-32.md)
- [Article R663-33](article-r663-33.md)
- [Article R663-34](article-r663-34.md)
- [Article R663-35](article-r663-35.md)
- [Article R663-36](article-r663-36.md)
- [Article R663-37](article-r663-37.md)
- [Article R663-38](article-r663-38.md)
- [Article R663-39](article-r663-39.md)
- [Article R663-40](article-r663-40.md)
