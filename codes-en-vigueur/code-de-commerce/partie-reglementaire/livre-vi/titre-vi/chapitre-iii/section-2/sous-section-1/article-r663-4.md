# Article R663-4

Il est alloué à l'administrateur judiciaire, pour les diligences relatives au diagnostic de la procédure de sauvegarde ou de redressement judiciaire au titre de laquelle il a été désigné, une rémunération fixée, en fonction du nombre de salariés employés par le débiteur ou de son chiffre d'affaires, selon le barème suivant :

1° 10 taux de base lorsque le nombre de salariés est compris entre 0 et 5 ou que le chiffre d'affaires est compris entre 0 et 750 000 euros ;

2° 20 taux de base lorsque le nombre de salariés est compris entre 6 et 19 ou que le chiffre d'affaires est compris entre 750 001 euros et 3 000 000 euros ;

3° 40 taux de base lorsque le nombre de salariés est compris entre 20 et 49 ou que le chiffre d'affaires est compris entre 3 000 001 euros et 7 000 000 euros ;

4° 80 taux de base lorsque le nombre de salariés est compris entre 50 et 149 ou que le chiffre d'affaires est compris entre 7 000 001 euros et 20 000 000 euros ;

5° 100 taux de base lorsque le nombre de salariés est supérieur à 150 ou que le chiffre d'affaires est supérieur à 20 000 000 euros.

Lorsque le débiteur relève de deux tranches de rémunération différentes au titre respectivement du nombre de salariés employés et du chiffre d'affaires, il y a lieu de se référer à la tranche la plus élevée.

La rémunération est, quel que soit le nombre de salariés du débiteur et son chiffre d'affaires, égale à 80 taux de base lorsque le total du bilan du débiteur est compris entre 3 650 000 euros et 10 000 000 euros et de 100 taux de base lorsqu'il est supérieur à 10 000 000 euros.

Cette rémunération est versée par le débiteur à l'administrateur judiciaire sans délai dès l'ouverture de la procédure.
