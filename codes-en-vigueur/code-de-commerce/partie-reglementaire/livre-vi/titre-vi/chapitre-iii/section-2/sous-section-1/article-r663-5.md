# Article R663-5

Il est alloué à l'administrateur judiciaire, au titre d'une mission d'assistance du débiteur au cours d'une procédure de sauvegarde ou de redressement judiciaire, un droit proportionnel calculé sur le chiffre d'affaires fixé selon le barème suivant :

1° De 0 à 150 000 euros : 2 % ;

2° De 150 001 euros à 750 000 euros : 1 % ;

3° De 750 001 euros à 3 000 000 euros : 0,60 % ;

4° De 3 000 001 euros à 7 000 000 euros : 0,40 % ;

5° De 7 000 001 euros à 20 000 000 euros : 0,30 %.

Au-delà de 20 000 000 euros, les dispositions de l'article R. 663-13 sont applicables.
