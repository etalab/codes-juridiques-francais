# Article 663-50

<div align="left">En cas d'ouverture d'une procédure de liquidation judiciaire dans les conditions prévues aux articles L. 645-9 et L. 645-12, l'indemnité prévue aux articles L. 663-3 et R. 663-41 est versée au mandataire judiciaire, déduction faite des sommes déjà versées sur le fondement des trois derniers alinéas de l'article R. 663-41.</div>
