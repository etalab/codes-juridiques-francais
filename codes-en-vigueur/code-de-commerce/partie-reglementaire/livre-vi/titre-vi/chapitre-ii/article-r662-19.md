# Article R662-19

<div align="left">L'administrateur coordonnateur établit un rapport sur la situation des sociétés faisant l'objet des procédures avec l'assistance de chacun des administrateurs. Ce rapport peut comporter des propositions dans l'intérêt commun de ces sociétés. Il est communiqué à chacune des juridictions et au ministère public. L'administrateur coordonnateur est destinataire des projets de plan soumis aux tribunaux. Ses observations sont transmises aux juridictions concernées.<br/>
<br/>
<br/>
</div>
