# CHAPITRE 1er : Règles générales

- [Section 1 : Règles personnelles.](section-1)
- [Section 2 : Devoirs envers les clients.](section-2)
- [Section 3 : Devoirs envers les confrères.](section-3)
- [Section 4 : Relations avec l'ordre et les administrations publiques.](section-4)
