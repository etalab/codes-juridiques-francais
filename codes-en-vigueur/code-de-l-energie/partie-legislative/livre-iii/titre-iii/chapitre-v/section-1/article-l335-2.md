# Article L335-2

Chaque fournisseur d'électricité doit disposer de garanties directes ou indirectes de capacités d'effacement de consommation et de production d'électricité pouvant être mises en œuvre pour satisfaire l'équilibre entre la production et la consommation sur le territoire métropolitain continental, notamment lors des périodes où la consommation de l'ensemble des consommateurs est la plus élevée.

Les obligations faites aux fournisseurs sont déterminées de manière à inciter au respect à moyen terme du niveau de sécurité d'approvisionnement en électricité retenu pour l'élaboration du bilan prévisionnel pluriannuel mentionné à l'article L. 141-1.

Le mécanisme d'obligation de capacité prend en compte l'interconnexion du marché français avec les autres marchés européens. Il tient compte de l'intérêt que représente l'effacement de consommation pour la collectivité et pour l'environnement par rapport au développement des capacités de production. A coût égal, il donne la priorité aux capacités d'effacement de consommation sur les capacités de production.

Les garanties de capacité sont requises avec une anticipation suffisante pour laisser aux investisseurs le temps de développer les capacités de production et d'effacement nécessaires pour résorber l'éventuel déséquilibre entre offre et demande prévisionnelles.
