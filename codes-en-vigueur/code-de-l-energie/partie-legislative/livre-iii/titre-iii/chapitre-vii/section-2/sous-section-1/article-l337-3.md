# Article L337-3

Les tarifs de vente d'électricité aux consommateurs domestiques tiennent compte du caractère indispensable de l'électricité pour les consommateurs dont les revenus du foyer sont, au regard de la composition familiale, inférieurs à un plafond, en instaurant pour une tranche de leur consommation une tarification spéciale produit de première nécessité. Cette tarification spéciale est applicable aux services liés à la fourniture.

Pour la mise en œuvre de cette mesure, l'administration fiscale et les organismes de sécurité sociale constituent un fichier regroupant les ayants droit potentiels. Ces fichiers sont transmis aux fournisseurs d'électricité ou, le cas échéant, à un organisme désigné à cet effet par ces fournisseurs, afin de leur permettre de notifier aux intéressés leurs droits à la tarification spéciale. Les fournisseurs d'électricité ou l'organisme qu'ils ont désigné préservent la confidentialité des informations contenues dans le fichier.

La tarification spéciale " produit de première nécessité " bénéficie aux gestionnaires des résidences sociales mentionnées à l'article L. 633-1 du code de la construction et de l'habitation qui font l'objet de la convention prévue à l'article L. 353-1 du même code.

Les sommes correspondantes sont déduites, sous réserve des frais de gestion, du montant des redevances quittancées aux occupants des chambres ou des logements situés dans ces résidences.

Les conditions d'application du présent article sont fixées par voie réglementaire.
