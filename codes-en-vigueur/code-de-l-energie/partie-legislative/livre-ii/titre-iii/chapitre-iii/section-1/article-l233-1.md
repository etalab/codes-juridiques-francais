# Article L233-1

Les personnes morales immatriculées au registre du commerce et des sociétés ainsi que les personnes morales de droit privé mentionnées à l'article L. 612-1 du code de commerce dont le total du bilan, le chiffre d'affaires ou les effectifs excèdent des seuils fixés par décret en Conseil d'Etat sont tenues de réaliser, tous les quatre ans, un audit énergétique satisfaisant à des critères définis par voie réglementaire, établi de manière indépendante par des auditeurs reconnus compétents, des activités exercées par elles en France.

Le premier audit est établi au plus tard le 5 décembre 2015. La personne morale assujettie transmet à l'autorité administrative les informations relatives à la mise en œuvre de cette obligation.
