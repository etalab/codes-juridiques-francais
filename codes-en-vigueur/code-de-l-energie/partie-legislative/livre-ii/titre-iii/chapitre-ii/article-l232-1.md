# Article L232-1

Le service public de la performance énergétique de l'habitat assure l'accompagnement des consommateurs souhaitant diminuer leur consommation énergétique. Il assiste les propriétaires et les locataires dans la réalisation des travaux d'amélioration de la performance énergétique de leur logement et leur fournit des informations et des conseils personnalisés.
