# Article L271-1

Un décret en Conseil d'Etat, pris sur proposition de la Commission de régulation de l'énergie, fixe la méthodologie utilisée pour établir les règles permettant la valorisation des effacements de consommation d'électricité sur les marchés de l'énergie et sur le mécanisme d'ajustement mentionné à l'article L. 321-10.

Ces règles prévoient la possibilité, pour un opérateur d'effacement, de procéder à des effacements de consommation, indépendamment de l'accord du fournisseur d'électricité des sites concernés, et de les valoriser sur les marchés de l'énergie ou sur le mécanisme d'ajustement mentionné au même article L. 321-10, ainsi qu'un régime de versement de l'opérateur d'effacement vers les fournisseurs d'électricité des sites effacés. Ce régime de versement est établi en tenant compte des quantités d'électricité injectées par ou pour le compte des fournisseurs des sites effacés et valorisées par l'opérateur d'effacement sur les marchés de l'énergie ou sur le mécanisme d'ajustement.

Une prime est versée aux opérateurs d'effacement, prenant en compte les avantages de l'effacement pour la collectivité, dans les conditions précisées au chapitre III du titre II du livre Ier.
