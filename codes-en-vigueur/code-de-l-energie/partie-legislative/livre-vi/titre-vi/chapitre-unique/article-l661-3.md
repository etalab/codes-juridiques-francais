# Article L661-3

Les critères de durabilité à respecter sont définis aux articles L. 661-4 à L. 661-6 et aux dispositions prises pour leur application. Ils s'appliquent à toutes les étapes de la chaîne de production et de distribution des biocarburants et bioliquides, depuis l'extraction ou la culture des matières premières jusqu'à la transformation de la biomasse en un produit de qualité requise pour être utilisée comme carburant ou combustible, le transport, la mise à la consommation et la distribution de ce produit.
