# Article L661-6

Les biocarburants et bioliquides ne doivent pas être produits à partir de matières premières qui, lorsqu'elles sont cultivées sur le territoire de l'Union européenne, ne respectent pas les exigences et les règles ou les bonnes conditions agricoles et environnementales applicables dans le cadre de la politique agricole communautaire.
