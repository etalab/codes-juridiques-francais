# Article L132-2

Le collège est composé de six membres nommés en raison de leurs qualifications juridiques, économiques et techniques.

Le président du collège est nommé par décret dans les conditions fixées par la loi organique n° 2010-837 du 23 juillet 2010 relative à l'application du cinquième alinéa de l'article 13 de la Constitution. Le collège est renouvelé par tiers tous les deux ans.

Le collège comprend également :

1° Un membre nommé par le président de l'Assemblée nationale, en raison de ses qualifications juridiques, économiques et techniques dans le domaine de la protection des données personnelles ;

2° Un membre nommé par le président du Sénat, en raison de ses qualifications juridiques, économiques et techniques dans le domaine des services publics locaux de l'énergie ;

3° Un membre nommé par décret, en raison de ses qualifications juridiques, économiques et techniques dans les domaines de la protection des consommateurs d'énergie et de la lutte contre la précarité énergétique ;

4° Un membre nommé par décret, en raison de ses qualifications juridiques, économiques et techniques dans les domaines de la maîtrise de la demande d'énergie et des énergies renouvelables ;

5° Un membre nommé par décret, sur proposition du ministre chargé de l'outre-mer, en raison de sa connaissance et de son expérience des zones non interconnectées.

La composition du collège respecte la parité entre les femmes et les hommes. Les membres du collège sont nommés pour six ans. Leur mandat n'est pas renouvelable.

En cas de vacance d'un siège de membre du collège, il est procédé à son remplacement pour la durée du mandat restant à courir. Un mandat exercé pendant moins de deux ans n'est pas pris en compte pour l'application de la règle de non-renouvellement.

Les fonctions de président et des autres membres du collège sont incompatibles avec tout mandat électif communal, départemental, régional, national ou européen et avec la détention, directe ou indirecte, d'intérêts dans une entreprise du secteur de l'énergie. Chaque membre du collège fait une déclaration d'intérêts au moment de sa désignation. Cette déclaration est rendue publique.

Le président et les autres membres du collège ne peuvent, sous peine de l'application des sanctions prévues à l'article 432-13 du code pénal, prendre ou recevoir une participation par travail, conseil ou capitaux dans l'une de ces entreprises avant l'expiration d'un délai de trois ans suivant la cessation de leurs fonctions.
