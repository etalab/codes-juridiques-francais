# Chapitre Ier : Les secteurs de l'électricité et du gaz

- [Section 1 : Distinction des activités](section-1)
- [Section 2 : Organisation des entreprises de transport](section-2)
- [Section 3 : Organisation des entreprises gestionnaires des réseaux publics de distribution d'électricité et de gaz](section-3)
- [Section 4 : Dispositions particulières aux entreprises Electricité de France et GDF-Suez](section-4)
- [Section 5 : Confidentialité des informations sensibles](section-5)
- [Section 6 : Dissociation et transparence de la comptabilité](section-6)
- [Section 7 : Droit d'accès aux réseaux et aux installations](section-7)
