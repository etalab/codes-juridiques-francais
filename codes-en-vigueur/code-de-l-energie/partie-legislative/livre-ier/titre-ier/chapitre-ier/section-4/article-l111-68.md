# Article L111-68

L'entreprise dénommée "GDF-Suez" est une société anonyme, dont le capital est détenu à plus du tiers par l'Etat.
