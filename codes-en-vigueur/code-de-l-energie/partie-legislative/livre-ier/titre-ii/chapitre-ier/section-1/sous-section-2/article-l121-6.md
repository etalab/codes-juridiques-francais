# Article L121-6

Les charges imputables aux missions de service public assignées aux opérateurs électriques définies aux articles L. 121-7 et L. 121-8 sont intégralement compensées.

La prime mentionnée à l'article L. 123-1 est couverte par la contribution prévue à l'article L. 121-10.
