# 2 : Dispositions générales

- [Article 398](article-398.md)
- [Article 399](article-399.md)
- [Article 400](article-400.md)
- [Article 401](article-401.md)
- [Article 402](article-402.md)
- [Article 403](article-403.md)
- [Article 404](article-404.md)
