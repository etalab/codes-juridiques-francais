# Section II : Juridiction gracieuse

- [C : Propositions des comptables secondaires de la direction générale des finances publiques chargés du recouvrement des créances de nature fiscale](c)
- [D : Dispositions particulières aux impôts directs](d)
