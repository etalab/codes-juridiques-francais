# I : Taxe foncière sur les propriétés bâties.

- [Article 328 E](article-328-e.md)
- [Article 328 F](article-328-f.md)
- [Article 328 G](article-328-g.md)
