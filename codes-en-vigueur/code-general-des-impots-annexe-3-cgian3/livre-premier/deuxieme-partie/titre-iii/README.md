# Titre III :  Dispositions communes aux titres Ier à II

- [Chapitre II : Abattement sur les bases d'impositions directes locales](chapitre-ii)
- [Chapitre III : Participation au financement du plafonnement en fonction de la valeur ajoutée](chapitre-iii)
- [Chapitre premier : Cotisation sur la valeur ajoutée des entreprises](chapitre-premier)
