# Article 344 vicies

Pour l'application du VII de l'article 1647-0 B septies du code général des impôts, les produits communaux et intercommunaux de cotisation foncière des entreprises s'entendent des produits tels qu'issus des rôles généraux établis au titre de l'année précédant celle mentionnée à l'article 344 sexdecies.

Lorsque l'établissement public de coopération intercommunale mentionné au VII précité est issu d'une fusion prenant fiscalement effet au 1er janvier de l'année au titre de laquelle la participation est calculée, le produit intercommunal de cotisation foncière des entreprises de l'année précédente s'entend des produits intercommunaux des établissements préexistants et afférents au territoire de la commune.
