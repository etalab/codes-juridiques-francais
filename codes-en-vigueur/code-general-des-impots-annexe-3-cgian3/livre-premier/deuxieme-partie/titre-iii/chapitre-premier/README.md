# Chapitre premier : Cotisation sur la valeur ajoutée des entreprises

- [Article 344 duodecies](article-344-duodecies.md)
- [Article 344 terdecies](article-344-terdecies.md)
- [Article 344 quaterdecies](article-344-quaterdecies.md)
