# Section IV : Taxe sur la cession à titre onéreux de terrains nus rendus constructibles

- [Article 331 K bis](article-331-k-bis.md)
- [Article 331 K ter](article-331-k-ter.md)
