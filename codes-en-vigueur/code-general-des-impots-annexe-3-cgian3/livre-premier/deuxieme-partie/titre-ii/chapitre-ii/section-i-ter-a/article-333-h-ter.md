# Article 333 H ter

La taxe due par les meuniers au titre des mois de juillet et août est liquidée respectivement sur la base de 30% et 70% des quantités de farines, semoules et gruaux de blé tendre livrées ou mises en oeuvre en vue de la consommation humaine. Les compléments de taxe sont liquidés en même temps que la taxe due au titre du mois de novembre.
