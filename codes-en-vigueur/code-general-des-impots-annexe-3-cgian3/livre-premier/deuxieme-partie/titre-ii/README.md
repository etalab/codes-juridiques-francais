# Titre II : Impositions perçues au profit de certains établissements publics et d'organismes divers

- [Chapitre II : Contributions indirectes](chapitre-ii)
- [Chapitre III : Enregistrement](chapitre-iii)
- [Chapitre premier : Impôts directs et taxes assimilées](chapitre-premier)
