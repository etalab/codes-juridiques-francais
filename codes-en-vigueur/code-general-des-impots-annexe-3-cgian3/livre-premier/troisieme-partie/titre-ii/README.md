# Titre II : Dispositions diverses

- [Chapitre II : Régime de certains organismes et sociétés](chapitre-ii)
- [Chapitre III : Les compétences de la direction générale des douanes et droits indirects en matière de contributions indirectes et de réglementations assimilées](chapitre-iii)
- [Chapitre IV : Compétences des fonctionnaires de la direction générale des finances publiques](chapitre-iv)
- [Chapitre premier : Commissions administratives des impôts](chapitre-premier)
