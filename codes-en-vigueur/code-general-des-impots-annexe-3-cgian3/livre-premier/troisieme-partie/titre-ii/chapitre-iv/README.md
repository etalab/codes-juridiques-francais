# Chapitre IV : Compétences des fonctionnaires de la direction générale des finances publiques

- [Article 350 terdecies](article-350-terdecies.md)
- [Article 350 quaterdecies](article-350-quaterdecies.md)
- [Article 350 quindecies](article-350-quindecies.md)
