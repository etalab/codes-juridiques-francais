# Section I : Déclarations des commissions, courtages, ristournes, honoraires et droits d'auteur

- [Article 47](article-47.md)
- [Article 47 bis](article-47-bis.md)
- [Article 47 A](article-47-a.md)
