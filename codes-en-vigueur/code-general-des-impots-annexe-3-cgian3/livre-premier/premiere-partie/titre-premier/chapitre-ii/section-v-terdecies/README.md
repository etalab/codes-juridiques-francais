# Section V terdecies : Crédit d'impôt pour formation des dirigeants

- [Article 49 septies ZC](article-49-septies-zc.md)
- [Article 49 septies ZD](article-49-septies-zd.md)
- [Article 49 septies ZE](article-49-septies-ze.md)
- [Article 49 septies ZF](article-49-septies-zf.md)
- [Article 49 septies ZG](article-49-septies-zg.md)
- [Article 49 septies ZH](article-49-septies-zh.md)
