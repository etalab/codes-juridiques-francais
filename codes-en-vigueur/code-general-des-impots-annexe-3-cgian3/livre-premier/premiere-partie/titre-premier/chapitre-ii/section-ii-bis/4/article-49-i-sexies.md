# Article 49 I sexies

1. Pour l'application des sixième et septième alinéas du 1 de l'article 242 ter du code général des impôts, les personnes mentionnées au I de l'article 49 I ter qualifient les revenus de la nature de ceux mentionnés au 4° du 1 du III du même article au vu des éléments communiqués dans les conditions prévues à l'article 49 I quinquies ou au 2 ci-après.

Les revenus sont qualifiés d'intérêts lorsque le quota d'investissement en titres de créances ou produits assimilés de l'organisme ou entité concerné a été déclaré supérieur au moins une fois à 40 % au cours de la période de détention des parts ou actions de cet organisme ou entité par le bénéficiaire effectif du revenu.

A défaut de pouvoir déterminer la période de détention des parts cédées par le bénéficiaire effectif, les revenus sont qualifiés d'intérêts lorsque le quota d'investissement en titres de créances ou produits assimilés a été déclaré supérieur à 40 % au moins une fois depuis la création de l'organisme ou entité ou depuis la situation communiquée à la date d'entrée en vigueur des sixième à huitième alinéas du 1 de l'article 242 ter.

A défaut d'information sur la situation de l'organisme ou entité, ces revenus sont qualifiés d'intérêts.

2. Lorsqu'un établissement payeur, au sens de l'article 75 de l'annexe II au code général des impôts, paie des revenus mentionnés au 4° du 1 du III de l'article 49 I ter à un autre établissement payeur, il doit accompagner ce paiement des informations relatives à la situation des organismes ou entités mentionnés à la première phrase du 3° du 1 du III du même article 49 I ter dont les parts sont cédées ou rachetées au regard de leur quota d'investissement, informations qui lui ont été transmises dans les conditions prévues à l'article 49 I quinquies.
