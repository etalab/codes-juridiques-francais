# Section V duodecies : Crédit d'impôt en faveur de l'agriculture biologique

- [Article 49 septies Z](article-49-septies-z.md)
- [Article 49 septies ZB](article-49-septies-zb.md)
- [Article 49 septies ZB bis](article-49-septies-zb-bis.md)
