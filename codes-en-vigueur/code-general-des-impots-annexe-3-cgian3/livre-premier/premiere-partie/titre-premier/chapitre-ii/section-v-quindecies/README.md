# Section V quindecies : Crédit d'impôt en faveur des métiers d'art

- [Article 49 septies ZM](article-49-septies-zm.md)
- [Article 49 septies ZN](article-49-septies-zn.md)
- [Article 49 septies ZO](article-49-septies-zo.md)
