# Section I : Impôt sur le revenu et impôt sur les sociétés

- [Article 64 bis](article-64-bis.md)
- [Article 64 ter](article-64-ter.md)
- [Article 64 quater](article-64-quater.md)
