# Chapitre III : Taxes diverses

- [Section I : Taxe sur les salaires](section-i)
- [Section VI : Taxe sur les loyers élevés des logements de petite surface](section-vi)
- [Section VII : Taxes sur les transactions financières](section-vii)
