# Article 10 E

Si elle est utilisée dans le délai et les conditions prévus à l'article 10 C, la provision est définitivement exonérée de l'impôt sur le revenu ou de l'impôt sur les sociétés et peut être virée à un compte de réserve quelconque au passif du bilan. Les sommes ainsi utilisées en travaux de recherches ou de récupération ou en participations peuvent, dans les conditions fixées par le code général des impôts, être comptabilisées en dépenses d'exploitation, faire l'objet d'amortissements annuels ou donner lieu à la constitution de provisions pour dépréciation.

A défaut de remploi dans le délai ci-dessus, la provision est rapportée au bénéfice imposable de l'exercice en cours à la date d'expiration de ce délai.
