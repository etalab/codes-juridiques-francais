# Article 10 D

La provision pour reconstitution des gisements est inscrite au passif du bilan de l'entreprise sous une rubrique spéciale faisant ressortir le montant des dotations de chaque exercice.
