# E : Provisions pour investissement à l'étranger

- [Article 10 GA](article-10-ga.md)
- [Article 10 GA bis](article-10-ga-bis.md)
- [Article 10 GA ter](article-10-ga-ter.md)
- [Article 10 GA quater](article-10-ga-quater.md)
- [Article 10 GA quinquies](article-10-ga-quinquies.md)
- [Article 10 GA sexies](article-10-ga-sexies.md)
