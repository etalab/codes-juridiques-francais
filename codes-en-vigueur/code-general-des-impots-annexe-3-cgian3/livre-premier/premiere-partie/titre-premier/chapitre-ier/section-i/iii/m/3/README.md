# 3 : Règles d'évaluation

- [Article 38 quinquies](article-38-quinquies.md)
- [Article 38 sexies](article-38-sexies.md)
- [Article 38 septies](article-38-septies.md)
- [Article 38 octies](article-38-octies.md)
- [Article 38 nonies](article-38-nonies.md)
- [Article 38 decies](article-38-decies.md)
- [Article 38 undecies](article-38-undecies.md)
