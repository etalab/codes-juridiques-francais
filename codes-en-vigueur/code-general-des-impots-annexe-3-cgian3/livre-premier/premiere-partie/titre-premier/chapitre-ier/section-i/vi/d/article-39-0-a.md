# Article 39-0 A

Les employeurs tenus au dépôt de la déclaration mentionnée à l'article 87 du code général des impôts communiquent dans le même délai aux personnes concernées les montants des cotisations ou primes et des sommes mentionnés respectivement aux neuvième et dixième alinéas du d du 2° de l'article 39.
