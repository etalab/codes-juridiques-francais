# VIII : Revenus des capitaux mobiliers

- [C : Prélèvement sur les produits de placements à revenu fixe](c)
- [A : Amortissement du capital social des sociétés concessionnaires](a)
- [B : Primes de remboursement des obligations et autres emprunts négociables](b)
- [E : Titres de créances négociables](e)
- [F : Émission par les sociétés françaises d'obligations à l'étranger. Régime spécial](f)
- [G : Fonds communs de placement, fonds professionnels de capital investissement et fonds professionnels spécialisés](g)
- [G bis : Modalités d'imposition et conditions de ventilation des revenus distribués ou répartis par des sociétés, organismes ou placements collectifs](g-bis)
- [G ter : Prélèvement sur les produits de placements à revenu fixe et les produits de bons ou contrats de capitalisation de source européenne](g-ter)
- [G quater : Sociétés unipersonnelles d'investissement à risque. Obligations de l'associé](g-quater)
