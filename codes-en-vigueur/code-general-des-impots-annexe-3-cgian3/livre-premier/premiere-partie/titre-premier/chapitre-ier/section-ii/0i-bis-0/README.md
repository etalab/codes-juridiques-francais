# 0I bis-0 : Contrats d'assurance groupe des professions indépendantes au titre de la retraite, de la prévoyance et de la perte d'emploi subie

- [Article 41 DN bis](article-41-dn-bis.md)
- [Article 41 DN ter](article-41-dn-ter.md)
