# III : Présentation et contenu des déclarations

- [Article 42](article-42.md)
- [Article 43](article-43.md)
- [Article 43 bis](article-43-bis.md)
- [Article 45](article-45.md)
- [Article 46](article-46.md)
