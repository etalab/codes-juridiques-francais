# 16° : Crédit d'impôt au titre des intérêts d'emprunt supportés pour l'acquisition ou la construction de l'habitation principale

- [Article 46 AZA sexies](article-46-aza-sexies.md)
- [Article 46 AZA septies](article-46-aza-septies.md)
