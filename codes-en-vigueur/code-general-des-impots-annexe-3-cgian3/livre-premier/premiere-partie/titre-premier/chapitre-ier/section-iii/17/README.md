# 17° : Réduction d'impôt accordée au titre des investissements locatifs

- [Article 46 AZA octies](article-46-aza-octies.md)
- [Article 46 AZA octies-0 A](article-46-aza-octies-0-a.md)
- [Article 46 AZA octies-0 AA](article-46-aza-octies-0-aa.md)
- [Article 46 AZA octies A](article-46-aza-octies-a.md)
- [Article 46 AZA octies B](article-46-aza-octies-b.md)
- [Article 46 AZA nonies](article-46-aza-nonies.md)
- [Article 46 AZA decies](article-46-aza-decies.md)
- [Article 46 AZA undecies](article-46-aza-undecies.md)
- [Article 46 AZA duodecies](article-46-aza-duodecies.md)
- [Article 46 AZA terdecies](article-46-aza-terdecies.md)
- [Article 46 AZA quaterdecies](article-46-aza-quaterdecies.md)
