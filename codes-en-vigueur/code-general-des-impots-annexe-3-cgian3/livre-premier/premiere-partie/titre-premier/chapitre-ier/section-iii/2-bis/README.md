# 2° bis : Réduction et crédit d'impôt accordés au titre de certains investissements forestiers

- [Article 46 AGH](article-46-agh.md)
- [Article 46 AGI](article-46-agi.md)
- [Article 46 AGJ](article-46-agj.md)
- [Article 46 AGK](article-46-agk.md)
