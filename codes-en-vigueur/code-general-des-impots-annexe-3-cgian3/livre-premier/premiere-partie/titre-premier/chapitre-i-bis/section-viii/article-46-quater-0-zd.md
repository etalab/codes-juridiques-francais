# Article 46 quater-0 ZD

Les options mentionnées aux premier, deuxième et troisième alinéas de l'article 223 A du code général des impôts sont notifiées au service des impôts auprès duquel est souscrite la déclaration du résultat d'ensemble.

La société mère adresse à ce même service :

1. Lors de la notification de l'option :

a) la liste des personnes morales et des établissements stables qui seront membres du groupe. Cette liste indique, pour chaque société mentionnée au b, sa désignation, l'adresse de son siège social et la répartition de son capital et, pour chaque autre personne morale, sa désignation, l'adresse de son siège social et, le cas échéant, celle de son établissement stable, et la nature du lien qui l'unit à l'entité tête du groupe formé en application du deuxième ou troisième alinéa de l'article 223 A du code général des impôts ;

a bis) La liste des personnes morales et des établissements stables qui seront qualifiés de sociétés intermédiaires. Cette liste indique, pour chaque société mentionnée au b bis, sa désignation, l'adresse de son siège social, l'impôt étranger équivalent à l'impôt sur les sociétés auquel elle est soumise et la répartition de son capital et, pour chaque autre personne morale, sa désignation, l'adresse de son siège social et, le cas échéant, celle de son établissement stable, l'impôt étranger équivalent à l'impôt sur les sociétés auquel elle ou son établissement stable est soumis et la nature du lien qui l'unit à l'entité tête du groupe formé en application du deuxième ou troisième alinéa de l'article 223 A du code général des impôts ;

b) Des attestations par lesquelles les sociétés filiales font connaître leur accord pour que la société mère retienne leurs propres résultats pour la détermination du résultat d'ensemble ;

b bis) Des attestations par lesquelles les sociétés filiales font connaître leur accord pour qu'elles soient qualifiées de sociétés intermédiaires ;

c) Le cas échéant, le document visé au premier alinéa du c du 6 de l'article 223 L du code général des impôts ou au deuxième alinéa du g du 6 du même article, qui comporte la liste et les attestations précédemment mentionnées.

2. Au plus tard à l'expiration du délai de dépôt de la déclaration de résultat de chacun des exercices arrêtés au cours de la période de validité de l'option :

a) La liste des sociétés qui seront membres du groupe défini à l'article 223 A du code général des impôts, au titre de l'exercice suivant ;

a bis) La liste des sociétés intermédiaires définies à l'article 223 A du code général des impôts ainsi que des sociétés qui cessent d'être qualifiées de sociétés intermédiaires au titre de l'exercice suivant ;

b) Les attestations mentionnées au b du 1 produites par les sociétés qui seront membres du groupe à compter de cet exercice suivant ;

b bis) Les attestations mentionnées au b bis du 1 produites par les sociétés qui seront qualifiées de sociétés intermédiaires à compter de cet exercice suivant ;

c) le cas échéant, l'extrait de l'annexe comptable comportant les informations suivantes sur les comptes combinés : nom de l'entreprise combinante, liste des entreprises du périmètre de combinaison et description de la nature des liens qui permettent de fonder les critères de sélection des entreprises dont les comptes sont combinés, ainsi que l'indication des motifs qui justifient la non-combinaison de certaines entreprises ;

d) Le cas échéant, la référence de l'agrément collectif délivré par l'Autorité de contrôle prudentiel et de résolution pour elle-même et pour les caisses locales qui la détiennent.

2 bis. Dans les trois mois de l'acquisition des titres d'une société du groupe ou d'une autre société intermédiaire, l'attestation mentionnée au b bis du 1 produite par la société qui sera qualifiée de société intermédiaire à compter de l'exercice au cours duquel est intervenue cette acquisition.

3. En même temps que la déclaration du résultat d'ensemble de l'exercice au cours duquel le capital de la société mère vient à être détenu à hauteur de 95 % au moins, directement ou indirectement, par une personne morale passible de l'impôt sur les sociétés dans les conditions prévues au premier alinéa du d du 6 de l'article 223 L du code général des impôts, une lettre signée des représentants dûment mandatés de la société mère et de la société détentrice des titres qui indique, de manière précise, la nature, les circonstances et les justifications juridiques, économiques ou sociales de l'opération à l'origine de la détention en cause.
