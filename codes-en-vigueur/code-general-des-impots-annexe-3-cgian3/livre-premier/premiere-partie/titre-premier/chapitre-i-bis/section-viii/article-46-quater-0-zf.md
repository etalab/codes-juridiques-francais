# Article 46 quater-0 ZF

Pour l'application des dispositions de l'article 223 A du code général des impôts, la détention de 95% au moins du capital d'une société s'entend de la détention en pleine propriété de 95% au moins des droits à dividendes et de 95% au moins des droits de vote attachés aux titres émis par cette société.

Les droits détenus indirectement s'entendent des droits détenus par l'intermédiaire d'une chaîne de participation. Le pourcentage de ces droits est apprécié en multipliant entre eux les taux de détention successifs. Toutefois, pour cette appréciation, la société qui détient 95% au moins du capital d'une autre société est considérée comme détenant ce capital en totalité.

Peuvent être membres du groupe les sociétés détenues, dans les conditions mentionnées aux deux alinéas précédents, par une société étrangère dont l'établissement stable est membre du groupe, à la condition que leurs titres soient inscrits à l'actif du bilan fiscal de cet établissement stable.
