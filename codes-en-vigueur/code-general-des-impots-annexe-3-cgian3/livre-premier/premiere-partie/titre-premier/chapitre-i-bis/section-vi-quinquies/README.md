# Section VI quinquies : Crédit d'impôt pour dépenses de production d'oeuvres phonographiques

- [Article 46 quater-0 YS](article-46-quater-0-ys.md)
- [Article 46 quater-0 YT](article-46-quater-0-yt.md)
- [Article 46 quater-0 YU](article-46-quater-0-yu.md)
