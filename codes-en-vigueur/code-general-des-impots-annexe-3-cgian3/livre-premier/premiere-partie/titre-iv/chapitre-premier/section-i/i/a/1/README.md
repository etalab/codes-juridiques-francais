# 1 : Actes et mutations soumis à la formalité de l'enregistrement. Actes publics et sous seings privés

- [Article 245](article-245.md)
- [Article 246](article-246.md)
- [Article 248](article-248.md)
