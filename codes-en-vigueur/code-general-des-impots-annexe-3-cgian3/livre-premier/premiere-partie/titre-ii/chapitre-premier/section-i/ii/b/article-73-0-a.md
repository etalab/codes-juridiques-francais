# Article 73-0 A

Pour l'application du deuxième alinéa de l'article 298 septies du code général des impôts, les taux réduits de la taxe sur la valeur ajoutée s'appliquent à hauteur de la part du prix hors taxe de l'offre composite représentative de la livraison de la publication imprimée. La détermination de cette part par l'éditeur peut être réalisée selon toute méthode traduisant la réalité économique des opérations.
