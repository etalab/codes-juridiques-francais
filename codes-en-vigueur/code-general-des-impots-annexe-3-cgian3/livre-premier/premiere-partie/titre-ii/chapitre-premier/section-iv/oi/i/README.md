# I :  Régime suspensif

- [C : Fermeture d'un régime ou d'un entrepôt fiscal suspensif](c)
- [D : Entrée et sortie des biens d'un régime ou d'un entrepôt fiscal suspensif](d)
- [A : Ouverture d'un régime ou d'un entrepôt fiscal suspensif](a)
- [B : Modification du fonctionnement d'un régime ou d'un entrepôt fiscal suspensif](b)
- [E : Fonctions du régime fiscal suspensif](e)
- [F : Biens admissibles sous un régime ou un entrepôt fiscal suspensif](f)
- [G : Définition des opérations effectuées en suspension du paiement de la taxe sur la valeur ajoutée](g)
- [H : Factures](h)
