# Attestation du contrôleur technique établissant qu'il a fait connaître au maître d'ouvrage de la construction son avis sur la prise en compte au stade de la conception des règles parasismiques.

- [Article A431-10 Annexe](article-a431-10-annexe.md)
