# Liste des servitudes d'utilité publique affectant l'utilisation du sol.

- [Article R*126-1, Annexe](article-r-126-1-annexe.md)
