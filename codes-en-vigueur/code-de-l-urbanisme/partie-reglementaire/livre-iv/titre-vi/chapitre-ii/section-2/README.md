# Section 2 : Dispositions applicables aux constructions soumises à des règles parasismiques.

- [Article A462-2](article-a462-2.md)
- [Article A462-3](article-a462-3.md)
- [Article A462-4](article-a462-4.md)
