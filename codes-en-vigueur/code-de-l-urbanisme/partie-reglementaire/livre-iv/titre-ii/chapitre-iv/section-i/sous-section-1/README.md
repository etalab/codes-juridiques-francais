# Sous-section 1 : Dispositions communes.

- [Article A424-1](article-a424-1.md)
- [Article A424-2](article-a424-2.md)
- [Article A424-3](article-a424-3.md)
- [Article A424-4](article-a424-4.md)
- [Article A424-5](article-a424-5.md)
- [Article A424-6](article-a424-6.md)
- [Article A424-7](article-a424-7.md)
- [Article A424-8](article-a424-8.md)
