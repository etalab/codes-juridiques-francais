# Section IV : Dispositions applicables aux constructions soumises à des règles parasismiques.

- [Article A431-10](article-a431-10.md)
- [Article A431-11](article-a431-11.md)
