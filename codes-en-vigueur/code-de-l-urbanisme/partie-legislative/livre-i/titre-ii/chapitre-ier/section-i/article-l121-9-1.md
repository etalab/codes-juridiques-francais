# Article L121-9-1

Des décrets en Conseil d'Etat déterminent, en tant que de besoin, les conditions d'application de la présente section. Ces décrets arrêtent notamment la liste des opérations d'intérêt national mentionnées à l'article L. 121-2.
