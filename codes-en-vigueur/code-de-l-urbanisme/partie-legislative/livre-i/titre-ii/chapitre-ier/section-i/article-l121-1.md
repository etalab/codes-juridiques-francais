# Article L121-1

Les schémas de cohérence territoriale, les plans locaux d'urbanisme et les cartes communales déterminent les conditions permettant d'assurer, dans le respect des objectifs du développement durable :

1° L'équilibre entre :

a) Le renouvellement urbain, le développement urbain maîtrisé, la restructuration des espaces urbanisés, la revitalisation des centres urbains et ruraux ;

b) L'utilisation économe des espaces naturels, la préservation des espaces affectés aux activités agricoles et forestières, et la protection des sites, des milieux et paysages naturels ;

c) La sauvegarde des ensembles urbains et du patrimoine bâti remarquables ;

d) Les besoins en matière de mobilité.

1° bis La qualité urbaine, architecturale et paysagère, notamment des entrées de ville ;

2° La diversité des fonctions urbaines et rurales et la mixité sociale dans l'habitat, en prévoyant des capacités de construction et de réhabilitation suffisantes pour la satisfaction, sans discrimination, des besoins présents et futurs de l'ensemble des modes d'habitat, d'activités économiques, touristiques, sportives, culturelles et d'intérêt général ainsi que d'équipements publics et d'équipement commercial, en tenant compte en particulier des objectifs de répartition géographiquement équilibrée entre emploi, habitat, commerces et services, d'amélioration des performances énergétiques, de développement des communications électroniques, de diminution des obligations de déplacements motorisés et de développement des transports alternatifs à l'usage individuel de l'automobile ;

3° La réduction des émissions de gaz à effet de serre, la maîtrise de l'énergie et la production énergétique à partir de sources renouvelables, la préservation de la qualité de l'air, de l'eau, du sol et du sous-sol, des ressources naturelles, de la biodiversité, des écosystèmes, des espaces verts, la préservation et la remise en bon état des continuités écologiques, et la prévention des risques naturels prévisibles, des risques miniers, des risques technologiques, des pollutions et des nuisances de toute nature.
