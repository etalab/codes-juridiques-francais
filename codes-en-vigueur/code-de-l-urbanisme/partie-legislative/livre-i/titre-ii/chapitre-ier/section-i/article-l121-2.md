# Article L121-2

Dans les conditions précisées par le présent titre, l'Etat veille au respect des principes définis à l'article L. 121-1 et à la prise en compte des projets d'intérêt général ainsi que des opérations d'intérêt national.

Le préfet porte à la connaissance des communes ou de leurs groupements compétents le cadre législatif et réglementaire à respecter, ainsi que les projets des collectivités territoriales et de l'Etat en cours d'élaboration ou existants. Tout retard ou omission dans la transmission de ces informations est sans effet sur les procédures engagées par les communes ou leurs groupements.

Le préfet leur transmet à titre d'information l'ensemble des études techniques nécessaires à l'exercice de leur compétence en matière d'urbanisme dont il dispose.

Les porters à connaissance sont tenus à la disposition du public par les communes ou leurs groupements compétents. En outre, tout ou partie de ces pièces peut être annexé au dossier d'enquête publique.
