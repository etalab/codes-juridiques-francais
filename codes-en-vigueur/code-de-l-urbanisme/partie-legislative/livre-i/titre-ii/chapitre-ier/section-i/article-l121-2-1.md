# Article L121-2-1

Le représentant de l'Etat dans le département transmet aux maires et aux présidents d'établissements publics de coopération intercommunale compétents en matière de politique locale de l'habitat qui en font la demande la liste des immeubles situés sur le territoire des communes où ils exercent leur compétence et appartenant à l'Etat et à ses établissements publics.
