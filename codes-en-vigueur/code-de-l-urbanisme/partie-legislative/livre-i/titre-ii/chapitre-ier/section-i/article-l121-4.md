# Article L121-4

I. ― L'Etat, les régions, les départements, les autorités organisatrices prévues à l'article L. 1231-1 du code des transports, les établissements publics de coopération intercommunale compétents en matière de programme local de l'habitat et les organismes de gestion des parcs naturels régionaux et des parcs nationaux sont associés à l'élaboration des schémas de cohérence territoriale et des plans locaux d'urbanisme dans les conditions définies aux chapitres II et III.

Il en est de même des chambres de commerce et d'industrie territoriales, des chambres de métiers, des chambres d'agriculture et, dans les communes littorales au sens de l'article L. 321-2 du code de l'environnement, des sections régionales de la conchyliculture. Ces organismes assurent les liaisons avec les organisations professionnelles intéressées.

Les études économiques nécessaires à la préparation des documents prévisionnels d'organisation commerciale et artisanale peuvent être réalisées à l'initiative des chambres de commerce et d'industrie territoriales et des chambres de métiers.

II. ― Pour l'élaboration des schémas de cohérence territoriale, sont, en outre, associés dans les mêmes conditions :

1° Les syndicats mixtes de transports créés en application de l'article L. 1231-10 du code des transports, lorsque le schéma est élaboré par un établissement public qui n'exerce pas les compétences définies aux articles L. 1231-10 et L. 1231-11 du même code ;

2° Les établissements publics chargés de l'élaboration, de la gestion et de l'approbation des schémas de cohérence territoriale limitrophes.

III. ― Pour l'élaboration des plans locaux d'urbanisme sont également associés, dans les mêmes conditions :

1° Les syndicats d'agglomération nouvelle ;

2° L'établissement public chargé de l'élaboration, de la gestion et de l'approbation du schéma de cohérence territoriale lorsque le territoire objet du plan est situé dans le périmètre de ce schéma ;

3° Les établissements publics chargés de l'élaboration, de la gestion et de l'approbation des schémas de cohérence territoriale limitrophes du territoire objet du plan lorsque ce territoire n'est pas couvert par un schéma de cohérence territoriale.
