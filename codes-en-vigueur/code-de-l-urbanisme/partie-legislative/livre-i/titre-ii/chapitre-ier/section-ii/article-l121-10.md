# Article L121-10

I. ― Font l'objet d'une évaluation environnementale, dans les conditions prévues par la directive 2001/42/ CE du Parlement européen et du Conseil, du 27 juin 2001, relative à l'évaluation des incidences de certains plans et programmes sur l'environnement ainsi que ses annexes et par la présente section :

1° Les directives territoriales d'aménagement et les directives territoriales d'aménagement et de développement durables ;

2° Le schéma directeur de la région d'Ile-de-France ;

3° Les schémas de cohérence territoriale et les schémas de secteur ;

4° Les prescriptions particulières de massif prévues à l'article L. 145-7.

II. ― Font également l'objet de l'évaluation environnementale prévue au premier alinéa du I les documents qui déterminent l'usage de petites zones au niveau local suivants :

1° Les plans locaux d'urbanisme :

a) Qui sont susceptibles d'avoir des effets notables sur l'environnement, au sens de l'annexe II à la directive 2001/42/ CE du Parlement européen et du Conseil, du 27 juin 2001, précitée, compte tenu notamment de la superficie du territoire auquel ils s'appliquent, de la nature et de l'importance des travaux et aménagements qu'ils autorisent et de la sensibilité du milieu dans lequel ceux-ci doivent être réalisés ;

b) Ou qui comprennent les dispositions des plans de déplacements urbains mentionnés aux articles 28 à 28-4 de la loi n° 82-1153 du 30 décembre 1982 d'orientation des transports intérieurs ;

2° Les cartes communales qui sont susceptibles d'avoir des incidences notables sur l'environnement, au sens de l'annexe II à la directive 2001/42/CE du Parlement européen et du Conseil du 27 juin 2001 précitée, au regard, notamment, de la superficie du territoire auquel elles s'appliquent, de la nature, de la sensibilité et de l'étendue des territoires couverts par les secteurs qu'elles déterminent, dans des conditions précisées par décret en Conseil d'Etat ;

3° Les schémas d'aménagement prévus à l'article L. 146-6-1 du présent code.

III. ― Sauf dans le cas où elles ne prévoient que des changements qui ne sont pas susceptibles d'avoir des effets notables sur l'environnement, au sens de l'annexe II à la directive 2001/42/ CE du Parlement européen et du Conseil, du 27 juin 2001, précitée, les modifications des documents mentionnés aux I et II du présent article donnent lieu soit à une nouvelle évaluation environnementale, soit à une actualisation de l'évaluation environnementale réalisée lors de leur élaboration.
