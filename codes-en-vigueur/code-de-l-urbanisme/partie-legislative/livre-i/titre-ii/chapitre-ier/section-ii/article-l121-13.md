# Article L121-13

Les documents d'urbanisme mentionnés à l'article L. 121-10 dont la mise en oeuvre est susceptible de produire des effets notables sur l'environnement d'un autre Etat membre de la Communauté européenne sont transmis aux autorités de cet Etat, à la demande de celles-ci ou à l'initiative des autorités françaises. L'Etat intéressé est invité à donner son avis dans un délai fixé par décret en Conseil d'Etat. En l'absence de réponse dans ce délai, l'avis est réputé émis.

Lorsqu'un document d'urbanisme dont la mise en oeuvre est susceptible de produire des effets notables sur le territoire national est transmis pour avis aux autorités françaises par un autre Etat, il peut être décidé de consulter le public sur le projet.

Les dispositions du présent article ne font pas obstacle à l'application de l'article L. 121-4-1.
