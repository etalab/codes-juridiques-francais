# Article L122-7

Un débat a lieu au sein de l'organe délibérant de l'établissement public prévu à l'article L. 122-4 sur les orientations du projet d'aménagement et de développement durables au plus tard quatre mois avant l'examen du projet de schéma.
