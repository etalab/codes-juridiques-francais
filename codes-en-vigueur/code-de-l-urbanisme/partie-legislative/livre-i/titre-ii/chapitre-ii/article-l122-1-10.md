# Article L122-1-10

En zone de montagne, le document d'orientation et d'objectifs définit :

1° La localisation, la consistance et la capacité globale d'accueil et d'équipement des unités touristiques nouvelles mentionnées au I de l'article L. 145-11 ;

2° Les principes d'implantation et la nature des unités touristiques nouvelles mentionnées au II du même article L. 145-11.
