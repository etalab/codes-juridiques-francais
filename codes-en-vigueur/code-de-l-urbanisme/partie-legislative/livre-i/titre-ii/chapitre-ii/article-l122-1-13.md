# Article L122-1-13

Lorsqu'un plan de gestion des risques d'inondation, mentionné à l'article L. 566-7 du code de l'environnement, est approuvé, les schémas de cohérence territoriale doivent être compatibles avec les objectifs de gestion des risques d'inondation et les orientations fondamentales définis par ce plan. Les schémas de cohérence territoriale doivent également être compatibles avec les dispositions des plans de gestion des risques d'inondation définies en application des 1° et 3° du même article L. 566-7.

Lorsqu'un plan de gestion des risques d'inondation est approuvé après l'approbation d'un schéma de cohérence territoriale, ce dernier doit, si nécessaire, être rendu compatible dans un délai de trois ans avec les éléments mentionnés au premier alinéa du présent article.

Dans ce cas, et par dérogation aux dispositions de l'article L. 111-1-1 du présent code, les schémas de cohérence territoriale n'ont pas à être compatibles avec les orientations fondamentales relatives à la prévention des inondations définies par les schémas directeurs d'aménagement et de gestion des eaux en application de l'article L. 212-1 du code de l'environnement.
