# Article L122-6-2

A leur demande, le président de l'établissement public mentionné à l'article L. 122-4 ou son représentant consulte la commission départementale de la   préservation des espaces naturels, agricoles et forestiers prévue à l'article L. 112-1-1 du code rural et de la pêche maritime, les communes limitrophes du périmètre du schéma de cohérence territoriale ainsi que les associations mentionnées à l'article L. 121-5.

Le président de l'établissement public, ou son représentant, peut recueillir l'avis de tout organisme ou association compétent en matière d'aménagement du territoire, d'urbanisme, d'environnement, d'architecture, d'habitat et de déplacements, y compris des collectivités territoriales des Etats limitrophes.
