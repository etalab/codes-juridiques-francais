# Titre II : Prévisions et règles d'urbanisme

- [Chapitre Ier : Dispositions générales communes aux schémas de cohérence territoriale, aux plans locaux d'urbanisme et aux cartes communales](chapitre-ier)
- [Chapitre II : Schémas de cohérence territoriale.](chapitre-ii)
- [Chapitre VI : Servitudes d'utilité publique affectant l'utilisation du sol.](chapitre-vi)
- [Chapitre VII : Dispositions favorisant la diversité de l'habitat.](chapitre-vii)
- [Chapitre VIII : Dispositions favorisant la performance énergétique et les énergies renouvelables dans l'habitat](chapitre-viii)
- [Chapitre IX : Dispositions favorisant la transmission et l'accès à l'information en matière d'urbanisme](chapitre-ix)
