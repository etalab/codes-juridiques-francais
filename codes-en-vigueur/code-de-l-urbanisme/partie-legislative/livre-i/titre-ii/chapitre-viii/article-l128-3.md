# Article L128-3

L'application combinée des articles L. 127-1, L. 128-1 et L. 128-2 ne peut conduire à autoriser un dépassement de plus de 50 %                  du volume autorisé par le gabarit de la construction.
