# Article L129-2

I.-A compter du 1er janvier 2016, les communes ou leurs groupements compétents transmettent à l'Etat sous format électronique, au fur et à mesure des modifications de leurs dispositions, la version en vigueur des schémas de cohérence territoriale, des plans locaux d'urbanisme, des documents en tenant lieu et des cartes communales applicables sur leur territoire incluant les délibérations les ayant approuvés.

II.-A compter du 1er juillet 2015, tout gestionnaire d'une servitude d'utilité publique visée à l'article L. 126-1 transmet à l'Etat, sous format électronique en vue de son insertion dans le portail national de l'urbanisme, la servitude dont il assure la gestion qui figure sur une liste dressée par décret en Conseil d'Etat.

L'insertion de ces servitudes dans le portail national de l'urbanisme ne doit pas porter atteinte à la conduite de la politique extérieure de la France, à la sécurité publique ou à la défense nationale.

Les dispositions du présent II ne font pas obstacle à ce que l'ensemble des servitudes demeurent transmises à l'Etat puis portées à la connaissance des communes et à leurs groupements dans le cadre de l'exercice de leurs compétences en matière d'urbanisme conformément aux dispositions de l'article L. 121-2 du code de l'urbanisme.

III.-La numérisation des documents d'urbanisme et des servitudes d'utilité publique en vue des transmissions prévues aux I et II s'effectue conformément aux standards de numérisation validés par la structure de coordination nationale prévue par les articles 18 et 19, paragraphe 2, de la directive 2007/2/ CE du Parlement européen et du Conseil du 14 mars 2007 établissant une infrastructure d'information géographique dans la Communauté européenne.

Si aucun standard de numérisation n'est validé dans les conditions du précédent alinéa, la numérisation des documents est effectuée dans un format de fichiers largement disponible.

IV.-Un arrêté du ministre chargé de l'urbanisme précise les modalités de transmission des documents d'urbanisme et des servitudes d'utilité publique prévus aux I et II.
