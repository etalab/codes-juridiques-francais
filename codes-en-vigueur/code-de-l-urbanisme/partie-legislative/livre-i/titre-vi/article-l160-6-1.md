# Article L160-6-1

Une servitude de passage des piétons, transversale au rivage peut être instituée sur les voies et chemins privés d'usage collectif existants, à l'exception de ceux réservés à un usage professionnel selon la procédure prévue au deuxième alinéa de l'article L. 160-6.

Cette servitude a pour but de relier la voirie publique au rivage de la mer ou aux sentiers d'accès immédiat à celui-ci, en l'absence de voie publique située à moins de cinq cent mètres et permettant l'accès au rivage.

En Guadeloupe, en Guyane, en Martinique, à La Réunion et à Mayotte la servitude transversale peut également être instituée, outre sur les voies et chemins privés d'usage collectif existants, sur les propriétés limitrophes du domaine public maritime par création d'un chemin situé à une distance d'au moins cinq cents mètres de toute voie publique d'accès transversale au rivage. L'emprise de cette servitude est de trois mètres de largeur maximum. Elle est distante d'au moins dix mètres des bâtiments à usage d'habitation édifiés avant le 1er août 2010. Cette distance n'est toutefois applicable aux terrains situés dans la zone comprise entre la limite du rivage de la mer et la limite supérieure de la zone dite des cinquante pas géométriques définie par l'article L. 5111-2 du code général de la propriété des personnes publiques et, à Mayotte, par l'article L. 5331-4 de ce code, que si les terrains ont été acquis de l'Etat avant le 1er août 2010 ou en vertu d'une demande déposée avant cette date.

Les dispositions de l'article L. 160-7 sont applicables à cette servitude.
