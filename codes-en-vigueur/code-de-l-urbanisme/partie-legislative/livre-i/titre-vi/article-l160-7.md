# Article L160-7

La servitude instituée par l'article L. 160-6 n'ouvre un droit à indemnité que s'il en résulte pour le propriétaire un dommage direct, matériel et certain.

La demande d'indemnité doit, à peine de forclusion, parvenir à l'autorité compétente dans le délai de six mois à compter de la date où le dommage a été causé.

L'indemnité est fixée soit à l'amiable, soit, en cas de désaccord, dans les conditions définies au deuxième alinéa de l'article L. 160-5.

Le montant de l'indemnité de privation de jouissance est calculé compte tenu de l'utilisation habituelle antérieure du terrain.

La responsabilité civile des propriétaires des terrains, voies et chemins grevés par les servitudes définies aux articles L. 160-6 et L. 160-6-1 ne saurait être engagée au titre de dommages causés ou subis par les bénéficiaires de ces servitudes.
