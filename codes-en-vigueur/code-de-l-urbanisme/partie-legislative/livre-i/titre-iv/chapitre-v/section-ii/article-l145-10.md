# Article L145-10

A l'exception du III de l'article L. 145-3, les dispositions de la section première du présent chapitre et les dispositions du chapitre II du titre IV du livre III du code du tourisme sont applicables aux unités touristiques nouvelles.
