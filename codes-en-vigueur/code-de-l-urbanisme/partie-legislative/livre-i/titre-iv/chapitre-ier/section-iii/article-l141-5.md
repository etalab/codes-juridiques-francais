# Article L141-5

Il est créé une zone de protection naturelle, agricole et forestière dans le périmètre de l'opération d'intérêt national du plateau de Saclay et de la petite région agricole de ce plateau qui comprend les communes dont la liste figure à l'annexe B à la loi n° 2010-597 du 3 juin 2010 relative au Grand Paris. Cette zone, non urbanisable, est délimitée par décret en Conseil d'Etat, pris dans un délai d'un an à compter de la promulgation de la même loi, après avis du conseil régional d'Ile-de-France, des conseils départementaux de l'Essonne et des Yvelines, des conseils municipaux et des organes délibérants des établissements publics de coopération intercommunale compétents situés dans le périmètre de l'opération d'intérêt national, ainsi que de la chambre interdépartementale d'agriculture d'Ile-de-France, de la société d'aménagement foncier et d'établissement rural de l'Ile-de-France, de l'Office national des forêts et des associations agréées pour la protection de l'environnement présentes dans le périmètre d'intervention de l'Etablissement public d'aménagement de Paris-Saclay.

Cette zone comprend au moins 2 300 hectares de terres consacrées à l'activité agricole situées sur les communes figurant à l'annexe B précitée.

Pour l'exercice de ses missions, l'organe délibérant de l'Etablissement public d'aménagement de Paris-Saclay définit les secteurs indispensables au développement du pôle scientifique et technologique. Ces secteurs ne peuvent être inclus dans la zone de protection.

La zone est délimitée après enquête publique conduite dans les conditions définies par le chapitre III du titre II du livre Ier du code de l'environnement. L'enquête porte également sur la ou les mises en compatibilité visées au dernier alinéa du présent article.

Une carte précisant le mode d'occupation du sol est annexée au décret en Conseil d'Etat précité.

L'interdiction d'urbaniser dans la zone de protection vaut servitude d'utilité publique et est annexée aux plans locaux d'urbanisme ou aux cartes communales des communes intéressées, dans les conditions prévues par l'article L. 126-1 du présent code.

Les communes intéressées disposent d'un délai de six mois à compter de la publication du décret en Conseil d'Etat visé au premier alinéa du présent article pour mettre en compatibilité leur plan local d'urbanisme.
