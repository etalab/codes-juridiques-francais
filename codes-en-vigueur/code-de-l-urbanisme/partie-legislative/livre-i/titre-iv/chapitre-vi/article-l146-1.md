# Article L146-1

Les dispositions du présent chapitre déterminent les conditions d'utilisation des espaces terrestres, maritimes et lacustres :

-dans les communes littorales définies à l'article 2 de la loi n° 86-2 du 3 janvier 1986 relative à l'aménagement, la protection et la mise en valeur du littoral ;

-dans les communes qui participent aux équilibres économiques et écologiques littoraux, lorsqu'elles en font la demande auprès du représentant de l'Etat dans le département. La liste de ces communes est fixée par décret en Conseil d'Etat, après avis du conservatoire de l'espace littoral et des rivages lacustres.

Les directives territoriales d'aménagement prévues à l'article L. 111-1-1 peuvent préciser les modalités d'application du présent chapitre. Ces directives sont établies par décret en Conseil d'Etat après avis ou sur proposition des conseils régionaux intéressés et après avis des départements et des communes ou groupements de communes concernés.

Les directives territoriales d'aménagement précisant les modalités d'application du présent chapitre ou, en leur absence, lesdites dispositions sont applicables à toute personne publique ou privée pour l'exécution de tous travaux, constructions, défrichements, plantations, installations et travaux divers, la création de lotissements et l'ouverture de terrains de camping ou de stationnement de caravanes, l'établissement de clôtures, pour l'ouverture de carrières, la recherche et l'exploitation de minerais. Elles sont également applicables aux installations classées pour la protection de l'environnement.
