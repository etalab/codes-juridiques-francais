# Article L143-1

Pour mettre en oeuvre une politique de protection et de mise en valeur des espaces agricoles et naturels périurbains, le département ou un établissement public ou un syndicat mixte mentionné à l'article L. 122-4 peut délimiter des périmètres d'intervention associés à des programmes d'action avec l'accord de la ou des communes concernées ou des établissements publics compétents en matière de plan local d'urbanisme, après avis de la chambre départementale d'agriculture et enquête publique réalisée conformément au chapitre III du titre II du livre Ier du code de l'environnement. Les périmètres approuvés et les programmes d'action associés sont tenus à la disposition du public.

Ces périmètres doivent être compatibles avec le schéma de cohérence territoriale, s'il en existe un. Ils ne peuvent inclure des terrains situés dans une zone urbaine ou à urbaniser délimitée par un plan local d'urbanisme, dans un secteur constructible délimité par une carte communale ou dans un périmètre ou un périmètre provisoire de zone d'aménagement différé.

L'établissement public ou le syndicat mixte mentionné au même article L. 122-4 ne peut définir un tel périmètre que sur le territoire des communes qui le composent.

Lorsqu'un établissement public ou un syndicat mixte mentionné audit article L. 122-4 est à l'initiative du périmètre de protection et de mise en valeur des espaces agricoles et naturels périurbains, les enquêtes publiques préalables à la création de ce périmètre et du schéma de cohérence territoriale peuvent être concomitantes.
