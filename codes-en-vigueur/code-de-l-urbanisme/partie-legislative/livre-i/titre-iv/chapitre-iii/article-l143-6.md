# Article L143-6

Un décret en Conseil d'Etat détermine les conditions d'application du présent chapitre. Il approuve les clauses types des cahiers des charges prévus par l'article L. 143-3, qui précisent notamment les conditions selon lesquelles cessions, locations ou concessions temporaires sont consenties et résolues en cas d'inexécution des obligations du cocontractant.
