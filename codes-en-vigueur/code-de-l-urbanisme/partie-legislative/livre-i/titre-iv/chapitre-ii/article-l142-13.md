# Article L142-13

Un décret en Conseil d'Etat détermine, en tant que de besoin, les conditions d'application du présent chapitre.
