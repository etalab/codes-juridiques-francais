# Article L111-1-5

En dehors des zones couvertes par un plan local d'urbanisme ou un document d'urbanisme en tenant lieu, l'autorité administrative peut, par arrêté pris dans des conditions fixées par décret en Conseil d'Etat, délimiter un périmètre à l'intérieur duquel l'exécution de travaux de la nature de ceux visés à l'article L. 421-1 est soumise à des règles particulières rendues nécessaires par l'existence d'installations classées pour la protection de l'environnement ou de stockage souterrain de gaz naturel, d'hydrocarbures liquides, liquéfiés ou gazeux ou de produits chimiques à destination industrielle.

Ces dispositions ne sont pas applicables aux installations classées bénéficiant de l'application des articles L. 515-8 à L. 515-12 du code de l'environnement ainsi qu'aux stockages souterrains visés à l'alinéa précédent bénéficiant de l'application du deuxième alinéa de l'article L. 264-1 du code minier.

Le permis de construire mentionne explicitement, le cas échéant, les servitudes instituées en application des dispositions précitées du code de l'environnement et du code minier.
