# Article L111-3-1

Les projets d'aménagement et la réalisation des équipements collectifs et des programmes de construction qui, par leur importance, leur localisation ou leurs caractéristiques propres peuvent avoir des incidences sur la protection des personnes et des biens contre les menaces et les agressions, doivent faire l'objet d'une étude préalable de sécurité publique permettant d'en apprécier les conséquences.

Un décret en Conseil d'Etat précise les modalités d'application du présent article. Il détermine :

-les seuils à partir desquels les projets d'aménagement, les équipements collectifs et les programmes de construction sont soumis à l'obligation mentionnée au premier alinéa et les conditions dans lesquelles le préfet, à la demande ou après avis du maire, peut délimiter les secteurs dont les caractéristiques particulières justifient l'application de seuils inférieurs ;

-le contenu de l'étude de sécurité publique, celle-ci devant porter au minimum sur les risques que peut entraîner le projet pour la protection des personnes et des biens contre la délinquance et sur les mesures envisagées pour les prévenir.

Lorsque l'opération porte sur un établissement recevant du public, le permis de construire ne peut être délivré si l'autorité compétente a constaté, après avis de la commission compétente en matière de sécurité publique, que l'étude remise ne remplit pas les conditions définies par le décret en Conseil d'Etat prévu au deuxième alinéa. En l'absence de réponse dans un délai de deux mois, l'avis de la commission est réputé favorable.

L'étude de sécurité publique constitue un document non communicable au sens du I de l'article 6 de la loi n° 78-753 du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal. Le maire peut obtenir communication de cette étude.
