# Article L111-8

Le sursis à statuer doit être motivé et ne peut excéder deux ans.

Lorsqu'une décision de sursis a été prise en application des articles visés à l'article L. 111-7, l'autorité compétente ne peut, à l'expiration du délai de validité du sursis ordonné, opposer à une même demande d'autorisation un nouveau sursis fondé sur le même motif que le sursis initial.

Si des motifs différents rendent possible l'intervention d'une décision de sursis à statuer par application d'une disposition législative autre que celle qui a servi de fondement au sursis initial, la durée totale des sursis ordonnés ne peut en aucun cas excéder trois ans.

A l'expiration du délai de validité du sursis à statuer, une décision doit, sur simple confirmation par l'intéressé de sa demande, être prise par l'autorité compétente chargée de la délivrance de l'autorisation, dans le délai de deux mois suivant cette confirmation. Cette confirmation peut intervenir au plus tard deux mois après l'expiration du délai de validité du sursis à statuer. Une décision définitive doit alors être prise par l'autorité compétente pour la délivrance de l'autorisation, dans un délai de deux mois suivant cette confirmation. A défaut de notification de la décision dans ce dernier délai, l'autorisation est considérée comme accordée dans les termes où elle avait été demandée.
