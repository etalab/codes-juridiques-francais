# Article L113-2

Le projet de directive territoriale d'aménagement et de développement durables est élaboré par l'Etat, en association avec la région, le département, les métropoles, les communautés urbaines, les communautés d'agglomération, les communautés de communes compétentes en matière de schéma de cohérence territoriale et les communes non membres d'une de ces communautés qui sont situées dans le périmètre du projet ainsi que les établissements publics mentionnés à l'article L. 122-4.

Il est soumis pour avis à ces collectivités territoriales et établissements publics. Cet avis est réputé favorable s'il n'a pas été rendu par écrit dans un délai de trois mois à compter de leur saisine.
