# Chapitre VII : Autres dispositions particulières à Mayotte

- [Article L157-1](article-l157-1.md)
- [Article L157-2](article-l157-2.md)
