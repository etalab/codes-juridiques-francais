# Article L156-1

Les dispositions du chapitre VI du titre IV du livre Ier sont applicables, en Guadeloupe, en Guyane, en Martinique et à La Réunion, aux communes littorales définies à l'article L. 321-2 du code de l'environnement, et à Mayotte à l'ensemble des communes, sous réserve des dispositions prévues aux articles L. 156-2 à L. 156-4.
