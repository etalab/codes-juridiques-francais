# Article L211-2

Lorsque la commune fait partie d'un établissement public de coopération intercommunale y ayant vocation, elle peut, en accord avec cet établissement, lui déléguer tout ou partie des compétences qui lui sont attribuées par le présent chapitre.

Toutefois, la compétence d'un établissement public de coopération intercommunale à fiscalité propre, ainsi que celle de la métropole de Lyon en matière de plan local d'urbanisme, emporte leur compétence de plein droit en matière de droit de préemption urbain.
