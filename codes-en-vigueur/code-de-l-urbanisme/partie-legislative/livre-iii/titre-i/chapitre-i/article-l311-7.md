# Article L311-7

Les plans d'aménagement de zone approuvés avant l'entrée en vigueur de la loi n° 2000-1208 du 13 décembre 2000 précitée demeurent applicables jusqu'à l'approbation par la commune ou l'établissement public de coopération intercommunale compétent d'un plan local d'urbanisme. Ils ont les mêmes effets pour la zone intéressée que les plans locaux d'urbanisme. Ils sont soumis au régime juridique des plans locaux d'urbanisme tel qu'il est défini par les articles L. 123-1 à L. 123-18, à l'exception du deuxième alinéa de l'article L. 123-1.

Ils peuvent faire l'objet :

a) D'une modification, à condition que le changement apporté au plan d'aménagement de zone :

-ne porte pas atteinte à l'économie générale des orientations d'urbanisme concernant l'ensemble de la commune ;

-ne réduise pas un espace boisé classé, une zone agricole ou une zone naturelle et forestière, ou une protection édictée en raison des risques de nuisance, de la qualité des sites, des paysages ou des milieux naturels ;

-ne comporte pas de graves risques de nuisance.

b) D'une modification simplifiée dans les conditions définies aux articles L. 123-13-1 et L. 123-13-3 ;

c) D'une mise en compatibilité selon les modalités définies par les articles L. 123-14 et L. 123-14-2.

Les projets de plan d'aménagement de zone qui ont été arrêtés en vue d'être soumis à enquête publique conformément à l'article L. 311-4 en vigueur avant l'application de la loi n° 2000-1208 du 13 décembre 2000 précitée, demeurent soumis aux dispositions législatives antérieures. Ils seront intégrés aux plans locaux d'urbanisme dès leur approbation.
