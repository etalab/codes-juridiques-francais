# Article L331-3

La part départementale de la taxe d'aménagement est instituée par délibération du conseil départemental dans les conditions fixées au neuvième alinéa de l'article L. 331-2 en vue de financer, d'une part, la politique de protection des espaces naturels sensibles prévue à l'article L. 142-1 ainsi que les dépenses prévues à l'article L. 142-2 et, d'autre part, les dépenses des conseils d'architecture, d'urbanisme et de l'environnement en application de l'article 8 de la loi n° 77-2 du 3 janvier 1977 sur l'architecture.

La part départementale de la taxe est instituée dans toutes les communes du département.

Le produit de la part départementale de la taxe a le caractère d'une recette de fonctionnement.

La métropole de Lyon est substituée au département du Rhône pour l'application des trois alinéas précédents aux autorisations d'urbanisme délivrées à compter du 1er janvier 2017 dans le périmètre de la métropole de Lyon. Les produits perçus à ce titre reviennent à la métropole de Lyon, en sus de ceux qui lui échoient en vertu du 3° de l'article L. 331-2.
