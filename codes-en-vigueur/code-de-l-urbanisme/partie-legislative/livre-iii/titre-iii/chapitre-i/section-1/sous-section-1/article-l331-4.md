# Article L331-4

La part de la taxe d'aménagement versée à la région d'Ile-de-France est instituée par délibération du conseil régional, dans les conditions fixées au neuvième  alinéa de l'article L. 331-2, en vue de financer des équipements collectifs, principalement des infrastructures de transport, rendus nécessaires par l'urbanisation.

Elle est instituée dans toutes les communes de la région.

Le produit de la taxe est affecté en section d'investissement du budget de la région d'Ile-de-France.
