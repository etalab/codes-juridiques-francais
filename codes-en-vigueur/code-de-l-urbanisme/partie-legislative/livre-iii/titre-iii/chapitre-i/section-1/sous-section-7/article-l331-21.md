# Article L331-21

Le droit de reprise de l'administration s'exerce jusqu'au 31 décembre de la troisième année qui suit, selon les cas, celle de la délivrance de l'autorisation de construire ou d'aménager, celle de la décision de non-opposition ou celle à laquelle l'autorisation est réputée avoir été accordée.

En cas de construction ou d'aménagement sans autorisation ou en infraction aux obligations résultant d'une autorisation de construire, le droit de reprise s'exerce jusqu'au 31 décembre de la sixième année qui suit celle de l'achèvement des constructions ou aménagements en cause.
