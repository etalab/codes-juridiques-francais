# Article L331-46

Le produit des versements dus au titre des densités de construction inférieures au seuil minimal de densité est attribué à la métropole de Lyon, aux communes ou établissements publics de coopération intercommunale mentionnés au premier alinéa de l'article L. 331-36.
