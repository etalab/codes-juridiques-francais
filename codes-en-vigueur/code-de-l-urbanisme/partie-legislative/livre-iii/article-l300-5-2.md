# Article L300-5-2

Les dispositions du deuxième alinéa de l'article L. 300-4 ne sont pas applicables aux concessions d'aménagement conclues entre le concédant et un aménageur sur lequel il exerce un contrôle analogue à celui qu'il exerce sur ses propres services et qui réalise l'essentiel de ses activités avec lui ou, le cas échéant, les autres personnes publiques qui le contrôlent.
