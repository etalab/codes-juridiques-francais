# Article L321-16

Les établissements publics d'aménagement sont habilités à créer des filiales et à acquérir des participations dans des sociétés, groupements ou organismes dont l'objet concourt à la réalisation de leurs missions dans les conditions déterminées par le décret prévu à l'article L. 321-28.
