# Article L321-19

Le conseil d'administration approuve le projet stratégique et opérationnel et procède à sa révision.

L'approbation et la révision prévues à l'alinéa précédent interviennent dans les conditions prévues par le décret mentionné à l'article L. 321-28.

En cas de modification des orientations stratégiques de l'Etat, le projet stratégique et opérationnel est, si nécessaire, révisé et approuvé dans un délai fixé par le décret prévu à l'article L. 321-28.
