# Article L321-6

Le conseil d'administration de l'établissement approuve le programme pluriannuel d'intervention et procède à sa révision.

Cette approbation et cette révision interviennent dans les conditions prévues par le décret mentionné à l'article L. 321-13.

En cas de modification des orientations stratégiques de l'Etat, le programme pluriannuel d'intervention est, si nécessaire, révisé et approuvé dans un délai fixé par le décret prévu à l'article L. 321-13.
