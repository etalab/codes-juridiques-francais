# Article L321-1-1

<div align="left">Nonobstant les missions définies à l'article L. 321-1, l'Etat peut confier par décret en Conseil d'Etat, en application de l'article L. 741-2 du code de la construction et de l'habitation, à un établissement public foncier la conduite d'une opération de requalification de copropriétés dégradées d'intérêt national, après avis de son conseil d'administration. L'avis est réputé favorable en l'absence de réponse dans un délai de trois mois. <br/>
<br/>La conduite de l'opération mentionnée au premier alinéa du présent article comporte : <br/>
<br/>1° La coordination des actions des personnes publiques signataires de la convention mentionnée à l'article L. 741-1 du code de la construction et de l'habitation ainsi que la préparation de ladite convention ; <br/>
<br/>2° La réalisation de tout ou partie des actions mentionnées aux 1°, 4°, 5° et 6° du même article L. 741-1. <br/>
<br/>Pour conduire une opération de requalification de copropriétés dégradées d'intérêt national, l'établissement public foncier d'Ile-de-France peut bénéficier du concours de l'Agence foncière et technique de la région parisienne mentionnée à l'article L. 321-29 du présent code, selon des modalités fixées par convention entre les deux établissements.<br/>
</div>
