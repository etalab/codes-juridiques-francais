# Article L321-30

L'Agence foncière et technique de la région parisienne est habilitée à créer des filiales et à acquérir des participations dans des sociétés, groupements ou organismes dont l'objet concourt à la réalisation de ses missions.
