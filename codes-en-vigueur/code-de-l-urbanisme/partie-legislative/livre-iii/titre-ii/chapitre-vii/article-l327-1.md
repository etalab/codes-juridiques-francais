# Article L327-1

Les collectivités territoriales et leurs groupements peuvent, créer, dans le cadre des compétences qui leur sont attribuées par la loi, des sociétés publiques locales d'aménagement dont ils détiennent la totalité du capital.

Une des collectivités territoriales ou un des groupements de collectivités territoriales participant à une société publique locale d'aménagement détient au moins la majorité des droits de vote.

Ces sociétés sont compétentes pour réaliser toute opération d'aménagement au sens du présent code. Elles sont également compétentes pour réaliser les opérations de requalification de copropriétés dégradées prévues à l'article L. 741-1 du code de la construction et de l'habitation, réaliser des études préalables, procéder à toute acquisition et cession d'immeubles en application des articles L. 221-1 et L. 221-2, procéder à toute opération de construction ou de réhabilitation immobilière en vue de la réalisation des objectifs énoncés à l'article L. 300-1, ou procéder à toute acquisition et cession de baux commerciaux, de fonds de commerce ou de fonds artisanaux dans les conditions prévues au chapitre IV du titre Ier du livre II du présent code. Elles peuvent exercer, par délégation de leurs titulaires, les droits de préemption et de priorité définis par le présent code et agir par voie d'expropriation dans les conditions fixées par des conventions conclues avec l'un de leurs membres.

Ces sociétés exercent leurs activités exclusivement pour le compte de leurs actionnaires et sur le territoire des collectivités territoriales et des groupements de collectivités territoriales qui en sont membres.

Ces sociétés revêtent la forme de société anonyme régie par le livre II du code de commerce et sont composées, par dérogation à l'article L. 225-1 du même code, d'au moins deux actionnaires.

Sous réserve des dispositions du présent article, elles sont soumises au titre II du livre V de la première partie du code général des collectivités territoriales.
