# Article L322-3-2

L'autorité administrative recueille, préalablement à la création de l'association, l'accord du conseil municipal sur l'opération lorsqu'un plan local d'urbanisme a été approuvé sur le territoire de la commune. Dans les autres cas, ou si l'association foncière urbaine est située à l'intérieur d'un périmètre d'opération d'intérêt national, l'autorité administrative recueille l'avis du conseil municipal.
