# Article L425-13

<div align="left">Lorsque le demandeur joint à sa demande de permis de construire une demande de dérogation prévue à l'article L. 111-4-1 du code de la construction et de l'habitation, le permis de construire ne peut pas être accordé avant l'obtention de cette dérogation.</div>
