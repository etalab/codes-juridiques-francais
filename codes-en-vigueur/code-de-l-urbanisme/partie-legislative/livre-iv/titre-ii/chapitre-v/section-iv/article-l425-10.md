# Article L425-10

Lorsque le projet porte sur une installation classée soumise à autorisation en application de l'article L. 512-2 du code de l'environnement ou à enregistrement en application de l'article L. 512-7 de ce code, les travaux ne peuvent être exécutés :

a) Avant la clôture de l'enquête publique pour les installations soumises à autorisation ;

b) Avant la décision d'enregistrement prévue à l'article L. 512-7-3 de ce code pour les installations soumises à enregistrement.
