# Section IV : Opérations pour lesquelles la délivrance d'un permis ou la réalisation des travaux est différée dans l'attente de formalités prévues par une autre législation

- [Article L425-6](article-l425-6.md)
- [Article L425-8](article-l425-8.md)
- [Article L425-9](article-l425-9.md)
- [Article L425-10](article-l425-10.md)
- [Article L425-11](article-l425-11.md)
- [Article L425-12](article-l425-12.md)
- [Article L425-13](article-l425-13.md)
