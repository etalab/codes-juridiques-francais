# Article L423-1

Les demandes de permis de construire, d'aménager ou de démolir et les déclarations préalables sont présentées et instruites dans les conditions et délais fixés par décret en Conseil d'Etat.

Sur demande du maire, un plan intérieur du projet concerné doit être joint au dossier de demande de permis de construire ou à la déclaration préalable lorsque les demandes de permis de construire ou les déclarations préalables concernent la construction de logements collectifs.

Aucune prolongation du délai d'instruction n'est possible en dehors des cas et conditions prévus par ce décret.

Pour l'instruction des dossiers d'autorisations ou de déclarations prévus au présent titre, le maire ou, s'il est compétent, le président de l'établissement public de coopération intercommunale peut déléguer sa signature aux agents chargés de l'instruction des demandes.
