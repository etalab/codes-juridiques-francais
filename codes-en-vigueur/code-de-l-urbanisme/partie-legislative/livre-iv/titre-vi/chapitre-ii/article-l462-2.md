# Article L462-2

L'autorité compétente mentionnée aux articles L. 422-1 à L. 422-3 peut, dans un délai fixé par décret en Conseil d'Etat, procéder ou faire procéder à un récolement des travaux et, lorsque ceux-ci ne sont pas conformes au permis délivré ou à la déclaration préalable, mettre en demeure le maître de l'ouvrage de déposer un dossier modificatif ou de mettre les travaux en conformité. Un décret en Conseil d'Etat fixe les cas où le récolement est obligatoire.

Passé ce délai, l'autorité compétente ne peut plus contester la conformité des travaux.
