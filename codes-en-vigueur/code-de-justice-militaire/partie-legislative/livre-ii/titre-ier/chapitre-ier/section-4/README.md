# Section 4 : Des juridictions d'instruction

- [Sous-section 1 : De l'instruction préparatoire.](sous-section-1)
- [Sous-section 2 : De la détention provisoire et de la liberté.](sous-section-2)
- [Sous-section 4 : De la réouverture de l'information sur charges nouvelles.](sous-section-4)
