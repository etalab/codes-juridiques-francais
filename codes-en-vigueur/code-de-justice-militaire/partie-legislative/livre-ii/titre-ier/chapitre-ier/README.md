# Chapitre Ier : En temps de paix et hors du territoire de la République

- [Section 1 : De la police judiciaire et des enquêtes.](section-1)
- [Section 2 : De la garde à vue.](section-2)
- [Section 3 : De l'action civile et de l'action publique.](section-3)
- [Section 4 : Des juridictions d'instruction](section-4)
- [Section 5 : De la défense](section-5)
