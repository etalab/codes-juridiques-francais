# Sous-section 1 : De l'arrestation, de la garde à vue et de la mise à disposition à l'égard des militaires.

- [Article L212-27](article-l212-27.md)
- [Article L212-28](article-l212-28.md)
- [Article L212-29](article-l212-29.md)
- [Article L212-30](article-l212-30.md)
- [Article L212-31](article-l212-31.md)
- [Article L212-32](article-l212-32.md)
