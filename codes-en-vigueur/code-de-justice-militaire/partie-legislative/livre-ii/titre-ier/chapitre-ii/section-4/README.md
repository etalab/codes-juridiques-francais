# Section 4 : De l'arrestation, de la garde à vue et de la mise à disposition

- [Sous-section 1 : De l'arrestation, de la garde à vue et de la mise à disposition à l'égard des militaires.](sous-section-1)
- [Sous-section 2 : De la garde à vue à l'égard des personnes étrangères aux armées.](sous-section-2)
