# Sous-section 2 : Des officiers de police judiciaire civile.

- [Article L212-5](article-l212-5.md)
- [Article L212-6](article-l212-6.md)
