# Article L212-37

En matière de crime et sous réserve des dispositions de l'article 213-5 du code pénal, l'action publique se prescrit par dix années révolues à compter du jour où le crime a été commis si, dans cet intervalle, il n'a été fait aucun acte d'instruction ou de poursuite.

S'il en a été effectué dans cet intervalle, elle ne se prescrit qu'après dix années révolues à compter du dernier acte. Il en est ainsi même à l'égard des personnes qui ne seraient pas impliquées dans cet acte d'instruction ou de poursuite.

Lorsque la victime est mineure et que le crime a été commis par un ascendant légitime, naturel ou adoptif ou par une personne ayant autorité sur elle, le délai de prescription ne commence à courir qu'à partir de sa majorité.
