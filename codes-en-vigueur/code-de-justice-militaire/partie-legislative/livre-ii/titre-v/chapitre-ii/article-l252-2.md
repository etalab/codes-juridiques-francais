# Article L252-2

Durant le séquestre, il peut être accordé des secours à la femme, aux enfants, aux ascendants du défaillant, s'ils sont dans le besoin.

Il est statué par ordonnance du président du tribunal de grande ou de première instance du domicile du défaillant, après avis du directeur des domaines.
