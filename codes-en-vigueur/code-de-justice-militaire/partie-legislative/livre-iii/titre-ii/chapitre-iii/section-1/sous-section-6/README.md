# Sous-section 6 : Du refus d'un service dû légalement.

- [Article L323-17](article-l323-17.md)
- [Article L323-18](article-l323-18.md)
