# Sous-section 1 : Des voies de fait et outrages à subordonné.

- [Article L323-19](article-l323-19.md)
- [Article L323-20](article-l323-20.md)
- [Article L323-21](article-l323-21.md)
