# Chapitre II : Des infractions contre l'honneur ou le devoir

- [Section 1 : De la capitulation.](section-1)
- [Section 2 : Du complot militaire.](section-2)
- [Section 3 : Des pillages.](section-3)
- [Section 4 : Des destructions.](section-4)
- [Section 5 : Du faux, de la falsification, des détournements.](section-5)
- [Section 6 : De l'usurpation d'uniformes, de décorations, de signes distinctifs et emblèmes.](section-6)
- [Section 7 : De l'outrage au drapeau ou à l'armée.](section-7)
- [Section 8 : De l'incitation à commettre des actes contraires au devoir ou à la discipline.](section-8)
