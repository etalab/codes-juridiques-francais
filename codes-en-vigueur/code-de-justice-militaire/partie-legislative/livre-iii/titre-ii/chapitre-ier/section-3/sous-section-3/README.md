# Sous-section 3 : Dispositions communes.

- [Article L321-20](article-l321-20.md)
- [Article L321-21](article-l321-21.md)
