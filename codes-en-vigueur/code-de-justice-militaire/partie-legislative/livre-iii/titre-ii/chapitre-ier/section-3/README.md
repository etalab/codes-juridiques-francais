# Section 3 : De la provocation à la désertion et du recel de déserteur

- [Sous-section 1 : De la provocation à la désertion.](sous-section-1)
- [Sous-section 2 : Du recel de déserteur.](sous-section-2)
- [Sous-section 3 : Dispositions communes.](sous-section-3)
