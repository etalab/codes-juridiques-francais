# Section 2 : De la désertion

- [Sous-section 1 : De la désertion à l'intérieur.](sous-section-1)
- [Sous-section 2 : De la désertion à l'étranger.](sous-section-2)
- [Sous-section 3 : De la désertion à bande armée.](sous-section-3)
- [Sous-section 4 : De la désertion à l'ennemi ou en présence de l'ennemi.](sous-section-4)
- [Sous-section 5 : Dispositions communes aux diverses désertions.](sous-section-5)
