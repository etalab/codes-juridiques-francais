# Section 1 : Des dépenses

- [Article D269-1](article-d269-1.md)
- [Article D269-2](article-d269-2.md)
- [Article D269-3](article-d269-3.md)
