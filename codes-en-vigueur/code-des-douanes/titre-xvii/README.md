# Titre XVII : Echanges de biens entre Etats membres de la Communauté européenne

- [Chapitre Ier : Dispositions relatives à la déclaration d'échange de biens entre les Etats membres de la Communauté européenne.](chapitre-ier)
- [Chapitre II : Présentation en douane des produits soumis a certaines restrictions de circulation dans les échanges avec les autres Etats membres de la Communauté européenne.](chapitre-ii)
- [Chapitre III : Renvoi des produits dans le pays d'origine.](chapitre-iii)
