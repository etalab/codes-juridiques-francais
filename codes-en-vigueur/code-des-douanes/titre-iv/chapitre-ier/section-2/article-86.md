# Article 86

Les marchandises importées ou exportées doivent être déclarées en détail par leurs détenteurs ou par les personnes ou services ayant obtenu l'agrément de commissionnaire en douane dans les conditions prévues par les articles 87 et suivants du présent code.
