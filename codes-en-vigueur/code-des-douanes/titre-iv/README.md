# Titre IV : Opérations de dédouanement

- [Chapitre Ier : Déclaration en détail](chapitre-ier)
- [Chapitre II : Vérification des marchandises](chapitre-ii)
- [Chapitre III : Liquidation et acquittement des droits et taxes](chapitre-iii)
- [Chapitre IV : Enlèvement des marchandises](chapitre-iv)
- [Chapitre V : Procédures de dédouanement dans les relations entre certains pays et territoires.](chapitre-v)
