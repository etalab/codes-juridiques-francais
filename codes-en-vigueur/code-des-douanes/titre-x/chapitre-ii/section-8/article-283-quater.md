# Article 283 quater

Le produit de la taxe correspondant aux sommes perçues pour l'usage du réseau  routier national est affecté à l'Agence de financement des infrastructures de  transport de France. La taxe forfaitaire due au titre de l'article 282 lui est également affectée.

L'Etat rétrocède aux collectivités  territoriales le produit de la taxe correspondant aux sommes perçues pour  l'usage du réseau routier dont elles sont propriétaires, déduction faite des  coûts exposés y afférents. Un arrêté conjoint des ministres chargés des  transports et du budget fixe le montant de cette retenue qui est affectée à l'Agence de financement des infrastructures de transport de France.
