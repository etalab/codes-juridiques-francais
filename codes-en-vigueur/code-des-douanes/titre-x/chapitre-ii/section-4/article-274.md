# Article 274

L'assiette de la taxe due est constituée par la longueur des sections de  tarification empruntées par le véhicule, exprimée en kilomètres, après  arrondissement à la centaine de mètres la plus proche.
