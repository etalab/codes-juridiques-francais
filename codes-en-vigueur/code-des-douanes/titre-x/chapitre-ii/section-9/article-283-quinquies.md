# Article 283 quinquies

Aux fins d'établissement de l'assiette de la taxe, de son recouvrement et des  contrôles nécessaires, un dispositif de traitement automatisé des données à  caractère personnel sera mis en œuvre, conformément aux modalités prévues par la  loi n° 78-17  du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés.
