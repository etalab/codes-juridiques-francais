# Article 277

1. Un décret en Conseil d'Etat définit les modalités de communication du montant aux sociétés habilitées fournissant un service de télépéage mentionnées au 3 de l'article 276 ainsi que les conditions dans lesquelles le redevable peut avoir accès à l'état récapitulatif des trajets et au détail de la tarification retenue dans les cas visés au 4 du même article.

2. Un décret en Conseil d'Etat fixe les modalités, y compris financières, selon lesquelles les équipements électroniques embarqués mentionnés au 1 de l'article 276 sont mis à disposition des redevables soumis aux dispositions du 4 du même article.

3. Un arrêté conjoint des ministres chargés des transports et du budget fixe les caractéristiques techniques des équipements électroniques embarqués mentionnés au 1 de l'article 276.

4. Un arrêté conjoint des ministres mentionnés au 3 définit les conditions dans lesquelles une société fournissant un service de télépéage peut être habilitée en vue de mettre à disposition des redevables visés au 3 de l'article 276 les équipements électroniques embarqués et d'acquitter la taxe pour leur compte.
