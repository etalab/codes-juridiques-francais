# Chapitre II : Taxe nationale sur les véhicules de transport de marchandises.

- [Section 1 : Champ d'application.](section-1)
- [Section 2 : Redevables.](section-2)
- [Section 3 : Fait générateur et exigibilité de la taxe.](section-3)
- [Section 4 : Assiette, taux et barème.](section-4)
- [Section 5 : Liquidation de la taxe.](section-5)
- [Section 6 : Paiement de la taxe.](section-6)
- [Section 7 : Recherche, constatation, sanction et poursuite.](section-7)
- [Section 8 : Affectation du produit de la taxe.](section-8)
- [Section 9 : Dispositions diverses.](section-9)
