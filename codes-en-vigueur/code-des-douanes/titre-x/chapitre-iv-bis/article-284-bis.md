# Article 284 bis

Les véhicules immatriculés en France circulant sur la voie publique et désignés à l'article 284 ter, à l'exclusion de ceux qui sont spécialement conçus pour le transport des personnes, sont soumis à une taxe spéciale.

Les dispositions de l'alinéa précédent sont applicables aux véhicules immatriculés dans un autre Etat qu'un Etat membre de la Communauté européenne.

Cette taxe est assise sur le poids total autorisé en charge de ces véhicules ou sur leur poids total roulant autorisé lorsqu'il est supérieur. Elle est exigible dès leur mise en circulation.
