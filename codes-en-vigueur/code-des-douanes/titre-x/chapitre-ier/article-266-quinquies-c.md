# Article 266 quinquies C

1. Il est institué une taxe intérieure sur la consommation finale d'électricité relevant du code NC 2716 de la nomenclature douanière, fournie ou consommée sous une puissance souscrite supérieure à 250 kilovoltampères.

2. Le fait générateur de la taxe intervient lors de la livraison de l'électricité par un fournisseur à chaque point de livraison situé en France d'un utilisateur final. La taxe est exigible au moment de la livraison. Toutefois, lorsque la livraison donne lieu à des décomptes ou à des encaissements successifs et que le redevable a exercé l'option prévue au second alinéa du a du 2 de l'article 269 du code général des impôts, l'exigibilité intervient au moment du débit.

L'exigibilité intervient, en tout état de cause, dès la perception d'acomptes financiers lorsqu'il en est demandé avant l'intervention du fait générateur.

Dans le cas mentionné au 2° du 3 du présent article, le fait générateur et l'exigibilité de la taxe interviennent lors de la consommation de l'électricité.

3. Sont redevables de la taxe :

1° Les fournisseurs d'électricité.

Un fournisseur d'électricité s'entend de la personne qui produit ou achète de l'électricité en vue de la revendre à un consommateur final.

Le montant de la taxe dû par les fournisseurs apparaît distinctement, en addition au prix de vente de l'électricité, sur les factures qu'ils émettent ou qui sont émises pour leur compte ;

2° Les personnes qui produisent de l'électricité et l'utilisent pour leurs propres besoins.

4. L'électricité n'est pas soumise à la taxe mentionnée au 1 dans les cas suivants :

1° Lorsqu'elle est principalement utilisée dans des procédés métallurgiques, d'électrolyse ou de réduction chimique. Le bénéfice de la présente mesure ne s'applique pas aux quantités d'électricité utilisées pour des besoins autres que ceux de ces procédés ;

2° Lorsque sa valeur représente plus de la moitié du coût d'un produit ;

3° Lorsqu'elle est utilisée dans des procédés de fabrication de produits minéraux non métalliques classés conformément au règlement (CEE) n° 3037/90 du Conseil, du 9 octobre 1990, relatif à la nomenclature statistique des activités économiques dans la Communauté européenne ;

4° Lorsqu'elle est consommée dans l'enceinte des établissements de production de produits énergétiques, pour les besoins de la production des produits énergétiques eux-mêmes ou pour ceux de la production de tout ou partie de l'énergie nécessaire à leur fabrication.

5. L'électricité est exonérée de la taxe mentionnée au 1 lorsqu'elle est :

1° Utilisée pour la production de l'électricité et pour le maintien de la capacité de production de l'électricité ;

2° Utilisée pour le transport de personnes et de marchandises par train, métro, tramway et trolleybus ;

3° Produite à bord des bateaux ;

4° Produite par de petits producteurs d'électricité qui la consomment pour les besoins de leur activité. Sont considérées comme petits producteurs d'électricité les personnes qui exploitent des installations de production d'électricité dont la production annuelle n'excède pas 240 millions de kilowattheures par site de production ;

5° D'une puissance souscrite supérieure à 250 kilovoltampères et utilisée par des personnes grandes consommatrices d'énergie soumises à autorisation pour l'émission de gaz à effet de serre pour les besoins des installations mentionnées à l'article L. 229-5 du code de l'environnement.

Sont considérées comme grandes consommatrices en énergie les entreprises :

― dont les achats d'électricité de puissance souscrite supérieure à 250 kilovoltampères et de produits énergétiques soumis aux taxes intérieures de consommation visées aux articles 265,266 quinquies et 266 quinquies B du présent code atteignent au moins 3 % du chiffre d'affaires ;

― ou pour lesquelles le montant total de la taxe applicable à l'électricité de puissance souscrite supérieure à 250 kilovoltampères et des taxes intérieures de consommation visées au précédent alinéa est au moins égal à 0,5 % de la valeur ajoutée telle que définie à l'article 1586 sexies du code général des impôts.

6. Sont admis en franchise de la taxe les achats d'électricité effectués par les gestionnaires de réseaux publics de transport et de distribution d'électricité pour les besoins de la compensation des pertes inhérentes aux opérations de transport et de distribution de l'électricité.

7. Les personnes qui ont reçu de l'électricité qu'elles utilisent dans les conditions mentionnées aux 4 à 6 adressent à leurs fournisseurs une attestation, conforme au modèle fixé par arrêté du ministre chargé du budget, justifiant la livraison de cette électricité sans application de la taxe. Elles sont tenues d'acquitter la taxe ou le supplément de taxe due lorsque tout ou partie de l'électricité n'a pas été affectée à l'usage ayant justifié l'absence de taxation, l'exonération ou la franchise.

8. La taxe est assise sur la quantité d'électricité d'une puissance souscrite supérieure à 250 kilovoltampères fournie ou consommée, exprimée en mégawattheures ou fraction de mégawattheure.

Le tarif de la taxe est fixé à 0,50 € par mégawattheure.

Les fournisseurs d'électricité établis en France sont tenus de se faire enregistrer auprès de l'administration des douanes et droits indirects chargée du recouvrement de la taxe intérieure de consommation préalablement au commencement de leur activité.

Ils tiennent une comptabilité des livraisons d'électricité d'une puissance souscrite supérieure à 250 kilovoltampères qu'ils effectuent en France et communiquent à l'administration chargée du recouvrement le lieu de livraison effectif, le nom ou la raison sociale et l'adresse du destinataire.

La comptabilité des livraisons doit être présentée à toute réquisition de l'administration.

Les fournisseurs d'électricité non établis en France désignent une personne qui y est établie et a été enregistrée auprès de l'administration des douanes et droits indirects pour effectuer en leur lieu et place les obligations qui leur incombent et acquitter la taxe intérieure sur la consommation finale d'électricité. A défaut, la taxe est due par le destinataire du produit soumis à accise.

Les fournisseurs communiquent chaque année à l'administration des douanes la liste de leurs clients non domestiques, selon les modalités définies par arrêté du ministre chargé du budget.

9. La taxe est acquittée, selon une périodicité trimestrielle, auprès de l'administration des douanes et des droits indirects.

Les quantités d'électricité de puissance souscrite supérieure à 250 kilovoltampères livrées à un utilisateur final ou consommées par un utilisateur final au titre d'un trimestre, pour lesquelles la taxe est devenue exigible, sont portées sur une déclaration déposée dans un délai de deux mois suivant le trimestre concerné. La taxe correspondante est acquittée lors du dépôt de la déclaration. Toutefois, les petits producteurs mentionnés au 4° du 5 sont dispensés de l'obligation d'établir la déclaration.

La forme de la déclaration d'acquittement et les modalités déclaratives sont définies par arrêté du ministre chargé du budget.

La déclaration d'acquittement peut être effectuée par voie électronique.

10. Lorsque l'électricité a été normalement soumise à la taxe intérieure de consommation alors qu'elle a été employée en tout ou partie par l'utilisateur final à un usage non taxable prévu au 4 ou à un usage exonéré prévu au 5 du présent article, l'utilisateur final peut demander le remboursement de la taxe ou de la fraction de taxe, dans les conditions prévues à l'article 352.

Un décret détermine les modalités d'application de l'assiette de la taxe lorsque les livraisons d'électricité donnent lieu, de la part des fournisseurs, à des décomptes ou à des encaissements successifs ou à la perception d'acomptes financiers. Il détermine également les modalités du contrôle et de la destination de l'électricité et de son affectation aux usages mentionnés aux 4 à 6.
