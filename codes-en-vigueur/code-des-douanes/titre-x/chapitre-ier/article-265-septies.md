# Article 265 septies

Les personnes soumises au droit commercial au titre de leur activité de transport routier de marchandises, propriétaires ou, en leur lieu et place, les personnes titulaires des contrats cités à l'article 284 bis A :

a) De véhicules routiers à moteur destinés au transport de marchandises et dont le poids total autorisé en charge est égal ou supérieur à 7,5 tonnes ;

b) De véhicules tracteurs routiers dont le poids total roulant est égal ou supérieur à 7,5 tonnes,

peuvent obtenir, sur demande de leur part, dans les conditions prévues à l'article 352, le remboursement d'une fraction de la taxe intérieure de consommation sur le gazole, identifié à l'indice 22 et mentionné au tableau B du 1 de l'article 265.

Le carburant doit avoir supporté la taxe intérieure de consommation sur le territoire douanier défini au 1 de l'article 1er, sauf dans les départements d'outre-mer.

Ce remboursement est calculé, au choix du demandeur :

-soit en appliquant au volume de gazole utilisé comme carburant dans des véhicules définis aux a et b, acquis dans chaque région et dans la collectivité territoriale de Corse, la différence entre 43,19 euros par hectolitre et le tarif qui y est applicable en application des articles 265 et 265 A bis ;

-soit en appliquant, au total du volume de gazole utilisé comme carburant dans des véhicules définis aux a et b, acquis dans au moins trois des régions, dont le cas échéant la collectivité territoriale de Corse, un taux moyen de remboursement calculé en pondérant les différents taux régionaux votés dans les conditions précisées au 2 de l'article 265 et à l'article 265 A bis par les volumes de gazole respectivement mis à la consommation dans chaque région et dans la collectivité territoriale de Corse. Le montant de ce taux moyen pondéré est fixé par arrêté.

Le remboursement est également accordé aux personnes établies dans un autre Etat membre de l'Union européenne qui sont en mesure de justifier qu'elles ont acquis du gazole en France au cours de la période couverte par le remboursement et que ce gazole a été utilisé comme carburant dans des véhicules définis aux a et b ci-dessus.

Les modalités d'application du présent article sont fixées par décret.
