# Article 266 quinquies

1. Le gaz naturel repris aux codes NC 2711-11 et 2711-21 de la nomenclature douanière, ainsi que le produit résultant du mélange du gaz naturel repris aux codes NC 2711-11 et 2711-21 et d'autres hydrocarbures gazeux repris au code NC 2711, destinés à être utilisés comme combustibles, sont soumis à une taxe intérieure de consommation.

2. Le fait générateur de la taxe intervient lors de la livraison de ces produits par un fournisseur à un utilisateur final et la taxe est exigible au moment de la facturation, y compris des acomptes, ou au moment des encaissements si ceux-ci interviennent avant le fait générateur ou la facturation. Le fait générateur intervient et la taxe est également exigible au moment de l'importation, lorsque ces produits sont directement importés par l'utilisateur final pour ses besoins propres.

Dans les autres cas, le fait générateur et l'exigibilité de la taxe interviennent lors de la consommation de ces produits effectuée sur le territoire douanier de la France par un utilisateur final.

3. La taxe est due :

a) Par le fournisseur de gaz naturel.

Est considérée comme fournisseur de gaz naturel toute personne titulaire de l'autorisation prévue au I de l'article 5 de la loi n° 2003-8 du 3 janvier 2003 relative aux marchés du gaz et de l'électricité et au service public de l'énergie ;

b) A l'importation, par la personne désignée comme destinataire réel des produits sur la déclaration en douane d'importation ;

c) Par l'utilisateur final mentionné au dernier alinéa du 2.

4. a. Les produits mentionnés au 1 ne sont pas soumis à la taxe intérieure de consommation prévue au 1 lorsqu'ils sont utilisés :

1° Autrement que comme combustible, sous réserve des dispositions de l'article 265 ;

2° A un double usage au sens du 2° du I de l'article 265 C ;

3° Dans un procédé de fabrication de produits minéraux non métalliques mentionné au 3° du I de l'article 265 C.

b. Les produits mentionnés au 1 ne sont pas soumis à la taxe intérieure de consommation prévue au 1 lorsqu'ils sont consommés dans les conditions prévues au III de l'article 265 C.

5. Les produits mentionnés au 1 sont exonérés de la taxe intérieure de consommation prévue au 1 lorsqu'ils sont utilisés :

a) Pour la production d'électricité, à l'exclusion des produits mentionnés au 1 utilisés pour leurs besoins par les petits producteurs d'électricité au sens du 4° du V de l'article L. 3333-2 du code général des collectivités territoriales.

Cette exonération ne s'applique pas aux produits mentionnés au 1 destinés à être utilisés dans les installations visées à l'article 266 quinquies A et qui bénéficient d'un contrat d'achat d'électricité conclu en application de l'article L. 314-1 du code de l'énergie ou mentionné à l'article L. 121-27 du même code ;

b) Pour les besoins de l'extraction et de la production du gaz naturel ;

c) (Abrogé) ;

d) Pour la consommation des autorités régionales et locales ou des autres organismes de droit public pour les activités ou opérations qu'ils accomplissent en tant qu'autorités publiques jusqu'au 1er janvier 2009.

6. Les modalités d'application des 4 et 5, ainsi que les modalités du contrôle et de la destination des produits et de leur affectation aux usages qui y sont mentionnés sont fixées par décret.

7. Sont également exonérés de la taxe intérieure de consommation mentionnée au 1 les gaz repris au code NC 2705, ainsi que le biogaz repris au code NC 2711-29, lorsqu'il n'est pas mélangé au gaz naturel.

8. La taxe intérieure de consommation mentionnée au 1 est assise sur la quantité d'énergie livrée. Elle est déterminée conformément au tableau ci-dessous :

<div align="center">

<table>
<tbody>
<tr>
<th>
<br/>
</th>
<th>
<br/>
</th>
<th colspan="3">
<br/>TARIF (EN EUROS) <br/>
</th>
</tr>
<tr>
<th>
<br/>DÉSIGNATION DES PRODUITS <br/>
</th>
<th>
<br/>UNITÉ DE PERCEPTION <br/>
</th>
<th>
<br/>2014 <br/>
</th>
<th>
<br/>2015 <br/>
</th>
<th>
<br/>2016 <br/>
</th>
</tr>
<tr>
<td align="center">
<br/>2711-11 et 2711-21 : gaz naturel destiné à être utilisé comme combustible <br/>
</td>
<td align="center">
<br/>Mégawattheure <br/>
</td>
<td align="center">
<br/>1,41 <br/>
</td>
<td align="center">
<br/>2,93 <br/>
</td>
<td align="center">
<br/>4,45 <br/>
</td>
</tr>
</tbody>
</table>

</div>

Le montant du tarif total est arrondi au mégawattheure le plus voisin.

9. a. Les fournisseurs de gaz naturel établis sur le territoire douanier de la France se font enregistrer auprès de l'administration des douanes et droits indirects chargée du recouvrement de la taxe intérieure de consommation préalablement au commencement de leur activité.

Ils tiennent une comptabilité des livraisons de gaz naturel qu'ils effectuent en France et communiquent à l'administration chargée du recouvrement la date et le lieu de livraison effectif, le nom ou la raison sociale et l'adresse du destinataire. La comptabilité des livraisons doit être présentée à toute réquisition de l'administration.

b. Les fournisseurs qui ne sont pas établis sur le territoire douanier de la France désignent une personne qui y est établie et qui a été enregistrée auprès de l'administration des douanes et droits indirects, pour effectuer en leurs lieu et place les obligations qui leur incombent et acquitter la taxe intérieure de consommation.

c. Les utilisateurs finals mentionnés au second alinéa du 2 et ceux qui importent du gaz naturel pour leurs besoins propres se font enregistrer auprès de l'administration des douanes et droits indirects. Ils lui communiquent tous les éléments d'assiette nécessaires pour l'établissement de la taxe.

d. Les fournisseurs doivent communiquer chaque année à l'administration des douanes la liste de leurs clients non domestiques, selon les modalités définies par arrêté du ministre chargé du budget.

10. La taxe est acquittée, selon une périodicité trimestrielle, auprès du service des douanes désigné lors de l'enregistrement.

Les quantités d'énergie livrées à un utilisateur final ou importées ou, dans les autres cas, consommées par un utilisateur final au titre d'un trimestre, pour lesquelles la taxe est devenue exigible, sont portées sur une déclaration déposée dans un délai de deux mois suivant le trimestre concerné. La taxe correspondante est acquittée lors du dépôt de la déclaration.

La forme de la déclaration d'acquittement et les modalités déclaratives sont définies par arrêté du ministre chargé du budget.

La déclaration d'acquittement peut être effectuée par voie électronique.

11. Les personnes qui ont reçu des produits mentionnés au 1, sans que ces produits soient soumis à la taxe intérieure de consommation dans les conditions mentionnées au 4, ou en exonération conformément au 5, sont tenues, sans préjudice des pénalités éventuellement applicables, d'acquitter les taxes ou le supplément de taxes dû, lorsque les produits n'ont pas été affectés à la destination ou à l'utilisation ayant justifié l'absence de taxation, l'exonération, l'octroi d'un régime fiscal privilégié ou d'un taux réduit.

12. Lorsque les produits mentionnés au 1 ont été normalement soumis à la taxe intérieure de consommation alors qu'ils ont été employés en tout ou partie par l'utilisateur final à un usage non taxable prévu au 4 ou à un usage exonéré prévu au 5, l'utilisateur final peut demander le remboursement de la taxe ou de la fraction de taxe, dans les conditions prévues à l'article 352.

Lorsque les produits mentionnés au 1 soumis à la taxe ont fait l'objet d'un rachat par le fournisseur auprès de son client, la taxe est remboursée au fournisseur, pour autant que le fournisseur justifie qu'il a précédemment acquitté la taxe. Ce remboursement peut s'effectuer par imputation sur le montant de la taxe due.
