# Article 265 bis

1. Les produits énergétiques mentionnés à l'article 265 sont admis en exonération des taxes intérieures de consommation lorsqu'ils sont destinés à être utilisés :

a) autrement que comme carburant ou combustible ;

b) Comme carburant ou combustible à bord des aéronefs utilisés par leur propriétaire ou la personne qui en a la disposition à la suite d'une location, d'un affrètement ou à tout autre titre à des fins commerciales, notamment pour les besoins d'opérations de transport de personnes, de transport de marchandises ainsi que pour la réalisation de prestations de services à titre onéreux. L'exonération s'applique également aux aéronefs utilisés pour les besoins des autorités publiques ;

c) Comme carburant ou combustible à bord des navires de pêche, des navires utilisés par leur propriétaire ou la personne qui en a la disposition à la suite d'une location, d'un affrètement ou à tout autre titre à des fins commerciales, notamment pour les besoins d'opérations de transport de personnes, de transport de marchandises ainsi que pour la réalisation de prestations de services à titre onéreux. L'exonération s'applique également aux navires utilisés pour les besoins des autorités publiques ;

d) Comme carburant ou combustible par le ministère de la défense. Cette exonération est accordée par voie de remboursement pour les produits consommés du 1er janvier 2006 au 1er janvier 2009. Toutefois, cette exonération ne s'applique pas aux produits pétroliers utilisés dans le cadre des actions n<sup>os</sup> 01, 02, 03 et 04 du programme n° 152 " Gendarmerie nationale " de la mission interministérielle " Sécurité " ;

e) Comme carburant ou combustible pour le transport de marchandises sur les voies navigables intérieures.

2. Les carburants destinés aux moteurs d'avions sont exonérés de la taxe intérieure de consommation lorsqu'ils sont utilisés dans le cadre de la construction, du développement, de la mise au point, des essais ou de l'entretien des aéronefs et de leurs moteurs.

3. Les produits mentionnés au 1 sont également exonérés lorsqu'ils sont utilisés :

a) Pour la production d'électricité, à l'exclusion des produits utilisés dans des installations mentionnées à l'article 266 quinquies A et des produits utilisés pour leurs besoins par les petits producteurs d'électricité au sens du 4° du V de l'article L. 3333-2 du code général des collectivités territoriales ;

b) Pour les besoins de l'extraction et de la production de gaz naturel.

Les modalités d'application des exonérations visées ci-dessus sont fixées par arrêté du ministre chargé du budget.
