# Article 265

1. Les produits énergétiques repris aux tableaux B et C ci-après, mis en vente, utilisés ou destinés à être utilisés comme carburant ou combustible sont passibles d'une taxe intérieure de consommation dont les tarifs sont fixés comme suit :

Tableau A (abrogé par l'article 43 de la loi de finances rectificative n° 92-1476 du 31 décembre 1992).

Tableau B : Produits pétroliers et assimilés.

1° Nomenclature et tarif.

<table>
<tbody>
<tr>
<th>DÉSIGNATION DES PRODUITS <p>(numéros du tarif des douanes) <br/>
</p>
</th>
<th>
<br/>INDICE <p>d'identification <br/>
</p>
</th>
<th>
<br/>UNITÉ <p>de perception <br/>
</p>
</th>
<th colspan="3">
<br/>TARIF <p>(en euros) <br/>
</p>
</th>
</tr>
<tr>
<th>
<br/>
</th>
<th>
<br/>
</th>
<th>
<br/>
</th>
<th>
<br/>2014 <br/>
</th>
<th>
<br/>2015 <br/>
</th>
<th>
<br/>2016 <br/>
</th>
</tr>
<tr>
<td align="center">
<br/>Ex 2706-00 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Goudrons de houille, de lignite ou de tourbe et autres goudrons minéraux, même déshydratés ou étêtés, y compris les goudrons reconstitués, utilisés comme combustibles. <br/>
</td>
<td align="center">
<br/>1 <br/>
</td>
<td align="center">
<br/>100 kg nets <br/>
</td>
<td align="center">
<br/>1,58 <br/>
</td>
<td align="center">
<br/>3,28 <br/>
</td>
<td align="center">
<br/>4,97 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Ex 2707-50 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Mélanges à forte teneur en hydrocarbures aromatiques distillant 65 % ou plus de leur volume (y compris les pertes) à 250° C d'après la méthode ASTM D 86, destinés à être utilisés comme carburants ou combustibles. <br/>
</td>
<td align="center">
<br/>2 <br/>
</td>
<td align="center">
<br/>Hectolitre ou 100 kg nets suivant les caractéristiques du produit <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2709-00 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Huiles brutes de pétrole ou de minéraux bitumineux. <br/>
</td>
<td align="center">
<br/>3 <br/>
</td>
<td align="center">
<br/>Hectolitre ou 100 kg nets suivant les caractéristiques du produit <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable aux huiles légères du 2710, suivant les caractéristiques du produit <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable aux huiles légères du 2710, suivant les caractéristiques du produit <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable aux huiles légères du 2710, suivant les caractéristiques du produit <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2710 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Huiles de pétrole ou de minéraux bitumineux, autres que les huiles brutes ; préparations non dénommées ni comprises ailleurs, contenant en poids 70 % ou plus d'huiles de pétrole ou de minéraux bitumineux et dont ces huiles constituent l'élément de base, autres que les déchets : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>--huiles légères et préparations : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>---essences spéciales : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>----white spirit destiné à être utilisé comme combustible ; <br/>
</td>
<td align="center">
<br/>4 bis <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>5,66 <br/>
</td>
<td align="center">
<br/>7,87 <br/>
</td>
<td align="center">
<br/>10,08<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>----autres essences spéciales : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>-----destinées à être utilisées comme carburants ou combustibles ; <br/>
</td>
<td align="center">
<br/>6 <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>58,92 <br/>
</td>
<td align="center">
<br/>60,64 <br/>
</td>
<td align="center">
<br/>62,35<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>-----autres ; <br/>
</td>
<td align="center">
<br/>9 <br/>
</td>
<td align="center"/>
<td align="center">
<br/>Exemption <br/>
</td>
<td align="center">
<br/>Exemption <br/>
</td>
<td align="center">
<br/>Exemption<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>---autres huiles légères et préparations : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>----essences pour moteur : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>-----essence d'aviation ; <br/>
</td>
<td align="center">
<br/>10 <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>35,90 <br/>
</td>
<td align="center">
<br/>37,81 <br/>
</td>
<td align="center">
<br/>39,72<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>-----supercarburant d'une teneur en plomb n'excédant pas 0,005 g/ litre, autre que le supercarburant correspondant à l'indice d'identification n° 11 bis ; <br/>
</td>
<td align="center">
<br/>11 <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>60,69 <br/>
</td>
<td align="center">
<br/>62,41 <br/>
</td>
<td align="center">
<br/>64,12<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>-----supercarburant d'une teneur en plomb n'excédant pas 0,005 g/ litre, contenant un additif spécifique améliorant les caractéristiques antirécession de soupape, à base de potassium, ou tout autre additif reconnu de qualité équivalente dans un autre Etat membre de l'Union européenne ou dans un autre Etat partie à l'accord sur l'Espace économique européen. <br/>
</td>
<td align="center">
<br/>11 bis <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>63,96 <br/>
</td>
<td align="center">
<br/>65,68 <br/>
</td>
<td align="center">
<br/>67,39<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>-----supercarburant d'une teneur en plomb n'excédant pas 0,005 g/ litre, autre que les supercarburants correspondant aux indices d'identification 11 et 11 bis, et contenant jusqu'à 10 % volume/ volume d'éthanol, 22 % volume/ volume d'éthers contenant 5 atomes de carbone, ou plus, par molécule et d'une teneur en oxygène maximale de 4 % en masse/ masse d'oxygène. <p>Ce supercarburant est dénommé E10 ; <br/>
</p>
</td>
<td align="center">
<br/>11 ter <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>60,69 <br/>
</td>
<td align="center">
<br/>62,41 <br/>
</td>
<td align="center">
<br/>64,12<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>----carburéacteurs, type essence : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>-----carburant utilisé pour les moteurs d'avions ; <br/>
</td>
<td align="center">
<br/>13 bis <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>30,20 <br/>
</td>
<td align="center">
<br/>32,11 <br/>
</td>
<td align="center">
<br/>34,02<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>-----autres ; <br/>
</td>
<td align="center">
<br/>13 ter <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>58,92 <br/>
</td>
<td align="center">
<br/>60,83 <br/>
</td>
<td align="center">
<br/>62,74<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>----autres huiles légères ; <br/>
</td>
<td align="center">
<br/>15 <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>58,92 <br/>
</td>
<td align="center">
<br/>60,64 <br/>
</td>
<td align="center">
<br/>62,35<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>--huiles moyennes : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>---pétrole lampant : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>----destiné à être utilisé comme combustible : <br/>
</td>
<td align="center">
<br/>15 bis <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>5,66 <br/>
</td>
<td align="center">
<br/>7,57 <br/>
</td>
<td align="center">
<br/>9,48<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>-----autres ; <br/>
</td>
<td align="center">
<br/>16 <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>41,69 <br/>
</td>
<td align="center">
<br/>43,60 <br/>
</td>
<td align="center">
<br/>45,51<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>---carburéacteurs, type pétrole lampant : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>----carburant utilisé pour les moteurs d'avions ; <br/>
</td>
<td align="center">
<br/>17 bis <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>30,20 <br/>
</td>
<td align="center">
<br/>32,11 <br/>
</td>
<td align="center">
<br/>34,02<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>---autres ; <br/>
</td>
<td align="center">
<br/>17 ter <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>41,69 <br/>
</td>
<td align="center">
<br/>43,60 <br/>
</td>
<td align="center">
<br/>45,51<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>---autres huiles moyennes ; <br/>
</td>
<td align="center">
<br/>18 <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>41,69 <br/>
</td>
<td align="center">
<br/>43,60 <br/>
</td>
<td align="center">
<br/>45,51<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>--huiles lourdes : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>---gazole : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>----destiné à être utilisé comme carburant sous condition d'emploi ; <br/>
</td>
<td align="center">
<br/>20 <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>8,86 <br/>
</td>
<td align="center">
<br/>10,84 <br/>
</td>
<td align="center">
<br/>12,83<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>----fioul domestique ; <br/>
</td>
<td align="center">
<br/>21 <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>5,66 <br/>
</td>
<td align="center">
<br/>7,64 <br/>
</td>
<td align="center">
<br/>9,63<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>----autres ; <br/>
</td>
<td align="center">
<br/>22 <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>42,84 <br/>
</td>
<td align="center">
<br/>46,82 <br/>
</td>
<td align="center">
<br/>48,81<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>----fioul lourd ; <br/>
</td>
<td align="center">
<br/>24 <br/>
</td>
<td align="center">
<br/>100 kg nets <br/>
</td>
<td align="center">
<br/>2,19 <br/>
</td>
<td align="center">
<br/>4,53 <br/>
</td>
<td align="center">
<br/>6,88<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>---huiles lubrifiantes et autres. <br/>
</td>
<td align="center">
<br/>29 <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2711-12 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Propane, à l'exclusion du propane d'une pureté égale ou supérieure à 99 % : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>--destiné à être utilisé comme carburant (y compris le mélange spécial de butane et de propane dans lequel le propane représente plus de 50 % en poids) : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>---sous condition d'emploi ; <br/>
</td>
<td align="center">
<br/>30 bis <br/>
</td>
<td align="center">
<br/>100 kg nets <br/>
</td>
<td align="center">
<br/>4,68 <br/>
</td>
<td align="center">
<br/>6,92 <br/>
</td>
<td align="center">
<br/>9,16<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>--autres ; <br/>
</td>
<td align="center">
<br/>30 ter <br/>
</td>
<td align="center">
<br/>100 kg nets <br/>
</td>
<td align="center">
<br/>10,76 <br/>
</td>
<td align="center">
<br/>13,00 <br/>
</td>
<td align="center">
<br/>15,24<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>--destiné à d'autres usages. <br/>
</td>
<td align="center">
<br/>31 <br/>
</td>
<td align="center"/>
<td align="center">
<br/>Exemption <br/>
</td>
<td align="center">
<br/>Exemption <br/>
</td>
<td align="center">
<br/>Exemption <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2711-13 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Butanes liquéfiés : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>--destinés à être utilisés comme carburant (y compris le mélange spécial de butane et de propane dans lequel le butane représente au moins 50 % en poids) : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>---sous condition d'emploi ; <br/>
</td>
<td align="center">
<br/>31 bis <br/>
</td>
<td align="center">
<br/>100 kg nets <br/>
</td>
<td align="center">
<br/>4,68 <br/>
</td>
<td align="center">
<br/>6,92 <br/>
</td>
<td align="center">
<br/>9,16<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>---autres ; <br/>
</td>
<td align="center">
<br/>31 ter <br/>
</td>
<td align="center">
<br/>100 kg nets <br/>
</td>
<td align="center">
<br/>10,76 <br/>
</td>
<td align="center">
<br/>13,00 <br/>
</td>
<td align="center">
<br/>15,24<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>--destinés à d'autres usages. <br/>
</td>
<td align="center">
<br/>32 <br/>
</td>
<td align="center"/>
<td align="center">
<br/>Exemption <br/>
</td>
<td align="center">
<br/>Exemption <br/>
</td>
<td align="center">
<br/>Exemption <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2711-14 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Ethylène, propylène, butylène et butadiène. <br/>
</td>
<td align="center">
<br/>33 <br/>
</td>
<td align="center">
<br/>100 kg nets <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2711-19 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Autres gaz de pétrole liquéfiés : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>--destinés à être utilisés comme carburant : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>---sous condition d'emploi ; <br/>
</td>
<td align="center">
<br/>33 bis <br/>
</td>
<td align="center">
<br/>100 kg nets <br/>
</td>
<td align="center">
<br/>4,68 <br/>
</td>
<td align="center">
<br/>6,92 <br/>
</td>
<td align="center">
<br/>9,16<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>---autres. <br/>
</td>
<td align="center">
<br/>34 <br/>
</td>
<td align="center">
<br/>100 kg nets <br/>
</td>
<td align="center">
<br/>10,76 <br/>
</td>
<td align="center">
<br/>13,00 <br/>
</td>
<td align="center">
<br/>15,24 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2711-21 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Gaz naturel à l'état gazeux : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>--destiné à être utilisé comme carburant ; <br/>
</td>
<td align="center">
<br/>36 <br/>
</td>
<td align="center">
<br/>100 m ³ <br/>
</td>
<td align="center">
<br/>1,49 <br/>
</td>
<td align="center">
<br/>3,09 <br/>
</td>
<td align="center">
<br/>4,69<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>--destiné, sous condition d'emploi, à alimenter des moteurs stationnaires, y compris dans le cadre d'essais. <br/>
</td>
<td align="center">
<br/>36 bis <br/>
</td>
<td align="center">
<br/>100 m ³ <br/>
</td>
<td align="center">
<br/>1,49 <br/>
</td>
<td align="center">
<br/>3,09 <br/>
</td>
<td align="center">
<br/>4,69 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2711-29 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Autres gaz de pétrole et autres hydrocarbures présentés à l'état gazeux : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>--destinés à être utilisés comme carburant ; <br/>
</td>
<td align="center">
<br/>38 bis <br/>
</td>
<td align="center">
<br/>100 m ³ <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable aux produits mentionnés aux indices 36 et 36 bis, selon qu'ils sont ou non utilisés sous condition d'emploi <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable aux produits mentionnés aux indices 36 et 36 bis, selon qu'ils sont ou non utilisés sous condition d'emploi <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable aux produits mentionnés aux indices 36 et 36 bis, selon qu'ils sont ou non utilisés sous condition d'emploi<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>--destinés à d'autres usages, autres que le biogaz et le biométhane visés au code NC 2711-29.<br/>
</td>
<td align="center">
<br/>39 <br/>
</td>
<td align="center"/>
<td align="center">
<br/>Exemption <br/>
</td>
<td align="center">
<br/>Exemption <br/>
</td>
<td align="center">
<br/>Exemption <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2712-10 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Vaseline. <br/>
</td>
<td align="center">
<br/>40 <br/>
</td>
<td align="center"/>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2712-20 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Paraffine contenant en poids moins de 0,75 % d'huile. <br/>
</td>
<td align="center">
<br/>41 <br/>
</td>
<td align="center"/>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Ex 2712-90 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Paraffine (autre que celle mentionnée au 2712-20), cires de pétrole et résidus paraffineux, même colorés. <br/>
</td>
<td align="center">
<br/>42 <br/>
</td>
<td align="center"/>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2713-20 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Bitumes de pétrole. <br/>
</td>
<td align="center">
<br/>46 <br/>
</td>
<td align="center"/>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>2713-90 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Autres résidus des huiles de pétrole ou de minéraux bitumineux. <br/>
</td>
<td align="center">
<br/>46 bis <br/>
</td>
<td align="center"/>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Autres. <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>2715-00 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Mélanges bitumeux à base d'asphalte ou de bitume naturel, de bitume de pétrole, de goudrons minéraux ou de brai de goudron minéral. <br/>
</td>
<td align="center">
<br/>47 <br/>
</td>
<td align="center"/>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3403-11 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Préparations pour le traitement des matières textiles, du cuir, des pelleteries ou d'autres matières, contenant moins de 70 % en poids d'huiles de pétrole ou de minéraux bitumeux. <br/>
</td>
<td align="center">
<br/>48 <br/>
</td>
<td align="center"/>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Ex 3403-19 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Préparations lubrifiantes contenant moins de 70 % en poids d'huiles de pétrole ou de minéraux bitumeux. <br/>
</td>
<td align="center">
<br/>49 <br/>
</td>
<td align="center"/>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>3811-21 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Additifs pour huiles lubrifiantes contenant des huiles de pétrole ou de minéraux bitumeux. <br/>
</td>
<td align="center">
<br/>51 <br/>
</td>
<td align="center"/>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
<td align="center">
<br/>Taxe intérieure de consommation applicable conformément au 3 du présent article <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Ex 3824-90-97 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Emulsion d'eau dans du gazole stabilisée par des agents tensio-actifs, dont la teneur en eau est égale ou supérieure à 7 % en volume sans dépasser 20 % en volume, destinée à être utilisée comme carburant : <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>--sous condition d'emploi ; <br/>
</td>
<td align="center">
<br/>52 <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>2,1 <br/>
</td>
<td align="center">
<br/>3,74 <br/>
</td>
<td align="center">
<br/>5,39 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Autres. <br/>
</td>
<td align="center">
<br/>53 <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>28,71 <br/>
</td>
<td align="center">
<br/>30,35 <br/>
</td>
<td align="center">
<br/>32 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Ex 3824-90-97 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Superéthanol E 85 destiné à être utilisé comme carburant. <br/>
</td>
<td align="center">
<br/>55 <br/>
</td>
<td align="center">
<br/>Hectolitre <br/>
</td>
<td align="center">
<br/>12,40 <br/>
</td>
<td align="center">
<br/>12,62 <br/>
</td>
<td align="center">
<br/>7,96 <br/>
</td>
</tr>
</tbody>
</table>

2° Règles d'application.

a) et b) (alinéas abrogés).

c) Pour les produits taxables à la masse, la taxe est assise sur la masse commerciale (masse dans l'air) exprimée en kilogrammes. Pour les produits liquides taxables au volume, la taxe est assise sur le volume mesuré à l'état liquide, à la température de 15° C et exprimé en litres.

Pour le méthane, le gaz naturel et les autres hydrocarbures présentés à l'état gazeux destinés à être utilisés comme carburants, la taxe est assise sur le volume mesuré à l'état gazeux sous la pression de 760 millimètres de mercure, à la température de 0° C et exprimé en centaines de mètres cubes avec deux décimales.

d) (alinéa abrogé).

Tableau C : Autres produits énergétiques.

1° Définition (division abrogée).

2° Tarif et règles d'application.

Les produits visés au présent tableau sont exemptés de la taxe intérieure de consommation, sauf lorsqu'ils sont destinés à être utilisés comme carburant ou combustible.

3° Nomenclature.

<table>
<tbody>
<tr>
<td>
<p align="center">NUMÉROS DU TARIF DES DOUANES </p>
</td>
<td>
<p align="center">DÉSIGNATION DES PRODUITS </p>
</td>
</tr>
<tr>
<td align="center">
<p align="center">1507 à 1518</p>
</td>
<td align="center">
<p align="left">Huiles végétales, graisses et huiles animales, fractions d'huiles végétales et animales.</p>
</td>
</tr>
<tr>
<td align="center">
<p align="center">2705-00</p>
</td>
<td align="center">
<p align="left">Gaz de houille, gaz à l'eau, gaz pauvre et gaz similaires, à l'exclusion des gaz de pétrole et autres hydrocarbures gazeux.</p>
</td>
</tr>
<tr>
<td align="center">
<p align="center">2707</p>
</td>
<td align="center">
<p align="left">Huiles et autres produits provenant de la distillation des goudrons de houille de haute température ; produits analogues dans lesquels les constituants aromatiques prédominent en poids par rapport aux constituants non aromatiques.</p>
</td>
</tr>
<tr>
<td align="center">
<p align="center">Ex 2710</p>
</td>
<td align="center">
<p align="left">Déchets d'huile.</p>
</td>
</tr>
<tr>
<td align="center">
<p align="center">2708</p>
</td>
<td align="center">
<p align="left">Brai et coke de brai de goudron de houille ou d'autres goudrons minéraux.</p>
</td>
</tr>
<tr>
<td align="center">
<p align="center">Ex 2711-12</p>
</td>
<td align="center">
<p align="left">Propane liquéfié d'une pureté égale ou supérieure à 99 %.</p>
</td>
</tr>
<tr>
<td align="center">
<p align="center">Ex 2712</p>
</td>
<td align="center">
<p align="left">Slack wax, ozokérite, cire de lignite, cire de tourbe, autres cires minérales et produits similaires obtenus par synthèse ou par d'autres procédés, même colorés.</p>
</td>
</tr>
<tr>
<td align="center">
<p align="center">Ex 2713</p>
</td>
<td align="center">
<p align="left">Coke de pétrole.</p>
</td>
</tr>
<tr>
<td align="center">
<p align="center">2714</p>
</td>
<td align="center">
<p align="left">Bitumes et asphaltes, naturels, schistes et sables bitumineux ; asphaltites et roches asphaltiques.</p>
</td>
</tr>
<tr>
<td align="center">
<p align="center">2901</p>
</td>
<td align="center">
<p align="left">Hydrocarbures acycliques.</p>
</td>
</tr>
<tr>
<td align="center">
<p align="center">2902</p>
</td>
<td align="center">
<p align="left">Hydrocarbures cycliques.</p>
</td>
</tr>
<tr>
<td align="center">
<p align="center">2905 11</p>
</td>
<td align="center">
<p align="left">Méthanol (alcool méthylique) qui n'est pas d'origine synthétique.</p>
</td>
</tr>
<tr>
<td align="center">
<p align="center">3403</p>
</td>
<td align="center">
<p align="left">Préparations lubrifiantes et préparations des types utilisés pour l'ensimage des matières textiles, l'huilage ou le graissage du cuir, des pelleteries ou d'autres matières, à l'exclusion de celles contenant comme constituants de base moins de 70 % en poids d'huiles de pétrole ou de minéraux bitumeux.</p>
</td>
</tr>
<tr>
<td align="center">
<p align="center">3811</p>
</td>
<td align="center">
<p align="left">Préparations antidétonantes, inhibiteurs d'oxydation, additifs peptisants, améliorants de viscosité, additifs anticorrosifs et autres additifs préparés pour huiles minérales (y compris l'essence) ou autres liquides utilisés aux même fins que les huiles minérales.</p>
</td>
</tr>
<tr>
<td align="center">
<p align="center">3817</p>
</td>
<td align="center">
<p align="left">Alkylbenzènes, en mélanges et alkylnaphtalène en mélange, autres que ceux des positions 2707 ou 2902.</p>
</td>
</tr>
<tr>
<td align="center">
<p align="center">3824-90-98</p>
</td>
<td align="center">
<p align="left">Tous produits de la position.</p>
</td>
</tr>
</tbody>
</table>

2. Une réfaction peut être effectuée sur les taux de taxe intérieure de consommation applicable au supercarburant repris aux indices d'identification 11 et 11 ter et au gazole repris à l'indice d'identification 22.

A compter du 1er janvier 2006, le montant de cette réfaction est de 1,77 euro par hectolitre pour le supercarburant et de 1,15 euro par hectolitre pour le gazole.

A compter du 1er janvier 2007, les conseils régionaux et l'assemblée de Corse peuvent réduire ou augmenter le montant de la réfaction du taux de la taxe intérieure de consommation applicable aux carburants vendus aux consommateurs finals sur leur territoire dans la double limite de la fraction de tarif affectée à chaque région et à la collectivité territoriale de Corse en vertu du I de l'article 40 de la loi n° 2005-1719 du 30 décembre 2005 de finances pour 2006 relatif à la compensation financière des transferts de compétences aux régions et de respectivement 1,77 euro par hectolitre pour le supercarburant mentionné aux indices d'identification 11 et 11 ter et 1, 15 euro par hectolitre pour le gazole mentionné à l'indice d'identification 22.

Les délibérations des conseils régionaux et de l'assemblée de Corse ne peuvent intervenir qu'une fois par an et au plus tard le 30 novembre de l'année qui précède l'entrée en vigueur du tarif modifié. Elles sont notifiées à la direction générale des douanes et droits indirects, qui procède à la publication des tarifs de la taxe intérieure de consommation ainsi modifiés au plus tard à la fin de la première quinzaine du mois de décembre suivant. Les tarifs modifiés de la taxe intérieure de consommation entrent en vigueur le 1er janvier de l'année suivante.

3. Tout produit autre que ceux pour lesquels un tarif de taxe intérieure de consommation est prévu au tableau B du 1, destiné à être utilisé, mis en vente ou utilisé comme carburant pour moteur ou comme additif en vue d'accroître le volume final des carburants pour moteur est assujetti à la taxe intérieure de consommation au taux applicable au carburant équivalent ou au carburant dans lequel il est incorporé.

A l'exclusion de la tourbe reprise au code NC 2703 de la nomenclature douanière, tout hydrocarbure autre que ceux pour lesquels un tarif de taxe intérieure de consommation est prévu par le présent code ou tout produit mentionné au tableau C du 1, mis en vente, utilisé ou destiné à être utilisé comme combustible, est soumis à la taxe intérieure de consommation au taux applicable pour le combustible équivalent, prévue au présent article et aux articles 266 quinquies et 266 quinquies B.

4. A compter du 1er janvier 2007, les personnes physiques ou morales qui vendent, en régime de droits acquittés, des carburants visés aux indices d'identification 11, 11 ter et 22 dans des régions ou collectivités territoriales où le taux de la taxe intérieure de consommation diffère du taux appliqué lors de la mise à la consommation :

a) Acquittent le montant différentiel de taxe si le taux supporté lors de la mise à la consommation est inférieur ;

b) Peuvent demander le remboursement du différentiel de taxe dans le cas contraire.

Pour le paiement du montant différentiel de taxe et des pénalités afférentes, l'administration des douanes et droits indirects peut demander une caution. Les obligations déclaratives des opérateurs concernés sont fixées par arrêté du ministre chargé du budget.
