# Section 2 : Prohibitions relatives à la protection des marques et des indications d'origine.

- [Article 39](article-39.md)
- [Article 40](article-40.md)
