# Chapitre Ier bis : Magasins et aires de dédouanement.

- [Article 82 bis](article-82-bis.md)
- [Article 82 ter](article-82-ter.md)
- [Article 82 quater](article-82-quater.md)
- [Article 82 quinquies](article-82-quinquies.md)
- [Article 82 sexies](article-82-sexies.md)
