# Chapitre Ier : Régime administratif des navires

- [Section 1 : Champ d'application.](section-1)
- [Section 2 : Francisation des navires](section-2)
- [Section 4 : Dispositions diverses relatives à la francisation et aux congés.](section-4)
- [Section 5 : Passeports.](section-5)
- [Section 7 : Hypothèques maritimes](section-7)
