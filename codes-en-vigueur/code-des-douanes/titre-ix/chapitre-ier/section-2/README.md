# Section 2 : Francisation des navires

- [Paragraphe 1 : Généralités.](paragraphe-1)
- [Paragraphe 2 : Conditions requises pour obtenir la francisation.](paragraphe-2)
- [Paragraphe 4 : Droit de francisation et de navigation.](paragraphe-4)
- [Paragraphe 5 : Acte de francisation.](paragraphe-5)
- [Paragraphe 6 : Réparations de navires français hors du territoire douanier.](paragraphe-6)
- [Paragraphe 7 : Ventes de navires francisés.](paragraphe-7)
