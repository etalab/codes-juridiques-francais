# Titre II : Organisation et fonctionnement du service des douanes

- [Chapitre Ier : Champ d'action du service des douanes.](chapitre-ier)
- [Chapitre II : Organisation des bureaux et des brigades de douane](chapitre-ii)
- [Chapitre III : Immunités, sauvegarde et obligations des agents des douanes.](chapitre-iii)
- [Chapitre IV : Pouvoirs des agents des douanes](chapitre-iv)
- [Chapitre IV bis : Consultation des traitements automatisés de données aux fins de contrôles douaniers](chapitre-iv-bis)
- [Chapitre V : Procédure préalable à la prise de décision :  le droit d'être entendu.](chapitre-v)
- [Chapitre VI : Sécurisation des contrôles et enquêtes](chapitre-vi)
