# Article 67 B

Lorsque la décision envisagée porte sur la notification d'une dette douanière à la suite d'un contrôle douanier, la communication des motifs mentionnée à l'article 67 A peut être faite oralement par tout agent des douanes. La personne concernée est invitée à faire connaître immédiatement ses observations, de la même manière. Elle est informée qu'elle peut demander à bénéficier d'une communication écrite dans les conditions prévues au même article 67 A.

La date, l'heure et le contenu de la communication orale mentionnée à l'alinéa précédent sont consignés par l'administration des douanes. Cet enregistrement atteste, sauf preuve contraire, que la personne concernée a exercé son droit de faire connaître ses observations.
