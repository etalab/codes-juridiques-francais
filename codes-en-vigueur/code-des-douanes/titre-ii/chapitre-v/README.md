# Chapitre V : Procédure préalable à la prise de décision :  le droit d'être entendu.

- [Article 67 C](article-67-c.md)
- [Article 67 D](article-67-d.md)
- [Article 67 A](article-67-a.md)
- [Article 67 B](article-67-b.md)
