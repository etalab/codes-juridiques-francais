# Article R213-5-2

Le président du tribunal de grande instance connaît des actions et requêtes dans les cas et conditions prévus au code des douanes.
