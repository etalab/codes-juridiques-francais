# Article R214-1

La commission d'indemnisation des victimes d'infractions est composée de deux magistrats du siège du tribunal de grande instance et d'une personne remplissant les conditions fixées par l'article L. 214-2.
