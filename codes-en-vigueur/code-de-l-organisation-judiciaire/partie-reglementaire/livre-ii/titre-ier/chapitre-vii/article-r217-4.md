# Article R217-4

Le procureur de la République financier préside l'assemblée des magistrats du parquet financier. Celle-ci peut entendre le président du tribunal de grande instance à l'initiative de son président, à la demande de la majorité de ses membres ou à celle du président lui-même.
