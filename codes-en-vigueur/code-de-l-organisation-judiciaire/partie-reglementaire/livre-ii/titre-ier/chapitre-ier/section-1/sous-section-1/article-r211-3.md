# Article R211-3

Dans les matières pour lesquelles compétence n'est pas attribuée expressément à une autre juridiction en raison de la nature de l'affaire ou du montant de la demande, le tribunal de grande instance statue à charge d'appel.

Lorsqu'il est appelé à connaître, en matière civile, d'une action personnelle ou mobilière portant sur une demande dont le montant est inférieur ou égal à la somme de 4 000 euros, le tribunal de grande instance statue en dernier ressort.

Dans les matières pour lesquelles il a compétence exclusive, et sauf disposition contraire, le tribunal de grande instance statue en dernier ressort lorsque le montant de la demande est inférieur ou égal à la somme de 4 000 euros.
