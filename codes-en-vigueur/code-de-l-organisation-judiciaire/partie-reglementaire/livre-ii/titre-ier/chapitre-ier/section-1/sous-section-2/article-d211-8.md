# Article D211-8

Le tribunal de grande instance compétent pour connaître des actions en identification du demandeur de visa par ses empreintes génétiques, dans les cas et conditions prévus par le code de l'entrée et du séjour des étrangers et du droit d'asile, est celui de Nantes.
