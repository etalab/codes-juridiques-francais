# Section 6 : Administration et inspection des juridictions du ressort du tribunal de grande instance

- [Article R212-58](article-r212-58.md)
- [Article R212-59](article-r212-59.md)
- [Article R212-60](article-r212-60.md)
- [Article R212-61](article-r212-61.md)
