# Sous-section 2 : L'assemblée des magistrats du siège

- [Article R212-34](article-r212-34.md)
- [Article R212-35](article-r212-35.md)
- [Article R212-36](article-r212-36.md)
- [Article R212-37](article-r212-37.md)
- [Article R212-37-1](article-r212-37-1.md)
