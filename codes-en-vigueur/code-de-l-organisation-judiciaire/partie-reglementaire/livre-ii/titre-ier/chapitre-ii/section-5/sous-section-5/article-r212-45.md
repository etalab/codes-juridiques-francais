# Article R212-45

Le directeur de greffe préside l'assemblée des fonctionnaires du greffe.

Le secrétaire en chef du parquet préside l'assemblée des fonctionnaires du secrétariat de parquet autonome.

Chacune de ces assemblées comprend :

1° Les greffiers en chef ;

2° Les greffiers ;

3° Les fonctionnaires et les agents de l'Etat relevant de la direction des services judiciaires.

Les fonctionnaires en stage rémunérés à titre permanent, les autres stagiaires ainsi que les fonctionnaires et les agents qui, placés sous l'autorité des magistrats, concourent au fonctionnement de la juridiction mais ne relèvent pas de la direction des services judiciaires, assistent aux réunions de l'assemblée des fonctionnaires.

Le président du tribunal de grande instance et le procureur de la République peuvent assister à l'assemblée des fonctionnaires.
