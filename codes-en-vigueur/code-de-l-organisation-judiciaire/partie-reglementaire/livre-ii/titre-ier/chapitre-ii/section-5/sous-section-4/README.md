# Sous-section 4 : L'assemblée des magistrats du siège et du parquet

- [Article R212-41](article-r212-41.md)
- [Article R212-42](article-r212-42.md)
- [Article R212-43](article-r212-43.md)
- [Article R212-44](article-r212-44.md)
