# Article R212-41

Le président du tribunal de grande instance préside l'assemblée des magistrats du siège et du parquet.

Cette assemblée comprend :

1° Les membres de l'assemblée des magistrats du siège ;

2° Les membres de l'assemblée des magistrats du parquet.

Les auditeurs de justice, en stage dans le tribunal de grande instance, assistent à l'assemblée des magistrats du siège et du parquet.
