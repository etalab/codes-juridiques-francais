# Sous-section 7 :  La commission plénière

- [Article R212-51](article-r212-51.md)
- [Article R212-52](article-r212-52.md)
- [Article R212-53](article-r212-53.md)
- [Article R212-54](article-r212-54.md)
- [Article R212-54-1](article-r212-54-1.md)
