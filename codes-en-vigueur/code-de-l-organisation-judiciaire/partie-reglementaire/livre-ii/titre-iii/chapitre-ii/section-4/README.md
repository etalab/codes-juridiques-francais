# Section 4 : Les assemblées générales

- [Sous-Section 1 : Dispositions communes aux différentes formations de l'assemblée générale](sous-section-1)
- [Sous-Section 2 : L'assemblée des juges de proximité](sous-section-2)
- [Sous-Section 3 : L'assemblée des juges de proximité et des magistrats du parquet](sous-section-3)
- [Article R232-5](article-r232-5.md)
