# Article R232-1

Il est procédé à l'installation des juges de proximité, en séance publique, par le magistrat chargé de la direction et de l'administration du tribunal d'instance dans le ressort duquel la juridiction de proximité a son siège.
