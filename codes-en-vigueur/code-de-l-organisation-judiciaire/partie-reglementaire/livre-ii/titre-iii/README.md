# TITRE III : LA JURIDICTION DE PROXIMITE

- [Chapitre Ier : Institution et compétence](chapitre-ier)
- [Chapitre II : Organisation et fonctionnement](chapitre-ii)
