# Article R222-25

L'assemblée des magistrats du siège et du parquet émet un avis sur :

1° Le nombre, le jour et la nature des audiences ;

2° Les heures d'ouverture et de fermeture au public du greffe ;

3° Les besoins nécessaires au fonctionnement de la juridiction exprimés par le magistrat chargé de la direction et de l'administration du tribunal d'instance avec le concours du directeur de greffe ;

4° L'affectation des moyens alloués à la juridiction ;

5° Les mesures relatives à l'entretien des locaux, à la bibliothèque et au mobilier ;

6° Les conditions de travail du personnel et les problèmes de sécurité ;

7° Les questions intéressant le fonctionnement interne de la juridiction.
