# Article R222-27

Chaque année, l'assemblée des magistrats du siège et du parquet entend le rapport général d'activité des juges de proximité.
