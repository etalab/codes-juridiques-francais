# Article R222-31

L'assemblée plénière procède à un échange de vues sur les questions mentionnées à l'article R. 222-29.

L'assemblée plénière élabore et arrête son règlement intérieur selon le règlement intérieur type arrêté par le garde des sceaux, ministre de la justice, en l'adaptant, le cas échéant, dans les conditions prévues au deuxième alinéa de l'article R. 222-20.
