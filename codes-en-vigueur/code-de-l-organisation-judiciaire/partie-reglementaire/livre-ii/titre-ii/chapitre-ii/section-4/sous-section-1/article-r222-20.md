# Article R222-20

Les modalités de convocation, de dépouillement des votes, de désignation du secrétaire, d'établissement et de dépôt des procès-verbaux des délibérations des différentes formations de l'assemblée générale sont déterminées par le règlement intérieur.

Le garde des sceaux, ministre de la justice, fixe par arrêté un règlement intérieur type pour chacune des assemblées. Ces dernières peuvent adapter ce règlement type pour tenir compte de spécificités locales ou pour améliorer la concertation interne.

Le règlement intérieur et les modifications qui lui sont apportées sont transmis au premier président de la cour d'appel et au procureur général près cette cour.
