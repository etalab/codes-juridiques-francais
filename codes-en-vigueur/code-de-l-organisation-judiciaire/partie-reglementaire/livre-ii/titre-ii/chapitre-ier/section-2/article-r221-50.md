# Article R221-50

Dans les cas prévus au 5° de l'article R. 221-14, la demande est portée devant le tribunal dans le ressort duquel sont situés les objets warrantés.
