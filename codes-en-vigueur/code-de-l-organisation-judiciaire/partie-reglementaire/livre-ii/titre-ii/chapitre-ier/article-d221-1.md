# Article D221-1

Le siège et le ressort des tribunaux d'instance sont fixés conformément au tableau IV annexé au présent code.

Le siège et le ressort des tribunaux d'instance appelés à recevoir et à enregistrer la déclaration de la nationalité française et à délivrer les certificats de nationalité française, dans les cas et conditions prévus par le code civil, sont fixés conformément au tableau IX annexé au présent code.

Pour l'application de l'article L. 221-8-1, le siège et le ressort des tribunaux d'instance compétents, dans le ressort de certains tribunaux de grande instance, pour connaître des mesures de traitement des situations de surendettement des particuliers et des procédures de rétablissement personnel sont fixés conformément au tableau IX-I annexé au présent code.

Le siège et le ressort des tribunaux d'instance ayant compétence exclusive en matière pénale sont fixés conformément au tableau X annexé au présent code.
