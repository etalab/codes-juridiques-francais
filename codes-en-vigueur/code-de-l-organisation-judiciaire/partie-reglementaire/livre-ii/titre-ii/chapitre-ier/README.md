# Chapitre Ier : Institution et compétence

- [Section 1 : Compétence matérielle](section-1)
- [Section 2 : Compétence territoriale](section-2)
- [Article D221-1](article-d221-1.md)
- [Article R221-2](article-r221-2.md)
