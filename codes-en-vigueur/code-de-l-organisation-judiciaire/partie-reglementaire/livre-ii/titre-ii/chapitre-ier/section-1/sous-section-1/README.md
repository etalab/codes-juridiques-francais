# Sous-section 1 : Compétence civile du  tribunal d'instance

- [Paragraphe 1 : Compétence à charge d'appel](paragraphe-1)
- [Paragraphe 2 : Compétence en dernier ressort](paragraphe-2)
- [Paragraphe 3 : Compétence à charge d'appel ou en dernier ressort selon le montant de la demande](paragraphe-3)
- [Paragraphe 4 : Compétence en matière de demandes incidentes et de moyens de défense](paragraphe-4)
