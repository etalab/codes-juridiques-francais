# Article R312-45

Le procureur général près la cour d'appel préside l'assemblée des magistrats du parquet.

Cette assemblée comprend :

1° Les magistrats du parquet près la cour d'appel ;

2° Les magistrats placés auprès du procureur général exerçant leurs fonctions au parquet près cette cour.

Les auditeurs de justice, en stage au parquet près la cour d'appel, assistent à cette assemblée.
