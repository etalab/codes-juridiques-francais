# Article Tableau IV bis

Compétence civile et pénale de certaines chambres détachées

<table>
<tbody>
<tr>
<th>
<br/>TRIBUNAL <br/>
<br/>de grande instance<br/>
</th>
<th>
<br/>CHAMBRE <br/>
<br/>détachée<br/>
</th>
<th>
<br/>COMPÉTENCE CIVILE<br/>
</th>
<th>
<br/>COMPÉTENCE PÉNALE<br/>
</th>
</tr>
<tr>
<td align="left" valign="top">
<br/>Agen<br/>
</td>
<td align="left" valign="top">
<br/>Marmande<br/>
</td>
<td align="left" valign="top">
<br/>Matières relevant de l'article L. 213-3 du code de l'organisation judiciaire, à l'exception de celles relevant du 1° et du d du 3° de cet article, et de celles relevant des articles 233, 237, 242 et 296 du code civil.<br/>
<br/>Matières relevant de l'article L. 213-3-1 du code de l'organisation judiciaire.<br/>
</td>
<td align="left" valign="top">
<br/>Délits dont la liste est fixée aux 2° et 3° de l'article 398-1 du code de procédure pénale.<br/>
<br/>Procédures de comparution sur reconnaissance préalable de culpabilité dont l'audience d'homologation est régie par les articles 495-9 et 495-11 à 495-16 du code de procédure pénale.<br/>
</td>
</tr>
<tr>
<td align="left" valign="top">
<br/>Lons-le-Saunier<br/>
</td>
<td align="left" valign="top">
<br/>Dole<br/>
</td>
<td align="left" valign="top">
<br/>Matières relevant de l'article L. 213-3 du code de l'organisation judiciaire, à l'exception de celles relevant du 1° et du d du 3° de cet article, et de celles relevant des articles 376 à 377-3 du code civil.<br/>
</td>
<td align="left" valign="top">
<br/>Délits dont la liste est fixée à l'article 398-1 du code de procédure pénale.<br/>
</td>
</tr>
<tr>
<td align="left" valign="top">
<br/>Rodez<br/>
</td>
<td align="left" valign="top">
<br/>Millau<br/>
</td>
<td align="left" valign="top">
<br/>Matières relevant de l'article L. 213-3 du code de l'organisation judiciaire, à l'exception de celles relevant des articles 377 à 377-3 du code civil.<br/>
<br/>Matières relevant de l'article L. 213-3-1 du code de l'organisation judiciaire.<br/>
</td>
<td align="left" valign="top">
<br/>Délits dont la liste est fixée à l'article 398-1 du code de procédure pénale.<br/>
<br/>Procédures de comparution sur reconnaissance préalable de culpabilité dont l'audience d'homologation est régie par les articles 495-9 et 495-11 à 495-16 du code de procédure pénale.<br/>
</td>
</tr>
<tr>
<td align="left" valign="top">
<p align="left">
<br/>Saint-Brieuc</p>
</td>
<td align="left" valign="top">
<p align="left">
<br/>Guingamp</p>
</td>
<td align="left" valign="top">
<p align="left">
<br/>Matières relevant de l'article 311-20 du code civil.<br/>
<br/>Matières relevant de l'article L. 213-3 du code de l'organisation judiciaire, à l'exception de celles relevant des articles 205, 206 et 376 à 377-3 du code civil, et de celles relevant des articles 233, 237, 242, 296, 371-4, 515-7, 515-8, 840, 1400, 1536 et 1569 du code civil pour lesquelles l'assignation a été enrôlée au tribunal de grande instance de Saint-Brieuc avant le 1er septembre 2014.<br/>
<br/>Matières relevant de l'article L. 213-3-1 du code de l'organisation judiciaire.</p>
</td>
<td align="left" valign="top">
<p align="left">
<br/>Aucune compétence pénale du 1er janvier 2015 au 28 février 2015 inclus.<br/>
<br/>A compter du 1er mars 2015, délits dont la liste est fixée à l'article 398-1 du code de procédure pénale.</p>
</td>
</tr>
</tbody>
</table>
