# Article Annexe Tableau XIII

Liste des bureaux fonciers

(annexe de l'article D. 223-3)

<table>
<tbody>
<tr>
<td align="center">
<p align="center">TRIBUNAL D'INSTANCE </p>
</td>
<td align="center">
<p align="center">BUREAU FONCIER</p>
</td>
<td align="center">
<p align="center">RESSORT </p>
</td>
</tr>
<tr>
<td colspan="3">
<p align="center">Cour d'appel de Colmar </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Thann <br/>
</p>
</td>
<td align="center">
<p align="left">Thann <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort du tribunal d'instance de Thann <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Colmar <br/>
</p>
</td>
<td align="center">
<p align="left">Colmar <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort du tribunal d'instance de Colmar <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Guebwiller <br/>
</p>
</td>
<td align="center">
<p align="left">Guebwiller <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort du tribunal d'instance de Guebwiller <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Haguenau <br/>
</p>
</td>
<td align="center">
<p align="left">Haguenau <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort du tribunal d'instance de Haguenau <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Mulhouse <br/>
</p>
</td>
<td align="center">
<p align="left">Mulhouse <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort du tribunal d'instance de Mulhouse <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Saverne <br/>
</p>
</td>
<td align="center">
<p align="left">Saverne <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort du tribunal de grande instance de Saverne <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Sélestat <br/>
</p>
</td>
<td align="center">
<p align="left">Sélestat <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort du tribunal d'instance de Sélestat <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Strasbourg <br/>
</p>
</td>
<td align="center">
<p align="left">Strasbourg <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort des tribunaux d'instance d'Illkirch-Graffenstaden, de Schiltigheim et de Strasbourg <br/>
</p>
</td>
</tr>
<tr>
<td colspan="3">
<p align="center">Cour d'appel de Metz </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Metz <br/>
</p>
</td>
<td align="center">
<p align="left">Metz <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort du tribunal de grande instance de Metz <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Sarreguemines <br/>
</p>
</td>
<td align="center">
<p align="left">Sarreguemines <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort du tribunal de grande instance de Sarreguemines <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Thionville <br/>
</p>
</td>
<td align="center">
<p align="left">Thionville <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort du tribunal de grande instance de Thionville<br/>
</p>
</td>
</tr>
</tbody>
</table>
