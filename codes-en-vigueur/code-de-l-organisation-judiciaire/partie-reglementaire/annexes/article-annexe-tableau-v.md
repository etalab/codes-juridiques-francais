# Article Annexe Tableau V

Siège et ressort des tribunaux de grande instance compétents pour connaître des actions

en matière d'obtentions végétales (annexe de l'article D. 211-5)

<table>
<tbody>
<tr>
<td>
<p align="center">SIÈGE</p>
</td>
<td>
<p align="center">RESSORT</p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">Cour d'appel d'Aix-en-Provence</p>
</td>
</tr>
<tr>
<td>
<p align="left">Marseille</p>
</td>
<td>
<p align="left">Ressort des cours d'appel d'Aix-en-Provence, Bastia et Nîmes. </p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">Cour d'appel de Bordeaux</p>
</td>
</tr>
<tr>
<td>
<p align="left">Bordeaux</p>
</td>
<td>
<p align="left">Ressort des cours d'appel d'Agen, Bordeaux et Poitiers.</p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">Cour d'appel de Colmar</p>
</td>
</tr>
<tr>
<td>
<p align="left">Strasbourg</p>
</td>
<td>
<p align="left">Ressort des cours d'appel de Colmar et Metz.</p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">Cour d'appel de Douai</p>
</td>
</tr>
<tr>
<td>
<p align="left">Lille</p>
</td>
<td>
<p align="left">Ressort des cours d'appel d'Amiens et Douai.</p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">Cour d'appel de Limoges</p>
</td>
</tr>
<tr>
<td>
<p align="left">Limoges</p>
</td>
<td>
<p align="left">Ressort des cours d'appel de Bourges, Limoges et Riom.</p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">Cour d'appel de Lyon</p>
</td>
</tr>
<tr>
<td>
<p align="left">Lyon</p>
</td>
<td>
<p align="left">Ressort des cours d'appel de Chambéry, Grenoble et Lyon.</p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">Cour d'appel de Nancy</p>
</td>
</tr>
<tr>
<td>
<p align="left">Nancy</p>
</td>
<td>
<p align="left">Ressort des cours d'appel de Besançon, Dijon et Nancy.</p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">Cour d'appel de Paris</p>
</td>
</tr>
<tr>
<td align="left" valign="top">
<p align="left">Paris</p>
</td>
<td align="left" valign="top">
<p align="left">Ressort des cours d'appel de Basse-Terre, Cayenne, Fort-de-France, Nouméa, Orléans, Papeete, Paris, Reims, Rouen, Saint-Denis et Versailles, et du tribunal supérieur d'appel de Saint-Pierre.</p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">Cour d'appel de Rennes</p>
</td>
</tr>
<tr>
<td>
<p align="left">Rennes</p>
</td>
<td>
<p align="left">Ressort des cours d'appel d'Angers, Caen et Rennes.</p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">Cour d'appel de Toulouse</p>
</td>
</tr>
<tr>
<td>
<p align="left">Toulouse</p>
</td>
<td>
<p align="left">Ressort des cours d'appel de Montpellier, Pau et Toulouse.</p>
</td>
</tr>
</tbody>
</table>
