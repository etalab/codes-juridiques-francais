# Article Annexe Tableau IX

Siège et ressort des tribunaux d'instance, des tribunaux de première instance et des sections détachées compétents pour recevoir et enregistrer les déclarations de nationalité française et délivrer les certificats de nationalité française (annexe de l'article D. 221-1)

<table>
<tbody>
<tr>
<td width="300">
<p align="center">Siège </p>
</td>
<td width="314">
<p align="center">Ressort </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel d'Agen </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Gers </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Auch </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Auch, Condom, Lectoure et Mirande. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Auch </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Auch et Condom. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Lot </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Cahors </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Cahors, Figeac et Gourdon. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Cahors </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Cahors et Figeac. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Lot-et-Garonne </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Agen </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Agen, Marmande, Nérac et Villeneuve-sur-Lot. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Agen </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Agen, Marmande et Villeneuve-sur-Lot. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel d'Aix-en-Provence </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Alpes-de-Haute-Provence </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Digne-les-Bains </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Barcelonnette, Digne-les-Bains et Forcalquier. (7)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Digne-les-Bains </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Barcelonnette, Digne-les-Bains et Manosque. (8)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Alpes-Maritimes </p>
</td>
</tr>
<tr>
<td width="300">
<p>Cannes </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Antibes, Cagnes-sur-Mer, Cannes et Grasse. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Nice </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Menton et Nice. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Bouches-du-Rhône </p>
</td>
</tr>
<tr>
<td width="300">
<p>Aix-en-Provence </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Aix-en-Provence, Martigues et Salon-de-Provence. </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Arles </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Arles et Tarascon. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Tarascon </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Tarascon. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>Marseille </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Aubagne et Marseille. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Var </p>
</td>
</tr>
<tr>
<td width="300">
<p>Fréjus </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Brignoles, Draguignan et Fréjus. </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Toulon </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Hyères et Toulon. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Toulon </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Toulon. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel d'Amiens </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Aisne </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Laon </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Laon, Saint-Quentin et Vervins. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Laon </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Laon et Saint-Quentin. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Soissons </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Château-Thierry et Soissons. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Soissons </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Soissons. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Oise </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Beauvais </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Beauvais et Clermont. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Beauvais </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Beauvais. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>Senlis </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Compiègne et Senlis. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Somme </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Amiens </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Abbeville, Amiens, Doullens, Montdidier et Péronne. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Amiens </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Abbeville, Amiens et Péronne. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel d'Angers </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Maine-et-Loire </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Angers </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Angers, Baugé, Cholet, Saumur et Segré. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Angers </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Angers, Cholet et Saumur. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Mayenne </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Laval </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Château-Gontier et Laval. (12 et 3)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Sarthe </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Le Mans </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de La Flèche, Le Mans, Mamers et Saint-Calais. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Le Mans </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de La Flèche et Le Mans. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Basse-Terre </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Guadeloupe </p>
</td>
</tr>
<tr>
<td width="300">
<p>Basse-Terre </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Basse-Terre. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Saint-Martin </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Saint-Martin. </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Marie-Galante </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Marie-Galante. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>Pointe-à-Pitre </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Pointe-à-Pitre. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Bastia </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Corse-du-Sud </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Ajaccio </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Ajaccio et Sartène. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Ajaccio </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance d'Ajaccio. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Haute-Corse </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Bastia </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Bastia, Corte et L'Ile Rousse. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Bastia </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Bastia. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Besançon </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Doubs </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Besançon </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Baume-les-Dames, Besançon et Pontarlier. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Besançon </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Besançon et Pontarlier. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>Montbéliard </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Montbéliard. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Haute-Saône </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Vesoul </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Gray, Lure, Luxeuil-les-Bains et Vesoul. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Vesoul </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Lure et Vesoul. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Jura </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Lons-le-Saunier </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Arbois, Dole, Lons-le-Saunier et Saint-Claude. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Lons-le-Saunier </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Dole, Lons-le-Saunier et Saint-Claude. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Territoire de Belfort </p>
</td>
</tr>
<tr>
<td width="300">
<p>Belfort </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Belfort. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Bordeaux </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Charente </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Angoulême </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Angoulême, Cognac, Confolens et Ruffec. (2 et 3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Angoulême </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Angoulême et Cognac. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Dordogne </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Périgueux </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Bergerac, Nontron, Périgueux, Ribérac et Sarlat-la-Canéda. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Périgueux </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Bergerac, Périgueux et Sarlat-la-Canéda. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Gironde </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Bordeaux </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Arcachon, Bazas, Blaye, Bordeaux, La Réole, Lesparre-Médoc et Libourne. (9)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Bordeaux </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Arcachon, Bazas, Blaye, Bordeaux, La Réole et Libourne. (10 et 3)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Bourges </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cher </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Bourges </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Bourges, Saint-Amand-Montrond, Sancerre et Vierzon. (11)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Bourges </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Bourges, Saint-Amand-Montrond et Sancerre. (12 et 3)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Indre </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Châteauroux </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Châteauroux, Issoudun, La Châtre et Le Blanc. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Châteauroux </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Châteauroux. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Nièvre </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Nevers </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Château-Chinon, Clamecy, Cosne-Cours-sur-Loire et Nevers. (9)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Nevers </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Clamecy et Nevers. (10)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Caen </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Calvados </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Caen </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Bayeux, Caen, Falaise, Lisieux, Pont-l'Evêque et Vire. (11)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Caen </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Bayeux, Caen, Lisieux, Pont-l'Evêque et Vire. (12 et 3)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Manche </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Cherbourg-Octeville </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Cherbourg-Octeville et Valognes. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Cherbourg-Octeville </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Cherbourg-Octeville. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Coutances </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Avranches, Coutances, Mortain et Saint-Lô. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Coutances </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Avranches et Coutances. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Orne </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Alençon </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Alençon, Argentan, Domfront et Mortagne-au-Perche. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Alençon </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Alençon, Argentan et Flers. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">Cour d'appel de Cayenne (Guyane)</p>
</td>
</tr>
<tr>
<td>
<p align="left">Cayenne</p>
</td>
<td>
<p align="left">Ressort du tribunal d'instance de Cayenne.</p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Chambéry </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Haute-Savoie </p>
</td>
</tr>
<tr>
<td width="300">
<p>Annecy </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance d'Annecy. </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Saint-Julien-en-Genevois </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Bonneville, Saint-Julien-en-Genevois et Thonon-les-Bains. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Annemasse </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Annemasse, Bonneville et Thonon-les-Bains. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Savoie </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Chambéry </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Aix-les-Bains, Albertville, Chambéry, Moutiers-Tarentaise et Saint-Jean-de-Maurienne. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Chambéry </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Albertville et Chambéry. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Colmar </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Bas-Rhin </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Haguenau </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Brumath, Haguenau et Wissembourg. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Haguenau </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance d'Haguenau. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>Illkirch-Graffenstaden </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance d'Illkirch-Graffenstaden. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Saverne </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Molsheim et Saverne. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Strasbourg </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Schiltigheim et Strasbourg. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Haut-Rhin </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Colmar </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Colmar, Guebwiller, Ribeauvillé et Sélestat. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Colmar </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Colmar, Guebwiller et Sélestat. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Mulhouse </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Altkirch, Huningue et Mulhouse. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Mulhouse </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Mulhouse. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>Thann </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Thann. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Dijon </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Côte-d'Or </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Dijon </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Beaune, Châtillon-sur-Seine, Dijon et Semur-en-Auxois. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Dijon </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Beaune, Dijon et Montbard. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Haute-Marne </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Chaumont </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Chaumont, Langres et Saint-Dizier. (9)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Chaumont </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Chaumont et Saint-Dizier. (10)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Saône-et-Loire </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Chalon-sur-Saône </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Autun, Chalon-sur-Saône et Louhans. (11)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Chalon-sur-Saône </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Chalon-sur-Saône et Louhans. (12 et 3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Le Creusot </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance du Creusot et de Montceau-les-Mines. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Le Creusot </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance du Creusot. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Mâcon </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Charolles et Mâcon. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Mâcon </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Mâcon. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Douai </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Nord </p>
</td>
</tr>
<tr>
<td width="300">
<p>Avesnes-sur-Helpe </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Avesnes-sur-Helpe et Maubeuge. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Douai </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Cambrai et Douai. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Dunkerque </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Dunkerque et Hazebrouck. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Lille </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Lille. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Roubaix </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Roubaix et Tourcoing. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Valenciennes </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Valenciennes. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Pas-de-Calais </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Arras </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Arras et Saint-Pol-sur-Ternoise. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Arras </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance d'Arras. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Béthune </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Béthune, Carvin, Houdain, Lens et Liévin. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Béthune </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Béthune et Lens. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>Boulogne-sur-Mer </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Boulogne-sur-Mer, Calais, Montreuil et Saint-Omer. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Fort-de-France (Martinique)<br/>
</p>
</td>
</tr>
<tr>
<td width="300">
<p>Fort-de-France </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Fort-de-France.</p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Grenoble </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Drôme </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Montélimar </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Montélimar et Nyons. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Montélimar </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Montélimar. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>Romans-sur-Isère </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Romans-sur-Isère. </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Valence </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Die et Valence. (13)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Valence </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Valence. (14)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Hautes-Alpes </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Gap </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Briançon et Gap. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Gap </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Gap. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Isère </p>
</td>
</tr>
<tr>
<td width="300">
<p>Bourgoin-Jallieu </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Bourgoin-Jallieu. </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Grenoble </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Grenoble, La Mure et Saint-Marcellin. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Grenoble </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Grenoble. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>Vienne </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Vienne. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Limoges </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Corrèze </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Tulle </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Brive-la-Gaillarde, Tulle et Ussel. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Tulle </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Brive-la-Gaillarde et Tulle. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Creuse </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Guéret </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Bourganeuf et Guéret. (9)] </p>
</td>
</tr>
<tr>
<td>
<p>[Guéret </p>
</td>
<td>
<p>Ressort du tribunal d'instance de Guéret. (10)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Haute-Vienne </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Limoges </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Bellac, Limoges, Rochechouart et Saint-Yrieix-la-Perche. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Limoges </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Limoges. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Lyon </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Ain </p>
</td>
</tr>
<tr>
<td width="300">
<p>Bourg-en-Bresse </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Belley, Bourg-en-Bresse, Nantua et Trévoux. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Loire </p>
</td>
</tr>
<tr>
<td width="300">
<p>Roanne </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Roanne. </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Saint-Etienne </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance du Chambon-Feugerolles, Montbrison et Saint-Etienne. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Saint-Etienne </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Montbrison et Saint-Etienne. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Rhône </p>
</td>
</tr>
<tr>
<td width="300">
<p>Lyon </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Lyon. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Villefranche-sur-Saône </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Villefranche-sur-Saône. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Villeurbanne </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Villeurbanne. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Metz </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Moselle </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Forbach </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Forbach, Saint-Avold et Sarreguemines. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Saint-Avold </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Saint-Avold et Sarreguemines. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Metz </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Boulay-Moselle, Château Salins, Metz et Sarrebourg. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Metz </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Metz et Sarrebourg. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Thionville </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Hayange et Thionville. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Thionville </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Thionville. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Montpellier </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Aude </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Carcassonne </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Carcassonne, Castelnaudary, Limoux et Narbonne. (9)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Carcassonne </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Carcassonne, Limoux et Narbonne. (10 et 3)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Aveyron </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Millau </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Millau et Saint-Affrique. (9)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Millau </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Millau. (10)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Rodez </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Espalion, Rodez et Villefranche-de-Rouergue. (9)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Rodez </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Rodez. (10)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Hérault </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Béziers </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Béziers et Saint-Pons-de-Thomières. (11)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Béziers </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Béziers. (12)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Montpellier </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Lodève, Montpellier et Sète. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Montpellier </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Montpellier et Sète. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Pyrénées-Orientales </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Perpignan </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Céret, Prades et Perpignan. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Perpignan </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Perpignan. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Nancy </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Meurthe-et-Moselle </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Briey </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Briey et Longwy. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Briey </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Briey. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Nancy </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Lunéville, Nancy et Toul. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Nancy </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Lunéville et Nancy. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Meuse </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Verdun </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Bar-le-Duc, Saint-Mihiel et Verdun. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Verdun </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Bar-le-Duc et Verdun. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Vosges </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Epinal </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Epinal, Mirecourt, Remiremont et Saint-Dié-des-Vosges. (11)] </p>
</td>
</tr>
<tr>
<td>
<p>[Epinal </p>
</td>
<td>
<p>Ressort des tribunaux d'instance d'Epinal, Mirecourt, et Saint-Dié-des-Vosges. (12 et 3)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Nîmes </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Ardèche </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Annonay </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance d'Annonay. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Aubenas </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance d'Aubenas. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Largentière </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Largentière. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>Privas </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Privas. </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Tournon-sur-Rhône </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Tournon-sur-Rhône. (3)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Gard </p>
</td>
</tr>
<tr>
<td width="300">
<p>Alès </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance d'Alès. </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Le Vigan </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance du Vigan. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>Nîmes </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Nîmes. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Uzès </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance d'Uzès. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Lozère </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Mende </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Florac, Marvejols et Mende. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Mende </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Mende. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Vaucluse </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Avignon </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Apt et Avignon. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Avignon </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance d'Avignon et Pertuis. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>Orange </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Carpentras et Orange. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Nouméa </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Nouvelle-Calédonie </p>
</td>
</tr>
<tr>
<td width="300">
<p>Nouméa </p>
</td>
<td width="314">
<p>Ressort du tribunal de Première instance de Nouméa, à l'exception de la province Nord et de la province des Iles Loyauté. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Koné </p>
</td>
<td width="314">
<p>Province Nord. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Lifou </p>
</td>
<td width="314">
<p>Province des lies Loyauté. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Wallis-et-Futuna </p>
</td>
</tr>
<tr>
<td width="300">
<p>Mata-Utu </p>
</td>
<td width="314">
<p>Ressort du tribunal de Première instance de Mata-Utu. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel d'Orléans </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Indre-et-Loire </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Tours </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Chinon, Loches et Tours. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Tours </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Tours. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Loiret </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Montargis </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Gien et Montargis. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Montargis </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Montargis. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Orléans </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance d'Orléans. (2)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Loir-et-Cher </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Blois </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Blois, Romorantin-Lanthenay et Vendôme. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Blois </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Blois. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Papeete </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Polynésie française </p>
</td>
</tr>
<tr>
<td width="300">
<p>Papeete </p>
</td>
<td width="314">
<p>Ressort du tribunal de Première instance de Papeete, l'exception des Îles Sous-le-Vent et des Îles Marquises. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Uturoa </p>
</td>
<td width="314">
<p>Îles Sous-le-Vent. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Nuku-Hiva </p>
</td>
<td width="314">
<p>Îles Marquises. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Paris </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Essonne </p>
</td>
</tr>
<tr>
<td width="300">
<p>Etampes </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance d'Etampes. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Evry </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance d'Evry. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Juvisy-sur-Orge </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Juvisy-sur-Orge. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Longjumeau </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Longjumeau. </p>
</td>
</tr>
<tr>
<td width="300">
<p>Palaiseau </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Palaiseau. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Paris </p>
</td>
</tr>
<tr>
<td width="300">
<p>Paris 1er arrondissement </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Paris 1er arrondissement, Paris 2e arrondissement, Paris 3e arrondissement, Paris 4e arrondissement, Paris 5e arrondissement, Paris 6e arrondissement, Paris 7e arrondissement, Paris 8e arrondissement, Paris 9e arrondissement, Paris 10e arrondissement, Paris 11e arrondissement, Paris 12e arrondissement, Paris 13e arrondissement, Paris 14e arrondissement, Paris 15e arrondissement, Paris 16e arrondissement, Paris 17e arrondissement, Paris 18e arrondissement, Paris 19e arrondissement et Paris 20e arrondissement. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Seine-et-Marne </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Fontainebleau </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Fontainebleau. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Montereau-Fault-Yonne </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Montereau-Fault-Yonne. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Lagny-sur-Marne </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Lagny-sur-Marne. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Meaux </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Coulommiers et Meaux. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Meaux </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Meaux. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Melun </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Melun et Provins. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Melun </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Melun. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Seine-Saint-Denis </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Aubervilliers </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance d'Aubervilliers. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Aulnay-sous-Bois </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance d'Aulnay-sous-Bois. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Bobigny </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Bobigny. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Le Raincy </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance du Raincy. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Montreuil </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Montreuil. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Pantin </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Pantin. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Saint-Denis </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Saint-Denis. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Saint-Ouen </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Saint-Ouen. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Val-de-Marne </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Boissy-Saint-Léger </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Boissy-Saint-Léger. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Charenton-le-Pont </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Charenton-le-Pont. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Ivry-sur-Seine </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance d'Ivry-sur-Seine. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Nogent-sur-Marne </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Nogent-sur-Marne. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Saint-Maur-des-Fossés </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Saint-Maur-des-Fossés. </p>
</td>
</tr>
<tr>
<td>
<p align="left">Sucy-en-Brie</p>
</td>
<td>
<p align="left">Ressort du tribunal d'instance de Sucy-en-Brie.</p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Villejuif </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Villejuif. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Vincennes </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Vincennes. (3)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Yonne </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Auxerre </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance d'Auxerre, Avallon et Tonnerre. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Auxerre </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance d'Auxerre. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Sens </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Joigny et Sens. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Sens </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Sens. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Pau </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Hautes-Pyrénées </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Tarbes </p>
</td>
<td width="314">
<p>Ressort des tribunaux d'instance de Bagnères-de-Bigorre, Lourdes et Tarbes. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p>[Tarbes </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Tarbes. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Landes </p>
</td>
</tr>
<tr>
<td width="300">
<p>Dax</p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Dax.</p>
</td>
</tr>
<tr>
<td>[Mont-de-Marsan <br/>
</td>
<td>Ressort des tribunaux d'instance de Mont-de-Marsan et Saint-Sever. (7)] <br/>
</td>
</tr>
<tr>
<td width="300">
<p>[Mont-de-Marsan </p>
</td>
<td width="314">
<p>Ressort du tribunal d'instance de Mont-de-Marsan. (8)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Pyrénées-Atlantiques </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Bayonne </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Bayonne, Biarritz et Saint-Palais. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Bayonne </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Bayonne. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Pau </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance d'Oloron-Sainte-Marie, Orthez et Pau. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Pau </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance d'Oloron-Sainte-Marie et Pau. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Poitiers </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Charente-Maritime </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[La Rochelle </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de La Rochelle, Marennes et Rochefort. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[La Rochelle </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de La Rochelle et Rochefort. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Saintes </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Jonzac, Saintes et Saint-Jean-d'Angély. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Saintes </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Jonzac et Saintes. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Deux-Sèvres </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Bressuire </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Bressuire et Parthenay. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Bressuire </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Bressuire. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Niort </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Melle et Niort. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Niort </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Niort. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Vendée </p>
</td>
</tr>
<tr>
<td align="left" valign="top">
<p>La Roche-sur-Yon </p>
</td>
<td align="left" valign="top">
<p>Ressort des tribunaux d'instance de Fontenay-le-Comte, La Roche-sur-Yon et Les Sables-d'Olonne. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Vienne </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Poitiers </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Châtellerault, Civray, Loudun, Montmorillon et Poitiers. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Poitiers </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Châtellerault et Poitiers. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Reims </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Ardennes </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Charleville-Mézières </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Charleville-Mézières, Rethel, Rocroi, Sedan et Vouziers. (13)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Charleville-Mézières </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Charleville-Mézières, Rethel, Rocroi et Sedan. (14 et 3)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Aube </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Troyes </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Bar-sur-Aube, Bar-sur-Seine, Nogent-sur-Seine et Troyes. (13)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Troyes </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Bar-sur-Seine, Nogent-sur-Seine et Troyes. (14 et 3)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Marne </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Châlons-en-Champagne </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Châlons-en-Champagne, Epernay et Vitry-le-François. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Châlons-en-Champagne </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Châlons-en-Champagne. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Reims </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Reims. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Rennes </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Côtes-d'Armor </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Saint-Brieuc </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Dinan, Guingamp, Lannion, Loudéac et Saint-Brieuc. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Saint-Brieuc </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Dinan, Guingamp et Saint-Brieuc. (5)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Saint-Brieuc </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Guingamp et Saint-Brieuc. (6)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Finistère </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Brest </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Brest et Morlaix. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Quimper </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Châteaulin, Quimper et Quimperlé. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Quimper </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Quimper. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Ille-et-Vilaine </p>
</td>
</tr>
<tr>
<td align="left" valign="top">
<p>[Rennes </p>
</td>
<td align="left" valign="top">
<p>Ressort des tribunaux d'instance de Fougères, Montfort-sur-Meu, Redon, Rennes, Saint-Malo et Vitré. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Rennes </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Redon, Rennes et Saint-Malo. (5)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Rennes </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Dinan, Redon, Rennes et Saint-Malo. (6)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Loire-Atlantique </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Nantes </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Châteaubriant et Nantes. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Nantes </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Nantes. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Saint-Nazaire </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Paimbœuf et Saint-Nazaire. (11)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Saint-Nazaire </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Saint-Nazaire. (12)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Morbihan </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Vannes </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance d'Auray, Lorient, Ploërmel, Pontivy et Vannes. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Vannes </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Lorient et Vannes. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Riom </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Allier </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Montluçon </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Montluçon. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Moulins </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Moulins. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Vichy </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Gannat et Vichy. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Vichy </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Vichy. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cantal </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Aurillac </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance d'Aurillac et Mauriac. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Aurillac </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance d'Aurillac. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Saint-Flour </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Murat et Saint-Flour. (11)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Saint-Flour </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Saint-Flour. (12)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Haute-Loire </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Le Puy-en-Velay </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Brioude, Le Puy-en-Velay et Yssingeaux. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Le Puy-en-Velay </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance du Puy-en-Velay. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Puy-de-Dôme </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Clermont-Ferrand </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Clermont-Ferrand. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Issoire </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance d'Ambert et Issoire. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Riom </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Riom. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Thiers </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Thiers. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Rouen </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Eure </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Evreux </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Bernay, Evreux, Les Andelys, Louviers et Pont-Audemer. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Evreux </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Bernay, Evreux et Les Andelys. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Seine-Maritime </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Dieppe </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Dieppe et Neufchâtel-en-Bray. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Dieppe </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Dieppe. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Le Havre </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance du Havre. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Rouen </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance d'Elbeuf, Rouen et Yvetot. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Rouen </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Rouen. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Saint-Denis </p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">Mayotte</p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Mamoudzou </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Mamoudzou <br/>
</p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">Réunion</p>
</td>
</tr>
<tr>
<td>
<p align="left">Saint-Benoît <br/>
</p>
</td>
<td>
<p align="left">Ressort du tribunal d'instance de Saint-Benoît. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Saint-Denis </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Saint-Denis. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Saint-Paul </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Saint-Paul. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Saint-Pierre </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Saint-Pierre. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Toulouse </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Ariège </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Foix </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Foix, Pamiers et Saint-Girons. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Foix </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Foix et Saint-Girons. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Haute-Garonne </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Muret </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Muret et Villefranche-de-Lauragais. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Muret </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Muret et Saint-Gaudens. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Toulouse </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Saint-Gaudens et Toulouse. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Toulouse </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Toulouse. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Tarn </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Albi </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance d'Albi, Castres, Gaillac et Lavaur. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Albi </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance d'Albi et Castres. (4)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Tarn-et-Garonne </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Montauban </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Castelsarrasin et Montauban. (2)] </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Cour d'appel de Versailles </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Eure-et-Loir </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Chartres </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Chartres, Châteaudun et Nogent-le-Rotrou. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Chartres </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Chartres. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Dreux </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Dreux. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Hauts-de-Seine </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Antony </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance d'Antony. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Asnières-sur-Seine </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance d'Asnières-sur-Seine. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Boulogne-Billancourt </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Boulogne-Billancourt. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Clichy </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Clichy. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Colombes </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Colombes. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Courbevoie </p>
</td>
<td width="314">
<p align="left">Ressort des tribunaux d'instance de Courbevoie, Levallois-Perret et Neuilly-sur-Seine. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Courbevoie </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Courbevoie. (4)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Puteaux </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Puteaux. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Vanves </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Vanves. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Val-d'Oise </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">[Ecouen </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance d'Ecouen. (3)] </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Gonesse </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Gonesse. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Montmorency </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Montmorency. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Pontoise </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Pontoise. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Sannois </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Sannois. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Yvelines </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Mantes-la-Jolie </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Mantes-la-Jolie. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Poissy </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Poissy. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Rambouillet </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Rambouillet. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Saint-Germain-en-Laye </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Saint-Germain-en-Laye. </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Versailles </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal d'instance de Versailles. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Tribunal supérieur d'appel de Saint-Pierre </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="center">Saint-Pierre-et-Miquelon </p>
</td>
</tr>
<tr>
<td width="300">
<p align="left">Saint-Pierre </p>
</td>
<td width="314">
<p align="left">Ressort du tribunal de Première instance de Saint-Pierre. </p>
</td>
</tr>
<tr>
<td colspan="2" width="614">
<p align="left">(2) applicable à compter du 1er janvier 2009. </p>
<p align="left">(3) applicable à compter du 31 décembre 2009. </p>
<p align="left">(4) applicable à compter du 1er janvier 2010. </p>
<p align="left">(5) applicable jusqu'au 31 décembre 2010. </p>
<p align="left">(6) applicable à compter du 1er janvier 2011. </p>
<p align="left">(7) applicable jusqu'au 31 janvier 2009. </p>
<p align="left">(8) applicable à compter du 1er février 2009. </p>
<p align="left">(9) applicable jusqu'au 30 septembre 2009. </p>
<p align="left">(10) applicable à compter du 1er octobre 2009. </p>
<p align="left">(11) applicable jusqu'au 31 août 2009. </p>
<p align="left">(12) applicable à compter du 1er septembre 2009. </p>
<p align="left">(13) applicable jusqu'au 30 juin 2009. </p>
<p align="left">(14) applicable à compter du 1er juillet 2009.</p>
</td>
</tr>
</tbody>
</table>
