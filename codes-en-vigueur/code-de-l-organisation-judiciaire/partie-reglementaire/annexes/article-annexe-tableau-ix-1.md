# Article Annexe Tableau IX-1

Siège et ressort des tribunaux d'instance compétents, dans le ressort de certains tribunaux de grande instance, pour connaître des mesures de traitement des situations de surendettement des particuliers et des procédures de rétablissement personnel (annexe de l'article D. 221-1)

<table>
<tbody>
<tr>
<td width="151">
<p align="center">SIÈGE DU TRIBUNAL<br/>de grande instance</p>
</td>
<td width="151">
<p align="center">SIÈGE DU TRIBUNAL d'instance</p>
</td>
<td width="378">
<p align="center">RESSORT</p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Cour d'appel d'Aix-en-Provence</p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Alpes-Maritimes</p>
</td>
</tr>
<tr>
<td valign="top" width="151">
<p align="left">Nice </p>
</td>
<td valign="top" width="151">
<p align="left">Nice </p>
</td>
<td valign="top" width="378">
<p align="left">Ressort des tribunaux d'instance de Nice et Menton </p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Bouches-du-Rhône</p>
</td>
</tr>
<tr>
<td valign="top" width="151">
<p align="left">Aix-en-Provence </p>
</td>
<td valign="top" width="151">
<p align="left">Aix-en-Provence </p>
</td>
<td valign="top" width="378">
<p align="left">Ressort des tribunaux d'instance d'Aix-en-Provence et Salon-de-Provence </p>
</td>
</tr>
<tr>
<td valign="top" width="151">
<p align="left">Marseille </p>
</td>
<td valign="top" width="151">
<p align="left">Marseille </p>
</td>
<td valign="top" width="378">
<p align="left">Ressort des tribunaux d'instance d'Aubagne et Marseille </p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Cour d'appel d'Angers</p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Maine-et-Loire</p>
</td>
</tr>
<tr>
<td valign="top" width="151">
<p align="left">Angers </p>
</td>
<td valign="top" width="151">
<p align="left">Angers </p>
</td>
<td valign="top" width="378">
<p align="left">Ressort des tribunaux d'instance d'Angers, Cholet et Saumur </p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Sarthe</p>
</td>
</tr>
<tr>
<td valign="top" width="151">
<p align="left">Le Mans </p>
</td>
<td valign="top" width="151">
<p align="left">Le Mans </p>
</td>
<td valign="top" width="378">
<p align="left">Ressort des tribunaux d'instance de La Flèche et Le Mans </p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Cour d'appel de Paris</p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Paris</p>
</td>
</tr>
<tr>
<td valign="top" width="151">
<p align="left">Paris </p>
</td>
<td valign="top" width="151">
<p align="left">Paris 19e arrondissement </p>
</td>
<td valign="top" width="378">
<p align="left">Ressort des tribunaux d'instance de Paris 1er arrondissement, Paris 2e arrondissement, Paris 3e arrondissement, Paris 4e arrondissement, Paris 5e arrondissement, Paris 6e arrondissement, Paris 7e arrondissement, Paris 8e arrondissement, Paris 9e arrondissement, Paris 10e arrondissement, Paris 11e arrondissement, Paris 12e arrondissement, Paris 13e arrondissement, Paris 14e arrondissement, Paris 15e arrondissement, Paris 16e arrondissement, Paris 17e arrondissement, Paris 18e arrondissement, Paris 19e arrondissement et Paris 20e arrondissement </p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Seine-Saint-Denis</p>
</td>
</tr>
<tr>
<td valign="top" width="151">
<p align="left">Bobigny </p>
</td>
<td valign="top" width="151">
<p align="left">Bobigny </p>
</td>
<td valign="top" width="378">
<p align="left">Ressort des tribunaux d'instance d'Aubervilliers, Aulnay-sous-Bois, Bobigny, Le Raincy, Montreuil, Pantin, Saint-Denis et Saint-Ouen </p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Val-de-Marne</p>
</td>
</tr>
<tr>
<td valign="top" width="151">
<p align="left">Créteil </p>
</td>
<td valign="top" width="151">
<p align="left">Villejuif </p>
</td>
<td valign="top" width="378">
<p align="left">Ressort des tribunaux d'instance de Sucy-en-Brie, Charenton-le-Pont, Ivry-sur-Seine, Nogent-sur-Marne, Saint-Maur-des-Fossés et Villejuif</p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Cour d'appel de Reims</p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Ardennes</p>
</td>
</tr>
<tr>
<td valign="top" width="151">
<p align="left">Charleville-Mézières </p>
</td>
<td valign="top" width="151">
<p align="left">Charleville-Mézières </p>
</td>
<td valign="top" width="378">
<p align="left">Ressort des tribunaux d'instance de Charleville-Mézières et Sedan </p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Cour d'appel de Toulouse</p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Ariège</p>
</td>
</tr>
<tr>
<td valign="top" width="151">
<p align="left">Foix </p>
</td>
<td valign="top" width="151">
<p align="left">Foix </p>
</td>
<td valign="top" width="378">
<p align="left">Ressort des tribunaux d'instance de Foix et Saint-Girons </p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Tarn-et-Garonne</p>
</td>
</tr>
<tr>
<td valign="top" width="151">
<p align="left">Montauban </p>
</td>
<td valign="top" width="151">
<p align="left">Montauban </p>
</td>
<td valign="top" width="378">
<p align="left">Ressort des tribunaux d'instance de Castelsarrasin et Montauban </p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Cour d'appel de Versailles</p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Hauts-de-Seine</p>
</td>
</tr>
<tr>
<td valign="top" width="151">
<p align="left">Nanterre </p>
</td>
<td valign="top" width="151">
<p align="left">Asnières-sur-Seine </p>
</td>
<td valign="top" width="378">
<p align="left">Ressort des tribunaux d'instance d'Antony, Asnières-sur-Seine, Boulogne-Billancourt, Colombes, Courbevoie, Puteaux et Vanves </p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Val-d'Oise</p>
</td>
</tr>
<tr>
<td valign="top" width="151">
<p align="left">Pontoise </p>
</td>
<td valign="top" width="151">
<p align="left">Pontoise </p>
</td>
<td valign="top" width="378">
<p align="left">Ressort des tribunaux d'instance de Gonesse, Montmorency, Pontoise et Sannois </p>
</td>
</tr>
<tr>
<td colspan="3" width="680">
<p align="center">Yvelines</p>
</td>
</tr>
<tr>
<td valign="top" width="151">
<p align="left">Versailles </p>
</td>
<td valign="top" width="151">
<p align="left">Versailles </p>
</td>
<td valign="top" width="378">
<p align="left">Ressort des tribunaux d'instance de Mantes-la-Jolie, Poissy, Rambouillet et Versailles</p>
</td>
</tr>
</tbody>
</table>
