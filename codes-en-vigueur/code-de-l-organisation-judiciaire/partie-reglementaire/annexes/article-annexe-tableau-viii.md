# Article Annexe Tableau VIII

SIÈGE ET RESSORT DES TRIBUNAUX DE GRANDE INSTANCE ET DE PREMIÈRE INSTANCE COMPÉTENTS POUR CONNAÎTRE DES CONTESTATIONS SUR LA NATIONALITÉ FRANÇAISE OU ÉTRANGÈRE DES PERSONNES PHYSIQUES (ANNEXE DE L'ARTICLE D. 211-10)

<table>
<tbody>
<tr>
<td align="center">
<p align="center">SIÈGE </p>
</td>
<td align="center">RESSORT </td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel d'Aix-en-Provence </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Marseille. <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort des cours d'appel d'Aix-en-Provence, Bastia, Montpellier et Nîmes. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Bordeaux </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Bordeaux.</p>
</td>
<td align="center">
<p align="left">Ressort des cours d'appel d'Agen, Bordeaux, Limoges, Pau et Toulouse. </p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Douai </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Lille.</p>
</td>
<td align="center">
<p align="left">Ressort des cours d'appel d'Amiens, Douai, Reims et Rouen.</p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">Cour d'appel de Cayenne</p>
</td>
</tr>
<tr>
<td>
<p align="left">Cayenne</p>
</td>
<td>
<p align="left">Ressort de la cour d'appel de Cayenne.</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Fort-de-France </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Fort-de-France.</p>
</td>
<td align="center">
<p align="left">Ressort des cours d'appel de Basse-Terre et de Fort-de-France.</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p>Cour d'appel de Lyon </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Lyon.</p>
</td>
<td align="center">
<p align="left">Ressort des cours d'appel de Chambéry, Grenoble, Lyon et Riom.</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p>Cour d'appel de Nancy </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Nancy.</p>
</td>
<td align="center">
<p align="left">Ressort des cours d'appel de Besançon, Colmar, Dijon, Metz et Nancy.</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p>Cour d'appel de Nouméa </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Nouméa.</p>
</td>
<td align="center">
<p align="left">Ressort du tribunal de première instance de Nouméa.</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Mata-Utu.</p>
</td>
<td align="center">
<p align="left">Ressort du tribunal de première instance de Mata-Utu.</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p>Cour d'appel de Papeete </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Papeete.</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel.</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p>Cour d'appel de Paris </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Paris.</p>
</td>
<td align="center">
<p align="left">Ressort des cours d'appel de Bourges, Orléans, Paris et Versailles.</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p>Cour d'appel de Rennes </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Nantes.</p>
</td>
<td align="center">
<p align="left">Ressort des cours d'appel d'Angers, Caen, Poitiers et Rennes.</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p>Cour d'appel de Saint-Denis </p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">La Réunion </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Saint-Denis.</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel, à l'exception du ressort du tribunal de grande instance de Mamoudzou </p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">Mayotte <br/>
</p>
</td>
</tr>
<tr>
<td>
<p align="left">Mamoudzou.</p>
</td>
<td>Ressort du tribunal de grande instance de Mamoudzou </td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Tribunal supérieur d'appel de Saint-Pierre </p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">Saint-Pierre-et-Miquelon </p>
</td>
</tr>
<tr>
<td>Saint-Pierre.</td>
<td>
<p align="left">Ressort du tribunal supérieur d'appel.</p>
</td>
</tr>
</tbody>
</table>
