# Article Annexe Tableau VIII-I

Siège et ressort des tribunaux de grande instance et des tribunaux de première instance compétents pour connaître des actions aux fins d'adoption ainsi que des actions aux fins de reconnaissance des jugements d'adoption rendus à l'étranger, lorsque l'enfant résidant habituellement à l'étranger a été, est ou doit être déplacé vers la France

(annexe de l'article D. 211-10-1)

<table>
<tbody>
<tr>
<td align="center">
<p align="center">SIÈGE </p>
</td>
<td align="center" colspan="6">
<p align="center">RESSORT </p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<br/>Cour d'appel d'Agen <br/>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Agen <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel d'Agen. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<br/>Cour d'appel d'Aix-en-Provence <br/>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Marseille <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel d'Aix-en-Provence. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel d'Amiens</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Amiens <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel d'Amiens. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel d'Angers </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Angers <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel d'Angers. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Basse-Terre </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Basse-Terre <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Basse-Terre. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<br/>Cour d'appel de Bastia <br/>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Bastia <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Bastia. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Besançon </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Besançon <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Besançon. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Bordeaux</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Bordeaux <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Bordeaux. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Bourges</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Bourges <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Bourges. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Caen</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Caen <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Caen. <br/>
</p>
</td>
</tr>
<tr>
<td colspan="2">
<p align="center">Cour d'appel de Cayenne</p>
</td>
</tr>
<tr>
<td>
<p align="left">Cayenne</p>
</td>
<td>
<p align="left">Ressort de la cour d'appel de Cayenne.</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Chambéry</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Chambéry <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Chambéry. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Colmar</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Strasbourg <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Colmar. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Dijon</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Dijon <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Dijon. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Douai</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Lille <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Douai. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Fort-de-France</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Fort-de-France <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Fort-de-France. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<br/>Cour d'appel de Grenoble <br/>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Grenoble <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Grenoble. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<br/>Cour d'appel de Limoges <br/>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Limoges <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Limoges. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Lyon</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Lyon <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Lyon. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Metz</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Metz <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Metz. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Montpellier</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Montpellier <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Montpellier. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Nancy</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Nancy <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Nancy. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Nîmes</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Nîmes <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Nîmes. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<br/>Cour d'appel de Nouméa <br/>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Nouméa <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Nouméa. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<br/>Cour d'appel d'Orléans <br/>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Orléans <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel d'Orléans. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<br/>Cour d'appel de Papeete <br/>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Papeete <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Papeete. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Paris</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Paris <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Paris. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Pau</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Pau <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Pau. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Poitiers</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Poitiers <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Poitiers. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Reims</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Reims <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Reims. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Rennes</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Nantes <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Rennes. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<br/>Cour d'appel de Riom <br/>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Clermont-Ferrand <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Riom. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<br/>Cour d'appel de Rouen <br/>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Rouen <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Rouen. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Saint-Denis </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Saint-Denis <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Saint-Denis. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<br/>Cour d'appel de Toulouse <br/>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Toulouse <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Toulouse. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Cour d'appel de Versailles</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Nanterre <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort de la cour d'appel de Versailles. <br/>
</p>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<p align="center">Tribunal supérieur d'appel de Saint-Pierre</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">Saint-Pierre <br/>
</p>
</td>
<td align="center">
<p align="left">Ressort du tribunal supérieur d'appel de Saint-Pierre.<br/>
</p>
</td>
</tr>
</tbody>
</table>
