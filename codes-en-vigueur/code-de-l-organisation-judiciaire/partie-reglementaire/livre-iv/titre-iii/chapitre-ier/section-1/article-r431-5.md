# Article R431-5

A l'audience de la chambre, au moins cinq de ses membres ayant voix délibérative sont présents.
