# Article R431-6

A l'audience d'une chambre, si, par l'effet des absences ou des empêchements, le nombre des membres ayant voix délibérative est inférieur à cinq, il peut être fait appel, en suivant l'ordre du rang, à des conseillers appartenant à d'autres chambres.
