# Sous-section 1 : Institution et compétence

- [Article D532-2](article-d532-2.md)
- [Article D532-5](article-d532-5.md)
- [Article D532-7](article-d532-7.md)
- [Article R532-3](article-r532-3.md)
- [Article R532-4](article-r532-4.md)
- [Article R532-6](article-r532-6.md)
