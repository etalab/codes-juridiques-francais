# Section 2 : La cour d'appel

- [Sous-section 1 : Institution et compétence](sous-section-1)
- [Sous-section 2 : Organisation et fonctionnement](sous-section-2)
