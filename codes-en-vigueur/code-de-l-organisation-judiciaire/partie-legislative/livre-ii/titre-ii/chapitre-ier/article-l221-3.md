# Article L221-3

Au sein du tribunal d'instance, un ou plusieurs juges exercent les fonctions de juge des tutelles des majeurs.
