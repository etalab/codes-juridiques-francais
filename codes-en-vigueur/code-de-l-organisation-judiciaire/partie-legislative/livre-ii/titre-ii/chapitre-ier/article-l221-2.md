# Article L221-2

Il y a au moins un tribunal d'instance dans le ressort de chaque cour d'appel.
