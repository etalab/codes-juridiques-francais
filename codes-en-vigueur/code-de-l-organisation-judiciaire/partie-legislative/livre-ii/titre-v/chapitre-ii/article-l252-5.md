# Article L252-5

En matière pénale, le juge des enfants connaît, dans les conditions définies par l'ordonnance n° 45-174 du 2 février 1945 relative à l'enfance délinquante, des délits et des contraventions de cinquième classe commis par les mineurs.
