# Chapitre Ier bis : Le tribunal correctionnel pour mineurs

- [Article L251-7](article-l251-7.md)
- [Article L251-8](article-l251-8.md)
