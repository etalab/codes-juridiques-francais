# Article L212-2

Lorsqu'une affaire, compte tenu de l'objet du litige ou de la nature des questions à juger, est portée devant le tribunal de grande instance statuant à juge unique, le renvoi à la formation collégiale est de droit, sur la demande non motivée d'une des parties formulée selon les modalités et délais fixés par décret en Conseil d'Etat.

Toutefois, les dispositions du présent article ne sont pas applicables en matières disciplinaires ou relatives à l'état des personnes, sous réserve des dispositions particulières aux matières de la compétence du juge aux affaires familiales.
