# Article L217-3

Par dérogation à l'article L. 122-4, le procureur de la République financier et ses substituts n'exercent les fonctions de ministère public que pour les affaires relevant de leurs attributions.
