# Article L213-5

Les fonctions de juge de l'exécution sont exercées par le président du tribunal de grande instance.

Lorsqu'il délègue ces fonctions à un ou plusieurs juges, le président du tribunal de grande instance fixe la durée et l'étendue territoriale de cette délégation.
