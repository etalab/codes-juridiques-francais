# Article L561-1

Le livre Ier du présent code et les dispositions du 3° de l'article L. 261-1 sont applicables en Nouvelle-Calédonie.
