# Chapitre Ier : Dispositions générales

- [Article L561-1](article-l561-1.md)
- [Article L561-2](article-l561-2.md)
