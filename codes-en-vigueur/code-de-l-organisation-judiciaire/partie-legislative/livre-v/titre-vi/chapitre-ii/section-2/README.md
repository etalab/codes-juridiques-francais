# Section 2 : La cour d'appel

- [Article L562-25](article-l562-25.md)
- [Article L562-26](article-l562-26.md)
- [Article L562-27](article-l562-27.md)
- [Article L562-28](article-l562-28.md)
