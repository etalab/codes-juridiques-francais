# Article L562-25

Les dispositions des articles L. 311-1, L. 311-3, L. 312-2 et L. 312-7 relatives à la cour d'appel sont applicables en Nouvelle-Calédonie.

Les dispositions des articles L. 311-9 et L. 312-6 relatives à la protection de l'enfance sont applicables en Nouvelle-Calédonie.
