# Sous-section 2 : Dispositions particulières à certaines formations

- [Article L312-4](article-l312-4.md)
- [Article L312-6](article-l312-6.md)
- [Article L312-6-1](article-l312-6-1.md)
