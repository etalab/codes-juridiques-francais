# Article L311-5

La cour d'appel connaît, en ce qui concerne la discipline   des commissaires-priseurs judiciaires, des huissiers de justice et des notaires, des recours contre les décisions de la chambre de discipline.
