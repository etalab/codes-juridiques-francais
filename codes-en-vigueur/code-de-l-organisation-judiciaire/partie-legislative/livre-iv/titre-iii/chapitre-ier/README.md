# Chapitre Ier : Les chambres de la cour

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Dispositions particulières aux chambres mixtes et à l'assemblée plénière](section-2)
