# Article L121-7

Chaque année, le magistrat chargé de la direction et de l'administration du tribunal d'instance organise par ordonnance le service dont les juges de proximité sont chargés au sein de ce tribunal, en tenant compte de celui auquel ils sont astreints au tribunal de grande instance.
