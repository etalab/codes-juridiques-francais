# Article R5112-3

La demande mentionnée à l'article R. 5112-2 comporte :

1° Le projet descriptif et le programme de l'opération ;

2° Une copie de la délibération du conseil municipal ou de la décision de l'organe délibérant de l'organisme ayant pour objet la réalisation d'opérations d'habitat social, sollicitant de l'Etat l'acquisition du terrain ;

3° Un plan de situation du terrain, établi par un géomètre expert ou par une personne remplissant les conditions prévues à l'article 30 de la loi n° 46-942 du 7 mai 1946 modifiée instituant l'ordre des géomètres experts. Ce plan mentionne la surface sur laquelle porte la demande. Il peut être établi un plan de situation commun à plusieurs demandes de cession. Chaque demande comporte une copie de ce plan ;

4° Des extraits du règlement du plan d'occupation des sols ou du plan local d'urbanisme de la commune mis en conformité avec les dispositions des articles L. 156-3 et L. 156-4 du code de l'urbanisme, se rapportant à la zone où est situé le terrain dont la cession est demandée.
