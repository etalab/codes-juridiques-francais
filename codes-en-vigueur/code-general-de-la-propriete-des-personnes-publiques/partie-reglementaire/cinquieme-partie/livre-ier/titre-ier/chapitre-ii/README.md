# Chapitre II : Dispositions particulières à la Guadeloupe et à la Martinique

- [Section 1 : Délimitation des espaces urbains et naturels](section-1)
- [Section 2 : Cession de terrains prévue par l'article L. 5112-4](section-2)
- [Section 3 : Cession de terrains prévue par l'article L. 5112-4-1](section-3)
- [Section 4 : Cession de terrains prévue par l'article L. 5112-5](section-4)
- [Section 5 : Cession de terrains prévue par l'article L. 5112-6](section-5)
- [Section 6 : Dispositions relatives à l'application de l'article L. 5112-9](section-6)
- [Section 7 : Dispositions relatives à l'application de l'article L. 5112-3](section-7)
