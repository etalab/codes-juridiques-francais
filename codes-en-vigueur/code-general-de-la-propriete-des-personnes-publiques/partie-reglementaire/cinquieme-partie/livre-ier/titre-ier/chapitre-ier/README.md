# Chapitre Ier : Dispositions communes

- [Article D5111-6](article-d5111-6.md)
- [Article R5111-1](article-r5111-1.md)
- [Article R5111-2](article-r5111-2.md)
- [Article R5111-3](article-r5111-3.md)
- [Article R5111-4](article-r5111-4.md)
- [Article R5111-5](article-r5111-5.md)
- [Article R5111-7](article-r5111-7.md)
- [Article R5111-8](article-r5111-8.md)
- [Article R5111-9](article-r5111-9.md)
- [Article R5111-10](article-r5111-10.md)
