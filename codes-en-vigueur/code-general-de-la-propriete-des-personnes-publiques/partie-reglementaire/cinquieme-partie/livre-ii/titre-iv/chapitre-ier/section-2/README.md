# Section 2 : Cessions à titre gratuit

- [Article R5241-6](article-r5241-6.md)
- [Article R5241-7](article-r5241-7.md)
- [Article R5241-8](article-r5241-8.md)
- [Article R5241-9](article-r5241-9.md)
