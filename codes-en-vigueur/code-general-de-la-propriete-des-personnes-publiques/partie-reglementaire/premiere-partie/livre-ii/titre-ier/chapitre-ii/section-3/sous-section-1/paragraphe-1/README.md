# Paragraphe 1 : Dispositions générales

- [Article R1212-9](article-r1212-9.md)
- [Article R1212-10](article-r1212-10.md)
- [Article R1212-11](article-r1212-11.md)
- [Article R1212-12](article-r1212-12.md)
- [Article R1212-13](article-r1212-13.md)
- [Article R1212-14](article-r1212-14.md)
- [Article R1212-15](article-r1212-15.md)
- [Article R1212-16](article-r1212-16.md)
- [Article R1212-17](article-r1212-17.md)
- [Article R1212-18](article-r1212-18.md)
