# Chapitre III : Biens sans maître

- [Article R1123-1](article-r1123-1.md)
- [Article R1123-2](article-r1123-2.md)
