# Section 2 : Dispositions applicables aux collectivités territoriales  et à leurs établissements publics

- [Article R1121-6](article-r1121-6.md)
- [Article R1121-7](article-r1121-7.md)
- [Article R1121-8](article-r1121-8.md)
