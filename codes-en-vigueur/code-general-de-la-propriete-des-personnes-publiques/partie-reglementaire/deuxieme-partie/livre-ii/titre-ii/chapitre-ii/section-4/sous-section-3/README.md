# Sous-section 3 : Dispositions communes à l'Etat et à ses établissements publics

- [Article R2222-30](article-r2222-30.md)
- [Article R2222-31](article-r2222-31.md)
- [Article R2222-32](article-r2222-32.md)
