# Article R2222-4-1

Sans préjudice des dispositions de l'article R. 2222-18, les immeubles du domaine privé de l'Etat peuvent faire l'objet de baux en vue de fournir un logement à ses agents civils et militaires sans que l'occupation de ce logement ne soit liée à des considérations de service. Dans ce cas, un loyer est mis à la charge de l'agent. Il est égal, sauf disposition spéciale liée à l'usage social de l'immeuble, à la valeur locative réelle des locaux occupés, déduction faite d'un abattement de 15 % destiné à tenir compte de la précarité de l'occupation mentionnée dans le bail.
