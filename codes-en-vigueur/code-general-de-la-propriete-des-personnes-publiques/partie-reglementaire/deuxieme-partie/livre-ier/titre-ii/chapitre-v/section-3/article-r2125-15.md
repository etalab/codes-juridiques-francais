# Article R2125-15

Dans le cas où l'occupation d'une dépendance du domaine public de l'Etat comprise dans les limites administratives d'un port relevant de la compétence du département, mis à sa disposition ou ayant fait l'objet à son profit d'un transfert de gestion, est constitutive d'un droit réel au profit du titulaire de l'autorisation, la redevance est, sous réserve des règlements particuliers, fixée par le président du conseil départemental en application des règles définies par le conseil départemental.
