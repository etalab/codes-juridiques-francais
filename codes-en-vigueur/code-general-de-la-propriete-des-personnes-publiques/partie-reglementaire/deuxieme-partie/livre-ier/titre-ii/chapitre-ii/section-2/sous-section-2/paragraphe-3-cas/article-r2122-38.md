# Article R2122-38

Par dérogation aux articles R. 2122-36, R. 2122-37 et R. 2122-41 à R. 2122-43, la convention de bail peut être librement négociée par l'Etat dans les conditions définies aux articles R. 2122-39 et R. 2122-40, à condition que l'avis de publicité ait mentionné les critères d'attribution du bail, le nombre minimal de candidats au moins égal à trois, que l'Etat prévoit d'inviter à soumissionner et, le cas échéant, le nombre maximal.

Les critères d'attribution sont pondérés. Si l'autorité administrative démontre qu'une telle pondération est objectivement impossible, ils sont alors hiérarchisés.
