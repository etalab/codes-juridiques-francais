# Article R2122-49

Postérieurement à la conclusion de la convention de bail, l'autorité administrative peut, en recourant à la procédure négociée sans publicité préalable, confier au bailleur la réalisation de travaux ou prestations complémentaires qui ne figuraient pas dans le projet initialement envisagé ni dans le contrat initial et qui, à la suite d'une circonstance imprévue, sont devenus nécessaires au parfait achèvement de l'ouvrage ou du service tel qu'il est décrit dans le contrat initial :

1° Lorsque ces travaux ou prestations complémentaires ne peuvent être techniquement ou économiquement séparés du contrat initial sans inconvénient majeur pour l'autorité administrative ;

2° Ou lorsque ces travaux ou prestations, quoique séparables de l'exécution du contrat initial, sont néanmoins strictement nécessaires à la réalisation de son objet.
