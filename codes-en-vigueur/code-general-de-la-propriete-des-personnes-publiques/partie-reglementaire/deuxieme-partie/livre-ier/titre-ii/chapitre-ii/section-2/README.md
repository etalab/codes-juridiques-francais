# Section 2 : Règles particulières à certaines occupations

- [Sous-section 1 : Dispositions communes](sous-section-1)
- [Sous-section 2 : Règles particulières à certaines opérations de construction](sous-section-2)
- [Sous-section 3 : Règles particulières au domaine public de l'Etat compris dans les limites administratives des ports relevant de la compétence des collectivités territoriales](sous-section-3)
- [Sous-section 4 : Règles particulières aux titres en cours](sous-section-4)
- [Article R2122-9](article-r2122-9.md)
