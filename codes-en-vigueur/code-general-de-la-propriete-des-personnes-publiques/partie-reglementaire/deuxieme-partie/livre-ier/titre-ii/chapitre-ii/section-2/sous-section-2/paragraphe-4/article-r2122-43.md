# Article R2122-43

La personne publique établit la liste des candidats admis à participer au dialogue en application des critères de sélection des candidatures mentionnés dans l'avis d'appel public à la concurrence, selon les règles fixées aux articles 43 à 45, 51 et 52 du code des marchés publics.

Le nombre de ces candidats ne peut être inférieur à trois sous réserve d'un nombre suffisant de candidats.
