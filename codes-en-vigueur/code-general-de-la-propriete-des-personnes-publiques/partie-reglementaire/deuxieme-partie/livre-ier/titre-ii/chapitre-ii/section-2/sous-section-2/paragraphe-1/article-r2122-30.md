# Article R2122-30

Tout projet de bail d'un loyer annuel supérieur à un million d'euros hors taxes conclu sur le fondement de l'article L. 2122-15 est soumis à la réalisation de l'évaluation préalable prévue à l'article 48 de la loi n° 2008-735 du 28 juillet 2008 relative aux contrats de partenariat.
