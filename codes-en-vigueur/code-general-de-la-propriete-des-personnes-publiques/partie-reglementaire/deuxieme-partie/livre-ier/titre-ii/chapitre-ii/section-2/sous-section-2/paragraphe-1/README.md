# Paragraphe 1 : Dispositions générales

- [Article R2122-28](article-r2122-28.md)
- [Article R2122-29](article-r2122-29.md)
- [Article R2122-30](article-r2122-30.md)
- [Article R2122-30-1](article-r2122-30-1.md)
- [Article R2122-31](article-r2122-31.md)
- [Article R2122-32](article-r2122-32.md)
- [Article R2122-33](article-r2122-33.md)
