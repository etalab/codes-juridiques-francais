# Article R2122-37

La convention de bail est conclue avec le candidat ayant présenté l'offre économiquement la plus avantageuse au regard des critères figurant dans l'avis d'appel public à la concurrence, en conformité avec les I, II et III de l'article 53 et l'article 55 du code des marchés publics.
