# Article R2122-41

Si, compte tenu de la complexité du projet, l'autorité administrative est objectivement dans l'impossibilité de définir les moyens techniques pouvant répondre à ses besoins ou d'établir le montage juridique ou financier du projet, la procédure du dialogue compétitif peut être utilisée pour passer la convention de bail.
