# Paragraphe 2 : Procédure de passation de droit commun

- [Article R2122-34](article-r2122-34.md)
- [Article R2122-35](article-r2122-35.md)
- [Article R2122-36](article-r2122-36.md)
- [Article R2122-37](article-r2122-37.md)
