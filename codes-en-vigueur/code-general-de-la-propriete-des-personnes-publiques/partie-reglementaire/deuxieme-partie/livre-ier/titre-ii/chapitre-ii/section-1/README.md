# Section 1 : Règles générales d'occupation

- [Article R2122-1](article-r2122-1.md)
- [Article R2122-2](article-r2122-2.md)
- [Article R2122-3](article-r2122-3.md)
- [Article R2122-4](article-r2122-4.md)
- [Article R2122-5](article-r2122-5.md)
- [Article R2122-6](article-r2122-6.md)
- [Article R2122-7](article-r2122-7.md)
- [Article R2122-8](article-r2122-8.md)
