# Chapitre II : Implantation et inventaire

- [Section 1 : Biens situés à l'étranger](section-1)
- [Section 2 : Biens situés en France](section-2)
- [Section 3 : Inventaire](section-3)
- [Section 4 : Délégations et représentations](section-4)
