# Section 1 : Biens situés à l'étranger

- [Article D2312-3](article-d2312-3.md)
- [Article R2312-1](article-r2312-1.md)
- [Article R2312-2](article-r2312-2.md)
