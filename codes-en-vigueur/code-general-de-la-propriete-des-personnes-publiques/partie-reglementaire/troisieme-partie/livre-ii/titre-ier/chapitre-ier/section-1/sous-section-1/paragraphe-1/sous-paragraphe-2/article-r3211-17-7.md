# Article R3211-17-7

<div align="left">Le président de la commission est nommé par arrêté des ministres chargés du logement et de l'urbanisme. </div>
