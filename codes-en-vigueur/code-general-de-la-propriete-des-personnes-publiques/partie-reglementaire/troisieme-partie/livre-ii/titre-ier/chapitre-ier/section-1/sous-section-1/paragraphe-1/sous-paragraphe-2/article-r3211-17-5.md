# Article R3211-17-5

<div align="left">L'organisation et le fonctionnement de la Commission nationale de l'aménagement, de l'urbanisme et du foncier, mentionnée au VII de l'article L. 3211-7, relèvent du décret n° 2006-672 du 8 juin 2006 relatif à la création, à la composition et au fonctionnement de commissions administratives à caractère consultatif.</div>
