# Paragraphe 2 : Dispositions applicables aux établissements publics de l'Etat

- [Article R3211-31](article-r3211-31.md)
- [Article R3211-32](article-r3211-32.md)
- [Article R3211-32-1](article-r3211-32-1.md)
- [Article R3211-32-2](article-r3211-32-2.md)
- [Article R3211-32-3](article-r3211-32-3.md)
- [Article R3211-32-4](article-r3211-32-4.md)
- [Article R3211-32-5](article-r3211-32-5.md)
- [Article R3211-32-6](article-r3211-32-6.md)
- [Article R3211-32-7](article-r3211-32-7.md)
- [Article R3211-32-8](article-r3211-32-8.md)
- [Article R3211-32-9](article-r3211-32-9.md)
