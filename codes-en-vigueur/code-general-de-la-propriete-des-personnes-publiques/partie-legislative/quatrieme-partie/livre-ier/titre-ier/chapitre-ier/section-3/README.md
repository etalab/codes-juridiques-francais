# Section 3 : Réception et authentification des actes.

- [Article L4111-3](article-l4111-3.md)
- [Article L4111-4](article-l4111-4.md)
- [Article L4111-5](article-l4111-5.md)
- [Article L4111-6](article-l4111-6.md)
