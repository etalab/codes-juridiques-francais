# Article L1112-8

Le droit de préemption de la Bibliothèque nationale de France à l'égard des archives privées est exercé dans les conditions fixées à la sous-section 3 de la section 2 du chapitre 2 du titre Ier du livre II du code du patrimoine.
