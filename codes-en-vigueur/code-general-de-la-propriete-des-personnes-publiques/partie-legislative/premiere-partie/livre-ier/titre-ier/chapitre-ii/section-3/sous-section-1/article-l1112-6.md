# Article L1112-6

Le droit de préemption des collectivités territoriales, de leurs groupements et de leurs établissements publics est exercé dans les conditions fixées :

1° Aux chapitres II et III du titre IV du livre Ier du code de l'urbanisme, en ce qui concerne les espaces naturels sensibles des départements et la protection et la mise en valeur des espaces agricoles et naturels périurbains ;

2° Aux chapitres Ier, II et III du titre Ier du livre II du code de l'urbanisme, en ce qui concerne le droit de préemption urbain, les zones d'aménagement différé et les périmètres provisoires ;

3° Au chapitre IV du titre Ier du livre II du code de l'urbanisme, en ce qui concerne les fonds artisanaux, les fonds de commerce et les baux commerciaux.

Le droit de préemption des établissements publics fonciers locaux est exercé dans les conditions fixées au chapitre IV du titre II du livre III du code de l'urbanisme.
