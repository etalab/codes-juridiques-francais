# Section 3 : Droit de préemption.

- [Sous-section 1 : Droit de préemption immobilier.](sous-section-1)
- [Sous-section 2 : Droit de préemption mobilier.](sous-section-2)
- [Article L1112-3](article-l1112-3.md)
