# Section 2 : Modalités d'acquisition.

- [Article L1123-2](article-l1123-2.md)
- [Article L1123-3](article-l1123-3.md)
- [Article L1123-4](article-l1123-4.md)
