# Section 2 : Dispositions applicables aux collectivités territoriales, à leurs groupements et à leurs établissements publics.

- [Article L1211-1](article-l1211-1.md)
- [Article L1211-2](article-l1211-2.md)
