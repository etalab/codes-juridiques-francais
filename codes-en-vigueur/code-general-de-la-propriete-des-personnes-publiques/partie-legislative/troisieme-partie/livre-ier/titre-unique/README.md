# TITRE UNIQUE : INALIÉNABILITÉ ET IMPRESCRIPTIBILITÉ

- [Chapitre Ier : Principe.](chapitre-ier)
- [Chapitre II : Dérogations.](chapitre-ii)
- [Chapitre III : Transfert de propriété du domaine public fluvial.](chapitre-iii)
- [Chapitre IV : Transfert de propriété du domaine public ferroviaire](chapitre-iv)
