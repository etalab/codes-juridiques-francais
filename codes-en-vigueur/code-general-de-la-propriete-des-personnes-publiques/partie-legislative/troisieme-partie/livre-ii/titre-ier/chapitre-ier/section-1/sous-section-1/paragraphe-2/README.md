# Paragraphe 2 : Dispositions applicables aux établissements publics de l'Etat, aux sociétés détenues par l'Etat et à leurs filiales appartenant au secteur public.

- [Article L3211-13](article-l3211-13.md)
- [Article L3211-13-1](article-l3211-13-1.md)
