# Paragraphe 4 : Dispositions communes à l'Etat, aux collectivités territoriales et à leurs groupements.

- [Article L3211-15](article-l3211-15.md)
- [Article L3211-16](article-l3211-16.md)
