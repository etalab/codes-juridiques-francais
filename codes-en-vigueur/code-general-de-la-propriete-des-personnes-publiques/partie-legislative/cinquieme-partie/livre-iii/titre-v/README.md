# TITRE V : AUTRES OPÉRATIONS IMMOBILIÈRES DES PERSONNES PUBLIQUES

- [Chapitre Ier : Prises à bail](chapitre-ier)
- [Chapitre II : Réception et authentification des actes.](chapitre-ii)
- [Chapitre III : Dispositions applicables aux biens détenus en jouissance par l'Etat.](chapitre-iii)
