# Chapitre III : Dispositions applicables aux biens détenus en jouissance par l'Etat.

- [Article L5353-1](article-l5353-1.md)
