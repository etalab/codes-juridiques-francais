# Article L5342-4

Les objets mobiliers ou matériels détenus à un titre quelconque par l'Etat, la collectivité départementale, les communes, leurs groupements ainsi que leurs établissements publics doivent être remis à l'autorité compétente, aux fins d'aliénation, lorsque ces personnes n'en ont plus l'emploi ou en ont décidé la vente, à moins qu'il n'en soit disposé autrement par des lois particulières.

Toutefois, cette obligation de remise ne s'applique pas aux biens mobiliers compris dans des marchés :

1° Ayant pour but le façonnage de matières neuves non précédemment employées ;

2° Ou tendant à la réparation ou à une meilleure utilisation, sous la même forme, des objets en service.
