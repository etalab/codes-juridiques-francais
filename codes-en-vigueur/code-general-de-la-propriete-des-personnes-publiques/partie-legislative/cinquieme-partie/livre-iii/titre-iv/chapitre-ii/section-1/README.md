# Section 1 : Modes de cession

- [Sous-section 1 : Cessions à titre onéreux](sous-section-1)
- [Sous-section 2 : Cessions à titre gratuit.](sous-section-2)
