# Sous-section 1 : Cessions à titre onéreux

- [Paragraphe 1 : Vente](paragraphe-1)
- [Paragraphe 2 : Autres modes.](paragraphe-2)
