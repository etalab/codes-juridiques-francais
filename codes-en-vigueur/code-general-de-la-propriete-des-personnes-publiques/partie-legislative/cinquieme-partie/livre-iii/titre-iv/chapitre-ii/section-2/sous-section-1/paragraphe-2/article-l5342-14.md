# Article L5342-14

Le produit des ventes est porté en recette au budget de la personne anciennement propriétaire, à moins de dispositions légales contraires, sous déduction, le cas échéant, des frais d'administration, de vente et de perception, perçus au profit de la collectivité départementale de Mayotte en application de l'article L. 5333-3.
