# Article L5342-16

Pour l'application de l'article L. 3222-2, les références aux articles L. 4221-4, L. 5211-37 et L. 5722-3 du code général des collectivités territoriales sont supprimées.
