# Sous-section 1 : Gestion et superposition d'affectations.

- [Article L5331-10](article-l5331-10.md)
- [Article L5331-11](article-l5331-11.md)
