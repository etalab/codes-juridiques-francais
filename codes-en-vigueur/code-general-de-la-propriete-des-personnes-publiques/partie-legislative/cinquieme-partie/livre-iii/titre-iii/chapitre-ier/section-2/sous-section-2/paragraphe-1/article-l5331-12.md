# Article L5331-12

L'article L. 2124-1 est ainsi modifié :

1° Les références aux articles L. 123-1 à L. 123-6 sont remplacées par la référence à l'article L. 651-3 ;

2° Il est ajouté un troisième alinéa ainsi rédigé :

" Le changement d'utilisation est également soumis pour avis au service technique chargé de l'environnement. "
