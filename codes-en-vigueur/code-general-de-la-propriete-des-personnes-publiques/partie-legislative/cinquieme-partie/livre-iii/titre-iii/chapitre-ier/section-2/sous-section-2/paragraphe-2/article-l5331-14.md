# Article L5331-14

L'autorité compétente peut concéder, aux conditions qu'elle aura fixées, le droit d'endigage, les accrues, atterrissements et alluvions des fleuves et rivières faisant partie du domaine public fluvial.
