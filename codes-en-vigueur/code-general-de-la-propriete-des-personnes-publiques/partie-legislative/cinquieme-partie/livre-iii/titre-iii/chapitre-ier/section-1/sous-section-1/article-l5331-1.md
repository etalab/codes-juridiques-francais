# Article L5331-1

Au second alinéa de l'article L. 2111-3, les mots : " selon les procédures fixées par les autorités compétentes " sont remplacés par les mots : " dans les conditions fixées par décret en Conseil d'Etat ".
