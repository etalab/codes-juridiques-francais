# Article L5331-2

Le domaine public maritime de l'Etat comprend :

1° Le sol et le sous-sol de la mer entre la limite extérieure de la mer territoriale et, côté terre, le rivage de la mer.

Le rivage de la mer est constitué par tout ce qu'elle couvre et découvre jusqu'où les plus hautes mers peuvent s'étendre en l'absence de perturbations météorologiques exceptionnelles ;

2° Les lais et relais de la mer ;

3° Les terrains soustraits artificiellement à l'action du flot ;

4° La zone bordant le littoral définie à l'article L. 5331-5.
