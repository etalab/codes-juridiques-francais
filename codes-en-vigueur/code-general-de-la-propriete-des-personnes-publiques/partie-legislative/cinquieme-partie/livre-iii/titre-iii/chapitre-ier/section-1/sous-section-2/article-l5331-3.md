# Article L5331-3

Les deux derniers alinéas de l'article L. 2111-5 sont supprimés.
