# Chapitre II : Biens relevant du domaine privé

- [Section 1 : Location, mise à disposition et affectation](section-1)
- [Section 2 : Concessions de logement.](section-2)
- [Section 3 : Concessions et locations de terres en vue de leur mise en valeur agricole.](section-3)
- [Section 4 : Restitution de biens.](section-4)
