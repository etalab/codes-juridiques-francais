# Section 2 : Acquisitions à titre gratuit

- [Sous-section 1 : Dons et legs.](sous-section-1)
- [Sous-section 2 : Biens sans maître et présumés sans maître](sous-section-2)
- [Sous-section 3 : Sommes et valeurs prescrites.](sous-section-3)
