# Chapitre Ier : Modes d'acquisition

- [Section 1 : Acquisitions à titre onéreux.](section-1)
- [Section 2 : Acquisitions à titre gratuit](section-2)
