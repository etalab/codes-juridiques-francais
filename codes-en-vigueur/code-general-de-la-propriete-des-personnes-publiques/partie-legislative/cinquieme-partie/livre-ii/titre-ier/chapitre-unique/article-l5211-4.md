# Article L5211-4

Pour l'application du présent code, les termes énumérés ci-après sont ainsi remplacés :

1° " Département " par " collectivité territoriale " ;

2° " Tribunal de grande instance " par " tribunal de première instance ".
