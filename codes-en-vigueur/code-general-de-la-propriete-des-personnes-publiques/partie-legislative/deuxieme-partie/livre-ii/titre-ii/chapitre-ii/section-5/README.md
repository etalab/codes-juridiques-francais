# Section 5 : Sommes et valeurs prescrites.

- [Article L2222-21](article-l2222-21.md)
- [Article L2222-22](article-l2222-22.md)
