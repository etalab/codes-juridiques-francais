# Sous-section 2 : Concessions de logement dans les immeubles appartenant aux collectivités territoriales, à leurs groupements et à leurs établissements publics.

- [Article L2222-11](article-l2222-11.md)
