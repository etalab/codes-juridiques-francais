# Section 7 : Utilisation du domaine public dans le cadre de l'exploitation de certaines activités commerciales

- [Article L2124-32-1](article-l2124-32-1.md)
- [Article L2124-33](article-l2124-33.md)
- [Article L2124-34](article-l2124-34.md)
- [Article L2124-35](article-l2124-35.md)
