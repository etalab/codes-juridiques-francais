# Section 4 : Exploitation des ressources naturelles.

- [Article L2124-27](article-l2124-27.md)
- [Article L2124-28](article-l2124-28.md)
- [Article L2124-29](article-l2124-29.md)
- [Article L2124-30](article-l2124-30.md)
