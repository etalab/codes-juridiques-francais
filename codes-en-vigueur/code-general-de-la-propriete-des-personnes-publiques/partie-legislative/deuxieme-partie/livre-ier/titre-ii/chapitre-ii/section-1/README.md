# Section 1 : Règles générales d'occupation.

- [Article L2122-1](article-l2122-1.md)
- [Article L2122-2](article-l2122-2.md)
- [Article L2122-3](article-l2122-3.md)
- [Article L2122-4](article-l2122-4.md)
