# Article L2122-7

Le droit réel conféré par le titre, les ouvrages, constructions et installations de caractère immobilier ne peuvent être cédés, ou transmis dans le cadre de mutations entre vifs ou de fusion, absorption ou scission de sociétés, pour la durée de validité du titre restant à courir, y compris dans le cas de réalisation de la sûreté portant sur lesdits droits et biens et dans les cas mentionnés aux premier et deuxième alinéas de l'article L. 2122-8, qu'à une personne agréée par l'autorité compétente, en vue d'une utilisation compatible avec l'affectation du domaine public occupé.

Lors du décès d'une personne physique titulaire d'un titre d'occupation constitutif de droit réel, celui-ci peut être transmis, dans les conditions mentionnées à l'alinéa précédent, au conjoint survivant ou aux héritiers sous réserve que le bénéficiaire, désigné par accord entre eux, soit présenté à l'agrément de l'autorité compétente dans un délai de six mois à compter du décès.
