# Sous-section 5 : Régime général des peines.

- [Article L2132-26](article-l2132-26.md)
- [Article L2132-27](article-l2132-27.md)
- [Article L2132-28](article-l2132-28.md)
