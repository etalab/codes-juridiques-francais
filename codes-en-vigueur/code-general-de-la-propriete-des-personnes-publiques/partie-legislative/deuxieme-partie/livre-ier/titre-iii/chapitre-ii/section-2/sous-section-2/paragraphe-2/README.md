# Paragraphe 2 : Domaine public fluvial.

- [Article L2132-5](article-l2132-5.md)
- [Article L2132-6](article-l2132-6.md)
- [Article L2132-7](article-l2132-7.md)
- [Article L2132-8](article-l2132-8.md)
- [Article L2132-9](article-l2132-9.md)
- [Article L2132-10](article-l2132-10.md)
- [Article L2132-11](article-l2132-11.md)
