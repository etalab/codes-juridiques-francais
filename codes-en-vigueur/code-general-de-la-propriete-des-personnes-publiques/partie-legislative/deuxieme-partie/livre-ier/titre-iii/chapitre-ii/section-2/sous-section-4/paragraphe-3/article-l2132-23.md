# Article L2132-23

Ont compétence pour constater concurremment les contraventions en matière de grande voirie définies aux articles L. 2132-5 à L. 2132-10, L. 2132-16 et L. 2132-17 :

1° Les fonctionnaires des collectivités territoriales et de leurs groupements ;

2° Les adjoints au maire et les gardes champêtres ;

3° Les personnels de Voies navigables de France sur le domaine qui lui a été confié, assermentés à cet effet devant le tribunal de grande instance ;

4° Les agents des ports autonomes fluviaux sur le domaine appartenant à ces ports ou qui leur a été confié, assermentés à cet effet devant le tribunal de grande instance ;

5° Les agents mentionnés à l'article L. 2132-21.

Les fonctionnaires mentionnés ci-dessus qui n'ont pas prêté serment en justice le prêtent devant le préfet.

Lorsqu'ils constatent une contravention en matière de grande voirie, les agents mentionnés aux 1° à 5° sont habilités à relever l'identité de l'auteur de la contravention. Si l'intéressé refuse ou se trouve dans l'impossibilité de justifier de son identité, ils en rendent compte à tout officier de police judiciaire territorialement compétent, qui peut ordonner au contrevenant de lui communiquer son identité. Lorsque l'officier de police judiciaire procède à une vérification d'identité dans les conditions prévues à l'article 78-3 du code de procédure pénale, le délai prévu au troisième alinéa du même article court à compter du relevé d'identité.
