# Section 1 : Procédures de recouvrement

- [Sous-section 1 : Modalités de recouvrement et mesures préalables aux poursuites.](sous-section-1)
- [Sous-section 2 : Exercice des poursuites.](sous-section-2)
- [Sous-section 3 : Mesures particulières.](sous-section-3)
