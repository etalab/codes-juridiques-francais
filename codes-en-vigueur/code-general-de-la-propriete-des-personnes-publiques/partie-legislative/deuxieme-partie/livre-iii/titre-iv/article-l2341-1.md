# Article L2341-1

I.-Un bien immobilier appartenant à l'Etat ou à un établissement public mentionné au onzième alinéa de l'article L. 710-1 du code de commerce, au premier alinéa de l'article 5-1 du code de l'artisanat ou à l'article L. 510-1 du code rural et de la pêche maritime peut faire l'objet d'un bail emphytéotique prévu à l'article L. 451-1 du même code, en vue de sa restauration, de sa réparation ou de sa mise en valeur. Ce bail est dénommé bail emphytéotique administratif.

Un tel bail peut être conclu même s'il porte sur une dépendance du domaine public.

Il peut prévoir l'obligation pour le preneur de se libérer du paiement de la redevance d'avance, pour tout ou partie de la durée du bail.

II.-Lorsque le bien objet du bail emphytéotique fait partie du domaine public de la personne publique, le bail conclu en application du I satisfait aux conditions particulières suivantes :

1° Les droits résultant du bail ne peuvent être cédés, avec l'agrément de la personne publique propriétaire, qu'à une personne subrogée au preneur dans les droits et obligations découlant de ce bail et, le cas échéant, des conventions non détachables conclues pour la réalisation de l'opération ;

2° Le droit réel conféré au preneur et les ouvrages dont il est propriétaire ne peuvent être hypothéqués qu'en vue de garantir des emprunts contractés par le preneur pour financer la réalisation des obligations qu'il tient du bail ; le contrat constituant l'hypothèque doit, à peine de nullité, être approuvé par la personne publique propriétaire ;

3° Seuls les créanciers hypothécaires peuvent exercer des mesures conservatoires ou des mesures d'exécution sur les droits immobiliers résultant du bail. La personne publique propriétaire peut se substituer au preneur dans la charge des emprunts en résiliant ou en modifiant le bail et, le cas échéant, les conventions non détachables ;

4° Les modalités de contrôle de l'activité du preneur par la personne publique propriétaire sont prévues dans le bail ;

5° Les constructions réalisées dans le cadre de ce bail peuvent donner lieu à la conclusion de contrats de crédit-bail. Dans ce cas, le contrat comporte des clauses permettant de préserver les exigences du service public.

III.-L'une ou plusieurs de ces conditions peuvent également être imposées au preneur lorsque le bien fait partie du domaine privé de la personne publique.
