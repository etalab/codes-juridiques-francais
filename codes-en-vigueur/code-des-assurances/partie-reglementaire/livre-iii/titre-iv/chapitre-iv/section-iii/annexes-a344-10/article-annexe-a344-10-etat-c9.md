# Article Annexe A344-10 ETAT C9

**ÉTAT C 9**

**Dispersion des réassureurs et simulations d'événements**

Les entreprises établissent, selon le modèle fixé ci-après, un état décrivant, à la date de clôture du dernier exercice inventorié, la dispersion de leurs cessionnaires et rétrocessionnaires et retraçant le niveau de protection conféré par leurs protections en réassurances si survenaient des événements défavorables.

Tableau A1

Répartition des provisions techniques cédées par réassureur ayant son siège social dans un Etat membre de la Communauté européenne ou dans un autre Etat partie à l'accord sur l'EEE

<table>
<tbody>
<tr>
<td rowspan="2" width="154">
<p align="center">DISPERSION <br/>des cessions (1) </p>
</td>
<td rowspan="2" width="154">
<p align="center">NOM (2) </p>
</td>
<td colspan="2" width="307">
<p align="center">PROVISIONS TECHNIQUES CÉDÉES OU RÉTROCÉDÉES </p>
</td>
</tr>
<tr>
<td width="154">
<p align="center">Montant notifié au réassureur </p>
</td>
<td width="154">
<p align="center">Montant non notifié au réassureur (3) </p>
</td>
</tr>
<tr>
<td width="154">
<br/>
</td>
<td width="154">
<p align="center">(A) </p>
</td>
<td width="154">
<p align="center">(B) </p>
</td>
<td width="154">
<p align="center">(C) </p>
</td>
</tr>
<tr>
<td valign="top" width="154">
<p>Réassureur 1 </p>
</td>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
</tr>
<tr>
<td valign="top" width="154">
<p>Réassureur 2 </p>
</td>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
</tr>
<tr>
<td valign="top" width="154">
<p>Réassureur 3 </p>
</td>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
</tr>
<tr>
<td valign="top" width="154">
<p>Réassureur 4 </p>
</td>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
</tr>
<tr>
<td valign="top" width="154">
<p>Réassureur 5 </p>
</td>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
</tr>
<tr>
<td valign="top" width="154">
<p>Réassureur 6 </p>
</td>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
</tr>
<tr>
<td valign="top" width="154">
<p>Réassureur 7 </p>
</td>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
</tr>
<tr>
<td valign="top" width="154">
<p>Réassureur 8 </p>
</td>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
</tr>
<tr>
<td valign="top" width="154">
<p>Réassureur 9 </p>
</td>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
</tr>
<tr>
<td valign="top" width="154">
<p>Réassureur 10 </p>
</td>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
</tr>
<tr>
<td valign="top" width="154">
<p>Autres réassureurs </p>
</td>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
</tr>
<tr>
<td valign="top" width="154">
<p>Total </p>
</td>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
<td valign="top" width="154"/>
</tr>
</tbody>
</table>

.

<table>
<tbody>
<tr>
<td width="101">
<p align="center">DISPERSION <br/>des cessions </p>
</td>
<td width="97">
<p align="center">SOLDE <br/>des comptes courants (4) </p>
</td>
<td width="98">
<p align="center">DÉPÔTS ESPÈCES </p>
</td>
<td width="99">
<p align="center">MONTANT <br/>des autres garanties apportées (5) </p>
</td>
<td width="121">
<p align="center">PROVISIONS techniques cédées non garanties / Capitaux propres nets d'incorporels (6) </p>
</td>
<td width="99">
<p align="center">MONTANT<br/>des créances de plus d'un an (7) </p>
</td>
</tr>
<tr>
<td width="101">
<br/>
</td>
<td width="97">
<p align="center">(D) </p>
</td>
<td width="98">
<p align="center">(E) </p>
</td>
<td width="99">
<p align="center">(F) </p>
</td>
<td width="121">
<p align="center">(G) </p>
</td>
<td width="99">
<p align="center">(H) </p>
</td>
</tr>
<tr>
<td valign="top" width="101">
<p>Réassureur 1 </p>
</td>
<td valign="top" width="97"/>
<td valign="top" width="98"/>
<td valign="top" width="99"/>
<td valign="top" width="121"/>
<td valign="top" width="99"/>
</tr>
<tr>
<td valign="top" width="101">
<p>Réassureur 2 </p>
</td>
<td valign="top" width="97"/>
<td valign="top" width="98"/>
<td valign="top" width="99"/>
<td valign="top" width="121"/>
<td valign="top" width="99"/>
</tr>
<tr>
<td valign="top" width="101">
<p>Réassureur 3 </p>
</td>
<td valign="top" width="97"/>
<td valign="top" width="98"/>
<td valign="top" width="99"/>
<td valign="top" width="121"/>
<td valign="top" width="99"/>
</tr>
<tr>
<td valign="top" width="101">
<p>Réassureur 4 </p>
</td>
<td valign="top" width="97"/>
<td valign="top" width="98"/>
<td valign="top" width="99"/>
<td valign="top" width="121"/>
<td valign="top" width="99"/>
</tr>
<tr>
<td valign="top" width="101">
<p>Réassureur 5 </p>
</td>
<td valign="top" width="97"/>
<td valign="top" width="98"/>
<td valign="top" width="99"/>
<td valign="top" width="121"/>
<td valign="top" width="99"/>
</tr>
<tr>
<td valign="top" width="101">
<p>Réassureur 6 </p>
</td>
<td valign="top" width="97"/>
<td valign="top" width="98"/>
<td valign="top" width="99"/>
<td valign="top" width="121"/>
<td valign="top" width="99"/>
</tr>
<tr>
<td valign="top" width="101">
<p>Réassureur 7 </p>
</td>
<td valign="top" width="97"/>
<td valign="top" width="98"/>
<td valign="top" width="99"/>
<td valign="top" width="121"/>
<td valign="top" width="99"/>
</tr>
<tr>
<td valign="top" width="101">
<p>Réassureur 8 </p>
</td>
<td valign="top" width="97"/>
<td valign="top" width="98"/>
<td valign="top" width="99"/>
<td valign="top" width="121"/>
<td valign="top" width="99"/>
</tr>
<tr>
<td valign="top" width="101">
<p>Réassureur 9 </p>
</td>
<td valign="top" width="97"/>
<td valign="top" width="98"/>
<td valign="top" width="99"/>
<td valign="top" width="121"/>
<td valign="top" width="99"/>
</tr>
<tr>
<td valign="top" width="101">
<p>Réassureur 10 </p>
</td>
<td valign="top" width="97"/>
<td valign="top" width="98"/>
<td valign="top" width="99"/>
<td valign="top" width="121"/>
<td valign="top" width="99"/>
</tr>
<tr>
<td valign="top" width="101">
<p>Autres réassureurs </p>
</td>
<td valign="top" width="97"/>
<td valign="top" width="98"/>
<td valign="top" width="99"/>
<td valign="top" width="121"/>
<td valign="top" width="99"/>
</tr>
<tr>
<td valign="top" width="101">
<p>Total </p>
</td>
<td valign="top" width="97"/>
<td valign="top" width="98"/>
<td valign="top" width="99"/>
<td valign="top" width="121"/>
<td valign="top" width="99"/>
</tr>
<tr>
<td colspan="6" valign="top" width="614">
<p>(1) Les réassureurs de l'entreprise sont à classer par ordre d'importance des provisions techniques cédées ou rétrocédées (y compris la part non notifiée de ces provisions). Le réassureur n° 1 correspond au réassureur le plus important. Les montants demandés dans ce tableau sont ceux à la date de clôture du dernier exercice inventorié et retracés dans le bilan. </p>
<p>(2) Il s'agit de la dénomination usuelle du réassureur. </p>
<p>(3) Il s'agit des montants de provisions techniques à charge des réassureurs figurant au bilan mais qui n'ont pas été communiqués à ces derniers. </p>
<p>(4) Il s'agit du montant des soldes des comptes courants à la date de clôture du dernier exercice inventorié (signe-s'ils sont en faveur du réassureur). </p>
<p>(5) Il s'agit du montant des garanties apportées conformément à l'article R. 332-17 (nantissement et garantie à première demande). </p>
<p>(6) Ce ratio doit être exprimé sous forme de pourcentage et précisé avec deux chiffres après la virgule (par exemple : 33, 18 %). Le calcul à effectuer est le suivant : (G) = (B + C + D-E-F) / (capitaux propres après affectation-actifs incorporels), ou zéro si le résultat est négatif. </p>
<p>(7) Il s'agit des créances au titre des comptes courants.L'ancienneté de ces créances est à mesurer par la durée qui sépare la date d'exigibilité de la créance de la date de clôture du dernier exercice inventorié. </p>
</td>
</tr>
</tbody>
</table>

Tableau A 2.

Répartition des provisions techniques cédées par réassureur

ayant son siège social dans un Etat non partie à l'accord sur l'EEE

<table>
<tbody>
<tr>
<td rowspan="2">
<p align="center">DISPERSION <br/>des cessions (1) </p>
</td>
<td rowspan="2">
<p align="center">NOM (2) </p>
<p align="center">(A)</p>
</td>
<td colspan="2">
<p align="center">PROVISIONS TECHNIQUES CÉDÉES OU RÉTROCÉDÉES </p>
</td>
</tr>
<tr>
<td>
<p align="center">Montant notifié au réassureur </p>
<p align="center">(B) </p>
</td>
<td>
<p align="center">Montant non notifié au réassureur (3) </p>
<p align="center">(C) </p>
</td>
</tr>
<tr>
<td align="center">
<br/>Réassureur 1 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Réassureur 2 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Réassureur 3 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Réassureur 4 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Réassureur 5 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Réassureur 6 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Réassureur 7 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Réassureur 8 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Réassureur 9 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Réassureur 10 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Autres réassureurs <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Total <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
</tbody>
</table>

<div align="left">. </div>

<table>
<tbody>
<tr>
<td>
<p align="center">DISPERSION<br/>des cessions </p>
</td>
<td>
<p align="center">SOLDE </p>
<p align="center">des comptes courants <br/>(4) (D)</p>
</td>
<td>
<p align="center">DÉPÔTS ESPÈCES </p>
<p align="center">(E)</p>
</td>
<td>
<p align="center">MONTANT </p>
<p align="center">des autres garanties </p>
<p align="center">apportées (5) </p>
<p align="center">(F)</p>
</td>
<td>
<p align="center">PROVISIONS </p>
<p align="center">techniques cédées </p>
<p align="center">non garanties / </p>
<p align="center">Capitaux propres <br/>nets d'incorporels (6) </p>
<p align="center">(G)</p>
</td>
<td>
<p align="center">MONTANT </p>
<p align="center">des créances </p>
<p align="center">de plus d'un an (7) </p>
<p align="center">(H)</p>
</td>
</tr>
<tr>
<td align="center">
<br/>Réassureur 1 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Réassureur 2 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Réassureur 3 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Réassureur 4 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Réassureur 5 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Réassureur 6 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Réassureur 7 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Réassureur 8 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Réassureur 9 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Réassureur 10 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Autres réassureurs <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Total <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
</tbody>
</table>

(1) Les réassureurs de l'entreprise sont à classer par ordre d'importance des provisions techniques cédées ou rétrocédées (y compris la part non notifiée de ces provisions). Le réassureur n° 1 correspond au réassureur le plus important. Les montants demandés dans ce tableau sont ceux à la date de clôture du dernier exercice inventorié et retracés dans le bilan.

(2) Il s'agit de la dénomination usuelle du réassureur.

(3) Il s'agit des montants de provisions techniques à charge des réassureurs figurant au bilan mais qui n'ont pas été communiqués à ces derniers.

(4) Il s'agit du montant des soldes des comptes courants à la date de clôture du dernier exercice inventorié (signe-s'ils sont en faveur du réassureur).

(5) Il s'agit du montant des garanties apportées conformément à l'article R. 332-17 (nantissement et garantie à première demande).

(6) Ce ratio doit être exprimé sous forme de pourcentage et précisé avec deux chiffres après la virgule (par exemple : 33, 18 %). Le calcul à effectuer est le suivant : (G) = (B + C + D-E-F) / (capitaux propres après affectation-actifs incorporels), ou zéro si le résultat est négatif.

(7) Il s'agit des créances au titre des comptes courants.L'ancienneté de ces créances est à mesurer par la durée qui sépare la date d'exigibilité de la créance de la date de clôture du dernier exercice inventorié.

Tableau A 3.

Répartition des provisions techniques cédées à des véhicules de titrisation

<table>
<tbody>
<tr>
<td rowspan="2">
<p align="center">DISPERSION <br/>des cessions (1)</p>
</td>
<td rowspan="2">
<p align="center">NOM (2) </p>
<p align="center">(A) </p>
</td>
<td colspan="2">
<p align="center">PROVISIONS TECHNIQUES CÉDÉES OU RÉTROCÉDÉES </p>
</td>
</tr>
<tr>
<td>
<p align="center">Montant notifié </p>
<p align="center">(B) <br/>
</p>
</td>
<td>
<p align="center">Montant non notifié (3) </p>
<p align="center">(C)</p>
</td>
</tr>
<tr>
<td align="center">
<br/>Véhicule 1 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Véhicule 2 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Véhicule 3 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Véhicule 4 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Véhicule 5 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Véhicule 6 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Véhicule 7 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Véhicule 8 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Véhicule 9 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Véhicule 10 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Autres véhicules <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Total <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
</tbody>
</table>

<div align="left">. </div>

<table>
<tbody>
<tr>
<td>
<p align="center">DISPERSION<br/>des cessions </p>
</td>
<td>
<p align="center">SOLDE </p>
<p align="center">des comptes courants <br/>(4) (D)</p>
</td>
<td>
<p align="center">DÉPÔTS ESPÈCES </p>
<p align="center">(E) </p>
</td>
<td>
<p align="center">MONTANT </p>
<p align="center">des autres garanties </p>
<p align="center">apportées (5) </p>
<p align="center">(F)</p>
</td>
<td>
<p align="center">PROVISIONS </p>
<p align="center">techniques cédées </p>
<p align="center">non garanties / </p>
<p align="center">Capitaux propres <br/>nets d'incorporels (6) </p>
<p align="center">(G)</p>
</td>
<td>
<p align="center">MONTANT </p>
<p align="center">des créances </p>
<p align="center">de plus d'un an (7) </p>
<p align="center">(H)</p>
</td>
</tr>
<tr>
<td align="center">
<br/>Véhicule 1 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Véhicule 2 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Véhicule 3 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Véhicule 4 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Véhicule 5 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Véhicule 6 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Véhicule 7 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Véhicule 8 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Véhicule 9 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Véhicule 10 <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Autres véhicules <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>Total <br/>
</td>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
<td align="center"/>
</tr>
</tbody>
</table>

(1) Les véhicules de titrisation sont à classer par ordre d'importance des provisions techniques cédées ou rétrocédées (y compris la part non notifiée de ces provisions). Le véhicule n° 1 correspond au véhicule de titrisation le plus important. Les montants demandés dans ce tableau sont ceux à la date de clôture du dernier exercice inventorié et retracés dans le bilan.

(2) Il s'agit de la dénomination usuelle du véhicule de titrisation.

(3) Il s'agit des montants de provisions techniques à charge des réassureurs figurant au bilan mais qui n'ont pas été communiqués à ces derniers.

(4) Il s'agit du montant des soldes des comptes courants à la date de clôture du dernier exercice inventorié (signe-s'ils sont en faveur du véhicule de titrisation).

(5) Il s'agit du montant des garanties apportées conformément à l'article R. 332-17 (nantissement et garantie à première demande).

(6) Ce ratio doit être exprimé sous forme de pourcentage et précisé avec deux chiffres après la virgule (par exemple : 33, 18 %). Le calcul à effectuer est le suivant : (G) = (B + C + D-E-F) / (capitaux propres après affectation-actifs incorporels), ou zéro si le résultat est négatif.

(7) Il s'agit des créances au titre des comptes courants.L'ancienneté de ces créances est à mesurer par la durée qui sépare la date d'exigibilité de la créance de la date de clôture du dernier exercice inventorié.

Tableau B

Simulations d'événements

Les entreprises agréées pour les opérations visées au 1° de l'article L. 310-1 ou au 2 de l'article R. 321-5-1 doivent renseigner les lignes numérotées 1, 7, 8 et 9 du tableau suivant. Les entreprises agréées pour les opérations visées aux 2° et 3° de l'article L. 310-1 ou au 1 de l'article R. 321-5-1 doivent renseigner les lignes numérotées 1 à 6.

<table>
<tbody>
<tr>
<td rowspan="3" width="205">
<p align="center">SIMULATION SUR L'ENSEMBLE <br/>des risques souscrits </p>
</td>
<td colspan="2" width="409">
<p align="center">CHARGE DE SINISTRE (1) </p>
</td>
</tr>
<tr>
<td width="205">
<p align="center">Brute </p>
</td>
<td width="205">
<p align="center">Nette </p>
</td>
</tr>
<tr>
<td width="205">
<p align="center">(A) </p>
</td>
<td width="205">
<p align="center">(B) </p>
</td>
</tr>
<tr>
<td width="205">
<p>1. Pire événement survenu pour la société (2) </p>
</td>
<td width="205"/>
<td width="205"/>
</tr>
<tr>
<td width="205">
<p>2. Tempêtes Lothar et Martin (3) </p>
</td>
<td width="205"/>
<td width="205"/>
</tr>
<tr>
<td width="205">
<p>3. Evénement centenaire (4) " tempête-ouragan-cyclone " (5) </p>
</td>
<td width="205"/>
<td width="205"/>
</tr>
<tr>
<td width="205">
<p>4. Evénement centenaire (4) " inondations " (5) </p>
</td>
<td width="205"/>
<td width="205"/>
</tr>
<tr>
<td width="205">
<p>5. Evénement centenaire (4) " tremblement de terre et autres cataclysmes " (5) </p>
</td>
<td width="205"/>
<td width="205"/>
</tr>
<tr>
<td width="205">
<p>6. Evénement majeur " responsabilité civile " (6) </p>
</td>
<td width="205"/>
<td width="205"/>
</tr>
<tr>
<td width="205">
<p>7. Evénement majeur " accidents technologiques " (7) </p>
</td>
<td width="205"/>
<td width="205"/>
</tr>
<tr>
<td width="205">
<p>8. Evénement majeur " épidémie " (8) </p>
</td>
<td width="205"/>
<td width="205"/>
</tr>
<tr>
<td width="205">
<p>9. Evénement majeur " garanties plancher " (9) </p>
</td>
<td width="205"/>
<td width="205"/>
</tr>
</tbody>
</table>

(1) Il s'agit de la charge de sinistres réévaluée correspondant à la survenance dans l'exercice en cours des événements définis dans ce tableau, compte tenu du portefeuille actuel de risques de la société. La charge nette doit tenir compte des couvertures actuelles en réassurance.

(2) Evénement qu'a connu l'entreprise dans le passé et qui conduirait, s'il survenait dans l'exercice en cours, à la charge de sinistres brute de réassurance la plus importante, compte tenu de l'actuel portefeuille de risques de l'entreprise. La charge nette doit tenir compte des couvertures actuelles en réassurance.

(3) Les charges de sinistres simulées relatives à ces deux événements (tempêtes du 26 et du 27 décembre 1999) doivent tenir compte de l'actuel portefeuille de risques de l'entreprise ainsi que de ses actuelles couvertures en réassurance.

(4) Evénement dont la période de retour, au regard du portefeuille de risques de l'entreprise, est égale à 100 années et dont la charge de sinistre brute de réassurance est la plus élevée.

(5) Sont à exclure les risques de la société qui sont cédés de manière illimitée à la Caisse centrale de réassurance avec la garantie de l'Etat.

(6) Il s'agit d'un scénario défavorable concernant le risque de responsabilité civile et utilisé par l'entreprise pour établir et analyser son programme de réassurance.

(7) Il s'agit d'un scénario défavorable de type accidents technologiques utilisé par l'entreprise pour établir et analyser son programme de réassurance.

(8) Il s'agit d'un scénario défavorable de type épidémie utilisé par l'entreprise pour établir et analyser son programme de réassurance.

(9) Il s'agit d'un scénario défini par la combinaison d'hypothèses financières standardisées. Ces hypothèses consistent, par rapport à leur moyenne respective constatée sur les trois dernières années, en une baisse de l'indice boursier de référence de 30 %, en une baisse de deux points des taux d'intérêt de l'obligation de référence et en une baisse de 20 % du prix des transactions immobilières. La charge de sinistres à renseigner correspond ici à la valeur actuelle probable, calculée au 1er janvier de l'exercice en cours, des prestations (nettes des prélèvements effectués au titre de ces garanties) associées aux garanties plancher jusqu'à leur extinction sous les hypothèses financières précédentes. Le taux d'actualisation à retenir est égal au minimum entre 3, 5 % et 60 % du TME. Entre la table de mortalité TD 88-90 et la table TV 88-90 doit être retenue celle donnant la valeur actuelle probable des prestations la plus élevée. Pour le calcul de la charge nette sont prises en compte les primes cédées et les prestations cédées, en valeur actuelle probable, et sont appliquées les conditions des traités en vigueur, notamment celles concernant la durée de la garantie, sans tenir compte des renouvellements éventuels de ces traités.
