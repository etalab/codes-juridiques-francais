# Article A344-9

Les comptes visés au 2° du premier alinéa de l'article A. 344-8 sont le compte de résultat, le bilan, y compris le tableau des engagements reçus et donnés, et l'annexe, ainsi qu'ils ont été arrêtés par le conseil d'administration ou le directoire pour être soumis à l'assemblée générale ou, pour une succursale d'entreprise étrangère, par le mandataire général à destination du siège social. Ils sont établis dans la forme prévue à l'article A. 344-3 et complétés par les informations énumérées à l'annexe au présent article.
