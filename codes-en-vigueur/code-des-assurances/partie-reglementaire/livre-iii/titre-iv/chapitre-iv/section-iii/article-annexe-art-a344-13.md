# Article Annexe art. A344-13

Etats trimestriels

Les montants sont arrondis au millier de francs le plus proche et exprimés en milliers de francs.

ETAT T 1

Flux trimestriels relatifs aux opérations en France

Les entreprises établissent trimestriellement, selon le modèle fixé ci-après, un état relatif à leurs opérations réalisées en France au cours du trimestre écoulé.

<table>
<tbody>
<tr>
<td width="156">
<p align="center">QUATRE TRIMESTRES PRÉCÉDENTS</p>
</td>
<td width="108">
<p align="center">TRIMESTRE T-7</p>
</td>
<td width="108">
<p align="center">TRIMESTRE T-6</p>
</td>
<td width="108">
<p align="center">TRIMESTRE T-5</p>
</td>
<td width="108">
<p align="center">TRIMESTRE T-4</p>
</td>
<td width="77">
<p align="center">CUMUL</p>
</td>
</tr>
<tr>
<td width="156">
<p>Nombre de contrats souscrits</p>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="77">
<br/>
</td>
</tr>
<tr>
<td width="156">
<p>Nombre de sinistres ouverts (1)</p>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="77">
<br/>
</td>
</tr>
<tr>
<td width="156">
<p>Primes émises nettes d'annulations (2)</p>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="77">
<br/>
</td>
</tr>
<tr>
<td width="156">
<p>Prestations payées (2)</p>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="77">
<br/>
</td>
</tr>
<tr>
<td width="156">
<p>Frais d'acquisition et d'administration (2)</p>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="77">
<br/>
</td>
</tr>
<tr>
<td width="156">
<p>Produits des placements (2)</p>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="77">
<br/>
</td>
</tr>
<tr>
<td width="156">
<p align="center">QUATRE DERNIERS TRIMESTRES</p>
</td>
<td width="108">
<p align="center">TRIMESTRE T-3</p>
</td>
<td width="108">
<p align="center">TRIMESTRE T-2</p>
</td>
<td width="108">
<p align="center">TRIMESTRE T-1</p>
</td>
<td width="108">
<p align="center">TRIMESTRE courant</p>
</td>
<td width="77">
<p align="center">CUMUL</p>
</td>
</tr>
<tr>
<td width="156">
<p>Nombre de contrats souscrits</p>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="77">
<br/>
</td>
</tr>
<tr>
<td width="156">
<p>Nombre des sinistres ouverts (1)</p>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="77">
<br/>
</td>
</tr>
<tr>
<td width="156">
<p>Primes émises nettes d'annulations (2)</p>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="77">
<br/>
</td>
</tr>
<tr>
<td width="156">
<p>Prestations payées (2)</p>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="77">
<br/>
</td>
</tr>
<tr>
<td width="156">
<p>Frais d'acquisition et d'administration (2)</p>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="77">
<br/>
</td>
</tr>
<tr>
<td width="156">
<p>Produits des placements (2)</p>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="108">
<br/>
</td>
<td width="77">
<br/>
</td>
</tr>
<tr>
<td colspan="6" width="665">
<p>(1) En vie et capitalisation, sinistres, sorties par tirage, échéances et rachats totaux.</p>
<p>(2) Montants extraits du grand livre au dernier jour ouvrable de chaque trimestre civil avant toute opération d'inventaire.</p>
</td>
</tr>
</tbody>
</table>

ÉTAT T 2 (cf. note 144)

Encours trimestriel des placements

Les entreprises établissent trimestriellement, selon le modèle fixé ci-après, un état détaillant l'encours en fin de trimestre de l'ensemble de leurs placements.

<table>
<thead>
<tr>
<td rowspan="3" width="227">
<p align="center">
<br/>DÉSIGNATION</p>
</td>
<td colspan="6" width="215">
<p align="center">
<br/>ENCOURS</p>
</td>
</tr>
<tr>
<td colspan="3" width="107">
<p align="center">
<br/>A la fin du trimestre inventorié</p>
</td>
<td colspan="3" width="107">
<p align="center">
<br/>A la fin du trimestre précédent</p>
</td>
</tr>
<tr>
<td width="31">
<p align="center">
<br/>Valeur brute</p>
</td>
<td width="31">
<p align="center">
<br/>Valeur nette</p>
</td>
<td width="44">
<p align="center">
<br/>Valeur de réalisation</p>
</td>
<td width="31">
<p align="center">
<br/>Valeur brute</p>
</td>
<td width="31">
<p align="center">
<br/>Valeur nette</p>
</td>
<td width="44">
<p align="center">
<br/>Valeur de réalisation</p>
</td>
</tr>
</thead>
<tbody>
<tr>
<td>
<p align="center">
<br/>A. - Placements mentionnés à l'article R. 332-2</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>1. Obligations et autres valeurs émises ou garanties par l'un des Etats membres de l'OCDE</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>2. Obligations, parts de fonds communs de créance et titres participatifs négociés sur un marché reconnu, autres que celles ou ceux visés au 1</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>3. Titres de créances négociables admissibles d'un an au plus</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>4. Bons à moyen terme négociables admissibles</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>5. Actions de SICAV et parts de FCP d'obligations et de titres de créances négociables</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p align="center">
<br/>Total placements obligataires</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>dont titres mis en pension</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>6. Actions et titres assimilés cotés sur des marchés reconnus, à l'exclusion de ceux émis par des sociétés d'assurance</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>7. Titres non cotés sur des marchés reconnus, à l'exclusion de ceux émis par des sociétés d'assurance</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>8. Parts de FCP à risques (dont FCP dans l'innovation) et fonds communs de proximité</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>9. OPCVM contractuels, OPCVM à procédure allégée, FCP à risques allégés</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>10. OPCVM à règles allégées, à l'exception des OPCVM de fonds alternatifs</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>11. OPCVM de fonds alternatifs</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>12. Actions de sociétés d'assurance, de réassurance ou de capitalisation ayant leur siège social dans l'OCDE</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>13. Actions de sociétés d'assurance, de réassurance ou de capitalisation ayant leur siège social hors de l'OCDE</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>14. Actions de SICAV et parts de FCP diversifiés</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p align="center">
<br/>Total des actions et titres assimilés</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>15. Droits réels immobiliers</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>16. Parts de sociétés immobilières ou foncières non cotées (y compris les avances en compte courant)</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p align="center">
<br/>Total des placements immobiliers</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>17. Prêts obtenus ou garantis par les Etats membres de l'OCDE ou assimilés</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>18. Prêts hypothécaires</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>19. Avances sur polices</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>20. Prêts de titres garantis par des espèces</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>21. Autres prêts de titres garantis</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>22. Autres prêts</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>Total des prêts</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>23. Fonds en dépôt</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>24. Placement immobiliers représentatifs de contrats en unités de compte</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>25. Autres placements représentatifs de contrats en unités de compte</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>Total A</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p align="center">
<br/>B. - Autres placements</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>26. Valeurs mobilières</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>27. Immeubles et parts de sociétés immobilières non cotées</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>28. Prêts de titres non garantis</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>29. Autres prêts</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>30. Fonds en dépôt</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>Total B</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>Total A + B</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>dont titres mis en pension</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>dont titres empruntés ou achetés à réméré</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p align="center">
<br/>C. - Instruments financiers à terme</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>31. Instruments financiers à terme liés à des produits de taux</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>32. Instruments financiers à terme liés à des actions et actifs assimilés</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>33. Autres instruments financiers à terme</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>Total C</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>Engagements reçus (1)</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td>
<p>
<br/>Engagements donnés (1)</p>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
<td>
<br/>
</td>
</tr>
<tr>
<td colspan="7">
<p>
<br/>(1) Hors instruments financiers à terme et hors nantissements.</p>
</td>
</tr>
</tbody>
</table>

ÉTAT T 3

Simulations actif-passif

Les entreprises établissent trimestriellement, selon le modèle fixé ci-après, un état retraçant l'incidence sur la valeur de réalisation de leurs placements ainsi que sur leurs provisions mathématiques, des hypothèses figurant ci-dessous.

<table>
<tbody>
<tr>
<td width="185">
<p align="center">PRODUITS DE TAUX</p>
</td>
<td width="60">
<p align="center">E(TEC 10) - 300 pb</p>
</td>
<td width="60">
<p align="center">E(TEC 10) - 200 pb</p>
</td>
<td width="60">
<p align="center">E(TEC 10) - 100 pb</p>
</td>
<td width="96">
<p align="center">E(TEC 10) (1)</p>
</td>
<td width="72">
<p align="center">TEC 10</p>
</td>
<td width="60">
<p align="center">E(TEC 10) + 200 pb</p>
</td>
<td width="68">
<p align="center">E(TEC 10) + 400 pb</p>
</td>
</tr>
<tr>
<td width="185">
<p>Cotés</p>
</td>
<td width="60"/>
<td width="60"/>
<td width="60"/>
<td width="96"/>
<td width="72"/>
<td width="60"/>
<td width="68"/>
</tr>
<tr>
<td width="185">
<p>Non cotés</p>
</td>
<td width="60"/>
<td width="60"/>
<td width="60"/>
<td width="96"/>
<td width="72"/>
<td width="60"/>
<td width="68"/>
</tr>
<tr>
<td width="185">
<p>Total</p>
</td>
<td width="60"/>
<td width="60"/>
<td width="60"/>
<td width="96"/>
<td width="72"/>
<td width="60"/>
<td width="68"/>
</tr>
<tr>
<td width="185">
<p>Instruments à terme liés (2)</p>
</td>
<td width="60"/>
<td width="60"/>
<td width="60"/>
<td width="96"/>
<td width="72"/>
<td width="60"/>
<td width="68"/>
</tr>
</tbody>
</table>

.

<table>
<tbody>
<tr>
<td width="209">
<p align="center">ACTIONS ET ACTIFS ASSIMILÉS</p>
</td>
<td width="72">
<p align="center">- 40 %</p>
</td>
<td width="72">
<p align="center">- 30 %</p>
</td>
<td width="72">
<p align="center">- 20 %</p>
</td>
<td width="72">
<p align="center">- 10 %</p>
</td>
<td width="168">
<p align="center">VALEUR DE RÉALISATION</p>
</td>
</tr>
<tr>
<td width="209">
<p>Cotés</p>
</td>
<td width="72">
<br/>
</td>
<td width="72">
<br/>
</td>
<td width="72">
<br/>
</td>
<td width="72">
<br/>
</td>
<td width="168">
<br/>
</td>
</tr>
<tr>
<td width="209">
<p>Non cotés</p>
</td>
<td width="72">
<br/>
</td>
<td width="72">
<br/>
</td>
<td width="72">
<br/>
</td>
<td width="72">
<br/>
</td>
<td width="168">
<br/>
</td>
</tr>
<tr>
<td width="209">
<p>Total</p>
</td>
<td width="72">
<br/>
</td>
<td width="72">
<br/>
</td>
<td width="72">
<br/>
</td>
<td width="72">
<br/>
</td>
<td width="168">
<br/>
</td>
</tr>
<tr>
<td width="209">
<p>Instruments à terme liés (2)</p>
</td>
<td width="72">
<br/>
</td>
<td width="72">
<br/>
</td>
<td width="72">
<br/>
</td>
<td width="72">
<br/>
</td>
<td width="168">
<br/>
</td>
</tr>
</tbody>
</table>

.

<table>
<tbody>
<tr>
<td width="149">
<p align="center">ACTIFS IMMOBILIERS</p>
</td>
<td width="71">
<p align="center">- 40 %</p>
</td>
<td width="73">
<p align="center">- 30 %</p>
</td>
<td width="72">
<p align="center">- 20 %</p>
</td>
<td width="60">
<p align="center">- 10 %</p>
</td>
<td width="236">
<p align="center">VALEUR DE RÉALISATION</p>
</td>
</tr>
<tr>
<td width="149">
<p>Cotés</p>
</td>
<td width="71">
<br/>
</td>
<td width="73">
<br/>
</td>
<td width="72">
<br/>
</td>
<td width="60">
<br/>
</td>
<td width="236">
<br/>
</td>
</tr>
<tr>
<td width="149">
<p>Non cotés</p>
</td>
<td width="71">
<br/>
</td>
<td width="73">
<br/>
</td>
<td width="72">
<br/>
</td>
<td width="60">
<br/>
</td>
<td width="236">
<br/>
</td>
</tr>
<tr>
<td width="149">
<p>Total</p>
</td>
<td width="71">
<br/>
</td>
<td width="73">
<br/>
</td>
<td width="72">
<br/>
</td>
<td width="60">
<br/>
</td>
<td width="236">
<br/>
</td>
</tr>
</tbody>
</table>

.

<table>
<tbody>
<tr>
<td width="245">
<p align="center">ENGAGEMENTS D'ASSURANCE VIE EN FRANCS Engagements viagers d'assurance non vie et PPE</p>
</td>
<td width="60">
<p align="center">E(TEC 10) - 330 pb</p>
</td>
<td width="60">
<p align="center">E(TEC 10) - 230 pb</p>
</td>
<td width="60">
<p align="center">E(TEC 10) - 130 pb</p>
</td>
<td width="60">
<p align="center">E(TEC 10) - 30 pb</p>
</td>
<td width="48">
<p align="center">TEC 10 - 30 pb</p>
</td>
<td width="60">
<p align="center">E(TEC 10) + 170 pb</p>
</td>
<td width="68">
<p align="center">E(TEC 10) + 370 pb</p>
</td>
</tr>
<tr>
<td width="245">
<p>Provisions pour participation aux excédents (3)</p>
</td>
<td width="60"/>
<td width="60"/>
<td width="60"/>
<td width="60"/>
<td width="48"/>
<td width="60"/>
<td width="68"/>
</tr>
<tr>
<td width="245">
<p>Engagements en francs liés à des contrats rachetables (4)</p>
</td>
<td width="60"/>
<td width="60"/>
<td width="60"/>
<td width="60"/>
<td width="48"/>
<td width="60"/>
<td width="68"/>
</tr>
<tr>
<td width="245">
<p>Autres engagements en francs</p>
</td>
<td width="60"/>
<td width="60"/>
<td width="60"/>
<td width="60"/>
<td width="48"/>
<td width="60"/>
<td width="68"/>
</tr>
<tr>
<td width="245">
<p>Total</p>
</td>
<td width="60"/>
<td width="60"/>
<td width="60"/>
<td width="60"/>
<td width="48"/>
<td width="60"/>
<td width="68"/>
</tr>
</tbody>
</table>

.

<table>
<tbody>
<tr>
<td width="281">
<p align="center">ENGAGEMENTS EN UNITÉS DE COMPTE</p>
</td>
<td width="72">
<p align="center">- 40 %</p>
</td>
<td width="72">
<p align="center">- 30 %</p>
</td>
<td width="72">
<p align="center">- 20 %</p>
</td>
<td width="60">
<p align="center">- 10 %</p>
</td>
<td width="104">
<p align="center">VALEUR DE RÉALISATION</p>
</td>
</tr>
<tr>
<td width="281">
<p>Actif représentatif</p>
</td>
<td width="72"/>
<td width="72"/>
<td width="72"/>
<td width="60"/>
<td width="104"/>
</tr>
<tr>
<td width="281">
<p>Engagements sans risque de placement</p>
</td>
<td width="72"/>
<td width="72"/>
<td width="72"/>
<td width="60"/>
<td width="104"/>
</tr>
<tr>
<td width="281">
<p>Résultat probable lié aux engagements avec risque de placement</p>
</td>
<td width="72"/>
<td width="72"/>
<td width="72"/>
<td width="60"/>
<td width="104"/>
</tr>
</tbody>
</table>

.

<table>
<tbody>
<tr>
<td width="197">
<p align="center">AUTRES PROVISIONS TECHNIQUES</p>
</td>
<td width="72">
<p align="center">N</p>
</td>
<td width="84">
<p align="center">N + 1</p>
</td>
<td width="84">
<p align="center">N + 2</p>
</td>
<td width="84">
<p align="center">N + 3</p>
</td>
<td width="72">
<p align="center">N + 4</p>
</td>
<td width="68">
<p align="center">N + 5 et ultérieurs</p>
</td>
</tr>
<tr>
<td width="197">
<p>Provisions pour primes non acquises</p>
</td>
<td width="72"/>
<td width="84"/>
<td width="84"/>
<td width="84"/>
<td width="72"/>
<td width="68"/>
</tr>
<tr>
<td width="197">
<p>Provisions pour risques en cours</p>
</td>
<td width="72"/>
<td width="84"/>
<td width="84"/>
<td width="84"/>
<td width="72"/>
<td width="68"/>
</tr>
<tr>
<td width="197">
<p>Provisions pour sinistres à payer</p>
</td>
<td width="72"/>
<td width="84"/>
<td width="84"/>
<td width="84"/>
<td width="72"/>
<td width="68"/>
</tr>
<tr>
<td width="197">
<p>Provisions pour risques croissants</p>
</td>
<td width="72"/>
<td width="84"/>
<td width="84"/>
<td width="84"/>
<td width="72"/>
<td width="68"/>
</tr>
<tr>
<td width="197">
<p>Total</p>
</td>
<td width="72"/>
<td width="84"/>
<td width="84"/>
<td width="84"/>
<td width="72"/>
<td width="68"/>
</tr>
</tbody>
</table>

.

<table>
<tbody>
<tr>
<td rowspan="2" width="185">
<p align="center">PREMIÈRES CONTREPARTIES (5)</p>
</td>
<td colspan="2" width="252">
<p align="center">VALEUR comptable</p>
</td>
<td colspan="2" width="224">
<p align="center">VALEUR de réalisation</p>
</td>
</tr>
<tr>
<td width="132">
<p align="center">Produits de taux</p>
</td>
<td width="120">
<p align="center">Autres</p>
</td>
<td width="120">
<p align="center">Produits de taux</p>
</td>
<td width="104">
<p align="center">Autres</p>
</td>
</tr>
<tr>
<td width="185">
<p>1</p>
</td>
<td width="132"/>
<td width="120"/>
<td width="120"/>
<td width="104"/>
</tr>
<tr>
<td width="185">
<p>2</p>
</td>
<td width="132"/>
<td width="120"/>
<td width="120"/>
<td width="104"/>
</tr>
<tr>
<td width="185">
<p>3</p>
</td>
<td width="132"/>
<td width="120"/>
<td width="120"/>
<td width="104"/>
</tr>
<tr>
<td width="185">
<p>4</p>
</td>
<td width="132"/>
<td width="120"/>
<td width="120"/>
<td width="104"/>
</tr>
<tr>
<td width="185">
<p>5</p>
</td>
<td width="132"/>
<td width="120"/>
<td width="120"/>
<td width="104"/>
</tr>
<tr>
<td colspan="5" width="661">
<p>(1) E(TEC 10) est le nombre entier de centaines de points de base immédiatement inférieur à la valeur annuelle du TEC 10.</p>
<p>(2) Les instruments à terme sont évalués à leurs coûts de remplacement déduits de chacune des valeurs de réalisation simulées sur les actifs sous-jacents.</p>
<p>(3) La provision pour participation aux excédents est évaluée à sa dernière valeur comptable connue.</p>
<p>(4) Les provisions mathématiques sont évaluées avec application, au titre des charges de gestion, d'un abattement de 30 points de base à chacun des taux retenus.</p>
<p>(5) Pour ces évaluations, une contrepartie est soit une société isolée, soit plusieurs sociétés appartenant au même groupe.</p>
</td>
</tr>
</tbody>
</table>
