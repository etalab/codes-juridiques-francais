# Article A362-2

I. Les informations requises visées à l'article L. 362-2 doivent être rédigées en langue française et comporter les éléments suivants :

a) La dénomination et l'adresse du siège social de l'entreprise ;

b) La liste des branches que l'entreprise est habilitée à pratiquer ;

c) La nature des risques ou engagements que l'entreprise se propose de prendre ou garantir sur le territoire français ;

d) Dans le cas où l'entreprise se proposerait de couvrir les risques définis à la branche 10 de l'article R. 321-1, à l'exclusion de la responsabilité civile du transporteur, une déclaration d'adhésion au bureau national d'assurance mentionné à l'article L. 421-15 et au Fonds national de garantie contre les accidents de circulation ainsi que le nom et l'adresse du représentant pour la gestion des sinistres qu'elle désigne sur le territoire français ;

e) Dans le cas où l'entreprise se proposerait de couvrir les risques définis à la branche 17 de l'article R. 321-1, l'option choisie parmi celles énoncées à l'article L. 322-2-3 ;

f) Un certificat des autorités compétentes de l'Etat d'origine de l'entreprise attestant qu'elle possède bien la marge de solvabilité.

II. L'entreprise peut commencer ses activités sur le territoire français dès que le ministre de l'économie a reçu communication des informations visées au I du présent article.

III. Toute modification envisagée de l'une des informations visées au I du présent article doit être préalablement notifiée en langue française au ministre chargé de l'économie et des finances par les autorités compétentes de l'Etat d'origine de l'entreprise.

La modification envisagée peut intervenir dès que l'entreprise a été avisée par les autorités compétentes de son Etat d'origine de la notification visée à l'alinéa précédent.
