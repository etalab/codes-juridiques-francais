# Paragraphe 8 : Dispositions particulières relatives à la provision pour risque d'exigibilité.

- [Article A331-26](article-a331-26.md)
- [Article A331-27](article-a331-27.md)
