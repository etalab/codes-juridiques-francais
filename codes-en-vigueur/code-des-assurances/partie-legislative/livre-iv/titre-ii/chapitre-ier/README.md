# Chapitre Ier : Le Fonds de garantie des assurances obligatoires de dommages.

- [Section I : Dispositions générales.](section-i)
- [Section II : Dispositions spéciales aux accidents de chasse survenus en France métropolitaine.](section-ii)
- [Section V : Régime financier du fonds de garantie.](section-v)
- [Section VI : Intervention du fonds en cas de retrait d'agrément administratif d'entreprises d'assurances obligatoires.](section-vi)
- [Section IX : Dispositions particulières applicables aux accidents d'automobile survenus à l'étranger.](section-ix)
- [Section X : Dispositions spéciales aux catastrophes technologiques.](section-x)
- [Section XI : Dispositions particulières applicables aux dommages immobiliers d'origine minière.](section-xi)
