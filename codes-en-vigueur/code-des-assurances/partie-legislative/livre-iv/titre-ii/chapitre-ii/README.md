# Chapitre II : Le fonds de garantie des victimes des actes de terrorisme et d'autres infractions.

- [Section I : Indemnisation des victimes des actes de terrorisme et d'autres infractions.](section-i)
- [Section II : Aide au recouvrement des dommages et intérêts pour les victimes d'infractions.](section-ii)
