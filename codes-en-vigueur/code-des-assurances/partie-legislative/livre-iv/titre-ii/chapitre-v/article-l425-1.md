# Article L425-1

I. - Un fonds de garantie des risques liés à l'épandage agricole des boues d'épuration urbaines ou industrielles est chargé d'indemniser les préjudices subis par les exploitants agricoles et les propriétaires des terres agricoles et forestières dans les cas où ces terres, ayant reçu des épandages de boues d'épuration urbaines ou industrielles, deviendraient totalement ou partiellement impropres à la culture en raison de la réalisation d'un risque sanitaire ou de la survenance d'un dommage écologique lié à l'épandage, dès lors que, du fait de l'état des connaissances scientifiques et techniques, ce risque ou ce dommage ne pouvait être connu au moment de l'épandage et dans la mesure où ce risque ou ce dommage n'est pas assurable par les contrats d'assurance de responsabilité civile du maître d'ouvrage des systèmes de traitement collectif des eaux usées domestiques ou, le cas échéant, de son ou ses délégataires, de l'entreprise de vidange, ou du maître d'ouvrage des systèmes de traitement des eaux usées industrielles, ci-après désignés par l'expression : "producteurs de boues", ou par les contrats d'assurance relatifs à la production et à l'élimination des boues.

La liste des branches industrielles visées par le présent article est définie par décret en Conseil d'Etat.

Le fonds assure l'indemnisation des dommages constatés dans la limite d'un montant maximum, sous réserve que l'épandage ait été effectué dans des conditions conformes à la réglementation en vigueur.

Le montant de l'indemnisation est fonction du préjudice subi et ne peut excéder, pour le propriétaire des terres, la valeur de celles-ci.

La gestion comptable et financière du fonds est assurée par la caisse centrale de réassurance dans un compte distinct de ceux retraçant les autres opérations qu'elle effectue. Les frais qu'elle expose pour cette gestion sont imputés sur le fonds.

La caisse est informée de tous les litiges liés à l'épandage agricole des boues d'épuration pris directement en charge par les assurances.

II. - Le fonds mentionné au I est financé par une taxe annuelle due par les producteurs de boues et dont l'assiette est la quantité de matière sèche de boue produite. En outre, le fonds peut recevoir des avances de l'Etat dans la mesure où les dommages survenus excèdent momentanément la capacité d'indemnisation de ce dernier.

Le montant de la taxe est fixé par décret en Conseil d'Etat dans la limite d'un plafond de 0,5 euros par tonne de matière sèche de boue produite.

Les redevables procèdent à la liquidation de la taxe due au titre de l'année précédente lors du dépôt de leur déclaration de taxe sur la valeur ajoutée du mois de mars ou du premier trimestre de l'année civile.

La taxe est recouvrée et contrôlée selon les mêmes procédures et sous les mêmes sanctions, garanties, sûretés et privilèges que la taxe sur la valeur ajoutée. Les réclamations sont présentées, instruites et jugées selon les règles applicables à cette même taxe.

III. - Un décret en Conseil d'Etat précise les conditions d'application du présent article, notamment le montant maximal que peuvent atteindre les ressources du fonds.
