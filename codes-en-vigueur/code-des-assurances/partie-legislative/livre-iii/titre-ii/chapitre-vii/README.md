# Chapitre VII : Privilèges.

- [Article L327-1](article-l327-1.md)
- [Article L327-2](article-l327-2.md)
- [Article L327-3](article-l327-3.md)
- [Article L327-4](article-l327-4.md)
- [Article L327-5](article-l327-5.md)
