# Paragraphe 1 : Dispositions générales.

- [Article L322-27](article-l322-27.md)
- [Article L322-27-1](article-l322-27-1.md)
- [Article L322-27-2](article-l322-27-2.md)
