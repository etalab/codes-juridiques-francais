# Section III : Entreprises nationales d'assurance et de capitalisation et sociétés centrales d'assurance

- [Paragraphe 1 : Constitution.](paragraphe-1)
- [Paragraphe 2 : Administration.](paragraphe-2)
- [Paragraphe 3 : Distribution et cession des actions des sociétés centrales d'assurance.](paragraphe-3)
