# Paragraphe 3 : Distribution et cession des actions des sociétés centrales d'assurance.

- [Article L322-22](article-l322-22.md)
- [Article L322-23](article-l322-23.md)
- [Article L322-24](article-l322-24.md)
