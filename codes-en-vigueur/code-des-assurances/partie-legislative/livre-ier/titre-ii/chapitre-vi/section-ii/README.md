# Section II : Dommages matériels.

- [Article L126-2](article-l126-2.md)
- [Article L126-3](article-l126-3.md)
