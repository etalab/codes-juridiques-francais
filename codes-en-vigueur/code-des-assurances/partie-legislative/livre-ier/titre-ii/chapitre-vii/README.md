# Chapitre VII : L'assurance de protection juridique.

- [Article L127-1](article-l127-1.md)
- [Article L127-2](article-l127-2.md)
- [Article L127-2-1](article-l127-2-1.md)
- [Article L127-2-2](article-l127-2-2.md)
- [Article L127-2-3](article-l127-2-3.md)
- [Article L127-3](article-l127-3.md)
- [Article L127-4](article-l127-4.md)
- [Article L127-5](article-l127-5.md)
- [Article L127-5-1](article-l127-5-1.md)
- [Article L127-6](article-l127-6.md)
- [Article L127-7](article-l127-7.md)
- [Article L127-8](article-l127-8.md)
