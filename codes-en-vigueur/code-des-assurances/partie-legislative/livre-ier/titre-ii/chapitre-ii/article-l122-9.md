# Article L122-9

L'assureur peut prévoir une minoration de la prime ou de la cotisation prévue par la police d'assurance garantissant les dommages incendie lorsqu'il est établi qu'il est satisfait  aux obligations prévues aux articles L. 129-8 et L. 129-9 du code de la construction et de l'habitation.
