# Article L125-4

Nonobstant toute disposition contraire, la garantie visée par l'article L. 125-1 du présent code inclut le remboursement du coût des études géotechniques rendues préalablement nécessaires pour la remise en état des constructions affectées par les effets d'une catastrophe naturelle.
