# Article L134-3

En cas d'insuffisance de représentation des engagements faisant l'objet d'une comptabilité auxiliaire d'affectation mentionnée à l'article L. 134-2, l'entreprise d'assurance parfait cette représentation par apport d'actifs représentatifs de ses réserves ou de ses provisions autres que ceux représentatifs de ses engagements réglementés. Lorsque le niveau de la représentation de ses engagements faisant l'objet d'une comptabilité auxiliaire d'affectation le permet, l'entreprise d'assurance réaffecte des actifs de celle-ci à la représentation d'autres réserves ou provisions.
