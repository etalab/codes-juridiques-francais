# Chapitre IV : Règles spéciales aux assurances fluviale et lacustre

- [Section I : Assurance sur corps.](section-i)
- [Section II : Assurance sur marchandises transportées](section-ii)
