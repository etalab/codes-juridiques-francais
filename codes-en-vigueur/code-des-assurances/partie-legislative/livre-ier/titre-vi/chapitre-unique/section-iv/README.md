# Section IV : Rachat par les entreprises d'assurance sur la vie des rentes inférieures à un certain montant minimal.

- [Article L160-5](article-l160-5.md)
