# Section III : Contrats d'assurance libellés en monnaie étrangère.

- [Article L160-3](article-l160-3.md)
- [Article L160-4](article-l160-4.md)
