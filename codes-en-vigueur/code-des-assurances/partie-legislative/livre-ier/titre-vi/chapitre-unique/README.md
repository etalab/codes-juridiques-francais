# Chapitre unique

- [Section II : Polices d'assurance sur la vie ou bons de capitalisation ou d'épargne égarés, détruits ou volés.](section-ii)
- [Section III : Contrats d'assurance libellés en monnaie étrangère.](section-iii)
- [Section IV : Rachat par les entreprises d'assurance sur la vie des rentes inférieures à un certain montant minimal.](section-iv)
- [Section V : Effet sur les contrats d'assurance de la réquisition des biens et services.](section-v)
- [Section V bis : Effet sur les contrats d'assurance sur la vie de la confiscation pénale.](section-v-bis)
- [Section VI : Assurances sur la vie à capital variable immobilier.](section-vi)
