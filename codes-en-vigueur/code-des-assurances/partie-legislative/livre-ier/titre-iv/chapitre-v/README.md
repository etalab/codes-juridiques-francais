# Chapitre V : Dispositions particulières relatives à la coassurance de certaines opérations collectives avec les organismes d'assurance relevant du code de la sécurité sociale et du code de la mutualité

- [Article L145-1](article-l145-1.md)
- [Article L145-2](article-l145-2.md)
- [Article L145-3](article-l145-3.md)
- [Article L145-4](article-l145-4.md)
- [Article L145-5](article-l145-5.md)
- [Article L145-6](article-l145-6.md)
- [Article L145-7](article-l145-7.md)
- [Article L145-8](article-l145-8.md)
- [Article L145-9](article-l145-9.md)
