# Chapitre IV : Régime de responsabilité applicable aux services postaux.

- [Article L7](article-l7.md)
- [Article L8](article-l8.md)
- [Article L9](article-l9.md)
