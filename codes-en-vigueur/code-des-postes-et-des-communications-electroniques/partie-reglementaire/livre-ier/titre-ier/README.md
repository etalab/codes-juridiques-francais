# TITRE Ier : Dispositions générales

- [Chapitre Ier : Le service universel postal et les obligations du service postal](chapitre-ier)
- [Chapitre IV : Conditions d'admission des objets de correspondance dans le régime intérieur.](chapitre-iv)
