# Chapitre II : Enregistrement, publicité foncière et timbre

- [Section V : Droit de timbre perçu au profit de l'Office national de la chasse et de la faune sauvage](section-v)
- [Section VI  : Droit affecté au fonds d'indemnisation de la profession d'avoués près les cours d'appel](section-vi)
