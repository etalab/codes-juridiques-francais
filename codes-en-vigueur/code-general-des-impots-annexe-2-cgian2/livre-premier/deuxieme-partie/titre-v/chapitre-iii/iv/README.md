# IV : Règles d'évaluation de la valeur locative des biens imposables

- [Article 333](article-333.md)
- [A : Évaluation des propriétés bâties](a)
- [B : Évaluation des propriétés non bâties.](b)
