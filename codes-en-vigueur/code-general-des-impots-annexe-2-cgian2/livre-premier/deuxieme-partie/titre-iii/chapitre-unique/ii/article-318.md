# Article 318

La part du quota de la taxe d'apprentissage mentionnée au deuxième alinéa de l'article L. 6241-2 du code du travail est versée au Trésor public par les organismes collecteurs dans le délai prévu à l'article R. 6241-5 du même code.
