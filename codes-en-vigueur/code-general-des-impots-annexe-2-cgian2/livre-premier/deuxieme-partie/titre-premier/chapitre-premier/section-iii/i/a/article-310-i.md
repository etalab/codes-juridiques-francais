# Article 310 I

Les coefficients prévus au 1 du III de l'article 1496 du code général des impôts qui doivent être utilisés, à compter du 1er janvier 1979, jusqu'à la prochaine actualisation des valeurs locatives foncières des propriétés bâties pour la détermination de la base d'imposition à la taxe foncière sur les propriétés bâties des locaux d'habitation ou à usage professionnel loués sous le régime de la réglementation édictée par la loi n° 48-1360 du 1er septembre 1948 modifiée, sont fixés comme suit :

<table>
<tbody>
<tr>
<td width="529">
<p align="center">CATÉGORIES </p>
</td>
<td width="151">
<p align="center">COEFFICIENT </p>
</td>
</tr>
<tr>
<td valign="top" width="529">
<p>Catégorie II A </p>
</td>
<td valign="top" width="151">
<p align="center">2,83 </p>
</td>
</tr>
<tr>
<td valign="top" width="529">
<p>Catégorie II B </p>
</td>
<td valign="top" width="151">
<p align="center">2,49 </p>
</td>
</tr>
<tr>
<td valign="top" width="529">
<p>Catégorie II C </p>
</td>
<td valign="top" width="151">
<p align="center">2,14 </p>
</td>
</tr>
<tr>
<td valign="top" width="529">
<p>Catégorie III A </p>
</td>
<td valign="top" width="151">
<p align="center">1,93 </p>
</td>
</tr>
<tr>
<td valign="top" width="529">
<p>Catégorie III B </p>
</td>
<td valign="top" width="151">
<p align="center">1,75 </p>
</td>
</tr>
<tr>
<td valign="top" width="529">
<p>Catégorie IV </p>
</td>
<td valign="top" width="151">
<p align="center">1,00 </p>
</td>
</tr>
<tr>
<td valign="top" width="529">
<p>Catégorie des locaux soumis au régime du loyer forfaitaire de l'article 34 de la loi du 1er septembre 1948 </p>
</td>
<td valign="top" width="151">
<p align="center">1,93</p>
</td>
</tr>
</tbody>
</table>
