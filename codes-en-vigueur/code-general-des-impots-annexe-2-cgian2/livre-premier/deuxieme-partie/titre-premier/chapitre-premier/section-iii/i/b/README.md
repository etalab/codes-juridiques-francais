# B : Immobilisations industrielles

- [Article 310 L](article-310-l.md)
- [Article 310 J bis](article-310-j-bis.md)
- [Article 310 K](article-310-k.md)
- [Article 310 K bis](article-310-k-bis.md)
