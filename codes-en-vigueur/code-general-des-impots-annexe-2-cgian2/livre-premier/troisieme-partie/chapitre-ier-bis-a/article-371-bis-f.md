# Article 371 bis F

Le professionnel de l'expertise comptable qui a conclu la convention prévue à l'article 371 bis B transmet à ses clients ou adhérents imposés d'après leur bénéfice réel, dans un délai de neuf mois suivant la clôture de leur exercice comptable, un dossier comprenant :

a) Les ratios et les autres éléments caractérisant la situation financière et économique de l'entreprise ;

b) Un commentaire sur la situation financière et économique de l'entreprise ;

c) A partir de la clôture du deuxième exercice suivant le début de leur relation contractuelle, une analyse comparative des bilans et des comptes de résultat de l'entreprise. Toutefois, pour les entreprises soumises au régime simplifié d'imposition, seule l'analyse comparative des comptes de résultat est adressée ;

d) Un document de synthèse présentant une analyse économique en matière de prévention des difficultés économiques et financières de l'entreprise avec l'indication, le cas échéant, des démarches à accomplir.
