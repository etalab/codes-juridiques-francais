# Article 371 H

La commission rend sa décision dans un délai de quatre mois à compter de la date de délivrance du récépissé mentionné à l'article 371 F.

L'absence de décision dans le délai fixé vaut acceptation de la demande. Le refus d'agrément doit être motivé.
