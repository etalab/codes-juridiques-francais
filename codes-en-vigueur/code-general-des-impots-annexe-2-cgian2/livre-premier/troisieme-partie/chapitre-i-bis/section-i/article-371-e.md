# Article 371 E

Les statuts du centre précisent les conditions de participation à la gestion du centre des personnes ou organismes qui ont pris l'initiative de sa création. Au sein du conseil d'administration ou de tout autre organe dirigeant, les adhérents doivent être représentés à hauteur d'un minimum d'un tiers des sièges. Les personnes ou organismes autres que les membres mentionnés à l'article 1649 quater C du code général des impôts et autres que les adhérents peuvent être membres associés et participer au conseil d'administration ou à tout autre organe dirigeant, à hauteur d'un maximum d'un tiers des sièges.

Ils doivent comporter en outre les stipulations suivantes :

1° Le centre fournit à ses membres adhérents imposés d'après leur bénéfice réel dans un délai de neuf mois suivant la clôture de leur exercice comptable, un dossier comprenant :

a. Les ratios et les autres éléments caractérisant la situation financière et économique de l'entreprise : la nature de ces ratios et autres éléments est fixée par arrêté du ministre du budget, du ministre de l'agriculture, du ministre de l'industrie et du ministre du commerce et de l'artisanat (1) ;

b. Un commentaire sur la situation financière et économique de l'entreprise ;

c. A partir de la clôture du deuxième exercice suivant celui de l'adhésion et dans le même délai de neuf mois, le centre fournit à ses adhérents une analyse comparative des bilans et des comptes de résultat de l'entreprise. Toutefois, pour les entreprises soumises au régime simplifié d'imposition, seule l'analyse comparative des comptes d'exploitation doit être fournie ;

d. Un document de synthèse présentant une analyse des informations économiques, comptables et financières de l'entreprise et lui indiquant, le cas échéant, les démarches à accomplir ;

2° Le centre élabore pour ceux de ses membres adhérents qui sont placés sous un régime réel d'imposition les déclarations afférentes à leur exploitation destinées à l'administration fiscale, lorsque ces membres en font la demande.

Toutefois, ces déclarations ne peuvent porter que sur une période au cours de laquelle les intéressés étaient membres du centre ;

3° L'adhésion au centre implique pour les membres adhérents imposés d'après leur bénéfice réel :

a. L'engagement de produire à la personne ou à l'organisme chargé de tenir et de présenter leurs documents comptables tous les éléments nécessaires à l'établissement d'une comptabilité sincère de leur exploitation ;

b. Abrogé ;

c. L'obligation de communiquer au centre le bilan et les comptes de résultat ainsi que tous documents annexes ;

d. Abrogé ;

e. L'autorisation pour le centre de communiquer à l'administration fiscale, dans le cadre de l'assistance que cette dernière lui apporte, les documents mentionnés au présent article.

En cas de manquements graves ou répétés aux engagements ou obligations sus-énoncés l'adhérent est exclu du centre. Il doit être mis en mesure, avant toute décision d'exclusion, de présenter sa défense sur les faits qui lui sont reprochés.

(1) Voir annexe IV, art. 164 F unvicies.
