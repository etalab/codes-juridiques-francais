# II : Opérations imposables sur option

- [5 : Livraison de certains biens immobiliers](5)
- [5 bis : Personnes qui ont passé un bail à construction](5-bis)
- [6 : Collectivités locales](6)
- [7 : Bailleurs de biens ruraux](7)
- [Article 180](article-180.md)
- [Article 181](article-181.md)
- [Article 182](article-182.md)
- [Article 183](article-183.md)
- [Article 184](article-184.md)
