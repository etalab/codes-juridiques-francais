# Section IV : Dispositions particulières aux opérations concourant à la production ou à la livraison d'immeubles

- [I : Dispositions relatives aux livraisons à soi-même](i)
- [II : Dispositions relatives aux mutations](ii)
