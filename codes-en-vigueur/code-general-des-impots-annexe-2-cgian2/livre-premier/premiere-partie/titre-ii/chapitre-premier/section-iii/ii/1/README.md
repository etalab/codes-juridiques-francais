# 1 : Modalités d'exercice

- [C : Dispositions diverses](c)
- [A : Détermination du quantum de taxe déductible](a)
- [B : Régularisations et reversements](b)
