# Section V : Régimes spéciaux

- [I : Exploitants agricoles](i)
- [II : Obligations des assujettis qui réalisent des opérations portant sur les animaux vivants de boucherie et de charcuterie](ii)
