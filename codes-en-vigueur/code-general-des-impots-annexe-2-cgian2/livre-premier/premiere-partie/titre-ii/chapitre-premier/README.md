# Chapitre premier : Taxe sur la valeur ajoutée

- [Section I : Champ d'application](section-i)
- [Section III : Liquidation de la taxe](section-iii)
- [Section III ter : Obligations des redevables](section-iii-ter)
- [Section IV : Dispositions particulières aux opérations concourant à la production ou à la livraison d'immeubles](section-iv)
- [Section V : Régimes spéciaux](section-v)
