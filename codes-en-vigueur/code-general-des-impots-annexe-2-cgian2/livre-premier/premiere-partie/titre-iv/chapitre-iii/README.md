# Chapitre III : Autres droits et taxes

- [00I : Prélèvement sur les sommes versées par les organismes d'assurance et assimilés à raison des contrats d'assurances en cas de décès](00i)
- [0I : Taxe sur les conventions d'assurance](0i)
- [II : Taxe sur les véhicules des sociétés](ii)
