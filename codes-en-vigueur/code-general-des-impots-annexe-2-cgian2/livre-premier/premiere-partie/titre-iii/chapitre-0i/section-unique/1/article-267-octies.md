# Article 267 octies

Les déclarations prévues aux articles 407 et 408 de récolte et de stocks de vins prévues aux articles 407 et 408 du code général des impôts et qui renferment en outre les indications fixées par décrets (1) sont établies sur des imprimés mis à la disposition des déclarants et déposées à la mairie qui en donne récépissé. Une copie de ces déclarations reste en mairie et doit être communiquée à tout requérant.

Les autres exemplaires sont transmis, par les soins de la mairie, au service des douanes et droits indirects dans le ressort duquel sont situées les exploitations intéressées. Ce service ne peut délivrer des titres de mouvement au déclarant pour une quantité de vin supérieure à celle qu'il a déclarée.

Le relevé nominatif des déclarations établi d'après leur ordre de dépôt, est affiché à la mairie.

Dès le début de la récolte, au fur et à mesure des nécessités de la vente, des déclarations partielles peuvent être faites dans les mêmes conditions que ci-dessus, sauf l'affichage qui a lieu après la déclaration totale.
