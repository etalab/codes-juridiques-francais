# Imputation prévue aux I et II de l'article 199 ter du code général des impôts

- [Article 92](article-92.md)
- [Article 93](article-93.md)
- [Article 94](article-94.md)
- [Article 95](article-95.md)
