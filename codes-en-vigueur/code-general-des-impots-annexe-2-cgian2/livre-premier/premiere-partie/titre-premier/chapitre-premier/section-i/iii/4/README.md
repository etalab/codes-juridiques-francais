# 4 : Emission d'obligations en France par les organismes étrangers ou internationaux. Régime spécial des titres émis avant le 1er janvier 1987

- [Article 51](article-51.md)
- [Article 52](article-52.md)
- [Article 53](article-53.md)
