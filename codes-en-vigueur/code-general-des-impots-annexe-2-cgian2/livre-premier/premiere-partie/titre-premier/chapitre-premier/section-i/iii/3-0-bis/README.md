# 3-0 bis : Revenus des valeurs mobilières émises hors de France et revenus assimilés

- [Article 50 bis](article-50-bis.md)
- [Article 50 ter](article-50-ter.md)
- [Article 50 quater](article-50-quater.md)
- [Article 50 quinquies](article-50-quinquies.md)
- [Article 50 sexies](article-50-sexies.md)
- [Article 50 septies](article-50-septies.md)
