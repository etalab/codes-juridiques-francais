# Article 161

I. ― Conformément aux dispositions de l'article R. 313-2 du code de la construction et de l'habitation, les employeurs redevables de la participation prévue à l'article L. 313-1 du même code sont tenus d'en mentionner, chaque année, l'assiette sur la déclaration prévue à l'article 87 du code général des impôts.

II. ― Conformément aux dispositions de l'article R. 716-28 du code rural et de la pêche maritime, les employeurs redevables de la participation prévue à l'article L. 716-2 du même code sont tenus d'en mentionner, chaque année, l'assiette sur la déclaration prévue à l'article 87 du code général des impôts.
