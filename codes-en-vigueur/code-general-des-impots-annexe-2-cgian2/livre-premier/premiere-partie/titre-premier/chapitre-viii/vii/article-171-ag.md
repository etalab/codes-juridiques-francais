# Article 171 AG

I. L'état individuel prévu à l'article 171 AF comporte, lorsque le copropriétaire n'est pas un fonds de placement quirataire, les renseignements suivants :

a) La date de la convention de copropriété, la dénomination et l'adresse de cette copropriété ainsi que l'identité et l'adresse de son gérant ;

b) Pour les navires déjà francisés, le nom et la désignation du navire, la date et le numéro de sa fiche matricule prévue à l'article 95 du décret n° 67-967 du 27 octobre 1967 modifié portant statut des navires et autres bâtiments de mer, l'adresse du bureau des douanes du port d'attache ainsi que les dates de livraison et de début d'exploitation du navire.

Pour les navires non encore francisés et relevant des dispositions de l'article L. 5114-2 du code des transports, le nom et la désignation du navire, la date et le numéro de sa fiche matricule et l'adresse du bureau des douanes du port d'attache ainsi que les dates prévues de livraison et de début d'exploitation du navire.

Pour les navires non encore francisés et ne relevant pas de l'article L. 5114-2 du code des transports, le nom et la désignation du navire, le contrat de construction, éventuellement traduit en langue française et comportant notamment la date de livraison, l'adresse du bureau des douanes du port d'attache où le navire sera francisé ainsi que la date prévue de début d'exploitation du navire ;

c) S'il y a lieu, les dates de signature et d'effet du contrat d'affrètement ainsi que les éléments permettant l'identification de l'affréteur : dénomination sociale, objet social et siège social ;

d) Le numéro des parts souscrites, la date de leur souscription, le montant et la date des versements effectués jusqu'à la livraison du navire ;

e) S'il y a lieu, la date, le prix et le numéro des parts cédées par le copropriétaire ainsi que les noms et adresses des cessionnaires.

II. L'état individuel prévu à l'article 171 AF comporte, dans le cas où le copropriétaire du navire est un fonds de placement quirataire :

a) Les renseignements dont la liste figure au I ;

b) L'indication du montant de la quote-part détenue par le contribuable dans le fonds de placement quirataire ;

c) S'il y a lieu, la date, le prix, le nombre de parts du fonds de placement quirataire cédées par le contribuable et l'identité du cessionnaire.
