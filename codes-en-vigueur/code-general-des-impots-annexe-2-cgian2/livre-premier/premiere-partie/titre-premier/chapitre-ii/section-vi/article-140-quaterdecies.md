# Article 140 quaterdecies

Le taux de rétrocession mentionné dix-neuvième alinéa (5°) du I et au 3° du I bis de l'article 217 undecies du code général des impôts est calculé par le rapport existant entre :

a) Au numérateur, la différence entre, d'une part, le montant hors taxe de l'investissement diminué de la fraction de son prix de revient financée par une subvention publique et, d'autre part, la valeur actualisée de l'ensemble des sommes mises à la charge du locataire lui permettant d'obtenir la disposition du bien et d'en acquérir la propriété au terme de la location ;

b) Au dénominateur, la valeur actualisée des économies d'impôt sur les sociétés procurée par la déduction pratiquée au titre de l'investissement, par l'imputation du déficit procuré par la location du bien acquis et de la moins-value réalisée lors de la cession de ce bien et des titres de la société bailleresse.

La valeur actualisée des sommes payées par le locataire est déterminée en retenant un taux d'actualisation égal à la moyenne pondérée, en fonction du montant des emprunts, des taux d'intérêts des emprunts souscrits pour le financement de l'investissement par le bailleur. Lorsque les emprunts sont rémunérés par un taux d'intérêt variable, seul le premier taux connu est retenu pour le calcul de la moyenne. Il n'est pas tenu compte pour ce calcul de l'avantage consenti en application de ces mêmes dispositions par les associés ou membres de cette entreprise.
