# Section III : Imputation de l'impôt sur le revenu (retenue à la source) sur le montant de l'impôt sur les sociétés

- [Article 135](article-135.md)
- [Article 136](article-136.md)
- [Article 138](article-138.md)
- [Article 139](article-139.md)
- [Article 140](article-140.md)
