# Article 163 undecies

Pour l'application de l'article 235 ter F du code général des impôts, peuvent se substituer au comité d'entreprise d'autres instances de représentation du personnel conformément à l'article R. 2323-2 du code du travail ou une commission spéciale créée dans les conditions prévues aux articles R. 2323-3 et R. 2323-4 du même code.
