# Chapitre VI ter : Participation des employeurs au développement de la formation professionnelle continue

- [I : Dispositions générales](i)
- [II : Employeurs occupant dix salariés et plus](ii)
- [III : Employeurs occupant moins de dix salariés](iii)
- [IV : Régimes spéciaux](iv)
