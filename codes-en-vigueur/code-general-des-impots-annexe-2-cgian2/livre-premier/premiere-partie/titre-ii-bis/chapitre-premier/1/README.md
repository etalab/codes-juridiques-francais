# 1° : Taxes sur le chiffre d'affaires

- [Article 267 quinquies](article-267-quinquies.md)
- [Article 267 sexies](article-267-sexies.md)
- [Article 267 septies](article-267-septies.md)
