# Chapitre premier : Régimes réels d'imposition

- [1° : Taxes sur le chiffre d'affaires](1)
- [2° : Bénéfices industriels et commerciaux](2)
