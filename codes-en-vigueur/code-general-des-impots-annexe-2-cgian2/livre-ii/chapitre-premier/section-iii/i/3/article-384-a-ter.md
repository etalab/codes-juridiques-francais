# Article 384 A ter

I.-L'héritier, le donataire, le légataire ou le copartageant, qui désire acquitter tout ou partie des droits de mutation ou de partage dont il est redevable par la remise d'immeubles en nature de bois et forêts ou espaces naturels pouvant être incorporés au domaine forestier de l'Etat mentionnés à l'article 1716 bis du code général des impôts, doit déposer au service des impôts ou au service de la publicité foncière compétent pour enregistrer l'acte constatant la mutation, le partage ou la déclaration de succession une offre de dation à l'Etat indiquant la nature, la situation et la valeur de chacun des biens qu'il envisage de remettre à l'Etat. Il en est délivré récépissé.

L'offre de dation en paiement doit être faite dans le délai prévu pour l'enregistrement de la déclaration de la succession ou de l'acte constatant la mutation ou le partage.

II.-L'offre est adressée par le service des impôts    ou le service de la publicité foncière à une commission dont la composition est fixée par arrêté conjoint du Premier ministre, du ministre chargé du budget, du ministre chargé des forêts et du ministre chargé de la protection de la nature.

Avant de se prononcer, cette commission consulte l'Office national des forêts.

La commission émet un avis sur l'existence d'un intérêt économique, environnemental ou social du bien offert, sur les conditions de sa gestion ainsi que sur la contribution de ce bien à l'enrichissement de l'ensemble foncier auquel il pourrait être, le cas échéant, incorporé. La commission se prononce également, après avoir consulté le directeur départemental ou, le cas échéant, régional des finances publiques, sur sa valeur libératoire.

III.-Au vu de l'avis de la commission, le ministre chargé des forêts propose au ministre chargé du budget l'octroi ou le refus de l'agrément.

IV.-La décision est notifiée au demandeur par pli recommandé avec demande d'avis de réception.

V.-En cas d'agrément, le demandeur dispose du délai fixé par la décision d'agrément pour accepter par lettre simple, le cachet de la poste faisant foi, la valeur libératoire reconnue au bien offert en paiement des droits.
