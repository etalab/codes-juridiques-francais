# Article 678

Lorsque la représentation est obligatoire, le jugement doit en outre être préalablement notifié aux représentants dans la forme des notifications entre avocats, faute de quoi la notification à la partie est nulle. Mention de l'accomplissement de cette formalité doit être portée dans l'acte de notification destiné à la partie.

Toutefois, si le représentant est décédé ou a cessé d'exercer ses fonctions, la notification n'est faite qu'à la partie avec l'indication du décès ou de la cessation de fonctions.

Le délai pour exercer le recours part de la notification à la partie elle-même.
