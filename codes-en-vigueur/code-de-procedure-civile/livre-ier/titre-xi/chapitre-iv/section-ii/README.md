# Section II : Le désistement d'instance.

- [Sous-section I : Le désistement de la demande en première instance.](sous-section-i)
- [Sous-section II : Le désistement de l'appel ou de l'opposition.](sous-section-ii)
