# Section I : Le sursis à statuer.

- [Article 378](article-378.md)
- [Article 379](article-379.md)
- [Article 380](article-380.md)
- [Article 380-1](article-380-1.md)
