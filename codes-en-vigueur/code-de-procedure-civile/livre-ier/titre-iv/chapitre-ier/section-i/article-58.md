# Article 58

La requête ou la déclaration est l'acte par lequel le demandeur saisit la juridiction sans que son adversaire en ait été préalablement informé.

Elle contient à peine de nullité :

1° Pour les personnes physiques : l'indication des nom, prénoms, profession, domicile, nationalité, date et lieu de naissance du demandeur ;

Pour les personnes morales : l'indication de leur forme, leur dénomination, leur siège social et de l'organe qui les représente légalement ;

2° L'indication des nom, prénoms et domicile de la personne contre laquelle la demande est formée, ou, s'il s'agit d'une personne morale, de sa dénomination et de son siège social ;

3° L'objet de la demande.

Sauf justification d'un motif légitime tenant à l'urgence ou à la matière considérée, en particulier lorsqu'elle intéresse l'ordre public, la requête ou la déclaration qui saisit la juridiction de première instance précise également les diligences entreprises en vue de parvenir à une résolution amiable du litige.

Elle est datée et signée.
