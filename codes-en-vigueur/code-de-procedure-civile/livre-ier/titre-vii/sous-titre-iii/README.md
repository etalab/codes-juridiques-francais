# Sous-titre III : Les contestations relatives à la preuve littérale.

- [Chapitre Ier : Les contestations relatives aux actes sous seing privé.](chapitre-ier)
- [Chapitre II : L'inscription de faux contre les actes authentiques.](chapitre-ii)
- [Article 285](article-285.md)
- [Article 286](article-286.md)
