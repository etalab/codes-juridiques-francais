# Sous-section II : L'enquête ordinaire.

- [Paragraphe 1 : Détermination des faits à prouver.](paragraphe-1)
- [Paragraphe 2 : Désignation des témoins.](paragraphe-2)
- [Paragraphe 3 : Détermination du mode et du calendrier de l'enquête.](paragraphe-3)
- [Paragraphe 4 : Convocation des témoins.](paragraphe-4)
