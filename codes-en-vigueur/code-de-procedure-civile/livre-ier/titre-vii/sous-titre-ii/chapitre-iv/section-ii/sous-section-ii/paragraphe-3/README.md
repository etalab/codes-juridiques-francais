# Paragraphe 3 : Détermination du mode et du calendrier de l'enquête.

- [Article 225](article-225.md)
- [Article 226](article-226.md)
- [Article 227](article-227.md)
