# Article 514

L'exécution provisoire ne peut pas être poursuivie sans avoir été ordonnée si ce n'est pour les décisions qui en bénéficient de plein droit.

Sont notamment exécutoires de droit à titre provisoire les ordonnances de référé, les décisions qui prescrivent des mesures provisoires pour le cours de l'instance, celles qui ordonnent des mesures conservatoires ainsi que les ordonnances du juge de la mise en état qui accordent une provision au créancier.
