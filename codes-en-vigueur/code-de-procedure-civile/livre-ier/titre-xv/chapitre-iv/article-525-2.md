# Article 525-2

Lorsqu'il est saisi en application des articles 524,525 et 525-1, le premier président statue en référé, par une décision non susceptible de pourvoi.
