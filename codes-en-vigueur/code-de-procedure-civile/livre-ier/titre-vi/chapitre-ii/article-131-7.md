# Article 131-7

Dès le prononcé de la décision désignant le médiateur, le greffe de la juridiction en notifie copie par lettre simple aux parties et au médiateur.

Le médiateur fait connaître sans délai au juge son acceptation.

Dès qu'il est informé par le greffe de la consignation, il doit convoquer les parties.
