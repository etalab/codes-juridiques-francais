# Article 131-3

La durée initiale de la médiation ne peut excéder trois mois. Cette mission peut être renouvelée une fois, pour une même durée, à la demande du médiateur.
