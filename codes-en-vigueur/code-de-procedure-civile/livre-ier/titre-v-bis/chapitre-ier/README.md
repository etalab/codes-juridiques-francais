# Chapitre Ier : La transmission par le juge de la question prioritaire de constitutionnalité à la Cour de cassation

- [Article 126-1](article-126-1.md)
- [Article 126-2](article-126-2.md)
- [Article 126-3](article-126-3.md)
- [Article 126-4](article-126-4.md)
- [Article 126-5](article-126-5.md)
- [Article 126-6](article-126-6.md)
- [Article 126-7](article-126-7.md)
