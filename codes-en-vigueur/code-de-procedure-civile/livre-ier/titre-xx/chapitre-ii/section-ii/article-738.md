# Article 738

Dès réception de la commission rogatoire, il est procédé aux opérations prescrites à l'initiative de la juridiction commise ou du juge que le président de cette juridiction désigne à cet effet.
