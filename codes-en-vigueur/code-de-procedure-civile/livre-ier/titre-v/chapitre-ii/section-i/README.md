# Section I : Les exceptions d'incompétence.

- [Sous-section I : L'incompétence soulevée par les parties.](sous-section-i)
- [Sous-section II : L'appel.](sous-section-ii)
- [Sous-section III : Le contredit.](sous-section-iii)
- [Sous-section IV : L'incompétence relevée d'office.](sous-section-iv)
- [Sous-section V : Dispositions communes.](sous-section-v)
