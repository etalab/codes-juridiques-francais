# Article 83

Le secrétaire de la juridiction qui a rendu la décision notifie sans délai à la partie adverse une copie du contredit, par lettre recommandée avec demande d'avis de réception, et en informe également son représentant si elle en a un.

Il transmet simultanément au greffier en chef de la cour le dossier de l'affaire avec le contredit et une copie du jugement.
