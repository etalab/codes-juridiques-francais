# Chapitre II : Les exceptions de procédure.

- [Section I : Les exceptions d'incompétence.](section-i)
- [Section II : Les exceptions de litispendance et de connexité.](section-ii)
- [Section III : Les exceptions dilatoires.](section-iii)
- [Section IV : Les exceptions de nullité.](section-iv)
- [Article 73](article-73.md)
- [Article 74](article-74.md)
