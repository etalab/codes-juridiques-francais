# Section IV : Les exceptions de nullité.

- [Sous-section I : La nullité des actes pour vice de forme.](sous-section-i)
- [Sous-section II : La nullité des actes pour irrégularité de fond.](sous-section-ii)
