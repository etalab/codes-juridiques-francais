# Article 748-3

Les envois, remises et notifications mentionnés à l'article 748-1 font l'objet d'un avis électronique de réception adressé par le destinataire, qui indique la date et, le cas échéant, l'heure de celle-ci.

Cet avis tient lieu de visa, cachet et signature ou autre mention de réception qui sont apposés sur l'acte ou sa copie lorsque ces formalités sont prévues par le présent code.

En cas de transmission par voie électronique, il n'est pas fait application des dispositions du présent code prévoyant la transmission en plusieurs exemplaires et la restitution matérielle des actes, et pièces remis ou notifiés.
