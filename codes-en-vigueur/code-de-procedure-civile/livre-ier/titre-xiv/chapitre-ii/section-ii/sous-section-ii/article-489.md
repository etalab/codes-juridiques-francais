# Article 489

L'ordonnance de référé est exécutoire à titre provisoire. Le juge peut toutefois subordonner l'exécution provisoire à la constitution d'une garantie dans les conditions prévues aux articles 517 à 522.

En cas de nécessité, le juge peut ordonner que l'exécution aura lieu au seul vu de la minute.
