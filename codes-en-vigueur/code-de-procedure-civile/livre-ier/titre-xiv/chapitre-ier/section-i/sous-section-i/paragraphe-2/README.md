# Paragraphe 2 : Dispositions propres à la procédure orale

- [Article 446-1](article-446-1.md)
- [Article 446-2](article-446-2.md)
- [Article 446-3](article-446-3.md)
- [Article 446-4](article-446-4.md)
