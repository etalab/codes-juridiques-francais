# Article 885

La demande est formée et le tribunal saisi par déclaration faite, remise ou adressée au greffe du tribunal ou par acte d'huissier de justice adressé à ce greffe.

Lorsqu'elle est formée par déclaration au greffe, la demande comporte les mentions prescrites par l'article 58.

Dans tous les cas, la demande doit indiquer, même de façon sommaire, les motifs sur lesquels elle repose.

Les demandes soumises à publication au fichier immobilier sont faites par acte d'huissier de justice.
