# Sous-section II : La procédure à jour fixe.

- [Article 917](article-917.md)
- [Article 918](article-918.md)
- [Article 919](article-919.md)
- [Article 920](article-920.md)
- [Article 921](article-921.md)
- [Article 922](article-922.md)
- [Article 923](article-923.md)
- [Article 924](article-924.md)
- [Article 925](article-925.md)
