# Section I : La procédure avec représentation obligatoire.

- [Sous-section I : La procédure ordinaire.](sous-section-i)
- [Sous-section II : La procédure à jour fixe.](sous-section-ii)
- [Sous-section III : L'appel par requête conjointe.](sous-section-iii)
- [Sous-section IV : Dispositions communes.](sous-section-iv)
- [Article 900](article-900.md)
