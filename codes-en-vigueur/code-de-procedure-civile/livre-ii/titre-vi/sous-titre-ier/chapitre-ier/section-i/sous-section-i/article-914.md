# Article 914

Le conseiller de la mise en état est, lorsqu'il est désigné et jusqu'à son dessaisissement, seul compétent pour prononcer la caducité de l'appel, pour déclarer l'appel irrecevable et trancher à cette occasion toute question ayant trait à la recevabilité de l'appel ou pour déclarer les conclusions irrecevables en application des articles 909 et 910. Les parties ne sont plus recevables à invoquer la caducité ou l'irrecevabilité après son dessaisissement, à moins que leur cause ne survienne ou ne soit révélée postérieurement.

Les ordonnances du conseiller de la mise en état statuant sur la fin de non-recevoir tirée de l'irrecevabilité de l'appel, sur la caducité de celui-ci ou sur l'irrecevabilité des conclusions en application des articles 909 et 910 ont autorité de la chose jugée au principal.
