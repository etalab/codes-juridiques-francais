# Article 751

Les parties sont, sauf disposition contraire, tenues de constituer avocat.

La constitution de l'avocat emporte élection de domicile.
