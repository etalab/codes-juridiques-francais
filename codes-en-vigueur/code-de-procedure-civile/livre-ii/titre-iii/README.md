# Titre III : Dispositions particulières au tribunal de commerce.

- [Chapitre Ier : La procédure devant le tribunal de commerce.](chapitre-ier)
- [Chapitre II : Les pouvoirs du président.](chapitre-ii)
- [Chapitre III : Dispositions diverses.](chapitre-iii)
- [Article 853](article-853.md)
