# Article 1013

La formation restreinte de la chambre à laquelle l'affaire a été distribuée statue après un rapport oral.
