# Chapitre VI : La saisine pour avis de la Cour de cassation.

- [Article 1031-1](article-1031-1.md)
- [Article 1031-2](article-1031-2.md)
- [Article 1031-3](article-1031-3.md)
- [Article 1031-4](article-1031-4.md)
- [Article 1031-5](article-1031-5.md)
- [Article 1031-6](article-1031-6.md)
- [Article 1031-7](article-1031-7.md)
