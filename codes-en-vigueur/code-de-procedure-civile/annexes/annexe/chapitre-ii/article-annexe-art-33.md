# Article ANNEXE, art. 33

L'acte introductif d'instance et l'ordonnance du président sont signifiés quinze jours au moins avant la date fixée. La signification indique, à peine de nullité, le délai dans lequel le défendeur est tenu de constituer avocat. L'acte signifié vaut conclusions.

L'affaire est instruite selon les dispositions des articles 755,756 et 759 à 787 du code de procédure civile.
