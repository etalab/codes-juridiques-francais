# Chapitre Ier : Dispositions particulières à la matière gracieuse.

- [Section I : Dispositions communes.](section-i)
- [Section II : Dispositions propres à certaines matières.](section-ii)
