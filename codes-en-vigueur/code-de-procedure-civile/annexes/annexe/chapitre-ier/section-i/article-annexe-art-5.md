# Article ANNEXE, art. 5

Les décisions du tribunal d'instance sont notifiées par lettre recommandée avec demande d'avis de réception.

Les décisions produisent effet du jour de leur notification lorsque le délai de recours est ouvert sans limitation de durée.

Lorsque le recours est enfermé dans un délai, l'exécution est suspendue jusqu'à l'expiration du délai ou par le recours exercé dans le délai.
