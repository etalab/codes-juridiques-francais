# Paragraphe 2 : La tenue du registre

- [Article ANNEXE, art. 30-5](article-annexe-art-30-5.md)
- [Article ANNEXE, art. 30-6](article-annexe-art-30-6.md)
- [Article ANNEXE, art. 30-7](article-annexe-art-30-7.md)
- [Article ANNEXE, art. 30-8](article-annexe-art-30-8.md)
