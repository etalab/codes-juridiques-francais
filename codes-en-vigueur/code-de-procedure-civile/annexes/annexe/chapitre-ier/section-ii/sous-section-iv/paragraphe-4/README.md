# Paragraphe 4 : Le retrait de la capacité juridique et la radiation du registre

- [Article ANNEXE, art. 30-11](article-annexe-art-30-11.md)
- [Article ANNEXE, art. 30-12](article-annexe-art-30-12.md)
