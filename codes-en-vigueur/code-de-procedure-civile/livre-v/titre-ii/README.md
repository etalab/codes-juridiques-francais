# Titre II : La procédure participative

- [Chapitre Ier  : La procédure conventionnelle](chapitre-ier)
- [Chapitre II : La procédure aux fins de jugement](chapitre-ii)
- [Article 1542](article-1542.md)
- [Article 1543](article-1543.md)
