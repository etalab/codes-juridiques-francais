# Chapitre Ier : La médiation conventionnelle

- [Article 1532](article-1532.md)
- [Article 1533](article-1533.md)
- [Article 1534](article-1534.md)
- [Article 1535](article-1535.md)
