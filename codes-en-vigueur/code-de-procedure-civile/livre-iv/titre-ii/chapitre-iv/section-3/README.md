# Section 3 : Dispositions communes aux sentences rendues en France et à l'étranger

- [Article 1526](article-1526.md)
- [Article 1527](article-1527.md)
