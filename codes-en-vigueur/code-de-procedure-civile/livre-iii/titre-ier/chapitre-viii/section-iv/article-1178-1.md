# Article 1178-1

Le délai de pourvoi en cassation suspend l'exécution de la décision relative à l'adoption. Le pourvoi en cassation exercé dans ce délai est également suspensif.
