# Chapitre VIII : L'adoption

- [Section I : Le consentement à l'adoption](section-i)
- [Section II : La procédure d'adoption](section-ii)
- [Section III : La procédure relative à la révocation de l'adoption simple](section-iii)
- [Section IV : Dispositions communes](section-iv)
