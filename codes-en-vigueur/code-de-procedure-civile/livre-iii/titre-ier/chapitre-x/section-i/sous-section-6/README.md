# Sous-section 6 : La curatelle et la tutelle.

- [Paragraphe 1 : Dispositions communes aux mineurs et aux majeurs.](paragraphe-1)
- [Paragraphe 2 : Dispositions relatives aux majeurs.](paragraphe-2)
