# Paragraphe 2 : Dispositions relatives aux majeurs.

- [Article 1255](article-1255.md)
- [Article 1256](article-1256.md)
- [Article 1257](article-1257.md)
