# Article 1239-2

L'appel contre le jugement qui refuse d'ouvrir une mesure de protection à l'égard d'un majeur n'est ouvert qu'au requérant.
