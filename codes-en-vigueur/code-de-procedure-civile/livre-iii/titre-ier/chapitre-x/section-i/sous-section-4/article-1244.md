# Article 1244

Le greffier de la cour convoque à l'audience prévue pour les débats :

1° S'il en a constitué un, l'avocat du requérant, par tout moyen ;

2° L'appelant et les personnes auxquelles la décision ou la délibération a été notifiée, par lettre recommandée avec demande d'avis de réception, ainsi que, le cas échéant, leurs avocats.

Ces dernières ont le droit d'intervenir devant la cour.
