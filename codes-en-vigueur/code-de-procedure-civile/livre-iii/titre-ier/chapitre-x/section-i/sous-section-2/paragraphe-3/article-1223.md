# Article 1223

L'avocat du majeur à protéger ou protégé peut se faire délivrer copie de tout ou partie des pièces du dossier.  Il ne peut communiquer les copies ainsi obtenues ou leur reproduction à son client ou à un tiers.
