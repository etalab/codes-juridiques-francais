# Paragraphe 2 : L'instruction de la demande

- [Article 1220](article-1220.md)
- [Article 1220-1](article-1220-1.md)
- [Article 1220-2](article-1220-2.md)
- [Article 1220-3](article-1220-3.md)
- [Article 1220-4](article-1220-4.md)
- [Article 1221](article-1221.md)
- [Article 1221-1](article-1221-1.md)
- [Article 1221-2](article-1221-2.md)
