# Paragraphe 5 : Les décisions du juge des tutelles

- [Article 1226](article-1226.md)
- [Article 1227](article-1227.md)
- [Article 1228](article-1228.md)
- [Article 1229](article-1229.md)
