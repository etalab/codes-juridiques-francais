# Article 1226

A l'audience, le juge entend le requérant à l'ouverture de la mesure de protection, le majeur à protéger, sauf application par le juge des dispositions du second alinéa de l'article 432 du code civil et, le cas échéant, le ministère public.

Les avocats des parties, lorsqu'elles en ont constitué un, sont entendus en leurs observations.

L'affaire est instruite et jugée en chambre du conseil.
