# Paragraphe 1 : Dispositions communes aux mineurs et aux majeurs

- [Article 1234](article-1234.md)
- [Article 1234-1](article-1234-1.md)
- [Article 1234-2](article-1234-2.md)
- [Article 1234-3](article-1234-3.md)
- [Article 1234-4](article-1234-4.md)
- [Article 1234-5](article-1234-5.md)
- [Article 1234-6](article-1234-6.md)
- [Article 1234-7](article-1234-7.md)
- [Article 1235](article-1235.md)
