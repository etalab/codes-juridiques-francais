# Article 1043

Dans toutes les instances où s'élève à titre principal ou incident une contestation sur la nationalité, une copie de l'assignation ou, le cas échéant, une copie des conclusions soulevant la contestation sont déposées au ministère de la justice qui en délivre récépissé. Le dépôt des pièces peut être remplacé par l'envoi de ces pièces par lettre recommandée avec demande d'avis de réception.

La juridiction civile ne peut statuer sur la nationalité avant l'expiration d'un délai d'un mois à compter de la délivrance du récépissé ou de l'avis de réception. Toutefois, ce délai est de dix jours lorsque la contestation sur la nationalité a fait l'objet d'une question préjudicielle devant une juridiction statuant en matière électorale.

L'assignation est caduque, les conclusions soulevant une question de nationalité irrecevables, s'il n'est pas justifié des diligences  prévues aux alinéas qui précèdent.

Les dispositions du présent article sont applicables aux voies de recours.
