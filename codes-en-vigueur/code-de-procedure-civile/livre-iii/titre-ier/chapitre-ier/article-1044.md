# Article 1044

Le procureur de la République est tenu d'agir dans les conditions de l'article 1040 s'il en est requis par une administration publique ou par une tierce personne qui a soulevé l'exception de nationalité devant une juridiction qui a sursis à statuer dans les conditions de l'article 1042.

Le tiers requérant est mis en cause.
