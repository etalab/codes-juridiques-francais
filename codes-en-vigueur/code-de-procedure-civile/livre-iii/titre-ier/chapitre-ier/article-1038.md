# Article 1038

Le tribunal de grande instance est seul compétent pour connaître en premier ressort des contestations sur la nationalité française ou étrangère des personnes physiques, sous réserve des dispositions figurant au code de la nationalité pour les juridictions répressives comportant un jury criminel.

Les exceptions de nationalité et d'extranéité ainsi que celle d'incompétence pour en connaître sont d'ordre public. Elles peuvent être soulevées en tout état de cause et doivent être relevées d'office par le juge.
