# Chapitre IV : Les absents

- [Section I : La présomption d'absence](section-i)
- [Section II : La déclaration d'absence](section-ii)
