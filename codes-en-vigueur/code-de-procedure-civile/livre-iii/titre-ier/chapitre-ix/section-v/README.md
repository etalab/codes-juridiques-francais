# Section V : Le déplacement illicite international d'enfants

- [Article 1210-4](article-1210-4.md)
- [Article 1210-5](article-1210-5.md)
- [Article 1210-6](article-1210-6.md)
- [Article 1210-7](article-1210-7.md)
- [Article 1210-8](article-1210-8.md)
- [Article 1210-9](article-1210-9.md)
