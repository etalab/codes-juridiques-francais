# Section I : L'exercice de l'autorité parentale

- [Article 1179](article-1179.md)
- [Article 1179-1](article-1179-1.md)
- [Article 1180](article-1180.md)
- [Article 1180-1](article-1180-1.md)
- [Article 1180-2](article-1180-2.md)
- [Article 1180-3](article-1180-3.md)
- [Article 1180-4](article-1180-4.md)
- [Article 1180-5](article-1180-5.md)
