# Section IV : Le consentement à la procréation médicalement assistée

- [Article 1157-2](article-1157-2.md)
- [Article 1157-3](article-1157-3.md)
