# A. : De la décision du juge de l'application des peines fixant les modalités d'exécution du travail d'intérêt général.

- [Article R131-23](article-r131-23.md)
- [Article R131-24](article-r131-24.md)
- [Article R131-25](article-r131-25.md)
- [Article R131-26](article-r131-26.md)
- [Article R131-27](article-r131-27.md)
- [Article R131-28](article-r131-28.md)
