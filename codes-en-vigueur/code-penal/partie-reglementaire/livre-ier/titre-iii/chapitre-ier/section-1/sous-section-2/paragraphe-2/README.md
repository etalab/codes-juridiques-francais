# Paragraphe 2 : De l'exécution du travail d'intérêt général

- [A. : De la décision du juge de l'application des peines fixant les modalités d'exécution du travail d'intérêt général.](a)
- [B. : Du contrôle de l'exécution du travail d'intérêt général.](b)
