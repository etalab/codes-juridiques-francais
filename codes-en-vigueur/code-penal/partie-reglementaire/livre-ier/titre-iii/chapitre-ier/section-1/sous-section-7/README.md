# Sous-section 7 : De la peine de confiscation d'un animal.

- [Article R131-50](article-r131-50.md)
- [Article R131-51](article-r131-51.md)
- [Article R131-52](article-r131-52.md)
- [Article R131-53](article-r131-53.md)
