# Sous-section 3 : De la peine de stage de citoyenneté

- [Paragraphe 1 : Objet et durée du stage.](paragraphe-1)
- [Paragraphe 2 : Organisation du stage.](paragraphe-2)
- [Paragraphe 3 : Déroulement et fin du stage.](paragraphe-3)
- [Paragraphe 4 : Dispositions spécifiques applicables aux mineurs.](paragraphe-4)
