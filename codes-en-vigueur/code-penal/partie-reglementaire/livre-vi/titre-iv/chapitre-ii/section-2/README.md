# Section 2 : Des atteintes à la monnaie.

- [Article R642-2](article-r642-2.md)
- [Article R642-3](article-r642-3.md)
- [Article R642-4](article-r642-4.md)
