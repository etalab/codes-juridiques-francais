# Chapitre II : Des contraventions de la 2e classe contre la nation, l'Etat ou la paix publique.

- [Section 1 : Du défaut de réponse à une réquisition des autorités judiciaires ou administratives.](section-1)
- [Section 2 : Des atteintes à la monnaie.](section-2)
