# Section 1 : De la violation des dispositions réglementant la vente ou l'échange de certains objets mobiliers.

- [Article R633-1](article-r633-1.md)
- [Article R633-2](article-r633-2.md)
- [Article R633-3](article-r633-3.md)
