# Section 6 : Des atteintes aux droits de la personne résultant des fichiers ou des traitements informatiques.

- [Article R625-10](article-r625-10.md)
- [Article R625-11](article-r625-11.md)
- [Article R625-12](article-r625-12.md)
- [Article R625-13](article-r625-13.md)
