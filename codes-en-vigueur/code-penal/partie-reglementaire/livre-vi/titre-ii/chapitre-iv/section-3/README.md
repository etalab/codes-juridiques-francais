# Section 3 : De la diffamation et de l'injure non publiques présentant un caractère raciste ou discriminatoire.

- [Article R624-3](article-r624-3.md)
- [Article R624-4](article-r624-4.md)
- [Article R624-5](article-r624-5.md)
- [Article R624-6](article-r624-6.md)
