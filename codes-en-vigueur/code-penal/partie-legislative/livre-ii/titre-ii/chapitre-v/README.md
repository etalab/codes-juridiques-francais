# Chapitre V : Des atteintes à la dignité de la personne

- [Section 1 : Des discriminations.](section-1)
- [Section 1 bis : De la traite des êtres humains.](section-1-bis)
- [Section 1 ter :  De la dissimulation forcée du visage](section-1-ter)
- [Section 2 ter : De l'exploitation de la mendicité.](section-2-ter)
- [Section 2 quater : De l'exploitation de la vente à la sauvette](section-2-quater)
- [Section 3 : Des conditions de travail et d'hébergement contraires à la dignité de la personne, du travail forcé et de la réduction en servitude.](section-3)
- [Section 3 bis : Du bizutage.](section-3-bis)
- [Section 4 : Des atteintes au respect dû aux morts.](section-4)
- [Section 5 : Peines complémentaires applicables aux personnes physiques.](section-5)
- [Section 6 : Dispositions communes aux personnes physiques et aux personnes morales.](section-6)
