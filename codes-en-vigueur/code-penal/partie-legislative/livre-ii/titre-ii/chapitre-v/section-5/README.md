# Section 5 : Peines complémentaires applicables aux personnes physiques.

- [Article 225-19](article-225-19.md)
- [Article 225-20](article-225-20.md)
- [Article 225-21](article-225-21.md)
