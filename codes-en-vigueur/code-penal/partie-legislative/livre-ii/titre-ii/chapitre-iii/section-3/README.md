# Section 3 : De l'entrave aux mesures d'assistance et de l'omission de porter secours.

- [Article 223-5](article-223-5.md)
- [Article 223-6](article-223-6.md)
- [Article 223-7](article-223-7.md)
- [Article 223-7-1](article-223-7-1.md)
