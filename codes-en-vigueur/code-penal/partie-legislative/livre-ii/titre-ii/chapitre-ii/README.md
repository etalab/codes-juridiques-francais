# Chapitre II : Des atteintes à l'intégrité physique ou psychique de la personne

- [Section 1 : Des atteintes volontaires à l'intégrité de la personne.](section-1)
- [Section 2 : Des atteintes involontaires à l'intégrité de la personne.](section-2)
- [Section 3 : Des agressions sexuelles.](section-3)
- [Section 3 bis : Du harcèlement moral.](section-3-bis)
- [Section 3 ter : De l'enregistrement et de la diffusion d'images de violence.](section-3-ter)
- [Section 4 : Du trafic de stupéfiants.](section-4)
- [Section 5 : Peines complémentaires applicables aux personnes physiques.](section-5)
- [Section 6 : Dispositions communes aux personnes physiques et aux personnes morales.](section-6)
