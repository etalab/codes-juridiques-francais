# Section 4 : Du trafic de stupéfiants.

- [Article 222-34](article-222-34.md)
- [Article 222-35](article-222-35.md)
- [Article 222-36](article-222-36.md)
- [Article 222-37](article-222-37.md)
- [Article 222-38](article-222-38.md)
- [Article 222-39](article-222-39.md)
- [Article 222-40](article-222-40.md)
- [Article 222-41](article-222-41.md)
- [Article 222-42](article-222-42.md)
- [Article 222-43](article-222-43.md)
- [Article 222-43-1](article-222-43-1.md)
