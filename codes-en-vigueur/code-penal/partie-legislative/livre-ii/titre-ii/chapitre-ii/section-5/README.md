# Section 5 : Peines complémentaires applicables aux personnes physiques.

- [Article 222-44](article-222-44.md)
- [Article 222-45](article-222-45.md)
- [Article 222-46](article-222-46.md)
- [Article 222-47](article-222-47.md)
- [Article 222-48](article-222-48.md)
- [Article 222-48-1](article-222-48-1.md)
- [Article 222-48-2](article-222-48-2.md)
