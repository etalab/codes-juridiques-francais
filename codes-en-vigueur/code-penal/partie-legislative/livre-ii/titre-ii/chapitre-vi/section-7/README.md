# Section 7 : Peines complémentaires applicables aux personnes physiques.

- [Article 226-31](article-226-31.md)
- [Article 226-32](article-226-32.md)
