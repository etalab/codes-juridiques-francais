# Chapitre VII : Des atteintes aux mineurs et à la famille

- [Section 1 : Du délaissement de mineur](section-1)
- [Section 2 : De l'abandon de famille](section-2)
- [Section 2 bis : De la violation des ordonnances prises par le juge aux affaires familiales en cas de violences](section-2-bis)
- [Section 3 : Des atteintes à l'exercice de l'autorité parentale](section-3)
- [Section 4 : Des atteintes à la filiation](section-4)
- [Section 5 : De la mise en péril des mineurs](section-5)
- [Section 6 : Peines complémentaires applicables aux personnes physiques](section-6)
- [Section 7 : Peine complémentaire commune aux personnes physiques et aux personnes morales](section-7)
