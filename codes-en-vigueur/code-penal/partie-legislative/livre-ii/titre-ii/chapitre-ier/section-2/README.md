# Section 2 : Des atteintes involontaires à la vie.

- [Article 221-6](article-221-6.md)
- [Article 221-6-1](article-221-6-1.md)
- [Article 221-6-2](article-221-6-2.md)
- [Article 221-7](article-221-7.md)
