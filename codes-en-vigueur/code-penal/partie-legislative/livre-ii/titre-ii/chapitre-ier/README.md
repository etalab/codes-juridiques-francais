# Chapitre Ier : Des atteintes à la vie de la personne

- [Section 1 : Des atteintes volontaires à la vie.](section-1)
- [Section 2 : Des atteintes involontaires à la vie.](section-2)
- [Section 3 : Peines complémentaires applicables aux personnes physiques.](section-3)
