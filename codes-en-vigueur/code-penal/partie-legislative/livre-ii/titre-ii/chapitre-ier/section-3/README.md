# Section 3 : Peines complémentaires applicables aux personnes physiques.

- [Article 221-8](article-221-8.md)
- [Article 221-9](article-221-9.md)
- [Article 221-9-1](article-221-9-1.md)
- [Article 221-10](article-221-10.md)
- [Article 221-11](article-221-11.md)
- [Article 221-11-1](article-221-11-1.md)
