# Section 2 : Peines complémentaires applicables aux personnes physiques et responsabilité pénale des personnes morales.

- [Article 445-3](article-445-3.md)
- [Article 445-4](article-445-4.md)
