# Section 3 : Des manquements au devoir de probité.

- [Paragraphe 1 : De la concussion.](paragraphe-1)
- [Paragraphe 2 : De la corruption passive et du trafic d'influence commis par des personnes exerçant une fonction publique.](paragraphe-2)
- [Paragraphe 3 : De la prise illégale d'intérêts.](paragraphe-3)
- [Paragraphe 5 : De la soustraction et du détournement de biens.](paragraphe-5)
