# Chapitre II : Des atteintes à l'administration publique commises par des personnes exerçant une fonction publique.

- [Section 1 : Des abus d'autorité dirigés contre l'administration.](section-1)
- [Section 2 : Des abus d'autorité commis contre les particuliers.](section-2)
- [Section 3 : Des manquements au devoir de probité.](section-3)
- [Section 4 : Peines complémentaires.](section-4)
