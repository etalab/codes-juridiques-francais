# Sous-section 1 : De la corruption et du trafic d'influence passifs.

- [Article 435-1](article-435-1.md)
- [Article 435-2](article-435-2.md)
