# Section 3 : Des atteintes à l'autorité de la justice

- [Paragraphe 1 : Des atteintes au respect dû à la justice.](paragraphe-1)
- [Paragraphe 2 : De l'évasion.](paragraphe-2)
- [Paragraphe 3 : Des autres atteintes à l'autorité de la justice pénale.](paragraphe-3)
