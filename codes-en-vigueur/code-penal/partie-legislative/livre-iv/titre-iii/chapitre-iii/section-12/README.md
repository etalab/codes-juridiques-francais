# Section 12 : Peines complémentaires et responsabilité des personnes morales.

- [Article 433-22](article-433-22.md)
- [Article 433-23](article-433-23.md)
- [Article 433-24](article-433-24.md)
- [Article 433-25](article-433-25.md)
