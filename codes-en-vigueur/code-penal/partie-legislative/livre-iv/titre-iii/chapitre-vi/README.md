# Chapitre VI : De la participation à une activité mercenaire.

- [Article 436-1](article-436-1.md)
- [Article 436-2](article-436-2.md)
- [Article 436-3](article-436-3.md)
- [Article 436-4](article-436-4.md)
- [Article 436-5](article-436-5.md)
