# Section 1 : Des atteintes à la sécurité des forces armées et aux zones protégées intéressant la défense nationale

- [Article 413-1](article-413-1.md)
- [Article 413-2](article-413-2.md)
- [Article 413-3](article-413-3.md)
- [Article 413-4](article-413-4.md)
- [Article 413-5](article-413-5.md)
- [Article 413-6](article-413-6.md)
- [Article 413-7](article-413-7.md)
- [Article 413-8](article-413-8.md)
