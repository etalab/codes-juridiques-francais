# Section 2 : Des crimes et délits de guerre communs aux conflits armés internationaux et non internationaux

- [Sous-section 1 : Des atteintes à la personne humaine perpétrées lors d'un conflit armé international ou non international](sous-section-1)
- [Sous-section 2 : Des crimes et délits de guerre liés à la conduite des hostilités](sous-section-2)
- [Sous-section 3 : Des groupements formés ou des ententes établies en vue de préparer des crimes ou des délits de guerre](sous-section-3)
