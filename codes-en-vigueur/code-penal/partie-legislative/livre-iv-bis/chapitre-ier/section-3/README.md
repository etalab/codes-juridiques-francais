# Section 3 : Des crimes et délits de guerre propres aux conflits armés internationaux

- [Sous-section 1 : Des atteintes à la liberté et aux droits des personnes dans les conflits armés internationaux](sous-section-1)
- [Sous-section 2 : Des moyens et méthodes de combat prohibés dans un conflit armé international](sous-section-2)
