# Sous-section 1 : De la semi-liberté, du placement à l'extérieur et du placement sous surveillance électronique

- [Paragraphe 1 : De la semi-liberté et du placement à l'extérieur](paragraphe-1)
- [Paragraphe 2 : Du placement sous surveillance électronique](paragraphe-2)
