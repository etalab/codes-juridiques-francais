# Sous-section 4 : Du sursis avec mise à l'épreuve

- [Paragraphe 1 : Des conditions d'octroi du sursis avec mise à l'épreuve](paragraphe-1)
- [Paragraphe 2 : Du régime de la mise à l'épreuve](paragraphe-2)
- [Paragraphe 3 : De la révocation du sursis avec mise à l'épreuve en cas de nouvelle infraction](paragraphe-3)
- [Paragraphe 4 : Des effets du sursis avec mise à l'épreuve](paragraphe-4)
