# Sous-section 2 : Des peines applicables en cas de récidive

- [Paragraphe 1 : Personnes physiques](paragraphe-1)
- [Paragraphe 2 : Personnes morales](paragraphe-2)
- [Paragraphe 3 : Dispositions générales](paragraphe-3)
