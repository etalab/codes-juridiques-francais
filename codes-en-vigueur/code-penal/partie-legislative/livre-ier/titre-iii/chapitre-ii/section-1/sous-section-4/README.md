# Sous-section 4 : Du prononcé des peines

- [Article 132-17](article-132-17.md)
- [Article 132-18](article-132-18.md)
- [Article 132-19](article-132-19.md)
- [Article 132-20](article-132-20.md)
- [Article 132-20-1](article-132-20-1.md)
- [Article 132-21](article-132-21.md)
- [Article 132-22](article-132-22.md)
