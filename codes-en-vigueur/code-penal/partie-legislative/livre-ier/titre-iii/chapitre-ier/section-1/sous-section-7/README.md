# Sous-section 7 : Du placement sous surveillance électronique mobile à titre de mesure de sûreté

- [Article 131-36-9](article-131-36-9.md)
- [Article 131-36-10](article-131-36-10.md)
- [Article 131-36-12](article-131-36-12.md)
- [Article 131-36-12-1](article-131-36-12-1.md)
- [Article 131-36-13](article-131-36-13.md)
