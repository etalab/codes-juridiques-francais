# Section 2 : Des destructions, dégradations et détériorations dangereuses pour les personnes.

- [Article 322-5](article-322-5.md)
- [Article 322-6](article-322-6.md)
- [Article 322-6-1](article-322-6-1.md)
- [Article 322-7](article-322-7.md)
- [Article 322-8](article-322-8.md)
- [Article 322-9](article-322-9.md)
- [Article 322-10](article-322-10.md)
- [Article 322-11](article-322-11.md)
- [Article 322-11-1](article-322-11-1.md)
