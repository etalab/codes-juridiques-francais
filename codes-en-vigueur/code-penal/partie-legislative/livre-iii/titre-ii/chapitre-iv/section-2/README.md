# Section 2 : Peines complémentaires applicables aux personnes physiques et responsabilité pénale des personnes morales.

- [Article 324-7](article-324-7.md)
- [Article 324-8](article-324-8.md)
- [Article 324-9](article-324-9.md)
