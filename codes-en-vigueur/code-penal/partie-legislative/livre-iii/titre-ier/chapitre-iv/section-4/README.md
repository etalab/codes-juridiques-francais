# Section 4 : Peines complémentaires applicables aux personnes physiques et responsabilité des personnes morales.

- [Article 314-10](article-314-10.md)
- [Article 314-11](article-314-11.md)
- [Article 314-12](article-314-12.md)
- [Article 314-13](article-314-13.md)
