# Section 2 : Du détournement de gage ou d'objet saisi.

- [Article 314-5](article-314-5.md)
- [Article 314-6](article-314-6.md)
