# Article L743-2

Les articles L. 630-1 et L. 630-2 sont applicables aux faits et gestes commis à l'égard des agents en charge des contrôles prévus au présent titre.
