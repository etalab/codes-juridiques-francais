# Article L742-11

Les contrôles en matière de formation professionnelle continue peuvent être opérés soit sur place, soit sur pièces.
