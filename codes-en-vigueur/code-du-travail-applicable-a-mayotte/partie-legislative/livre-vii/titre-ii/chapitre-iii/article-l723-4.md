# Article L723-4

Le stagiaire non titulaire d'un contrat de travail bénéficie du repos dominical.
