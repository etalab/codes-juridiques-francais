# Article L723-1

Pendant la durée de sa présence en entreprise au titre de l'une des actions de formation mentionnées à l'article L. 711-2, le stagiaire non titulaire d'un contrat de travail bénéficie des dispositions du présent code relatives :

1° A la durée du travail, à l'exception de celles relatives aux heures supplémentaires ;

2° Au repos hebdomadaire ;

3° A la santé et à la sécurité.
