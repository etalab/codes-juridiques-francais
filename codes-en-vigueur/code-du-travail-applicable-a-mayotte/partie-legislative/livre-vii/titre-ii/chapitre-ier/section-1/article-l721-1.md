# Article L721-1

L'Etat, le Département de Mayotte, les employeurs et les organismes collecteurs paritaires agréés concourent au financement de la rémunération des stagiaires de la formation professionnelle.

L'institution mentionnée à l'article L. 326-6 y concourt également, le cas échéant pour le compte de l'organisme mentionné à l'article L. 327-54.
