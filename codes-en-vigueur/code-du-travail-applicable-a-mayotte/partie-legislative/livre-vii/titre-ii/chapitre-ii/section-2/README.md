# Section 2 : Prise en charge des cotisations de sécurité sociale

- [Article L722-2](article-l722-2.md)
- [Article L722-3](article-l722-3.md)
