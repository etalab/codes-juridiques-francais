# Article L722-7

Un décret en Conseil d'Etat détermine les mesures d'application du présent chapitre autres que celles qui portent fixation des taux forfaitaires prévus à l'article L. 722-3.
