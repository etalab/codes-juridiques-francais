# Article L711

La formation professionnelle tout au long de la vie constitue une obligation nationale. Elle comporte une formation initiale, comprenant notamment l'apprentissage, et des formations ultérieures destinées aux adultes et aux jeunes déjà engagés dans la vie active ou qui s'y engagent. Ces formations ultérieures constituent la formation professionnelle continue.
