# Article L511-4

En cas de cessation concertée de travail des personnels mentionnés à l'article L. 511-2, l'heure de cessation et celle de reprise du travail ne peuvent être différentes pour les diverses catégories ou pour les divers membres du personnel intéressé.

Des arrêts de travail affectant par échelonnement successif ou par roulement concerté les divers secteurs ou les diverses catégories professionnelles d'un même établissement ou service ou les différents établissements ou services d'une même entreprise ou d'un même organisme ne peuvent avoir lieu.
