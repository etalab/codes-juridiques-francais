# Article L513-5

A l'issue des réunions de la commission de conciliation, le président établit un procès-verbal qui constate l'accord, le désaccord total ou partiel des parties et leur est aussitôt notifié.

Le procès-verbal précise les points sur lesquels les parties se sont mises d'accord, le cas échéant, et ceux sur lesquels le désaccord persiste.

L'accord de conciliation est applicable dans les conditions prévues par l'article L. 512-2.
