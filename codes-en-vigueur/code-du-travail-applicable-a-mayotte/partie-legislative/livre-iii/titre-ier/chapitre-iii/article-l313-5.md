# Article L313-5

Les agents de contrôle mentionnés à l'article L. 313-1 peuvent, sur demande écrite, obtenir des organismes chargés d'un régime de protection sociale tous renseignements et tous documents utiles à l'accomplissement de leur mission de lutte contre le travail illégal. Ils transmettent à ces organismes, qui doivent en faire la demande par écrit, tous renseignements et tous documents permettant à ces derniers de recouvrer les sommes impayées ou d'obtenir le remboursement de sommes indûment versées.
