# Article L342-3

Toute infraction aux dispositions de l'article L. 330-10 est punie de trois ans d'emprisonnement et de 45 000 Euros d'amende.
