# Article L433-15

Lorsqu'un délégué titulaire cesse ses fonctions pour une des causes indiquées à l'article L. 433-14, ou se trouve momentanément absent pour une cause quelconque, son remplacement est assuré par un membre suppléant appartenant à une liste présentée par l'organisation syndicale qui a présenté la liste sur laquelle le titulaire à remplacer a été élu, la priorité étant donnée au suppléant de la même catégorie.

S'il n'existe pas de suppléant élu sur une liste présentée par l'organisation syndicale qui a présenté le titulaire, le remplacement est assuré par le candidat présenté par la même organisation et venant sur la liste immédiatement après le dernier candidat élu soit comme titulaire, soit comme suppléant et, à défaut, par le suppléant de la même catégorie qui a obtenu le plus grand nombre de voix.

Le suppléant devient titulaire jusqu'au retour de celui qu'il remplace ou jusqu'au renouvellement de l'institution.
