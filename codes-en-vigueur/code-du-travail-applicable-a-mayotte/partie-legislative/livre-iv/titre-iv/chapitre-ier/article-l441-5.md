# Article L441-5

Le comité d'entreprise a pour objet d'assurer une expression collective des salariés, permettant la prise en compte permanente de leurs intérêts dans les décisions relatives à la gestion et à l'évolution économique et financière de l'entreprise, à l'organisation du travail, à la formation professionnelle et aux techniques de production.

Il formule, à son initiative, et examine, à la demande du chef d'entreprise, toute proposition de nature à améliorer les conditions de travail, d'emploi et de formation professionnelle des salariés, leurs conditions de vie dans l'entreprise.

Il exerce ses missions sans préjudice des dispositions relatives à l'expression des salariés et aux délégués du personnel.
