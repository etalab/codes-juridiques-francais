# Article L441-6

La décision du chef d'entreprise doit être précédée par la consultation du comité d'entreprise.

Pour lui permettre de formuler un avis motivé, le comité d'entreprise doit disposer d'informations précises et écrites transmises par le chef d'entreprise, d'un délai d'examen suffisant et de la réponse motivée du chef d'entreprise à ses propres observations.

Pour l'exercice de ses missions, le comité d'entreprise a accès à l'information nécessaire détenue par les administrations publiques et les organismes agissant pour leur compte, conformément aux dispositions en vigueur concernant l'accès aux documents administratifs.

Il peut, en outre, entreprendre les études et recherches nécessaires à sa mission.
