# Article L442-15

I. - Dans les sociétés, le comité d'entreprise peut demander en justice la désignation d'un mandataire chargé de convoquer l'assemblée générale des actionnaires en cas d'urgence.

Il peut également requérir l'inscription de projets de résolutions à l'ordre du jour des assemblées.

II. - Dans les sociétés, deux membres du comité d'entreprise, désignés par le comité et appartenant l'un à la catégorie des cadres techniciens et agents de maîtrise, l'autre à la catégorie des employés et ouvriers, ou, le cas échéant, les personnes mentionnées aux troisième et quatrième alinéas de l'article L. 442-14, peuvent assister aux assemblées générales. Ils doivent, à leur demande, être entendus lors de toutes les délibérations requérant l'unanimité des associés.
