# Article L442-17

Le comité d'entreprise assure ou contrôle la gestion de toutes les activités sociales et culturelles établies dans l'entreprise prioritairement au bénéfice des salariés ou de leur famille ou participe à cette gestion, quel qu'en soit le mode de financement, dans des conditions fixées par décret en Conseil d'Etat.

Ce décret détermine notamment les conditions dans lesquelles les pouvoirs du comité d'entreprise peuvent être délégués à des organismes créés par lui et soumis à son contrôle ainsi que les règles d'octroi et d'étendue de la personnalité civile des comités d'entreprise et des organismes créés par eux. Il fixe en outre les conditions de financement des activités sociales et culturelles.

En cas de reliquat budgétaire limité à 1 % de son budget, les membres du comité d'entreprise, après s'être prononcés par un vote majoritaire, peuvent décider de verser ces fonds à une association humanitaire reconnue d'utilité publique afin de favoriser les actions locales de lutte contre l'exclusion ou des actions de réinsertion sociale.
