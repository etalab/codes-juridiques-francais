# Article L420-2

Cette commission comprend un nombre égal de membres employeurs et de membres salariés désignés par le représentant de l'Etat à Mayotte sur propositions respectives de chacune des organisations professionnelles d'employeurs et des organisations syndicales de salariés représentatives dans la collectivité départementale, au sens de l'article L. 412-1.
