# Article L420-6

La commission consultative du travail est convoquée par le représentant de l'Etat à Mayotte de sa propre initiative ou à la demande de la majorité de ses membres titulaires.

Elle se réunit au moins deux fois par an.
