# Article L412-11

Les entreprises et établissements de Mayotte sont pris en compte pour la détermination de la représentativité des organisations syndicales organisée au niveau de la branche professionnelle en application des articles L. 2122-5 à L. 2122-8 et L. 2122-10-1 à L. 2122-13 du code du travail.
