# Sous-section 3 : Représentativité syndicale au niveau de la branche professionnelle

- [Article L412-7](article-l412-7.md)
- [Article L412-8](article-l412-8.md)
- [Article L412-9](article-l412-9.md)
- [Article L412-10](article-l412-10.md)
