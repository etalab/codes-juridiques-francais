# Section 2 : Capacité civile

- [Article L413-7](article-l413-7.md)
- [Article L413-8](article-l413-8.md)
- [Article L413-9](article-l413-9.md)
- [Article L413-10](article-l413-10.md)
- [Article L413-11](article-l413-11.md)
- [Article L413-12](article-l413-12.md)
