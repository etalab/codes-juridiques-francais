# Article L413-22

Les syndicats professionnels de salariés ou d'employeurs, leurs unions et les associations de salariés ou d'employeurs mentionnés à l'article L. 413-18 tenus d'établir des comptes assurent la publicité de leurs comptes dans des conditions déterminées par décret.

Le premier alinéa est applicable au syndicat ou à l'association qui combine les comptes des organisations mentionnées à l'article L. 413-20. Ces organisations sont alors dispensées de l'obligation de publicité.
