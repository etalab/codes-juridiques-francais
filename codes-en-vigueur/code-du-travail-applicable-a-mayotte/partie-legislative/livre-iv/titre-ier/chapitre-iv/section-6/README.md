# Section 6 : Dispositions pénales

- [Article L414-60](article-l414-60.md)
- [Article L414-61](article-l414-61.md)
