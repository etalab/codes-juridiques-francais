# Article L414-24

Les modalités d'aménagement et d'utilisation par les sections syndicales des locaux mis à leur disposition sont fixées par accord avec l'employeur.
