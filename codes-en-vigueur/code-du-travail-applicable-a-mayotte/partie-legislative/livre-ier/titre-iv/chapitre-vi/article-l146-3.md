# Article L146-3

Le fait de méconnaître les dispositions de l'article L. 146-1, relatives aux économats, est puni d'une amende de 3 750 €.
