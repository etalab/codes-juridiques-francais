# Article L141-3

Les salariés liés à leur employeur par contrat de travail comportant un horaire hebdomadaire au moins égal à la durée légale fixée par l'article L. 212-1 ou à la durée fixée par la convention collective ou les usages, ont droit à une rémunération mensuelle minimale égale au produit du montant de la rémunération horaire minimale prévue à l'article L. 141-2 par le nombre d'heures correspondant à la durée hebdomadaire de travail prévue à ce contrat pour le mois considéré.

La rémunération mensuelle minimale prévue ci-dessus est réduite à due concurrence lorsque, au cours du mois considéré, le travailleur a effectué un nombre d'heures inférieur à celui qui correspond à la durée hebdomadaire de travail visée à l'article précédent pour l'un des motifs suivants :

1° Suspension du contrat de travail, notamment par suite d'absence du salarié ou par suite de maladie, d'accident ou de maternité ;

2° Effet direct d'une cessation collective de travail ;

3° Diminution collective de l'horaire de travail décidée par l'employeur et justifiée par des impératifs techniques ou économiques.

Cette rémunération mensuelle minimale est également réduite à due concurrence lorsque le contrat de travail a débuté ou s'est terminé au cours du mois considéré.
