# Article L143-15

Sans préjudice de l'application des articles L. 143-17 et L. 143-18, la créance de salaire des salariés et apprentis est privilégiée sur les meubles et immeubles, à savoir :

-les rémunérations des gens de service pour l'année échue et l'année en cours ;

-les rémunérations pour les six derniers mois des salariés et apprentis ;

-l'indemnité due en raison de l'inobservation du délai congé prévue à l'article L. 122-21 et L. 122-60 ;

-les indemnités dues pour les congés payés ;

-les indemnités de licenciement dues en application des conventions collectives de travail, des accords collectifs d'établissement, des règlements de travail, des usages, des dispositions des articles L. 122-22 et L. 122-60 pour la totalité de la portion inférieure ou égale au plafond visé à l'article L. 143-17 et pour le quart de la portion supérieure audit plafond ;

-les indemnités dues, le cas échéant, aux salariés, en application des articles L. 122-10, L. 122-61 et L. 122-63.
