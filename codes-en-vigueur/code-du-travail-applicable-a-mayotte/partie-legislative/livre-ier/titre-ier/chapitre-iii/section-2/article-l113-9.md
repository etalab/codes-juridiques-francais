# Article L113-9

Sous réserve de dispositions contractuelles ou conventionnelles plus favorables, l'apprenti perçoit un salaire déterminé en pourcentage du salaire minimum interprofessionnel garanti et dont le montant, qui varie en fonction de l'âge du bénéficiaire, est fixé pour chaque semestre d'apprentissage par arrêté du représentant de l'Etat à Mayotte pris après avis de la commission consultative du travail et du              comité mahorais de coordination de l'emploi et de la formation professionnelle.

Les modalités de rémunération des heures supplémentaires sont celles qui sont applicables au personnel de l'entreprise concernée.

L'arrêté prévu au premier alinéa fixe les conditions dans lesquelles les avantages en nature peuvent être déduits du salaire.
