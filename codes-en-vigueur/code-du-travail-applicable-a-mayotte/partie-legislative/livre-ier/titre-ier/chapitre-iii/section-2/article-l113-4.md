# Article L113-4

Nul ne peut recevoir des apprentis s'il n'est majeur ou émancipé.
