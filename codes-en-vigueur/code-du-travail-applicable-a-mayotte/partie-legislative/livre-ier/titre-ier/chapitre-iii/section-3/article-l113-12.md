# Article L113-12

Le contrat d'apprentissage doit être passé par écrit et rédigé en français.

Il est exempté de tous droits de timbre et d'enregistrement.

Un arrêté du représentant de l'Etat à Mayotte détermine les clauses et mentions qui doivent obligatoirement figurer dans le contrat.
