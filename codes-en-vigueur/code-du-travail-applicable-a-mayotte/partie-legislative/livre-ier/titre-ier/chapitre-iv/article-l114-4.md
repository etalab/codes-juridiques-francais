# Article L114-4

Le travail de nuit défini à l'article L. 213-7 est interdit pour les apprentis âgés de moins de dix-huit ans. Toutefois, des dérogations pourront être accordées, par arrêté du représentant de l'Etat à Mayotte, pour les professions de la boulangerie, de la restauration et de l'hôtellerie.
