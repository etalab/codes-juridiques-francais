# Article L116-4

Les modalités d'application du présent titre sont fixées, en tant que de besoin, par arrêté du représentant de l'Etat à Mayotte après avis de la commission consultative du travail.
