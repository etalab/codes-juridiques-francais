# Article L135-4

Les organisations ou groupements ayant la capacité d'ester en justice, dont les membres sont liés par une convention ou un accord collectif de travail, peuvent exercer toutes les actions en justice qui naissent de ce chef en faveur de leurs membres, sans avoir à justifier d'un mandat de l'intéressé, pourvu que celui-ci ait été averti et n'ait pas déclaré s'y opposer. L'intéressé peut toujours intervenir à l'instance engagée par l'organisation ou le groupement.

Lorsqu'une action née de la convention ou de l'accord collectif de travail est intentée soit par une personne, soit par une organisation ou groupement, toute organisation ou groupement ayant la capacité d'ester en justice, dont les membres sont liés par la convention ou l'accord, peut toujours intervenir à l'instance engagée, à raison de l'intérêt collectif que la solution du litige peut présenter pour ses membres.
