# Article L135-5

Les organisations ou groupements ayant la capacité d'ester en justice, liés par une convention ou un accord collectif de travail, peuvent en leur nom propre intenter contre les autres organisations ou groupements, leurs propres membres ou toute personne liée par la convention ou l'accord, toute action visant à obtenir l'exécution des engagements contractés et, le cas échéant, des dommages-intérêts.
