# Article L135-1

Sans préjudice des effets attachés à l'extension ou à l'élargissement conformément aux dispositions du chapitre III du présent titre, les conventions et accords collectifs de travail obligent tous ceux qui les ont signés ou qui sont membres des organisations ou groupements signataires.

L'adhésion à une organisation ou à un groupement signataire emporte les conséquences de l'adhésion à la convention ou à l'accord collectif de travail lui-même, sous réserve que les conditions prévues à l'article L. 132-9 soient réunies.

L'employeur qui démissionne de l'organisation ou du groupement signataire postérieurement à la signature de la convention ou de l'accord collectif demeure lié par ces textes.
