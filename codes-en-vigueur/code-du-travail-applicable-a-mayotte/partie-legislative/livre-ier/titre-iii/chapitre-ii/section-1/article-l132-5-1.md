# Article L132-5-1

La convention collective applicable est celle dont relève l'activité principale exercée par l'employeur. En cas de concours d'activités rendant incertaine l'application de ce critère pour le rattachement d'une entreprise à un champ conventionnel, les conventions collectives et les accords professionnels peuvent, par des clauses réciproques et de nature identique, prévoir les conditions dans lesquelles l'entreprise détermine les conventions et accords qui lui sont applicables.
