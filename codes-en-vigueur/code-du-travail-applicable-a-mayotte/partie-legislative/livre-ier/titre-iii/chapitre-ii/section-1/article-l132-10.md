# Article L132-10

Les conventions et accords collectifs de travail, ainsi que leurs avenants et annexes, sont déposés par la partie la plus diligente auprès de la direction du travail, de l'emploi et de la formation professionnelle.

La partie la plus diligente remet également un exemplaire de chaque convention ou accord collectif de travail au secrétariat-greffe de la juridiction du travail.

Les textes sont applicables, sauf stipulations contraires, à partir du jour qui suit leur dépôt auprès du service compétent.

Il peut être donné communication et délivré copie des textes déposés.
