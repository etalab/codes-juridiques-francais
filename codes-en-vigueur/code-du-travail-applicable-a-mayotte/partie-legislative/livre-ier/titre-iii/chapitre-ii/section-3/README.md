# Section 3 : Conventions et accords collectifs d'entreprise.

- [Article L132-17](article-l132-17.md)
- [Article L132-18](article-l132-18.md)
- [Article L132-19](article-l132-19.md)
- [Article L132-20](article-l132-20.md)
- [Article L132-21](article-l132-21.md)
- [Article L132-22](article-l132-22.md)
- [Article L132-23](article-l132-23.md)
- [Article L132-24](article-l132-24.md)
- [Article L132-25](article-l132-25.md)
- [Article L132-26](article-l132-26.md)
- [Article L132-27](article-l132-27.md)
- [Article L132-28](article-l132-28.md)
