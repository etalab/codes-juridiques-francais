# CHAPITRE III : Conventions et accords susceptibles d'être étendus et procédures d'extension et d'élargissement

- [Section 1 : Conventions et accords susceptibles d'être étendus.](section-1)
- [Section 2 : Procédures d'extension et d'élargissement.](section-2)
