# Article L127-12

Les salariés des associations intermédiaires ont droit à la formation professionnelle continue à l'initiative de l'employeur, dans le cadre du plan de formation de l'association ou des actions de formation en alternance.
