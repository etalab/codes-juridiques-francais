# Article L127-13

Lorsque l'activité de l'association intermédiaire est exercée dans les conditions de la présente sous-section, ne sont pas applicables :

1° Les sanctions relatives au marchandage, prévues à l'article L. 124-4 ;

2° Les sanctions relatives au prêt illicite de main-d'œuvre, prévues à l'article L. 124-2.

Les sanctions prévues en cas de non-respect des dispositions auxquelles renvoie l'article L. 124-3, relatives aux opérations de prêt de main-d'œuvre à but non lucratif, sont applicables.
