# Article L121-4

On ne peut engager ses services qu'à temps ou pour une entreprise déterminée.
