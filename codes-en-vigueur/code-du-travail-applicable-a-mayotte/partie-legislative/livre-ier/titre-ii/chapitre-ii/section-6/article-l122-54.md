# Article L122-54

Un décret en Conseil d'Etat détermine les modalités d'application des dispositions des articles L. 122-45 à L. 122-51 et le régime des sanctions applicables à l'employeur qui a méconnu lesdites dispositions.
