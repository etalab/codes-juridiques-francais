# Article L122-53

L'inobservation par l'employeur des dispositions des articles L. 122-45 à L. 122-51 peut donner lieu à l'attribution de dommages-intérêts au profit du bénéficiaire, en sus de l'indemnité de licenciement.

En outre, lorsque, en application des dispositions précitées, le licenciement est nul, l'employeur est tenu de verser le montant du salaire qui aurait été perçu pendant la période couverte par la nullité.
