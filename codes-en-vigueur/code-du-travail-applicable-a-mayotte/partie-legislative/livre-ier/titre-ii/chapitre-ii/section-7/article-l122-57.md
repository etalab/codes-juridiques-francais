# Article L122-57

Les dispositions de l'article L. 122-55 ne font pas obstacle à l'échéance du contrat de travail à durée déterminée.

Toutefois, lorsque ce contrat comporte une clause de renouvellement, l'employeur ne peut, au cours des périodes définies au premier alinéa dudit article, refuser le renouvellement que s'il justifie d'un motif réel et sérieux, étranger à l'accident ou à la maladie. A défaut, il devra verser au salarié une indemnité correspondant au préjudice subi. Cette indemnité ne peut être inférieure au montant des salaires et avantages que le salarié aurait reçus jusqu'au terme de la période suivante de validité du contrat prévue par la clause de renouvellement.
