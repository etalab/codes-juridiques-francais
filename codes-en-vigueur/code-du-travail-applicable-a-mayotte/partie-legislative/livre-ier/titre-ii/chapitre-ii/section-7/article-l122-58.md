# Article L122-58

A l'issue des périodes de suspension définies à l'article L. 122-55, le salarié, s'il y est déclaré apte par le médecin chargé de la surveillance médicale du travail, retrouve son emploi ou un emploi similaire assorti d'une indemnité équivalente.

Les conséquences de l'accident ou de la maladie professionnelle ne peuvent entraîner pour l'intéressé aucun retard de promotion ou d'avancement au sein de l'entreprise.
