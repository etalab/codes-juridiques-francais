# Section 4 bis : Dispositions particulières applicables aux personnes participant à des opérations de secours ou ayant souscrit un engagement dans la réserve de sécurité civile

- [Article L122-41-1](article-l122-41-1.md)
- [Article L122-41-2](article-l122-41-2.md)
