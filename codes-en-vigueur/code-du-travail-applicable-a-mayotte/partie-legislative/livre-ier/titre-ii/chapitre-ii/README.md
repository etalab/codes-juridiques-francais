# CHAPITRE II : Règles propres au contrat de travail

- [Section 1 : Contrat à durée déterminée.](section-1)
- [Section 2 : Résiliation du contrat de travail à durée indéterminée.](section-2)
- [Section 3 : Conséquences de la rupture du contrat.](section-3)
- [Section 4 : Règles particulières aux personnes intéressées par le service national, aux jeunes gens astreints aux obligations imposées par le service préparatoire et aux hommes rappelés au service national.](section-4)
- [Section 4 bis : Dispositions particulières applicables aux personnes participant à des opérations de secours ou ayant souscrit un engagement dans la réserve de sécurité civile](section-4-bis)
- [Section 6 : Protection de la maternité et éducation des enfants.](section-6)
- [Section 7 : Règles particulières aux salariés victimes d'un accident du travail ou d'une maladie professionnelle.](section-7)
- [Section 8 : Congé pour la création d'entreprise.](section-8)
