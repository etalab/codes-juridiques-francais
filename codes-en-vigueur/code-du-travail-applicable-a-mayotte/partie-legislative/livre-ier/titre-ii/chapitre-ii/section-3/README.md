# Section 3 : Conséquences de la rupture du contrat.

- [Article L122-32](article-l122-32.md)
- [Article L122-33](article-l122-33.md)
- [Article L122-34](article-l122-34.md)
