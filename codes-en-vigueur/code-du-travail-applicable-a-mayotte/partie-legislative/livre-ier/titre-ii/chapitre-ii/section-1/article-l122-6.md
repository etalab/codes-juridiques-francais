# Article L122-6

Sauf dispositions législatives expresses et à l'exclusion des dispositions concernant la rupture du contrat de travail, les dispositions légales et conventionnelles ainsi que celles qui résultent des usages, applicables aux salariés liés par un contrat de travail à durée indéterminée, s'appliquent également aux salariés liés par un contrat de travail à durée déterminée.
