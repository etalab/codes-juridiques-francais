# Article L122-37

Les dispositions de l'article L. 122-35 ci-dessus sont applicables, lors de leur renvoi dans leurs foyers, aux personnes qui, ayant accompli leur service actif, ont été maintenues au service national.
