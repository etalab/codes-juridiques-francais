# Article L122-20

Toute clause d'un contrat individuel fixant un délai-congé inférieur à celui qui résulte des dispositions de l'article L. 122-19 ou une condition d'ancienneté de services supérieure à celle qu'énoncent ces dispositions est nulle de plein droit.
