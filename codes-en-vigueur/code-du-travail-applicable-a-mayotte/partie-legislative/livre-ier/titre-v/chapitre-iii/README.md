# CHAPITRE III : Droit disciplinaire.

- [Section 1 : Sanction disciplinaire](section-1)
- [Section 2 : Procédure disciplinaire](section-2)
- [Section 3 : Contrôle juridictionnel](section-3)
- [Section 4 : Dispositions pénales](section-4)
