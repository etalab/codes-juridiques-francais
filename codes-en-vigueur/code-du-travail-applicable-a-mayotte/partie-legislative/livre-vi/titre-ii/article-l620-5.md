# Article L620-5

Les attestations, consignes, résultats et rapports relatifs aux vérifications et contrôles mis à la charge des employeurs au titre de l'hygiène et de la sécurité du travail sont datés et mentionnent l'identité de la personne ou de l'organisme chargé du contrôle ou de la vérification et celle de la personne qui a effectué le contrôle ou la vérification.
