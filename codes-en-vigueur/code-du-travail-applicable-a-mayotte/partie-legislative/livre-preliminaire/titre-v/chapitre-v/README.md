# Chapitre V : Dispositions pénales

- [Article L055-1](article-l055-1.md)
- [Article L055-2](article-l055-2.md)
