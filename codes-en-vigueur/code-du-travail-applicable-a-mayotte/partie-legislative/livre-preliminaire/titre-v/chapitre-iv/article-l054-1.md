# Article L054-1

Lorsque survient un litige relatif à l'application des articles L. 052-1 à L. 052-3 et L. 053-1 à L. 053-4, le candidat à un emploi, à un stage ou à une période de formation en entreprise où le salarié établit des faits qui permettent de présumer l'existence d'un harcèlement.

Au vu de ces éléments, il incombe à la partie défenderesse de prouver que ces agissements ne sont pas constitutifs d'un tel harcèlement et que sa décision est justifiée par des éléments objectifs étrangers à tout harcèlement.

Le juge forme sa conviction après avoir ordonné, en cas de besoin, toutes les mesures d'instruction qu'il estime utiles.
