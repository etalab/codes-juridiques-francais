# Chapitre II : Dispositions générales

- [Article L042-1](article-l042-1.md)
- [Article L042-2](article-l042-2.md)
- [Article L042-3](article-l042-3.md)
- [Article L042-4](article-l042-4.md)
- [Article L042-5](article-l042-5.md)
- [Article L042-6](article-l042-6.md)
