# Chapitre III : Différences de traitement autorisées

- [Article L033-1](article-l033-1.md)
- [Article L033-2](article-l033-2.md)
- [Article L033-3](article-l033-3.md)
- [Article L033-4](article-l033-4.md)
