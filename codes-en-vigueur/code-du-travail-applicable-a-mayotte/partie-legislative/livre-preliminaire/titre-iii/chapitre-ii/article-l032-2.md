# Article L032-2

Aucun salarié ne peut être sanctionné, licencié ou faire l'objet d'une mesure discriminatoire mentionnée à l'article L. 032-1 en raison de l'exercice normal du droit de grève.
