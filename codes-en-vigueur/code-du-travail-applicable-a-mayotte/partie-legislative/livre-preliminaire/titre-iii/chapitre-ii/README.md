# Chapitre II : Principe de non-discrimination

- [Article L032-1](article-l032-1.md)
- [Article L032-2](article-l032-2.md)
- [Article L032-3](article-l032-3.md)
- [Article L032-4](article-l032-4.md)
