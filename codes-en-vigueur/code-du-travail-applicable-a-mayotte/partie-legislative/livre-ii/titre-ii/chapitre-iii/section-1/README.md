# Section 1 : Droit au congé.

- [Article L223-1](article-l223-1.md)
- [Article L223-2](article-l223-2.md)
