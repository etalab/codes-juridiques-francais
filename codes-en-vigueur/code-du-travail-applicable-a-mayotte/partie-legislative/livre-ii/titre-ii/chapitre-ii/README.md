# CHAPITRE II : Jours fériés

- [Section 1 : Dispositions générales.](section-1)
- [Section 2 : Dispositions particulières à la journée du 1er mai.](section-2)
