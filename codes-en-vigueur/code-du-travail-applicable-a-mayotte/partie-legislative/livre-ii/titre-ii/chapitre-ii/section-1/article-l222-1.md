# Article L222-1

Les fêtes légales ci-après désignées sont des jours fériés :

- le 1er janvier ;

- le lundi de Pâques ;

- le 27 avril, commémoration de l'abolition de l'esclavage ;

- le 1er mai ;

- le 8 mai ;

- l'Ascension ;

- le lundi de Pentecôte ;

- le 14 juillet ;

- l'Assomption ;

- la Toussaint ;

- le 11 novembre ;

- le jour de Noël.

La liste qui précède ne porte atteinte ni aux stipulations des conventions ou accords collectifs de travail ou des contrats individuels de travail, ni aux usages qui prévoiraient des jours fériés supplémentaires, notamment les fêtes de Miradji, Idi-el-Fitri, Idi-el-Kabir et Maoulid.
