# Article L221-2

Il est interdit d'occuper plus de six jours par semaine un même salarié.
