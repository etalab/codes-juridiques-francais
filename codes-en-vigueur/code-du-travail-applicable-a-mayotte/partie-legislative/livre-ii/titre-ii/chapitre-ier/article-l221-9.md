# Article L221-9

Les recours pour excès de pouvoir contre les décisions prévues aux articles L. 221-7 et L. 221-8 ont un effet suspensif.
