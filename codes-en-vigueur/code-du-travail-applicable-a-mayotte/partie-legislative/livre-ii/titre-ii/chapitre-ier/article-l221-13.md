# Article L221-13

En cas de travaux urgents dont l'exécution immédiate est nécessaire pour organiser des mesures de sauvetage, pour prévenir des accidents imminents ou réparer des accidents survenus au matériel, aux installations ou aux bâtiments de l'établissement, le repos hebdomadaire peut être suspendu pour le personnel nécessaire à l'exécution des travaux urgents.

Cette faculté de suspension s'applique non seulement aux salariés de l'entreprise où les travaux urgents sont nécessaires, mais aussi à ceux d'une autre entreprise faisant les réparations pour le compte de la première. Dans cette seconde entreprise, chaque salarié doit jouir d'un repos compensateur d'une durée égale au repos supprimé. Il en est de même pour les salariés de la première entreprise préposés habituellement au service d'entretien et de réparation.
