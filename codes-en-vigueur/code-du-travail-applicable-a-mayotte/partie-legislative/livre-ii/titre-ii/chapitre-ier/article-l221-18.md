# Article L221-18

Lorsqu'un accord est intervenu entre les syndicats d'employeurs et de travailleurs d'une profession déterminée sur les conditions dans lesquelles le repos hebdomadaire est donné au personnel suivant un des modes prévus par les articles précédents, le représentant de l'Etat à Mayotte peut, par arrêté, sur la demande des syndicats intéressés, ordonner la fermeture au public des établissements de la profession pendant toute la durée de ce repos.
