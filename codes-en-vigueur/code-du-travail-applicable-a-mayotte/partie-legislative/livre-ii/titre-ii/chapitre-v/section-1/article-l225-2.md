# Article L225-2

Le ou les congés de formation économique et sociale et de formation syndicale donnent lieu à une rémunération par les employeurs, dans les entreprises de dix salariés et plus, dans des conditions prévues par voie réglementaire.

Cette rémunération est versée à la fin du mois au cours duquel la session de formation a eu lieu.

Pour l'application de cet article, l'ensemble des établissements de l'entreprise, y compris ceux situés en métropole et dans les départements d'outre-mer, est pris en compte.
