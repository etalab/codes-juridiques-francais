# Article L251-7

Les décisions du juge des référés prévues aux articles L. 251-4 et L. 251-6 ainsi que les condamnations prononcées en application de l'article L. 251-5 ne peuvent, sous réserve des dispositions de l'alinéa suivant, entraîner ni rupture, ni suspension du contrat de travail, ni aucun préjudice pécuniaire à l'encontre des salariés concernés.

Lorsque la fermeture totale et définitive entraîne le licenciement du personnel, elle donne lieu à l'indemnité de préavis et à l'indemnité de licenciement prévues en cas de rupture du contrat de travail.
