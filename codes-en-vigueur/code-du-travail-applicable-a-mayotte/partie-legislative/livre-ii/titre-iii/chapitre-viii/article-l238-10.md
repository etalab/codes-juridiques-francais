# Article L238-10

Des décrets en Conseil d'Etat fixent les mesures nécessaires à l'application du présent chapitre, notamment des articles L. 238-1, L. 238-2, L. 238-4, L. 238-5 et L. 238-6. Ils en adaptent les dispositions aux établissements mentionnés à l'article 2 du titre IV du statut général des fonctionnaires de l'Etat et des collectivités territoriales, aux entreprises ou établissements où le personnel est dispersé, ainsi qu'aux entreprises ou établissements opérant sur un même site, dans un même immeuble ou un même local.
