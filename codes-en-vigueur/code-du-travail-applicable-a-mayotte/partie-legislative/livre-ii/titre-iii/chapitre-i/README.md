# CHAPITRE I : Dispositions générales

- [Article L231-1](article-l231-1.md)
- [Article L231-2](article-l231-2.md)
- [Article L231-3](article-l231-3.md)
- [Article L231-4](article-l231-4.md)
- [Article L231-5](article-l231-5.md)
- [Article L231-6](article-l231-6.md)
- [Article L231-7](article-l231-7.md)
- [Article L231-8](article-l231-8.md)
- [Article L231-9](article-l231-9.md)
- [Article L231-10](article-l231-10.md)
- [Article L231-11](article-l231-11.md)
- [Article L231-12](article-l231-12.md)
- [Article L231-13](article-l231-13.md)
- [Article L231-14](article-l231-14.md)
- [Article L231-15](article-l231-15.md)
- [Article L231-16](article-l231-16.md)
