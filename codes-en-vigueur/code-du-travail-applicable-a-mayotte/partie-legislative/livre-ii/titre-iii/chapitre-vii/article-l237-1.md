# Article L237-1

Dans les établissements mentionnés à l'article L. 231-1, les dispositions relatives à la protection des travailleurs, salariés ou non, contre les risques d'exposition aux rayonnements ionisants sont fixées dans le respect des principes généraux de radioprotection des personnes énoncés à l'article L. 1333-1 du code de la santé publique et des obligations prévues à l'article L. 1333-10 du même code.

Les modalités d'application aux travailleurs, salariés ou non, des dispositions mentionnées à l'alinéa précédent, et notamment les valeurs limites que doivent respecter l'exposition de ces travailleurs, les références d'exposition et les niveaux qui leur sont applicables, compte tenu des situations particulières d'exposition, ainsi que les éventuelles restrictions ou interdictions concernant les activités, procédés, dispositifs ou substances dangereux pour les travailleurs, sont fixées par décret en Conseil d'Etat.
