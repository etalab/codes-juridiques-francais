# Article L234-2

Des décrets en Conseil d'Etat déterminent, pour tous les établissements mentionnés à l'article L. 231-1, y compris les mines et carrières et leurs dépendances et les entreprises de transports, les différents genres de travaux présentant des causes de danger ou excédant les forces, ou dangereux pour la moralité, et qui sont interdits aux jeunes salariés de moins de dix-huit ans et aux femmes.
