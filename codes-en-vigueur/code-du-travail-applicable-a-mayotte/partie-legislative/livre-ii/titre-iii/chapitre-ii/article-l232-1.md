# Article L232-1

Les établissements et locaux où sont employés des salariés doivent être tenus dans un état constant de propreté et présenter les conditions d'hygiène et de salubrité nécessaires à la santé du personnel.
