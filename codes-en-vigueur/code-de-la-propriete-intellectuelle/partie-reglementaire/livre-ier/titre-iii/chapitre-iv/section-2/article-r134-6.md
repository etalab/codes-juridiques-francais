# Article R134-6

L'opposition prévue au premier alinéa du I de l'article L. 134-4 s'exerce auprès de la Bibliothèque nationale de France. Dès réception, la Bibliothèque nationale de France inscrit dans la base de données publique créée par l'article L. 134-2 une mention selon laquelle le livre fait l'objet d'une déclaration d'opposition en cours d'instruction. Elle en informe les sociétés de perception et de répartition des droits agréées mentionnées à l'article L. 134-3 et leur communique les pièces produites à l'appui de l'opposition dans un délai d'un mois.

Faute pour ces sociétés d'établir dans les trois mois suivant la communication de ces pièces que la déclaration d'opposition a été présentée par une personne n'ayant pas qualité pour ce faire, la Bibliothèque nationale de France inscrit dans la base de données publique créée par l'article L. 134-2 une mention selon laquelle elles ne peuvent exercer le droit d'autoriser la reproduction et la représentation sous forme numérique du livre concerné.

Si la déclaration d'opposition émane de l'auteur du livre indisponible, la Bibliothèque nationale de France cesse de rendre accessible au public les données et informations relatives à ce livre.
