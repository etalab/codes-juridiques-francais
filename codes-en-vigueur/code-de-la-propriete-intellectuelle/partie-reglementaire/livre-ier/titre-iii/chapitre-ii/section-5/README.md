# Section 5 : Nantissement du droit d'exploitation des logiciels

- [Article R132-8](article-r132-8.md)
- [Article R132-9](article-r132-9.md)
- [Article R132-10](article-r132-10.md)
- [Article R132-11](article-r132-11.md)
- [Article R132-12](article-r132-12.md)
- [Article R132-13](article-r132-13.md)
- [Article R132-14](article-r132-14.md)
- [Article R132-15](article-r132-15.md)
- [Article R132-16](article-r132-16.md)
- [Article R132-17](article-r132-17.md)
