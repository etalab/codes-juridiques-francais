# Sous-section 3 : Dispositions relatives aux personnes morales et aux établissements ouverts au public mettant en œuvre l'exception.

- [Article R122-17](article-r122-17.md)
- [Article R122-18](article-r122-18.md)
