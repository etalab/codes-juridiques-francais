# Article R722-2

La saisie, descriptive ou réelle, prévue à l'article L. 722-4 est ordonnée par le président du tribunal de grande instance compétent pour connaître du fond.

Le président peut autoriser l'huissier à procéder à toute constatation utile en vue d'établir l'origine, la consistance et l'étendue de l'atteinte à une indication géographique.
