# Article R712-13

L'opposition à enregistrement formée par le propriétaire d'une marque antérieure ou le bénéficiaire d'un droit exclusif d'exploitation dans les conditions prévues à l'article L. 712-4 peut être présentée par l'intéressé agissant personnellement ou par l'intermédiaire d'une personne remplissant les conditions prévues à l'article R. 712-2.
