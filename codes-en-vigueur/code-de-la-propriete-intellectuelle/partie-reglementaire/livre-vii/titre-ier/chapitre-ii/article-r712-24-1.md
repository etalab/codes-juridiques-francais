# Article R712-24-1

Il est statué sur la déclaration de renouvellement dans un délai de six mois à compter de son dépôt. Ce délai est interrompu, le cas échéant, jusqu'à la régularisation de la demande prévue à l'article R. 712-11.
