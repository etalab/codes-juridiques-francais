# Article R712-23-1

Il est statué sur la demande d'enregistrement de marque dans un délai de six mois à compter de la demande. Ce délai est interrompu, le cas échéant, jusqu'à la décision statuant sur la demande d'opposition prévue à l'article L. 712-4 ou jusqu'à la régularisation de la demande prévue à l'article R. 712-11.
