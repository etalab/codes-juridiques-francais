# Titre Ier : Marques de fabrique, de commerce ou de service

- [Chapitre II : Acquisition du droit sur la marque](chapitre-ii)
- [Chapitre IV : Transmission et perte du droit sur la marque](chapitre-iv)
- [Chapitre V : Marques collectives](chapitre-v)
- [Chapitre VI : Contentieux](chapitre-vi)
- [Chapitre VII : Marque internationale et marque communautaire](chapitre-vii)
- [Chapitre VIII : Dispositions communes](chapitre-viii)
