# Article R422-42

Les actes et documents destinés aux tiers, notamment les lettres, factures, annonces et publications diverses, émanant d'une société d'exercice libéral de conseils en propriété industrielle doivent indiquer la dénomination sociale précédée ou suivie immédiatement, selon le cas :

- soit de la mention "société d'exercice libéral à responsabilité limitée de conseils en propriété industrielle" ou de la mention "S.E.L.A.R.L. de conseils en propriété industrielle" ;

- soit de la mention "société d'exercice libéral à forme anonyme de conseils en propriété industrielle" ou de la mention "S.E.L.A.F.A. de conseils en propriété industrielle" ;

- soit de la mention "société d'exercice libéral en commandite par actions de conseils en propriété industrielle" ou de la mention "S.E.L.C.A. de conseils en propriété industrielle",

ainsi que de l'énonciation du montant de son capital social, de l'adresse de son siège social, de la mention de son inscription sur la liste des conseils en propriété industrielle et de son numéro d'immatriculation au registre du commerce et des sociétés.
