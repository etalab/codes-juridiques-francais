# Article R421-9

La demande d'inscription est présentée au directeur général de l'Institut national de la propriété industrielle. Lui est jointe la justification qu'il est satisfait selon le cas aux conditions prévues à l'article R. 421-1, à l'article R. 421-1-1 ou aux articles R. 421-7 et R. 421-8.

Il est donné récépissé de la demande.
