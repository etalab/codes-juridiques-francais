# Article R411-19

La cour d'appel territorialement compétente pour connaître directement des recours formés contre les décisions du directeur général de l'Institut national de la propriété industrielle en matière de délivrance, rejet ou maintien des titres de propriété industrielle est celle du lieu où demeure la personne qui forme le recours.
