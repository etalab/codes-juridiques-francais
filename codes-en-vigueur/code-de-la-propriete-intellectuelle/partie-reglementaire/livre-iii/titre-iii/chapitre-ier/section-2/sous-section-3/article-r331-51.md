# Article R331-51

La Haute Autorité se prononce compte tenu de l'existence d'objections formulées dans les conditions prévues à l'article R. 331-49 et qui n'auraient pas été suivies de l'accord mentionné à l'article R. 331-50 ou du retrait par l'auteur de la demande de labellisation de l'œuvre concernée par l'objection.

Elle statue au plus tôt, en l'absence d'objection, au terme du délai mentionné à l'article R. 331-49, et, en présence d'une objection, au terme du délai fixé en application de l'article R. 331-50.
