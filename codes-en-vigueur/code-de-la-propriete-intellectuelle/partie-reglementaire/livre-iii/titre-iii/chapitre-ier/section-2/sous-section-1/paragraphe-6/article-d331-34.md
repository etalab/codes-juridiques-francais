# Article D331-34

La déclaration d'intérêts mentionnée à l'article L. 331-18 est établie conformément au modèle figurant en annexe au présent article.

Les déclarations sont actualisées chaque année et, en tout état de cause, dès qu'un fait nouveau intervient dans la situation professionnelle ou personnelle des déclarants.
