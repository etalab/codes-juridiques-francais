# Article R331-7

Les séances de la commission de protection des droits ne sont pas publiques.
