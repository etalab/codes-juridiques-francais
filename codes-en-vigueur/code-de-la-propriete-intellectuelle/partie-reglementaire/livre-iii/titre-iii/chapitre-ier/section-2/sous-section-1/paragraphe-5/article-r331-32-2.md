# Article R331-32-2

Les experts mentionnés à l'article L. 331-19 sont désignés par le président de la Haute Autorité sur proposition du rapporteur chargé de l'instruction de l'affaire. La décision du président définit l'objet de l'expertise, fixe le délai de sa réalisation et évalue les honoraires prévisibles correspondants.
