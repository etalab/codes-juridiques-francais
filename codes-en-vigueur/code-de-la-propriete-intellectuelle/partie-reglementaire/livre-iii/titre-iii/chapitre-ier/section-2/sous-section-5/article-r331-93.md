# Article R331-93

Lorsque la Haute Autorité modifie les spécifications fonctionnelles que les moyens de sécurisation doivent présenter en application du premier alinéa de l'article L. 331-26, elle peut demander à l'éditeur d'un moyen de sécurisation labellisé de faire procéder à une nouvelle évaluation.
