# Article R331-42

La commission de protection des droits constate par une délibération prise à la majorité d'au moins deux voix que les faits sont susceptibles de constituer l'infraction prévue à l'article R. 335-5 ou les infractions prévues aux articles L. 335-2, L. 335-3 et L. 335-4.

Toutefois, lorsque seuls deux membres de la commission sont présents et en cas de partage des voix, l'examen de la procédure est renvoyé à la première séance plénière de la commission.
