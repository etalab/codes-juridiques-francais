# Article R331-35

Pour être recevables, les saisines adressées à la commission de protection des droits de la Haute Autorité par les organismes de défense professionnelle régulièrement constitués, les sociétés de perception et de répartition des droits et le Centre national du cinéma et de l'image animée dans les conditions prévues à l'article L. 331-24 doivent comporter :

1° Les données à caractère personnel et les informations mentionnées au 1° de l'annexe du décret n° 2010-236 du 5 mars 2010 relatif au traitement automatisé de données à caractère personnel autorisé par l'article L. 331-29 du code de la propriété intellectuelle dénommé " Système de gestion des mesures pour la protection des œuvres sur internet " ;

2° Une déclaration sur l'honneur selon laquelle l'auteur de la saisine a qualité pour agir au nom du titulaire de droits sur l'œuvre ou l'objet protégé concerné par les faits.

Dès réception de la saisine, la commission de protection des droits en accuse réception par voie électronique.
