# Article R331-44

Le procureur de la République informe la commission de protection des droits des suites données à la procédure transmise.
