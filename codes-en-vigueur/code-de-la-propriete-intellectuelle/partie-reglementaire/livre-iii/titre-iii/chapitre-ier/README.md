# Chapitre Ier : Dispositions générales

- [Section 1 : Dispositions communes](section-1)
- [Section 2 : Haute Autorité pour la diffusion des œuvres et la protection des droits sur internet](section-2)
