# Article R323-5

La désignation prévue au deuxième alinéa du I de l'article L. 132-20-1 et au deuxième alinéa du I de l'article L. 217-2 se fait par lettre recommandée avec avis de réception adressée à une société de perception et de répartition des droits.

La rétractation peut être effectuée dans les conditions prévues par les statuts de cette société.
