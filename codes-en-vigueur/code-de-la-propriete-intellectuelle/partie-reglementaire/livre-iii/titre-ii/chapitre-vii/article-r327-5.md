# Article R327-5

Tout changement de statut ou de règlement général, et toute cessation de fonction d'un membre des organes délibérants et dirigeants d'une société agréée sont communiqués au ministre chargé de la culture dans un délai de quinze jours à compter de l'évènement correspondant. Le défaut de déclaration peut entraîner le retrait de l'agrément.
