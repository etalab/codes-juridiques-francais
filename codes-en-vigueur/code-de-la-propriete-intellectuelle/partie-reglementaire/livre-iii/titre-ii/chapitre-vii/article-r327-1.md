# Article R327-1

Une société régie par le titre II du livre III peut être agréée au titre de l'article L. 134-3, si elle :

1° Apporte la preuve, par la composition de ses organes délibérants et dirigeants, de la diversité de ses associés à raison des catégories et du nombre des ayants droit, de l'importance économique exprimée en revenu ou en chiffre d'affaires et de la diversité des genres éditoriaux ;

2° Apporte la preuve de la représentation paritaire des auteurs et des éditeurs parmi ses associés et au sein de ses organes dirigeants ;

3° Justifie, par tout moyen, de la qualification professionnelle de ses gérants et mandataires sociaux en raison :

a) De leur qualité d'auteur ; ou

b) De la nature et du niveau de leurs diplômes ; ou

c) De leur expérience dans le secteur de l'édition ou de la gestion d'organismes professionnels ;

4° Donne les informations nécessaires relatives :

a) A l'organisation administrative et aux conditions d'installation et d'équipement ;

b) Aux moyens mis en œuvre pour gérer les opérations relatives aux livres indisponibles au regard des dispositions des articles L. 134-1 et suivants et en informer la Bibliothèque nationale de France aux fins de mention dans la base de données publique mentionnée à l'article L. 134-2 ;

c) Aux moyens mis en œuvre pour la perception des rémunérations et le traitement des données nécessaires à la répartition de ces rémunérations ;

d) Au plan de financement et au budget prévisionnel des trois exercices suivant la demande d'agrément ;

5° Indique les dispositions qu'elle a prises ou qu'elle entend prendre pour garantir le respect des règles de répartition des rémunérations entre les auteurs et les éditeurs ainsi que le caractère équitable des règles de répartition des sommes perçues entre les ayants droit, qu'ils soient ou non parties au contrat d'édition ;

6° Donne les informations nécessaires relatives aux moyens mis en œuvre afin d'identifier et de retrouver les titulaires de droits aux fins de répartir les sommes perçues ;

7° Donne les informations nécessaires relatives aux moyens mis en œuvre pour développer des relations contractuelles permettant d'assurer la plus grande disponibilité possible des œuvres ;

8° Indique les dispositions qu'elle a prises ou qu'elle entend prendre pour veiller à la défense des intérêts légitimes des ayants droit non parties au contrat d'édition.
