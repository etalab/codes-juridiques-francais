# Section 2 : Information des acquéreurs de supports d'enregistrement soumis à la rémunération pour copie privée

- [Article R311-9](article-r311-9.md)
- [Article R311-10](article-r311-10.md)
- [Article R311-11](article-r311-11.md)
- [Article R311-12](article-r311-12.md)
