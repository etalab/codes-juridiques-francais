# Article R512-12

La demande de relevé de déchéance prévue à l'article L. 512-3 doit être formée dans un délai de deux mois à compter de la cessation de l'empêchement, et l'acte non accompli doit l'être dans le même délai. Elle n'est plus recevable après un délai préfixe de six mois décompté à partir de l'expiration du délai non observé.

La demande est présentée au directeur général de l'institut par le titulaire du dépôt, qui doit être le titulaire inscrit au Registre national des dessins et modèles si le dépôt est publié, ou son mandataire.

La demande n'est recevable qu'après paiement de la redevance prescrite.

La demande est écrite. Elle indique les faits et justifications invoqués à son appui.

La décision motivée est notifiée au demandeur.
