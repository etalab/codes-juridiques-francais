# Chapitre IV : Dispositions communes

- [Section 1 : Procédure](section-1)
- [Section 2 : Dispositions transitoires](section-2)
