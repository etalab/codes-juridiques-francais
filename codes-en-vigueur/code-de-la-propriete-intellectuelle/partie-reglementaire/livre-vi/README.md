# Livre VI : Protection des inventions et des connaissances techniques

- [Titre Ier : Brevets d'invention](titre-ier)
- [Titre II : Protection des connaissances techniques](titre-ii)
- [Titre III : Tribunaux compétents en matière d'actions relatives aux inventions et aux connaissances techniques](titre-iii)
