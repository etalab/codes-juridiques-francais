# Section 2 : L'instruction des demandes

- [Sous-section 1 : Demandes intéressant la défense nationale](sous-section-1)
- [Sous-section 2 : Division de la demande](sous-section-2)
- [Sous-section 3 : Rectification, retrait et publication de la demande](sous-section-3)
- [Sous-section 4 : Rejet de la demande](sous-section-4)
- [Sous-section 5 : Etablissement du rapport de recherche](sous-section-5)
- [Sous-section 6 : Délivrance et publication du brevet](sous-section-6)
