# Section 6 : Etablissement de l'avis documentaire

- [Article R613-60](article-r613-60.md)
- [Article R613-61](article-r613-61.md)
- [Article R613-62](article-r613-62.md)
