# Section 2 : Droit au titre

- [Sous-section 1 : Inventions de salariés.](sous-section-1)
- [Sous-section 2 : Les inventions des fonctionnaires et des agents publics.](sous-section-2)
- [Sous-section 3 : Désignation de l'inventeur et revendication de propriété.](sous-section-3)
