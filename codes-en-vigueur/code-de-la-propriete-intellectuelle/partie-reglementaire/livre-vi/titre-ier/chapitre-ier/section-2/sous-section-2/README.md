# Sous-section 2 : Les inventions des fonctionnaires et des agents publics.

- [Article Annexe art. R611-14-1](article-annexe-art-r611-14-1.md)
- [Article R611-11](article-r611-11.md)
- [Article R611-12](article-r611-12.md)
- [Article R611-14](article-r611-14.md)
- [Article R611-14-1](article-r611-14-1.md)
