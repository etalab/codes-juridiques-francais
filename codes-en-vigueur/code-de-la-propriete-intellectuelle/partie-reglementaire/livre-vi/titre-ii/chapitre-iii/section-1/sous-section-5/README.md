# Sous-section 5 : Renonciation - Déchéance.

- [Article R623-36](article-r623-36.md)
- [Article R623-37](article-r623-37.md)
