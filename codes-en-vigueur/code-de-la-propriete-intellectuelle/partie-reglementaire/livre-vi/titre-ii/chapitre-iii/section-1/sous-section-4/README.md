# Sous-section 4 : Redevances annuelles.

- [Article R623-31](article-r623-31.md)
- [Article R623-32](article-r623-32.md)
- [Article R623-33](article-r623-33.md)
- [Article R623-34](article-r623-34.md)
- [Article R623-35](article-r623-35.md)
