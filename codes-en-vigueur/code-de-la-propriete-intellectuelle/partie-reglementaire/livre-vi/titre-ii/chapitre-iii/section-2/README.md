# Section 2 : Champ d'application des certificats d'obtention végétale, durée et portée du droit de l'obtenteur

- [Article D623-58-1](article-d623-58-1.md)
- [Article R623-58](article-r623-58.md)
