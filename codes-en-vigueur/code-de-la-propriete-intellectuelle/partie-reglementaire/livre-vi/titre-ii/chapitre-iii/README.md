# Chapitre III : Obtentions végétales

- [Section 1 : Délivrance et maintien en vigueur des certificats d'obtention végétale](section-1)
- [Section 2 : Champ d'application des certificats d'obtention végétale, durée et portée du droit de l'obtenteur](section-2)
- [Section 2 bis : Semences de ferme](section-2-bis)
- [Section 3 : La retenue](section-3)
