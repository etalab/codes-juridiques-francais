# Article L521-3

L'action civile en contrefaçon se prescrit par cinq ans à compter des faits qui en sont la cause.
