# Chapitre Ier : Inscription sur la liste des personnes qualifiées en matière de propriété industrielle

- [Article L421-1](article-l421-1.md)
- [Article L421-2](article-l421-2.md)
