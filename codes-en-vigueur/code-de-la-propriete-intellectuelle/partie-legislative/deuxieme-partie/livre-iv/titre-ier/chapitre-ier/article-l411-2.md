# Article L411-2

Les recettes de l'Institut se composent de toutes redevances établies dans les conditions prévues à l'article 5 de l'ordonnance n° 59-2 du 2 janvier 1959 portant loi organique relative aux lois de finances et perçues en matière de propriété industrielle et en matière du registre du commerce et des métiers et de dépôt des actes de sociétés, ainsi que des recettes accessoires. Ces recettes doivent obligatoirement équilibrer toutes les charges de l'établissement.

Le contrôle de l'exécution du budget de l'Institut s'exerce a posteriori selon des modalités fixées par décret en Conseil d'Etat.
