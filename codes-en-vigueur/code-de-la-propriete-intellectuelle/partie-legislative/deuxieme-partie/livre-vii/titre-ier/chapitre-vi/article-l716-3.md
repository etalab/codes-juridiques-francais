# Article L716-3

Les actions civiles et les demandes relatives aux marques, y compris lorsqu'elles portent également sur une question connexe de concurrence déloyale, sont exclusivement portées devant des tribunaux de grande instance, déterminés par voie réglementaire.
