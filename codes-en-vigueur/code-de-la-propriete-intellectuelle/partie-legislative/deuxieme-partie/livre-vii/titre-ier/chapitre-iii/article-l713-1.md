# Article L713-1

L'enregistrement de la marque confère à son titulaire un droit de propriété sur cette marque pour les produits et services qu'il a désignés.
