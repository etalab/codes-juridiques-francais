# Article L713-3

Sont interdits, sauf autorisation du propriétaire, s'il peut en résulter un risque de confusion dans l'esprit du public :

a) La reproduction, l'usage ou l'apposition d'une marque, ainsi que l'usage d'une marque reproduite, pour des produits ou services similaires à ceux désignés dans l'enregistrement ;

b) L'imitation d'une marque et l'usage d'une marque imitée, pour des produits ou services identiques ou similaires à ceux désignés dans l'enregistrement.
