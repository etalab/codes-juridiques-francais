# Titre Ier : Marques de fabrique, de commerce ou de service

- [Chapitre Ier : Eléments constitutifs de la marque](chapitre-ier)
- [Chapitre II : Acquisition du droit sur la marque](chapitre-ii)
- [Chapitre III : Droits conférés par l'enregistrement](chapitre-iii)
- [Chapitre IV : Transmission et perte du droit sur la marque](chapitre-iv)
- [Chapitre V : Marques collectives](chapitre-v)
- [Chapitre VI : Contentieux](chapitre-vi)
- [Chapitre VII : La marque communautaire](chapitre-vii)
