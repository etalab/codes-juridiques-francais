# Chapitre V : Marques collectives

- [Article L715-1](article-l715-1.md)
- [Article L715-2](article-l715-2.md)
- [Article L715-3](article-l715-3.md)
