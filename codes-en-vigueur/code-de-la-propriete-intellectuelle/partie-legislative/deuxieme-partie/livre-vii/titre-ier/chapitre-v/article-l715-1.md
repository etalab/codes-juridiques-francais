# Article L715-1

La marque est dite collective lorsqu'elle peut être exploitée par toute personne respectant un règlement d'usage établi par le titulaire de l'enregistrement.

La marque collective de certification est appliquée au produit ou au service qui présente notamment, quant à sa nature, ses propriétés ou ses qualités, des caractères précisés dans son règlement.
