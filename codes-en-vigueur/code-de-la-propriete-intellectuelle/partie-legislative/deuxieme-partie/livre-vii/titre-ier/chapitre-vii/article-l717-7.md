# Article L717-7

La formule exécutoire mentionnée à l'article 82 du règlement communautaire mentionné à l'article L. 717-1 est apposée par l'Institut national de la propriété industrielle.
