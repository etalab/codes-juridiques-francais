# Chapitre IV bis :  La retenue

- [Article L614-32](article-l614-32.md)
- [Article L614-33](article-l614-33.md)
- [Article L614-34](article-l614-34.md)
- [Article L614-35](article-l614-35.md)
- [Article L614-36](article-l614-36.md)
- [Article L614-37](article-l614-37.md)
- [Article L614-38](article-l614-38.md)
- [Article L614-39](article-l614-39.md)
