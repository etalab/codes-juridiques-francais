# Section 2 : Demandes internationales

- [Paragraphe 1 : Dépôt des demandes internationales](paragraphe-1)
- [Paragraphe 2 : Effets en France des demandes internationales](paragraphe-2)
- [Article L614-17](article-l614-17.md)
