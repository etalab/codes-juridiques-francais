# Section 2 : Transmission et perte des droits

- [Article L613-8](article-l613-8.md)
- [Article L613-9](article-l613-9.md)
- [Article L613-11](article-l613-11.md)
- [Article L613-12](article-l613-12.md)
- [Article L613-13](article-l613-13.md)
- [Article L613-14](article-l613-14.md)
- [Article L613-15](article-l613-15.md)
- [Article L613-15-1](article-l613-15-1.md)
- [Article L613-16](article-l613-16.md)
- [Article L613-17](article-l613-17.md)
- [Article L613-17-1](article-l613-17-1.md)
- [Article L613-17-2](article-l613-17-2.md)
- [Article L613-18](article-l613-18.md)
- [Article L613-19](article-l613-19.md)
- [Article L613-19-1](article-l613-19-1.md)
- [Article L613-20](article-l613-20.md)
- [Article L613-21](article-l613-21.md)
- [Article L613-22](article-l613-22.md)
- [Article L613-24](article-l613-24.md)
- [Article L613-25](article-l613-25.md)
- [Article L613-26](article-l613-26.md)
- [Article L613-27](article-l613-27.md)
- [Article L613-28](article-l613-28.md)
