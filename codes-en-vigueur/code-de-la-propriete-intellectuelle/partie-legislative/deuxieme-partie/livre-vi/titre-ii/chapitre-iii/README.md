# Chapitre III : Obtention végétale

- [Section 1 : Délivrance des certificats d'obtention végétale](section-1)
- [Section 2 : Droits et obligations attachés aux certificats d'obtention végétale](section-2)
- [Section 2 bis : Semences de ferme](section-2-bis)
- [Section 3 : Actions en justice](section-3)
- [Section 4 : La retenue](section-4)
