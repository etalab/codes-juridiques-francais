# Section 4 : La retenue

- [Article L623-36](article-l623-36.md)
- [Article L623-37](article-l623-37.md)
- [Article L623-38](article-l623-38.md)
- [Article L623-39](article-l623-39.md)
- [Article L623-40](article-l623-40.md)
- [Article L623-41](article-l623-41.md)
- [Article L623-42](article-l623-42.md)
- [Article L623-43](article-l623-43.md)
- [Article L623-44](article-l623-44.md)
