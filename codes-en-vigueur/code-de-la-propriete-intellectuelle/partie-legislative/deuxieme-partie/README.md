# Deuxième partie : La propriété industrielle

- [Livre IV : Organisation administrative et professionnelle](livre-iv)
- [Livre V : Les dessins et modèles](livre-v)
- [Livre VI : Protection des inventions et des connaissances techniques](livre-vi)
- [Livre VII : Marques de fabrique, de commerce ou de service et autres signes distinctifs](livre-vii)
