# Paragraphe 2 : Dispositions particulières à l'édition d'un livre sous une forme numérique

- [Article L132-17-5](article-l132-17-5.md)
- [Article L132-17-6](article-l132-17-6.md)
- [Article L132-17-7](article-l132-17-7.md)
