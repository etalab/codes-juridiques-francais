# Paragraphe 1 : Dispositions communes à l'édition d'un livre sous une forme imprimée et sous une forme numérique

- [Article L132-17-1](article-l132-17-1.md)
- [Article L132-17-2](article-l132-17-2.md)
- [Article L132-17-3](article-l132-17-3.md)
- [Article L132-17-4](article-l132-17-4.md)
