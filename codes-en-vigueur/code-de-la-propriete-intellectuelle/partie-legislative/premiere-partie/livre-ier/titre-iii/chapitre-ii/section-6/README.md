# Section 6 : Droit d'exploitation des œuvres des journalistes

- [Article L132-35](article-l132-35.md)
- [Article L132-36](article-l132-36.md)
- [Article L132-37](article-l132-37.md)
- [Article L132-38](article-l132-38.md)
- [Article L132-39](article-l132-39.md)
- [Article L132-40](article-l132-40.md)
- [Article L132-41](article-l132-41.md)
- [Article L132-42](article-l132-42.md)
- [Article L132-42-1](article-l132-42-1.md)
- [Article L132-43](article-l132-43.md)
- [Article L132-44](article-l132-44.md)
- [Article L132-45](article-l132-45.md)
