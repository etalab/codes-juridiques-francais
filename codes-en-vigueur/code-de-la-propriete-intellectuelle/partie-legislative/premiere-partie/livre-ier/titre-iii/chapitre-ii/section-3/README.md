# Section 3 : Contrat de production audiovisuelle

- [Article L132-23](article-l132-23.md)
- [Article L132-24](article-l132-24.md)
- [Article L132-25](article-l132-25.md)
- [Article L132-26](article-l132-26.md)
- [Article L132-27](article-l132-27.md)
- [Article L132-28](article-l132-28.md)
- [Article L132-29](article-l132-29.md)
- [Article L132-30](article-l132-30.md)
