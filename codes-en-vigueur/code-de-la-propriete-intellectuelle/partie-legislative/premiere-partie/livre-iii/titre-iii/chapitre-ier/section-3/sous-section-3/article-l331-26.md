# Article L331-26

Après consultation des concepteurs de moyens de sécurisation destinés à prévenir l'utilisation illicite de l'accès à un service de communication au public en ligne, des personnes dont l'activité est d'offrir l'accès à un tel service ainsi que des sociétés régies par le titre II du présent livre et des organismes de défense professionnelle régulièrement constitués, la Haute Autorité rend publiques les spécifications fonctionnelles pertinentes que ces moyens doivent présenter.

Au terme d'une procédure d'évaluation certifiée prenant en compte leur conformité aux spécifications visées au premier alinéa et leur efficacité, la Haute Autorité établit une liste labellisant les moyens de sécurisation. Cette labellisation est périodiquement revue.

Un décret en Conseil d'Etat précise la procédure d'évaluation et de labellisation de ces moyens de sécurisation.
