# Article L331-23

Au titre de sa mission d'encouragement au développement de l'offre légale, qu'elle soit ou non commerciale, et d'observation de l'utilisation, qu'elle soit licite ou illicite, des œuvres et des objets protégés par un droit d'auteur ou par un droit voisin sur les réseaux de communications électroniques, la Haute Autorité publie chaque année des indicateurs dont la liste est fixée par décret. Elle rend compte du développement de l'offre légale dans le rapport mentionné à l'article L. 331-14.

Dans des conditions fixées par décret en Conseil d'Etat, la Haute Autorité attribue aux offres proposées par des personnes dont l'activité est d'offrir un service de communication au public en ligne un label permettant aux usagers de ce service d'identifier clairement le caractère légal de ces offres. Cette labellisation est revue périodiquement.

La Haute Autorité veille à la mise en place, à la mise en valeur et à l'actualisation d'un portail de référencement de ces mêmes offres.

Elle évalue, en outre, les expérimentations conduites dans le domaine des technologies de reconnaissance des contenus et de filtrage par les concepteurs de ces technologies, les titulaires de droits sur les œuvres et objets protégés et les personnes dont l'activité est d'offrir un service de communication au public en ligne. Elle rend compte des principales évolutions constatées en la matière, notamment pour ce qui regarde l'efficacité de telles technologies, dans son rapport annuel prévu à l'article L. 331-14.

Elle identifie et étudie les modalités techniques permettant l'usage illicite des œuvres et des objets protégés par un droit d'auteur ou par un droit voisin sur les réseaux de communications électroniques. Dans le cadre du rapport prévu à l'article L. 331-14, elle propose, le cas échéant, des solutions visant à y remédier.
