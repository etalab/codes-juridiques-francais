# Livre III : Dispositions générales relatives au droit d'auteur, aux droits voisins et droits des producteurs de bases de données

- [Titre Ier : Rémunération pour copie privée](titre-ier)
- [Titre II : Sociétés de perception et de répartition des droits](titre-ii)
- [Titre III : Prévention, procédures et sanctions](titre-iii)
- [Titre IV : Droits des producteurs de bases de données](titre-iv)
