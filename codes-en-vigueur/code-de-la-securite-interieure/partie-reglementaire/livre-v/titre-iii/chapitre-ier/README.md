# Chapitre Ier : Agents de la ville de Paris  chargés d'un service de police

- [Section 1 : Missions](section-1)
- [Section 2 : Recrutement et agrément](section-2)
- [Section 3 : Carte professionnelle, tenue et équipements](section-3)
- [Section 4 : Convention de coordination avec la police nationale](section-4)
