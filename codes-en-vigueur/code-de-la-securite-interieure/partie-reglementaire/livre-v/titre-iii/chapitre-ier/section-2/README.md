# Section 2 : Recrutement et agrément

- [Article R531-3](article-r531-3.md)
- [Article R531-4](article-r531-4.md)
- [Article R531-5](article-r531-5.md)
- [Article R531-6](article-r531-6.md)
- [Article R531-7](article-r531-7.md)
- [Article R531-8](article-r531-8.md)
- [Article R531-9](article-r531-9.md)
