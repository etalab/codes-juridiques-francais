# Article R522-1

Les gardes champêtres ont, sur le bras, une plaque de métal où sont inscrits ces mots : " La Loi " ainsi que le nom de la municipalité et celui du garde.

Ils peuvent être armés dans les conditions prévues aux articles R. 312-22, R. 312-24 et R. 312-25.
