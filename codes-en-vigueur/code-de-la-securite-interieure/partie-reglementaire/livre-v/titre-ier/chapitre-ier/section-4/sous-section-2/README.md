# Sous-section 2 : Acquisition, détention  et conservation des armes par la commune

- [Article R511-30](article-r511-30.md)
- [Article R511-31](article-r511-31.md)
- [Article R511-32](article-r511-32.md)
- [Article R511-33](article-r511-33.md)
- [Article R511-34](article-r511-34.md)
