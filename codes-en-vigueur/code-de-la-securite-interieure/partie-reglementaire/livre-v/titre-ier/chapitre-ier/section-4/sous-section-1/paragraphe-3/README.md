# Paragraphe 3 : Autorisation

- [Article R511-18](article-r511-18.md)
- [Article R511-19](article-r511-19.md)
- [Article R511-20](article-r511-20.md)
