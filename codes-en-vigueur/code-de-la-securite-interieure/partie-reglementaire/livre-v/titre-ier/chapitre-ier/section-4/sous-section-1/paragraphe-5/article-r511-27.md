# Article R511-27

Pour les séances de formation prévues par l'article R. 511-22, lors des trajets entre le poste de police municipale et le centre d'entraînement, l'agent de police municipale transporte, déchargée et rangée dans une mallette fermée à clé, ou, pour les armes mentionnées aux c et d du 1° et au 3° de l'article R. 511-12, dans un sac ou une housse spécifiquement prévus à cet effet, l'arme qui lui a été remise. Il prend toutes les précautions utiles de nature à éviter le vol de l'arme et des munitions.
