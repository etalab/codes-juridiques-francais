# Paragraphe 1 : Armes susceptibles d'être autorisées

- [Article R511-12](article-r511-12.md)
- [Article R511-13](article-r511-13.md)
