# Section 6 : Dispositions diverses

- [Article D511-41](article-d511-41.md)
- [Article R511-42](article-r511-42.md)
