# Article R546-1

Les gardes champêtres ont, sur le bras, une plaque de métal où sont inscrits ces mots : " La Loi " ainsi que le nom de la municipalité et celui du garde.

Ils peuvent être armés dans l'exercice de leurs fonctions.
