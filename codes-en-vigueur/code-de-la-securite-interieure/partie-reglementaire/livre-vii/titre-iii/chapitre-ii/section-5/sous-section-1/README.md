# Sous-section 1 : Dispositions générales

- [Article R732-19](article-r732-19.md)
- [Article R732-20](article-r732-20.md)
- [Article R732-21](article-r732-21.md)
