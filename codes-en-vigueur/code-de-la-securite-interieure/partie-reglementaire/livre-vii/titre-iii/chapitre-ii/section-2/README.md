# Section 2 : Capacité suffisante de communication radioélectrique des services de secours

- [Article R732-9](article-r732-9.md)
- [Article R732-10](article-r732-10.md)
