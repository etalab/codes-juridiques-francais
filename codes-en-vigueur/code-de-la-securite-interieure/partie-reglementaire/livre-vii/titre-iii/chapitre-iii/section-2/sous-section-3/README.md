# Sous-section 3 : Dispositions relatives aux opérations de dépollution pyrotechnique dans le cadre des cessions des biens immobiliers de l'Etat dont le ministère de la défense est l'utilisateur

- [Article R733-9](article-r733-9.md)
- [Article R733-10](article-r733-10.md)
- [Article R733-11](article-r733-11.md)
- [Article R733-12](article-r733-12.md)
