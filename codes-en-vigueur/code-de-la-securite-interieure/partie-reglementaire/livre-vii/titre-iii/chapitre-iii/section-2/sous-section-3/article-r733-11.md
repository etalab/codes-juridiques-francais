# Article R733-11

Lorsque la cession intervient en application de l'article 67 de la loi n° 2008-1425 du 27 décembre 2008 de finances pour 2009, l'acquéreur informe le ministère de la défense de l'usage futur qu'il compte faire de l'emprise, en fonction duquel seront effectuées les opérations de dépollution pyrotechnique, lorsqu'un projet d'aménagement aura été déterminé.
