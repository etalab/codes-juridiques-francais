# Article R731-3

Le plan communal de sauvegarde est adapté aux moyens dont la commune dispose. Il comprend :

1° Le document d'information communal sur les risques majeurs prévu au III de l'article R. 125-11 du code de l'environnement ;

2° Le diagnostic des risques et des vulnérabilités locales ;

3° L'organisation assurant la protection et le soutien de la population qui précise les dispositions internes prises par la commune afin d'être en mesure à tout moment d'alerter et d'informer la population et de recevoir une alerte émanant des autorités. Ces dispositions comprennent notamment un annuaire opérationnel et un règlement d'emploi des différents moyens d'alerte susceptibles d'être mis en œuvre ;

4° Les modalités de mise en œuvre de la réserve communale de sécurité civile quand cette dernière a été constituée en application de l'article L. 724-2 du présent code.
