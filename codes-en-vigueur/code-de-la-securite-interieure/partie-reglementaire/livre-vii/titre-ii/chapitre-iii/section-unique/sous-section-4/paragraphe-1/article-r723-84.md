# Article R723-84

Dès leur engagement, les sapeurs-pompiers volontaires, membres du service de santé et de secours médical, peuvent participer à l'exercice de tout ou partie des missions du service de santé et de secours médical, et notamment aux activités opérationnelles, conformément aux qualifications acquises et y compris dans l'attente de suivre les formations initiales de leur grade.
