# Article R723-17

Les sapeurs de 2e classe reçoivent l'appellation de sapeurs de 1re classe dès l'acquisition de la formation initiale et la fin de leur période probatoire.
