# Paragraphe 3 : Déroulement du volontariat

- [Sous-paragraphe  1 : Période probatoire](sous-paragraphe-1)
- [Sous-paragraphe  2 : Formation](sous-paragraphe-2)
- [Sous-paragraphe  3 : Changements de grade](sous-paragraphe-3)
- [Sous-paragraphe  4 : Discipline](sous-paragraphe-4)
- [Sous-paragraphe  5 : Renouvellement de l'engagement](sous-paragraphe-5)
- [Sous-paragraphe  6 : Suspension de l'engagement](sous-paragraphe-6)
- [Sous-paragraphe  7 : Changement d'autorité de gestion](sous-paragraphe-7)
- [Sous-paragraphe  8 : Cessation d'activité](sous-paragraphe-8)
