# Article R723-16

La formation dont bénéficie le sapeur-pompier volontaire comprend :

1° Une formation initiale adaptée aux missions effectivement confiées au sapeur-pompier volontaire et nécessaire à leur accomplissement. Dans l'attente de l'acquisition de cette formation, le sapeur-pompier volontaire peut être engagé sur des opérations au fur et à mesure de l'acquisition des unités de valeur. Dès son recrutement, il peut être engagé en qualité de sapeur-pompier volontaire apprenant, dès lors qu'il a reçu une formation aux règles de sécurité individuelle et collective sur intervention ;

2° La formation continue et de perfectionnement destinée à permettre le maintien des compétences, l'adaptation aux fonctions, l'acquisition et l'entretien des spécialités.

Le contenu et les modalités d'organisation, notamment dans le temps, de la formation, le contenu des épreuves ainsi que la liste des organismes agréés pour dispenser les enseignements correspondants sont fixés par un arrêté du ministre chargé de la sécurité civile.
