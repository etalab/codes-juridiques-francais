# Article R723-62

L'honorariat est accordé :

1° Pour les grades de caporal honoraire de sapeurs-pompiers volontaires, de sergent honoraire de sapeurs-pompiers volontaires et d'adjudant honoraire de sapeurs-pompiers volontaires, par arrêté de l'autorité de gestion ;

2° Pour les grades de lieutenant honoraire de sapeurs-pompiers volontaires et de capitaine honoraire de sapeurs-pompiers volontaires, par arrêté conjoint de l'autorité de gestion et du représentant de l'Etat dans le département ;

3° Pour les grades de commandant honoraire de sapeurs-pompiers volontaires, de lieutenant-colonel honoraire de sapeurs-pompiers volontaires et de colonel honoraire de sapeurs-pompiers volontaires, par arrêté conjoint de l'autorité de gestion et du ministre chargé de la sécurité civile.

Les sapeurs-pompiers volontaires honoraires peuvent être autorisés par la décision leur conférant l'honorariat à porter la fourragère tricolore à titre individuel.
