# Article R723-25

Les sapeurs-pompiers volontaires qui ont accompli deux années en qualité de sous-officier et qui ont acquis les compétences correspondant aux formations définies par arrêté du ministre chargé de la sécurité civile peuvent être nommés lieutenant sur proposition du directeur départemental des services d'incendie et de secours (DDSIS), après avis du comité consultatif départemental des sapeurs-pompiers volontaires (CCDSPV), sous réserve des nécessités du service.
