# Article R723-33

L'encadrement en officiers de sapeurs-pompiers volontaires respectivement du corps départemental, du corps communal ou du corps intercommunal, non compris les membres du service de santé et de secours médical, est au maximum de 15 % de l'effectif total de sapeurs-pompiers volontaires non compris les membres du service de santé et de secours médical.
