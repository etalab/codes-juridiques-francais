# Article R723-75

Les comités consultatifs communaux et intercommunaux de sapeurs-pompiers volontaires, institués respectivement auprès des communes et des établissements publics de coopération intercommunale, sont compétents pour donner leur avis sur toutes les questions relatives aux sapeurs-pompiers volontaires des corps communaux et intercommunaux, à l'exclusion de celles intéressant la discipline.

Ils sont notamment consultés sur l'engagement et le refus de renouvellement d'engagement des sapeurs-pompiers volontaires des corps communaux et intercommunaux, sur les changements de grade autres que ceux mentionnés à l'article R. 723-78 et sont informés des recours contre les décisions de refus d'engagement et de renouvellement d'engagement mentionnées à l'article R. 723-54.

Ils sont obligatoirement saisis, pour avis, du règlement intérieur du corps communal ou intercommunal.

Ils sont présidés par l'autorité territoriale compétente et comprennent un nombre égal de représentants de l'administration et de représentants élus des sapeurs-pompiers volontaires du corps communal ou intercommunal.

Lorsqu'ils doivent rendre un avis sur la situation individuelle d'un sapeur-pompier volontaire, ils ne peuvent comprendre de sapeurs-pompiers volontaires d'un grade inférieur à celui du sapeur-pompier volontaire dont la situation est examinée.

La composition et les modalités de fonctionnement des comités consultatifs communaux et intercommunaux des sapeurs-pompiers volontaires ainsi que de désignation de leurs membres sont fixées par arrêté du ministre chargé de la sécurité civile.
