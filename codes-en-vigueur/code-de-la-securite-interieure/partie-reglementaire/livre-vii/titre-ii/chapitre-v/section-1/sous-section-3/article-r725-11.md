# Article R725-11

L'agrément est retiré lorsque l'association ne se conforme pas à ses obligations ou ne remplit plus les conditions qui ont permis son agrément. La décision de retrait, prise après que l'association a été invitée à présenter ses observations, est publiée dans les mêmes conditions que la décision d'agrément.

En cas d'urgence, l'autorité de délivrance peut, par décision motivée, prononcer la suspension immédiate de l'agrément durant la procédure de retrait. La durée de la suspension ne peut excéder trois mois.
