# Article R725-7

Les associations disposant de délégations ou d'associations locales fédérées, ayant une activité régulière dans au moins vingt départements, ainsi qu'une équipe nationale permanente de responsables opérationnels, peuvent obtenir un agrément national.

Cet agrément établit la liste des délégations ou associations locales fédérées aptes à participer aux dispositifs de sécurité locaux.
