# Article R741-44

Le contenu et les modalités de réalisation du plan d'intervention et de sécurité par l'exploitant d'un système de transport public guidé sont fixées par le décret n° 2003-425 du 9 mai 2003 relatif à la sécurité des transports publics guidés.
