# Sous-section 2 : Dispositions propres à la gendarmerie nationale

- [Article R434-31](article-r434-31.md)
- [Article R434-32](article-r434-32.md)
- [Article R434-33](article-r434-33.md)
