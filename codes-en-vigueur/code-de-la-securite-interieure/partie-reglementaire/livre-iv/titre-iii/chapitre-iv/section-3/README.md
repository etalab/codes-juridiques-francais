# Section 3 : Dispositions communes à la police nationale  et à la gendarmerie nationale

- [Sous-section 1 : Relation avec la population et respect des libertés](sous-section-1)
- [Sous-section 2 : Contrôle de l'action de la police et de la gendarmerie](sous-section-2)
