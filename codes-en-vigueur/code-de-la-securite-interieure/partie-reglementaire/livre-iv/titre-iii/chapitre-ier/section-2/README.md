# Section 2 : Organisation de la coopération

- [Article R431-6](article-r431-6.md)
- [Article R431-7](article-r431-7.md)
- [Article R431-8](article-r431-8.md)
