# Chapitre Ier : Répartition des attributions et organisation de la coopération en matière de sécurité et de paix publiques

- [Section 1 : Répartition des attributions](section-1)
- [Section 2 : Organisation de la coopération](section-2)
