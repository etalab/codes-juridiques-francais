# Chapitre V : Dispositions applicables en Polynésie française

- [Article R445-1](article-r445-1.md)
- [Article R445-2](article-r445-2.md)
