# Article R413-34

Le conseil d'administration se réunit au moins trois fois par an sur convocation de son président qui fixe l'ordre du jour. Il peut également être convoqué, à la demande du ministre de l'intérieur ou de la majorité de ses membres, sur un ordre du jour déterminé.
