# Article R413-35

Le conseil d'administration ne peut valablement délibérer que si la moitié au moins de ses membres est présente. Si le quorum n'est pas atteint, le conseil est à nouveau convoqué sur le même ordre du jour dans un délai de dix jours. Il délibère alors valablement quel que soit le nombre des membres présents.

Les délibérations sont adoptées à la majorité des membres présents. En cas de partage égal des voix, celle du président est prépondérante.
