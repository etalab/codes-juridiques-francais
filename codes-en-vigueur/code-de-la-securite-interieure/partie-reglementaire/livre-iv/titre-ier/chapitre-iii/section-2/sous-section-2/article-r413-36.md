# Article R413-36

Le directeur de l'établissement, le président du conseil scientifique, le contrôleur budgétaire et l'agent comptable assistent aux séances du conseil d'administration avec voix consultative.

Le président peut appeler à participer aux séances, à titre d'expert, toute personne dont il juge la présence utile.
