# Article R413-37

Les fonctions de membre du conseil d'administration ne donnent pas lieu à rémunération. Toutefois, ces fonctions ouvrent droit au remboursement des frais de déplacement et de séjour dans les conditions prévues par la réglementation applicable aux fonctionnaires de l'Etat.
