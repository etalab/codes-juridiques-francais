# Article R413-48

Des comptables secondaires peuvent être désignés à la demande du directeur de l'institut, après avis de l'agent comptable et avec l'agrément du ministre chargé du budget.
