# Article R413-42

Le conseil scientifique est composé, outre son président nommé par arrêté du ministre de l'intérieur sur proposition du ministre chargé de la recherche :

1° De membres de droit :

a) Le directeur de la technologie au ministère chargé de la recherche ;

b) Les directeurs des laboratoires de l'établissement ou leur représentant ;

c) Le chef du service central de l'identité judiciaire de la sous-direction de la police technique et scientifique de la direction centrale de la police judiciaire ou son représentant ;

d) Le conseiller scientifique du sous-directeur chargé de la police technique et scientifique de la direction centrale de la police judiciaire ;

e) Deux représentants élus des personnels scientifiques de la police nationale en fonctions à l'institut ;

2° De personnalités qualifiées :

a) Deux choisies sur proposition du ministre chargé de l'enseignement supérieur en raison de leurs compétences en matière scientifique ;

b) Une choisie sur proposition du ministre chargé de la recherche en raison de sa compétence en matière scientifique ;

c) Deux choisies sur proposition du ministre chargé de l'industrie en raison de leurs compétences dans le domaine des normes et procédures de qualité ;

d) Deux choisies sur proposition du ministre chargé de la santé en raison de leurs compétences dans le domaine de la toxicologie et de la biologie ;

e) Une de nationalité étrangère choisie sur proposition du directeur de l'institut après avis du comité de direction, en raison de sa compétence en criminalistique ;

f) Une choisie sur proposition du garde des sceaux, ministre de la justice ;

3° D'un représentant élu des personnels actifs de la police nationale en fonctions à l'institut.
