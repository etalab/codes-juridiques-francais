# Chapitre III : Etablissements publics de la police nationale

- [Section 1 : Ecole nationale supérieure de la police](section-1)
- [Section 2 : Institut national de police scientifique](section-2)
