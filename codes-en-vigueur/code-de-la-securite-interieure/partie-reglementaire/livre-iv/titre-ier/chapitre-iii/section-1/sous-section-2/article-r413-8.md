# Article R413-8

Le conseil d'administration se réunit au moins deux fois par an sur convocation de son président, qui fixe l'ordre du jour. Il est réuni en outre à la demande du ministre de l'intérieur ou de la majorité de ses membres.
