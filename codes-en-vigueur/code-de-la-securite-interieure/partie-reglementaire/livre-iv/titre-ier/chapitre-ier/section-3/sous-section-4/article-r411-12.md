# Article R411-12

L'expérience professionnelle des adjoints de sécurité acquise pendant au moins trois ans, hors période de formation, peut donner lieu à validation dans les conditions prévues à l'article L. 335-5 du code de l'éducation.
