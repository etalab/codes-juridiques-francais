# Article R411-30

En dehors des cas mentionnés au cinquième alinéa de l'article L. 411-11 :

1° La résiliation du contrat est prononcée, sur demande écrite du réserviste volontaire de la police nationale, formulée au moins un mois avant la date souhaitée de fin de contrat ;

2° La suspension peut être prononcée, à la demande du réserviste volontaire de la police nationale, à raison de son indisponibilité, dûment justifiée, notamment pour des raisons médicales. Elle n'a pas pour effet de proroger le terme du contrat d'engagement.
