# Article D411-31

Les réservistes volontaires de la police nationale mentionnés à l'article L. 411-9 sont indemnisés en fonction des compétences requises pour l'exercice des missions qui leur sont confiées. Cette indemnisation est établie selon une classification en six niveaux.
