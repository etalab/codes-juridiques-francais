# Section 4 : Réserve civile

- [Sous-section 1 : Dispositions communes aux réservistes de la police nationale](sous-section-1)
- [Sous-section 2 : Dispositions relatives aux réservistes retraités  de la police nationale tenus à l'obligation de disponibilité](sous-section-2)
- [Sous-section 3 : Dispositions relatives aux volontaires  dans la réserve civile de la police nationale](sous-section-3)
