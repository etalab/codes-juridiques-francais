# Section 2 : Autorisation d'exercice délivrée aux exploitants individuels et aux personnes morales

- [Article R622-4](article-r622-4.md)
- [Article R622-5](article-r622-5.md)
- [Article R622-6](article-r622-6.md)
- [Article R622-7](article-r622-7.md)
- [Article R622-8](article-r622-8.md)
- [Article R622-9](article-r622-9.md)
