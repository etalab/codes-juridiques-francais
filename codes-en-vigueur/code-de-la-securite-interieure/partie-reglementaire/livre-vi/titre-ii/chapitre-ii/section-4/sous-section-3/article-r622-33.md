# Article R622-33

Les employés des agences de recherches privées se prévalant de l'exercice continu de leur activité en justifient par tout moyen auprès de leur employeur, qui leur délivre une attestation à cet effet.
