# Sous-section 3 : Dispositions spécifiques aux employés

- [Article R622-32](article-r622-32.md)
- [Article R622-33](article-r622-33.md)
- [Article R622-34](article-r622-34.md)
- [Article R622-35](article-r622-35.md)
