# Article R647-4

La commission locale d'agrément et de contrôle comprend :

1° Quatre représentants de l'Etat :

a) L'administrateur supérieur des îles Wallis et Futuna ou son représentant ;

b) Le directeur du service de la police nationale compétent ou son représentant ;

c) Le commandant de la gendarmerie pour la Nouvelle-Calédonie et les îles Wallis et Futuna ou son représentant ;

d) Le payeur du territoire ou son représentant ;

2° Le procureur de la République près le tribunal de première instance dans le ressort duquel la commission a son siège ou son représentant ;

3° Le président du tribunal administratif dans le ressort duquel la commission a son siège ou son représentant ;

4° Deux personnes issues des activités privées de sécurité mentionnées au titre Ier du présent livre, ou leurs suppléants, nommées par le ministre de l'intérieur sur proposition de l'ensemble des membres du collège désignés au 4° de l'article R. 632-2.

Le président de la commission locale peut appeler à participer aux séances, avec voix consultative, des personnes qualifiées dans le domaine du travail ou relevant de la caisse de compensation des prestations familiales de Wallis et Futuna.
