# Article R612-24

Les exploitants individuels, les dirigeants et les gérants ainsi que les employés des entreprises exerçant l'une des activités mentionnées à l'article L. 611-1 du présent code justifient de leur aptitude professionnelle par la détention :

1° Soit d'une certification professionnelle, enregistrée au répertoire national des certifications professionnelles, se rapportant à l'activité exercée ;

2° Soit d'un certificat de qualification professionnelle élaboré par la branche professionnelle de l'activité concernée, agréé par arrêté du ministre de l'intérieur ou, s'agissant des activités visant à assurer préventivement la sûreté des vols mentionnées à l'article L. 6341-2 du code des transports, par arrêté conjoint du ministre de l'intérieur et du ministre chargé des transports ;

3° Soit d'un titre reconnu par un Etat membre de l'Union européenne ou par un des Etats parties à l'accord sur l'Espace économique européen, se rapportant à l'activité exercée.
