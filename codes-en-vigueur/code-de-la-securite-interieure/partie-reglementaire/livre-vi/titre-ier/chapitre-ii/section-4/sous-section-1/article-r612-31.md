# Article R612-31

L'agrément du certificat de qualification professionnelle est délivré, pour une durée maximale de cinq ans, au regard d'un cahier des charges défini par arrêté du ministre de l'intérieur ou, s'agissant des activités visant à assurer préventivement la sûreté des vols mentionnées à l'article L. 6341-2 du code des transports, par arrêté conjoint du ministre de l'intérieur et du ministre chargé des transports. Il peut être retiré dans les mêmes conditions en cas de non-respect du cahier des charges.
