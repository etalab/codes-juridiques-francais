# Article R612-19

L'autorisation préalable d'accès à la formation professionnelle et l'autorisation provisoire d'exercice, mentionnées aux articles L. 612-22 et L. 612-23 sont délivrées, sous la forme dématérialisée d'un numéro d'enregistrement, par la commission régionale ou interrégionale d'agrément et de contrôle dans le ressort de laquelle le demandeur a son domicile.

Pour les employés des personnes morales mentionnées au 2° de l'article L. 612-1 et à l'article L. 612-11, l'autorisation provisoire est délivrée par la commission régionale d'agrément et de contrôle comportant Paris dans son ressort.

Un arrêté du ministre de l'intérieur précise les conditions dans lesquelles l'organisme ou l'employeur qui assure la formation, auquel la personne titulaire de l'autorisation préalable ou de l'autorisation provisoire a communiqué le numéro d'enregistrement, a accès aux informations mentionnées à l'article R. 612-23.
