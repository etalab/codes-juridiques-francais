# Article R612-14

La demande de carte professionnelle comprend les informations suivantes :

1° Le nom, les prénoms, la date et le lieu de naissance (ville et pays) ainsi que le domicile du demandeur ;

2° La ou les activités au titre desquelles, parmi les activités suivantes, la carte est sollicitée :

a) Surveillance humaine ou surveillance par des systèmes électroniques de sécurité ou gardiennage ;

b) Transport de fonds ;

c) Protection physique de personnes ;

d) Agent cynophile ;

e) Sûreté aéroportuaire ;

f) Vidéoprotection ;

3° Si l'activité est celle d'agent cynophile, la copie de la carte d'identification de chacun des chiens dont l'utilisation est envisagée ;

4° Si le demandeur est salarié, le nom, la raison sociale et l'adresse de l'employeur.
