# Article R616-7

Le silence gardé par la commission mentionnée au 2° de l'article R. 635-1 pendant deux mois sur la demande de carte professionnelle prévue à l'article L. 616-2 vaut rejet de celle-ci.
