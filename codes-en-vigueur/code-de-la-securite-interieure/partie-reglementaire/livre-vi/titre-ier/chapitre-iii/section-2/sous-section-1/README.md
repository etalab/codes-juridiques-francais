# Sous-section 1 : Missions

- [Paragraphe 1 : Autorisation de la surveillance des biens sur la voie publique](paragraphe-1)
- [Paragraphe 2 : Agrément des employés des entreprises de surveillance et de gardiennage pour l'inspection visuelle et la fouille des bagages à main et les palpations de sécurité](paragraphe-2)
- [Paragraphe 3 : Agrément des membres des services d'ordre affectés à la sécurité d'une manifestation sportive, récréative ou culturelle](paragraphe-3)
- [Paragraphe 4 : Utilisation de chiens](paragraphe-4)
