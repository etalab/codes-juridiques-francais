# Section 2 : Activités de surveillance et de gardiennage

- [Sous-section 1 : Missions](sous-section-1)
- [Sous-section 2 : Coordination avec les services de la police nationale et de la gendarmerie nationale](sous-section-2)
