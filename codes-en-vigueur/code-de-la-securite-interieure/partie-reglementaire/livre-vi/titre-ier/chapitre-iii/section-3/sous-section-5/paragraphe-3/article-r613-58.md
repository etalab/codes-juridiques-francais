# Article R613-58

Peuvent assister aux travaux de la commission prévue à l'article R. 613-57, avec voix consultative :

1° Un représentant de la Fédération bancaire française ;

2° Un représentant de la Fédération des entreprises de la sécurité fiduciaire ;

3° Un représentant des laboratoires reconnus par l'Etat chargés des vérifications et des tests des dispositifs de neutralisation de valeurs, désigné par le ministre de l'intérieur sur proposition de ces laboratoires.
