# Article D613-75

Les distributeurs automatiques de billets et les guichets automatiques de banque sont équipés d'un dispositif garantissant que les fonds délivrés ou déposés pourront être rendus impropres à leur destination, agréé par le ministre de l'intérieur conformément aux articles R. 613-53 à R. 613-56.

Les dispositions du présent article s'appliquent à compter de l'expiration d'un délai de trois mois suivant l'agrément d'au moins deux dispositifs :

1° A toute nouvelle implantation ou tout remplacement d'un distributeur automatique de billets ou d'un guichet automatique de banque ;

2° Dans les trois ans, pour les automates bancaires présentant un caractère prioritaire, déterminés par une convention nationale entre les représentants des établissements de crédit, des établissements financiers et de l'Etat, qui fixe les conditions et emplacements d'implantation prioritaire de ces dispositifs ou, à défaut, par un arrêté du ministre de l'intérieur ;

3° Dans les cinq ans, pour l'ensemble des automates bancaires.
