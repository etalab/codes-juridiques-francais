# Paragraphe 1 : Dispositions générales

- [Article D613-60](article-d613-60.md)
- [Article D613-61](article-d613-61.md)
- [Article D613-62](article-d613-62.md)
- [Article D613-63](article-d613-63.md)
- [Article D613-64](article-d613-64.md)
- [Article D613-65](article-d613-65.md)
- [Article D613-66](article-d613-66.md)
- [Article D613-67](article-d613-67.md)
- [Article D613-68](article-d613-68.md)
- [Article D613-69](article-d613-69.md)
- [Article D613-70](article-d613-70.md)
- [Article D613-71](article-d613-71.md)
