# Article D613-77

La commission peut être saisie pour avis :

1° Par le ministre de l'intérieur sur tout projet de texte législatif ou réglementaire en matière de transport de fonds, bijoux et métaux précieux et sur toute question soulevée, notamment par une commission départementale de la sécurité des transports de fonds, dans ce domaine ;

2° Par un tiers de ses membres, sur toute question relevant de son champ de compétence.
