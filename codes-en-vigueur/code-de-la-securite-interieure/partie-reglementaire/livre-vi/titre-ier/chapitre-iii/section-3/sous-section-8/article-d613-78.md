# Article D613-78

La commission est informée annuellement par le Conseil national des activités privées de sécurité des résultats des missions de contrôle des entreprises de transport de fonds.
