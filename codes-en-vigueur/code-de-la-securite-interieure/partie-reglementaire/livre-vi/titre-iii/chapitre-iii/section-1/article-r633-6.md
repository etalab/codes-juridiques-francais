# Article R633-6

Les dispositions des articles R. 632-20 à R. 632-23 sont applicables aux membres des commissions régionales ou interrégionales d'agrément et de contrôle.
