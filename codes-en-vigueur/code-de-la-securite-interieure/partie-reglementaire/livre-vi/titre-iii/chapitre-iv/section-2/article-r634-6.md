# Article R634-6

La personne interdite temporairement d'exercer, ou dont l'agrément ou la carte professionnelle est retiré, n'accomplit aucun acte professionnel relevant du présent livre.

Elle ne peut faire état de sa qualité de personne morale ou physique exerçant les activités relevant de ce même livre.
