# Section 2 : Organisation administrative et fonctionnement

- [Sous-section 1 : Collège](sous-section-1)
- [Sous-section 2 : Commission nationale d'agrément et de contrôle](sous-section-2)
- [Sous-section 3 : Directeur et agents du Conseil national des activités privées de sécurité](sous-section-3)
- [Sous-section 4 : Organisation financière](sous-section-4)
- [Sous-section 5 : Dispositions communes au collège et à la Commission nationale d'agrément et de contrôle](sous-section-5)
