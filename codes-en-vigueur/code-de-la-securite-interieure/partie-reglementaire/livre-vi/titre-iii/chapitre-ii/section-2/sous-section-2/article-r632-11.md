# Article R632-11

La Commission nationale d'agrément et de contrôle :

1° Veille au respect des orientations générales fixées par le collège ainsi qu'à la cohérence des décisions des commissions régionales ou interrégionales ;

2° Statue sur les recours administratifs préalables formés à l'encontre des décisions des commissions régionales et interrégionales, sur le fondement de l'article L. 633-3.

Elle rend compte de son activité au collège.
