# Article R632-22

Le président et les membres du collège et de la Commission nationale ne peuvent ni assister, ni prendre part aux délibérations lorsqu'ils ont un intérêt personnel à l'affaire qui en est l'objet, et ils ne sont alors pas comptés pour le calcul du quorum et de la majorité.
