# Article R312-72

Dans le cas où l'arme relève de la catégorie C ou du 1° de la catégorie D, le préfet ne peut la restituer que sur présentation par la personne intéressée de l'un des titres prévus au premier alinéa de l'article R. 312-53, sauf si cette personne en a hérité.

Si l'acquisition de l'arme est soumise à déclaration ou à une demande d'enregistrement, le préfet ne peut la restituer que si la personne intéressée a déclaré l'arme ou a fait une demande d'enregistrement, dans les conditions prévues aux articles R. 312-55 et R. 312-56 du présent code.
