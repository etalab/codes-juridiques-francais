# Article R312-77

Le fichier de données à caractère personnel relatif aux personnes interdites d'acquisition et de détention d'armes institué par l'article L. 312-16 est mis en œuvre par le ministère de l'intérieur (direction des libertés publiques et des affaires juridiques). Il est dénommé : " Fichier national des interdits d'acquisition et de détention d'armes " (FINIADA).

Ce fichier a pour finalité la mise en œuvre et le suivi, au niveau national, des interdictions d'acquisition et de détention des armes en application de l'article L. 312-16.
