# Article R312-81

Sur requête individuelle et dans la limite de leurs attributions légales, l'Office national de la chasse et de la faune sauvage, les armuriers et représentants de la Fédération nationale des chasseurs sont destinataires du statut des personnes enregistrées dans le fichier national des interdits d'acquisition et de détention d'armes.
