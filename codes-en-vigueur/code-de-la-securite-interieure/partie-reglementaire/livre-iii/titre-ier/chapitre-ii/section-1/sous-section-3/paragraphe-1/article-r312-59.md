# Article R312-59

Lorsqu'ils transfèrent leur domicile dans un autre département, les titulaires d'un récépissé de déclaration ou d'enregistrement doivent déclarer au préfet de ce département le nombre et la nature des armes et éléments d'arme des catégories B, C et du 1° de la catégorie D qu'ils détiennent.

Cette disposition ne s'applique pas aux armes soumises à enregistrement, acquises ou détenues avant le 1er décembre 2011.
