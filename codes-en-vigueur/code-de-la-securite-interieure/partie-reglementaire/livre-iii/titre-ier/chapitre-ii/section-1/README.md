# Section 1 : Dispositions générales

- [Sous-section 1 : Interdiction d'acquisition par les mineurs](sous-section-1)
- [Sous-section 2 : Armes soumises à autorisation](sous-section-2)
- [Sous-section 3 : Armes soumises à déclaration ou à enregistrement](sous-section-3)
- [Sous-section 4 : Dispositions diverses](sous-section-4)
