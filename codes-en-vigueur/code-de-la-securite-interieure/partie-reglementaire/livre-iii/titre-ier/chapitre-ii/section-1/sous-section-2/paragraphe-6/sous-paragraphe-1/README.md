# Sous-paragraphe  1 : Fonctionnaires et agents publics

- [Article R312-22](article-r312-22.md)
- [Article R312-23](article-r312-23.md)
- [Article R312-24](article-r312-24.md)
- [Article R312-25](article-r312-25.md)
