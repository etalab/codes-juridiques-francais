# Article R312-13

L'autorisation d'acquisition et de détention prévue à l'article R. 312-21 ainsi qu'au 2° de l'article R. 312-40 et à l'article R. 312-44 est accordée pour une durée maximale de cinq ans, sous réserve des dispositions des articles R. 312-14 et R. 312-15.

Son renouvellement est accordé dans les conditions prévues aux articles R. 312-2, R. 312-4 et R. 312-5.
