# Article R312-48

La demande d'autorisation de recomplètement de stocks de munitions prévue à l'article R. 312-47, accompagnée de toutes justifications utiles, est remise au préfet du lieu de domicile qui l'enregistre.

L'autorisation rédigée conformément au modèle fixé par l'arrêté prévu à l'article R. 311-6 est notifiée par le préfet qui a reçu la demande.

Elle est complétée par le vendeur dans les conditions fixées au 3° de l'article 87 du décret n° 2013-700 du 30 juillet 2013 et adressée au préfet par ses soins.
