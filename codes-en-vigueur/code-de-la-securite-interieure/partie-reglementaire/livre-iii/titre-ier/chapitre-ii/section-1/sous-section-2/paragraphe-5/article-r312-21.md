# Article R312-21

L'acquisition et la détention des armes, munitions et leurs éléments de la catégorie B peut être autorisée aux personnes relevant de l'une des catégories prévues au paragraphe 6 et remplissant les conditions propres à cette catégorie. L'autorisation est délivrée par le préfet.

L'autorisation n'est pas accordée lorsque le demandeur :

1° Est inscrit au fichier national des interdits d'acquisition et de détention d'armes ;

2° A été condamné pour l'une des infractions mentionnées au 1° de l'article L. 312-3 du présent code figurant au bulletin n° 2 de son casier judiciaire ou dans un document équivalent pour les ressortissants d'un Etat membre de l'Union européenne ou d'un autre Etat partie à l'accord sur l'Espace économique européen ;

3° A un comportement incompatible avec la détention d'une arme, révélé par l'enquête diligentée par le préfet. Cette enquête peut donner lieu à la consultation des traitements automatisés de données personnelles mentionnés à l'article 26 de la loi n° 78-17 du 6 janvier 1978 ;

4° Fait l'objet d'un régime de protection en application de l'article 425 du code civil, a été ou est admis en soins psychiatriques sans consentement en application de l'article 706-135 du code de procédure pénale et des articles L. 3212-1 à L. 3213-11 du code de la santé publique ou est dans un état physique ou psychique manifestement incompatible avec la détention de ces matériels, armes et munitions.

L'autorisation peut toutefois être accordée par le préfet dès lors que la personne ayant fait l'objet de soins psychiatriques sans consentement présente un certificat médical conforme aux dispositions de l'article R. 312-6.
