# Paragraphe 6 : Conditions particulières de délivrance d'autorisation

- [Sous-paragraphe  1 : Fonctionnaires et agents publics](sous-paragraphe-1)
- [Sous-paragraphe  2 : Spectacles](sous-paragraphe-2)
- [Sous-paragraphe  3 : Collectivités publiques, musées, collections](sous-paragraphe-3)
- [Sous-paragraphe  4 : Essais industriels](sous-paragraphe-4)
- [Sous-paragraphe  5 : Experts judiciaires](sous-paragraphe-5)
- [Sous-paragraphe  6 : Activités privées de sécurité](sous-paragraphe-6)
- [Sous-paragraphe  7 : Personnes exposées à des risques sérieux du fait de leur activité professionnelle](sous-paragraphe-7)
- [Sous-paragraphe  8 : Tir sportif](sous-paragraphe-8-tir)
- [Sous-paragraphe  9 : Tir forain](sous-paragraphe-9-tir)
