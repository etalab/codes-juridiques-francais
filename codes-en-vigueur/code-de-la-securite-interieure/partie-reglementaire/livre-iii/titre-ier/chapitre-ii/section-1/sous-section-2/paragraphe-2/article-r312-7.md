# Article R312-7

Le préfet de département statue après :

1° S'être fait délivrer le bulletin n° 2 du casier judiciaire du demandeur ;

2° S'être assuré que le demandeur n'est pas au nombre des personnes interdites d'acquisition et de détention d'armes en vertu des articles L. 312-10 et L. 312-13.
