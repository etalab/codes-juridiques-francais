# Article R315-6

Le ministre de l'intérieur peut autoriser par arrêté toute personnalité étrangère séjournant en France ainsi que les personnes assurant sa sécurité, sur la demande du gouvernement du pays dont cette personnalité est ressortissante, à détenir, porter et transporter une arme de poing et, dans les limites fixées au 1° de l'article R. 312-47, les munitions correspondantes.

L'autorisation ne peut être délivrée pour une durée supérieure à celle du séjour en France de la personnalité.
