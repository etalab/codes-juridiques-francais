# Article R314-3

Les armes à feu, leurs éléments et leurs munitions de catégorie A et B doivent être conservés :

1° Soit dans des coffres-forts ou des armoires fortes adaptés au type et au nombre de matériels détenus ;

2° Soit à l'intérieur de pièces fortes comportant une porte blindée et dont les ouvrants sont protégés par des barreaux.

Les matériels des 6°, 8°, 9° et 10° de la catégorie A2, dont les systèmes d'armes ont été neutralisés, doivent être conservés dans des locaux sécurisés par une alarme audible de la voie publique et par des moyens de protection physique adaptés.
