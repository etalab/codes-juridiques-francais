# Article R314-11

Les matériels de la catégorie A2 mentionnés à l'article R. 312-27 sont détenus dans un lieu dont les accès sont sécurisés.

Les aéronefs du 9° de la catégorie A2 sont conservés dans un hangar, sauf si leur taille ne le permet pas.

Les véhicules terrestres, les navires et les aéronefs sont mis hors d'état de fonctionner immédiatement. Les systèmes d'armes et armes embarqués sont neutralisés selon des modalités définies par arrêté conjoint des ministres de la défense et de l'intérieur et des ministres chargés de l'industrie et des douanes.
