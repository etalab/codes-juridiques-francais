# Article R314-15

La perte ou le vol d'armes, d'éléments d'arme ou de munitions de la catégorie A, B, C et du 1° de la catégorie D détenus par une administration ou remis par cette dernière à ses agents, conformément aux dispositions des articles R. 312-22 et R. 312-24, doit faire l'objet sans délai de la part de cette administration d'une déclaration écrite adressée au commissaire de police ou au commandant de brigade de gendarmerie et donnant toutes indications utiles sur les circonstances de la perte ou du vol.
