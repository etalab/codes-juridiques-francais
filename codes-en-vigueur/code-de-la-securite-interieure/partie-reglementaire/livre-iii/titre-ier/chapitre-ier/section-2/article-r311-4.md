# Article R311-4

Pour classer les armes, éléments d'arme et munitions dans une catégorie déterminée, les arrêtés prennent en compte des caractéristiques équivalentes à celles des armes, éléments d'arme et munitions figurant dans cette catégorie, notamment pour des raisons tenant à leur dangerosité, d'ordre ou de sécurité publics ou de défense nationale.
