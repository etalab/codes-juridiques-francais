# Article R317-4

Est puni de l'amende prévue pour les contraventions de la quatrième classe le fait pour :

1° Toute association sportive agréée membre d'une fédération sportive ayant reçu du ministre chargé des sports, au titre de l'article L. 131-14 du code du sport, délégation pour la pratique du tir ou du ball-trap, d'acquérir ou de détenir plus d'une arme pour quinze tireurs ou fraction de quinze tireurs ou plus de soixante armes en violation du 1° de l'article R. 312-40 du présent code ;

2° Toute personne majeure d'acquérir ou de détenir plus de douze armes en violation de la limitation prévue à l'article R. 312-40 ;

3° Toute personne âgée de plus de douze ans, sans remplir les conditions prévues à l'article R. 312-40, de détenir plus de trois armes de poing à percussion annulaire à un coup du 1° de la catégorie B ;

4° Toute personne d'acquérir ou de détenir plus de dix armes de poing à percussion annulaire à un coup en violation du quota fixé à l'article R. 312-41.
