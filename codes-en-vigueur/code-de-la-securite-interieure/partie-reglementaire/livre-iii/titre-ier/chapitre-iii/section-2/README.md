# Section 2 : Autorisation d'ouverture du commerce de détail

- [Sous-section 1 : Conditions de délivrance](sous-section-1)
- [Sous-section 2 : Obligations du commerçant titulaire de l'autorisation](sous-section-2)
- [Sous-section 3 : Mesures de sécurité](sous-section-3)
- [Sous-section 4 : Conditions de suspension ou de retrait](sous-section-4)
