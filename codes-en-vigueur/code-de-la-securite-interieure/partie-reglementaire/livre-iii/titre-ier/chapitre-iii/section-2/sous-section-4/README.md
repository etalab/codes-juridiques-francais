# Sous-section 4 : Conditions de suspension ou de retrait

- [Article R313-18](article-r313-18.md)
- [Article R313-19](article-r313-19.md)
