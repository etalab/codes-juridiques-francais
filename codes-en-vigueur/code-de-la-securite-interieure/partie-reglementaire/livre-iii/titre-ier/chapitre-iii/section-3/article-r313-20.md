# Article R313-20

Par dérogation aux dispositions de l'article R. 313-17 :

1° Des manifestations commerciales peuvent être organisées dans les conditions prévues par l'article L. 762-2 du code de commerce ;

2° Sans préjudice de l'application éventuelle des dispositions de l'article L. 310-2 du code de commerce, des ventes au détail hors d'un local fixe et permanent peuvent être autorisées à l'occasion de manifestations autres que celles définies par l'article L. 762-2 du code de commerce par le préfet du département du lieu où elles se tiennent.

Seules peuvent être autorisées à y vendre des armes, des éléments d'arme et des munitions des catégories B, C, du 1° de la catégorie D et des a, b, c, h, i et j du 2° de la catégorie D les personnes titulaires :

a) Soit de l'autorisation mentionnée à l'article R. 313-8 ;

b) Soit de l'autorisation d'un local de vente au détail délivrée dans les conditions prévues au dernier alinéa de l'article 74 du décret n° 2013-700 du 30 juillet 2013 ;

c) Soit d'une autorisation spéciale délivrée par le préfet attestant que les conditions de la vente des armes, des éléments d'arme et des munitions ne présentent pas de risque pour l'ordre et la sécurité publics ;

d) Soit de l'agrément d'armurier prévu à l'article L. 313-2.

Les organisateurs de ces manifestations commerciales où sont présentés ou vendus des armes, leurs éléments ou leurs munitions sont tenus de vérifier que les exposants possèdent l'une de ces autorisations ;

3° Lors des rencontres organisées sur les sites accueillant des participants aux activités de paintball, la vente de lanceurs de paintball classés au 4° de la catégorie C et au h du 2° de la catégorie D peut être réalisée par des commerçants autorisés.
