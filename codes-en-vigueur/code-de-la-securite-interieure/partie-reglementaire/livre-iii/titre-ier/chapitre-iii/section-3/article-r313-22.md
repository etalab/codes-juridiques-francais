# Article R313-22

Lors des ventes aux enchères publiques, seules peuvent enchérir :

1° Pour les matériels de la catégorie A, les personnes titulaires d'une autorisation mentionnée au dernier alinéa de l'article 74 du décret n° 2013-700 du 30 juillet 2013 ;

2° Pour les matériels de la catégorie B, les personnes titulaires d'une autorisation mentionnée au dernier alinéa de l'article 74 du décret n° 2013-700 du 30 juillet 2013 ou à l'article R. 312-21 ;

3° Pour les armes de la catégorie C et du 1° de la catégorie D, les titulaires d'une autorisation mentionnée à l'article R. 313-8 du présent code ou les personnes titulaires de l'un des titres prévus au premier alinéa de l'article R. 312-53.

Les organisateurs de la vente doivent se faire présenter ces documents avant la vente.

Les armes et leurs éléments destinés à la vente aux enchères publiques sont conservés dans les conditions prévues aux 1° et 2° de l'article R. 314-10.
