# Section 1 : Agrément d'armurier

- [Article R313-1](article-r313-1.md)
- [Article R313-2](article-r313-2.md)
- [Article R313-3](article-r313-3.md)
- [Article R313-4](article-r313-4.md)
- [Article R313-5](article-r313-5.md)
- [Article R313-6](article-r313-6.md)
- [Article R313-7](article-r313-7.md)
