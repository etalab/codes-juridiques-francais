# Article D346-1

Sont applicables dans les îles Wallis et Futuna, sous réserve des adaptations prévues à l'article D. 346-2, les dispositions du présent livre mentionnées dans la colonne de gauche du tableau ci-après, dans leur rédaction indiquée dans la colonne de droite du même tableau :

<div>
<div align="center">

<table>
<tr>
<td align="center" valign="bottom">
<br/>DISPOSITIONS APPLICABLES </td>
<td align="center" valign="bottom">
<br/>DANS LEUR RÉDACTION </td>
</tr>
<tr>
<td align="left">
<br/>Au titre II </td>
<td align="left"/>
</tr>
<tr>
<td align="justify">
<br/>
D. 322-1
</td>
<td align="justify">
<br/>Résultant du décret n° 2014-1253 du 27 octobre 2014 relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples)</td>
</tr>
</table>

</div>
</div>
