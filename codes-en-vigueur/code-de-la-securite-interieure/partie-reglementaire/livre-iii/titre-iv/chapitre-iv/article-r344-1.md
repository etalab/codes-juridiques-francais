# Article R344-1

Outre celles des sections 1 et 2 du présent chapitre, sont applicables en Polynésie française, sous réserve des adaptations prévues aux articles R. 344-2 et R. 344-3, les dispositions du présent livre mentionnées dans la colonne de gauche du tableau ci-après, dans leur rédaction indiquée dans la colonne de droite du même tableau :

<div align="center">

<table>
<tbody>
<tr>
<th>
<br/>DISPOSITIONS APPLICABLES <br/>
</th>
<th>
<br/>DANS LEUR RÉDACTION <br/>
</th>
</tr>
<tr>
<td align="center" colspan="2">
<br/>Au titre Ier <br/>
</td>
</tr>
<tr>
<td align="justify">
<br/>
R. 311-1 à R. 311-6
<br/>
</td>
<td align="justify">
<br/>Résultant du décret n° 2014-1253 du 27 octobre 2014 relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (décrets en Conseil d'Etat et décrets simples) <br/>
</td>
</tr>
<tr>
<td align="justify">
<br/>
R. 312-1 à R. 312-83
<br/>
</td>
<td align="justify">
<br/>Résultant du décret n° 2014-1253 du 27 octobre 2014 relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (décrets en Conseil d'Etat et décrets simples) <br/>
</td>
</tr>
<tr>
<td align="justify">
<br/>
R. 313-1 à R. 313-26
<br/>
</td>
<td align="justify">
<br/>Résultant du décret n° 2014-1253 du 27 octobre 2014 relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (décrets en Conseil d'Etat et décrets simples) <br/>
</td>
</tr>
<tr>
<td align="justify">
<br/>
R. 314-1 à R. 314-20
<br/>
</td>
<td align="justify">
<br/>Résultant du décret n° 2014-1253 du 27 octobre 2014 relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (décrets en Conseil d'Etat et décrets simples) <br/>
</td>
</tr>
<tr>
<td align="justify">
<br/>
R. 315-1 à R. 315-18
<br/>
</td>
<td align="justify">
<br/>Résultant du décret n° 2014-1253 du 27 octobre 2014 relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (décrets en Conseil d'Etat et décrets simples) <br/>
</td>
</tr>
<tr>
<td align="justify">
<br/>
R. 317-1 à R. 317-14
<br/>
</td>
<td align="justify">
<br/>Résultant du décret n° 2014-1253 du 27 octobre 2014 relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (décrets en Conseil d'Etat et décrets simples) <br/>
</td>
</tr>
<tr>
<td align="center" colspan="2" valign="middle">
<br/>Au titre III <br/>
</td>
</tr>
<tr>
<td align="justify">
<br/>
R. 332-1 et R. 333-1
<br/>
</td>
<td align="justify">
<br/>Résultant du décret n° 2014-1253 du 27 octobre 2014 relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (décrets en Conseil d'Etat et décrets simples)<br/>
</td>
</tr>
</tbody>
</table>

</div>
