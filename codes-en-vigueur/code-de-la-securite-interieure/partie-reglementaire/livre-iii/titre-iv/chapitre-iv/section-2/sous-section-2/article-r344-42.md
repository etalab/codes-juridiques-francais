# Article R344-42

Si l'exploitation des loteries et des appareils de jeux porte atteinte à l'ordre public, le haut-commissaire peut en interdire la poursuite pour une période de six mois.
