# Article R345-2

<div align="left">Sont applicables en Nouvelle-Calédonie, sous réserve des adaptations prévues à l'article D. 345-5, les dispositions du présent livre mentionnées dans la colonne de gauche du tableau ci-après, dans leur rédaction indiquée dans la colonne de droite du même tableau : <br/>
<div align="center">

<table>
<tbody>
<tr>
<th>
<br/>DISPOSITIONS APPLICABLES <br/>
</th>
<th>
<br/>DANS LEUR RÉDACTION <br/>
</th>
</tr>
<tr>
<td align="center" colspan="2">
<br/>Au titre II <br/>
</td>
</tr>
<tr>
<td align="justify">
<br/>
D. 321-22 à D. 321-25
<br/>
</td>
<td align="justify">
<br/>Résultant du décret n° 2014-1253 du 27 octobre 2014 relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples) <br/>
</td>
</tr>
<tr>
<td align="justify">
<br/>
D. 322-4
<br/>
</td>
<td align="justify">
<br/>Résultant du décret n° 2014-1253 du 27 octobre 2014 relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples)<br/>
</td>
</tr>
</tbody>
</table>

</div>
<br/>
</div>
