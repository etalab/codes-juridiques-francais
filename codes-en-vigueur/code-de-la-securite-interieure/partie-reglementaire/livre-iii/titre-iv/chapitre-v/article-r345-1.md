# Article R345-1

Sont applicables en Nouvelle-Calédonie, sous réserve des adaptations prévues aux articles R. 345-3, R. 345-4 et D. 345-5, les dispositions du présent livre mentionnées dans la colonne de gauche du tableau ci-après, dans leur rédaction indiquée dans la colonne de droite du même tableau :

<div align="center">

<table>
<tbody>
<tr>
<th>
<br/>DISPOSITIONS APPLICABLES <br/>
</th>
<th>
<br/>DANS LEUR RÉDACTION <br/>
</th>
</tr>
<tr>
<td align="center" colspan="2">
<br/>Au titre Ier <br/>
</td>
</tr>
<tr>
<td align="justify">
<br/>
R. 311-1 à R. 311-6
<br/>
</td>
<td align="justify">
<br/>Résultant du décret n° 2014-1253 du 27 octobre 2014relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples) <br/>
</td>
</tr>
<tr>
<td align="justify">
<br/>
R. 312-1 à R. 312-48
<br/>
</td>
<td align="justify">
<br/>Résultant du décret n° 2014-1253 du 27 octobre 2014 relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples) <br/>
</td>
</tr>
<tr>
<td align="justify">
<br/>
R. 312-50 à R. 312-83
<br/>
</td>
<td align="justify">
<br/>Résultant du décret n° 2014-1253 du 27 octobre 2014 relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples) <br/>
</td>
</tr>
<tr>
<td align="justify">
<br/>
R. 313-1 à R. 313-26
<br/>
</td>
<td align="justify">
<br/>Résultant du décret n° 2014-1253 du 27 octobre 2014 relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples) <br/>
</td>
</tr>
<tr>
<td align="justify">
<br/>
R. 314-1 à R. 314-20
<br/>
</td>
<td align="justify">
<br/>Résultant du décret n° 2014-1253 du 27 octobre 2014 relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples) <br/>
</td>
</tr>
<tr>
<td align="justify">
<br/>
R. 315-1 à R. 315-18
<br/>
</td>
<td align="justify">
<br/>Résultant du décret n° 2014-1253 du 27 octobre 2014 relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples) <br/>
</td>
</tr>
<tr>
<td align="justify">
<br/>
R. 317-1 à R. 317-14
<br/>
</td>
<td align="justify">
<br/>Résultant du décret n° 2014-1253 du 27 octobre 2014 relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples) <br/>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<br/>Au titre II <br/>
</td>
</tr>
<tr>
<td align="left">
<br/>
R. 321-21, R. 321-26
<br/>
</td>
<td align="justify">
<br/>Résultant du décret n° 2014-1253 du 27 octobre 2014 relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples)<br/>
</td>
</tr>
</tbody>
</table>

</div>
