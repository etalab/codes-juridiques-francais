# Article R333-1

Les missions de police administrative dévolues au représentant de l'Etat en application de l'article L. 333-1 sont exercées par le préfet de département, à Paris, par le préfet de police, et dans le département des Bouches-du-Rhône, par le préfet de police des Bouches-du-Rhône.
