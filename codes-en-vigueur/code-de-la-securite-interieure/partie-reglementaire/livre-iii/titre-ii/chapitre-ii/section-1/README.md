# Section 1 : Loteries d'objets mobiliers exclusivement destinées à des actes de bienfaisance, à l'encouragement des arts ou au financement d'activités sportives à but non lucratif

- [Article D322-1](article-d322-1.md)
- [Article D322-2](article-d322-2.md)
- [Article D322-3](article-d322-3.md)
