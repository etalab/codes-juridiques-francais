# Article R321-27

Toute personne désirant accéder aux salles de jeux est tenue de justifier de son identité. A cette fin, chaque établissement met en place un dispositif de contrôle systématique à l'entrée des salles de jeux. Ce contrôle est exercé dans tous les cas, que l'accès aux salles soit payant ou non.

L'accès aux salles de jeux est interdit :

1° Aux mineurs, même émancipés ;

2° Aux personnes dont le ministre de l'intérieur a prononcé l'exclusion en application de l'article R. 321-28 ;

3° Aux personnes en état d'ivresse ;

4° Aux personnes susceptibles de provoquer des incidents ;

5° Aux fonctionnaires en uniforme et militaires en uniforme, en dehors de l'exercice de leurs missions.
