# Article R321-34

Il est interdit aux employés des salles de jeux de transporter des jetons, des plaques et des espèces, ou tout titre de valeur, pendant leur service, à l'intérieur du casino dans des conditions autres que celles prévues par l'arrêté mentionné au premier alinéa de l'article R. 321-39.

Il est interdit aux membres du personnel des salles de jeux, responsables d'une caisse, telle que caisse d'une table de jeux, caisse de changeur ou caisse principale, de détenir soit dans leur caisse, soit par-devers eux, des jetons, plaques, espèces, chèques ou devises et tout autre titre de valeur dont la provenance ou l'utilisation ne pourrait être justifiée par le fonctionnement normal des jeux.
