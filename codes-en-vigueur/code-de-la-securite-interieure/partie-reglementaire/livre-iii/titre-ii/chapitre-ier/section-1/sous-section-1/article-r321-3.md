# Article R321-3

La demande d'autorisation est soumise à une enquête sauf lorsqu'elle a pour objet :

1° Un renouvellement d'autorisation ;

2° Un transfert de l'activité autorisée par l'arrêté mentionné au premier alinéa de l'article R. 321-5 sauf lorsque l'enquête initiale n'a porté que sur un lieu provisoire d'implantation ;

3° (Abrogé)

4° Une expérimentation prévue à l'article R. 321-15 ;

5° Une augmentation du nombre de tables de jeux autorisées ;

6° Une augmentation du nombre de machines à sous autorisées.
