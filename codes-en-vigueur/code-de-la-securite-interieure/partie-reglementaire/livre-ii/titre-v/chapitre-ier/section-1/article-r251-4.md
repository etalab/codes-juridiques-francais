# Article R251-4

Les membres de la commission exercent leurs fonctions à titre gratuit. Ils peuvent bénéficier du remboursement de leurs frais de déplacement et de séjour dans les conditions prévues par la réglementation applicable aux fonctionnaires de l'Etat.
