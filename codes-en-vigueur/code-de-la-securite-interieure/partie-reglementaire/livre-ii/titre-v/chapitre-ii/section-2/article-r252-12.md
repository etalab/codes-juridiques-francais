# Article R252-12

Les agents des services de la police ou de la gendarmerie nationales, ainsi que les agents des douanes ou des services d'incendie et de secours destinataires des images et enregistrements de systèmes de vidéoprotection appartenant à des tiers, en application des articles L. 252-2 et L. 252-3, sont individuellement désignés et dûment habilités par le chef de service ou le chef d'unité à compétence départementale, régionale, zonale ou nationale sous l'autorité duquel ils sont affectés.
