# Article D211-17

Les armes à feu susceptibles d'être utilisées par les représentants de la force publique pour le maintien de l'ordre public en application de l'article R. 211-16 sont les suivantes :

<table>
<tbody>
<tr>
<th>
<br/>APPELLATION </th>
<th>
<br/>CLASSIFICATION </th>
</tr>
<tr>
<td>
<br/>Grenades GLI F4 <br/>Grenades lacrymogène instantanée </td>
<td rowspan="3">
<br/>
Article R. 311-2
<br/>5° et 6° de la catégorie A2 </td>
</tr>
<tr>
<td>
<br/>Grenades OF F1 </td>
</tr>
<tr>
<td>
<br/>Grenades instantanée </td>
</tr>
<tr>
<td>
<br/>Lanceurs de grenades de 56 mm et leurs munitions </td>
<td>
<br/>Article R. 311-2 <br/>4°, 5° et 6° de la catégorie A2 </td>
</tr>
<tr>
<td>
<br/>Lanceurs de grenades de 40 mm et leurs munitions </td>
<td>
<br/>Article R. 311-2 <br/>4°, 5° et 6° de la catégorie A2 </td>
</tr>
<tr>
<td>
<br/>Grenades à main de désencerclement </td>
<td>
<br/>Article R. 311-2 <br/>6° de la catégorie A2</td>
</tr>
</tbody>
</table>
