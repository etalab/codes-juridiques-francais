# Chapitre Ier : Dispositions générales

- [Article R241-1](article-r241-1.md)
- [Article R241-2](article-r241-2.md)
