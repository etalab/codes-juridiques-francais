# Chapitre II : Conditions des interceptions

- [Section 1 : Groupement interministériel de contrôle](section-1)
- [Section 2 : Réalisation des opérations matérielles  nécessaires à la mise en place des interceptions](section-2)
