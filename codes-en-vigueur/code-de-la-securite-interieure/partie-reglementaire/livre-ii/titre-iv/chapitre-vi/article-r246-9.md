# Article R246-9

Les coûts identifiables et spécifiques supportés par les opérateurs et les personnes mentionnés à l'article L. 246-1 pour la transmission des informations ou des documents font l'objet d'un remboursement par l'Etat par référence aux tarifs et selon des modalités fixés par un arrêté des ministres chargés de la sécurité intérieure, de la défense, de l'économie, du budget et des communications électroniques.
