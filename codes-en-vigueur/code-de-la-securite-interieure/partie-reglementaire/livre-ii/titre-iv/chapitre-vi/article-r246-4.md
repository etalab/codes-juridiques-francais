# Article R246-4

Les demandes de recueil d'informations ou de documents prévues à l'article L. 246-2 comportent :

a) Le nom, le prénom et la qualité du demandeur ainsi que son service d'affectation et l'adresse de celui-ci ;

b) La nature précise des informations ou des documents dont le recueil est demandé et, le cas échéant, la période concernée ;

c) La date de la demande et sa motivation au regard des finalités mentionnées à l'article L. 241-2.
