# Article R246-3

Afin de permettre la désignation de la personnalité qualifiée mentionnée au II de l'article L. 246-2 et de ses adjoints, le Premier ministre transmet à la Commission nationale de contrôle des interceptions de sécurité, pour chaque poste à pourvoir, une liste d'au moins trois personnes choisies en raison de leur compétence et de leur impartialité. Ces propositions sont motivées. Elles sont adressées à la commission au moins trois mois avant le terme du mandat de la personnalité qualifiée et de ses adjoints. La commission désigne, au sein des listes, la personnalité qualifiée et ses adjoints deux mois au plus tard après avoir reçu les propositions.

Toute décision désignant la personnalité qualifiée et ses adjoints est notifiée sans délai au Premier ministre par la commission et publiée au Journal officiel de la République française.

Les adjoints de la personnalité qualifiée sont au maximum au nombre de quatre.
