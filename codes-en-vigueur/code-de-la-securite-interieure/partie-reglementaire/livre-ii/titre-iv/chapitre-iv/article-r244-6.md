# Article R244-6

L'intégralité des frais liés à la mise en œuvre de l'obligation prévue par l'article L. 244-1 est prise en charge, sur la base des frais réellement exposés par le fournisseur et dûment justifiés par celui-ci, par le budget des services du Premier ministre.
