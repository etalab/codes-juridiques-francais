# Article R288-1

Sont applicables dans les Terres australes et antarctiques françaises les dispositions du présent livre mentionnées dans la colonne de gauche du tableau ci-après, dans leur rédaction indiquée dans la colonne de droite du même tableau :

<table>
<tbody>
<tr>
<th>DISPOSITIONS APPLICABLES <br/>
</th>
<th>DANS LEUR RÉDACTION <br/>
</th>
</tr>
<tr>
<td align="center">
<p align="center">Au titre Ier <br/>
</p>
</td>
<td align="center">
<br/>
</td>
</tr>
<tr>
<td align="center">
R. 211-2 à R. 211-8
<br/>
</td>
<td align="center">
<p align="left">Résultant du décret n° 2013-1113 relatif aux dispositions des livres Ier, II, IV et V de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples) <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
R. 211-11 à R. 211-16, R. 211-18 et R. 211-21
<br/>
</td>
<td align="center">
<p align="left">Résultant du décret n° 2014-1253 du 27 octobre 2014 relatif aux dispositions des livres III, VI et VII de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples) </p>
</td>
</tr>
<tr>
<td align="center">
R. 211-27 à R. 211-30
<br/>
</td>
<td align="center">
<p align="left">Résultant du décret n° 2013-1113 relatif aux dispositions des livres Ier, II, IV et V de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples) <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
R. 214-1 à R. 214-3
<br/>
</td>
<td align="center">
<p align="left">Résultant du décret n° 2013-1113 relatif aux dispositions des livres Ier, II, IV et V de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples) <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<p align="center">Au titre II <br/>
</p>
</td>
<td align="center">
<br/>
</td>
</tr>
<tr>
<td>
<p align="center"> R. 222-1</p>
</td>
<td>Résultant du décret n° 2014-1641 du 26 décembre 2014 pris pour l'application des articles 15, 18 et 19 de la loi n° 2013-1168 du 18 décembre 2013 relative à la programmation militaire pour les années 2014 à 2019 et portant diverses dispositions concernant la défense et la sécurité nationale </td>
</tr>
<tr>
<td align="center">
R. 223-2
<br/>
</td>
<td align="center">
<p align="left">Résultant du décret n° 2013-1113 relatif aux dispositions des livres Ier, II, IV et V de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples) <br/>
</p>
</td>
</tr>
<tr>
<td>
<p align="center">
R. 224-1 à R. 224-6
</p>
</td>
<td>Résultant du décret n° 2015-26 du 14 janvier 2015 relatif à l'interdiction de sortie du territoire des ressortissants français projetant de participer à des activités terroristes à l'étranger <br/>
</td>
</tr>
<tr>
<td align="center">
<p align="center">Au titre III <br/>
</p>
</td>
<td align="center">
<br/>
</td>
</tr>
<tr>
<td align="center">
R. 232-1 à R. 232-5-1
</td>
<td align="center">
<p align="left">Résultant du décret n° 2014-1095 du 26 septembre 2014 portant création d'un traitement de données à caractère personnel dénommé " système API-PNR France " pris pour l'application de l'article L. 232-7 du code de la sécurité intérieure </p>
</td>
</tr>
<tr>
<td>
<p align="center">
R. 232-12 à R. 232-18
</p>
</td>
<td>
<p align="left">Résultant du décret n° 2014-1095 du 26 septembre 2014 portant création d'un traitement de données à caractère personnel dénommé " système API-PNR France " pris pour l'application de l'article L. 232-7 du code de la sécurité intérieure </p>
</td>
</tr>
<tr>
<td>
<p align="center">
R. 232-19
</p>
</td>
<td>Résultant du décret n° 2015-26 du 14 janvier 2015 relatif à l'interdiction de sortie du territoire des ressortissants français projetant de participer à des activités terroristes à l'étranger <br/>
</td>
</tr>
<tr>
<td align="center">
R. 236-1 à R. 236-45
<br/>
</td>
<td align="center">
<p align="left">Résultant du décret n° 2013-1113 relatif aux dispositions des livres Ier, II, IV et V de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples) <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<p align="center">Au titre IV <br/>
</p>
</td>
<td align="center"/>
</tr>
<tr>
<td align="center">
R. 241-1 et R. 241-2
</td>
<td align="center">
<p align="left">Résultant du décret n° 2014-1576 du 24 décembre 2014 relatif à l'accès administratif aux données de connexion </p>
</td>
</tr>
<tr>
<td>
<p align="center">
R. 242-2
</p>
</td>
<td>Résultant du décret n° 2013-1113 relatif aux dispositions des livres Ier, II, IV et V de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples) <br/>
</td>
</tr>
<tr>
<td>
<p align="center">
R. 246-1 à R. 246-9
</p>
</td>
<td>Résultant du décret n° 2014-1576 du 24 décembre 2014 relatif à l'accès administratif aux données de connexion </td>
</tr>
<tr>
<td align="center">
<p align="center">Au titre V <br/>
</p>
</td>
<td align="center">
<br/>
</td>
</tr>
<tr>
<td>
<p align="center">
R. 251-1
</p>
</td>
<td>
<p align="left">Résultant du décret n° 2014-901 du 18 août 2014 relatif aux activités privées de sécurité </p>
</td>
</tr>
<tr>
<td align="center">
R. 251-8, à l'exception des 3° et 4°, R. 251-9 à R. 251-12
</td>
<td align="center">
<p align="left">Résultant du décret n° 2013-1113 relatif aux dispositions des livres Ier, II, IV et V de la partie réglementaire du code de la sécurité intérieure <br/>(Décrets en Conseil d'Etat et décrets simples) </p>
</td>
</tr>
<tr>
<td align="center">
R. 252-2 à R. 253-4
<br/>
</td>
<td align="center">
<p align="left">Résultant du décret n° 2013-1113 relatif aux dispositions des livres Ier, II, IV et V de la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et décrets simples)<br/>
</p>
</td>
</tr>
</tbody>
</table>
