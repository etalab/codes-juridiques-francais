# Article R231-14

Le bureau national dénommé " Sirene " (supplément d'information requis à l'entrée nationale) est placé sous l'autorité fonctionnelle du directeur central de la police judiciaire, sans préjudice des responsabilités relevant des autorités judiciaires.

Il est situé dans les locaux de la direction centrale de la police judiciaire, qui en assure le fonctionnement.
