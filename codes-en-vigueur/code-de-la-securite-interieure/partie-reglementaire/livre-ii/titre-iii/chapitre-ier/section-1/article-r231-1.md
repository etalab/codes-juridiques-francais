# Article R231-1

Le système d'information Schengen (SIS) a pour objet de concourir à la préservation de l'ordre et de la sécurité publics, dans le contexte de la libre circulation des personnes sur l'ensemble du territoire des Etats parties à la convention du 19 juin 1990 d'application de l'accord de Schengen du 14 juin 1985.

Le système d'information Schengen est composé d'une partie centrale dite " de support technique " et d'une partie nationale dans chaque Etat membre.
