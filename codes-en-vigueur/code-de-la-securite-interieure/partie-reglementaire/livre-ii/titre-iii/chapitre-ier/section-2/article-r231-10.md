# Article R231-10

Pour les signalements relatifs aux objets, les données enregistrées dans le système informatique national N-SIS sont les suivantes :

1° Pour les armes à feu : le numéro d'arme, le type d'arme (marque, modèle, calibre), le motif de la recherche, la conduite à tenir ;

2° Pour les documents d'identité délivrés : le nom et le ou les prénoms du titulaire, ainsi que sa date de naissance, le motif de la recherche, la conduite à tenir ;

3° Pour les billets de banque : le motif de la recherche, la conduite à tenir ;

4° Pour les documents d'identité vierges, le motif de la recherche, la conduite à tenir ;

5° Pour les véhicules : le motif de la recherche, les caractéristiques (couleur, catégorie, marque, nationalité, numéros de série et d'immatriculation, dangerosité), la conduite à tenir.

Sont également saisis et, en cas de réponse positive, restitués, les éléments de référence du dossier archivé relatifs soit à l'objet lui-même, soit à la nature, au procès-verbal et au lieu de l'infraction concernés.
