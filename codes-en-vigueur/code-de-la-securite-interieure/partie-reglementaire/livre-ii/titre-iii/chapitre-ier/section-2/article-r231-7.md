# Article R231-7

Peuvent faire l'objet d'un traitement automatisé dans le système informatique national N-SIS aux seules fins de surveillance discrète et de contrôle spécifique les données relatives aux personnes ou aux véhicules signalés pour les motifs suivants :

1° Cet enregistrement est nécessaire à la répression d'infractions pénales et à la prévention de menaces pour la sécurité publique, lorsque des indices réels font présumer que la personne concernée envisage de commettre ou commet des faits punissables nombreux et extrêmement graves, ou lorsque l'appréciation globale de l'individu, en particulier sur la base des faits punissables commis jusqu'alors par l'intéressé, permet de supposer qu'il commettra également à l'avenir des faits punissables extrêmement graves ;

2° Des indices concrets permettent de supposer que les informations mentionnées au paragraphe 4 de l'article 99 de la convention du 19 juin 1990 d'application de l'accord de Schengen du 14 juin 1985 sont nécessaires à la prévention d'une menace grave émanant de l'intéressé ou d'autres menaces graves pour la sûreté intérieure et extérieure de l'Etat.
