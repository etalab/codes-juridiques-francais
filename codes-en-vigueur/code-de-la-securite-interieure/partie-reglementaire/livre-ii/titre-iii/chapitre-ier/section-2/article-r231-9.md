# Article R231-9

Pour les signalements relatifs aux personnes, les données nominatives enregistrées dans le système informatique national N-SIS sont les suivantes :

1° L'état civil (nom, prénoms et alias, date et lieu de naissance), le sexe et la nationalité ;

2° Les signes physiques particuliers, objectifs et inaltérables, ainsi que l'indication que la personne est armée ou violente ;

3° Le motif du signalement ;

4° La conduite à tenir en cas de découverte.
