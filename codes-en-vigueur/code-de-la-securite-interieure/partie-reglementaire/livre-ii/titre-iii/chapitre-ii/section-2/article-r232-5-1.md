# Article R232-5-1

I.-En cas de méconnaissance par un transporteur aérien des obligations fixées à l'article R. 232-1-1, l'amende et la procédure prévues par l'article L. 232-5 s'appliquent à son égard.

II.-Le procès-verbal mentionné à l'article L. 232-5 comprend le nom de l'entreprise de transport et les références du vol ou du voyage au titre duquel la responsabilité de l'entreprise de transport est susceptible d'être engagée. Il précise, pour chaque vol ou voyage, les obligations définies méconnues par l'entreprise de transport. Il comporte également, le cas échéant, les observations de celle-ci.

Il est signé par le directeur de l'UIP ou son adjoint.

III.-Le directeur de l'UIP notifie à l'entreprise de transport, par lettre recommandée avec demande d'avis de réception, le projet de sanction prévu par l'article L. 232-5. Pendant le délai d'un mois prévu par le même article pour produire ses observations, l'entreprise peut se faire délivrer copie à ses frais de tout ou partie de la procédure.

IV.-En cas de sanction, l'amende est recouvrée dans les conditions prévues pour les créances de l'Etat.
