# Section 5 : Traitement de données à caractère personnel dénommé " Sécurisation des interventions et demandes particulières de protection "

- [Article R236-38](article-r236-38.md)
- [Article R236-39](article-r236-39.md)
- [Article R236-40](article-r236-40.md)
- [Article R236-41](article-r236-41.md)
- [Article R236-42](article-r236-42.md)
- [Article R236-43](article-r236-43.md)
- [Article R236-44](article-r236-44.md)
- [Article R236-45](article-r236-45.md)
