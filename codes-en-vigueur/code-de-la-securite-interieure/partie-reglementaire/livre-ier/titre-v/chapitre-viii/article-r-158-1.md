# Article R*158-1

<div align="left">Sont applicables dans les Terres australes et antarctiques françaises les dispositions du présent livre mentionnées dans la colonne de gauche du tableau ci-après, dans leur rédaction indiquée dans la colonne de droite du même tableau : <table>
<tbody>
<tr>
<td>
<p align="center">DISPOSITIONS APPLICABLES </p>
</td>
<td>
<p align="center">DANS LEUR RÉDACTION </p>
</td>
</tr>
<tr>
<td align="center">
<br/>Au titre Ier <br/>
</td>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>
R. * 121-1
<br/>
</td>
<td align="center">
<br/>Résultant du décret n° 2013-1112 relatif à la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et en conseil des ministres) <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Au titre II <br/>
</td>
<td align="center"/>
</tr>
<tr>
<td align="center">
R. * 122-1 à R. * 122-12
<br/>
<br/>
</td>
<td align="center">Résultant du décret n° 2014-1252 du 27 octobre 2014 relatif à la partie réglementaire du code de la sécurité intérieure (Décrets en Conseil d'Etat et en conseil des ministres)<br/>
<br/>
</td>
</tr>
</tbody>
</table>
</div>
