# Article D123-30

Le personnel de l'institut comprend des agents publics civils ou militaires ainsi que des agents non titulaires.

Les conditions de mise à disposition du personnel civil et militaire sont précisées par des conventions conclues à cet effet.
