# Article D123-19

Les membres du conseil d'administration mentionnés aux 3° à 5° et aux 7° à 9° de l'article D. 123-18 sont nommés pour une durée de trois ans par arrêté du Premier ministre. Le mandat des membres autres que ceux désignés aux 1°, 2° et 6° du même article est renouvelable une fois.

En cas de vacance d'un siège pour quelque cause que ce soit, il est immédiatement pourvu au remplacement dans les mêmes conditions, pour la durée du mandat restant à courir de la personne remplacée. Ce mandat partiel peut être suivi d'un mandat de trois ans renouvelable une fois.
