# Article D123-22

Le président du conseil d'administration peut appeler à participer aux séances, avec voix consultative, toute personne dont la présence lui paraît utile.
