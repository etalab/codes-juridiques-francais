# Article D123-6

Pendant la durée des sessions, les auditeurs ne sont ni administrés ni rémunérés par l'institut.

Les auditeurs, magistrats ou fonctionnaires civils et militaires de l'Etat et les agents soumis à un statut de droit public bénéficient des dispositions statutaires qui les régissent, notamment en matière de couverture de risques. Les autres auditeurs sont, pendant la durée des sessions, des collaborateurs bénévoles et occasionnels du service public.
