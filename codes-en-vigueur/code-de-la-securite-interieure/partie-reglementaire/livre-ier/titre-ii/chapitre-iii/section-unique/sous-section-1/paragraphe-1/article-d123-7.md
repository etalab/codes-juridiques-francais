# Article D123-7

La liste des auditeurs qui ont satisfait aux obligations des sessions nationales ou régionales est établie par arrêté du Premier ministre sur proposition du directeur de l'institut.

La liste des participants qui ont satisfait aux obligations des autres formations est fixée par décision du directeur de l'institut.

Après leur session, les auditeurs et participants peuvent mettre en œuvre les connaissances acquises, notamment dans des associations agréées par l'institut.
