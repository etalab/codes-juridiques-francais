# Sous-section 7 : Dispositions particulières  à la zone de défense et de sécurité de Paris

- [Article R*122-39](article-r-122-39.md)
- [Article R*122-40](article-r-122-40.md)
- [Article R*122-41](article-r-122-41.md)
- [Article R*122-41-1](article-r-122-41-1.md)
- [Article R*122-42](article-r-122-42.md)
- [Article R*122-43](article-r-122-43.md)
- [Article R*122-44](article-r-122-44.md)
