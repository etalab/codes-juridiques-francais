# Paragraphe 1 : Pouvoirs généraux du préfet de zone de défense  et de sécurité en matière de sécurité nationale

- [Article R*122-4](article-r-122-4.md)
- [Article R*122-5](article-r-122-5.md)
- [Article R*122-6](article-r-122-6.md)
- [Article R*122-7](article-r-122-7.md)
