# Section 2 : Conseil départemental de prévention de la délinquance, d'aide aux victimes et de lutte contre la drogue, les dérives sectaires et les violences faites aux femmes

- [Article D132-5](article-d132-5.md)
- [Article D132-6](article-d132-6.md)
