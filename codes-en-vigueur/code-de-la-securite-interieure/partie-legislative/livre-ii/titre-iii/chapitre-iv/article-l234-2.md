# Article L234-2

La consultation prévue à l'article L. 234-1 est faite par des agents individuellement désignés et spécialement habilités :

1° De la police et de la gendarmerie nationales ;

2° Dans des conditions fixées par le décret en Conseil d'Etat mentionné à l'article L. 234-1, des services spécialisés de renseignement mentionnés au I de l'article 6 nonies de l'ordonnance n° 58-1100 du 17 novembre 1958 relative au fonctionnement des assemblées parlementaires.

Dans des conditions déterminées par décret en Conseil d'Etat, elle peut également être effectuée par des personnels investis de missions de police administrative désignés selon les mêmes procédures.
