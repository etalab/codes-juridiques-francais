# Sous-section 1 : Manifestations sur la voie publique

- [Article L211-12](article-l211-12.md)
- [Article L211-13](article-l211-13.md)
- [Article L211-14](article-l211-14.md)
