# Article L245-3

Le fait par une personne exploitant un réseau de communications électroniques ou fournissant des services de communications électroniques de refuser, en violation des articles L. 246-1 à L. 246-3 et du premier alinéa de l'article L. 244-2, de communiquer les informations ou documents ou de communiquer des renseignements erronés est puni de six mois d'emprisonnement et de 7 500 euros d'amende.
