# Article L243-7

La commission remet chaque année au Premier ministre un rapport sur les conditions d'exercice et les résultats de son activité, qui précise notamment le nombre de recommandations qu'elle a adressées au Premier ministre en application

des articles L. 243-8,
L. 246-3 et L. 246-4, ainsi que les suites qui leur ont été données. Ce rapport est rendu public.

La commission adresse, à tout moment, au Premier ministre les observations qu'elle juge utiles.
