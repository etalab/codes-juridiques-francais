# Chapitre Ier : Dispositions générales

- [Article L611-1](article-l611-1.md)
- [Article L611-2](article-l611-2.md)
