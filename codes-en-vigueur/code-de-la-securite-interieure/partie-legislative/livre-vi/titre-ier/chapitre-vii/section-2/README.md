# Section 2 : Modalités d'exercice

- [Sous-section 1 : Activités de surveillance et de gardiennage](sous-section-1)
- [Sous-section 2 : Activités de transport de fonds](sous-section-2)
- [Sous-section 3 : Activités de protection des navires](sous-section-3)
