# Article L612-2

L'exercice d'une activité mentionnée aux 1° et 2° de l'article L. 611-1 est exclusif de toute autre prestation de services non liée à la surveillance, au gardiennage ou au transport de fonds, de bijoux ou de métaux précieux.

L'exercice de l'activité mentionnée au 3° de l'article L. 611-1 est exclusif de toute autre activité.

L'exercice de l'activité mentionnée au 4° du même article L. 611-1 est exclusif de toute autre activité, à l'exception du conseil et de la formation en matière de sûreté maritime.
