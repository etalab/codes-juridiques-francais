# Article L314-2

Une arme de catégorie B ne peut être cédée par un particulier à un autre que dans le cas où le cessionnaire est autorisé à la détenir dans les conditions fixées aux articles L. 312-1 à L. 312-4-3.

Dans tous les cas, les transferts d'armes ou de munitions de la catégorie B sont opérés suivant des formes définies par décret en Conseil d'Etat.
