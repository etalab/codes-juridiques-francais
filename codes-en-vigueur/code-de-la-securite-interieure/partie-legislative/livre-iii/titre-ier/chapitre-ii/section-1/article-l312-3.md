# Article L312-3

Nul ne peut acquérir et détenir légalement des matériels ou des armes des catégories B et C s'il ne remplit pas les conditions suivantes :

1° Disposer d'un bulletin n° 2 de son casier judiciaire ne comportant pas de mention de condamnation pour l'une des infractions suivantes :

-meurtre, assassinat ou empoisonnement prévus aux articles 221-1 et suivants du code pénal ;

-tortures et actes de barbarie prévus aux articles 222-1 et suivants du code pénal ;

-violences volontaires prévues aux articles 222-7 et suivants du code pénal ;

-menaces d'atteinte aux personnes prévues aux articles 222-17 et suivants du code pénal ;

-viol et agressions sexuelles prévus aux articles 222-22 et suivants du code pénal ;

-exhibition sexuelle prévue à l'article 222-32 du code pénal ;

-harcèlement sexuel prévu à l'article 222-33 du code pénal ;

-harcèlement moral prévu aux articles 222-33-2 et 222-33-2-1 du code pénal ;

-enregistrement et diffusion d'images de violence prévus à l'article 222-33-3 du code pénal ;

-trafic de stupéfiants prévu aux articles 222-34 et suivants du code pénal ;

-enlèvement et séquestration prévus aux articles 224-1 et suivants du code pénal ;

-détournement d'aéronef, de navire ou de tout autre moyen de transport prévu aux articles 224-6 et suivants du code pénal ;

-traite des êtres humains prévue aux articles 225-4-1 et suivants du code pénal ;

-proxénétisme et infractions qui en résultent prévus aux articles 225-5 et suivants du code pénal ;

-recours à la prostitution des mineurs ou de personnes particulièrement vulnérables prévu aux articles 225-12-1 et suivants du code pénal ;

-exploitation de la mendicité prévue aux articles 225-12-5 et suivants du code pénal ;

-vols prévus aux articles 311-1 et suivants du code pénal ;

-extorsions prévues aux articles 312-1 et suivants du code pénal ;

-recel de vol ou d'extorsion prévu aux articles 321-1 et suivants du code pénal ;

-destructions, dégradations et détériorations dangereuses pour les personnes prévues aux articles 322-5 et suivants du code pénal ;

-menaces de destruction, de dégradation ou de détérioration et fausses alertes prévues aux articles 322-12 et 322-14 du code pénal ;

-blanchiment prévu aux articles 324-1 et suivants du code pénal ;

-participation à un attroupement en étant porteur d'une arme ou provocation directe à un attroupement armé prévues aux articles 431-5 et 431-6 du code pénal ;

-participation à une manifestation ou à une réunion publique en étant porteur d'une arme prévue à l'article 431-10 du code pénal ;

-intrusion dans un établissement d'enseignement scolaire par une personne porteuse d'une arme prévue aux articles 431-24 et 431-25 du code pénal ;

-introduction d'armes dans un établissement scolaire prévue à l'article 431-28 du code pénal ;

-rébellion armée et rébellion armée en réunion prévues à l'article 433-8 du code pénal ;

-destructions, dégradations et détériorations ne présentant pas de danger pour les personnes prévues aux articles 322-1 et suivants du code pénal commises en état de récidive légale ;

-fabrication ou commerce des matériels de guerre ou d'armes ou de munitions de défense sans autorisation prévus et réprimés par les articles L. 2339-2, L. 2339-3 et L. 2339-4 du code de la défense ainsi que par les articles L. 317-1-1, L. 317-2 et L. 317-3-1 du présent code ;

-acquisition, cession ou détention, sans autorisation, d'une ou plusieurs armes ou matériels des catégories A, B, C ou d'armes de catégorie D mentionnées à l'article L. 312-4-2 ou de leurs munitions prévues et réprimées par les articles L. 317-4, L. 317-5, L. 317-6 et L. 317-7 ;

-port, transport et expéditions d'armes des catégories A, B, C ou d'armes de la catégorie D soumises à enregistrement sans motif légitime prévus et réprimés par les articles L. 317-8 et L. 317-9 ;

-importation sans autorisation des matériels des catégories A, B, C ou d'armes de la catégorie D énumérées par un décret en Conseil d'Etat prévue et réprimée par les articles L. 2339-10 et L. 2339-11 du code de la défense ;

-fabrication, vente, exportation, sans autorisation, d'un engin ou produit explosif ou incendiaire, port ou transport d'artifices non détonants prévus et réprimés par les articles L. 2353-4 à L. 2353-13 du code de la défense ;

2° Ne pas se signaler par un comportement laissant objectivement craindre une utilisation de l'arme ou du matériel dangereuse pour soi-même ou pour autrui.
