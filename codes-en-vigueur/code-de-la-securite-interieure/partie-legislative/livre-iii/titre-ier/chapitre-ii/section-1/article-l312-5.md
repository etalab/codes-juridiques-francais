# Article L312-5

Dans les ventes publiques, seules peuvent se porter acquéreurs des matériels de guerre, armes et munitions et de leurs éléments des catégories A et B ainsi que des armes de catégorie D figurant sur une liste établie par un décret en Conseil d'Etat les personnes physiques ou morales qui peuvent régulièrement acquérir et détenir des matériels et armes de ces différentes catégories en application des sections 1 et 2 du présent chapitre, de l'article L. 313-3 du présent code et de l'article L. 2332-1 du code de la défense.

La vente de ces mêmes matériels par les brocanteurs est interdite.

Un décret en Conseil d'Etat détermine les conditions d'application du présent article.
