# Article L312-1

Nul ne peut acquérir et détenir légalement des matériels ou des armes de toute catégorie s'il n'est pas âgé de dix-huit ans révolus, sous réserve des exceptions définies par décret en Conseil d'Etat pour la chasse et les activités encadrées par la fédération sportive ayant reçu, au titre de l'article L. 131-14 du code du sport, délégation du ministre chargé des sports pour la pratique du tir.
