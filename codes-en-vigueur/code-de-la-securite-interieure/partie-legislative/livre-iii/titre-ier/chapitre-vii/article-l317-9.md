# Article L317-9

Si le transport d'armes est effectué par au moins deux personnes ou si deux personnes au moins sont trouvées ensemble porteuses d'armes, les peines prévues à l'article L. 317-8 sont portées :

1° S'il s'agit de matériels de guerre mentionnés à l'article L. 311-2, d'armes, de leurs éléments essentiels ou de munitions des catégories A ou B, à dix ans d'emprisonnement et 500 000 € d'amende ;

2° S'il s'agit d'armes, de leurs éléments essentiels ou de munitions de catégorie C, à cinq ans d'emprisonnement et 75 000 € d'amende ;

3° S'il s'agit d'armes, de munitions ou de leurs éléments de la catégorie D, à l'exception de ceux qui présentent une faible dangerosité et figurent sur une liste fixée par arrêté, à deux ans d'emprisonnement et 30 000 € d'amende.
