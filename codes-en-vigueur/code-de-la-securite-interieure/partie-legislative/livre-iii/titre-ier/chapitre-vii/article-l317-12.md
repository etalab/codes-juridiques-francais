# Article L317-12

<div align="left">En cas de condamnation pour les infractions prévues au présent chapitre, le prononcé des peines complémentaires suivantes est obligatoire : <br/>
<br/> 1° L'interdiction de détenir ou de porter, pour une durée de cinq ans au plus, une arme soumise à autorisation ; <br/>
<br/> 2° La confiscation d'une ou de plusieurs armes dont le condamné est propriétaire ou dont il a la libre disposition ; <br/>
<br/> 3° Le retrait du permis de chasser avec interdiction de solliciter la délivrance d'un nouveau permis pendant cinq ans au plus. <br/>
<br/> Toutefois, la juridiction peut, par une décision spécialement motivée, décider de ne pas prononcer ces peines, en considération des circonstances de l'infraction et de la personnalité de son auteur. <br/>
</div>
