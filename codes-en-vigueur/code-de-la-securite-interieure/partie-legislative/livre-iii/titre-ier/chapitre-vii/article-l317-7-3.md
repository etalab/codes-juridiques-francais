# Article L317-7-3

Les peines peuvent être portées à dix ans d'emprisonnement et 150 000 € d'amende si les infractions mentionnées à l'article L. 317-7-2 sont commises en bande organisée.
