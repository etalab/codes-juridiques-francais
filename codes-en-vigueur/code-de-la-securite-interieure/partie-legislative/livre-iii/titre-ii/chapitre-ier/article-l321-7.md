# Article L321-7

Sauf disposition contraire, les conditions d'application du présent chapitre sont définies par décret en Conseil d'Etat.

La liste des jeux de hasard pouvant être autorisés dans les casinos est fixée par décret.
