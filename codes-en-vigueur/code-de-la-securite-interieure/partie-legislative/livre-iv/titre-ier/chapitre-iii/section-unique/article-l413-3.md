# Article L413-3

Les ressources de l'Institut national de police scientifique sont constituées par des subventions de l'Etat ou des autres personnes publiques, par les honoraires d'expertise et autres redevances pour services rendus, par les produits des emprunts, par les dons et legs et par le produit des ventes qu'il effectue dans le cadre de ses missions.
