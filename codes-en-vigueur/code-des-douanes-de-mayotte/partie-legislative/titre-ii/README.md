# Titre II : Organisation et fonctionnement du service des douanes

- [Chapitre Ier : Champ d'action du service des douanes.](chapitre-ier)
- [Chapitre II : Organisation des bureaux et des brigades de douane](chapitre-ii)
- [Chapitre III : Immunités, sauvegarde et obligations des agents de douanes.](chapitre-iii)
- [Chapitre IV : Pouvoirs des agents des douanes](chapitre-iv)
