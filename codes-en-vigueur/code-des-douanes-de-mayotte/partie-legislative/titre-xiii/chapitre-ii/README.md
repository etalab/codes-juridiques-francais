# Chapitre II : Constatation des infractions.

- [Article 315](article-315.md)
- [Article 316](article-316.md)
- [Article 317](article-317.md)
- [Article 318](article-318.md)
- [Article 319](article-319.md)
