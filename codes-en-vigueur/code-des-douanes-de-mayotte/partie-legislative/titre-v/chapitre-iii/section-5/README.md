# Section 5 : L'entrepôt spécial

- [Paragraphe 1 : Etablissement de l'entrepôt spécial.](paragraphe-1)
- [Paragraphe 2 : Séjour des marchandises.](paragraphe-2)
