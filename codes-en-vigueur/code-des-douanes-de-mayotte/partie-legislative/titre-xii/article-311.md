# Article 311

1. Les constatations matérielles et techniques faites par la commission, portant sur l'espèce, l'origine ou la valeur des marchandises litigieuses sont les seuls modes de preuve et d'expertise admis auprès des tribunaux.

2. Chaque fois que la juridiction compétente considère que la commission s'est prononcée dans des conditions irrégulières ou encore si elle s'estime insuffisamment informée ou enfin si elle n'admet pas les constatations matérielles ou techniques de la commission, elle renvoie devant ladite commission. Dans ces cas, le président de la commission peut désigner de nouveaux assesseurs techniques.

3. Le jugement de renvoi pour complément de la procédure doit énoncer d'une manière précise les points à examiner par la commission et lui impartir un délai pour l'accomplissement de cette mission.

4. Lorsqu'il a été interjeté appel du jugement de renvoi prévu au 3 ci-dessus, la procédure est poursuivie à moins que le juge d'appel n'en décide autrement.
