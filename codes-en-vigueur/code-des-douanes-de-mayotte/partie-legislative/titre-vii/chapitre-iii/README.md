# Chapitre III : Importation et exportation en franchise temporaire des objets destinés à l'usage personnel des voyageurs.

- [Article 161](article-161.md)
- [Article 162](article-162.md)
