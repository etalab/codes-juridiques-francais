# SECTION I : Concession à titre normal

- [PARAGRAPHE 1 : Dispositions générales.](paragraphe-1)
- [PARAGRAPHE 2 : Dispositions particulières.](paragraphe-2)
- [PARAGRAPHE 3 : Concession de la médaille militaire aux officiers généraux.](paragraphe-3)
