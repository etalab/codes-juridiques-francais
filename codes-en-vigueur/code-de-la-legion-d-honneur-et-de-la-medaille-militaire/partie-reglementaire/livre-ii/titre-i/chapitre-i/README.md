# CHAPITRE I : Conditions de concession

- [SECTION I : Concession à titre normal](section-i)
- [SECTION II : Concession de la médaille militaire en cas de décès ou de blessures.](section-ii)
