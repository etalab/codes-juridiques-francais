# CHAPITRE III : Cérémonial

- [SECTION I : Réception des civils.](section-i)
- [SECTION II : Réception des militaires.](section-ii)
- [SECTION III : Dispositions communes.](section-iii)
