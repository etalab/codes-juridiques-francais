# Article R108

Dans le cas prévu à l'article R. 93, le grand chancelier prend l'avis du conseil de l'ordre et fait inscrire sur les matricules de la Légion d'honneur la mention de suspension en précisant que la personne ainsi frappée est privée, pendant la durée de la suspension, de l'exercice de tous les droits et prérogatives attachés à la qualité de membre de l'ordre ainsi que du droit au traitement afférent.
