# PARAGRAPHE 1 : Dispositions concernant les mutilés dont le degré d'invalidité est au moins égal à 65 p. 100.

- [Article R39](article-r39.md)
- [Article R40](article-r40.md)
- [Article R41](article-r41.md)
