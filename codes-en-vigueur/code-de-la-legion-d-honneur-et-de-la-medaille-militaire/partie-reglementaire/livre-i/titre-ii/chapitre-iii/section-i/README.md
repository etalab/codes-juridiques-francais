# SECTION I : Tableaux spéciaux.

- [Article R36](article-r36.md)
- [Article R37](article-r37.md)
- [Article R38](article-r38.md)
