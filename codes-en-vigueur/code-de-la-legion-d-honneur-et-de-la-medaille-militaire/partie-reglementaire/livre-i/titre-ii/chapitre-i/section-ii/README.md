# SECTION II : Propositions à titre exceptionnel.

- [Article R25](article-r25.md)
- [Article R26](article-r26.md)
- [Article R27](article-r27.md)
