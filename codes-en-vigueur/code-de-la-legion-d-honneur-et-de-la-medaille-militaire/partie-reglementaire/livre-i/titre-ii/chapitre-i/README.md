# CHAPITRE I : Conditions de nomination et de promotion.

- [SECTION I : Propositions à titre normal](section-i)
- [SECTION II : Propositions à titre exceptionnel.](section-ii)
- [Article R16](article-r16.md)
- [Article R17](article-r17.md)
