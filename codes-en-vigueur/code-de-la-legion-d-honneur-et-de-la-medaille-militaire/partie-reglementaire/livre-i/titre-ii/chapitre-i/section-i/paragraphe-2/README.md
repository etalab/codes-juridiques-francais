# PARAGRAPHE 2 : Dispositions particulières.

- [Article R21](article-r21.md)
- [Article R22](article-r22.md)
- [Article R23](article-r23.md)
- [Article R24](article-r24.md)
