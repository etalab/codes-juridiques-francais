# Article R88

Les honneurs funèbres militaires dus aux dignitaires de l'ordre national de la Légion d'honneur sont rendus conformément aux dispositions des articles 45 et 48 du décret n° 89-655 du 13 septembre 1989 relatif aux cérémonies publiques, préséances, honneurs civils et militaires.
