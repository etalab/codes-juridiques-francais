# Sous-section 4 : Dispositions applicables en cas de consignation et de contrôle automatisé

- [Article A37-12](article-a37-12.md)
- [Article A37-13](article-a37-13.md)
- [Article A37-14](article-a37-14.md)
