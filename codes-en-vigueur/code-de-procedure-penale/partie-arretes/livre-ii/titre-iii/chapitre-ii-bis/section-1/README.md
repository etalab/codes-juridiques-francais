# Section 1 : Dispositions relatives au procès-verbal, à l'avis de contravention et à la carte de paiement concernant les contraventions forfaitisées

- [Sous-section 1 : Dispositions communes](sous-section-1)
- [Sous-section 2 : Dispositions applicables aux contraventions ne donnant pas lieu à retrait de points du permis de conduire](sous-section-2)
- [Sous-section 3 : Dispositions applicables aux contraventions donnant lieu à retrait de points du permis de conduire](sous-section-3)
- [Sous-section 4 : Dispositions applicables en cas de consignation et de contrôle automatisé](sous-section-4)
- [Sous-section 5 : Dispositions spécifiques applicables en cas de constatation ne permettant pas l'édition immédiate de l'avis de contravention et en cas d'utilisation d'un appareil électronique sécurisé](sous-section-5)
