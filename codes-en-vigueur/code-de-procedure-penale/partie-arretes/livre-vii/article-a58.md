# Article A58

Le siège du service pénitentiaire d'insertion et de probation et la liste des antennes locales d'insertion et de probation de la Nouvelle-Calédonie prévus par l'article D.N.C. 572 sont fixés conformément au tableau ci-dessous :

<table>
<tbody>
<tr>
<td>
<p align="center">SPIP</p>
</td>
<td>
<p align="center">SIÈGE</p>
</td>
<td>
<p align="center">ANTENNE</p>
</td>
<td>
<p align="center">RESSORT DE COMPÉTENCE</p>
</td>
</tr>
<tr>
<td valign="top">
<p>Nouvelle-Calédonie.</p>
</td>
<td valign="top">
<p>Nouméa.</p>
</td>
<td valign="top">
<p>Nouméa.</p>
</td>
<td valign="top">
<p>Camp est.</p>
</td>
</tr>
<tr>
<td valign="top"/>
<td valign="top"/>
<td valign="top"/>
<td valign="top">
<p>Province sud.</p>
</td>
</tr>
<tr>
<td valign="top"/>
<td valign="top"/>
<td valign="top"/>
<td valign="top">
<p>Province des îles Loyauté.</p>
</td>
</tr>
<tr>
<td valign="top"/>
<td valign="top"/>
<td valign="top">
<p>Koné.</p>
</td>
<td valign="top">
<p>Province nord.</p>
</td>
</tr>
</tbody>
</table>
