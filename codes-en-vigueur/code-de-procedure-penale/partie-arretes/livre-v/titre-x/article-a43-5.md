# Article A43-5

Conformément aux dispositions de l'article R. 121, les indemnités dues, en application des articles R. 121-3 et R. 121-4, aux associations habilitées ayant passé une convention avec le premier président et le procureur général de la cour d'appel, sont fixées par le tableau ci-après :

<table>
<tbody>
<tr>
<td align="center">
<br/>IA. ¹ <br/>
</td>
<td align="center">
<br/>70 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>IA. ² <br/>
</td>
<td align="center">
<br/>70 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>IA. ³ <br/>
</td>
<td align="center">
<br/>1110 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>IA. <sup>4</sup>
<br/>
</td>
<td align="center">
<br/>925 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>IA. 5 <br/>
</td>
<td align="center">
<br/>370 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>IA.<sup> 6</sup>
<br/>
</td>
<td align="center">
<br/>12 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>IA. <sup>7</sup>
<br/>
</td>
<td align="center">
<br/>31 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>IA. <sup>8 <br/>
</sup>
</td>
<td align="center">
<br/>31 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>IA. <sup>9</sup>
<br/>
</td>
<td align="center">
<br/>77, lorsque la durée de la mission de médiation est inférieure ou égale à un mois. <p>153, lorsque cette durée est supérieure à un mois et inférieure ou égale à trois mois. </p>
<p>305, lorsqu'elle est supérieure à trois mois. <br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<br/>IA. <sup>10</sup>
<br/>
</td>
<td align="center">
<br/>31 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>IA. <sup>11 <br/>
</sup>
</td>
<td align="center">
<br/>16 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>IA. <sup>12</sup>
<br/>
</td>
<td align="center">
<br/>31 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>IA. <sup>13 <br/>
</sup>
</td>
<td align="center">
<br/>8 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>IA. <sup>14</sup>
<br/>
</td>
<td align="center">
<br/>25 <br/>
</td>
</tr>
</tbody>
</table>

L'indemnité IA. <sup>14</sup> prévue en cas de carence n'est applicable que lorsque qu'elle est inférieure à l'indemnité correspondant à l'accomplissement de la mission : elle n'est due qu'à la condition qu'au moins deux convocations aient été adressées à la personne faisant l'objet de la mesure.
