# Titre VII ter : Du placement sous surveillance électronique mobile à titre de mesure de sûreté

- [Article 763-10](article-763-10.md)
- [Article 763-11](article-763-11.md)
- [Article 763-12](article-763-12.md)
- [Article 763-13](article-763-13.md)
- [Article 763-14](article-763-14.md)
