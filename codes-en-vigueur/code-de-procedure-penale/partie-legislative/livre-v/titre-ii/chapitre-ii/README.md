# Chapitre II : De l'exécution des peines privatives de liberté

- [Section 1 : Dispositions générales](section-1)
- [Section 1 bis : De la libération sous contrainte](section-1-bis)
- [Section 2 : De la suspension et du fractionnement des peines privatives de liberté](section-2)
- [Section 3 : De la période de sûreté](section-3)
- [Section 4 : Des réductions de peines](section-4)
- [Section 9 : Dispositions relatives à la surveillance judiciaire de personnes dangereuses condamnées pour crime ou délit](section-9)
