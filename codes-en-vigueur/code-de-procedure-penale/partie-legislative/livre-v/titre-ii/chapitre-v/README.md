# Chapitre V : Du transfèrement des personnes condamnées

- [Article 728-2](article-728-2.md)
- [Article 728-3](article-728-3.md)
- [Article 728-4](article-728-4.md)
- [Article 728-5](article-728-5.md)
- [Article 728-6](article-728-6.md)
- [Article 728-7](article-728-7.md)
- [Article 728-8](article-728-8.md)
- [Article 728-9](article-728-9.md)
