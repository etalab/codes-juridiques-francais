# Chapitre VI : De l'exécution des décisions de condamnation à une peine ou à une mesure de sûreté privative de liberté en application de la décision-cadre 2008/909/ JAI du Conseil du 27 novembre 2008 concernant l'application du principe de reconnaissance mutuelle aux jugements en matière pénale prononçant des peines ou des mesures privatives de liberté aux fins de leur exécution dans l'Union européenne.

- [Section 1 : Dispositions générales.](section-1)
- [Section 2 : Dispositions relatives à l'exécution, sur le territoire des autres Etats membres de l'Union européenne, des condamnations prononcées par les juridictions françaises.](section-2)
- [Section 3 : Dispositions relatives à l'exécution sur le territoire français des condamnations prononcées par les juridictions des autres Etats membres de l'Union européenne.](section-3)
- [Section 4 : Dispositions relatives au transit
sur le territoire français.](section-4)
