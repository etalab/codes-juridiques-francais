# Paragraphe 1 : Dispositions générales

- [Article 713](article-713.md)
- [Article 713-1](article-713-1.md)
- [Article 713-2](article-713-2.md)
- [Article 713-3](article-713-3.md)
- [Article 713-4](article-713-4.md)
