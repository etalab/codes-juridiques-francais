# Section 2 : Compétence et procédure devant les juridictions du premier degré

- [Article 712-4](article-712-4.md)
- [Article 712-5](article-712-5.md)
- [Article 712-6](article-712-6.md)
- [Article 712-7](article-712-7.md)
- [Article 712-8](article-712-8.md)
- [Article 712-9](article-712-9.md)
- [Article 712-10](article-712-10.md)
