# Dispositions générales

- [Article 801](article-801.md)
- [Article 801-1](article-801-1.md)
- [Article 802](article-802.md)
- [Article 803](article-803.md)
- [Article 803-1](article-803-1.md)
- [Article 803-2](article-803-2.md)
- [Article 803-3](article-803-3.md)
- [Article 803-4](article-803-4.md)
- [Article 803-5](article-803-5.md)
- [Article 803-6](article-803-6.md)
