# Paragraphe 1 : Dispositions générales

- [Article 462](article-462.md)
- [Article 463](article-463.md)
- [Article 464](article-464.md)
- [Article 464-1](article-464-1.md)
- [Article 465](article-465.md)
- [Article 465-1](article-465-1.md)
- [Article 466](article-466.md)
- [Article 467](article-467.md)
- [Article 468](article-468.md)
- [Article 469-1](article-469-1.md)
- [Article 470](article-470.md)
- [Article 470-1](article-470-1.md)
- [Article 470-2](article-470-2.md)
- [Article 471](article-471.md)
- [Article 472](article-472.md)
- [Article 474](article-474.md)
- [Article 474-1](article-474-1.md)
- [Article 475-1](article-475-1.md)
- [Article 478](article-478.md)
- [Article 479](article-479.md)
- [Article 480](article-480.md)
- [Article 480-1](article-480-1.md)
- [Article 481](article-481.md)
- [Article 482](article-482.md)
- [Article 484](article-484.md)
- [Article 484-1](article-484-1.md)
- [Article 485](article-485.md)
- [Article 486](article-486.md)
