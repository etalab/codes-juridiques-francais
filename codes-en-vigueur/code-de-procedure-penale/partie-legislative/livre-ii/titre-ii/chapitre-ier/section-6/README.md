# Section 6 : Du jugement par défaut et de l'opposition

- [Paragraphe 1er : Du défaut](paragraphe-1er)
- [Paragraphe 2 : De l'opposition](paragraphe-2)
- [Paragraphe 3 : De l'itératif défaut](paragraphe-3)
