# Paragraphe 2 : De l'opposition

- [Article 489](article-489.md)
- [Article 490](article-490.md)
- [Article 490-1](article-490-1.md)
- [Article 491](article-491.md)
- [Article 492](article-492.md)
- [Article 493](article-493.md)
