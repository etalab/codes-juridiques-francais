# Paragraphe 2 : De la comparution volontaire et de la citation

- [Article 389](article-389.md)
- [Article 390](article-390.md)
- [Article 390-1](article-390-1.md)
- [Article 390-2](article-390-2.md)
- [Article 391](article-391.md)
- [Article 392](article-392.md)
- [Article 392-1](article-392-1.md)
