# Section 7 : De la procédure simplifiée

- [Article 495](article-495.md)
- [Article 495-1](article-495-1.md)
- [Article 495-2](article-495-2.md)
- [Article 495-2-1](article-495-2-1.md)
- [Article 495-3](article-495-3.md)
- [Article 495-3-1](article-495-3-1.md)
- [Article 495-4](article-495-4.md)
- [Article 495-5](article-495-5.md)
- [Article 495-5-1](article-495-5-1.md)
- [Article 495-6](article-495-6.md)
