# Chapitre IV : De la procédure préparatoire aux sessions d'assises

- [Section 1 : Des actes obligatoires](section-1)
- [Section 2 : Des actes facultatifs ou exceptionnels](section-2)
