# Section 2 : Des actes facultatifs ou exceptionnels

- [Article 283](article-283.md)
- [Article 284](article-284.md)
- [Article 285](article-285.md)
- [Article 286](article-286.md)
- [Article 286-1](article-286-1.md)
- [Article 287](article-287.md)
