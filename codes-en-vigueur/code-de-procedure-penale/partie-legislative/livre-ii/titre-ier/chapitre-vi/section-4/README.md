# Section 4 : De la clôture des débats et de la lecture des questions

- [Article 347](article-347.md)
- [Article 348](article-348.md)
- [Article 349](article-349.md)
- [Article 349-1](article-349-1.md)
- [Article 350](article-350.md)
- [Article 351](article-351.md)
- [Article 352](article-352.md)
- [Article 353](article-353.md)
- [Article 354](article-354.md)
