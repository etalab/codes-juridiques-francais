# Chapitre III : Mesures de sûreté pouvant être ordonnées en cas de déclaration d'irresponsabilité pénale pour cause de trouble mental ou en cas de reconnaissance d'altération du discernement

- [Article 706-135](article-706-135.md)
- [Article 706-136](article-706-136.md)
- [Article 706-136-1](article-706-136-1.md)
- [Article 706-137](article-706-137.md)
- [Article 706-138](article-706-138.md)
- [Article 706-139](article-706-139.md)
- [Article 706-140](article-706-140.md)
