# Titre XVI : De la poursuite, de l'instruction et du jugement des infractions en matière de trafic de stupéfiants

- [Article 706-26](article-706-26.md)
- [Article 706-27](article-706-27.md)
- [Article 706-28](article-706-28.md)
- [Article 706-30-1](article-706-30-1.md)
- [Article 706-31](article-706-31.md)
- [Article 706-32](article-706-32.md)
- [Article 706-33](article-706-33.md)
