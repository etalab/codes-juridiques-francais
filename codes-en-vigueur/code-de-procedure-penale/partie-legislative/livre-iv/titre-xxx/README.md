# Titre XXX : De l'agence de gestion et de recouvrement des avoirs saisis et confisqués.

- [Chapitre Ier : Des missions de l'agence](chapitre-ier)
- [Chapitre II : De l'organisation de l'agence](chapitre-ii)
- [Chapitre III : Du paiement des dommages et intérêts sur les biens confisqués](chapitre-iii)
