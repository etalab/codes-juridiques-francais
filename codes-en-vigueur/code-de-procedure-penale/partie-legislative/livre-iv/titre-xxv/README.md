# Titre XXV : De la procédure applicable à la criminalité et à la délinquance organisées

- [Chapitre Ier : Compétence des juridictions spécialisées](chapitre-ier)
- [Chapitre II : Procédure](chapitre-ii)
- [Article 706-73](article-706-73.md)
- [Article 706-74](article-706-74.md)
