# Section 4 : Des perquisitions

- [Article 706-89](article-706-89.md)
- [Article 706-90](article-706-90.md)
- [Article 706-91](article-706-91.md)
- [Article 706-92](article-706-92.md)
- [Article 706-93](article-706-93.md)
- [Article 706-94](article-706-94.md)
