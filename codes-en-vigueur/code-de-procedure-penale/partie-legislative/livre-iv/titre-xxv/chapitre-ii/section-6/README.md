# Section 6 : Des sonorisations et des fixations d'images de certains lieux ou véhicules

- [Article 706-96](article-706-96.md)
- [Article 706-97](article-706-97.md)
- [Article 706-98](article-706-98.md)
- [Article 706-99](article-706-99.md)
- [Article 706-100](article-706-100.md)
- [Article 706-101](article-706-101.md)
- [Article 706-102](article-706-102.md)
