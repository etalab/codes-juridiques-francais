# Titre XXII : Saisine pour avis de la Cour de cassation

- [Article 706-64](article-706-64.md)
- [Article 706-65](article-706-65.md)
- [Article 706-66](article-706-66.md)
- [Article 706-67](article-706-67.md)
- [Article 706-68](article-706-68.md)
- [Article 706-69](article-706-69.md)
- [Article 706-70](article-706-70.md)
