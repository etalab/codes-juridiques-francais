# Chapitre II : De la chambre de l'instruction : juridiction d'instruction du second degré

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Pouvoirs propres du président de la chambre de l'instruction](section-2)
- [Section 3 : Du contrôle de l'activité des officiers et agents de police judiciaire](section-3)
