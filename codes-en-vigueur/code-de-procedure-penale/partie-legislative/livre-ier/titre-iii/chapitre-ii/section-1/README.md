# Section 1 : Dispositions générales

- [Article 191](article-191.md)
- [Article 192](article-192.md)
- [Article 193](article-193.md)
- [Article 194](article-194.md)
- [Article 195](article-195.md)
- [Article 196](article-196.md)
- [Article 197](article-197.md)
- [Article 197-1](article-197-1.md)
- [Article 198](article-198.md)
- [Article 199](article-199.md)
- [Article 200](article-200.md)
- [Article 201](article-201.md)
- [Article 202](article-202.md)
- [Article 203](article-203.md)
- [Article 204](article-204.md)
- [Article 205](article-205.md)
- [Article 206](article-206.md)
- [Article 207](article-207.md)
- [Article 207-1](article-207-1.md)
- [Article 208](article-208.md)
- [Article 209](article-209.md)
- [Article 210](article-210.md)
- [Article 211](article-211.md)
- [Article 212](article-212.md)
- [Article 212-1](article-212-1.md)
- [Article 212-2](article-212-2.md)
- [Article 213](article-213.md)
- [Article 214](article-214.md)
- [Article 215](article-215.md)
- [Article 216](article-216.md)
- [Article 217](article-217.md)
- [Article 218](article-218.md)
