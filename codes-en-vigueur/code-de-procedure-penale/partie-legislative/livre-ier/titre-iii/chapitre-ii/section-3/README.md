# Section 3 : Du contrôle de l'activité des officiers et agents de police judiciaire

- [Article 224](article-224.md)
- [Article 225](article-225.md)
- [Article 226](article-226.md)
- [Article 227](article-227.md)
- [Article 228](article-228.md)
- [Article 229](article-229.md)
- [Article 230](article-230.md)
