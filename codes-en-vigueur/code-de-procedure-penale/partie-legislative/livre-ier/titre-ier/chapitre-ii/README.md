# Chapitre II : Du ministère public

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Des attributions du procureur général près la cour d'appel](section-2)
- [Section 3 : Des attributions du procureur de la République](section-3)
- [Section 4 : Du ministère public près le tribunal de police](section-4)
- [Section 5 : Du bureau d'ordre national automatisé des procédures judiciaires](section-5)
