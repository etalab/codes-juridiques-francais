# Chapitre Ier : De la police judiciaire

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Des officiers de police judiciaire](section-2)
- [Section 3 : Des agents de police judiciaire](section-3)
- [Section 4 : Des fonctionnaires et agents chargés de certaines fonctions de police judiciaire](section-4)
