# Article D47-38

Par application des dispositions de l'article 706-176, les tribunaux de grande instance désignés dans le tableau figurant ci-dessous sont compétents pour connaître, dans les circonscriptions définies à ce tableau, des infractions mentionnées à cet article.

<table>
<tbody>
<tr>
<th>
<br/>TRIBUNAUX <br/>
<br/>de grande instance compétents <br/>
</th>
<th>
<br/>COMPÉTENCE TERRITORIALE <br/>
<br/>s'étendant au ressort des cours d'appel ou du tribunal supérieur d'appel de : <br/>
</th>
</tr>
<tr>
<td align="center" valign="middle">
<br/>Marseille <br/>
</td>
<td>
<br/>Aix-en-Provence, Bastia, Chambéry, Grenoble, Lyon, Nîmes et Montpellier <br/>
</td>
</tr>
<tr>
<td align="center" valign="middle">
<br/>Paris <br/>
</td>
<td>
<br/>Agen, Amiens, Angers, Basse-Terre, Besançon, Bordeaux, Bourges, Caen, Cayenne, Colmar, Dijon, Douai, Fort-de-France, Limoges, Metz, Nancy, Nouméa, Orléans, Papeete, Paris, Pau, Poitiers, Reims, Rennes, Riom, Rouen, Saint-Denis, Toulouse, Versailles et Saint-Pierre<br/>
</td>
</tr>
</tbody>
</table>
