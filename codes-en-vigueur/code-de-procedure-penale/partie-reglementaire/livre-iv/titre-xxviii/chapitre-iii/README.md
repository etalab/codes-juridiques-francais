# Chapitre III : Mesures de sûreté pouvant être ordonnées en cas de déclaration d'irresponsabilité pénale pour cause de trouble mental.

- [Section 1 : De l'hospitalisation d'office](section-1)
- [Section 2 : Des autres mesures de sûreté](section-2)
