# Section 1 : De l'hospitalisation d'office

- [Article D47-29](article-d47-29.md)
- [Article D47-29-1](article-d47-29-1.md)
- [Article D47-29-2](article-d47-29-2.md)
- [Article D47-29-3](article-d47-29-3.md)
- [Article D47-29-4](article-d47-29-4.md)
- [Article D47-29-5](article-d47-29-5.md)
