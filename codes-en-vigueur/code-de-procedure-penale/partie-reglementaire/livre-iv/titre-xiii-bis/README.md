# Titre XIII bis : De la procédure applicable en matière sanitaire

- [Article D47-5](article-d47-5.md)
- [Article D47-6](article-d47-6.md)
