# Article D47-5

Par application des dispositions de l'article 706-2 du code de procédure pénale, les tribunaux de grande instance désignés dans le tableau figurant ci-dessous sont compétents pour connaître, dans les circonscriptions définies à ce tableau, des infractions mentionnées à cet article.

<table>
<tbody>
<tr>
<td width="245">
<p align="center">TRIBUNAUX <br/>de grande instance compétents </p>
</td>
<td width="360">
<p align="center">COMPÉTENCE TERRITORIALE <br/>s'étendant au ressort des cours d'appel <br/>ou des tribunaux supérieurs d'appel de : </p>
</td>
</tr>
<tr>
<td valign="top" width="245">
<p>Marseille </p>
</td>
<td valign="top" width="360">
<p>Aix-en-Provence, Bastia, Chambéry, Grenoble, Lyon, Nîmes et Montpellier </p>
</td>
</tr>
<tr>
<td valign="top" width="245">
<p>Paris </p>
</td>
<td valign="top" width="360">
<p>Agen, Amiens, Angers, Basse-Terre, Besançon, Bordeaux, Bourges, Caen, Cayenne, Colmar, Dijon, Douai, Fort-de-France, Limoges, Metz, Nancy, Nouméa, Orléans, Papeete, Paris, Pau, Poitiers, Reims, Rennes, Riom, Rouen, Saint-Denis-de-La-Réunion, Toulouse, Versailles et du tribunal supérieur d'appel de Saint-Pierre-et-Miquelon.</p>
</td>
</tr>
</tbody>
</table>
