# Section 1 : Modalités d'application des dispositions
concernant le droit à un interprète

- [Paragraphe 1 : Droit à l'interprète lors des auditions](paragraphe-1)
- [Paragraphe 2 : Droit à l'interprète lors des entretiens de la personne avec son avocat](paragraphe-2)
- [Paragraphe 3 : Dispositions communes](paragraphe-3)
