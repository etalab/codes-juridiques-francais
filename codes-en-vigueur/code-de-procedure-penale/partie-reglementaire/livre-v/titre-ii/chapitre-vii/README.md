# Chapitre VII : De la gestion des biens et de l'entretien des personnes détenues

- [Section 1 : De la gestion des biens des détenus](section-1)
- [Section 2 : De l'entretien des détenus](section-2)
