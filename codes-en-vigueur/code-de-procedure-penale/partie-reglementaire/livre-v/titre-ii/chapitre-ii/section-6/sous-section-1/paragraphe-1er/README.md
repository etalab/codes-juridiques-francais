# Paragraphe 1er : De la mise en oeuvre du crédit de réduction de peine.

- [Article D115](article-d115.md)
- [Article D115-1](article-d115-1.md)
- [Article D115-2](article-d115-2.md)
- [Article D115-3](article-d115-3.md)
- [Article D115-4](article-d115-4.md)
- [Article D115-5](article-d115-5.md)
- [Article D115-6](article-d115-6.md)
