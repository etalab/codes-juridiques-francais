# Sous-section 1 : Du crédit de réduction de peine.

- [Paragraphe 1er : De la mise en oeuvre du crédit de réduction de peine.](paragraphe-1er)
- [Paragraphe 2 : Du retrait du crédit de réduction de peine.](paragraphe-2)
