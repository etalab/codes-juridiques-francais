# Paragraphe 2 : De la réduction de peine supplémentaire.

- [Article D116-2](article-d116-2.md)
- [Article D116-3](article-d116-3.md)
- [Article D116-4](article-d116-4.md)
