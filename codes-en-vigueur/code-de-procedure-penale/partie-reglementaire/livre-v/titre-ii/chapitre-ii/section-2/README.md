# Section 2 : De la procédure d'orientation et des décisions d'affectation des condamnés

- [Paragraphe 1er : Dispositions générales](paragraphe-1er)
- [Paragraphe 2 : La procédure d'orientation](paragraphe-2)
- [Paragraphe 3 : La décision d'affectation](paragraphe-3)
- [Paragraphe 4 : Changements d'affectation](paragraphe-4)
