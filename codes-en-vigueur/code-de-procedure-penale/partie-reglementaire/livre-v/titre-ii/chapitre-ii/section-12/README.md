# Section 12 : Dispositions relatives au suivi des condamnés après leur libération pendant le temps des réductions de peine

- [Article D147-45](article-d147-45.md)
- [Article D147-46](article-d147-46.md)
- [Article D147-47](article-d147-47.md)
- [Article D147-48](article-d147-48.md)
- [Article D147-49](article-d147-49.md)
- [Article D147-50](article-d147-50.md)
- [Article D147-51](article-d147-51.md)
