# Sous-section 3 : De l'accès des mineurs détenus à l'enseignement, à la formation et aux activités socio-éducatives, culturelles et sportives

- [Article D516](article-d516.md)
- [Article D517](article-d517.md)
- [Article D517-1](article-d517-1.md)
- [Article D518](article-d518.md)
- [Article D518-1](article-d518-1.md)
- [Article D518-2](article-d518-2.md)
