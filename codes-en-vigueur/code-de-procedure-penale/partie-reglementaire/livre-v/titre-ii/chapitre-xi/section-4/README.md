# Section 4 : Des détenus mineurs

- [Sous-section 1 : Dispositions générales](sous-section-1)
- [Sous-section 2 : Du maintien des liens familiaux](sous-section-2)
- [Sous-section 3 : De l'accès des mineurs détenus à l'enseignement, à la formation et aux activités socio-éducatives, culturelles et sportives](sous-section-3)
- [Sous-section 4 : De la santé des mineurs](sous-section-4)
- [Sous-section 5 : De la mesure de protection individuelle](sous-section-5)
