# Titre II : De la détention

- [Chapitre Ier : De l'exécution de la détention provisoire](chapitre-ier)
- [Chapitre II : Des conditions générales de détention](chapitre-ii)
- [Chapitre IV : De l'administration des établissements pénitentiaires](chapitre-iv)
- [Chapitre VI : Des mouvements de personnes détenues](chapitre-vi)
- [Chapitre VII : De la gestion des biens et de l'entretien des personnes détenues](chapitre-vii)
- [Chapitre IX : Des relations des personnes détenues avec l'extérieur](chapitre-ix)
- [Chapitre X : Des actions de préparation à la réinsertion des personnes détenues](chapitre-x)
- [Chapitre XI : De différentes catégories de personnes détenues](chapitre-xi)
- [Article D50](article-d50.md)
- [Article D52](article-d52.md)
- [Article D52-1](article-d52-1.md)
