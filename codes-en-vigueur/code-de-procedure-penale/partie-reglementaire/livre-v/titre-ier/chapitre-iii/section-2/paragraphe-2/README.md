# Paragraphe 2 : Dispositions relatives au juge de l'application des peines

- [Article D49-27](article-d49-27.md)
- [Article D49-28](article-d49-28.md)
- [Article D49-29](article-d49-29.md)
- [Article D49-30](article-d49-30.md)
- [Article D49-31](article-d49-31.md)
- [Article D49-32](article-d49-32.md)
- [Article D49-33](article-d49-33.md)
- [Article D49-34](article-d49-34.md)
- [Article D49-34-1](article-d49-34-1.md)
- [Article D49-35](article-d49-35.md)
- [Article D49-35-1](article-d49-35-1.md)
- [Article D49-35-2](article-d49-35-2.md)
