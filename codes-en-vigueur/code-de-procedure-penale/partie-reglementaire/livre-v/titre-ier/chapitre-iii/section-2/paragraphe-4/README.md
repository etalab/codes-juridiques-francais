# Paragraphe 4 : Dispositions applicables en cas d'appel

- [Article D49-39](article-d49-39.md)
- [Article D49-39-1](article-d49-39-1.md)
- [Article D49-40](article-d49-40.md)
- [Article D49-41](article-d49-41.md)
- [Article D49-41-1](article-d49-41-1.md)
- [Article D49-41-2](article-d49-41-2.md)
- [Article D49-42](article-d49-42.md)
- [Article D49-42-1](article-d49-42-1.md)
- [Article D49-43](article-d49-43.md)
- [Article D49-44](article-d49-44.md)
- [Article D49-44-1](article-d49-44-1.md)
