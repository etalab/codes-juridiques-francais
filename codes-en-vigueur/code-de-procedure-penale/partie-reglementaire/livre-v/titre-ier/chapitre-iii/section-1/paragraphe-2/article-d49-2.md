# Article D49-2

Sauf dans les cours d'appel figurant dans le tableau ci-après, il est établi dans chaque cour d'appel un tribunal de l'application des peines dont la compétence territoriale s'étend au ressort de cette cour.

<table>
<tbody>
<tr>
<td width="181">
<p align="center">COURS D'APPEL</p>
</td>
<td width="209">
<p align="center">TRIBUNAUX DE GRANDE INSTANCE<br/>sièges des tribunaux d'application des peines de ces cours</p>
</td>
<td width="215">
<p align="center">RESSORT DE CES TRIBUNAUX<br/>d'application des peines</p>
</td>
</tr>
<tr>
<td valign="top" width="181">
<p>Aix-en-Provence</p>
</td>
<td valign="top" width="209">
<p>Aix-en Provence</p>
</td>
<td valign="top" width="215">
<p>Ressorts des tribunaux de grande instance d'Aix-en-Provence, Marseille, Digne et Tarascon</p>
</td>
</tr>
<tr>
<td valign="top" width="181"/>
<td valign="top" width="209">
<p>Draguignan</p>
</td>
<td valign="top" width="215">
<p>Ressorts des tribunaux de grande instance de Draguignan et Toulon</p>
</td>
</tr>
<tr>
<td valign="top" width="181"/>
<td valign="top" width="209">
<p>Nice</p>
</td>
<td valign="top" width="215">
<p>Ressorts des tribunaux de grande instance de Grasse et Nice</p>
</td>
</tr>
<tr>
<td valign="top" width="181">
<p>Bastia</p>
</td>
<td valign="top" width="209">
<p>Bastia</p>
</td>
<td valign="top" width="215">
<p>Ressort du tribunal de grande instance de Bastia</p>
</td>
</tr>
<tr>
<td valign="top" width="181"/>
<td valign="top" width="209">
<p>Ajaccio</p>
</td>
<td valign="top" width="215">
<p>Ressort du tribunal de grande instance d'Ajaccio</p>
</td>
</tr>
<tr>
<td valign="top" width="181">
<p>Douai</p>
</td>
<td valign="top" width="209">
<p>Arras</p>
</td>
<td valign="top" width="215">
<p>Ressorts des tribunaux de grande instance d'Arras, Béthune, Saint-Omer et Boulogne-sur-Mer</p>
</td>
</tr>
<tr>
<td valign="top" width="181"/>
<td valign="top" width="209">
<p>Lille</p>
</td>
<td valign="top" width="215">
<p>Ressorts des tribunaux de grande instance de Lille, Dunkerque, Douai, Valenciennes, Cambrai et Avesnes-sur-Helpe</p>
</td>
</tr>
<tr>
<td valign="top" width="181">
<p>Paris</p>
</td>
<td valign="top" width="209">
<p>Paris</p>
</td>
<td valign="top" width="215">
<p>Ressort du tribunal de grande instance de Paris</p>
</td>
</tr>
<tr>
<td valign="top" width="181"/>
<td valign="top" width="209">
<p>Bobigny</p>
</td>
<td valign="top" width="215">
<p>Ressort du tribunal de grande instance de Bobigny</p>
</td>
</tr>
<tr>
<td valign="top" width="181"/>
<td valign="top" width="209">
<p>Créteil</p>
</td>
<td valign="top" width="215">
<p>Ressort du tribunal de grande instance de Créteil</p>
</td>
</tr>
<tr>
<td valign="top" width="181"/>
<td valign="top" width="209">
<p>Evry</p>
</td>
<td valign="top" width="215">
<p>Ressort du tribunal de grande instance d'Evry</p>
</td>
</tr>
<tr>
<td valign="top" width="181"/>
<td valign="top" width="209">
<p>Melun</p>
</td>
<td valign="top" width="215">
<p>Ressorts des tribunaux de grande instance de Melun, Fontainebleau et Meaux</p>
</td>
</tr>
<tr>
<td valign="top" width="181"/>
<td valign="top" width="209">
<p>Auxerre</p>
</td>
<td valign="top" width="215">
<p>Ressorts des tribunaux de grande instance d'Auxerre et Sens</p>
</td>
</tr>
<tr>
<td valign="top" width="181">
<p>Reims</p>
</td>
<td valign="top" width="209">
<p>Reims</p>
</td>
<td valign="top" width="215">
<p>Ressorts des tribunaux de grande instance de Reims, Châlons-en-Champagne et Charleville-Mézières</p>
</td>
</tr>
<tr>
<td valign="top" width="181"/>
<td valign="top" width="209">
<p>Troyes</p>
</td>
<td valign="top" width="215">
<p>Ressort du tribunal de grande instance de Troyes</p>
</td>
</tr>
<tr>
<td valign="top" width="181">
<p>Rennes</p>
</td>
<td valign="top" width="209">
<p>Rennes</p>
</td>
<td valign="top" width="215">
<p>Ressorts des tribunaux de grande instance de Rennes, Saint-Malo, Saint-Brieuc, Quimper et Brest</p>
</td>
</tr>
<tr>
<td valign="top" width="181"/>
<td valign="top" width="209">
<p>Nantes</p>
</td>
<td valign="top" width="215">
<p>Ressorts des tribunaux de grande instance de Nantes, Saint-Nazaire, Lorient et Vannes</p>
</td>
</tr>
<tr>
<td valign="top" width="181">
<p>Riom</p>
</td>
<td valign="top" width="209">
<p>Clermont-Ferrand</p>
</td>
<td valign="top" width="215">
<p>Ressorts des tribunaux de grande instance de Clermont-Ferrand, Aurillac et Le Puy-en-Velay</p>
</td>
</tr>
<tr>
<td valign="top" width="181"/>
<td valign="top" width="209">
<p>Moulins</p>
</td>
<td valign="top" width="215">
<p>Ressorts des tribunaux de grande instance de Moulins, Cusset et Montluçon</p>
</td>
</tr>
<tr>
<td>Saint-Denis </td>
<td>Saint-Denis </td>
<td>Ressort des tribunaux de grande instance de Saint-Denis et de Saint-Pierre<br/>
</td>
</tr>
<tr>
<td/>
<td>Mamoudzou </td>
<td>Ressort du tribunal de grande instance de Mamoudzou </td>
</tr>
</tbody>
</table>
