# Section 1 : Etablissement et composition

- [Paragraphe 1 : Du juge de l'application des peines.](paragraphe-1)
- [Paragraphe 2 : Du tribunal de l'application des peines.](paragraphe-2)
- [Paragraphe 3 : De la chambre de l'application des peines de la cour d'appel.](paragraphe-3)
