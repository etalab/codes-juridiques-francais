# Section 5 : Dispositions applicables aux personnes condamnées pour actes de terrorisme

- [Article D49-75](article-d49-75.md)
- [Article D49-76](article-d49-76.md)
- [Article D49-77](article-d49-77.md)
- [Article D49-78](article-d49-78.md)
- [Article D49-79](article-d49-79.md)
- [Article D49-80](article-d49-80.md)
- [Article D49-81](article-d49-81.md)
