# Section 7 : Du contrôle judiciaire, de l'assignation à résidence avec surveillance électronique et de la détention provisoire

- [Sous-section 2 : De l'assignation à résidence avec surveillance électronique](sous-section-2)
- [Sous-section 3 : Dispositions relatives au contrôle judiciaire et à l'assignation à résidence applicables en cas de violences au sein du couple](sous-section-3)
- [Sous-section 4 : De la détention provisoire](sous-section-4)
