# Paragraphe 1 : Mesures préalables au placement sous assignation à résidence avec surveillance électronique

- [Article D32-4](article-d32-4.md)
- [Article D32-5](article-d32-5.md)
- [Article D32-6](article-d32-6.md)
