# Sous-section 2 : De l'assignation à résidence avec surveillance électronique

- [Paragraphe 1 : Mesures préalables au placement sous assignation à résidence avec surveillance électronique](paragraphe-1)
- [Paragraphe 2 : Accord de la personne mise en examen](paragraphe-2)
- [Paragraphe 3 : Placement sous assignation à résidence 
avec surveillance électronique](paragraphe-3)
- [Paragraphe 4 : Modification ou mainlevée de l'assignation à résidence avec surveillance électronique](paragraphe-4)
- [Paragraphe 5 : Dispositions applicables en cas de non-respect de l'assignation à résidence avec surveillance électronique](paragraphe-5)
- [Paragraphe 6 : Dispositions applicables en cas de non-lieu](paragraphe-6)
- [Paragraphe 7 : Dispositions applicables en cas de renvoi devant la juridiction de jugement](paragraphe-7)
- [Paragraphe 8 : Dispositions applicables aux mineurs](paragraphe-8)
- [Article D32-3](article-d32-3.md)
