# Section 3 : Instructions et renseignements donnés par l'autorité judiciaire

- [Article D32-1](article-d32-1.md)
- [Article D32-2](article-d32-2.md)
