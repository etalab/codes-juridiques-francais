# Article L13 F

Les agents de l'administration peuvent, sans que le contribuable puisse s'y opposer, prendre copie des documents dont ils ont connaissance dans le cadre des procédures de contrôle prévues aux articles L. 12 et L. 13. Les modalités de sécurisation des copies de documents sous forme dématérialisée sont précisées par arrêté du ministre chargé du budget.
