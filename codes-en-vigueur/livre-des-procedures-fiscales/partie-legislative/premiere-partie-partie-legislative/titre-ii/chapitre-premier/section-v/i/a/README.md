# A : En cas de défaut ou de retard dans le dépôt des déclarations

- [Article L66](article-l66.md)
- [Article L67](article-l67.md)
- [Article L68](article-l68.md)
