# Article L59

Lorsque le désaccord persiste sur les rectifications notifiées, l'administration, si le contribuable le demande, soumet le litige à l'avis soit de la commission départementale des impôts directs et des taxes sur le chiffre d'affaires prévue à l'article 1651 du code général des impôts, soit de la Commission nationale des impôts directs et des taxes sur le chiffre d'affaires prévue à l'article 1651 H du même code, soit de la commission départementale de conciliation prévue à l'article 667 du même code.

Les commissions peuvent également être saisies à l'initiative de l'administration.
