# Section II : Dispositions particulières à certains impôts

- [I : Dispositions particulières aux impôts directs](i)
- [I bis : Dispositions particulières à l'impôt sur le revenu ou sur les bénéfices et à la taxe sur la valeur ajoutée](i-bis)
- [I quater : Dispositions particulières au contrôle en matière de taxe sur la valeur ajoutée des redevables placés sous le régime simplifié d'imposition](i-quater)
- [III : Dispositions particulières aux contributions indirectes, au timbre et aux législations assimilées](iii)
