# Article L36 A

Les opérateurs visés au 4° du 1 du I de l'article 302 D, les opérateurs bénéficiant des exonérations prévues à l'article 302 D bis et ceux définis à l'article 302 H ter du code général des impôts sont soumis aux contrôles de l'administration dans les conditions prévues à l'article L. 35.
