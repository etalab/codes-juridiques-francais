# Article L28

Chez les viticulteurs, l'intervention des agents de l'administration est limitée aux chais et ne peut avoir pour objet que de vérifier les déclarations de récolte ou de stocks et de prélever des échantillons de vendanges, de moûts ou de vins.

Les vérifications ne peuvent être empêchées par aucun obstacle du fait des viticulteurs. Ces derniers doivent déclarer aux agents les quantités de boissons existant dans les fûts, vaisseaux, foudres ou autres récipients.
