# Article L80 N

I. ― Pour rechercher et constater les infractions prévues au code général des impôts en matière de tabac, les agents de l'administration des douanes des catégories A et B ont accès aux informations contenues dans les traitements prévus à l'article 569 du même code, au moyen de la marque d'identification unique, sécurisée et indélébile mentionnée à ce même article.

Les frais occasionnés par l'accès à ces traitements sont à la charge des personnes responsables de ces traitements se livrant aux activités mentionnées au premier alinéa dudit article 569.

En cas de constatation d'une infraction, le résultat de la consultation mentionnée au deuxième alinéa est indiqué sur tout document, quel qu'en soit le support, annexé au procès-verbal constatant l'infraction.

II. ― Un décret en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés, fixe les modalités d'accès aux données mentionnées au I par les agents de l'administration des douanes mentionnés au même I.
