# Article L101

L'autorité judiciaire doit communiquer à l'administration des finances toute indication qu'elle peut recueillir, de nature à faire présumer une fraude commise en matière fiscale ou une manoeuvre quelconque ayant eu pour objet ou ayant eu pour résultat de frauder ou de compromettre un impôt, qu'il s'agisse d'une instance civile ou commerciale ou d'une information criminelle ou correctionnelle même terminée par un non-lieu.

L'administration des finances porte à la connaissance du juge d'instruction ou du procureur de la République, spontanément dans un délai de six mois après leur transmission ou à sa demande, l'état d'avancement des recherches de nature fiscale auxquelles elle a procédé à la suite de la communication des indications effectuée en application du premier alinéa.

Le résultat du traitement définitif de ces dossiers par l'administration des finances fait l'objet d'une communication au ministère public.
