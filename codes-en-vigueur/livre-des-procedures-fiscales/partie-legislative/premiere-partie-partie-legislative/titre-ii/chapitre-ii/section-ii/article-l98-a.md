# Article L98 A

Les organismes débiteurs de l'allocation aux adultes handicapés, du revenu minimum d'insertion et du revenu de solidarité active sont tenus de fournir à l'administration fiscale, dans des conditions fixées par arrêté :

1° La liste des personnes bénéficiaires de l'allocation aux adultes handicapés au 1er janvier de l'année d'imposition ;

2° La liste des personnes auxquelles le revenu minimum d'insertion a été versé au 1er janvier ou au cours de l'année d'imposition ainsi que celle des personnes ayant cessé de percevoir ce revenu minimum au cours de l'année précédente ;

3° La liste des personnes auxquelles le revenu de solidarité active a été versé en 2010 et en 2011.
