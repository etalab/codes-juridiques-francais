# Article L83 B

Les                 agents de la concurrence, de la consommation et de la répression des fraudes et de la direction générale des douanes et droits indirects peuvent se communiquer spontanément tous les renseignements et documents recueillis dans le cadre de leurs missions respectives.
