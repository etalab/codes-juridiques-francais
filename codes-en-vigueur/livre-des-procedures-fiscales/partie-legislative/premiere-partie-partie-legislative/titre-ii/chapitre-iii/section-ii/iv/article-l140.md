# Article L140

Conformément aux articles L. 141-5, L. 241-2 et L. 314-4 du code des juridictions financières, les agents des services financiers sont déliés du secret professionnel à l'égard des magistrats, conseillers maîtres en service extraordinaire, rapporteurs de la Cour des comptes et des experts qu'elle désigne, des magistrats de lachambre régionale des comptes ainsi que des rapporteurs auprès de la Cour de discipline budgétaire et financière, à l'occasion des enquêtes effectuées par ces magistrats, conseillers, rapporteurs et experts dans le cadre de leurs attributions.

(Alinéa disjoint).

Conformément aux articles L. 141-9,
L. 241-4 et L. 314-4 du code des juridictions financières, les agents des services financiers dont l'audition est jugée nécessaire pour les besoins du contrôle ont l'obligation de répondre à la convocation de la Cour des comptes ou à celle de la chambre régionale des comptes dans le ressort de laquelle ils exercent leurs fonctions. Ils peuvent être interrogés en qualité de témoins par les rapporteurs auprès de la Cour de discipline budgétaire et financière.
