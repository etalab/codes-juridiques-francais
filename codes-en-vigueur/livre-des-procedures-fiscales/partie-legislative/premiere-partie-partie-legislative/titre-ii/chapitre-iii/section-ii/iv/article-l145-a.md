# Article L145 A

Conformément aux dispositions du deuxième alinéa du I de l'article L. 611-2 du code de commerce, le président du tribunal de commerce peut recevoir de l'administration communication des renseignements de nature à lui donner une exacte information sur la situation économique et financière du débiteur.
