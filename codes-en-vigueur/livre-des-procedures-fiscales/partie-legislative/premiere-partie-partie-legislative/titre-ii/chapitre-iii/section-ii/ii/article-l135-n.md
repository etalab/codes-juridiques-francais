# Article L135 N

Les agents de la Commission de régulation de l'énergie, habilités et assermentés en application de l'article L. 135-13 du code de l'énergie (1), peuvent recevoir de l'administration fiscale les renseignements nécessaires à l'établissement du plafonnement de la contribution au service public de l'électricité prévu à l'article L. 121-21 du même code (2).
