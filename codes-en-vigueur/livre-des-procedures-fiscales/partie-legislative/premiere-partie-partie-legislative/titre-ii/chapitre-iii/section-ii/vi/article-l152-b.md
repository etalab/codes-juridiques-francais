# Article L152 B

Conformément à la première phrase de l'article L. 651-5-1 du code de la sécurité sociale, l'organisme chargé du recouvrement de la contribution sociale de solidarité prévue à l'article L. 651-1 du même code peut obtenir des renseignements auprès des administrations fiscales.
