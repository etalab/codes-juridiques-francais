# Titre II : Le contrôle de l'impôt

- [Chapitre I ter : Le droit de contrôle des entrepôts](chapitre-i-ter)
- [Chapitre I quater : Procédure d'imposition contradictoire en matière de contributions indirectes.](chapitre-i-quater)
- [Chapitre Ier quinquies : Consultation des traitements automatisés de données concernant le marquage des conditionnements des produits du tabac](chapitre-ier-quinquies)
- [Chapitre II : Le droit de communication](chapitre-ii)
- [Chapitre II bis : Obligation et délais de conservation des documents](chapitre-ii-bis)
- [Chapitre III : Le secret professionnel en matière fiscale](chapitre-iii)
- [Chapitre IV : Les délais de prescription](chapitre-iv)
- [Chapitre premier : Le droit de contrôle de l'administration](chapitre-premier)
