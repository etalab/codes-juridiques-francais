# B : Dispositions particulières à certains impôts

- [Article L171](article-l171.md)
- [Article L171-0 A](article-l171-0-a.md)
- [Article L171 A](article-l171-a.md)
- [Article L172](article-l172.md)
- [Article L172 C](article-l172-c.md)
- [Article L172 A](article-l172-a.md)
- [Article L172 E](article-l172-e.md)
- [Article L172 F](article-l172-f.md)
- [Article L172 G](article-l172-g.md)
- [Article L172 H](article-l172-h.md)
