# Article L234

Les infractions relatives à l'application des taxes sur le chiffre d'affaires et taxes assimilées à l'importation sont poursuivies et jugées selon la procédure et par les tribunaux compétents en matière douanière.

Il en est de même :

1° Des infractions relatives à l'assiette, à la liquidation et au recouvrement de la taxe sur la valeur ajoutée perçue par l'administration des douanes et droits indirects sur les produits pétroliers, à l'exception du contentieux relatif aux déductions ;

2° (Abrogé).
