# Article L274

Les comptables publics des administrations fiscales qui n'ont fait aucune poursuite contre un redevable pendant quatre années consécutives à compter du jour de la mise en recouvrement du rôle ou de l'envoi de l'avis de mise en recouvrement sont déchus de tous droits et de toute action contre ce redevable.

Le délai de prescription de l'action en recouvrement prévu au premier alinéa est augmenté de deux années pour les redevables établis dans un Etat non membre de l'Union européenne avec lequel la France ne dispose d'aucun instrument juridique relatif à l'assistance mutuelle en matière de recouvrement ayant une portée similaire à celle prévue par la directive 2010/24/UE du Conseil du 16 mars 2010 concernant l'assistance mutuelle en matière de recouvrement des créances relatives aux taxes, impôts, droits et autres mesures.
