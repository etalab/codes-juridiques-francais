# Article L255 A

Les parts communale, départementale et régionale de la taxe d'aménagement prévues par les articles L. 331-1 à L. 331-4 du code de l'urbanisme et le versement pour sous-densité prévu par les articles L. 331-36 et L. 331-38 du même code sont assis, liquidés et recouvrés en vertu d'un titre de recettes individuel ou collectif délivré par le responsable chargé de l'urbanisme dans le département. Ce responsable peut déléguer sa signature aux agents placés sous son autorité.
