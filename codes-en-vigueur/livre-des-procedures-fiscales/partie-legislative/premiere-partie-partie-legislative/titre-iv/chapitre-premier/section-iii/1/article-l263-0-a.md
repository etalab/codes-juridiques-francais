# Article L263-0 A

Peuvent faire l'objet d'un avis à tiers détenteur notifié par le comptable chargé du recouvrement, dans les conditions prévues aux articles L. 262 et L. 263, les sommes versées par un redevable souscripteur ou adhérent d'un contrat d'assurance rachetable, y compris si la possibilité de rachat fait l'objet de limitations, dans la limite de la valeur de rachat des droits à la date de la notification de l'avis à tiers détenteur.
