# Article L262

Les dépositaires, détenteurs ou débiteurs de sommes appartenant ou devant revenir aux redevables d'impôts, de pénalités et de frais accessoires dont le recouvrement est garanti par le privilège du Trésor sont tenus, sur la demande qui leur en est faite sous forme d'avis à tiers détenteur notifié par le comptable chargé du recouvrement, de verser, aux lieu et place des redevables, les fonds qu'ils détiennent ou qu'ils doivent, à concurrence des impositions dues par ces redevables (1).

Les dispositions du présent article s'appliquent également aux gérants, administrateurs, directeurs ou liquidateurs des sociétés pour les impositions dues par celles-ci.

(1) Cette disposition s'applique aux majorations, pénalités et frais accessoires relatifs aux infractions constatées à partir du 1er janvier 1982. Elle est applicable en ce qui concerne les droits d'enregistrement, la taxe de publicité foncière, les droits de timbre et les contributions indirectes, aux impositions mises en recouvrement à partir du 1er janvier 1982 (loi n° 81-1179 du 31 décembre 1981, art. 8-III).
