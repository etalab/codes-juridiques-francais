# Titre IV : Le recouvrement de l'impôt

- [Chapitre II : Le sursis de paiement](chapitre-ii)
- [Chapitre III : Le contentieux du recouvrement](chapitre-iii)
- [Chapitre IV : Assistance au recouvrement au sein de l'Union européenne](chapitre-iv)
- [Chapitre premier : Les procédures de recouvrement](chapitre-premier)
