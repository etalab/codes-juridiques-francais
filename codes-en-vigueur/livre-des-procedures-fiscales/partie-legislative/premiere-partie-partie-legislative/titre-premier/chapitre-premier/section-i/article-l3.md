# Article L3

Dans les départements où des productions agricoles spécialisées ne font pas l'objet d'une évaluation spéciale, les exploitants agricoles qui se livrent à ces productions peuvent être imposés sur la base des forfaits établis pour les mêmes productions dans un département comportant le même type de production.
