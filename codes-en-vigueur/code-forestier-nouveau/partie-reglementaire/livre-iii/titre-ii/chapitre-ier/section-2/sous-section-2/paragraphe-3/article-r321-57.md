# Article R321-57

A la date fixée par l'arrêté ministériel prévu à l'article R. 321-52, il est procédé publiquement au dépouillement des bulletins de vote par une commission de recensement des votes instituée par arrêté du préfet, et comprenant :

1° Le préfet ou son représentant, président ;

2° Le directeur départemental des territoires ou son représentant ;

3° Deux membres désignés par le préfet parmi les propriétaires forestiers ou représentants des personnes morales et indivisions, membres du collège départemental, autres que les candidats.

La commission désigne des scrutateurs parmi les électeurs présents.
