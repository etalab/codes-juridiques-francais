# Chapitre Ier : Régime d'autorisation préalable

- [Section 1 : Demande](section-1)
- [Section 2 : Instruction et décision](section-2)
- [Section 3 : Suites données aux infractions constatées  en matière de défrichement](section-3)
