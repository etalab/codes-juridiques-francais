# Sous-section 2 : Modalités de contrôle

- [Article D332-9](article-d332-9.md)
- [Article D332-10](article-d332-10.md)
- [Article D332-11](article-d332-11.md)
