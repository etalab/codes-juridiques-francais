# Section 8 : Extraction et dépôt de matériaux pour les travaux publics

- [Article R213-70](article-r213-70.md)
- [Article R213-71](article-r213-71.md)
- [Article R213-72](article-r213-72.md)
- [Article R213-73](article-r213-73.md)
- [Article R213-74](article-r213-74.md)
- [Article R213-75](article-r213-75.md)
