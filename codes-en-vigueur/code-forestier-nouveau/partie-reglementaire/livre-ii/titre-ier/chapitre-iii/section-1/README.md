# Section 1 : Acquisition, affectation et aliénation

- [Article R213-1](article-r213-1.md)
- [Article R213-2](article-r213-2.md)
