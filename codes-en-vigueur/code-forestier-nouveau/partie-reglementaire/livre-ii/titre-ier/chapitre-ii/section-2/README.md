# Section 2 : Règlement type de gestion

- [Article D212-9](article-d212-9.md)
- [Article D212-10](article-d212-10.md)
- [Article R212-7](article-r212-7.md)
- [Article R212-8](article-r212-8.md)
