# Section 5 : Pâturage, produits accessoires et droits de jouissance collectifs

- [Article R214-28](article-r214-28.md)
- [Article R214-29](article-r214-29.md)
