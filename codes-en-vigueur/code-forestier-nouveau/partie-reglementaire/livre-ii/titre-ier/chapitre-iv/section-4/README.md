# Section 4 : Ventes des coupes et produits des coupes

- [Article D214-22](article-d214-22.md)
- [Article D214-23](article-d214-23.md)
- [Article R214-24](article-r214-24.md)
- [Article R214-25](article-r214-25.md)
- [Article R214-26](article-r214-26.md)
- [Article R214-27](article-r214-27.md)
