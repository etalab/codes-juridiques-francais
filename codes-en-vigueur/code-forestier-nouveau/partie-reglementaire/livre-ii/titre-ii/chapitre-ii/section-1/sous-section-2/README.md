# Sous-section 2 : Compétences et règles de délibération

- [Article D222-5](article-d222-5.md)
- [Article D222-6](article-d222-6.md)
- [Article D222-7](article-d222-7.md)
- [Article D222-8](article-d222-8.md)
- [Article D222-9](article-d222-9.md)
- [Article D222-10](article-d222-10.md)
