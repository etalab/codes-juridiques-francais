# Section 3 : Droits d'usage et d'affouage

- [Article R261-9](article-r261-9.md)
- [Article R261-10](article-r261-10.md)
- [Article R261-11](article-r261-11.md)
- [Article R261-12](article-r261-12.md)
- [Article R261-13](article-r261-13.md)
- [Article R261-14](article-r261-14.md)
- [Article R261-15](article-r261-15.md)
- [Article R261-16](article-r261-16.md)
- [Article R261-17](article-r261-17.md)
