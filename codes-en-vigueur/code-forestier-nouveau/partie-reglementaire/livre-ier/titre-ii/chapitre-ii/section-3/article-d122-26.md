# Article D122-26

Le délai mentionné au troisième alinéa de l'article L. 122-13 est de cinq ans à compter de la publication de l'arrêté approuvant ce plan.
