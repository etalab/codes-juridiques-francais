# Section 2 : Coordination des procédures administratives

- [Sous-section 1 : Elaboration et approbation des annexes  aux directives ou aux schémas régionaux](sous-section-1)
- [Sous-section 2 : Approbation ou agrément d'un document de gestion  au titre de la coordination des procédures](sous-section-2)
- [Sous-section 3 : Reconstitution après coupe](sous-section-3)
