# Sous-section 3 : Dispositions relatives aux travaux de recherche et aux captages d'eau  destinée à la consommation humaine dans les forêts de protection

- [Paragraphe 1 : Dispositions communes](paragraphe-1)
- [Paragraphe 2 : Travaux nécessaires à la recherche  de la ressource en eau dans les forêts de protection](paragraphe-2)
- [Paragraphe 3 : Travaux et ouvrages nécessaires au captage d'eau  dans les forêts de protection](paragraphe-3)
