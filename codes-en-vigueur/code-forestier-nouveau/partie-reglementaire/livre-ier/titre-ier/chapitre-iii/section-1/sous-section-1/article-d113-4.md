# Article D113-4

Le Conseil supérieur de la forêt, des produits forestiers et de la transformation du bois peut être consulté par le ministre chargé des forêts et formuler des propositions sur toute question relative au secteur de la forêt et du bois. A sa demande ou à celle d'un autre ministre, il examine l'incidence des autres politiques nationales ou européennes d'intérêt général sur la forêt, ses produits et ses services, et formule un avis transmis au ministre demandeur, au Premier ministre, au président du Sénat et à celui de l'Assemblée nationale.

Il est tenu informé de l'évolution des dotations budgétaires provenant du budget de l'Etat ou de l'Union européenne, affectées à des actions menées dans le secteur de la forêt et du bois, et de leur emploi. Il formule des recommandations sur la politique de contractualisation entre l'Etat et les régions, et est informé du contenu et de la mise en œuvre des contrats Etat-régions signés pour autant qu'ils comportent une partie relative à la forêt et aux industries du bois.

Le règlement intérieur du conseil énonce celles de ses attributions qui peuvent être déléguées au comité de politique forestière.
