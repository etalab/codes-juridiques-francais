# Article D113-11

La commission régionale de la forêt et des produits forestiers concourt à l'élaboration et à la mise en œuvre dans la région des orientations de la politique forestière dans le respect des objectifs définis à l'article L. 121-4.

Elle est notamment chargée :

1° D'élaborer les orientations régionales forestières, qu'elle soumet pour avis, lorsqu'il y a lieu, à l'établissement public du parc national ;

2° D'émettre un avis sur les orientations du projet de contrat entre l'Etat et la région dans le secteur de la forêt et du bois ;

3° D'émettre un avis sur les projets de directives régionales d'aménagement des forêts et de schémas régionaux d'aménagement des forêts relevant du régime forestier, ainsi que sur les projets de schémas régionaux de gestion sylvicole des bois et forêts des particuliers ;

4° De formuler toute observation relative à l'application, dans la région, de la politique forestière ou de toute autre politique régionale, nationale ou communautaire ayant une incidence sur la forêt, ses produits et ses services ;

5° De faire toute proposition visant à :

a) Améliorer l'efficacité des programmes annuels d'investissement bénéficiant d'aides publiques et leur cohérence avec les orientations régionales forestières ;

b) Favoriser le développement de l'interprofessionnalité.

La liste mentionnée à l'article D. 122-13 est portée annuellement à sa connaissance.

Elle est informée des dotations, tant nationales que communautaires, affectées à des actions conduites dans les secteurs de la forêt et de la transformation du bois et de l'application de contrats d'engagement pluriannuels passés entre l'Etat et la région en tant qu'ils concernent la forêt et le bois.
