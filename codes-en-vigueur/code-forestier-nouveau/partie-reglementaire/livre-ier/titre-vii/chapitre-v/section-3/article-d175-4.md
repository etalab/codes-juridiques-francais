# Article D175-4

La commission de la forêt et des produits forestiers du Département de Mayotte est présidée par le préfet et comprend :

1° Le directeur de l'alimentation, de l'agriculture et de la forêt ;

2° Le directeur de l'environnement, de l'aménagement et du logement ;

3° Le directeur des entreprises, de la concurrence, de la consommation, du travail et de l'emploi ;

4° Des représentants du conseil général et de l'association des maires de Mayotte ;

5° Des représentants de la propriété forestière des particuliers ;

6° Des représentants de la propriété forestière des bois et forêts relevant du 2° du I de l'article L. 211-1 ;

7° Des représentants de l'Office national des forêts ;

8° Des représentants de l'industrie du bois ;

9° Des représentants des prestataires de services dans le secteur de la forêt et du bois ;

10° Des représentants des structures interprofessionnelles dans le secteur de la forêt et du bois ;

11° Des représentants d'associations d'usagers de la forêt, de protection de l'environnement et de gestionnaires d'espaces naturels ;

12° Des représentants de la chambre d'agriculture, de la pêche et de l'aquaculture, de la chambre de commerce et d'industrie et de la chambre des métiers ;

13° Des personnalités qualifiées.

Le nombre de membres de la commission nommés au titre du 5°, du 6° et du 7° est fonction des surfaces respectives de chacun des régimes de propriété forestière dans la région.

Le préfet désigne les membres des catégories mentionnées aux 5° à 11° et constate par arrêté la composition de la commission.
