# Article D172-3

En Guyane, la commission régionale de la forêt et des produits forestiers comprend, outre les membres prévus à l'article D. 113-12, des représentants des autorités coutumières des communautés d'habitants mentionnées à l'article L. 172-3, désignés par le préfet, ainsi qu'un représentant de l'établissement public gérant le Parc amazonien de Guyane.
