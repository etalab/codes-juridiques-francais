# Article Annexe II

TABLEAU ÉTABLI POUR L'APPLICATION DE L'ARTICLE R. 321-5

Répartition des représentants des centres régionaux de la propriété forestière

au conseil d'administration du Centre national de la propriété forestière

<table>
<tbody>
<tr>
<td width="221">
<p align="center">CENTRES RÉGIONAUX </p>
</td>
<td width="221">
<p align="center">RÉGIONS REPRÉSENTÉES </p>
</td>
<td width="220">
<p align="center">NOMBRE DE SIÈGES RÉSERVÉS <br/>dans la commission aux représentants de chaque centre régional </p>
</td>
</tr>
<tr>
<td width="221">
<p>Aquitaine </p>
</td>
<td width="221">
<p>Aquitaine </p>
</td>
<td width="220">
<p align="center">3 </p>
</td>
</tr>
<tr>
<td width="221">
<p>Auvergne </p>
</td>
<td width="221">
<p>Auvergne </p>
</td>
<td width="220">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="221">
<p>Bourgogne </p>
</td>
<td width="221">
<p>Bourgogne </p>
</td>
<td width="220">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="221">
<p>Bretagne </p>
</td>
<td width="221">
<p>Bretagne </p>
</td>
<td width="220">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="221">
<p>Champagne-Ardenne </p>
</td>
<td width="221">
<p>Champagne-Ardenne </p>
</td>
<td width="220">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="221">
<p>Corse </p>
</td>
<td width="221">
<p>Corse </p>
</td>
<td width="220">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="221">
<p>Franche-Comté </p>
</td>
<td width="221">
<p>Franche-Comté </p>
</td>
<td width="220">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="221">
<p>Ile-de-France-Centre </p>
</td>
<td width="221">
<p>Ile-de-France-Centre </p>
</td>
<td width="220">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="221">
<p>Languedoc-Roussillon </p>
</td>
<td width="221">
<p>Languedoc-Roussillon </p>
</td>
<td width="220">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="221">
<p>Limousin </p>
</td>
<td width="221">
<p>Limousin </p>
</td>
<td width="220">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="221">
<p>Lorraine-Alsace </p>
</td>
<td width="221">
<p>Lorraine-Alsace </p>
</td>
<td width="220">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="221">
<p>Midi-Pyrénées </p>
</td>
<td width="221">
<p>Midi-Pyrénées </p>
</td>
<td width="220">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="221">
<p>Nord-Pas-de-Calais-Picardie </p>
</td>
<td width="221">
<p>Nord-Pas-de-Calais-Picardie </p>
</td>
<td width="220">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="221">
<p>Normandie </p>
</td>
<td width="221">
<p>Haute-Normandie, Basse-Normandie </p>
</td>
<td width="220">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="221">
<p>Pays de la Loire </p>
</td>
<td width="221">
<p>Pays de la Loire </p>
</td>
<td width="220">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="221">
<p>Poitou-Charentes </p>
</td>
<td width="221">
<p>Poitou-Charentes </p>
</td>
<td width="220">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="221">
<p>Provence-Alpes-Côte d'Azur </p>
</td>
<td width="221">
<p>Provence-Alpes-Côte d'Azur </p>
</td>
<td width="220">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="221">
<p>Rhône-Alpes </p>
</td>
<td width="221">
<p>Rhône-Alpes </p>
</td>
<td width="220">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="221">
<p>Total </p>
</td>
<td valign="top" width="221">
<br/>
</td>
<td width="220">
<p align="center">25</p>
</td>
</tr>
</tbody>
</table>
