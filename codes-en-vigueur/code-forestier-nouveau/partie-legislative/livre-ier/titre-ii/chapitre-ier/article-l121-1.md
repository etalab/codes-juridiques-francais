# Article L121-1

La politique forestière relève de la compétence de l'Etat. Ses orientations, ses financements et ses investissements s'inscrivent dans le long terme.

L'Etat veille :

1° A l'adaptation des essences forestières au milieu ;

2° A l'optimisation du stockage de carbone dans les bois et forêts, le bois et les produits fabriqués à partir de bois ;

3° Au maintien de l'équilibre et de la diversité biologiques et à l'adaptation des forêts au changement climatique ;

4° A la régénération des peuplements forestiers dans des conditions satisfaisantes d'équilibre sylvo-cynégétique, au sens du dernier alinéa de l'article L. 425-4 du code de l'environnement ;

5° A la satisfaction des besoins des industries du bois, notamment par l'équilibre des classes d'âge des peuplements forestiers au niveau national ;

6° Au renforcement de la compétitivité et de la durabilité des filières d'utilisation du bois, par la valorisation optimale des ressources forestières nationales et par l'accompagnement en formation des nouveaux métiers du bois ;

7° Au développement des territoires.

La politique forestière a pour objet d'assurer la gestion durable des bois et forêts. Elle prend en compte leurs fonctions économique, écologique et sociale. Elle concourt au développement de la qualification des emplois en vue de leur pérennisation. Elle vise à favoriser le regroupement technique et économique des propriétaires et l'organisation interprofessionnelle de la filière forestière pour en renforcer la compétitivité. Elle tend à satisfaire les demandes sociales relatives à la forêt.
