# Section 1 : Dispositions générales

- [Article L134-1](article-l134-1.md)
- [Article L134-2](article-l134-2.md)
- [Article L134-3](article-l134-3.md)
- [Article L134-4](article-l134-4.md)
