# Sous-section 3 : Dispositions applicables aux élus fonctionnaires de l'Etat et des collectivités territoriales, ou agents contractuels de l'Etat, des collectivités territoriales et de leurs établissements publics administratifs

- [Article R121-30](article-r121-30.md)
- [Article R121-31](article-r121-31.md)
- [Article R121-32](article-r121-32.md)
- [Article R121-33](article-r121-33.md)
