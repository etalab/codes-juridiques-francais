# Section 1 : Formation

- [Article R121-2](article-r121-2.md)
- [Article R121-3](article-r121-3.md)
- [Article R121-4](article-r121-4.md)
- [Article R121-5](article-r121-5.md)
