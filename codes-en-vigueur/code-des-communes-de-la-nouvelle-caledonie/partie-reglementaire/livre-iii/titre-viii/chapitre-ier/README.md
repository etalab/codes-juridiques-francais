# Chapitre Ier : Dispositions générales.

- [Article D381-1](article-d381-1.md)
- [Article D381-2](article-d381-2.md)
- [Article D381-3](article-d381-3.md)
- [Article D381-4](article-d381-4.md)
