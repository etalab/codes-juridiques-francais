# Paragraphe 1 : Dispositions générales

- [Article R323-56](article-r323-56.md)
- [Article R323-57](article-r323-57.md)
- [Article R323-58](article-r323-58.md)
