# Sous-section 1 : Dispositions générales

- [Article R234-1](article-r234-1.md)
- [Article R234-2](article-r234-2.md)
