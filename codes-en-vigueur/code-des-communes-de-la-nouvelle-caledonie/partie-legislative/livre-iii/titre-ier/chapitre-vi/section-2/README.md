# Section 2 : Exercice, par un contribuable, des actions appartenant à la commune

- [Article L316-5](article-l316-5.md)
- [Article L316-6](article-l316-6.md)
- [Article L316-7](article-l316-7.md)
- [Article L316-8](article-l316-8.md)
