# Article L212-1

Le budget de la commune est proposé par le maire et voté par le conseil municipal. Dans les communes de 3 500 habitants et plus, un débat a lieu au conseil municipal sur les orientations générales du budget de l'exercice ainsi que sur les engagements pluriannuels envisagés, dans un délai de deux mois précédant l'examen de celui-ci, dans les conditions fixées par le règlement intérieur prévu à l'article L. 121-10-1.
