# Section 5 : Autres redevances pour services rendus

- [Sous-section 1 : Redevance d'enlèvement des ordures ménagères](sous-section-1)
- [Sous-section 2 : Redevance d'assainissement](sous-section-2)
