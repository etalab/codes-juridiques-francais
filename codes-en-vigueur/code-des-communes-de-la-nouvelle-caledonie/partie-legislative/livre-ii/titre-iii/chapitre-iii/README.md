# Chapitre III : Taxes, redevances ou versements autres que ceux prévus par le code territorial des impôts

- [Section 1 : Taxe sur l'électricité](section-1)
- [Section 2 : Taxe sur la publicité](section-2)
- [Section 3 : Taxes particulières aux stations](section-3)
- [Section 4 : Taxe de trottoirs et de pavage](section-4)
- [Section 5 : Autres redevances pour services rendus](section-5)
