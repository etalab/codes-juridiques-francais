# Chapitre III : Syndicat de communes

- [Section 1 : Création du syndicat](section-1)
- [Section 2 : Administration et fonctionnement du syndicat](section-2)
- [Section 3 : Modification aux conditions initiales de composition et de fonctionnement du syndicat](section-3)
- [Section 4 : Durée du syndicat](section-4)
