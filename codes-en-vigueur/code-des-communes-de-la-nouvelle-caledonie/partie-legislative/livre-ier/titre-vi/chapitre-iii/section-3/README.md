# Section 3 : Modification aux conditions initiales de composition et de fonctionnement du syndicat

- [Article L163-15](article-l163-15.md)
- [Article L163-16](article-l163-16.md)
- [Article L163-17](article-l163-17.md)
