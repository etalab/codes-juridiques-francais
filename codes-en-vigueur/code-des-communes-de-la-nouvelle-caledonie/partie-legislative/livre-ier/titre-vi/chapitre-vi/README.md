# Chapitre VI : Syndicats mixtes

- [Section 1 : Syndicats mixtes auxquels ne participent pas la Nouvelle-Calédonie ou les provinces](section-1)
- [Section 2 : Syndicats mixtes auxquels participent la Nouvelle-Calédonie ou les provinces](section-2)
