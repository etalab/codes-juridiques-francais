# Chapitre VI : Participation des habitants et des usagers à la vie des services publics.

- [Article L126-1](article-l126-1.md)
- [Article L126-2](article-l126-2.md)
