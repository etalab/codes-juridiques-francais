# Section 4 : Garanties accordées à  l'issue du mandat

- [Article L122-29](article-l122-29.md)
- [Article L122-30](article-l122-30.md)
