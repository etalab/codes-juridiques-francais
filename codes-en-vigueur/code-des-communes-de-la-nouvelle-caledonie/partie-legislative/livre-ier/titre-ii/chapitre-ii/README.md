# Chapitre II : Maires et adjoints

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Désignation et statut des maires et adjoints](section-2)
- [Section 3 : Attributions des maires et adjoints](section-3)
- [Section 4 : Garanties accordées à  l'issue du mandat](section-4)
