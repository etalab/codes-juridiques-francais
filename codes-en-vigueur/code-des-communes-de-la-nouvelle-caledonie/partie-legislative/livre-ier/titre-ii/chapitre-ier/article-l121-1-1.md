# Article L121-1-1

Les membres des conseils municipaux exercent leur mandat dans le respect des principes déontologiques consacrés par la charte de l'élu local prévue à l'article L. 1111-1-1 du code général des collectivités territoriales.
