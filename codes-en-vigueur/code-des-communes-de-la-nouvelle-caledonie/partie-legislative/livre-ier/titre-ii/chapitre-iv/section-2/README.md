# Section 2 : Dispositions applicables aux maires et adjoints

- [Article L124-5](article-l124-5.md)
- [Article L124-6](article-l124-6.md)
- [Article L124-7](article-l124-7.md)
- [Article L124-8](article-l124-8.md)
