# Sous-section 2 : Dispositions applicables aux fusions simples

- [Article L112-9](article-l112-9.md)
- [Article L112-10](article-l112-10.md)
