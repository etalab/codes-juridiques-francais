# Section 3 : Modifications aux limites territoriales des communes

- [Article L112-13](article-l112-13.md)
- [Article L112-14](article-l112-14.md)
