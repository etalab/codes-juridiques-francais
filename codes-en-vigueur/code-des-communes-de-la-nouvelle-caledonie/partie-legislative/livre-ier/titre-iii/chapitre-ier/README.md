# Chapitre Ier : Dispositions générales

- [Section 1 : Pouvoirs généraux du maire en matière de police](section-1)
- [Section 2 : Pouvoirs de police du maire portant sur des objets particuliers](section-2)
- [Section 3 : Pouvoirs du haut-commissaire en matière de police municipale](section-3)
