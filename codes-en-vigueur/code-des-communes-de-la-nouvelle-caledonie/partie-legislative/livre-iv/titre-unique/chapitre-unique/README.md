# Chapitre unique : Recrutement, formation et promotion sociale

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Dispositions applicables aux gardes champêtres et aux agents de la police municipale](section-2)
