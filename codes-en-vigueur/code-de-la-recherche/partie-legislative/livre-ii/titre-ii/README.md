# TITRE II : LA RECHERCHE EN MÉDECINE ET BIOLOGIE HUMAINE

- [Chapitre Ier : La génétique.](chapitre-ier)
- [Chapitre II : Utilisation à des fins scientifiques d'éléments et produits du corps humain et de leurs dérivés.](chapitre-ii)
- [Chapitre III : Les recherches biomédicales.](chapitre-iii)
- [Chapitre IV : La recherche sur l'embryon et les cellules souches embryonnaires.](chapitre-iv)
- [Chapitre V : Les traitements de données à caractère personnel.](chapitre-v)
