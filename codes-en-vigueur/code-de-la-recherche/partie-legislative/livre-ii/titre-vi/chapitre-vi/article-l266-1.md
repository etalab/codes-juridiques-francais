# Article L266-1

Sous réserve des dispositions des articles 26 et 37 de la loi organique n° 2004-192 du 27 février 2004 portant statut d'autonomie de la Polynésie française, les dispositions de l'article L. 251-1 sont applicables en Polynésie française, dans sa rédaction résultant de l'ordonnance n° 2015-24 du 14 janvier 2015 portant extension et adaptation dans les îles Wallis et Futuna, en Polynésie française et en Nouvelle-Calédonie de la loi n° 2013-660 du 22 juillet 2013 relative à l'enseignement supérieur et à la recherche.
