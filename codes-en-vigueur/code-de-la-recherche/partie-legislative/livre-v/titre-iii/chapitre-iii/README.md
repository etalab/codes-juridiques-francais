# Chapitre III : La valorisation des résultats de la recherche par les établissements et organismes de recherche

- [Article L533-1](article-l533-1.md)
- [Article L533-2](article-l533-2.md)
- [Article L533-3](article-l533-3.md)
