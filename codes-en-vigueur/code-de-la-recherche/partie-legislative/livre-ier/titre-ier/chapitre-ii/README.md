# Chapitre II : Objectifs et moyens institutionnels de la recherche publique.

- [Article L112-1](article-l112-1.md)
- [Article L112-2](article-l112-2.md)
- [Article L112-3](article-l112-3.md)
- [Article L112-4](article-l112-4.md)
- [Article L112-5](article-l112-5.md)
