# Article L112-2

La recherche publique est organisée dans les services publics, notamment les établissements publics d'enseignement supérieur, les établissements publics de recherche et les établissements de santé, et dans les entreprises publiques.
