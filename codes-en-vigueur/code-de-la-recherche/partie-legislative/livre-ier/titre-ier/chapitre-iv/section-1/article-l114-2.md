# Article L114-2

Les organismes publics de recherche font l'objet de procédures d'évaluation périodique.
