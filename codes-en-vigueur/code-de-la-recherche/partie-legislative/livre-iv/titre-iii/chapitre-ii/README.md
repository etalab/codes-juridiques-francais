# Chapitre II : Les chercheurs et enseignants associés.

- [Article L432-1](article-l432-1.md)
- [Article L432-2](article-l432-2.md)
