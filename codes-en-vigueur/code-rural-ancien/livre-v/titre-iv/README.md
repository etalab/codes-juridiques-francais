# Titre IV : Dispositions diverses

- [Chapitre Ier : Régime des prêts hypothécaires.](chapitre-ier)
- [Chapitre II : Prêts aux agriculteurs et artisans ruraux éprouvés par la guerre.](chapitre-ii)
- [Chapitre III : Prêts aux anciens prisonniers et déportés, combattants volontaires de la résistance, réfractaires, anciens combattants d'Indochine et de Corée](chapitre-iii)
- [Chapitre IV : Prêts pour la mise en valeur des terres incultes.](chapitre-iv)
- [Chapitre V : Domaine - Retraite.](chapitre-v)
- [Chapitre VI : Dispositions d'application.](chapitre-vi)
