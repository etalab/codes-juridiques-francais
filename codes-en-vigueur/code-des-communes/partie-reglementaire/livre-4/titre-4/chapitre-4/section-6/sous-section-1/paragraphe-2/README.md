# PARAGRAPHE 2 : Congés annuels.

- [Article R*444-102](article-r-444-102.md)
- [Article R*444-103](article-r-444-103.md)
- [Article R*444-104](article-r-444-104.md)
- [Article R*444-105](article-r-444-105.md)
- [Article R*444-106](article-r-444-106.md)
- [Article R*444-107](article-r-444-107.md)
- [Article R*444-108](article-r-444-108.md)
