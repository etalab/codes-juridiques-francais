# SECTION 6 : Positions.

- [SOUS-SECTION 1 : Activité, congés.](sous-section-1)
- [SOUS-SECTION 8 : Mutations.](sous-section-8)
- [Article R*444-88](article-r-444-88.md)
