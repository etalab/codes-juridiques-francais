# CHAPITRE 4 : Dispositions applicables à la ville de Paris

- [SECTION 1 : Dispositions générales et organiques](section-1)
- [SECTION 2 : Recrutement.](section-2)
- [SECTION 3 : Rémunération.](section-3)
- [SECTION 4 : Notation et avancement](section-4)
- [SECTION 5 : Discipline.](section-5)
- [SECTION 6 : Positions.](section-6)
- [SECTION 7 : Cessation de fonctions.](section-7)
