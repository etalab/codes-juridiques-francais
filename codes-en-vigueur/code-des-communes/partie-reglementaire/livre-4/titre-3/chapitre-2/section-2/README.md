# SECTION 2 : Transfert définitif des personnels.

- [Article R*432-4](article-r-432-4.md)
- [Article R*432-5](article-r-432-5.md)
- [Article R*432-6](article-r-432-6.md)
- [Article R*432-7](article-r-432-7.md)
- [Article R*432-8](article-r-432-8.md)
- [Article R*432-9](article-r-432-9.md)
