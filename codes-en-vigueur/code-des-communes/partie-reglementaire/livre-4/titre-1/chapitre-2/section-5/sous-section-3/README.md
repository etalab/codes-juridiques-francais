# SOUS-SECTION 3 : Dispositions applicables aux personnels des écoles d'art et musées.

- [Article R*412-123](article-r-412-123.md)
- [Article R*412-124](article-r-412-124.md)
- [Article R412-125](article-r412-125.md)
- [Article R412-126](article-r412-126.md)
