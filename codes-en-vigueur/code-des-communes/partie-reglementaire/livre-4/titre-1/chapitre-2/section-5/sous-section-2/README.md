# SOUS-SECTION 2 : Dispositions applicables aux personnels affectés au traitement de l'information.

- [Article R412-120](article-r412-120.md)
- [Article R412-121](article-r412-121.md)
- [Article R412-122](article-r412-122.md)
