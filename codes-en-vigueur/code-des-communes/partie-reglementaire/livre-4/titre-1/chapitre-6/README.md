# CHAPITRE 6 : Cessation de fonctions.

- [SECTION 1 : L'admission à la retraite.](section-1)
- [SECTION 4 : Nomination dans une autre commune.](section-4)
- [Article R*416-1](article-r-416-1.md)
