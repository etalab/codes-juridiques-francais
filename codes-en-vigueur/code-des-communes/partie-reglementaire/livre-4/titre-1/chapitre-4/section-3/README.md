# SECTION 3 : Discipline

- [SOUS-SECTION 1 : Le conseil de discipline.](sous-section-1)
- [SOUS-SECTION 2 : Les sanctions disciplinaires.](sous-section-2)
- [SOUS-SECTION 4 : Dispositions applicables aux personnels divers.](sous-section-4)
