# SECTION 6 : La médaille d'honneur régionale, départementale et communale.

- [Article R411-41](article-r411-41.md)
- [Article R411-42](article-r411-42.md)
- [Article R411-43](article-r411-43.md)
- [Article R411-44](article-r411-44.md)
- [Article R411-45](article-r411-45.md)
- [Article R411-46](article-r411-46.md)
- [Article R411-47](article-r411-47.md)
- [Article R411-48](article-r411-48.md)
- [Article R411-49](article-r411-49.md)
- [Article R411-50](article-r411-50.md)
- [Article R411-51](article-r411-51.md)
- [Article R411-52](article-r411-52.md)
- [Article R411-53](article-r411-53.md)
