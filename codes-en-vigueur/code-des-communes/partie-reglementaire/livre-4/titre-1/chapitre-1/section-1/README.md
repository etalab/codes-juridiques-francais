# SECTION 1 : Dispositions générales.

- [Article R*411-1](article-r-411-1.md)
- [Article R*411-2](article-r-411-2.md)
- [Article R*411-3](article-r-411-3.md)
