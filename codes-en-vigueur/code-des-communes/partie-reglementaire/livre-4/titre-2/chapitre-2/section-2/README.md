# SECTION 2 : Formation professionnelle continue.

- [SOUS-SECTION 1 : Actions de formation.](sous-section-1)
- [SOUS-SECTION 2 : Participation des agents non titulaires aux cycles ou stages pour la préparation à l'accès aux emplois.](sous-section-2)
- [SOUS-SECTION 3 : Actions de formation choisies par les agents non titulaires en vue de leur formation personnelle.](sous-section-3)
- [SOUS-SECTION 4 : Participation des agents non titulaires à temps plein aux stages de conversion ou de promotion professionnelle .](sous-section-4)
- [Article R*422-3](article-r-422-3.md)
- [Article R*422-4](article-r-422-4.md)
