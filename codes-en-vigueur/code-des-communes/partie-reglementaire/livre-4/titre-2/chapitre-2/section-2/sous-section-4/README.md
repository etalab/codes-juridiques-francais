# SOUS-SECTION 4 : Participation des agents non titulaires à temps plein aux stages de conversion ou de promotion professionnelle .

- [Article R*422-33](article-r-422-33.md)
- [Article R*422-34](article-r-422-34.md)
- [Article R*422-35](article-r-422-35.md)
- [Article R*422-36](article-r-422-36.md)
