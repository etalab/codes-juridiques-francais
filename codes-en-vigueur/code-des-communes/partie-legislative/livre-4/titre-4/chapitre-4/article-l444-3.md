# Article L444-3

Le préfet de police exerce les pouvoirs du maire sur les personnels de la commune placés sous son autorité.
