# Article L413-15

Un décret en Conseil d'Etat détermine les règles suivant lesquelles sont fixées les modalités d'organisation et de fonctionnement du fonds ainsi que les autres conditions d'application des articles L. 413-11 à L. 413-14.
