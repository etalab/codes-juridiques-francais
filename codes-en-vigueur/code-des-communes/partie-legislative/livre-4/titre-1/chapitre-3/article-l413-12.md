# Article L413-12

Les communes et les établissements publics communaux et intercommunaux sont tenus de s'affilier au fonds national de compensation.

Les dépenses qui résultent tant du paiement du supplément familial du traitement que du fonctionnement du fonds constituent des dépenses obligatoires pour ces collectivités.
