# Article L413-11

Un fonds national de compensation répartit entre les communes et les établissements publics communaux et intercommunaux les charges résultant pour ces collectivités du paiement du supplément familial de traitement qu'elles versent à leur personnel.

La compensation est opérée sur la base du montant total des salaires payés aux agents des collectivités locales affiliées au fonds national de compensation, et dans la limite du supplément familial de traitement.
