# Article L417-16

Les agents communaux tributaires de la caisse nationale de retraites des agents des collectivités locales qui, antérieurement à leur affiliation à cet organisme, ont été assujettis à un règlement particulier régulièrement approuvé et dont les pensions ont été révisées, ou ont été ou seront concédées en vertu du décret n° 49-1416 du 5 octobre 1949, conservent également, sous réserve de l'application des dispositions de l'article 15 de ce décret, le bénéfice du nombre et du taux des annuités résultant du règlement particulier pour les services antérieurs au 1er juillet 1941.

Toute révision des pensions qui résulte postérieurement au 31 décembre 1954 d'une modification des émoluments leur servant de base est effectuée suivant les mêmes modalités de calcul.
