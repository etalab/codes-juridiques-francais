# Article L417-9

Les conditions d'attribution et les modalités de concession, de liquidation, de paiement et de révision de l'allocation temporaire d'invalidité sont fixées par voie réglementaire.
