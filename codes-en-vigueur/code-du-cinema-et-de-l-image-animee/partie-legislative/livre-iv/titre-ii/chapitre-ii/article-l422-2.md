# Article L422-2

Dans le cas prévu au 10° de l'article L. 421-1, peut être prononcée une sanction pécuniaire dont le montant ne peut excéder 3 % du chiffre d'affaires hors taxes réalisé au cours du dernier exercice clos calculé sur une période de douze mois. Ce maximum est porté à 5 % en cas de réitération du même manquement dans un délai de cinq ans à compter de la date de notification de la première sanction.
