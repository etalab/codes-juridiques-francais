# Chapitre VI : Taxes, prélèvements et autres produits affectés au Centre national du cinéma et de l'image animée

- [Article L116-1](article-l116-1.md)
- [Article L116-2](article-l116-2.md)
- [Article L116-3](article-l116-3.md)
- [Article L116-4](article-l116-4.md)
- [Article L116-5](article-l116-5.md)
