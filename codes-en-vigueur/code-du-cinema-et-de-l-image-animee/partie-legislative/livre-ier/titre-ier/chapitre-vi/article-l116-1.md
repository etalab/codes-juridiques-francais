# Article L116-1

Est affecté au Centre national du cinéma et de l'image animée le produit de la taxe sur les ventes et locations de vidéogrammes destinés à l'usage privé du public et sur les opérations assimilées mentionnées à l'article 1609 sexdecies B du code général des impôts.
