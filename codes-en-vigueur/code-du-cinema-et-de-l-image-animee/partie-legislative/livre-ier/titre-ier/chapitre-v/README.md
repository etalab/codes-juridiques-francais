# Chapitre V : Impositions affectées au Centre national du cinéma et de l'image animée et perçues par lui

- [Section 1 : Taxe sur le prix des entrées aux séances organisées par les exploitants d'établissements de spectacles cinématographiques](section-1)
- [Section 2 : Taxe sur les éditeurs et distributeurs de services de télévision](section-2)
- [Section 3 : Cotisations professionnelles](section-3)
- [Section 4 : Recouvrement et contrôle](section-4)
