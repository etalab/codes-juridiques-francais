# Titre Ier : Centre national du cinéma et de l'image animée

- [Chapitre Ier : Statut et missions](chapitre-ier)
- [Chapitre II : Organisation et fonctionnement](chapitre-ii)
- [Chapitre III : Recrutement et statut des agents contractuels](chapitre-iii)
- [Chapitre IV : Dispositions financières et comptables](chapitre-iv)
- [Chapitre V : Impositions affectées au Centre national du cinéma et de l'image animée et perçues par lui](chapitre-v)
- [Chapitre VI : Taxes, prélèvements et autres produits affectés au Centre national du cinéma et de l'image animée](chapitre-vi)
