# Titre III : Incitations fiscales

- [Chapitre Ier : Crédits d'impôt](chapitre-ier)
- [Chapitre II : Financement en capital d'œuvres cinématographiques ou audiovisuelles](chapitre-ii)
- [Chapitre III : Déductions fiscales et réductions d'impôt
au titre des investissements outre-mer](chapitre-iii)
- [Chapitre IV : Taxe sur la valeur ajoutée](chapitre-iv)
- [Chapitre V : Contribution économique territoriale](chapitre-v)
- [Chapitre VI : Dispositions diverses](chapitre-vi)
