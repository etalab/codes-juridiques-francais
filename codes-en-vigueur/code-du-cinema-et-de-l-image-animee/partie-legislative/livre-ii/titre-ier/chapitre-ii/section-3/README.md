# Section 3 : Homologation des établissements de spectacles cinématographiques

- [Article L212-14](article-l212-14.md)
- [Article L212-15](article-l212-15.md)
- [Article L212-16](article-l212-16.md)
- [Article L212-17](article-l212-17.md)
