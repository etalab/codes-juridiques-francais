# Section 6 : Formules d'accès au cinéma

- [Article L212-27](article-l212-27.md)
- [Article L212-28](article-l212-28.md)
- [Article L212-29](article-l212-29.md)
- [Article L212-30](article-l212-30.md)
- [Article L212-31](article-l212-31.md)
