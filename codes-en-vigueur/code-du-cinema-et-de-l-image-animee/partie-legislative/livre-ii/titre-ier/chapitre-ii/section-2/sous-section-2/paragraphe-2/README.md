# Paragraphe 2 : Décision de la commission départementale d'aménagement cinématographique

- [Article L212-9](article-l212-9.md)
- [Article L212-10](article-l212-10.md)
- [Article L212-10-1](article-l212-10-1.md)
- [Article L212-10-2](article-l212-10-2.md)
