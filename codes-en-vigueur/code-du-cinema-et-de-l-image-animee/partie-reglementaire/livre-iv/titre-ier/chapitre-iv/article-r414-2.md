# Article R414-2

Une copie des procès-verbaux prévus à l'article L. 414-1 et des notifications adressées en application de l'article L. 414-2 est transmise au président du Centre national du cinéma et de l'image animée.
