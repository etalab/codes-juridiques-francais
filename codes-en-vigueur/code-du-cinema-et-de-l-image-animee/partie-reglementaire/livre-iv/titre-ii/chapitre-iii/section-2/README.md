# Section 2 : Procédure de sanction

- [Article R423-9](article-r423-9.md)
- [Article R423-10](article-r423-10.md)
- [Article R423-11](article-r423-11.md)
- [Article R423-12](article-r423-12.md)
- [Article R423-13](article-r423-13.md)
- [Article R423-14](article-r423-14.md)
- [Article R423-15](article-r423-15.md)
