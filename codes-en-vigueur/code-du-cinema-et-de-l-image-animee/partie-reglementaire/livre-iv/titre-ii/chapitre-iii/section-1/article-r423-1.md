# Article R423-1

Le collège de la commission du contrôle de la réglementation compétent pour prononcer les sanctions prévues aux articles L. 422-1 et L. 422-2 comprend, outre le président de la commission, neuf membres :

1° Au titre des représentants de l'Etat :

a) Un représentant du ministre chargé de la culture ;

b) Un représentant du ministre chargé du budget ;

c) Un représentant du garde des sceaux, ministre de la justice ;

2° Au titre des professionnels :

a) Deux représentants du secteur du cinéma ;

b) Un représentant des secteurs de la vidéo et du multimédia ;

3° Au titre des personnalités qualifiées :

a) Une personnalité qualifiée, désignée en raison de ses compétences en matière de droit de la propriété intellectuelle ;

b) Une personnalité qualifiée, désignée en raison de ses compétences en matière de droit public ;

c) Une personnalité qualifiée, désignée en raison de ses compétences en matière de gestion et de comptabilité des entreprises.
