# Section 1 : Infractions aux obligations d'information du public concernant les interdictions aux mineurs

- [Article R432-1](article-r432-1.md)
- [Article R432-2](article-r432-2.md)
