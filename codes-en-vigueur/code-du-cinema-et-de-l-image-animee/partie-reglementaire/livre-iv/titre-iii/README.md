# Titre III : Dispositions pénales

- [Chapitre II : Infractions aux dispositions relatives au visa d'exploitation cinématographique](chapitre-ii)
- [Chapitre IV : Infractions aux dispositions relatives à l'implantation des établissements de spectacles cinématographiques](chapitre-iv)
