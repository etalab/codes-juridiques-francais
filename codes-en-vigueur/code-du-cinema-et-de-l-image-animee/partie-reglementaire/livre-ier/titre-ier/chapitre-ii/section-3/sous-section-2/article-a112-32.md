# Article A112-32

Sont publiées au Bulletin officiel du cinéma et de l'image animée :

1° Les directives, instructions et circulaires, prises par le président du Centre national du cinéma et de l'image animée au titre des prérogatives prévues à l'article L. 111-3, qui comportent une interprétation du droit positif ou une description des procédures administratives, en application de l'article 29 du décret n° 2005-1755 du 30 décembre 2005 relatif à la liberté d'accès aux documents administratifs et à la réutilisation des informations publiques, pris pour l'application de la loi n° 78-753 du 17 juillet 1978 ;

2° Lorsque le conseil d'administration en décide ainsi, les directives, instructions et circulaires du Centre national du cinéma et de l'image animée qui comportent une interprétation du droit positif ou une description des procédures administratives, en application de l'article 32 du décret du 30 décembre 2005 mentionné au 1° du présent article.
