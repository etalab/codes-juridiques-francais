# Section 2 : Président

- [Article R112-23](article-r112-23.md)
- [Article R112-24](article-r112-24.md)
- [Article R112-25](article-r112-25.md)
