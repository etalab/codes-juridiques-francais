# Article A113-5

Les règles relatives à certains emplois fonctionnels du Centre national du cinéma et de l'image animée sont fixées par l'arrêté du 28 août 2008 fixant le nombre de directeurs et de directeurs adjoints du Centre national de la cinématographie pouvant accéder à l'échelon exceptionnel de leurs emplois.
