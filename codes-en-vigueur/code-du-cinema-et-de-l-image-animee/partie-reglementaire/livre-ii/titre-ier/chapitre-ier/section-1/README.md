# Section 1 : Délivrance du visa d'exploitation cinématographique

- [Sous-section 1 : Demande de visa d'exploitation cinématographique](sous-section-1)
- [Sous-section 2 : Rapport des comités de classification et avis de la commission de classification](sous-section-2)
- [Sous-section 3 : Décision du ministre chargé de la culture](sous-section-3)
