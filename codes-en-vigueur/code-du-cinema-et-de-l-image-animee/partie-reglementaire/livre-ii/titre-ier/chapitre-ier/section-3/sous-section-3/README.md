# Sous-section 3 : Dispositions communes

- [Article D211-44](article-d211-44.md)
- [Article R211-39](article-r211-39.md)
- [Article R211-40](article-r211-40.md)
- [Article R211-41](article-r211-41.md)
- [Article R211-42](article-r211-42.md)
- [Article R211-43](article-r211-43.md)
