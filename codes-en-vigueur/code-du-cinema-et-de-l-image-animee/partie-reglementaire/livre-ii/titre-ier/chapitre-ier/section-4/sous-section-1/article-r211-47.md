# Article R211-47

Le visa d'exploitation cinématographique délivré pour la représentation cinématographique locale d'une œuvre ou d'un document comporte les indications mentionnées aux 1° à 4° de l'article R. 211-46.
