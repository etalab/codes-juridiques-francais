# Article A210-11

L'œuvre cinématographique de longue durée représentant le cinéma français pour l'attribution de l'Oscar du film en langue étrangère est sélectionnée par une commission composée :

1° Du délégué général de l'Association française du festival international du film (AFFIF) ;

2° De cinq personnalités qualifiées nommées chaque année par le ministre chargé de la culture ;

3° D'une personnalité qualifiée désignée par le président du Centre national du cinéma et de l'image animée dans les conditions du 2° de l'article L. 111-3.

Le secrétariat de la commission est assuré par le Centre national du cinéma et de l'image animée.
