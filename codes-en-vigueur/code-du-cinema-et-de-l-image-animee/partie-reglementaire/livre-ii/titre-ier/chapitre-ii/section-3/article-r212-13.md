# Article R212-13

Le président du Centre national du cinéma et de l'image animée peut retirer l'homologation lorsqu'il s'avère que les caractéristiques d'une salle ou des équipements techniques de projection ne sont pas conformes à la description figurant dans le dossier de la demande au vu duquel l'homologation a été accordée.

Le titulaire est mis à même de faire valoir ses observations dans un délai de trente jours.
