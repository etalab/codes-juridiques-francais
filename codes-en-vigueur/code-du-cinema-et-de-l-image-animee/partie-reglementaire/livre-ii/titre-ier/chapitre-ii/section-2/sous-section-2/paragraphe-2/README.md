# Paragraphe 2 : Décision de la commission départementale d'aménagement cinématographique

- [Sous-Paragraphe 1 : Demande d'autorisation](sous-paragraphe-1)
- [Sous-Paragraphe 2 : Procédure d'autorisation](sous-paragraphe-2)
- [Sous-Paragraphe 3 : Dispositions diverses](sous-paragraphe-3)
