# Sous-Paragraphe 2 : Examen du recours

- [Article R212-7-25](article-r212-7-25.md)
- [Article R212-7-26](article-r212-7-26.md)
- [Article R212-7-27](article-r212-7-27.md)
- [Article R212-7-28](article-r212-7-28.md)
- [Article R212-7-29](article-r212-7-29.md)
- [Article R212-7-30](article-r212-7-30.md)
- [Article R212-7-31](article-r212-7-31.md)
