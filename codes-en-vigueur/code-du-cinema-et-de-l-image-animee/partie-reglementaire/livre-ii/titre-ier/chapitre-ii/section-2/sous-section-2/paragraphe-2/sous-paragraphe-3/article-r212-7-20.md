# Article R212-7-20

<div align="left">Lorsque la réalisation d'un projet autorisé ne nécessite pas de permis de construire, l'autorisation est périmée pour les salles et pour les places de spectateurs qui n'ont pas été mises en exploitation dans un délai de trois ans à compter de la notification prévue à l'article R. 212-7-18 ou de la date à laquelle l'autorisation est réputée accordée en vertu de l'article L. 212-10-1. <br/>
<br/>Lorsque la réalisation d'un projet autorisé est subordonnée à l'obtention d'un permis de construire, l'autorisation est périmée si un dossier de demande de permis de construire considéré comme complet au regard des articles R. 423-19 à R. 423-22 du code de l'urbanisme n'est pas déposé dans un délai de deux ans à compter de la date fixée au premier alinéa. <br/>
<br/>Si la faculté de recours prévue à l'article L. 212-10-3 a été exercée, ces délais courent à compter de la date de la notification de la décision de la Commission nationale d'aménagement cinématographique. <br/>
<br/>En cas de suspension de l'exécution d'une autorisation, ces délais sont suspendus pendant la durée de la suspension. <br/>
<br/>Lorsqu'une demande de permis de construire a été déposée dans le délai et les conditions prévus au deuxième alinéa, l'autorisation est périmée pour les salles et pour les places de spectateurs qui n'ont pas été mises en exploitation dans un délai de trois ans à compter de la date à laquelle le permis de construire est devenu définitif. Toutefois, ce délai est porté à cinq ans dans le cas où le projet a vocation à s'intégrer dans un ensemble commercial de plus de 6 000 mètres carrés, situé sur le même terrain.<br/>
<br/>
<br/>
</div>
