# Article D212-91

Les établissements de spectacles cinématographiques d'art et d'essai sont répartis en deux groupes, selon les modalités prévues aux articles D. 212-92 et D. 212-93.
