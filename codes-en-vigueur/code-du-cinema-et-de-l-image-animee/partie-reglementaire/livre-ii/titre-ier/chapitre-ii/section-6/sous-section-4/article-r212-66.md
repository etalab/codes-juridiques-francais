# Article R212-66

L'agrément peut être retiré, en cas de violation des conditions exigées pour sa délivrance, après avis de la commission d'agrément des formules d'accès au cinéma et à l'issue d'une procédure contradictoire.
