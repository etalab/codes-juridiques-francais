# Article R212-63

Les membres de la commission d'agrément des formules d'accès au cinéma sont soumis à une obligation de confidentialité en ce qui concerne tous les faits, renseignements et documents dont ils ont connaissance en cette qualité.
