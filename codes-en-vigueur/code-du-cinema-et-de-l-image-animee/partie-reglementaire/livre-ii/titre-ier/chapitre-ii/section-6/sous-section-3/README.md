# Sous-section 3 : Commission d'agrément des formules d'accès au cinéma

- [Article R212-58](article-r212-58.md)
- [Article R212-59](article-r212-59.md)
- [Article R212-60](article-r212-60.md)
- [Article R212-61](article-r212-61.md)
- [Article R212-62](article-r212-62.md)
- [Article R212-63](article-r212-63.md)
- [Article R212-64](article-r212-64.md)
- [Article R212-65](article-r212-65.md)
