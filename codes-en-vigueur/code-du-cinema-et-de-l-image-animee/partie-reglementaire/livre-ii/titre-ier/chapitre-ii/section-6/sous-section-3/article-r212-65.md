# Article R212-65

Le secrétariat de la commission d'agrément des formules d'accès au cinéma est assuré par le Centre national du cinéma et de l'image animée.
