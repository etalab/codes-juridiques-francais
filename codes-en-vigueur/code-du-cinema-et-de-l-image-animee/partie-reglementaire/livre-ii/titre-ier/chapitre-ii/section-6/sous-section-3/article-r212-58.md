# Article R212-58

La commission d'agrément des formules d'accès au cinéma, placée auprès du président du Centre national du cinéma et de l'image animée, est saisie pour avis par ce dernier avant la délivrance de l'agrément ou d'un agrément modificatif.

Toutefois, en cas de modification relevant du 3° de l'article R. 212-46, le président du Centre national du cinéma et de l'image animée n'est pas tenu de saisir cette commission mais l'informe de la modification envisagée.
