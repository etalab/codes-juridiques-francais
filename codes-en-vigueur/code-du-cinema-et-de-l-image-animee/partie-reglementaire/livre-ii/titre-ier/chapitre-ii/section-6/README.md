# Section 6 : Formules d'accès au cinéma

- [Sous-section 1 : Conditions de l'agrément](sous-section-1)
- [Sous-section 2 : Demande d'agrément](sous-section-2)
- [Sous-section 3 : Commission d'agrément des formules d'accès au cinéma](sous-section-3)
- [Sous-section 4 : Retrait de l'agrément](sous-section-4)
