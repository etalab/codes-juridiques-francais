# Paragraphe 1 : Dispositions générales

- [Article D212-68](article-d212-68.md)
- [Article D212-69](article-d212-69.md)
- [Article D212-70](article-d212-70.md)
- [Article D212-71](article-d212-71.md)
