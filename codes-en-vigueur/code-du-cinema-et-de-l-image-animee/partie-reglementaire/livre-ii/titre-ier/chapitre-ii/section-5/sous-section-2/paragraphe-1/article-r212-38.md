# Article R212-38

Les engagements de programmation donnent lieu à l'établissement, par les opérateurs concernés, d'un rapport annuel d'exécution remis au président du Centre national du cinéma et de l'image animée.
