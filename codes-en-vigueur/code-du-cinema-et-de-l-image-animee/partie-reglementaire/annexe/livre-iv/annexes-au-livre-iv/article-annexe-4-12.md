# Article Annexe 4-12

Agrément de diffusion (article 412-17)

Liste des documents justificatifs :

1° Le contrat de cession des droits de diffusion de la ou des œuvres cinématographiques de courte durée ;

2° Une copie DVD de la ou des œuvres cinématographiques de courte durée.
