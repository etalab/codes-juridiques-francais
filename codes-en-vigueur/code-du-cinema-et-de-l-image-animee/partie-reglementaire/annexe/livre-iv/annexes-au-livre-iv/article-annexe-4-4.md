# Article Annexe 4-4

Aides aux programmes de production avant réalisation Festivals français de catégorie 1 (article 411-40)

1° Aix-en-Provence : Festival Tout Courts ;

2° Alès : Festival Itinérances ;

3° Angers : Festival Premiers Plans ;

4° Annecy : Festival du Film d'animation ;

5° Arcueil : Festival Ecrans documentaires ;

6° Aubagne : Festival International du Film ;

7° Belfort : Festival Entrevues ;

8° Brest : Festival Européen du Film Court ;

9° Brive : Festival du moyen métrage de Brive ;

10° Cannes :

- Festival International du Film ;

- Quinzaine des Réalisateurs ;

- Semaine Internationale de la Critique ;

11° Cinéssonne : Festival du cinéma européen en Essonne ;

12° Clermont-Ferrand : Festival International du Court Métrage ;

13° Cognac : Festival du film policier de Cognac ;

14° Créteil : Festival International de Films de Femmes ;

15° Douarnenez : Festival de cinéma ;

16° Gardanne : Festival Cinématographique d'Automne ;

17° Gérardmer : Festival international du film fantastique ;

18° Gindou : Rencontres Cinéma ;

19° Grenoble : Festival du Court Métrage en plein air ;

20° Lille : Rencontres audiovisuelles ;

21° Lussas : Etats généraux du documentaire ;

22° Marseille : Festival International du Documentaire ;

23° Meudon : Festival du Court Métrage d'Humour ;

24° Montpellier : Festival International du Film Méditerranéen ;

25° Moulins sur Allier : Festival Jean Carmet ;

26° Nice : Un festival c'est trop court ;

27° Pantin : Festival international du Film Court ;

28° Paris :

- Festival de films documentaires - Cinéma du Réel ;

- Silhouette ;

- Courts Devant ;

29° Strasbourg : Festival européen du film fantastique ;

30° Trouville : Festival Off-Courts de Trouville ;

31° Vendôme : Festival Images en Région ;

32° Villeurbanne : Festival du Film Court ;

33° MyFrenchFilmFestival.com.
