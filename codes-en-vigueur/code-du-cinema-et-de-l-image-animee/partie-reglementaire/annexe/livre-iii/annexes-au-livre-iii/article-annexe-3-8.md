# Article Annexe 3-8

Autorisation définitive (article 311-100)

Liste des documents justificatifs, par genre :

I. - Fiction :

1° L'attestation, dûment renseignée et certifiée, de l'acceptation de l'œuvre par le ou les éditeurs de services de télévision chargés d'en assurer la diffusion ou par le ou les éditeurs de services chargés d'en assurer la mise à disposition du public ;

2° Un document comptable indiquant le coût définitif de l'œuvre, les moyens de son financement et faisant apparaître précisément les dépenses réalisées en France, le cas échéant certifié par un commissaire aux comptes ;

3° Toute pièce justificative d'un financement public ou privé ;

4° Tout contrat de coproduction conclu avec une autre entreprise française ou étrangère s'il a été modifié ou non fourni au moment de l'autorisation préalable ;

5° Le relevé complet des génériques ;

6° La liste nominative définitive avec mention des nationalités et de la résidence fiscale des personnels engagés sur la production de l'œuvre, précisant la fonction, les salaires bruts et la part patronale des charges afférentes à chacune de ces rémunérations ;

7° La liste définitive des entreprises prestataires engagées, précisant leur lieu d'établissement ;

8° La copie de tout contrat de cession des droits à l'image et d'interprétation des artistes-interprètes et contrats des acteurs principaux ;

9° La copie des notes de droits d'auteur et la copie des bulletins de paie correspondant aux postes suivants : réalisateur-technicien, directeur de la photographie, chef opérateur de prise de son, chef monteur ;

10° Le contrat du ou des diffuseurs, ses annexes et éventuels avenants s'ils n'ont pas été fournis lors de l'autorisation préalable ou si de nouveaux avenants ont été signés ;

11° Tout contrat de coproduction conclu depuis l'obtention de l'autorisation préalable ainsi que la justification de son inscription au registre public du cinéma et de l'audiovisuel, lorsque l'œuvre a été immatriculée ;

12° Une copie vidéo de l'œuvre incluant les génériques.

II. - Animation :

1° L'attestation, dûment renseignée et certifiée, de l'acceptation de l'œuvre par le ou les éditeurs de services de télévision chargés d'en assurer la diffusion ou par le ou les éditeurs de services chargés d'en assurer la mise à disposition du public ;

2° Un document comptable indiquant le coût définitif de l'œuvre, les moyens de son financement et faisant apparaître précisément les dépenses réalisées en France, le cas échéant certifié par un commissaire aux comptes ;

3° Toute pièce justificative d'un financement public ou privé ;

4° Tout contrat de coproduction conclu avec une autre entreprise française ou étrangère s'il a été modifié ou non fourni au moment de l'autorisation préalable ;

5° Le relevé complet des génériques ;

6° La liste nominative définitive avec mention des nationalités et de la résidence fiscale des personnels engagés sur la production de l'œuvre, précisant la fonction, la durée du travail, les salaires bruts et la part patronale des charges afférentes à chacune de ces rémunérations ;

7° La copie des notes de droits d'auteur et la copie des bulletins de paie correspondant aux postes suivants : réalisateur, storyboarder, chef layout, chef-décorateur ;

8° Le contrat du ou des diffuseurs, ses annexes et éventuels avenants s'ils n'ont pas été fournis lors de l'autorisation préalable ou si de nouveaux avenants ont été signés ;

9° La liste définitive des entreprises prestataires engagées, précisant leur lieu d'établissement ;

10° Le récapitulatif des factures des prestataires techniques avec mention de leur nom et adresse ;

11° Le ou les contrats de prestation, les annexes et éventuels avenants s'ils n'ont pas été fournis lors de l'autorisation préalable ou si de nouveaux avenants ont été signés ;

12° Tout contrat de coproduction conclu depuis l'obtention de l'autorisation préalable ainsi que la justification de son inscription au registre public du cinéma et de l'audiovisuel, lorsque l'œuvre a été immatriculée ;

13° Une copie vidéo de l'œuvre incluant les génériques.

III. - Documentaire de création :

1° L'attestation, dûment renseignée et certifiée, de l'acceptation de l'œuvre par le ou les éditeurs de services de télévision chargés d'en assurer la diffusion ou par le ou les éditeurs de services chargés d'en assurer la mise à disposition du public ;

2° Un document comptable indiquant le coût définitif de l'œuvre, les moyens de son financement et faisant apparaître précisément les dépenses réalisées en France, le cas échéant certifié par un commissaire aux comptes ;

3° Toute pièce justificative d'un financement public ou privé ;

4° Tout contrat de coproduction conclu avec une autre entreprise française ou étrangère s'il a été modifié ou non fourni au moment de l'autorisation préalable ;

5° Le relevé complet des génériques ;

6° La liste nominative définitive avec mention des nationalités et de la résidence fiscale des personnels engagés sur la production de l'œuvre, précisant la fonction, les salaires bruts et la part patronale des charges afférentes à chacune de ces rémunérations ;

7° La liste définitive des entreprises prestataires engagées, précisant leur lieu d'établissement ;

8° Une copie des contrats de cession des archives des images existantes ;

9° La copie des notes de droits d'auteur et la copie des bulletins de paie correspondant aux postes suivants : réalisateur (y compris lorsqu'il est embauché sous le statut de journaliste), directeur de la photographie, chef opérateur de prise de vues, chef opérateur de prise de son, ingénieur du son, chef monteur, directeur de production, producteur exécutif et l'animateur intervenant à l'image ;

10° Le contrat du ou des diffuseurs, ses annexes et éventuels avenants s'ils n'ont pas été fournis lors de l'autorisation préalable ou si de nouveaux avenants ont été signés ;

11° Tout contrat de coproduction conclu depuis l'obtention de l'autorisation préalable ainsi que la justification de son inscription au registre public du cinéma et de l'audiovisuel, lorsque l'œuvre a été immatriculée ;

12° Une copie vidéo de l'œuvre incluant les génériques.

IV. - Adaptation audiovisuelle de spectacle vivant :

1° L'attestation, dûment renseignée et certifiée, de l'acceptation de l'œuvre par le ou les éditeurs de services de télévision chargés d'en assurer la diffusion ou par le ou les éditeurs de services chargés d'en assurer la mise à disposition du public ;

2° Un document comptable indiquant le coût définitif de l'œuvre, les moyens de son financement et faisant apparaître précisément les dépenses réalisées en France, le cas échéant certifié par un commissaire aux comptes ;

3° La copie du découpage ;

4° Toute pièce justificative d'un financement public ou privé, tout contrat de coproduction conclu avec une autre entreprise française ou étrangère s'il a été modifié ou non fourni au moment de l'autorisation préalable ;

5° Le relevé complet des génériques ;

6° La liste nominative définitive avec mention des nationalités et de la résidence fiscale des personnels engagés sur la production de l'œuvre, précisant la fonction, les salaires bruts et la part patronale des charges afférentes à chacune de ces rémunérations ;

7° La liste définitive des entreprises prestataires engagées, précisant leur lieu d'établissement ;

8° Tout contrat de cession des droits à l'image et d'interprétation des artistes-interprètes ;

9° La copie des notes de droits d'auteur et la copie des bulletins de paie correspondant aux postes suivants : réalisateur-technicien, directeur de la photographie, ingénieur du son tournage, chef-monteur, ingénieur du son mixage, scripte ;

10° Le contrat du ou des diffuseurs, ses annexes et éventuels avenants s'ils n'ont pas été fournis lors de l'autorisation préalable ou si de nouveaux avenants ont été signés ;

11° Tout contrat de coproduction conclu depuis l'obtention de l'autorisation préalable ainsi que la justification de son inscription au registre public du cinéma et de l'audiovisuel, lorsque l'œuvre a été immatriculée ;

12° Une copie vidéo de l'œuvre incluant les génériques.

V. - Magazine :

1° L'attestation, dûment renseignée et certifiée, de l'acceptation de l'œuvre par le ou les éditeurs de services de télévision chargés d'en assurer la diffusion ou par le ou les éditeurs de services chargés d'en assurer la mise à disposition du public ;

2° Un document comptable indiquant le coût définitif de l'œuvre, les moyens de son financement et faisant apparaître précisément les dépenses réalisées en France, le cas échéant certifié par un commissaire aux comptes ;

3° Toute pièce justificative d'un financement public ou privé ;

4° Tout contrat de coproduction conclu avec une autre entreprise française ou étrangère s'il a été modifié ou non fourni au moment de l'autorisation préalable ;

5° Le relevé complet des génériques ;

6° La liste nominative définitive avec mention des nationalités et de la résidence fiscale des personnels engagés sur la production de l'œuvre, précisant la fonction, les salaires bruts et la part patronale des charges afférentes à chacune de ces rémunérations ;

7° La liste définitive des entreprises prestataires engagées, précisant leur lieu d'établissement ;

8° Une copie des contrats de cession des archives des images existantes ;

9° La copie des notes de droits d'auteur et la copie des bulletins de paie correspondant aux postes suivants : réalisateur (y compris lorsqu'il est embauché sous le statut de journaliste), directeur de la photographie, chef opérateur de prise de vues, chef opérateur de prise de son, ingénieur du son, chef monteur, directeur de production, producteur exécutif et l'animateur intervenant à l'image ;

10° Le contrat du ou des diffuseurs, ses annexes et éventuels avenants s'ils n'ont pas été fournis lors de l'autorisation préalable ou si de nouveaux avenants ont été signés ;

11° Une copie vidéo de l'œuvre incluant les génériques.
