# Article Annexe 2-34

Aide art et essai - Coefficient minorateur Confort de la salle et qualité de projection (article 231-9)

<table>
<tbody>
<tr>
<th>
<br/>QUALITÉ</th>
<th>
<br/>GROUPE 1</th>
<th>
<br/>GROUPE 2</th>
</tr>
<tr>
<td align="center" valign="middle">
<br/>Très mauvais</td>
<td align="center" valign="middle">
<br/>- 25</td>
<td align="center" valign="middle">
<br/>- 0,25</td>
</tr>
<tr>
<td align="center" valign="middle">
<br/>Médiocre</td>
<td align="center" valign="middle">
<br/>- 10</td>
<td align="center" valign="middle">
<br/>- 0,10</td>
</tr>
<tr>
<td align="center" valign="middle">
<br/>Moyen</td>
<td align="center" valign="middle">
<br/>- 5</td>
<td align="center" valign="middle">
<br/>- 0,05</td>
</tr>
</tbody>
</table>
