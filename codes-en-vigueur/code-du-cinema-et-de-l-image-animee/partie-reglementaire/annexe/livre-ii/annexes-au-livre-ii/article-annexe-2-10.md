# Article Annexe 2-10

Décision d'attribution à titre définitif de l'aide à la production après réalisation (article 211-136)

Liste des documents justificatifs :

1° Un document certifié par un expert-comptable indiquant le coût définitif de production ;

2° Le plan de financement signé et daté par l'entreprise de production ;

3° Les attestations de comptes à jour délivrées par les organismes de protection sociale (URSSAF, congés spectacles, Pôle emploi, AFDAS, Audiens).
