# Article Annexe 2-5

Allocation à la création de fichiers numériques de sous-titrage et d'audiodescription (article 211-96)

Liste des documents justificatifs :

1° Les factures acquittées détaillées correspondant aux travaux de création de fichiers numériques de sous-titrage et d'audiodescription, ainsi que des travaux de transfert multi-support desdits fichiers ;

2° Un tableau récapitulatif de l'ensemble des aides de minimis reçues au cours des trois derniers exercices fiscaux par l'entreprise de production.
