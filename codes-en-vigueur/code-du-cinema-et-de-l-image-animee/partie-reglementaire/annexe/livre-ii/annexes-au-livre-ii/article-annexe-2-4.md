# Article Annexe 2-4

Autorisation d'investissement spécifique pour certaines œuvres d'animation (article 211-79)

Liste des documents justificatifs :

1° Le budget prévisionnel des frais de préparation individualisant les dépenses prévues en France ;

2° Un devis des dépenses de production, accompagné de tout document de nature à justifier que le financement de la production de l'œuvre, hors aides publiques, est confirmé pour au moins 30% de ce devis ;

3° Un plan de financement prévisionnel ;

4° Les contrats de cession de droits d'exploitation conclus avec les auteurs ;

5° Un document attestant du montant des sommes inscrites sur le compte automatique audiovisuel ouvert au nom du producteur.
