# Article Annexe 2-33

Aide art et essai - Coefficient minorateur Nombre d'œuvres d'art et d'essai (article 231-9)

<table>
<tbody>
<tr>
<th colspan="9">
<br/>NOMBRE DE FILMS ART ET ESSAI MINIMUM (BASE 373 FILMS RECOMMANDÉS ART &amp; ESSAI)</th>
</tr>
<tr>
<th/>
<th colspan="2">
<br/>Groupe 1</th>
<th colspan="6">
<br/>Groupe 2</th>
</tr>
<tr>
<th rowspan="2">
<br/>Ecrans</th>
<th colspan="2">
<br/>A et B</th>
<th colspan="2">
<br/>C</th>
<th colspan="2">
<br/>D</th>
<th colspan="2">
<br/>E</th>
</tr>
<tr>
<th>
<br/>Minimum</th>
<th>
<br/>Inéligible</th>
<th>
<br/>Minimum</th>
<th>
<br/>Inéligible</th>
<th>
<br/>Minimum</th>
<th>
<br/>Inéligible</th>
<th>
<br/>Minimum</th>
<th>
<br/>Inéligible</th>
</tr>
<tr>
<td align="center" valign="middle">
<br/>1</td>
<td align="center" valign="middle">
<br/>44</td>
<td align="center" valign="middle">
<br/>36</td>
<td align="center" valign="middle">
<br/>58</td>
<td align="center" valign="middle">
<br/>42</td>
<td align="center" valign="middle">
<br/>53</td>
<td align="center" valign="middle">
<br/>32</td>
<td align="center" valign="middle">
<br/>37</td>
<td align="center" valign="middle">
<br/>21</td>
</tr>
<tr>
<td align="center" valign="middle">
<br/>2</td>
<td align="center" valign="middle">
<br/>86</td>
<td align="center" valign="middle">
<br/>56</td>
<td align="center" valign="middle">
<br/>68</td>
<td align="center" valign="middle">
<br/>47</td>
<td align="center" valign="middle">
<br/>58</td>
<td align="center" valign="middle">
<br/>37</td>
<td align="center" valign="middle">
<br/>47</td>
<td align="center" valign="middle">
<br/>26</td>
</tr>
<tr>
<td align="center" valign="middle">
<br/>3</td>
<td align="center" valign="middle">
<br/>96</td>
<td align="center" valign="middle">
<br/>64</td>
<td align="center" valign="middle">
<br/>79</td>
<td align="center" valign="middle">
<br/>53</td>
<td align="center" valign="middle">
<br/>63</td>
<td align="center" valign="middle">
<br/>47</td>
<td align="center" valign="middle">
<br/>58</td>
<td align="center" valign="middle">
<br/>32</td>
</tr>
<tr>
<td align="center" valign="middle">
<br/>4</td>
<td align="center" valign="middle">
<br/>104</td>
<td align="center" valign="middle">
<br/>72</td>
<td align="center" valign="middle">
<br/>89</td>
<td align="center" valign="middle">
<br/>58</td>
<td align="center" valign="middle">
<br/>68</td>
<td align="center" valign="middle">
<br/>53</td>
<td align="center" valign="middle">
<br/>63</td>
<td align="center" valign="middle">
<br/>37</td>
</tr>
<tr>
<td align="center" valign="middle">
<br/>5</td>
<td align="center" valign="middle">
<br/>112</td>
<td align="center" valign="middle">
<br/>80</td>
<td align="center" valign="middle">
<br/>100</td>
<td align="center" valign="middle">
<br/>63</td>
<td align="center" valign="middle">
<br/>84</td>
<td align="center" valign="middle">
<br/>58</td>
<td align="center" valign="middle">
<br/>68</td>
<td align="center" valign="middle">
<br/>42</td>
</tr>
<tr>
<td align="center" valign="middle">
<br/>6</td>
<td align="center" valign="middle">
<br/>120</td>
<td align="center" valign="middle">
<br/>88</td>
<td align="center" valign="middle">
<br/>110</td>
<td align="center" valign="middle">
<br/>68</td>
<td align="center" valign="middle">
<br/>105</td>
<td align="center" valign="middle">
<br/>63</td>
<td align="center" valign="middle">
<br/>79</td>
<td align="center" valign="middle">
<br/>53</td>
</tr>
<tr>
<td align="center" valign="middle">
<br/>7</td>
<td align="center" valign="middle">
<br/>128</td>
<td align="center" valign="middle">
<br/>96</td>
<td align="center" valign="middle">
<br/>121</td>
<td align="center" valign="middle">
<br/>74</td>
<td align="center" valign="middle">
<br/>110</td>
<td align="center" valign="middle">
<br/>74</td>
<td align="center" valign="middle">
<br/>89</td>
<td align="center" valign="middle">
<br/>63</td>
</tr>
<tr>
<td align="center" valign="middle">
<br/>8</td>
<td align="center" valign="middle">
<br/>136</td>
<td align="center" valign="middle">
<br/>104</td>
<td align="center" valign="middle">
<br/>137</td>
<td align="center" valign="middle">
<br/>84</td>
<td align="center" valign="middle">
<br/>116</td>
<td align="center" valign="middle">
<br/>84</td>
<td align="center" valign="middle">
<br/>100</td>
<td align="center" valign="middle">
<br/>74</td>
</tr>
<tr>
<td align="center" valign="middle">
<br/>9</td>
<td align="center" valign="middle">
<br/>144</td>
<td align="center" valign="middle">
<br/>112</td>
<td align="center" valign="middle">
<br/>152</td>
<td align="center" valign="middle">
<br/>95</td>
<td align="center" valign="middle">
<br/>126</td>
<td align="center" valign="middle">
<br/>95</td>
<td align="center" valign="middle">
<br/>116</td>
<td align="center" valign="middle">
<br/>84</td>
</tr>
<tr>
<td align="center" valign="middle">
<br/>10</td>
<td align="center" valign="middle">
<br/>152</td>
<td align="center" valign="middle">
<br/>120</td>
<td align="center" valign="middle">
<br/>168</td>
<td align="center" valign="middle">
<br/>110</td>
<td align="center" valign="middle">
<br/>137</td>
<td align="center" valign="middle">
<br/>105</td>
<td align="center" valign="middle">
<br/>126</td>
<td align="center" valign="middle">
<br/>95</td>
</tr>
<tr>
<td align="center" valign="middle">
<br/>11</td>
<td align="center" valign="middle">
<br/>160</td>
<td align="center" valign="middle">
<br/>128</td>
<td align="center" valign="middle">
<br/>184</td>
<td align="center" valign="middle">
<br/>126</td>
<td align="center" valign="middle">
<br/>147</td>
<td align="center" valign="middle">
<br/>116</td>
<td align="center" valign="middle">
<br/>137</td>
<td align="center" valign="middle">
<br/>105</td>
</tr>
<tr>
<td align="center" valign="middle">
<br/>12</td>
<td align="center" valign="middle">
<br/>168</td>
<td align="center" valign="middle">
<br/>136</td>
<td align="center" valign="middle">
<br/>200</td>
<td align="center" valign="middle">
<br/>142</td>
<td align="center" valign="middle">
<br/>163</td>
<td align="center" valign="middle">
<br/>126</td>
<td align="center" valign="middle">
<br/>147</td>
<td align="center" valign="middle">
<br/>116</td>
</tr>
<tr>
<td align="center" valign="middle">
<br/>13</td>
<td align="center" valign="middle">
<br/>176</td>
<td align="center" valign="middle">
<br/>144</td>
<td align="center" valign="middle">
<br/>215</td>
<td align="center" valign="middle">
<br/>158</td>
<td align="center" valign="middle">
<br/>179</td>
<td align="center" valign="middle">
<br/>137</td>
<td align="center" valign="middle">
<br/>158</td>
<td align="center" valign="middle">
<br/>126</td>
</tr>
<tr>
<td align="center" valign="middle">
<br/>14</td>
<td align="center" valign="middle">
<br/>184</td>
<td align="center" valign="middle">
<br/>152</td>
<td align="center" valign="middle">
<br/>231</td>
<td align="center" valign="middle">
<br/>173</td>
<td align="center" valign="middle">
<br/>194</td>
<td align="center" valign="middle">
<br/>147</td>
<td align="center" valign="middle">
<br/>168</td>
<td align="center" valign="middle">
<br/>137</td>
</tr>
<tr>
<td align="center" valign="middle">
<br/>15 et plus</td>
<td align="center" valign="middle">
<br/>192</td>
<td align="center" valign="middle">
<br/>160</td>
<td align="center" valign="middle">
<br/>247</td>
<td align="center" valign="middle">
<br/>189</td>
<td align="center" valign="middle">
<br/>210</td>
<td align="center" valign="middle">
<br/>158</td>
<td align="center" valign="middle">
<br/>179</td>
<td align="center" valign="middle">
<br/>147</td>
</tr>
</tbody>
</table>
