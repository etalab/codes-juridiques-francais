# Article Annexe 7-2

Allocations directes au sous-titrage (article 721-12)

Liste des documents justificatifs :

1° Les factures détaillées des frais de sous-titrage au nom de l'entreprise qui sollicite l'aide en précisant, pour chaque facture, les frais auxquels elles correspondent ;

2° Une copie vidéo de l'œuvre cinématographique sous-titrée.
