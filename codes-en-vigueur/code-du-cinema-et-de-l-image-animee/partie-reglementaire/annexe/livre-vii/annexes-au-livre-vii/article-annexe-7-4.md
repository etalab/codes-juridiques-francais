# Article Annexe 7-4

Aide à la promotion à l'étranger des activités et du catalogue d'une entreprise (article 721-25)

Liste des documents justificatifs :

1° Une note détaillant la stratégie de prospection mise en place au cours de l'année de référence et la place réservée aux œuvres cinématographiques françaises ;

2° Une note décrivant les autres supports et actions de promotion réalisés ;

3° Les factures détaillées correspondant à l'ensemble des frais engagés ;

4° Un exemplaire de chaque support de promotion réalisé.
