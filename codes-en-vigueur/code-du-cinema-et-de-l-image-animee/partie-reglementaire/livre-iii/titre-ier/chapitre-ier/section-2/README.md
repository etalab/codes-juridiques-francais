# Section 2 : Exclusion du bénéfice des aides financières

- [Sous-section 1 : Exclusion des œuvres ou documents à caractère pornographique ou d'incitation à la violence](sous-section-1)
- [Sous-section 2 : Exclusion des établissements de spectacles cinématographiques représentant des œuvres à caractère pornographique](sous-section-2)
