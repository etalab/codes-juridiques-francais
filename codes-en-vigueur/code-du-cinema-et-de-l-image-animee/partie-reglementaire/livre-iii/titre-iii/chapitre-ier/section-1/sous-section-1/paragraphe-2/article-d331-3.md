# Article D331-3

Pour l'application du c du 1 du II de l'article 220 sexies du code général des impôts, sont considérées comme réalisées principalement sur le territoire français les œuvres cinématographiques ou audiovisuelles qui remplissent les conditions suivantes :

1° Les œuvres cinématographiques ou audiovisuelles appartenant au genre de la fiction tournées et faisant l'objet de travaux de traitement des images et de postproduction, principalement en France. Des dérogations peuvent être accordées à la condition de localisation principale du tournage en France lorsqu'une partie du temps de tournage est réalisée à l'étranger pour des raisons artistiques tenant à un scénario imposant le recours à des décors naturels ou historiques ;

2° Les œuvres cinématographiques ou audiovisuelles appartenant au genre du documentaire faisant l'objet de travaux de conception et d'écriture, de travaux de traitement des images et de postproduction principalement en France ;

3° Les œuvres cinématographiques ou audiovisuelles appartenant au genre de l'animation faisant l'objet de travaux de conception et d'écriture, de travaux de fabrication, de traitement des images et de postproduction principalement en France.
