# Article D331-22

Le respect des conditions de création des jeux vidéo prévues aux 3° et 4° du 1 du III de l'article 220 terdecies du code général des impôts est vérifié au moyen d'un barème de points. Ce barème est composé d'un groupe " Auteurs et collaborateurs de création " et d'un groupe " Contribution au développement de la création ".

Sont considérés comme répondant aux conditions de création mentionnées à l'alinéa précédent les jeux vidéo ayant obtenu cumulativement un nombre de 11 points au moins au titre du groupe " Auteurs et collaborateurs de création " et un nombre de 14 points au moins au titre du groupe " Contribution au développement de la création ".
