# Article R611-4

La Commission nationale des monuments historiques est présidée par le ministre chargé de la culture ou, en son absence, par le directeur général des patrimoines ou son représentant.
