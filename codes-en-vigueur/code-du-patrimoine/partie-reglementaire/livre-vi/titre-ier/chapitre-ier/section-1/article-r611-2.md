# Article R611-2

La Commission nationale des monuments historiques est divisée en six sections dont les compétences sont les suivantes :

1° Première section : classement des immeubles ;

2° Deuxième section : travaux sur les immeubles classés ou inscrits ;

3° Troisième section : périmètres de protection des immeubles classés ou inscrits et travaux sur les immeubles situés dans ces périmètres ;

4° Quatrième section : classement des objets mobiliers et travaux sur les objets mobiliers classés ;

5° Cinquième section : classement et inscription des orgues, buffets d'orgue et instruments de musique et travaux s'y rapportant ;

6° Sixième section : classement des grottes ornées et travaux sur les grottes ornées classées.
