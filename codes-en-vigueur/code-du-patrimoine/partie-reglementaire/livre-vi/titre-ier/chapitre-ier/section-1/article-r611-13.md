# Article R611-13

La section " classement des objets mobiliers et travaux sur les objets mobiliers classés " comprend les membres suivants :

1° Treize représentants de l'Etat :

a) Quatre membres de droit :

― le directeur général des patrimoines ;

― le responsable des monuments historiques à la direction générale des patrimoines ;

― le responsable des musées de France à la direction générale des patrimoines ;

― le responsable de l'inventaire général du patrimoine culturel à la direction générale des patrimoines ;

b) Neuf membres nommés par arrêté du ministre chargé de la culture :

― sept membres de l'inspection des patrimoines, dont au moins un architecte ;

― deux représentants des services déconcentrés du ministère chargé de la culture ;

2° Deux titulaires d'un mandat électif national ou local nommés par arrêté du ministre chargé de la culture ;

3° Dix personnalités qualifiées nommées par arrêté du ministre chargé de la culture, dont trois membres  d'associations ou de fondations ayant pour objet de favoriser la connaissance, la protection et la conservation du patrimoine ;

4° Des personnalités qualifiées choisies comme expert en raison de leur compétence dans un domaine spécifique traité par la section et nommées par arrêté du ministre chargé de la culture. Leur nombre total ne peut dépasser quinze. Elles siègent lorsque sont examinés des dossiers qui relèvent de leur domaine de compétence.
