# Article R612-8

La commission régionale du patrimoine et des sites, la délégation permanente et la section mentionnée à l'article R. 612-3 se réunissent sur convocation de leur président. L'ordre du jour des séances est arrêté par le président et adressé au ministre chargé de la culture et, selon le cas, aux membres de la commission ou de la section.

Les préfets des départements et les maires des communes dans lesquels se trouvent des immeubles soumis à l'examen de la commission, de la délégation permanente ou de la section mentionnée à l'article R. 612-3 sont informés des questions inscrites à l'ordre du jour qui les concernent et sont entendus par la commission, la délégation permanente ou la section s'ils en font la demande. Ils ne participent ni à la délibération ni au vote.

L'architecte des Bâtiments de France qui a émis l'avis ou pris la décision est invité par le président de la section mentionnée à l'article R. 612-3 à présenter ses observations. Il se retire lorsque la section délibère de l'affaire.

Les membres de l'inspection des patrimoines territorialement compétents sont invités à participer aux réunions de la commission et de la délégation permanente avec voix consultative pour les affaires qui les concernent.

Le président peut faire entendre par la commission, la délégation permanente ou la section mentionnée à l'article R. 612-3 toute personne dont l'audition lui paraît utile. Ces personnes ne participent ni à la délibération ni au vote.

Les rapporteurs sont désignés par le président parmi les membres de la commission ou de la section mentionnée à l'article R. 612-3, ou parmi des personnalités extérieures. Lorsque le rapporteur n'appartient pas à la commission ou à la section, il ne prend pas part au vote.

Les frais de déplacement entraînés par le fonctionnement de la commission ou de la section mentionnée à l'article R. 612-3 sont remboursés dans les conditions fixées par la réglementation applicable aux personnels civils de l'Etat.
