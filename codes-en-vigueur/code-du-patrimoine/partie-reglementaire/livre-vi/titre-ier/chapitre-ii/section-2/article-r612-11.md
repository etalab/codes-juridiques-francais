# Article R612-11

Dans les départements autres que la Haute-Corse et la Corse-du-Sud, la commission départementale des objets mobiliers comprend vingt-cinq membres. Elle est composée :

1° De membres de droit :

a) Le préfet ou son représentant, président ;

b) Le directeur régional des affaires culturelles ou son représentant ;

c) Le conservateur régional des monuments historiques ou son représentant ;

d) Le conservateur du patrimoine, chargé des monuments historiques territorialement compétent ;

e) Le chef de service des opérations d'inventaire du patrimoine culturel ou son représentant ;

f) Le conservateur des antiquités et objets d'art et l'un de ses délégués ou leurs représentants ;

g) L'architecte des Bâtiments de France ou son représentant ;

h) Le directeur des services d'archives du département ou son représentant ;

i) Le directeur départemental de la sécurité publique ou son représentant ;

j) Le commandant de groupement de la gendarmerie ou son représentant ;

2° De membres désignés :

a) Un conservateur de musée ou son suppléant désignés par le préfet ;

b) Un conservateur de bibliothèque ou son suppléant désignés par le préfet ;

c) Deux conseillers départementaux ou leurs suppléants désignés par le conseil départemental ;

d) Trois maires ou leurs suppléants désignés par le préfet ;

e) Cinq personnalités désignées par le préfet ;

f) Deux représentants d'associations ou fondations ayant pour objet de favoriser la connaissance, la protection et la conservation du patrimoine ou leurs suppléants.
