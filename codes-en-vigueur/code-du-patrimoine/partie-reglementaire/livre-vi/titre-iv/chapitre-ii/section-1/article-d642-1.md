# Article D642-1

La décision de mettre à l'étude un projet d'aire de mise en valeur de l'architecture et du patrimoine en application du premier alinéa de l'article L. 642-3 est prise sur délibérations concordantes du ou des conseils municipaux de la ou des communes concernées ou sur délibération de l'organe délibérant de l'établissement public de coopération intercommunale compétent en matière de plan local d'urbanisme.

La délibération par laquelle cette mise à l'étude est prescrite fait l'objet d'un affichage, durant un mois à compter de son adoption, dans les mairies des communes concernées ou au siège de l'établissement public de coopération intercommunale compétent, ainsi que d'une mention insérée dans un journal d'annonces légales diffusé dans le département. Lorsque plusieurs communes sont concernées, le délai d'un mois court à compter de l'adoption de la dernière de ces délibérations.

La délibération est, en outre, publiée :

1° Au recueil des actes administratifs mentionné à l'article R. 2121-10 du code général des collectivités territoriales, lorsqu'il s'agit de la délibération du conseil municipal d'une commune de 3 500 habitants et plus ;

2° Au recueil des actes administratifs mentionné à l'article R. 5211-41 du même code, s'il existe, lorsqu'il s'agit de la délibération de l'organe délibérant d'un établissement public de coopération intercommunale comportant au moins une commune de 3 500 habitants et plus.
