# Article D642-12

La demande d'autorisation est adressée par pli recommandé avec demande d'avis de réception ou déposée à la mairie de la commune dans laquelle les travaux sont envisagés :

1° Par le ou les propriétaires du ou des terrains, leur mandataire ou par une ou plusieurs personnes attestant être autorisées par eux à exécuter les travaux ;

2° En cas d'indivision, par un ou plusieurs co-indivisaires ou leur mandataire ;

3° Par une personne ayant qualité pour bénéficier de l'expropriation pour cause d'utilité publique.
