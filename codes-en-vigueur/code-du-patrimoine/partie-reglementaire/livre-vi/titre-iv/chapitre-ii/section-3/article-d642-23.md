# Article D642-23

Lorsque l'autorité compétente relève de l'Etat, le maire adresse au chef du service de l'Etat chargé de l'architecture et du patrimoine son avis sur chaque demande. Cet avis est réputé favorable s'il n'est pas intervenu dans le délai d'un mois à compter du dépôt à la mairie de la demande.

Lorsque la commune a confié l'instruction des demandes d'autorisation de travaux à un établissement public de coopération intercommunale, le président de cet établissement adresse son avis au chef du service de l'Etat chargé de l'architecture et du patrimoine dans les mêmes conditions et délais.

Le chef du service de l'Etat chargé de l'architecture et du patrimoine adresse un projet de décision à l'autorité compétente.
