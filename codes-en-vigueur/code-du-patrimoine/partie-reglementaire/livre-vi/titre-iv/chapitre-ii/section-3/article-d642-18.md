# Article D642-18

Dans les quinze jours qui suivent le dépôt de la demande et pendant la durée d'instruction de celle-ci, le maire procède à l'affichage en mairie d'un avis de dépôt de demande d'autorisation précisant les caractéristiques essentielles du projet, dans des conditions prévues par arrêté du ministre chargé des monuments historiques et des espaces protégés.
