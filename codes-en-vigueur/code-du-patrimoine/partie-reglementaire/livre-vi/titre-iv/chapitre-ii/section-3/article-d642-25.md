# Article D642-25

La décision mentionnée au premier alinéa de l'article D. 642-24 est notifiée au demandeur par lettre recommandée avec demande d'avis de réception postal, ou par transmission électronique.

Lorsque la décision est prise par le président de l'établissement public de coopération intercommunale, celui-ci en adresse copie au maire de la commune.

Lorsque l'autorité compétente est le maire ou le président de l'établissement public de coopération intercommunale, elle informe le demandeur de la date à laquelle la décision et le dossier ont été transmis au préfet ou à son délégué dans les conditions définies aux articles L. 2131-1 et L. 2131-2 du code général des collectivités territoriales.
