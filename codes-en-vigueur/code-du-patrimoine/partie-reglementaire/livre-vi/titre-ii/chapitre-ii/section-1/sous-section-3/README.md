# Sous-section 3 : Contrôle scientifique et technique

- [Article R622-18](article-r622-18.md)
- [Article R622-19](article-r622-19.md)
- [Article R622-20](article-r622-20.md)
- [Article R622-21](article-r622-21.md)
- [Article R622-22](article-r622-22.md)
- [Article R622-23](article-r622-23.md)
- [Article R622-24](article-r622-24.md)
- [Article R622-25](article-r622-25.md)
