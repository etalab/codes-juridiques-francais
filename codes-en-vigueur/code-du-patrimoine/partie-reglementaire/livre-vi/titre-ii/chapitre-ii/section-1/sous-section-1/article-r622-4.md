# Article R622-4

Le préfet soumet pour avis à la commission départementale des objets mobiliers les demandes de classement d'objets mobiliers dont il est saisi, après avoir vérifié le caractère complet du dossier, ainsi que les propositions de classement dont il prend l'initiative. Lorsqu'il estime que l'objet mobilier le justifie, le préfet saisit le ministre chargé de la culture d'une proposition de classement. Dans tous les cas, il informe le demandeur de sa décision.

Lorsque la demande ou la proposition de classement porte sur un orgue, le préfet la transmet au ministre chargé de la culture qui recueille l'avis de la Commission nationale des monuments historiques. Le préfet peut préalablement recueillir l'avis de la commission départementale des objets mobiliers.

Lorsque le ministre chargé de la culture est saisi par le préfet d'une demande ou d'une proposition de classement, il statue après avoir recueilli l'avis de la Commission nationale des monuments historiques. Il consulte également la Commission nationale des monuments historiques lorsqu'il prend l'initiative d'un classement. Il informe la commission, avant qu'elle ne rende son avis, de l'avis du propriétaire ou de l'affectataire domanial sur la proposition de classement.

Le ministre informe le préfet de l'avis de la commission et de sa décision.

Le ministre ne peut classer un objet n'appartenant pas à l'Etat qu'au vu d'un dossier contenant l'accord de son propriétaire sur la mesure de classement.
