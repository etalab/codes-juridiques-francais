# Article R622-61

La rémunération des missions de maîtrise d'œuvre exercées pour le compte de l'Etat mentionnées aux articles R. 622-59 et R. 622-60, ainsi que de celles exercées en application du cinquième alinéa de l'article R. 622-59, est calculée dans des conditions fixées par arrêté conjoint des ministres chargés de la culture et du budget.
