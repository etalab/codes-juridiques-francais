# Article R621-92

I.-La création d'un périmètre de protection adapté mentionné au cinquième alinéa de l'article L. 621-30 est proposée par l'architecte des Bâtiments de France et fait l'objet d'une instruction conduite sous l'autorité du préfet du département dans lequel se situe l'immeuble classé ou inscrit générant le périmètre de protection.

II.-La modification d'un périmètre de protection est proposée par l'architecte des Bâtiments de France en application du sixième alinéa de l'article L. 621-30, et fait l'objet d'une instruction qui est conduite :

-soit sous l'autorité du préfet du département dans lequel se situe l'immeuble classé ou inscrit générant le périmètre de protection ;

-soit, lorsque la modification du périmètre est effectuée conjointement à l'élaboration, la modification ou la révision d'un plan local d'urbanisme ou d'une carte communale, par l'autorité compétente en matière de plan local d'urbanisme ou de carte communale
