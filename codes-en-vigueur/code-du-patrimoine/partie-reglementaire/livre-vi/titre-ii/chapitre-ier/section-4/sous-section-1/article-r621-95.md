# Article R621-95

La décision de création d'un périmètre de protection adapté ou de modification d'un périmètre de protection est prise par un arrêté du préfet de département publié au recueil des actes administratifs de la préfecture.

Le préfet notifie l'arrêté aux maires des communes concernées et, le cas échéant, au président de l'établissement public de coopération intercommunale compétent en matière de plan local d'urbanisme ou de carte communale. Lorsque le territoire concerné est soumis à un plan local d'urbanisme ou à une carte communale, l'autorité compétente annexe le tracé des nouveaux périmètres à ce plan, dans les conditions prévues à l'
article L. 126-1 du code de l'urbanisme
.
