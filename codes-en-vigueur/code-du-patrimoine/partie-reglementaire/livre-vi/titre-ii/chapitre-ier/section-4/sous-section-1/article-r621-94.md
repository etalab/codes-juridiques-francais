# Article R621-94

Lorsque le projet de périmètre de protection est instruit à l'occasion de l'élaboration, de la modification ou de la révision d'un plan local d'urbanisme ou de l'élaboration ou de la révision d'une carte communale, le préfet peut saisir le préfet de région pour recueillir l'avis de la commission régionale du patrimoine et des sites. Le projet et l'avis de la commission régionale du patrimoine et des sites sont alors portés à la connaissance de la collectivité territoriale.

L'organe délibérant de la collectivité territoriale compétente émet un avis sur le projet de périmètre en même temps qu'il arrête le projet de plan local d'urbanisme, dans les conditions fixées par l'article L. 123-9 du code de l'urbanisme
. Lorsque cet avis est favorable, l'enquête publique prévue par l'article L. 123-10 du code de l'urbanisme porte à la fois sur le projet de plan local d'urbanisme et sur le projet de périmètre de protection.

Lors de l'élaboration d'une carte communale, l'organe délibérant de la collectivité territoriale compétente émet un avis sur le projet de périmètre de protection. Lorsque cet avis est favorable, l'enquête publique prévue par l'
article L. 124-2 du code de l'urbanisme
porte à la fois sur le projet de carte communale et sur le projet de périmètre de protection.
