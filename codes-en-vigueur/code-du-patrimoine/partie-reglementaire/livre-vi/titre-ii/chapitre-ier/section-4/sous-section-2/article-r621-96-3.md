# Article R621-96-3

I.-Lorsque le projet est situé dans le champ de visibilité d'un édifice classé ou inscrit au titre des monuments historiques ou d'un parc ou d'un jardin classé ou inscrit ne comportant pas d'édifice et dont le périmètre de protection a été délimité en application des cinquième et sixième alinéas de l'article L. 621-30, le dossier joint à la demande d'autorisation comprend un plan permettant de connaître la situation du terrain à l'intérieur de la commune et une notice indiquant les matériaux utilisés et les modes d'exécution des travaux. Il comprend, en outre :

1° Lorsque le projet a pour objet d'édifier ou de modifier une construction :

a) Un plan-masse coté dans les trois dimensions ainsi qu'une représentation de l'aspect extérieur de la construction faisant apparaître les modifications projetées ;

b) Lorsque les travaux projetés nécessitent la démolition de bâtiments soumis au régime du permis de démolir, la justification du dépôt de la demande de permis de démolir ;

2° Lorsque le projet a pour objet la réalisation ou la modification d'une infrastructure ou un aménagement des sols :

a) Un plan-masse faisant apparaître les cotes de niveau du terrain avant et après travaux, s'il y a lieu, les constructions, la végétation et les éléments paysagers existants ainsi que le traitement des constructions, clôtures, végétations ou aménagements situés en limite de terrain, lorsque les travaux portent sur l'aménagement ou la modification du terrain ;

b) Un plan de coupe longitudinale et des plans de coupe transversale précisant l'implantation de l'infrastructure par rapport au profil du terrain et indiquant, lorsque les travaux ont pour effet de modifier le profil du terrain, l'état initial et l'état futur ;

c) Une notice exposant les partis retenus pour assurer l'insertion du projet dans son environnement et la prise en compte des paysages accompagnée de deux documents photographiques permettant de situer le terrain respectivement dans l'environnement proche et, sauf si le demandeur justifie qu'aucune photographie de loin n'est possible, dans le paysage lointain. Les points et les angles des prises de vue sont reportés sur le plan de situation et le plan de masse ;

d) Un plan faisant apparaître le traitement des espaces libres, notamment les plantations à conserver ou à créer ainsi que l'organisation et l'aménagement des accès au terrain, aux constructions et aux aires de stationnement.

Lorsque les travaux projetés nécessitent une autorisation de défrichement en application des articles L. 214-13 et L. 341-3 du nouveau code forestier, la demande d'autorisation est complétée par la copie de la lettre par laquelle le préfet fait connaître au demandeur que son dossier de demande d'autorisation de défrichement est complet.

II.-Lorsque le projet porte sur un immeuble adossé à un immeuble classé, le dossier joint à la demande d'autorisation comprend, outre les pièces mentionnées au I, les documents permettant d'apprécier l'impact architectural et technique des travaux sur l'immeuble classé.
