# Article R621-96-9

Lorsque le dossier est complet, le silence gardé par le préfet pendant plus de quarante jours à compter du dépôt de la demande vaut décision de rejet, conformément au sixième alinéa de l'article L. 621-32.

Lorsque le dossier est incomplet, le préfet avise le demandeur, dans un délai d'un mois à compter de l'enregistrement de la demande, des pièces manquant à son dossier. Dans ce cas, le délai mentionné à l'alinéa précédent court à compter du dépôt de ces pièces. A défaut pour le demandeur de déposer ces pièces auprès du maire dans un délai de trois mois à compter de la réception de cet avis, la demande est réputée rejetée.
