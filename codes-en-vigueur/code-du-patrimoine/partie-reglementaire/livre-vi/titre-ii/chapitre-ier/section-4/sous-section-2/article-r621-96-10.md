# Article R621-96-10

L'architecte des Bâtiments de France dispose d'un délai d'un mois à compter de sa saisine pour faire connaître son avis au préfet. A défaut, il est réputé avoir émis un avis favorable.

S'il estime que le dossier est incomplet, il en avise le préfet, dans le délai de quinze jours à compter de sa saisine. Le préfet fait alors application du deuxième alinéa de l'article R. 621-96-9.
