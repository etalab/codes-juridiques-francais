# Article R621-46

En application de l'article L. 621-12, lorsque la conservation d'un immeuble classé au titre des monuments historiques est gravement compromise par l'inexécution de travaux de réparation ou d'entretien, le ministre chargé de la culture fait établir un rapport constatant la nécessité des travaux à réaliser, décrivant et estimant ces travaux et recueille l'avis de la Commission nationale des monuments historiques.

L'arrêté de mise en demeure donne au propriétaire un délai de quinze jours pour choisir le maître d'œuvre chargé d'assurer l'exécution des travaux. A défaut, le ministre chargé de la culture procède à sa désignation.

L'arrêté fixe les délais dans lesquels, à compter de la date d'approbation du projet, les travaux devront être entrepris et exécutés.
