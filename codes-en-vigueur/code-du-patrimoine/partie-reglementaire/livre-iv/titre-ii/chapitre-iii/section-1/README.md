# Section 1 : Acquisitions

- [Article D423-2](article-d423-2.md)
- [Article D423-4](article-d423-4.md)
- [Article D423-5](article-d423-5.md)
- [Article R423-1](article-r423-1.md)
- [Article R423-3](article-r423-3.md)
