# Sous-section 1 : Dispositions générales

- [Article D451-1](article-d451-1.md)
- [Article R451-2](article-r451-2.md)
