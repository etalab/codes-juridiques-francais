# Section 1 : Acquisitions

- [Sous-section 1 : Dispositions générales](sous-section-1)
- [Sous-section 2 : Dispositions applicables à la Commission scientifique nationale  des musées de France](sous-section-2)
- [Sous-section 3 : Dispositions particulières aux commissions scientifiques  régionales ou interrégionales](sous-section-3)
- [Sous-section 4 : Dispositions communes aux commissions scientifiques](sous-section-4)
