# Article R310-9

Le contrôle technique de l'Etat sur les bibliothèques des communes ou des établissements publics de coopération intercommunale porte sur les conditions de constitution, de gestion, de traitement, de conservation et de communication des collections et des ressources documentaires et d'organisation des locaux.

Il est destiné à assurer la sécurité des fonds, la qualité des collections, leur renouvellement, leur caractère pluraliste et diversifié, l'accessibilité des services pour tous les publics, la qualité technique des bibliothèques, la compatibilité des systèmes de traitement, la conservation des collections dans le respect des exigences techniques relatives à la communication, l'exposition, la reproduction, l'entretien et le stockage en magasin.
