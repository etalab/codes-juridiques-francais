# Article R310-4

Les collections de l'Etat déposées dans les bibliothèques municipales, dont les communes ou les établissements publics de coopération intercommunale ont l'usage et doivent assurer la conservation, sont placées sous la surveillance des communes ou des établissements publics de coopération intercommunale.

Ces collections peuvent être retirées par le ministre chargé des bibliothèques en cas d'insuffisance de soins ou d'abus de la part des communes ou des établissements publics de coopération intercommunale.
