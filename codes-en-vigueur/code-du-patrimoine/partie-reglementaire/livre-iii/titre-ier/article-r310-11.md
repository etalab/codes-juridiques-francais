# Article R310-11

Les communes ou les établissements publics de coopération intercommunale informent le préfet de tout projet de construction, d'extension ou d'aménagement de bâtiments à usage de bibliothèques ainsi que des projets de travaux dans ces bâtiments.

Le préfet dispose d'un délai de deux mois pour faire connaître l'avis technique de l'Etat à la collectivité territoriale ou à l'établissement public de coopération intercommunale intéressé. Les travaux ne peuvent commencer avant la transmission de cet avis ou l'expiration de ce délai.
