# Chapitre II : Utilisation des détecteurs de métaux

- [Article R542-1](article-r542-1.md)
- [Article R542-2](article-r542-2.md)
