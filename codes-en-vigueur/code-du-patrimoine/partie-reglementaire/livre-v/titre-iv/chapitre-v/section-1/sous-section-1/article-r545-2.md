# Article R545-2

Le Conseil national de la recherche archéologique est compétent pour les questions relatives aux recherches archéologiques sur le territoire national, sous réserve des compétences attribuées aux commissions interrégionales de la recherche archéologique définies à la section 2 du présent chapitre.

Le Conseil national de la recherche archéologique est consulté sur toute question intéressant la recherche archéologique que lui soumet le ministre chargé de la culture.

Il examine et il propose toute mesure relative à l'étude scientifique du patrimoine archéologique et à son inventaire, à la publication et à la diffusion des résultats de la recherche ainsi qu'à la protection, à la conservation et à la mise en valeur de ce patrimoine.

A ce titre, le Conseil national de la recherche archéologique :

1° Propose au ministre chargé de la culture les objectifs généraux de la recherche, assure une mission de prospective scientifique ainsi que l'harmonisation nationale des programmations interrégionales et émet des avis sur les principes, les méthodes et les normes de la recherche en archéologie ;

2° Peut être consulté sur tout dossier transmis au ministre chargé de la culture par le préfet d'une région, siège de commission interrégionale de la recherche archéologique, en particulier sur les dossiers concernant plusieurs interrégions ;

3° Contribue à la mise en place de réseaux et de partenariats scientifiques aux niveaux national et international ;

4° Participe à la réflexion en matière d'archéologie dans le cadre de la coopération européenne et internationale et en apprécie les effets, notamment dans les domaines de la formation et des échanges de savoir-faire ;

5° Procède à toute évaluation scientifique à la demande du ministre chargé de la culture ;

6° Etablit chaque année la liste des experts, prévue à l'article R. 531-12, compétents pour déterminer la valeur d'objets provenant de fouilles archéologiques et de découvertes fortuites.

Il émet, en outre, les avis mentionnés aux articles R. 522-11, R. 541-4 et R. 541-5.
