# Article R545-7

Le Conseil national de la recherche archéologique peut déléguer ses attributions mentionnées à l'article R. 522-11 à la délégation permanente prévue à l'article R. 545-8.
