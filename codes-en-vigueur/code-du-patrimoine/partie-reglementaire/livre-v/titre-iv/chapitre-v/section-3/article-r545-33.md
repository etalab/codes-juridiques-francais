# Article R545-33

Le président de l'Institut national de recherches archéologiques préventives préside le comité technique paritaire et le      comité d'hygiène, de sécurité et des conditions de travail, et peut s'y faire représenter par le directeur général.
