# Article R545-36

Le conseil d'administration se réunit au moins deux fois par an. Il peut être également réuni par son président à la demande du tiers au moins de ses membres ou de l'un des ministres chargés de la tutelle.

Le conseil ne peut valablement délibérer que si la moitié au moins de ses membres sont présents ou représentés. Si le quorum n'est pas atteint, le conseil est à nouveau convoqué dans un délai de quinze jours sur le même ordre du jour. Il délibère alors valablement, quel que soit le nombre des membres présents.

Les délibérations sont prises à la majorité des membres présents. En cas de partage égal des voix, celle du président est prépondérante.
