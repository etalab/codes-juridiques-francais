# Section 2 : Dispositions relatives aux détecteurs de métaux

- [Article R544-3](article-r544-3.md)
- [Article R544-4](article-r544-4.md)
