# Sous-section 3 : Dispositions communes

- [Article R522-9](article-r522-9.md)
- [Article R522-10](article-r522-10.md)
- [Article R522-11](article-r522-11.md)
- [Article R522-12](article-r522-12.md)
- [Article R522-13](article-r522-13.md)
