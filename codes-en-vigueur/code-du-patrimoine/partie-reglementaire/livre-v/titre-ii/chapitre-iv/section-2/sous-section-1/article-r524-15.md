# Article R524-15

Les membres de la commission exercent leurs fonctions à titre gratuit. Toutefois, leurs frais de séjour et de déplacement supportés à l'occasion des réunions de la commission sont pris en charge dans les conditions prévues par la réglementation applicable aux personnels civils de l'Etat.

Les crédits nécessaires au fonctionnement de la commission, et notamment à la prise en charge des frais de séjour et de déplacement de ses membres, sont inscrits au budget du ministère chargé de la culture.
