# Article R523-58

La décision de l'arbitre mentionnée aux articles R. 523-55 et R. 523-57 peut être contestée devant le Conseil d'Etat.
