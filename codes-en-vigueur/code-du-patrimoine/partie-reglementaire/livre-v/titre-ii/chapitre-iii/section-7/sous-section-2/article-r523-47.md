# Article R523-47

Lorsque le déroulement des opérations fait apparaître la nécessité d'une modification substantielle du projet scientifique d'intervention, un projet révisé est soumis au préfet de région, qui dispose d'un délai de quinze jours pour l'approuver ou en demander la modification. A défaut de notification d'une décision dans ce délai, le projet révisé est réputé refusé.

En cas de découvertes survenues pendant l'opération conduisant à remettre en cause les résultats du diagnostic et les données scientifiques du cahier des charges, le préfet de région peut formuler des prescriptions complémentaires.

Les modifications et prescriptions complémentaires mentionnées aux alinéas précédents ne peuvent conduire à modifier l'économie générale du contrat mentionné à l'article R. 523-44.
