# Article R523-39

Lorsque le préfet de région prescrit, dans les conditions prévues par l'article R. 523-19, la réalisation d'une fouille, il assortit son arrêté de prescription d'un cahier des charges scientifique qui :

1° Définit les objectifs, les données scientifiques ainsi que les principes méthodologiques et techniques de l'intervention et des études à réaliser ;

2° Précise les qualifications du responsable scientifique de l'opération et, le cas échéant, celles des spécialistes nécessaires à l'équipe d'intervention ;

3° Définit la nature prévisible des travaux nécessités par l'opération archéologique. Le cahier des charges scientifique en indique, le cas échéant, la durée minimale et fournit une composition indicative de l'équipe ;

4° Détermine les mesures à prendre pour la conservation préventive des vestiges mis au jour ;

5° Fixe le délai limite pour la remise du rapport final.
