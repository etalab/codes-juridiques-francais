# Sous-section 2 : La désignation de l'opérateur chargé du diagnostic

- [Article R523-24](article-r523-24.md)
- [Article R523-25](article-r523-25.md)
- [Article R523-26](article-r523-26.md)
- [Article R523-27](article-r523-27.md)
- [Article R523-28](article-r523-28.md)
- [Article R523-29](article-r523-29.md)
