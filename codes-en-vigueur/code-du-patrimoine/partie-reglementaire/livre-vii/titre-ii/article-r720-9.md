# Article R720-9

Les articles R. 612-1 à R. 612-16 ne sont pas applicables à Saint-Pierre-et-Miquelon.
