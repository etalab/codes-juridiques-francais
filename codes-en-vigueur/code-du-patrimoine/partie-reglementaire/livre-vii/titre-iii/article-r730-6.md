# Article R730-6

La commission régionale du patrimoine et des sites comprend à Mayotte vingt membres :

1° Six membres de droit :

a) Le préfet de Mayotte ;

b) Le directeur des affaires culturelles ;

c) Le directeur de l'environnement, de l'aménagement et du logement ;

d) Le chef du service chargé des monuments historiques compétent à Mayotte ;

e) Le commandant du groupement de gendarmerie ;

f) Le conservateur départemental des antiquités et objets d'art ;

2° Quatorze membres nommés par le préfet de Mayotte pour une durée de quatre ans :

a) Deux fonctionnaires de l'Etat, compétents dans le domaine des monuments historiques, de l'archéologie ou de l'inventaire général du patrimoine culturel ;

b) Cinq titulaires d'un mandat électif national ou local ;

c) Cinq personnalités qualifiées dans le domaine de l'architecture, de l'urbanisme, du paysage, du patrimoine ou de l'ethnologie ;

d) Deux représentants d'associations ou de fondations ayant pour objet de favoriser la connaissance, la protection et la conservation du patrimoine.
