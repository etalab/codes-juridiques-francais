# Article R790-4

Pour l'application de l'article R. 310-4, les bibliothèques de Saint-Martin sont assimilées aux bibliothèques municipales.
