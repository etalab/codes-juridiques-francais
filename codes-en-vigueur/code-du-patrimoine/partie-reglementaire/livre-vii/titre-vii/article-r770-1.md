# Article R770-1

Les dispositions applicables dans les Terres australes et antarctiques françaises en vertu du présent titre sont celles en vigueur dans leur rédaction résultant du décret n° 2015-318 du 19 mars 2015.
