# Article R710-8

Pour l'application en Guadeloupe, en Guyane, en Martinique et à La Réunion, de l'article R. 612-6 :

1° Au premier alinéa, le mot : " onze " est remplacé par le mot : " huit " ;

2° Le 3° est ainsi rédigé :

" Trois personnalités qualifiées choisies et désignées par le représentant de l'Etat parmi les membres de la commission mentionnés au c ou au d du 2° de l'article R. 710-6. "
