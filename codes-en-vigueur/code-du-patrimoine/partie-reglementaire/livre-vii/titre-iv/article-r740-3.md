# Article R740-3

Les articles R. 123-1 à R. 123-8 et R. 131-1 à R. 133-1 sont applicables en Nouvelle-Calédonie.
