# Article R780-15

A Saint-Barthélemy, la délégation permanente de la commission régionale du patrimoine et des sites prévue à l'article R. 612-2 comprend sept membres :

1° Deux membres de droit :

a) Le directeur des affaires culturelles de Guadeloupe ;

b) Le chef du service chargé des monuments historiques de Guadeloupe ;

2° Cinq membres nommés par le représentant de l'Etat :

a) Deux membres désignés parmi les titulaires d'un mandat électif national ou local mentionnés à l'article R. 780-14 ;

b) Trois membres désignés parmi les personnalités mentionnées au c du 2° de l'article R. 710-6 et au dernier alinéa de l'article R. 780-14. ;

3° Les deuxième à sixième alinéas de l'article R. 790-14 sont remplacés par les six alinéas suivants :

1° Deux membres de droit :

a) Le directeur des affaires culturelles de Guadeloupe ;

b) Le chef du service chargé des monuments historiques de Guadeloupe ;

2° Cinq membres nommés par le représentant de l'Etat :

a) Deux membres désignés parmi les titulaires d'un mandat électif national ou local mentionnés à l'article R. 790-13 ;

b) Trois membres désignés parmi les personnalités mentionnées au c du 2° de l'article R. 710-6 et au dernier alinéa de l'article R. 790-13.
