# Article R112-4

Les dispositions de l'article R. 112-3 s'appliquent aux biens culturels de toute nature et de toute époque présentant à un titre quelconque une valeur artistique ou historique qui les rattache au patrimoine culturel national, que ces biens culturels appartiennent à l'Etat, à une collectivité publique ou à une personne de droit public ou privé et qu'ils aient ou non été classés ou inscrits au titre des monuments historiques ou classés comme archives historiques.
