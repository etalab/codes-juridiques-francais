# Chapitre Ier : Régime de circulation des biens culturels

- [Section 1 : Délivrance des certificats d'exportation  des biens culturels](section-1)
- [Section 2 : Sortie temporaire des biens culturels  et des trésors nationaux](section-2)
- [Section 3 : Sortie illicite des biens culturels](section-3)
- [Section 4 : Exportation des biens culturels et exportation temporaire  des trésors nationaux vers un Etat non membre  de l'Union européenne](section-4)
- [Section 5 : Commission consultative des trésors nationaux](section-5)
- [Article R111-1](article-r111-1.md)
- [Article R111-2](article-r111-2.md)
- [Article R111-3](article-r111-3.md)
