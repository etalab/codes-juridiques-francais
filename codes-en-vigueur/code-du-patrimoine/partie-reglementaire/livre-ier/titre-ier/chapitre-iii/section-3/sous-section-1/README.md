# Sous-section 1 : Comité consultatif des prêts et dépôts d'œuvres et d'objets d'art  inscrits sur l'inventaire du Fonds national d'art contemporain

- [Article D113-24](article-d113-24.md)
- [Article D113-25](article-d113-25.md)
- [Article R113-26](article-r113-26.md)
