# Section 3 : Régime financier

- [Article R142-22](article-r142-22.md)
- [Article R142-23](article-r142-23.md)
- [Article R142-25](article-r142-25.md)
- [Article R142-26](article-r142-26.md)
