# Article R212-30

La demande de renouvellement de l'agrément est faite au plus tard six mois avant le terme de la période d'agrément, dans les mêmes conditions que celles applicables à la demande initiale. Il est statué sur cette demande de renouvellement selon la même procédure.
