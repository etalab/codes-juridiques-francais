# Paragraphe 2 : Dépôt des archives communales

- [Article R212-57](article-r212-57.md)
- [Article R212-58](article-r212-58.md)
- [Article R212-59](article-r212-59.md)
- [Article R212-60](article-r212-60.md)
- [Article R212-61](article-r212-61.md)
