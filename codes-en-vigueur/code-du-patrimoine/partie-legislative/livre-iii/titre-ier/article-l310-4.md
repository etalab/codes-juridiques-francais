# Article L310-4

Le classement d'une bibliothèque ne peut être modifié sans consultation préalable de la commune intéressée.
