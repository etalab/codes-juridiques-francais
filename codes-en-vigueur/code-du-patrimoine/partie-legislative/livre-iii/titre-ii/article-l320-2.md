# Article L320-2

Les bibliothèques centrales de prêt sont transférées aux départements. Elles sont dénommées bibliothèques départementales de prêt.
