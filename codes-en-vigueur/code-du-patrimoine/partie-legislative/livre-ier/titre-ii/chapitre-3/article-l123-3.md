# Article L123-3

Les conditions d'application des articles L. 123-1 et L. 123-2 sont fixées par décret en Conseil d'Etat.
