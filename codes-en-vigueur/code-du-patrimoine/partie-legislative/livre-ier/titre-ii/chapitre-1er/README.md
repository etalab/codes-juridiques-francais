# Chapitre 1er : Acquisition de biens culturels présentant le caractère de trésor national et faisant l'objet d'un refus de certificat d'exportation.

- [Article L121-1](article-l121-1.md)
- [Article L121-2](article-l121-2.md)
- [Article L121-3](article-l121-3.md)
- [Article L121-4](article-l121-4.md)
