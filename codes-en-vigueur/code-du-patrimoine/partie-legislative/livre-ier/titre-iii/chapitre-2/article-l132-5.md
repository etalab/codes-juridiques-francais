# Article L132-5

L'artiste-interprète, le producteur de phonogrammes ou de vidéogrammes ou l'entreprise de communication audiovisuelle ne peut interdire la reproduction et la communication au public des documents mentionnés à l'article L. 131-2 dans les conditions prévues à l'article L. 132-4.
