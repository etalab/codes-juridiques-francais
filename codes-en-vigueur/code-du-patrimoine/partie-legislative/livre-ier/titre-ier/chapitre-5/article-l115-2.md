# Article L115-2

La commission scientifique nationale des collections comprend un député et un sénateur nommés par leur assemblée respective, des représentants de l'Etat et des collectivités territoriales, des professionnels de la conservation des biens concernés et des personnalités qualifiées.

Un décret en Conseil d'Etat précise sa composition et fixe ses modalités de fonctionnement.
