# Article L112-25

Un décret en Conseil d'Etat fixe les conditions d'application du présent chapitre.
