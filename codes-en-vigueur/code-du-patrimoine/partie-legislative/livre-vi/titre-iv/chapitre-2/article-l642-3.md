# Article L642-3

La mise à l'étude de la création ou de la révision de l'aire de mise en valeur de l'architecture et du patrimoine est décidée par délibération de l'organe délibérant de l'autorité mentionnée au premier alinéa de l'article L. 642-1. La délibération mentionne les modalités de la concertation prévue à l'article L. 300-2 du code de l'urbanisme.

Le projet de création ou de révision de l'aire de mise en valeur de l'architecture et du patrimoine est arrêté par délibération de cette autorité. Le projet arrêté est soumis à l'avis de la commission régionale du patrimoine et des sites prévue à l'article L. 612-1 du présent code.

Ce projet donne lieu à un examen conjoint des personnes publiques mentionnées à l'article L. 123-14-2 du code de l'urbanisme.

Il fait l'objet d'une enquête publique conduite par les autorités compétentes concernées. L'organe délibérant de l'autorité mentionnée au premier alinéa de l'article L. 642-1 du présent code peut, par délibération, désigner à cette fin l'une de ces autorités compétentes concernées.

Lorsque le projet n'est pas compatible avec les dispositions du plan local d'urbanisme, l'aire de mise en valeur de l'architecture et du patrimoine ne peut être créée que si celui-ci a été mis en compatibilité avec ses dispositions selon la procédure définie à l'article L. 123-14-2 du code de l'urbanisme.

Après accord du préfet, l'aire de mise en valeur de l'architecture et du patrimoine est créée ou révisée par délibération de l'autorité mentionnée au premier alinéa de l'article L. 642-1 du présent code. Lorsque l'enquête publique précitée a porté à la fois sur l'aire et sur un plan local d'urbanisme, l'acte portant création ou révision de l'aire prononce également la révision ou la modification du plan local d'urbanisme.
