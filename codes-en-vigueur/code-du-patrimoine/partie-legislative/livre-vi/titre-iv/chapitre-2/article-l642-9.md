# Article L642-9

Les zones de protection créées en application des articles 17 à 20 et 28 de la loi du 2 mai 1930 ayant pour objet de réorganiser la protection des monuments naturels et des sites de caractère artistique, historique, scientifique, légendaire ou pittoresque continuent à produire leurs effets jusqu'à leur suppression ou leur remplacement par des zones de protection du patrimoine architectural, urbain et paysager ou des aires de mise en valeur de l'architecture et du patrimoine.
