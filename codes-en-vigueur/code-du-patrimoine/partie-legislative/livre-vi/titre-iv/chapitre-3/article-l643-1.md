# Article L643-1

Les règles fiscales relatives à la détermination du revenu net des personnes propriétaires d'un immeuble situé en secteur sauvegardé ou en zone de protection du patrimoine architectural, urbain et paysager sont fixées au b ter du 1° du I de l'article 31 et au 3° du I de l'article 156 du code général des impôts.
