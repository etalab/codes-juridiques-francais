# Article L630-1

Les règles relatives à la protection des monuments naturels et des sites sont fixées par les articles L. 341-1 à L. 341-22 du code de l'environnement ci-après reproduits :

" Section 1

" Inventaire et classement

" Art. L. 341-1. - Il est établi dans chaque département une liste des monuments naturels et des sites dont la conservation ou la préservation présente, au point de vue artistique, historique, scientifique, légendaire ou pittoresque, un intérêt général.

"Après l'enquête publique réalisée conformément au chapitre III du titre II du livre Ier, l'inscription sur la liste est prononcée par arrêté du ministre chargé des sites et, en Corse, par délibération de l'Assemblée de Corse après avis du représentant de l'Etat.

"L'inscription entraîne, sur les terrains compris dans les limites fixées par l'arrêté, l'obligation pour les intéressés de ne pas procéder à des travaux autres que ceux d'exploitation courante en ce qui concerne les fonds ruraux et d'entretien normal en ce qui concerne les constructions sans avoir avisé, quatre mois d'avance, l'administration de leur intention."

" Art. L. 341-2. - Les monuments naturels et les sites inscrits ou non sur la liste dressée par la commission départementale peuvent être classés dans les conditions et selon les distinctions établies par la présente section.

"Lorsque la commission supérieure des sites, perspectives et paysages est saisie directement d'une demande de classement, celle-ci est renvoyée à la commission départementale aux fins d'instruction et, le cas échéant, de proposition de classement. En cas d'urgence, le ministre chargé des sites fixe à la commission départementale un délai pour émettre son avis. Faute par elle de se prononcer dans ce délai, le ministre consulte la commission supérieure et donne à la demande la suite qu'elle comporte.

"Dans les zones de montagne, la décision de classement est prise après consultation du comité de massif concerné."

" Art. L. 341-3. - Le projet de classement est soumis à une enquête publique réalisée conformément au chapitre III du titre II du livre Ier."

" Art. L. 341-4. - Le monument naturel ou le site compris dans le domaine public ou privé de l'Etat est classé par arrêté du ministre chargé des sites, en cas d'accord avec le ministre dans les attributions duquel le monument naturel ou le site se trouve placé ainsi qu'avec le ministre chargé du domaine.

" Il en est de même toutes les fois qu'il s'agit de classer un lac ou un cours d'eau susceptible de produire une puissance permanente de 50 kilowatts d'énergie électrique.

" Dans le cas contraire, le classement est prononcé par décret en Conseil d'Etat. "

" Art. L. 341-5. - Le monument naturel ou le site compris dans le domaine public ou privé d'un département ou d'une commune ou appartenant à un établissement public est classé par arrêté du ministre chargé des sites s'il y a consentement de la personne publique propriétaire.

" Dans le cas contraire, le classement est prononcé, après avis de la Commission supérieure des sites, perspectives et paysages, par décret en Conseil d'Etat. "

" Art. L. 341-6. - Le monument naturel ou le site appartenant à toute autre personne que celles énumérées aux articles L. 341-4 et L. 341-5 est classé par arrêté du ministre chargé des sites, s'il y a consentement du propriétaire. L'arrêté détermine les conditions du classement.

" A défaut du consentement du propriétaire, le classement est prononcé, après avis de la commission supérieure, par décret en Conseil d'Etat. Le classement peut donner droit à indemnité au profit du propriétaire s'il entraîne une modification à l'état ou à l'utilisation des lieux déterminant un préjudice direct, matériel et certain.

" La demande d'indemnité doit être produite dans le délai de six mois à dater de la mise en demeure faite au propriétaire de modifier l'état ou l'utilisation des lieux en application des prescriptions particulières de la décision de classement. A défaut d'accord amiable, l'indemnité est fixée par le juge de l'expropriation.

" Si le Gouvernement entend ne pas donner suite au classement d'office dans les conditions ainsi fixées, il peut, à tout moment de la procédure et au plus tard dans le délai de trois mois à compter de la notification de la décision judiciaire, abroger le décret de classement.

" Le classement d'un lac ou d'un cours d'eau pouvant produire une énergie électrique permanente d'au moins 50 kilowatts ne peut être prononcé qu'après avis des ministres intéressés. Cet avis doit être formulé dans le délai de trois mois, à l'expiration duquel il peut être passé outre.

" En cas d'accord avec les ministres intéressés, le classement peut être prononcé par arrêté du ministre chargé des sites. Dans le cas contraire, il est prononcé par décret en Conseil d'Etat. "

" Art. L. 341-7. - A compter du jour où l'administration chargée des sites notifie au propriétaire d'un monument naturel ou d'un site son intention d'en poursuivre le classement, aucune modification ne peut être apportée à l'état des lieux ou à leur aspect pendant un délai de douze mois, sauf autorisation spéciale et sous réserve de l'exploitation courante des fonds ruraux et de l'entretien normal des constructions.

" Lorsque l'identité ou le domicile du propriétaire sont inconnus, la notification est valablement faite au maire qui en assure l'affichage et, le cas échéant, à l'occupant des lieux. "

" Art. L. 341-8. - Tout arrêté ou décret prononçant un classement est publié, par les soins de l'administration chargée des sites, au fichier immobilier.

Cette publication, qui ne donne lieu à aucune perception au profit du Trésor, est faite dans les formes et de la manière prescrites par les lois et règlements concernant la publicité foncière."

" Art. L. 341-9. - Les effets du classement suivent le monument naturel ou le site classé, en quelques mains qu'il passe.

" Quiconque aliène un monument naturel ou un site classé est tenu de faire connaître à l'acquéreur l'existence de ce classement.

" Toute aliénation d'un monument naturel ou d'un site classé doit, dans les quinze jours de sa date, être notifiée au ministre chargé des sites par celui qui l'a consentie. "

" Art. L. 341-10. - Les monuments naturels ou les sites classés ne peuvent ni être détruits ni être modifiés dans leur état ou leur aspect sauf autorisation spéciale. "

" Art. L. 341-11. - Sur le territoire d'un site classé au titre du présent chapitre, il est fait obligation d'enfouissement des réseaux électriques ou téléphoniques ou, pour les lignes électriques d'une tension inférieure à 19 000 volts, d'utilisation de techniques de réseaux torsadés en façade d'habitation, lors de la création de lignes électriques nouvelles ou de réseaux téléphoniques nouveaux.

" Lorsque des nécessités techniques impératives ou des contraintes topographiques rendent l'enfouissement impossible, ou bien lorsque les impacts de cet enfouissement sont jugés supérieurs à ceux d'une pose de ligne aérienne, il peut être dérogé à titre exceptionnel à cette interdiction par arrêté conjoint du ministre chargé de l'énergie ou des télécommunications et du ministre chargé de l'environnement. "

" Art. L. 341-12. - A compter du jour où l'administration chargée des sites notifie au propriétaire d'un monument naturel ou d'un site non classé son intention d'en poursuivre l'expropriation, tous les effets du classement s'appliquent de plein droit à ce monument naturel ou à ce site. Ils cessent de s'appliquer si la déclaration d'utilité publique n'intervient pas dans les douze mois de cette notification. Lorsque l'utilité publique a été déclarée, l'immeuble peut être classé sans autre formalité par arrêté du ministre chargé des sites. "

" Art. L. 341-13. - Le déclassement total ou partiel d'un monument ou d'un site classé est prononcé, après avis de la commission supérieure des sites, par décret en Conseil d'Etat. Le déclassement est notifié aux intéressés et publié au fichier immobilier, dans les mêmes conditions que le classement.

Le décret de déclassement détermine, sur avis conforme du Conseil d'Etat, s'il y a lieu ou non à la restitution de l'indemnité prévue à l'article L. 341-6. "

" Art. L. 341-14. - Aucun monument naturel ou site classé ou proposé pour le classement ne peut être compris dans une enquête aux fins d'expropriation pour cause d'utilité publique qu'après que le ministre chargé des sites a été appelé à présenter ses observations.

" Nul ne peut acquérir par prescription, sur un monument naturel ou sur un site classé, de droit de nature à modifier son caractère ou à changer l'aspect des lieux.

" Aucune servitude ne peut être établie par convention sur un monument naturel ou un site classé qu'avec l'agrément du ministre chargé des sites. "

" Art. L. 341-15. - La liste des sites et monuments naturels classés est tenue à jour. Dans le courant du premier trimestre de chaque année est publiée au Journal officiel la nomenclature des monuments naturels et des sites classés ou protégés au cours de l'année précédente. "

" Section 2

" Organismes

" Art. L. 341-16. - Une commission départementale compétente en matière de nature, de paysages et de sites siège dans chaque département.

" Cette commission est présidée par le représentant de l'Etat dans le département. Lorsqu'elle intervient dans les cas prévus aux articles L. 111-1-4, L. 122-2, L. 145-3, L. 145-5, L. 145-11, L. 146-4, L. 146-6, L. 146-6-1, L. 146-7 et L. 156-2 du code de l'urbanisme, elle siège dans une formation comprenant des représentants de l'Etat, des représentants élus des collectivités territoriales et des établissements publics de coopération intercommunale et des personnalités qualifiées en matière de sciences de la nature ou de protection des sites ou du cadre de vie.

" En Corse, les attributions dévolues à la commission des sites, perspectives et paysages sont exercées par le conseil des sites de Corse prévu à l'article L. 4421-4 du code général des collectivités territoriales. "

" Art. L. 341-17. - Une commission supérieure des sites, perspectives et paysages est placée auprès du ministre chargé des sites.

" Cette commission, présidée par le ministre chargé des sites, est composée de représentants des ministres concernés, de députés et de sénateurs désignés par chacune des assemblées, de personnalités qualifiées en matière de protection des sites, du cadre de vie et des sciences de la nature désignées par le ministre chargé des sites. "

" Art. L. 341-18. - Un décret en Conseil d'Etat fixe les conditions d'application du présent chapitre, notamment la composition, le mode de désignation et les modalités de fonctionnement des commissions prévues aux articles L. 341-16 et L. 341-17. "

" Section 3

" Dispositions pénales

" Art. L. 341-19. - I. - Est puni de six mois d'emprisonnement et de 30 000 euros d'amende :

"1° Le fait de procéder à des travaux sur un monument naturel ou un site inscrit sans en aviser l'administration dans les conditions prévues au dernier alinéa de l'article L. 341-1 ;

"2° Le fait d'aliéner un monument naturel ou un site classé sans faire connaître à l'acquéreur l'existence du classement ou sans notifier cette aliénation à l'administration dans les conditions prévues à l'article L. 341-9 ;

"3° Le fait d'établir une servitude sur un monument naturel ou un site classé sans l'agrément de l'administration dans les conditions prévues à l'article L. 341-14.

"II. - Est puni d'un an d'emprisonnement et de 150 000 euros d'amende le fait de modifier l'état ou l'aspect d'un monument naturel ou d'un site en instance de classement ou classé, en méconnaissance des prescriptions édictées par les autorisations prévues aux articles L. 341-7 et L. 341-10.

"III. - Est puni de deux ans d'emprisonnement et de 300 000 euros d'amende :

"1° Le fait de modifier l'état ou l'aspect d'un monument naturel ou d'un site en instance de classement sans l'autorisation prévue à l'article L. 341-7 ;

"2° Le fait de détruire un monument naturel ou un site classé ou d'en modifier l'état ou l'aspect sans l'autorisation prévue à l'article L. 341-10 ;

"3° Le fait de ne pas se conformer aux prescriptions fixées par un décret de création d'une zone de protection pris en application de l'article 19 de la loi du 2 mai 1930 ayant pour objet de réorganiser la protection des monuments naturels et des sites de caractère artistique, historique, scientifique, légendaire ou pittoresque et continuant à produire ses effets en application de l'article L. 642-9 du code du patrimoine."

" Art. L. 341-20. - Outre les officiers et agents de police judiciaire et les inspecteurs de l'environnement mentionnés à l'article L. 172-1, sont habilités à rechercher et constater les infractions au présent titre :

"1° Les agents des services de l'Etat chargés des forêts commissionnés à raison de leurs compétences en matière forestière et assermentés à cet effet ;

"2° Les agents de l'Office national des forêts commissionnés à raison de leurs compétences en matière forestière et assermentés à cet effet ;

"3° Les gardes du littoral mentionnés à l'article L. 322-10-1, agissant dans les conditions prévues à cet article ;

"4° Les agents des réserves naturelles mentionnés à l'article L. 332-20, agissant dans les conditions prévues à cet article."

" Art. L. 341-21. - (Abrogé).

" Art. L. 341-22. - Les dispositions du présent chapitre sont applicables aux monuments naturels et aux sites régulièrement classés avant le 2 mai 1930 conformément aux dispositions de la loi du 21 avril 1906 organisant la protection des sites et monuments naturels de caractère artistique. "
