# Article L612-1

La commission régionale du patrimoine et des sites, placée auprès du représentant de l'Etat dans la région, est compétente notamment dans le cas prévu à l'article L. 642-3.

Elle comprend des personnalités titulaires d'un mandat électif national ou local, des représentants de l'Etat et des personnalités qualifiées.

Sa composition, ses attributions et son mode de fonctionnement sont précisés par décret en Conseil d'Etat.

Une section de la commission régionale du patrimoine et des sites est instituée pour l'examen des recours prévus par les articles L. 621-32 et L. 641-1.

Elle est présidée par le préfet de région ou son représentant. Elle comprend en outre des représentants de l'Etat, des personnes titulaires d'un mandat électif et des personnalités qualifiées nommés par arrêté du préfet de région.

Les titulaires d'un mandat électif sont deux membres élus par chaque conseil départemental  en son sein et un maire désigné par chaque président de l'association départementale des maires. Ils ne siègent qu'à l'occasion de l'examen des affaires concernant le département dont ils sont issus.

Un décret en Conseil d'Etat détermine les conditions de désignation de ses membres et ses modalités de fonctionnement.
