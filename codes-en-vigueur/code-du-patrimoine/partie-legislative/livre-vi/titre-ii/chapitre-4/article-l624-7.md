# Article L624-7

Un décret en Conseil d'Etat détermine les conditions d'application du présent titre, à l'exception des articles L. 621-26, L. 621-28, L. 621-34 et L. 623-1. Il définit notamment les conditions dans lesquelles est dressé de manière périodique, dans chaque région et dans la collectivité territoriale de Corse, un état de l'avancement de l'instruction des demandes d'autorisation prévues à l'article L. 621-9.
