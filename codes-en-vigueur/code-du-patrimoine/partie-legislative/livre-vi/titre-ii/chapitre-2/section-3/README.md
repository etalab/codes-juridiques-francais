# Section 3 : Dispositions communes aux objets classés et aux objets inscrits.

- [Article L622-24](article-l622-24.md)
- [Article L622-25](article-l622-25.md)
- [Article L622-26](article-l622-26.md)
- [Article L622-27](article-l622-27.md)
- [Article L622-28](article-l622-28.md)
- [Article L622-29](article-l622-29.md)
