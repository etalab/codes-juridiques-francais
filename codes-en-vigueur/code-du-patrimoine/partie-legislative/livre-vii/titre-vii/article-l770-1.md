# Article L770-1

Les articles L. 131-1, L. 131-2, L. 132-1 à L. 132-6, L. 133-1, L. 211-1 à L. 211-6, L. 212-1 à L. 212-5, L. 212-15 à L. 212-28, L. 212-31 à L. 212-33, L. 212-37, L. 213-1 à L. 213-8, L. 214-1 à L. 214-10
, L. 510-1, L. 532-1 à L. 532-14 et L. 544-5 à L. 544-11 sont applicables au territoire des Terres australes et antarctiques françaises.
