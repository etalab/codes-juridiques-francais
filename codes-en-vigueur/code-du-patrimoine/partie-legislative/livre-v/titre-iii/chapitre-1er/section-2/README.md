# Section 2 : Exécution de fouilles par l'Etat.

- [Article L531-9](article-l531-9.md)
- [Article L531-10](article-l531-10.md)
- [Article L531-11](article-l531-11.md)
- [Article L531-12](article-l531-12.md)
- [Article L531-13](article-l531-13.md)
