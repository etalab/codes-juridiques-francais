# Article L531-4

L'autorité administrative statue sur les mesures définitives à prendre à l'égard des découvertes de caractère immobilier faites au cours des fouilles. Elle peut, à cet effet, ouvrir pour ces vestiges une instance de classement conformément aux dispositions de l'article L. 621-7.
