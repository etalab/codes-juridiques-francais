# Chapitre 1er : Régime de propriété des vestiges immobiliers.

- [Article L541-1](article-l541-1.md)
- [Article L541-2](article-l541-2.md)
