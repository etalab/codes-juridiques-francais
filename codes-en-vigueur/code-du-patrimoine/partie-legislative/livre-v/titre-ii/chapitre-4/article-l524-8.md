# Article L524-8

I. ― Lorsqu'elle est perçue sur les travaux mentionnés au a de l'article L. 524-2, la redevance est établie dans les conditions prévues aux articles L. 331-19 et L. 331-20 du code de l'urbanisme. Les règles de contrôle et les sanctions sont celles prévues aux articles L. 331-21 à L. 331-23 du même code.

II. ― Lorsqu'elle est perçue sur des travaux mentionnés aux b et c de l'article L. 524-2 du présent code ou sur la demande mentionnée au dernier alinéa de l'article L. 524-4, la redevance est établie par les services de l'Etat chargés des affaires culturelles dans la région.

Lorsque l'opération est réalisée par tranches de travaux, un titre de perception est émis au début de chacune des tranches prévues dans l'autorisation administrative, pour le montant dû au titre de cette tranche.

Le droit de reprise de l'administration s'exerce jusqu'au 31 décembre de la troisième année qui suit, selon les cas, la réalisation du fait générateur mentionné aux trois derniers alinéas de l'article L. 524-4 ou, lorsque l'autorisation administrative est accordée pour une durée supérieure à trois ans, l'année d'expiration de l'autorisation administrative.

Lorsqu'il apparaît que la superficie déclarée par l'aménageur est erronée, la procédure contradictoire prévue aux articles L. 55 à L. 61 B du livre des procédures fiscales est applicable.

III. ― La redevance due sur les travaux mentionnés aux a, b et c de l'article L. 524-2 du présent code ou sur la demande mentionnée au dernier alinéa de l'article L. 524-4 est recouvrée par les comptables publics compétents comme en matière de créances étrangères à l'impôt et au domaine.

Sont solidaires du paiement de la redevance les époux et les partenaires liés par un pacte civil de solidarité.

Le recouvrement de la redevance est garanti par le privilège prévu au 1 de l'article 1929 du code général des impôts.

L'action en recouvrement se prescrit par cinq ans à compter de l'émission du titre de perception.

Lorsque la redevance est perçue sur des travaux mentionnés au a de l'article L. 524-2 du présent code, le montant total est dû douze mois à compter de la date des faits générateurs mentionnés au a de l'article L. 524-4. Elle est émise avec la première échéance ou l'échéance unique de taxe d'aménagement à laquelle elle est adossée.

En cas de modification apportée au permis de construire ou d'aménager ou à l'autorisation tacite de construire ou d'aménager, le complément de redevance fait l'objet d'un titre de perception émis dans le délai de douze mois à compter de la date de délivrance du permis modificatif ou de l'autorisation réputée accordée.

En cas de transfert total de l'autorisation de construire ou d'aménager, le redevable de la redevance est le nouveau titulaire du droit à construire ou d'aménager. Un titre d'annulation est émis au profit du redevable initial. Un titre de perception est émis à l'encontre du nouveau titulaire du droit à construire ou d'aménager.

En cas de transfert partiel, un titre d'annulation des sommes correspondant à la surface transférée est émis au profit du titulaire initial du droit à construire ou d'aménager. Un titre de perception est émis à l'encontre du titulaire du transfert partiel.

En cas de transfert total ou partiel, le ou les titres de perception sont émis dans les trente-six mois suivant l'émission du titre d'annulation.

IV. ― L'Etat effectue un prélèvement de 3 % sur le montant des sommes recouvrées, au titre des frais d'assiette et de recouvrement.
