# Article L524-11

La redevance d'archéologie préventive mentionnée à l'article L. 524-2 est affectée dans les conditions prévues au présent article, dans la limite du plafond prévu au I de l'article 46 de la loi n° 2011-1977 du 28 décembre 2011 de finances pour 2012.

Après encaissement de la redevance, le comptable public compétent en reverse le produit à l'établissement public mentionné à l'article L. 523-1 ou, dans le cas mentionné au b de l'article L. 523-4, à la collectivité territoriale ou au groupement de collectivités territoriales après déduction des frais d'assiette et de recouvrement et après prélèvement du pourcentage du produit de la redevance alimentant le Fonds national pour l'archéologie préventive prévu à l'article L. 524-14. Le reversement intervient au plus tard à la fin du mois qui suit le mois d'encaissement.

Toutefois, lorsque l'établissement public réalise un diagnostic prescrit à l'occasion de travaux d'aménagement réalisés pour le compte d'une collectivité territoriale ou d'un groupement de collectivités territoriales qui, dans le cas prévu au quatrième alinéa de l'article L. 523-4, n'a pas donné son accord à l'intervention du service archéologique de la collectivité territoriale mentionnée au b de l'article L. 523-4, cette dernière reverse à l'établissement public le montant de la redevance d'archéologie préventive perçue au titre de ces travaux.

Dans le cas où une collectivité territoriale ou un groupement de collectivités territoriales assure l'intégralité d'un diagnostic en application du a de l'article L. 523-4, la redevance lui est reversée par l'établissement public, la collectivité territoriale ou le groupement de collectivités territoriales qui l'a perçue.

Le plafond mentionné au premier alinéa du présent article porte prioritairement sur la part affectée au Fonds national pour l'archéologie préventive prévu à l'article L. 524-14, puis sur la part affectée à l'établissement public mentionné à l'article L. 523-1.

Lorsque le plafond précédemment mentionné est atteint en cours d'année, le comptable public compétent poursuit les versements de redevance aux collectivités territoriales et aux groupements de collectivités territoriales dans les conditions prévues aux alinéas précédents. Le trop-perçu par le Fonds national pour l'archéologie préventive prévu à l'article L. 524-14 et, le cas échéant, par l'établissement public mentionné à l'article L. 523-1 est restitué au budget général selon les modalités fixées au A du III de l'article 46 de la loi n° 2011-1977 du 28 décembre 2011 précitée.
