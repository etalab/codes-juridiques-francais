# Chapitre 2 : Appellation "musée de France"

- [Section 1 : Conditions d'attribution et de retrait de l'appellation "musée de France".](section-1)
- [Section 2 : Dispositions pénales.](section-2)
- [Section 3 : Dispositions générales liées à l'appellation "musée de France"](section-3)
