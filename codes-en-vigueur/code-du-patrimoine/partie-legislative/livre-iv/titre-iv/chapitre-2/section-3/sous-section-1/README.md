# Sous-section 1 : Dispositions relatives à l'accueil du public.

- [Article L442-6](article-l442-6.md)
- [Article L442-7](article-l442-7.md)
