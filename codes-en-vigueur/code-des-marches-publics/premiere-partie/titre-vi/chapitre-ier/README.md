# Chapitre Ier : Règlement des litiges

- [Section 1 : Comités consultatifs de règlement amiable des différends ou des litiges relatifs aux marchés publics.](section-1)
- [Section 2 : Arbitrage.](section-2)
