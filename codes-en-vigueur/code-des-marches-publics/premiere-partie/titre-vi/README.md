# TITRE VI : DISPOSITIONS DIVERSES

- [Chapitre Ier : Règlement des litiges](chapitre-ier)
- [Chapitre III : Observatoire économique de l'achat public.](chapitre-iii)
- [Chapitre IV : Liste des marchés conclus.](chapitre-iv)
