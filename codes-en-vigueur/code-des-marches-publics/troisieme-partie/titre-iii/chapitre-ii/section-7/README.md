# Section 7 : Présentation des offres

- [Article 226](article-226.md)
- [Article 227](article-227.md)
- [Article 228](article-228.md)
- [Article 229](article-229.md)
- [Article 230](article-230.md)
