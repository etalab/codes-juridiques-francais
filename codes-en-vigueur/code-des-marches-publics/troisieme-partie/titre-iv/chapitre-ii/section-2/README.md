# Section 2 : Acceptation et agrément des conditions 
de paiement des sous-contractants

- [Sous-section 1 : Dispositions applicables aux sous-contrats présentant le caractère de sous-traités](sous-section-1)
- [Sous-section 2 : Dispositions applicables aux sous-contrats ne présentant pas le caractère de contrats de sous-traitance](sous-section-2)
