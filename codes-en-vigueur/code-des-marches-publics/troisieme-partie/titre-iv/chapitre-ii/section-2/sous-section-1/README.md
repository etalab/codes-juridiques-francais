# Sous-section 1 : Dispositions applicables aux sous-contrats présentant le caractère de sous-traités

- [Article 276](article-276.md)
- [Article 277](article-277.md)
- [Article 278](article-278.md)
- [Article 279](article-279.md)
- [Article 280](article-280.md)
- [Article 281](article-281.md)
