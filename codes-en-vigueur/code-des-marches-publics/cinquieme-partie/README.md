# CINQUIÈME PARTIE : DISPOSITIONS APPLICABLES AUX COLLECTIVITÉS D'OUTRE-MER.

- [Chapitre Ier : Dispositions applicables à Saint-Pierre-et-Miquelon.](chapitre-ier)
- [Chapitre II : Dispositions applicables à Mayotte.](chapitre-ii)
