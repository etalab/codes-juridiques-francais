# Article L532-6

Toute demande d'agrément pour une utilisation confinée d'organismes génétiquement modifiés est assortie du versement d'une taxe à la charge de l'exploitant de l'installation.

Le montant de cette taxe est fixé par arrêté des ministres compétents en fonction de la nature de la demande et de la destination, lucrative ou non, de l'utilisation, dans la limite de 2 000 euros.

Le recouvrement et le contentieux de la taxe instituée au présent article sont suivis par les comptables publics compétents selon les modalités fixées aux articles 81 à 95 du décret n° 62-1587 du 29 décembre 1962 portant règlement général sur la comptabilité publique.
