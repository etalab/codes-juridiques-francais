# Section 2 : Sanctions

- [Article L536-3](article-l536-3.md)
- [Article L536-4](article-l536-4.md)
- [Article L536-5](article-l536-5.md)
