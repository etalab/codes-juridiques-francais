# Titre III : Organismes génétiquement modifiés

- [Chapitre Ier : Dispositions générales](chapitre-ier)
- [Chapitre II : Utilisation confinée des organismes génétiquement modifiés](chapitre-ii)
- [Chapitre III : Dissémination volontaire d'organismes génétiquement modifiés](chapitre-iii)
- [Chapitre IV : Surveillance biologique du territoire](chapitre-iv)
- [Chapitre V : Contrôle et sanctions administratifs](chapitre-v)
- [Chapitre VI : Dispositions pénales](chapitre-vi)
- [Chapitre VII : Dispositions diverses](chapitre-vii)
