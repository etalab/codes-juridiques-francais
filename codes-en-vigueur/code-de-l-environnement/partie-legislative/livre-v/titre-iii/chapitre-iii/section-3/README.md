# Section 3 : Mise sur le marché

- [Article L533-4](article-l533-4.md)
- [Article L533-5](article-l533-5.md)
- [Article L533-5-1](article-l533-5-1.md)
- [Article L533-6](article-l533-6.md)
- [Article L533-7](article-l533-7.md)
- [Article L533-8](article-l533-8.md)
- [Article L533-8-1](article-l533-8-1.md)
- [Article L533-9](article-l533-9.md)
