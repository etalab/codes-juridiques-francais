# Section 6 : Dispositions en matière de sanctions administratives et pénales

- [Sous-section 1 : Procédure administrative](sous-section-1)
- [Sous-section 2 : Sanctions pénales](sous-section-2)
