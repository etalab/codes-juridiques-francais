# Section 1 : Dispositions générales

- [Article L555-1](article-l555-1.md)
- [Article L555-2](article-l555-2.md)
- [Article L555-3](article-l555-3.md)
- [Article L555-4](article-l555-4.md)
- [Article L555-5](article-l555-5.md)
- [Article L555-6](article-l555-6.md)
