# Article L555-11

Avant la mise en service d'une canalisation de transport, le titulaire de l'autorisation communique à un organisme habilité dans des conditions fixées par un décret en Conseil d'Etat ses coordonnées, la nature des fluides transportés et la zone d'implantation de la canalisation. Toute modification ultérieure de ces informations est précédée d'une communication à cet organisme.

L'organisme habilité met les informations ainsi collectées gratuitement à la disposition des communes dont le territoire est concerné par la canalisation.
