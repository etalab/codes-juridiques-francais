# Article L555-12

Des arrêtés complémentaires peuvent être pris par l'autorité administrative compétente. Ils peuvent fixer toutes les prescriptions additionnelles que la protection des intérêts mentionnés au II de l'article L. 555-1 rend nécessaires. Ils peuvent également prescrire des analyses, expertises ou contrôles durant les phases de construction, d'exploitation et de cessation d'activité des canalisations de transport. Ces arrêtés sont pris après avis de l'exploitant et de la commission consultative compétente en matière de prévention des risques technologiques.
