# Section 4 : Déclaration d'utilité publique et servitudes

- [Article L555-25](article-l555-25.md)
- [Article L555-26](article-l555-26.md)
- [Article L555-27](article-l555-27.md)
- [Article L555-28](article-l555-28.md)
- [Article L555-29](article-l555-29.md)
- [Article L555-30](article-l555-30.md)
