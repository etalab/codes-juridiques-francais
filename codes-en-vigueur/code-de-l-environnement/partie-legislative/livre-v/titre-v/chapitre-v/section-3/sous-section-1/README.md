# Sous-section 1 : Contrôles et sanctions administratives

- [Paragraphe 1 : Contrôles et sanctions concernant le maître d'ouvrage ou l'exploitant](paragraphe-1)
- [Paragraphe 2 : Contrôles et sanctions concernant les travaux à proximité des canalisations de transport](paragraphe-2)
