# Article L555-17

Les fonctionnaires ou agents des services de l'Etat chargés de la surveillance des canalisations de transport mentionnées à l'article L. 555-1 peuvent procéder à toutes investigations utiles à l'exercice de leur mission dans les conditions fixées par aux articles L. 171-1 à L. 171-3.
