# Article L553-4

Par dérogation aux dispositions de l'article L. 514-6, les décisions mentionnées aux I et II dudit article concernant les installations de production d'électricité utilisant l'énergie mécanique du vent classées au titre de l'article L. 511-2 peuvent être déférées à la juridiction administrative :

1° Par les demandeurs ou exploitants, dans un délai de deux mois à compter du jour où lesdits actes leur ont été notifiés ;

2° Par les tiers, personnes physiques ou morales, les communes intéressées ou leurs groupements, en raison des inconvénients ou des dangers que le fonctionnement de l'installation présente pour les intérêts visés à l'article L. 511-1, dans un délai de six mois à compter de la publication ou de l'affichage desdits actes.
