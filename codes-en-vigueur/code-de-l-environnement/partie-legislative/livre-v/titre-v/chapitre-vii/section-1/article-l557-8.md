# Article L557-8

En raison des risques spécifiques qu'ils présentent, certains produits et équipements sont classés en catégories distinctes, en fonction de leur type d'utilisation, de leur destination ou de leur niveau de risque, ainsi que de leur niveau sonore.
