# Article L557-10

Les opérateurs économiques tiennent à jour et à disposition de l'autorité administrative compétente et des agents compétents mentionnés à l'article L. 557-46 la liste des opérateurs économiques leur ayant fourni ou auxquels ils ont fourni un produit ou un équipement mentionné à l'article L. 557-1.

Cette liste est tenue à jour et à disposition pendant une durée de dix ans à compter de la date où le produit ou l'équipement leur a été fourni et de la date où ils ont fourni le produit ou l'équipement.
