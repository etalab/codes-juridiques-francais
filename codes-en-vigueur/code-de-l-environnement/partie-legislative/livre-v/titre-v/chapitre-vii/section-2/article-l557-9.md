# Article L557-9

Les opérateurs économiques ne mettent pas à disposition sur le marché aux personnes physiques ne possédant pas les connaissances mentionnées à l'article L. 557-6 ou ne répondant pas aux conditions d'âge mentionnées à l'article L. 557-7 les produits ou les équipements faisant l'objet des restrictions mentionnées à ces mêmes articles.
