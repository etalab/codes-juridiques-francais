# Article L557-30

L'exploitant détient et met à jour un dossier comportant les éléments relatifs à la fabrication et à l'exploitation du produit ou de l'équipement.
