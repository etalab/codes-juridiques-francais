# Chapitre VII :  Produits et équipements à risques

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Obligations des opérateurs économiques](section-2)
- [Section 3 : Suivi en service](section-3)
- [Section 4 : Obligations relatives aux organismes habilités](section-4)
- [Section 5 : Contrôles administratifs et mesures de police administrative](section-5)
- [Section 6 : Recherche et constatation des infractions](section-6)
- [Section 7 : Sanctions pénales](section-7)
- [Section 8 : Mise en œuvre](section-8)
