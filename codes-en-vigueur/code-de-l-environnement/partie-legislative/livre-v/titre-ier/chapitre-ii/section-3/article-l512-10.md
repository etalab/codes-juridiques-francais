# Article L512-10

Pour la protection des intérêts mentionnés à l'article L. 511-1, le ministre chargé des installations classées peut fixer par arrêté, après consultation des ministres intéressés et du Conseil supérieur de la prévention des risques technologiques, les prescriptions générales applicables à certaines catégories d'installations soumises à déclaration.

Ces arrêtés s'imposent de plein droit aux installations nouvelles.

Ils précisent, après avis des organisations professionnelles intéressées, les délais et les conditions dans lesquels ils s'appliquent aux installations existantes. Ils précisent également les conditions dans lesquelles ces prescriptions peuvent être adaptées par arrêté préfectoral aux circonstances locales.
