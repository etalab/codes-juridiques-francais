# Article L512-12-1

Lorsque l'installation soumise à déclaration est mise à l'arrêt définitif, l'exploitant place le site dans un état tel qu'il ne puisse porter atteinte aux intérêts mentionnés à l'article L. 511-1 et qu'il permette un usage futur comparable à la dernière période d'activité de l'installation. Il en informe le propriétaire du terrain sur lequel est sise l'installation ainsi que le maire ou le président de l'établissement public de coopération intercommunale compétent en matière d'urbanisme.
