# Article L512-15

L'exploitant est tenu d'adresser sa demande d'autorisation ou d'enregistrement, ou sa déclaration en même temps que sa demande de permis de construire.

Il doit renouveler sa demande d'autorisation ou d'enregistrement, ou sa déclaration soit en cas de transfert, soit en cas d'extension ou de transformation de ses installations, ou de changement dans ses procédés de fabrication, entraînant des dangers ou inconvénients mentionnés à l'article L. 511-1.
