# Article L512-7-2

Le préfet peut décider que la demande d'enregistrement sera instruite selon les règles de procédure prévues par la section 1 du présent chapitre :

1° Si, au regard de la localisation du projet, en prenant en compte les critères mentionnés au point 2 de l'annexe III de la directive 85 / 337 / CEE du 27 juin 1985 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement, la sensibilité environnementale du milieu le justifie ;

2° Ou si le cumul des incidences du projet avec celles d'autres projets d'installations, ouvrages ou travaux situés dans cette zone le justifie ;

3° Ou si l'aménagement des prescriptions générales applicables à l'installation, sollicité par l'exploitant, le justifie.

Dans ce cas, le préfet notifie sa décision motivée au demandeur, en l'invitant à déposer le dossier correspondant. Sa décision est rendue publique.
