# Chapitre VII : Dispositions diverses

- [Article L517-1](article-l517-1.md)
- [Article L517-2](article-l517-2.md)
