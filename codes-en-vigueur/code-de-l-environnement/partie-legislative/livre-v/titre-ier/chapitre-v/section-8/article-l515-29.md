# Article L515-29

I.-Les informations, fournies par l'exploitant, nécessaires au réexamen des conditions d'autorisation de l'installation sont soumises à l'enquête publique prévue au chapitre III du titre II du livre Ier dans les cas suivants :

-lors d'un réexamen périodique prévu à l'article L. 515-28 si l'exploitant sollicite une dérogation permettant de fixer des valeurs limites d'émission qui excèdent les niveaux d'émission associés aux conclusions sur les meilleures techniques disponibles ;

-lors d'un réexamen à l'initiative de l'autorité administrative si la pollution causée par l'installation est telle qu'il convient de réviser les valeurs limites d'émission indiquées dans l'autorisation ou d'inclure de nouvelles valeurs limites d'émission.

A l'issue de cette enquête, un arrêté complémentaire est pris en application de l'article L. 512-3.

Si une dérogation est accordée, l'autorité compétente met à la disposition du public, y compris par les moyens de communication électroniques, la décision qui mentionne les raisons spécifiques pour lesquelles cette dérogation a été accordée et les conditions dont elle a été assortie.

II.-Jusqu'au 1er janvier 2019, les informations mentionnées au I font l'objet, en lieu et place de l'enquête publique, d'une mise à disposition du public. Celui-ci est informé des modalités selon lesquelles il peut les consulter et formuler des observations avant qu'une décision ne soit prise. Cette information est faite par voie d'affichage sur le site de l'installation par l'exploitant et, à la diligence du préfet, dans les mairies de la commune d'implantation et des communes situées à proximité de cette installation ou par tous autres moyens appropriés tels que les moyens de communication électroniques.
