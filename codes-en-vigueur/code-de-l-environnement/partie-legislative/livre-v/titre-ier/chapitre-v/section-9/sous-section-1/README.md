# Sous-section 1 : Dispositions communes

- [Article L515-32](article-l515-32.md)
- [Article L515-33](article-l515-33.md)
- [Article L515-34](article-l515-34.md)
- [Article L515-35](article-l515-35.md)
