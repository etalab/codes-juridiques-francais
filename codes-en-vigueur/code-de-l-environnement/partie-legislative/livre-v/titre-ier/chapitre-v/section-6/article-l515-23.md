# Article L515-23

Le plan de prévention des risques technologiques approuvé vaut servitude d'utilité publique. Il est porté à la connaissance des maires des communes situées dans le périmètre du plan en application de l'article L. 121-2 du code de l'urbanisme. Il est annexé aux plans locaux d'urbanisme, conformément à l'article L. 126-1 du même code.
