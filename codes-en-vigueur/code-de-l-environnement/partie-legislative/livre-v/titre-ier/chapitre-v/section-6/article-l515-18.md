# Article L515-18

Les mesures prévues par les plans de prévention des risques technologiques, en particulier au II et au III de l'article L. 515-16, sont mises en oeuvre progressivement en fonction notamment de la probabilité, de la gravité et de la cinétique des accidents potentiels ainsi que du rapport entre le coût des mesures envisagées et le gain en sécurité attendu.
