# Article L516-2

Pour les installations relevant des catégories visées à l'article L. 516-1, l'exploitant est tenu d'informer le préfet en cas de modification substantielle des capacités techniques et financières visées à l'article L. 512-1.

S'il constate que les capacités techniques et financières ne sont pas susceptibles de permettre de satisfaire aux obligations de l'article L. 512-1, le préfet peut imposer la constitution ou la révision des garanties financières visées à l'article L. 516-1.

Un décret en Conseil d'Etat définit les modalités d'application de l'article L. 516-1 et du présent article ainsi que les conditions de leur application aux installations régulièrement mises en service ou autorisées avant la publication de la loi n° 2003-699 du 30 juillet 2003 relative à la prévention des risques technologiques et naturels et à la réparation des dommages.
