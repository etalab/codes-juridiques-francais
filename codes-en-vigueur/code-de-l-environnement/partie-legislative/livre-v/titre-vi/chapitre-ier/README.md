# Chapitre Ier : Mesures de sauvegarde des populations menacées par certains risques naturels majeurs

- [Article L561-1](article-l561-1.md)
- [Article L561-2](article-l561-2.md)
- [Article L561-3](article-l561-3.md)
- [Article L561-4](article-l561-4.md)
- [Article L561-5](article-l561-5.md)
