# Article L595-2

L'Autorité de sûreté nucléaire accorde les autorisations ou agréments et reçoit les déclarations relatifs au transport de substances radioactives.
