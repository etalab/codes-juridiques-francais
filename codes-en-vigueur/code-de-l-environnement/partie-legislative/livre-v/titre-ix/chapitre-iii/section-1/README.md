# Section 1 : Régime d'autorisation

- [Sous-section 1 : Définitions et principes généraux](sous-section-1)
- [Sous-section 2 : Création et mise en service](sous-section-2)
- [Sous-section 3 : Fonctionnement](sous-section-3)
- [Sous-section 4 : Arrêt définitif, démantèlement et déclassement](sous-section-4)
- [Sous-section 5 : Dispositions diverses](sous-section-5)
