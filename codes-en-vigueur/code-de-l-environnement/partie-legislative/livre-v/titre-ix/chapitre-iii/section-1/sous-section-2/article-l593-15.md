# Article L593-15

Un projet de modification de l'installation ou de ses conditions d'exploitation soumis à l'accord de l'Autorité de sûreté nucléaire qui, sans constituer une modification notable de l'installation, est susceptible de provoquer un accroissement significatif de ses prélèvements d'eau ou de ses rejets dans l'environnement fait l'objet d'une mise à disposition du public selon les modalités définies à l'article L. 122-1-1.
