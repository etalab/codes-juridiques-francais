# Article L593-29

Les installations de stockage de déchets radioactifs ne sont pas soumises aux articles L. 593-25 à L. 593-27.

L'article L. 593-28 est applicable aux autorisations accordées en application du présent paragraphe.
