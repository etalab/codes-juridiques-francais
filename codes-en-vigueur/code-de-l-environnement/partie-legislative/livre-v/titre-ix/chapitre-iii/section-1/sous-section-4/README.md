# Sous-section 4 : Arrêt définitif, démantèlement et déclassement

- [Paragraphe 1 : Dispositions propres aux installations nucléaires de base autres que les installations de stockage de déchets radioactifs](paragraphe-1)
- [Paragraphe 2 : Dispositions propres aux installations de stockage de déchets radioactifs](paragraphe-2)
- [Paragraphe 3 : Dispositions communes relatives au déclassement](paragraphe-3)
