# Chapitre III : Installations nucléaires de base

- [Section 1 : Régime d'autorisation](section-1)
- [Section 2 : Installations nouvelles ou temporaires et installations fonctionnant au bénéfice des droits acquis](section-2)
