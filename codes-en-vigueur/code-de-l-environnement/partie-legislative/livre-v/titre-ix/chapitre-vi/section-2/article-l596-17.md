# Article L596-17

Si l'intéressé ne défère pas à la mise en demeure de régulariser sa situation faite en application de l'article L. 596-16 ou si sa demande d'autorisation ou d'agrément est rejetée, l'Autorité de sûreté nucléaire peut :

1° Faire application des dispositions prévues aux 1° et 2° de l'article L. 596-15 ;

2° En cas de nécessité, et par une décision motivée, ordonner l'arrêt du fonctionnement de l'installation ou du déroulement de l'opération.
