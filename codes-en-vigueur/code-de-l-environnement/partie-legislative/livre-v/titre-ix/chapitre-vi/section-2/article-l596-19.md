# Article L596-19

L'Autorité de sûreté nucléaire prend les mesures provisoires rendues nécessaires pour l'application des mesures prévues aux articles L. 593-13, L. 593-21, L. 593-22 et L. 593-24 ainsi qu'aux articles L. 596-14 à L. 596-17, y compris l'apposition des scellés.
