# Chapitre VI : Contrôle et contentieux

- [Section 1 : Inspecteurs de la sûreté nucléaire](section-1)
- [Section 2 : Mesures de police et sanctions administratives](section-2)
- [Section 3 : Contentieux](section-3)
- [Section 4 : Dispositions pénales](section-4)
