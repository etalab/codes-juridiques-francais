# Article L596-2

Les inspecteurs de la sûreté nucléaire, pour l'exercice de leur mission de surveillance, sont assermentés et astreints au secret professionnel dans les conditions et sous les sanctions prévues aux articles 226-13 et 226-14 du code pénal.
