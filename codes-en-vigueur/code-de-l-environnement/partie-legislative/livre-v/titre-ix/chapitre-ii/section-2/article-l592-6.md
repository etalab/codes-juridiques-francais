# Article L592-6

Dès leur nomination, les membres du collège de l'Autorité de sûreté nucléaire établissent une déclaration mentionnant les intérêts qu'ils détiennent ou ont détenus au cours des cinq années précédentes dans les domaines relevant de la compétence de l'autorité.

Cette déclaration, déposée au siège de l'autorité et tenue à la disposition des membres du collège, est mise à jour à l'initiative du membre du collège intéressé dès qu'une modification intervient.

Aucun membre ne peut détenir, au cours de son mandat, d'intérêt de nature à affecter son indépendance ou son impartialité.
