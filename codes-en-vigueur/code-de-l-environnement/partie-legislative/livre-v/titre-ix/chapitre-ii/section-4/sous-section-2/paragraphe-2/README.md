# Paragraphe 2 : Pouvoirs de désignation de l'Autorité de sûreté nucléaire

- [Article L592-22](article-l592-22.md)
- [Article L592-23](article-l592-23.md)
