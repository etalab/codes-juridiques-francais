# Article L592-19

L'Autorité de sûreté nucléaire peut prendre des décisions réglementaires à caractère technique pour compléter les modalités d'application des décrets et arrêtés pris en matière de sûreté nucléaire ou de radioprotection, à l'exception de ceux ayant trait à la médecine du travail.

Ces décisions sont soumises à l'homologation par arrêté selon le cas du ministre chargé de la sûreté nucléaire pour celles d'entre elles qui sont relatives à la sûreté nucléaire ou du ministre chargé de la radioprotection pour celles d'entre elles qui sont relatives à la radioprotection.

Les arrêtés d'homologation ainsi que les décisions homologuées sont publiés au Journal officiel de la République française.
