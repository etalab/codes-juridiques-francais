# Section 4 : Attributions

- [Sous-section 1 : Décisions](sous-section-1)
- [Sous-section 2 : Missions de contrôle](sous-section-2)
- [Sous-section 3 : Autres attributions](sous-section-3)
- [Sous-section 4 : Situations d'urgence radiologique](sous-section-4)
