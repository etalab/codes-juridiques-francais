# Article L594-6

I. ― Les exploitants disposent à titre dérogatoire d'un report de cinq ans à compter du 30 juin 2011 pour la mise en œuvre du plan de constitution des actifs définis à l'article L. 594-2 si les deux conditions suivantes sont remplies :

1° Les charges mentionnées à l'article L. 594-1, à l'exclusion de celles liées au cycle d'exploitation, évaluées en euros courants sur la période allant du 29 juin 2011 à 2030 sont inférieures à 10 % de l'ensemble des charges mentionnées au même article, à l'exclusion de celles liées au cycle d'exploitation, évaluées en euros courants ;

2° Au moins 75 % des provisions mentionnées au premier alinéa de l'article L. 594-2, à l'exclusion de celles liées au cycle d'exploitation, sont couvertes au 29 juin 2011 par des actifs mentionnés à ce même article.

II. ― Jusqu'au 29 juin 2016, la dotation moyenne annuelle au titre des actifs mentionnés à l'article L. 594-2 doit être positive ou nulle, déduction faite des décaissements au titre des opérations de démantèlement en cours et des dotations au titre des charges nouvelles ajoutées au passif des fonds dédiés.
