# Article L541-4-3

Un déchet cesse d'être un déchet après avoir été traité dans une installation visée à l'article L. 214-1 soumise à autorisation ou à déclaration ou dans une installation visée à l'article L. 511-1 soumise à autorisation, à enregistrement ou à déclaration et avoir subi une opération de valorisation, notamment de recyclage ou de préparation en vue de la réutilisation, s'il répond à des critères remplissant l'ensemble des conditions suivantes :

― la substance ou l'objet est couramment utilisé à des fins spécifiques ;

― il existe une demande pour une telle substance ou objet ou elle répond à un marché ;

― la substance ou l'objet remplit les exigences techniques aux fins spécifiques et respecte la législation et les normes applicables aux produits ;

― son utilisation n'aura pas d'effets globaux nocifs pour l'environnement ou la santé humaine.

Ces critères sont fixés par l'autorité administrative compétente. Ils comprennent le cas échéant des teneurs limites en substances polluantes et sont fixés en prenant en compte les effets nocifs des substances ou de l'objet sur l'environnement.

Les modalités d'application du présent article sont fixées par décret.
