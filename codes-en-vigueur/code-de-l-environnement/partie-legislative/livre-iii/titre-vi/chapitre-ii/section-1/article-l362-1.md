# Article L362-1

En vue d'assurer la protection des espaces naturels, la circulation des véhicules à moteur est interdite en dehors des voies classées dans le domaine public routier de l'Etat, des départements et des communes, des chemins ruraux et des voies privées ouvertes à la circulation publique des véhicules à moteur.

La charte de chaque parc naturel régional ou la charte de chaque parc national comporte un article établissant les règles de circulation des véhicules à moteur sur les voies et chemins de chaque commune adhérente du parc naturel régional ou du parc national et des communes comprises en tout ou partie dans le coeur du parc national.
