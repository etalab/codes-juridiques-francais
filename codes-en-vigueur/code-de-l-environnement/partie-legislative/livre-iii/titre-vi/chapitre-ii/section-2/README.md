# Section 2 : Dispositions en matière pénale

- [Article L362-5](article-l362-5.md)
- [Article L362-7](article-l362-7.md)
