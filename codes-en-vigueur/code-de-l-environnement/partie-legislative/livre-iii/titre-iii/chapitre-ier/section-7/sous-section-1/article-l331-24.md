# Article L331-24

I.-Les personnes qui se trouvent à l'intérieur du coeur ou d'une réserve intégrale d'un parc national ou qui en sortent sont tenues d'ouvrir leurs sacs, carniers ou poches à gibier à toute réquisition des agents mentionnés à l'article L. 172-1.

Les frais de transport, d'entretien et de garde des objets saisis sont supportés par l'auteur de l'infraction. Le jugement de condamnation peut prononcer la confiscation de l'objet de l'infraction ainsi que des instruments et véhicules ayant servi à la commettre.
