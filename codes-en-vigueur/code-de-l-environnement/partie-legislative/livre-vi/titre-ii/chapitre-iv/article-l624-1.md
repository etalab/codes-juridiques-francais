# Article L624-1

Sont applicables à la Polynésie française les articles L. 229-1 à L. 229-4.
