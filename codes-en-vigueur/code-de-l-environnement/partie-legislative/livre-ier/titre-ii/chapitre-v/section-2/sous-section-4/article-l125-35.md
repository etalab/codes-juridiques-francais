# Article L125-35

Le Haut Comité pour la transparence et l'information sur la sécurité nucléaire peut faire réaliser des expertises nécessaires à l'accomplissement de ses missions et organiser des débats contradictoires.

Il organise périodiquement des concertations et des débats concernant la gestion durable des matières et des déchets nucléaires radioactifs.

Les personnes responsables d'activités nucléaires, l'Autorité de sûreté nucléaire ainsi que les autres services de l'Etat concernés lui communiquent tous les documents et toutes les informations utiles à l'accomplissement de ses missions. Selon le cas, les dispositions des articles L. 125-10 et L. 125-11 ou celles du chapitre IV du titre II du livre Ier et de la loi n° 78-753 du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal sont applicables à cette communication.
