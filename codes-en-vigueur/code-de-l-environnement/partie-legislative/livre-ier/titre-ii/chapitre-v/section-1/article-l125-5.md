# Article L125-5

I. ― Les acquéreurs ou locataires de biens immobiliers situés dans des zones couvertes par un plan de prévention des risques technologiques ou par un plan de prévention des risques naturels prévisibles, prescrit ou approuvé, ou dans des zones de sismicité définies par décret en Conseil d'Etat, sont informés par le vendeur ou le bailleur de l'existence des risques visés par ce plan ou ce décret.

A cet effet, un état des risques naturels et technologiques est établi à partir des informations mises à disposition par le préfet. En cas de mise en vente de l'immeuble, l'état est produit dans les conditions et selon les modalités prévues aux articles L. 271-4 et L. 271-5 du code de la construction et de l'habitation.

II. ― En cas de mise en location de l'immeuble, l'état des risques naturels et technologiques est fourni au nouveau locataire dans les conditions et selon les modalités prévues à l'article 3-3 de la loi n° 89-462 du 6 juillet 1989 tendant à améliorer les rapports locatifs et portant modification de la loi n° 86-1290 du 23 décembre 1986.

L'état des risques naturels et technologiques, fourni par le bailleur, est joint aux baux commerciaux mentionnés aux articles L. 145-1 et L. 145-2 du code de commerce.

III. ― Le préfet arrête la liste des communes dans lesquelles les dispositions du I et du II sont applicables ainsi que, pour chaque commune concernée, la liste des risques et des documents à prendre en compte.

IV. ― Lorsqu'un immeuble bâti a subi un sinistre ayant donné lieu au versement d'une indemnité en application de l'article L. 125-2 ou de l'article L. 128-2 du code des assurances, le vendeur ou le bailleur de l'immeuble est tenu d'informer par écrit l'acquéreur ou le locataire de tout sinistre survenu pendant la période où il a été propriétaire de l'immeuble ou dont il a été lui-même informé en application des présentes dispositions. En cas de vente de l'immeuble, cette information est mentionnée dans l'acte authentique constatant la réalisation de la vente.

V. ― En cas de non-respect des dispositions du présent article, l'acquéreur ou le locataire peut poursuivre la résolution du contrat ou demander au juge une diminution du prix.

VI. ― Un décret en Conseil d'Etat fixe les conditions d'application du présent article.

VII. ― Le présent article n'est pas applicable aux conventions mentionnées aux articles L. 323-14 et L. 411-37 du code rural et de la pêche maritime.
