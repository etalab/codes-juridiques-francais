# Article L123-15

Le commissaire enquêteur ou la commission d'enquête rend son rapport et ses conclusions motivées dans un délai de trente jours à compter de la fin de l'enquête. Si ce délai ne peut être respecté, un délai supplémentaire peut être accordé à la demande du commissaire enquêteur ou de la commission d'enquête par l'autorité compétente pour organiser l'enquête, après avis du responsable du projet.

Le rapport doit faire état des contre-propositions qui ont été produites durant l'enquête ainsi que des réponses éventuelles du maître d'ouvrage.

Le rapport et les conclusions motivées sont rendus publics.

Si, à l'expiration du délai prévu au premier alinéa, le commissaire enquêteur ou la commission d'enquête n'a pas remis son rapport et ses conclusions motivées, ni justifié d'un motif pour le dépassement du délai, l'autorité compétente pour organiser l'enquête peut, avec l'accord du maître d'ouvrage et après une mise en demeure du commissaire enquêteur ou de la commission d'enquête restée infructueuse, demander au président du tribunal administratif ou au conseiller qu'il délègue de dessaisir le commissaire enquêteur ou la commission d'enquête et de lui substituer son suppléant, un nouveau commissaire enquêteur ou une nouvelle commission d'enquête ; celui-ci doit, à partir des résultats de l'enquête, remettre le rapport et les conclusions motivées dans un maximum de trente jours à partir de sa nomination.

Le nouveau commissaire enquêteur ou la nouvelle commission d'enquête peut faire usage des prérogatives prévues par l'article L. 123-13.
