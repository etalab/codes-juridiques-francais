# Article L123-10

I. ― Quinze jours au moins avant l'ouverture de l'enquête et durant celle-ci, l'autorité compétente pour ouvrir et organiser l'enquête informe le public :

― de l'objet de l'enquête ;

― de la ou des décisions pouvant être adoptées au terme de l'enquête et des autorités compétentes pour statuer ;

― du nom et des qualités du commissaire enquêteur ou des membres de la commission d'enquête, de la date d'ouverture, du lieu de l'enquête, de sa durée et de ses modalités ;

― de l'existence d'une évaluation environnementale, d'une étude d'impact ou, à défaut, d'un dossier comprenant les informations environnementales se rapportant à l'objet de l'enquête, et du lieu où ces documents peuvent être consultés ;

― lorsqu'il a été émis, de l'existence de l'avis de l'autorité administrative de l'Etat compétente en matière d'environnement mentionné aux articles L. 122-1 et L. 122-7 du présent code ou à l'article L. 121-12 du code de l'urbanisme, et le lieu où il peut être consulté.

II. ― L'information du public est assurée par tous moyens appropriés, selon l'importance et la nature du projet, plan ou programme, notamment par voie d'affichage sur les lieux concernés par l'enquête, par voie de publication locale ou par voie électronique.

Un décret détermine les projets, plans ou programmes qui font obligatoirement l'objet d'une communication au public par voie électronique, comprenant non seulement les éléments indiqués au I mais également, selon les cas, l'évaluation environnementale et son résumé non technique, l'étude d'impact et son résumé non technique ou, à défaut, le dossier d'informations environnementales se rapportant à l'objet de l'enquête publique ainsi que, lorsqu'ils sont rendus obligatoires, les avis émis par une autorité administrative sur les projets, plans ou programmes. Ce décret permet, dans un premier temps, une expérimentation sur une liste limitée de projets, plans ou programmes ; cette liste pourra être étendue en fonction du résultat de cette expérimentation.

La personne responsable du projet assume les frais afférents à ces différentes mesures de publicité de l'enquête publique.
