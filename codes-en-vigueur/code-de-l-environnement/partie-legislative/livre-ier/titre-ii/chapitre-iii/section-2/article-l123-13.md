# Article L123-13

I. ― Le commissaire enquêteur ou la commission d'enquête conduit l'enquête de manière à permettre au public de disposer d'une information complète sur le projet, plan ou programme, et de participer effectivement au processus de décision en lui permettant de présenter ses observations et propositions. Dans les conditions fixées par décret en Conseil d'Etat, la participation du public peut s'effectuer par voie électronique.

II. ― Pendant l'enquête, le commissaire enquêteur ou le président de la commission d'enquête reçoit le maître d'ouvrage de l'opération soumise à l'enquête publique à la demande de ce dernier. Il peut en outre :

― recevoir toute information et, s'il estime que des documents sont utiles à la bonne information du public, demander au maître d'ouvrage de communiquer ces documents au public ;

― visiter les lieux concernés, à l'exception des lieux d'habitation, après en avoir informé au préalable les propriétaires et les occupants ;

― entendre toutes les personnes concernées par le projet, plan ou programme qui en font la demande et convoquer toutes les personnes dont il juge l'audition utile ;

― organiser, sous sa présidence, toute réunion d'information et d'échange avec le public en présence du maître d'ouvrage.

A la demande du commissaire enquêteur ou du président de la commission d'enquête et lorsque les spécificités de l'enquête l'exigent, le président du tribunal administratif ou le conseiller qu'il délègue peut désigner un expert chargé d'assister le commissaire enquêteur ou la commission d'enquête. Le coût de cette expertise est à la charge du responsable du projet.
