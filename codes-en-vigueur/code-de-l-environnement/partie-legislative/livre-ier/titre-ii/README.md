# Titre II : Information et participation des citoyens

- [Chapitre III : Enquêtes publiques relatives aux opérations susceptibles d'affecter l'environnement](chapitre-iii)
- [Chapitre V : Autres modes d'information](chapitre-v)
- [Chapitre VI : Déclaration de projet](chapitre-vi)
- [Chapitre VII : De l'infrastructure d'information géographique](chapitre-vii)
