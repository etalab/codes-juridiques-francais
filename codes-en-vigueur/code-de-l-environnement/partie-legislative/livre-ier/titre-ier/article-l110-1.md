# Article L110-1

I. - Les espaces, ressources et milieux naturels, les sites et paysages, la qualité de l'air, les espèces animales et végétales, la diversité et les équilibres biologiques auxquels ils participent font partie du patrimoine commun de la nation.

II. - Leur protection, leur mise en valeur, leur restauration, leur remise en état et leur gestion sont d'intérêt général et concourent à l'objectif de développement durable qui vise à satisfaire les besoins de développement et la santé des générations présentes sans compromettre la capacité des générations futures à répondre aux leurs. Elles s'inspirent, dans le cadre des lois qui en définissent la portée, des principes suivants :

1° Le principe de précaution, selon lequel l'absence de certitudes, compte tenu des connaissances scientifiques et techniques du moment, ne doit pas retarder l'adoption de mesures effectives et proportionnées visant à prévenir un risque de dommages graves et irréversibles à l'environnement à un coût économiquement acceptable ;

2° Le principe d'action préventive et de correction, par priorité à la source, des atteintes à l'environnement, en utilisant les meilleures techniques disponibles à un coût économiquement acceptable ;

3° Le principe pollueur-payeur, selon lequel les frais résultant des mesures de prévention, de réduction de la pollution et de lutte contre celle-ci doivent être supportés par le pollueur ;

4° Le principe selon lequel toute personne a le droit d'accéder aux informations relatives à l'environnement détenues par les autorités publiques ;

5° Le principe de participation en vertu duquel toute personne est informée des projets de décisions publiques ayant une incidence sur l'environnement dans des conditions lui permettant de formuler ses observations, qui sont prises en considération par l'autorité compétente.

III. - L'objectif de développement durable, tel qu'indiqué au II, répond, de façon concomitante et cohérente, à cinq finalités :

1° La lutte contre le changement climatique ;

2° La préservation de la biodiversité, des milieux et des ressources ;

3° La cohésion sociale et la solidarité entre les territoires et les générations ;

4° L'épanouissement de tous les êtres humains ;

5° Une dynamique de développement suivant des modes de production et de consommation responsables.

IV. - L'Agenda 21 est un projet territorial de développement durable.
