# Article L172-4

Les fonctionnaires et agents de l'Etat et des collectivités territoriales, et de leurs établissements publics, habilités à rechercher et à constater les infractions aux dispositions du présent code et des textes pris pour son application exercent leurs compétences dans les conditions prévues à la présente section.

Les agents de police judiciaire adjoints mentionnés à l'article 21 du code de procédure pénale sont habilités à rechercher et constater les infractions au présent code dans les conditions définies par les autres livres du présent code. Ils exercent ces missions dans les limites et selon les modalités fixées par le code de procédure pénale.
