# Article L414-9

Des plans nationaux d'action pour la conservation ou le rétablissement des espèces visées aux articles L. 411-1 et L. 411-2 ainsi que des espèces d'insectes pollinisateurs sont élaborés et, après consultation du public, mis en œuvre sur la base des données des instituts scientifiques compétents lorsque la situation biologique de ces espèces le justifie.

Ces plans tiennent compte des exigences économiques, sociales et culturelles ainsi que des impératifs de la défense nationale.

Les informations relatives aux actions prévues par les plans sont diffusées aux publics intéressés ; les informations prescrites leur sont également accessibles pendant toute la durée des plans, dans les secteurs géographiques pertinents.

Un décret précise, en tant que de besoin, les modalités d'application du présent article.
