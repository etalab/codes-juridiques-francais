# Article L413-1

Les dispositions du présent chapitre ne s'appliquent pas aux produits de la pêche maritime et de la conchyliculture destinés à la consommation ni aux établissements de pêche et aux instituts chargés de leur contrôle.
