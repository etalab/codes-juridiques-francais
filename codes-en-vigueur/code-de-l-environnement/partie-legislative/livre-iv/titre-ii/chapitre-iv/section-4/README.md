# Section 4 : Commercialisation et transport du gibier

- [Sous-section 1 : Interdiction permanente](sous-section-1)
- [Sous-section 2 : Interdiction temporaire](sous-section-2)
