# Sous-section 1 : Interdiction permanente

- [Article L424-8](article-l424-8.md)
- [Article L424-9](article-l424-9.md)
- [Article L424-10](article-l424-10.md)
- [Article L424-11](article-l424-11.md)
