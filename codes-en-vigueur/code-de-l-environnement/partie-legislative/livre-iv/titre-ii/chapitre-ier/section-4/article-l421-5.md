# Article L421-5

Les associations dénommées fédérations départementales des chasseurs participent à la mise en valeur du patrimoine cynégétique départemental, à la protection et à la gestion de la faune sauvage ainsi que de ses habitats. Elles assurent la promotion et la défense de la chasse ainsi que des intérêts de leurs adhérents.

Elles apportent leur concours à la prévention du braconnage. Elles conduisent des actions d'information, d'éducation et d'appui technique à l'intention des gestionnaires des territoires et des chasseurs et, le cas échéant, des gardes-chasse particuliers. Elles mènent des actions d'information et d'éducation au développement durable en matière de connaissance et de préservation de la faune sauvage et de ses habitats ainsi qu'en matière de gestion de la biodiversité.

Elles coordonnent les actions des associations communales et intercommunales de chasse agréées.

Elles conduisent des actions de prévention des dégâts de gibier et assurent l'indemnisation des dégâts de grand gibier dans les conditions prévues par les articles L. 426-1 et L. 426-5.

Elles conduisent également des actions pour surveiller les dangers sanitaires impliquant le gibier ainsi que des actions participant à la prévention de la diffusion de dangers sanitaires entre les espèces de gibier, les animaux domestiques et l'homme.

Elles élaborent, en association avec les propriétaires, les gestionnaires et les usagers des territoires concernés, un schéma départemental de gestion cynégétique, conformément aux dispositions de l'article L. 425-1.

Elles contribuent, à la demande du préfet, à l'exécution des arrêtés préfectoraux autorisant des tirs de prélèvement. Elles agissent dans ce cadre en collaboration avec leurs adhérents.

Elles peuvent apporter leur concours à la validation du permis de chasser.

Les associations de chasse spécialisée sont associées aux travaux des fédérations.

Les fédérations peuvent recruter, pour l'exercice de leurs missions, des agents de développement mandatés à cet effet. Ceux-ci veillent notamment au respect du schéma départemental de gestion cynégétique sur tous les territoires où celui-ci est applicable. Dans des conditions fixées par décret en Conseil d'Etat, leurs constats font foi jusqu'à preuve contraire.
