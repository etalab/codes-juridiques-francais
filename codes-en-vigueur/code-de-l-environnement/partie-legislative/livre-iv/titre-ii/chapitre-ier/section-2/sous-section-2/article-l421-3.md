# Article L421-3

Les fonctions d'agent de l'Office national de la chasse et de la faune sauvage commissionné au titre des eaux et forêts et assermenté sont soumises aux règles d'incompatibilité prévues à l'article L. 341-4 du code forestier.
