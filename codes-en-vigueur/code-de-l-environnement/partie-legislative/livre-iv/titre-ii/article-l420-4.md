# Article L420-4

Les dispositions du présent titre ne sont pas applicables dans le département de la Guyane, à l'exception de l'article L. 421-1 ainsi que du 4° du I de l'article L. 428-5 en tant que les espaces mentionnés concernent le parc amazonien de Guyane et les réserves naturelles.
