# Sous-section 8 : Dispositions diverses

- [Article L422-25](article-l422-25.md)
- [Article L422-26](article-l422-26.md)
