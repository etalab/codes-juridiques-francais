# Sous-section 4 : Territoire

- [Paragraphe 1 : Terrains soumis à l'action de l'association](paragraphe-1)
- [Paragraphe 2 : Terrains faisant l'objet d'une opposition](paragraphe-2)
- [Paragraphe 3 : Apports](paragraphe-3)
- [Paragraphe 4 : Modification du territoire de l'association](paragraphe-4)
- [Paragraphe 5 : Enclaves](paragraphe-5)
