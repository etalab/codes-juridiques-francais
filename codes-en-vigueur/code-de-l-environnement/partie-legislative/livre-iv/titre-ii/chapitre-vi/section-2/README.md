# Section 2 : Indemnisation judiciaire des dégâts causés aux récoltes

- [Article L426-7](article-l426-7.md)
- [Article L426-8](article-l426-8.md)
