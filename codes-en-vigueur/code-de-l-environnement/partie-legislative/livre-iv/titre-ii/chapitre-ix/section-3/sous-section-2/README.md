# Sous-section 2 : Dispositions particulières à l'indemnisation des dégâts causés par les sangliers

- [Article L429-27](article-l429-27.md)
- [Article L429-28](article-l429-28.md)
- [Article L429-29](article-l429-29.md)
- [Article L429-30](article-l429-30.md)
- [Article L429-31](article-l429-31.md)
- [Article L429-32](article-l429-32.md)
