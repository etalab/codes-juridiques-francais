# Sous-section 1 : Ban communal

- [Article L429-2](article-l429-2.md)
- [Article L429-3](article-l429-3.md)
- [Article L429-4](article-l429-4.md)
- [Article L429-5](article-l429-5.md)
- [Article L429-6](article-l429-6.md)
