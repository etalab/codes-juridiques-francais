# Sous-section 2 : Battues administratives

- [Article L427-4](article-l427-4.md)
- [Article L427-5](article-l427-5.md)
- [Article L427-6](article-l427-6.md)
- [Article L427-7](article-l427-7.md)
