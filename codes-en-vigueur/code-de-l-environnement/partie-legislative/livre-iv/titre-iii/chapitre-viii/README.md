# Chapitre VIII : Dispositions diverses

- [Article L438-1](article-l438-1.md)
- [Article L438-2](article-l438-2.md)
