# Section 2 : Droit de pêche des riverains

- [Article L435-4](article-l435-4.md)
- [Article L435-5](article-l435-5.md)
