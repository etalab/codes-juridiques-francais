# Article L228-3

I. ― Dans les communes ou groupements de communes de plus de 100 000 habitants où une mauvaise qualité de l'air est avérée, notamment par des dépassements de normes réglementaires ou des risques de dépassements de ces normes, une zone d'actions prioritaires pour l'air, dont l'accès est interdit aux véhicules contribuant le plus à la pollution atmosphérique, peut être instituée, à titre expérimental, afin de lutter contre cette pollution et notamment réduire les émissions de particules et d'oxydes d'azote.

Les communes ou groupements de communes souhaitant participer à l'expérimentation adressent, dans un délai de deux ans à compter de la publication de la loi n° 2010-788 du 12 juillet 2010 portant engagement national pour l'environnement, leur projet de zones d'actions prioritaires pour l'air au représentant de l'Etat dans le département qui le transmet, accompagné de ses observations, au ministre chargé des collectivités territoriales et au ministre chargé du développement durable.

Dans les zones dans lesquelles sont constatés ou prévus des dépassements des valeurs limites de la qualité de l'air telles que définies à l'article L. 221-1, le représentant de l'Etat dans le département peut proposer aux communes ou groupements de communes de mettre en place une expérimentation de zone d'actions prioritaires pour l'air.

Les expérimentations sont autorisées par décret pour une durée ne pouvant excéder trois ans. Elles peuvent être prorogées par décret pour une durée de dix-huit mois à la demande des communes ou groupements de communes à l'initiative du projet.

Les communes ou groupements de communes où l'expérimentation a été autorisée adressent, après chaque période de douze mois d'expérimentation, au ministre chargé des collectivités territoriales et au ministre chargé du développement durable un rapport contenant les informations nécessaires à son évaluation.

Trois ans après l'entrée en vigueur de la loi n° 2010-788 du 12 juillet 2010 précitée, le Gouvernement transmet au Parlement un rapport d'évaluation portant sur les expérimentations mises en œuvre en application de la présente section.

II. ― Le projet de zone d'actions prioritaires pour l'air prévu au deuxième alinéa du I du présent article doit, préalablement à sa transmission au représentant de l'Etat dans le département, avoir fait l'objet d'une évaluation environnementale élaborée dans les conditions prévues à la section 2 du chapitre II du titre II du livre premier, ainsi que d'une concertation avec l'ensemble des parties concernées, notamment les communes limitrophes de la zone, les gestionnaires de voirie, les autorités organisatrices de transport compétentes dans la zone et les chambres consulaires concernées. L'opportunité, les objectifs, les caractéristiques principales du projet et son évaluation environnementale sont mis à la disposition du public dans les conditions prévues à l'article 233 de la loi n° 2010-788 du 12 juillet 2010 précitée.

Le projet précise le périmètre de la zone d'actions prioritaires pour l'air, lequel doit être cohérent avec les objectifs assignés à ce dispositif et compatible, lorsqu'il existe, avec le plan de protection de l'atmosphère défini à l'article L. 222-4.

Il précise également, par référence à une nomenclature des véhicules classés en fonction de leur niveau d'émission de polluants atmosphériques établie par arrêté du ministre chargé du développement durable, les véhicules dont l'accès à la zone d'actions prioritaires pour l'air est interdit, ainsi que les modalités d'identification des véhicules autorisés à accéder à la zone, y compris pour les véhicules en transit.

Un décret précise les véhicules auxquels l'accès aux zones d'actions prioritaires pour l'air ne peut être interdit, ainsi que les modalités de demande d'autorisation supplémentaire pour certains véhicules de circuler, par dérogation, dans les zones d'actions prioritaires pour l'air.

III. ― Le fait de ne pas respecter l'interdiction de circuler dans une zone d'actions prioritaires pour l'air est puni d'une peine d'amende prévue par décret en Conseil d'Etat.
