# Article L229-11

L'autorité administrative notifie aux exploitants des installations autorisées à émettre des gaz à effet de serre le montant total des quotas d'émission affectés au titre de chaque période et la quantité délivrée chaque année.

Un décret en Conseil d'Etat fixe les modalités de notification des décisions d'affectation et de délivrance des quotas, les conditions dans lesquelles les informations correspondantes sont rendues accessibles au public, les règles de délivrance annuelle des quotas, les règles applicables en cas de changement d'exploitant ou de cessation ou de transfert d'activité ainsi que les conditions dans lesquelles les décisions d'affectation ou de délivrance et le plan national d'affectation des quotas prévu à l'article L. 229-8 peuvent être contestés.
