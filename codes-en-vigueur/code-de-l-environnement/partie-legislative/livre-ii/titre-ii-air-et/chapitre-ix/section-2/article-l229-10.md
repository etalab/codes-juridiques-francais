# Article L229-10

Une partie des quotas délivrés au cours de la période de cinq ans débutant le 1er janvier 2008 le sont à titre onéreux, dans la limite de 10 % de ces quotas.
