# Article L229-7

Un quota d'émission de gaz à effet de serre au sens de la présente section est une unité de compte représentative de l'émission de l'équivalent d'une tonne de dioxyde de carbone.

Pour chaque installation bénéficiant de l'autorisation d'émettre des gaz à effet de serre, ou pour les émissions de gaz à effet de serre résultant d'activités aériennes, l'Etat affecte à l'exploitant, pour une période déterminée, des quotas d'émission et lui délivre chaque année, au cours de cette période, une part des quotas qui lui ont été ainsi affectés.

La quantité de gaz à effet de serre émise au cours d'une année civile est calculée ou mesurée et exprimée en tonnes de dioxyde de carbone.

A l'issue de chacune des années civiles de la période d'affectation, l'exploitant restitue à l'Etat sous peine des sanctions prévues à l'article L. 229-18 un nombre de quotas égal au total des émissions de gaz à effet de serre de ses installations ou résultant de ses activités aériennes, que ces quotas aient été délivrés ou qu'ils aient été acquis en application des dispositions de l'article L. 229-15 ou du IV de l'article L. 229-12. Au titre de cette obligation, l'exploitant d'une installation ne peut pas restituer de quotas délivrés à un exploitant d'aéronef suivant les dispositions de l'article L. 229-12. Il n'est en revanche pas tenu de restituer les quotas correspondant aux émissions de dioxyde de carbone ayant été vérifiées comme faisant l'objet d'un captage et d'un transport en vue d'un stockage permanent vers un site de stockage géologique de dioxyde de carbone exploité conformément aux dispositions de la section 6 du chapitre IX du titre II du livre II.

Toutefois, lorsqu'une installation utilise, dans un processus de combustion, des gaz fournis par une installation sidérurgique, les quotas correspondants sont affectés et délivrés à l'exploitant de cette dernière installation. Celui-ci est seul responsable, à ce titre, des obligations prévues par la présente section.

L'exploitant peut, dans la limite des pourcentages mentionnés à l'article 11 bis de la directive 2003/87/ CE du 13 octobre 2003, s'acquitter de l'obligation prévue au quatrième alinéa du présent article au moyen de certaines unités inscrites à son compte dans le registre de l'Union mentionné à l'article L. 229-16. Ces unités recouvrent :

-les unités issues des activités de projets visés à l'article L. 229-22 ;

-les unités provenant d'autres activités que les activités de projets ci-dessus destinées à réduire les émissions conformément aux accords multilatéraux ou bilatéraux conclus par l'Union européenne avec les pays tiers ;

-les unités issues d'un système contraignant d'échange de droits d'émission reconnu par un accord entre l'Union européenne et l'entité nationale, infra ou supranationale de laquelle ce système dépend ;

-les unités issues de projets de réduction des émissions de gaz à effet de serre non couvertes par le système communautaire d'échange de quotas d'émission et réalisés sur le territoire d'un Etat membre de l'Union européenne.

Les conditions d'utilisation de ces unités sont déterminées par les actes d'exécution de l'Union européenne prévus aux articles 11 bis, 24 bis et 25 de la directive 2003/87/ CE du 13 octobre 2003.
