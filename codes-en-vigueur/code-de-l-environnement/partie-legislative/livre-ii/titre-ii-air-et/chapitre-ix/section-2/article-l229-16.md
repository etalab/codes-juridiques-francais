# Article L229-16

I.-Un registre européen des quotas d'émission de gaz à effet de serre comptabilise les quotas ainsi que les unités définies à l'article L. 229-7 délivrés, détenus, transférés et annulés selon les modalités prévues par le règlement pris en application de l'article 19 de la directive 2003/87/ CE du 13 octobre 2003.

II.-Le rôle d'administrateur national pour ce registre est délégué à titre exclusif à une personne morale désignée par décret en Conseil d'Etat, qui fixe en outre les modalités d'application du présent II, et notamment les missions du délégataire et les conditions de sa rémunération.
