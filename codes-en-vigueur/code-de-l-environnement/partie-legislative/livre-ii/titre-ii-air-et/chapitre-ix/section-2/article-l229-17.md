# Article L229-17

L'Etat peut inclure, après approbation de la Commission européenne, des activités et des gaz à effet de serre qui ne sont pas mentionnés à l'annexe I de la directive 2003/87/ CE du 13 octobre 2003.

L'application du système d'échange de quotas d'émission aux activités et gaz ci-dessus ainsi que l'affectation de quotas supplémentaires évitent les distorsions potentielles de concurrence et préservent l'intégrité environnementale de ce système.
