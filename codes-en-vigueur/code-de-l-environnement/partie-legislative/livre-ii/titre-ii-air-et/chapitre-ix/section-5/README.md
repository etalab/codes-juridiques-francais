# Section 5 : Recherche de formations souterraines aptes 

au stockage géologique de dioxyde de carbone

- [Article L229-27](article-l229-27.md)
- [Article L229-28](article-l229-28.md)
- [Article L229-29](article-l229-29.md)
- [Article L229-30](article-l229-30.md)
- [Article L229-31](article-l229-31.md)
