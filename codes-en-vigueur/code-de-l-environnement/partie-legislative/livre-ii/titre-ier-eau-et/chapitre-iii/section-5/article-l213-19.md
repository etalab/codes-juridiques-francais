# Article L213-19

L'office peut prononcer d'office le dégrèvement ou la restitution de redevances et pénalités qui n'étaient pas dues.

Le contribuable qui conteste tout ou partie des redevances mises à sa charge adresse, préalablement à tout recours contentieux, une réclamation au directeur de l'office de l'eau.

L'office de l'eau peut accorder des remises totales ou partielles de redevances, majorations et intérêts de retard soit sur demande du contribuable lorsque celui-ci est dans l'impossibilité de payer par suite de gêne ou d'indigence, soit sur demande du mandataire judiciaire pour les entreprises soumises à la procédure de sauvegarde, de redressement ou de liquidation judiciaire.
