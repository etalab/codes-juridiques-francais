# Article L213-6

Un décret en Conseil d'Etat précise les conditions d'application de la présente section.
