# Article L213-4

L'Office national de l'eau et des milieux aquatiques détermine les domaines et les conditions de son action dans un programme pluriannuel d'intervention qui indique les montants de dépenses et de recettes nécessaires à sa mise en oeuvre.

Dans le cadre de la mise en œuvre du programme mentionné au V de l'article L. 213-10-8, l'Office national de l'eau et des milieux aquatiques apporte directement ou indirectement des concours financiers aux personnes publiques ou privées.

L'exécution du programme pluriannuel d'intervention fait l'objet d'un rapport annuel présenté par le Gouvernement au Parlement.
