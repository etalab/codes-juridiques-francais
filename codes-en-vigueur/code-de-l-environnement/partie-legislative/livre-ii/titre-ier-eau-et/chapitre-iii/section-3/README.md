# Section 3 : Comités de bassin et agences de l'eau

- [Sous-section 1 : Dispositions générales](sous-section-1)
- [Sous-section 2 : Dispositions financières](sous-section-2)
- [Sous-section 3 : Redevances des agences de l'eau](sous-section-3)
- [Sous-section 4 : Obligations déclaratives, contrôle et modalités de recouvrement](sous-section-4)
