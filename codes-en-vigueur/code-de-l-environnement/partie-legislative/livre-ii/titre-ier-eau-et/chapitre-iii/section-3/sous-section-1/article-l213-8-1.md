# Article L213-8-1

Dans chaque bassin ou groupement de bassins visé à l'article L. 212-1, une agence de l'eau, établissement public de l'Etat à caractère administratif, met en oeuvre les schémas visés aux articles L. 212-1 et L. 212-3, en favorisant une gestion équilibrée et économe de la ressource en eau et des milieux aquatiques, l'alimentation en eau potable, la régulation des crues et le développement durable des activités économiques.

L'agence de l'eau est administrée par un conseil d'administration composé :

1° D'un président nommé par décret ;

2° De représentants désignés par les personnes visées au 1° de l'article L. 213-8 en leur sein ;

3° De représentants désignés par les personnes visées au 2° de l'article L. 213-8 en leur sein ;

4° De représentants de l'Etat ou de ses établissements publics ;

5° D'un représentant du personnel de l'agence.

Les catégories mentionnées aux 2°, 3° et 4° du présent article disposent d'un nombre égal de sièges.

Un décret en Conseil d'Etat fixe les conditions d'application du présent article.
