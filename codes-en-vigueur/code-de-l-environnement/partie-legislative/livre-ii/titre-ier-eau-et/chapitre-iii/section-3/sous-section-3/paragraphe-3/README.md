# Paragraphe 3 : Redevances pour modernisation des réseaux de collecte

- [Article L213-10-5](article-l213-10-5.md)
- [Article L213-10-6](article-l213-10-6.md)
- [Article L213-10-7](article-l213-10-7.md)
