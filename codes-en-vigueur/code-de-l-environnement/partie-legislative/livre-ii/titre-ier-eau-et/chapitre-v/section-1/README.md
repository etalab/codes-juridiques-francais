# Section 1 : Droits des riverains

- [Article L215-1](article-l215-1.md)
- [Article L215-2](article-l215-2.md)
- [Article L215-3](article-l215-3.md)
- [Article L215-4](article-l215-4.md)
- [Article L215-6](article-l215-6.md)
