# Chapitre VIII : Dispositions spéciales aux eaux marines et aux voies ouvertes à la navigation maritime

- [Section 1 : Pollution par les rejets des navires](section-1)
- [Section 2 : Pollution due aux opérations d'exploration ou d'exploitation du fond de la mer ou de son sous-sol](section-2)
- [Section 3 : Pollution par les opérations d'immersion](section-3)
- [Section 4 : Pollution par les opérations d'incinération](section-4)
- [Section 5 : Mesures de police maritime d'urgence](section-5)
- [Section 6 : Autres dispositions applicables aux rejets nuisibles en mer ou dans les eaux salées](section-6)
- [Section 7 : Zone de protection écologique](section-7)
- [Section 8 : Dispositions relatives au contrôle et à la gestion des eaux de ballast et des sédiments des navires](section-8)
