# Article L218-15

Est puni d'un an d'emprisonnement et de 200 000 € d'amende le fait, pour tout capitaine d'un navire, de se rendre coupable d'infractions aux dispositions de la règle 8 de l'annexe IV, des règles 3, 4 et 5 de l'annexe V et des règles 12, 13, 14, 16 et 18 de l'annexe VI de la convention MARPOL.
