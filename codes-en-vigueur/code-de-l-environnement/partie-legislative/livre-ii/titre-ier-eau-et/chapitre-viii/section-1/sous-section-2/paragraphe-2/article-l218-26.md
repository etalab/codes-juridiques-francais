# Article L218-26

Indépendamment des officiers et agents de police judiciaire, qui exercent leurs pouvoirs conformément au code de procédure pénale, sont habilités à constater les infractions aux dispositions des règles 15,17,34 et 36 de l'annexe I, des règles 13 et 15 de l'annexe II, de la règle 7 de l'annexe III, de la règle 8 de l'annexe IV, des règles 3,4 et 5 de l'annexe V, des règles 12,13,14,16 et 18 de l'annexe VI et du protocole I de la Convention internationale pour la prévention de la pollution par les navires mentionnée à l'article L. 218-10, les infractions aux dispositions de la présente sous-section ainsi que les infractions aux dispositions réglementaires prises pour leur application :

1° Les administrateurs des affaires maritimes ;

2° Les officiers du corps technique et administratif des affaires maritimes ;

3° Les fonctionnaires affectés dans les services exerçant des missions de contrôle dans le domaine des affaires maritimes sous l'autorité ou à la disposition du ministre chargé de la mer ;

4° (Abrogé)

5° (Abrogé)

6° Les fonctionnaires et agents assermentés et commissionnés des services maritimes, des ports autonomes maritimes et des grands ports maritimes ;

7° Les ingénieurs des mines, les ingénieurs de l'industrie et des mines, les ingénieurs des ponts, des eaux et des forêts et les ingénieurs des travaux publics de l'Etat affectés dans les services déconcentrés du ministère chargé de l'environnement ou à la direction régionale de l'environnement, de l'aménagement et du logement ;

8° Les officiers de port, officiers de port adjoints et surveillants de port ayant la qualité de fonctionnaire ;

9° Les chercheurs, ingénieurs et techniciens assermentés de l'Institut français de recherche pour l'exploitation de la mer ;

10° Les agents des douanes ;

11° Les commandants, commandants en second ou   commissaires des armées embarqués des bâtiments de la marine nationale ainsi que les chefs de bord des aéronefs de la marine nationale et des aéronefs de la défense chargés de la surveillance en mer.
