# Section 2 : Pollution due aux opérations d'exploration ou d'exploitation du fond de la mer ou de son sous-sol

- [Article L218-32](article-l218-32.md)
- [Article L218-33](article-l218-33.md)
- [Article L218-34](article-l218-34.md)
- [Article L218-35](article-l218-35.md)
- [Article L218-36](article-l218-36.md)
- [Article L218-37](article-l218-37.md)
- [Article L218-38](article-l218-38.md)
- [Article L218-39](article-l218-39.md)
- [Article L218-40](article-l218-40.md)
- [Article L218-41](article-l218-41.md)
