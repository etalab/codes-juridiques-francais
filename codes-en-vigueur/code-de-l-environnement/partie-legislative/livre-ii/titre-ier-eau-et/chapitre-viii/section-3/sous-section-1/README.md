# Sous-section 1 : Dispositions générales

- [Article L218-42](article-l218-42.md)
- [Article L218-43](article-l218-43.md)
- [Article L218-44](article-l218-44.md)
- [Article L218-45](article-l218-45.md)
- [Article L218-46](article-l218-46.md)
- [Article L218-47](article-l218-47.md)
