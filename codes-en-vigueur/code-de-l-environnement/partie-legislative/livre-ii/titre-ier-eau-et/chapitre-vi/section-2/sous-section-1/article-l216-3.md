# Article L216-3

Outre les officiers et agents de police judiciaire et les inspecteurs de l'environnement mentionnés à l'article L. 172-1, sont habilités à rechercher et à constater les infractions aux dispositions des chapitres Ier à VII du présent titre ainsi que des textes et des décisions pris pour leur application :

1° Les agents des services de l'Etat chargés des forêts commissionnés à raison de leurs compétences en matière forestière et assermentés à cet effet ;

2° Les agents de l'Office national des forêts commissionnés à raison de leurs compétences en matière forestière et assermentés à cet effet ;

3° Les inspecteurs de la sûreté nucléaire désignés en application de l'article L. 592-22 ;

4° Les chercheurs, ingénieurs et techniciens assermentés de l'Institut français de recherche pour l'exploitation de la mer ;

5° Les officiers de port et officiers de port adjoints ;

6° Les gardes champêtres ;

7° Les agents des douanes ;

8° Les gardes du littoral mentionnés à l'article L. 322-10-1, agissant dans les conditions prévues à cet article ;

9° Les agents des réserves naturelles mentionnés à l'article L. 332-20, agissant dans les conditions prévues à cet article.
