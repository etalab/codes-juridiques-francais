# Article R512-46-25

I. ― Lorsqu'une installation classée soumise à enregistrement est mise à l'arrêt définitif, l'exploitant notifie au préfet la date de cet arrêt trois mois au moins avant celui-ci. Il est donné récépissé sans frais de cette notification.

II. ― La notification prévue au I indique les mesures prises ou prévues pour assurer, dès l'arrêt de l'exploitation, la mise en sécurité du site. Ces mesures comportent, notamment :

1° L'évacuation des produits dangereux et, pour les installations autres que les installations de stockage de déchets, la gestion des déchets présents sur le site ;

2° Des interdictions ou limitations d'accès au site ;

3° La suppression des risques d'incendie et d'explosion ;

4° La surveillance des effets de l'installation sur son environnement.

III. ― En outre, l'exploitant doit placer le site de l'installation dans un état tel qu'il ne puisse porter atteinte aux intérêts mentionnés à l'article L. 511-1 et qu'il permette un usage futur du site déterminé selon les dispositions des articles R. 512-46-26 et R. 512-46-27.
