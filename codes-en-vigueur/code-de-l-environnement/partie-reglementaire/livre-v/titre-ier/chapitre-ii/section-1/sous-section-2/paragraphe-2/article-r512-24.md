# Article R512-24

Lorsqu'il existe un comité d'hygiène, de sécurité et des conditions de travail dans l'établissement où est située l'installation, ce comité est consulté dans les conditions fixées par les articles L. 4612-15, R. 4523-2, R. 4523-3, R. 4612-4 et R. 4612-5 du code du travail.
