# Article R512-22

Le préfet met en oeuvre les dispositions de l'article R. 122-11 :

1° Lorsque le périmètre défini au III de l'article R. 512-14 comprend une commune transfrontalière ;

2° Lorsque le projet est susceptible d'avoir des incidences notables dans un autre Etat ou lorsque les autorités de cet Etat en font la demande.
