# Article R512-12

Lorsqu'il constate qu'une installation classée, dont la demande d'autorisation lui est présentée, relève de la liste prévue à l'article L. 515-8, le préfet en informe le maire de la ou des communes d'implantation, ainsi que le demandeur. Le maire est avisé qu'il lui appartient, s'il le juge utile, de demander l'institution des servitudes mentionnées à l'article L. 515-8.
