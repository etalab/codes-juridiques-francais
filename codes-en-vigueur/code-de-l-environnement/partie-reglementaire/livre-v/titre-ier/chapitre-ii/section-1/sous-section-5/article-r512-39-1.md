# Article R512-39-1

I.-Lorsqu'une installation classée soumise à autorisation est mise à l'arrêt définitif, l'exploitant notifie au préfet la date de cet arrêt trois mois au moins avant celui-ci. Ce délai est porté à six mois dans le cas des installations visées à l'article R. 512-35. Il est donné récépissé sans frais de cette notification.

II.-La notification prévue au I indique les mesures prises ou prévues pour assurer, dès l'arrêt de l'exploitation, la mise en sécurité du site. Ces mesures comportent, notamment :

1° L'évacuation des produits dangereux, et, pour les installations autres que les installations de stockage de déchets, gestion des déchets présents sur le site ;

2° Des interdictions ou limitations d'accès au site ;

3° La suppression des risques d'incendie et d'explosion ;

4° La surveillance des effets de l'installation sur son environnement.

III.-En outre, l'exploitant doit placer le site de l'installation dans un état tel qu'il ne puisse porter atteinte aux intérêts mentionnés à l'article L. 511-1 et qu'il permette un usage futur du site déterminé selon les dispositions des articles R. 512-39-2 et R. 512-39-3.
