# Article R512-9

I. ― L'étude de dangers mentionnée à l'article R. 512-6 justifie que le projet permet d'atteindre, dans des conditions économiquement acceptables, un niveau de risque aussi bas que possible, compte tenu de l'état des connaissances et des pratiques et de la vulnérabilité de l'environnement de l'installation.

Le contenu de l'étude de dangers doit être en relation avec l'importance des risques engendrés par l'installation, compte tenu de son environnement et de la vulnérabilité des intérêts mentionnés aux articles L. 211-1 et L. 511-1.

II. ― Cette étude précise, notamment, la nature et l'organisation des moyens de secours dont le demandeur dispose ou dont il s'est assuré le concours en vue de combattre les effets d'un éventuel sinistre. Dans le cas des installations figurant sur la liste prévue à l'article L. 515-8, le demandeur doit fournir les éléments indispensables pour l'élaboration par les autorités publiques d'un plan particulier d'intervention.

L'étude comporte, notamment, un résumé non technique explicitant la probabilité, la cinétique et les zones d'effets des accidents potentiels, ainsi qu'une cartographie des zones de risques significatifs.

Le ministre chargé des installations classées peut préciser les critères techniques et méthodologiques à prendre en compte pour l'établissement des études de dangers, par arrêté pris dans les formes prévues à l'article L. 512-5.

Pour certaines catégories d'installations impliquant l'utilisation, la fabrication ou le stockage de substances dangereuses, le ministre chargé des installations classées peut préciser, par arrêté pris sur le fondement de l'article L. 512-5, le contenu de l'étude de dangers portant, notamment, sur les mesures d'organisation et de gestion propres à réduire la probabilité et les effets d'un accident majeur.

III. ― (Abrogé)
