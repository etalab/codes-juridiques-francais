# Sous-section 3 : Autorisation et prescriptions

- [Article R512-28](article-r512-28.md)
- [Article R512-29](article-r512-29.md)
- [Article R512-30](article-r512-30.md)
- [Article R512-31](article-r512-31.md)
- [Article R512-32](article-r512-32.md)
- [Article R512-33](article-r512-33.md)
- [Article R512-34](article-r512-34.md)
- [Article R512-35](article-r512-35.md)
- [Article R512-36](article-r512-36.md)
- [Article R512-37](article-r512-37.md)
