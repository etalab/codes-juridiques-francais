# Sous-section 1 : Dispositions générales

- [Article R512-47](article-r512-47.md)
- [Article R512-48](article-r512-48.md)
- [Article R512-49](article-r512-49.md)
- [Article R512-50](article-r512-50.md)
- [Article R512-51](article-r512-51.md)
- [Article R512-52](article-r512-52.md)
- [Article R512-54](article-r512-54.md)
