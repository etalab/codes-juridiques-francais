# Article R512-51

Les prescriptions générales applicables aux installations soumises à déclaration font l'objet d'arrêtés préfectoraux pris en application de l'article L. 512-9 après avis du conseil départemental de l'environnement et des risques sanitaires et technologiques.

Une ampliation des arrêtés prévus à l'alinéa précédent est adressée à chacun des maires du département et un extrait en est publié dans deux journaux locaux ou régionaux diffusés dans tout le département.
