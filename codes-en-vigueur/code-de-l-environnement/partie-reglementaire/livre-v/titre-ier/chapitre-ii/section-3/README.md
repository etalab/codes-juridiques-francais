# Section 3 : Installations soumises à déclaration

- [Sous-section 1 : Dispositions générales](sous-section-1)
- [Sous-section 2 : Contrôle périodique de certaines installations](sous-section-2)
- [Sous-section 3 : Mise à l'arrêt définitif et remise en état](sous-section-3)
