# Article R515-49

En application de l'article L. 515-25, le projet de plan de prévention des risques technologiques pour un dépôt de munitions anciennes n'est pas soumis à enquête publique.
