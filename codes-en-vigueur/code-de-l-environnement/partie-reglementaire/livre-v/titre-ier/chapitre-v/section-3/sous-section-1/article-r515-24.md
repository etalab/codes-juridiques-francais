# Article R515-24

Les dispositions de la présente sous-section sont applicables dans le cas où l'installation donne lieu à l'institution des servitudes d'utilité publique prévues par l'article L. 515-12.
