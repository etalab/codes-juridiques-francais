# Article R515-97

<div align="left">Les informations prévues à l'article L. 515-38 sont notamment communiquées par écrit aux établissements recevant du public, au sens de l'article R. 123-2 du code de la construction et de l'habitation, et à toutes les installations classées voisines susceptibles d'être affectés en cas d'accident majeur. <br/>
<br/>Les informations sont envoyées à chaque mise à jour suite à un changement notable et au moins tous les cinq ans.<br/>
<br/>
</div>
