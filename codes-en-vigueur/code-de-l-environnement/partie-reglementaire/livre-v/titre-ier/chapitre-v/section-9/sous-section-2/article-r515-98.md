# Article R515-98

<div align="left">I. ― L'étude de dangers mentionnée à l'article R. 512-9 démontre qu'a été établi un plan d'opération interne et qu'a été mis en œuvre un système de gestion de la sécurité de façon appropriée. <br/>
<br/>II. ― Elle fait l'objet d'un réexamen au moins tous les cinq ans et d'une mise à jour si nécessaire. <br/>
<br/>Elle est par ailleurs réalisée ou réexaminée et mise à jour : <br/>
<br/>― avant la mise en service d'une nouvelle installation, en application de l'article L. 512-1 ; <br/>
<br/>― avant la mise en œuvre de changements notables ; <br/>
<br/>― dans le délai de deux ans à compter du jour où l'installation entre dans le champ d'application de la présente sous-section ; <br/>
<br/>― à la suite d'un accident majeur. <br/>
<br/>III. ― Sans préjudice des dispositions des articles L. 124-1, L. 124-4 et L. 515-36, lorsque l'étude de dangers peut être communiquée, un résumé non technique de cette étude est également mis à disposition. Ce résumé comprend au moins des informations générales sur les risques liés aux accidents majeurs et sur les effets potentiels sur la santé publique et l'environnement en cas d'accident majeur.<br/>
<br/>
</div>
