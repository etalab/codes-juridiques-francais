# Article R515-90

<div align="left">L'étude de dangers mentionnée à l'article R. 512-9 justifie que l'exploitant met en œuvre les mesures de maîtrise des risques internes à l'établissement dans des conditions économiques acceptables, c'est-à-dire celles dont le coût n'est pas disproportionné par rapport aux bénéfices attendus, soit pour la sécurité globale de l'installation, soit pour la protection des intérêts mentionnés à l'article L. 511-1. <br/>
<br/>L'étude de dangers démontre par ailleurs qu'une politique de prévention des accidents majeurs telle que mentionnée à l'article L. 515-33 est mise en œuvre de façon appropriée.<br/>
<br/>
<br/>
<br/>
</div>
