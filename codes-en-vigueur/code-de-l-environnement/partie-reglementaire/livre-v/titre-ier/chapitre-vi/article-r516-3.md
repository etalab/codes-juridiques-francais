# Article R516-3

Le préfet appelle et met en oeuvre les garanties financières soit en cas de non-exécution par l'exploitant des opérations mentionnées au IV de l'article R. 516-2, après intervention des mesures prévues à l'article L. 514-1, soit en cas de disparition juridique de l'exploitant. Le préfet ne peut appeler la garantie additionnelle mentionnée au VI de l'article R. 516-2 qu'à la cessation d'activité.
