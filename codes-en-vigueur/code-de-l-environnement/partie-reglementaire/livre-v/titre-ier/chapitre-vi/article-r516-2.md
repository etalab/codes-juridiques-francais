# Article R516-2

I.-Les garanties financières exigées à l'article L. 516-1 résultent, au choix de l'exploitant :

a) De l'engagement écrit d'un établissement de crédit, d'une entreprise d'assurance ou d'une société de caution mutuelle ;

b) D'une consignation entre les mains de la Caisse des dépôts et consignations ;

c) Pour les installations de stockage de déchets, d'un fonds de garantie géré par l'Agence de l'environnement et de la maîtrise de l'énergie ;

d) D'un fonds de garantie privé, proposé par un secteur d'activité et dont la capacité financière adéquate est définie par arrêté du ministre chargé des installations classées ; ou

e) De l'engagement écrit, portant garantie autonome au sens de l'article 2321 du code civil, de la personne physique, où que soit son domicile, ou de la personne morale, où que se situe son siège social, qui possède plus de la moitié du capital de l'exploitant ou qui contrôle l'exploitant au regard des critères énoncés à l'article L. 233-3 du code de commerce. Dans ce cas, le garant doit lui-même être bénéficiaire d'un engagement écrit d'un établissement de crédit, d'une entreprise d'assurance, d'une société de caution mutuelle ou d'un fonds de garantie mentionné au d ci-dessus, ou avoir procédé à une consignation entre les mains de la Caisse des dépôts et consignations.

Lorsque le siège social de la personne morale garante n'est pas situé dans un Etat membre de l'Union européenne ou dans un Etat partie à l'accord sur l'Espace économique européen, le garant doit disposer d'une agence, d'une succursale ou d'une représentation établie en France.

II.-L'arrêté d'autorisation fixe le montant des garanties financières exigées ainsi que les modalités d'actualisation de ce montant.

III.-Dès la mise en activité de l'installation, l'exploitant transmet au préfet un document attestant la constitution des garanties financières. Ce document est établi selon un modèle défini par arrêté conjoint du ministre chargé de l'économie et du ministre chargé des installations classées.

IV.-Le montant des garanties financières est établi d'après les indications de l'exploitant et compte tenu du coût des opérations suivantes, telles qu'elles sont indiquées dans l'arrêté d'autorisation :

1° Pour les installations de stockage de déchets :

a) Surveillance du site ;

b) Interventions en cas d'accident ou de pollution ;

c) Remise en état du site après exploitation ;

2° Pour les carrières :

Remise en état du site après exploitation.

Dans le cas où le site comporte des installations de stockage de déchets inertes résultant de son exploitation, les garanties financières tiennent aussi compte de :

-la surveillance des installations de stockage de déchets inertes et de terres non polluées résultant de l'exploitation de la carrière lorsqu'elles sont susceptibles de donner lieu à un accident majeur à la suite d'une défaillance ou d'une mauvaise exploitation, tel que l'effondrement d'une verse ou la rupture d'une digue ;

-l'intervention en cas d'effondrement de verses ou de rupture de digues constituées de déchets inertes et de terres non polluées résultant de l'industrie extractive lorsque les conséquences sont susceptibles de donner lieu à un accident majeur.

3° Pour les installations mentionnées au 3° du I de l'article R. 516-1 :

a) Surveillance et maintien en sécurité de l'installation en cas d'événement exceptionnel susceptible d'affecter l'environnement ;

b) Interventions en cas d'accident ou de pollution.

4° Pour les sites de stockage mentionnés au 4° du I de l'article R. 516-1 :

a) Mise en œuvre des mesures prévues par le plan de postfermeture incluant notamment la mise à l'arrêt définitif du site et sa surveillance durant une période d'au moins trente ans après sa mise à l'arrêt définitif. Ce montant correspond au minimum au montant de la soulte prévu au d du I de l'article L. 229-47 ;

b) Interventions en cas de risques de fuites ou de fuites de dioxyde de carbone ou d'accident ou de pollution avant ou après la mise à l'arrêt définitif du site ;

c) La restitution, en cas de fuites, de quotas d'émissions de gaz à effet de serre.

5° Pour les installations mentionnées au 5° de l'article R. 516-1 :

a) Mise en sécurité du site de l'installation en application des dispositions mentionnées aux articles R. 512-39-1 et R. 512-46-25. Un arrêté du ministre chargé des installations classées fixe les modalités de détermination et d'actualisation du montant des garanties financières relatives à la mise en sécurité ;

b) Dans le cas d'une garantie additionnelle à constituer en application des dispositions du VI du présent article, mesures de gestion de la pollution des sols ou des eaux souterraines.

Indépendamment de la mise en jeu des garanties financières pour les opérations qu'elles couvrent, l'exploitant demeure tenu aux obligations mentionnées aux articles R. 512-39-1 à R. 512-39-3 et R. 512-46-25 à R. 512-46-28.

V.-Les garanties financières doivent être renouvelées au moins trois mois avant leur échéance.

VI.-Sans préjudice des obligations de l'exploitant en cas de cessation d'activité, le préfet peut demander, pour les installations visées au 5° de l'article R. 516-1, la constitution d'une garantie additionnelle en cas de survenance d'une pollution accidentelle significative des sols ou des eaux souterraines causée par l'exploitant postérieurement au 1er juillet 2012 et ne pouvant faire l'objet de façon immédiate, pour cause de contraintes techniques ou financières liées à l'exploitation du site, de toutes les mesures de gestion de la pollution des sols ou des eaux souterraines.

Un arrêté du ministre chargé des installations classées définit ces mesures de gestion.

La constitution ou la révision des garanties financières additionnelles est appréciée par le préfet au regard des capacités techniques et financières de l'exploitant et s'effectue dans les formes prévues au premier alinéa de l'article R. 516-5.
