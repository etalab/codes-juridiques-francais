# Sous-section 2 : Mesures à prendre préalablement à l'exécution des travaux

- [Article R554-24](article-r554-24.md)
- [Article R554-25](article-r554-25.md)
- [Article R554-26](article-r554-26.md)
- [Article R554-27](article-r554-27.md)
