# Article R555-52

Les décisions individuelles prises en application des dispositions du présent chapitre peuvent être déférées à la juridiction administrative :

a) Par les tiers, personnes physiques ou morales, les communes intéressées ou leurs groupements, en raison des inconvénients ou des dangers que le fonctionnement de la canalisation de transport présente pour les intérêts mentionnés au II de l'article L. 555-1 dans un délai d'un an à compter de la publication ou de l'affichage de ces décisions. Toutefois, si la mise en service de la canalisation de transport n'est pas intervenue six mois après la publication ou l'affichage de ces décisions, le délai de recours continue à courir jusqu'à l'expiration d'une période de six mois après cette mise en service ;

b) Par les pétitionnaires ou transporteurs, dans un délai de deux mois à compter de la date à laquelle la décision leur a été notifiée.
