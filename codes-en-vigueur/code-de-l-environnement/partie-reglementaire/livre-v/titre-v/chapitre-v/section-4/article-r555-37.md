# Article R555-37

Les prescriptions fixées par l'arrêté prévu à l'article L. 555-3 tiennent compte notamment, d'une part, de l'efficacité des meilleures techniques disponibles et de leur économie, d'autre part, de la qualité, de la vocation et de l'utilisation des milieux environnants, de la présence humaine qui y est recensée, des activités qui y sont exercées ainsi que de la préservation de la ressource en eau. Il fixe en outre, le cas échéant, les seuils à partir desquels ces prescriptions s'appliquent.

Il peut prévoir des dispositions spécifiques concernant le contrôle des installations intéressant la défense.
