# Article R553-10

Le délai mentionné au premier alinéa de l'article R. 512-74 peut être prorogé dans la limite d'un délai total de dix ans, incluant le délai initial de trois ans, par le représentant de l'Etat dans le département, sur demande de l'exploitant, en l'absence de changement substantiel de circonstances de fait et de droit ayant fondé l'autorisation, lorsque, pour des raisons indépendantes de sa volonté, l'exploitant n'a pu mettre en service son installation dans ce délai, le cas échéant après prorogation de l'enquête publique en application de l'article R. 123-24.

La prorogation de l'enquête publique mentionnée à l'alinéa précédent est acquise si aucune décision n'a été adressée à l'exploitant dans le délai de deux mois à compter de la date de l'avis de réception par le représentant de l'Etat dans le département.
