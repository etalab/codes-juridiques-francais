# Article R551-14

Outre celle prévue au 5° de l'article R. 512-6, des études de danger, au sens de l'article L. 551-1, sont prévues aux dispositions suivantes :

1° A l'article R. 542-20 ;

2° Au 3 du II de l'article 6 du décret n° 2006-649 du 2 juin 2006 relatif aux travaux miniers, au stockage souterrain et à la police des mines et des stockages souterrains ;

3° A l'article 8 du même décret ;

4° Aux articles 10,37 et 43 du décret n° 2007-1557 du 2 novembre 2007 relatif aux installations nucléaires de base et au contrôle, en matière de sûreté nucléaire, du transport de substances radioactives.
