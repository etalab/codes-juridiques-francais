# Sous-section 6 : Emprunt du territoire national lors des échanges entre Etats membres de la Communauté européenne et transit sur le territoire national

- [Article R542-59](article-r542-59.md)
- [Article R542-60](article-r542-60.md)
- [Article R542-61](article-r542-61.md)
- [Article R542-62](article-r542-62.md)
- [Article R542-63](article-r542-63.md)
