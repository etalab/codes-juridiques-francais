# Sous-section 5 : Exportation à destination d'un Etat n'appartenant pas à la Communauté européenne

- [Article R542-53](article-r542-53.md)
- [Article R542-54](article-r542-54.md)
- [Article R542-55](article-r542-55.md)
- [Article R542-56](article-r542-56.md)
- [Article R542-57](article-r542-57.md)
- [Article R542-58](article-r542-58.md)
