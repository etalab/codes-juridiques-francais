# Article R543-256

Sont chargés de contrôler l'application de la présente sous-section les fonctionnaires et agents de l'Etat mentionnés à l'article L. 541-44.
