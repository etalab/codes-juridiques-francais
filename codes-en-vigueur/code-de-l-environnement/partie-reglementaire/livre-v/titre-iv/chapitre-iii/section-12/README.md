# Section 12 : Déchets de produits textiles d'habillement, de chaussures ou de linge de maison destinés aux ménages

- [Sous-section 1 : Dispositions relatives à l'agrément des organismes visés à l'article L. 541-10-3](sous-section-1)
- [Sous-section 2 : Dispositions relatives à l'approbation des systèmes individuels visés à l'article L. 541-10-3](sous-section-2)
- [Sous-section 3 : Dispositions communes aux agréments et aux approbations](sous-section-3)
