# Sous-section 3 : Dispositions relatives à la collecte, à l'enlèvement, à l'entreposage et au traitement des déchets de bouteilles de gaz

- [Paragraphe 1 : Dispositions relatives aux modalités de collecte, d'enlèvement, d'entreposage et de traitement des déchets de bouteilles de gaz](paragraphe-1)
- [Paragraphe 2 : Dispositions relatives à l'approbation des systèmes individuels](paragraphe-2)
- [Paragraphe 3 : Dispositions relatives à l'agrément des éco-organismes](paragraphe-3)
- [Paragraphe 4 : Dispositions relatives à l'agrément de l'organisme coordonnateur](paragraphe-4)
