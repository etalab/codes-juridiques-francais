# Paragraphe 1 : Dispositions relatives aux modalités de collecte, d'enlèvement, d'entreposage et de traitement des déchets de bouteilles de gaz

- [Article R543-262](article-r543-262.md)
- [Article R543-263](article-r543-263.md)
- [Article R543-264](article-r543-264.md)
- [Article R543-265](article-r543-265.md)
