# Section 16 : Bouteilles rechargeables de gaz destinées   à un usage individuel et déchets de bouteilles de gaz

- [Sous-section 1 : Champ d'application et définitions](sous-section-1)
- [Sous-section 2 : Dispositions relatives à la prévention et à la mise en place d'une consigne ou d'un système de reprise équivalent](sous-section-2)
- [Sous-section 3 : Dispositions relatives à la collecte, à l'enlèvement, à l'entreposage et au traitement des déchets de bouteilles de gaz](sous-section-3)
- [Sous-section 4 : Dispositions relatives au suivi de la filière](sous-section-4)
- [Sous-section 5 : Sanctions administratives](sous-section-5)
