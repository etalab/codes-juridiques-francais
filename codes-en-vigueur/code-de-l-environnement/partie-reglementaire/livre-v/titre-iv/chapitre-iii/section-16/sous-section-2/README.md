# Sous-section 2 : Dispositions relatives à la prévention et à la mise en place d'une consigne ou d'un système de reprise équivalent

- [Paragraphe 1 : Dispositions relatives à la prévention de la production de déchets de bouteilles de gaz](paragraphe-1)
- [Paragraphe 2 : Dispositions relatives à la mise en place d'une consigne ou d'un système de reprise équivalent des bouteilles de gaz](paragraphe-2)
