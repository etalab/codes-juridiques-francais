# Sous-section 1 : Prise en compte des exigences liées à l'environnement dans la conception et la fabrication des emballages

- [Article R543-42](article-r543-42.md)
- [Article R543-43](article-r543-43.md)
- [Article R543-44](article-r543-44.md)
- [Article R543-45](article-r543-45.md)
- [Article R543-46](article-r543-46.md)
- [Article R543-47](article-r543-47.md)
- [Article R543-48](article-r543-48.md)
- [Article R543-49](article-r543-49.md)
- [Article R543-50](article-r543-50.md)
- [Article R543-51](article-r543-51.md)
- [Article R543-52](article-r543-52.md)
