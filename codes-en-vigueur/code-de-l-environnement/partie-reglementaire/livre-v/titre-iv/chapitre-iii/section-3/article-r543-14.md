# Article R543-14

Un cahier des charges prévoit, notamment, les conditions juridiques, financières et techniques dans lesquelles les exploitants d'une installation de traitement des huiles usagées s'acquittent de l'obligation qui leur incombe d'accepter et de traiter les huiles usagées qui leur sont présentées.
