# Sous-section 3 : Elimination des   déchets de piles et d'accumulateurs

- [Paragraphe 1 : Piles et accumulateurs portables](paragraphe-1)
- [Paragraphe 2 : Piles et accumulateurs automobiles](paragraphe-2)
- [Paragraphe 3 : Piles et accumulateurs industriels](paragraphe-3)
