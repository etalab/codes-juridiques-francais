# Paragraphe 1 : Piles et accumulateurs portables

- [Article R543-128-1](article-r543-128-1.md)
- [Article R543-128-2](article-r543-128-2.md)
- [Article R543-128-3](article-r543-128-3.md)
- [Article R543-128-4](article-r543-128-4.md)
- [Article R543-128-5](article-r543-128-5.md)
