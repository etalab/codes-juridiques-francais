# Paragraphe 2 : Piles et accumulateurs automobiles

- [Article R543-129-1](article-r543-129-1.md)
- [Article R543-129-2](article-r543-129-2.md)
- [Article R543-129-3](article-r543-129-3.md)
- [Article R543-129-4](article-r543-129-4.md)
