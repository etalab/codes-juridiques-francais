# Article R543-149

Les producteurs peuvent créer des organismes appropriés afin de remplir collectivement les obligations qui leur incombent en matière  de gestion des  déchets de pneumatiques.
