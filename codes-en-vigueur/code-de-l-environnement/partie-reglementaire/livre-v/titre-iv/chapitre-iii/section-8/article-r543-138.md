# Article R543-138

Pour l'application des dispositions de la présente section :

1° Sont considérées comme producteurs les personnes qui fabriquent, importent ou introduisent en France des pneumatiques, mettent sur le marché des pneumatiques à leur marque, importent ou introduisent des engins équipés de pneumatiques. Ne sont pas considérées comme producteurs les personnes effectuant du réemploi, du rechapage ou du recyclage ;

2° Sont considérées comme distributeurs les personnes qui vendent des pneumatiques ou des engins équipés de pneumatiques ;

3° Sont considérées comme détenteurs les personnes qui ont dans leur propre entreprise des déchets de pneumatiques en raison de leurs activités professionnelles ainsi que les communes ou leurs groupements, lorsque ces communes ou ces groupements ont procédé à la collecte séparée des déchets de pneumatiques ;

4° Sont considérées comme collecteurs les personnes qui assurent le ramassage, auprès des distributeurs et détenteurs, des déchets de pneumatiques, leur regroupement, leur tri ou leur transport jusqu'aux installations de traitement.
