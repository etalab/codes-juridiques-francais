# Sous-section 2 : Barème et modalités de calcul de la contribution financière et de son reversement

- [Article D543-211](article-d543-211.md)
- [Article D543-212](article-d543-212.md)
