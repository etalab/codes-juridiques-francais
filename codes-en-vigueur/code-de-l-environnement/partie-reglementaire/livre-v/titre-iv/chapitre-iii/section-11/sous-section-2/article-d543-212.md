# Article D543-212

Le soutien versé aux collectivités en application de l'article D. 543-210 est modulé pour tenir compte du mode de traitement des déchets issus des imprimés papiers et des papiers à usage graphique, transformés, manufacturés, conditionnés et destinés à être imprimés, conformément au tableau ci-dessous :

<div>
<table>
<tbody>
<tr>
<th>MODE DE TRAITEMENT DES DÉCHETS MÉNAGERS ET ASSIMILÉS,<br/>issus d'imprimés papiers et de papiers à usage graphique,<br/>transformés, manufacturés, conditionnés et destinés à être imprimés</th>
<th>
<br/>MONTANT DU SOUTIEN<br/>par tonne de déchets traités</th>
<th>
<br/>ANNÉE DE TRAITEMENT<br/>des déchets</th>
</tr>
<tr>
<td align="left">
<p>Recyclage matière</p>
</td>
<td align="center">
<br/>80 euros</td>
<td align="center">
<br/>2012 et suivantes</td>
</tr>
<tr>
<td align="left">
<p>Valorisation énergétique dans une installation d'incinération dont la performance énergétique, calculée selon les normes réglementaires en vigueur, est supérieure ou égale à 0,6 ; Compostage à des fins agricoles ou de végétalisation, ou méthanisation</p>
</td>
<td align="center">
<p>25 euros</p>
<p>20 euros</p>
</td>
<td align="center">
<p>2012 et 2013</p>
<p>2014 et suivantes</p>
</td>
</tr>
<tr>
<td align="left">
<p>Traitement thermique avec production d'énergie dans une installation d'incinération dont la performance énergétique, calculée selon les normes réglementaires en vigueur, est comprise entre 0,2 et 0,6</p>
</td>
<td align="center">
<br/>5 euros</td>
<td align="center">
<br/>2012 et suivantes</td>
</tr>
<tr>
<td align="left">
<p>Autre traitement</p>
</td>
<td align="center">
<br/>1 euro</td>
<td align="center">
<br/>2012 et suivantes</td>
</tr>
</tbody>
</table>
</div>
