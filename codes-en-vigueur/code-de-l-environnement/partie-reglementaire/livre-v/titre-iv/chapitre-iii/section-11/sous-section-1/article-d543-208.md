# Article D543-208

Les donneurs d'ordre mentionnés au I de l'article L. 541-10-1 déclarent, dans les conditions prévues à l'article D. 543-208-2, auprès de l'organisme mentionné à l'article D. 543-207 le tonnage d'imprimés papiers qu'ils ont émis ou fait émettre à destination des utilisateurs finaux, au cours de l'année civile précédente, à l'exclusion des imprimés réalisés à partir de papiers à usage graphique, transformés, manufacturés, conditionnés et destinés à être imprimés, mentionnés à l'article D. 543-208-1.

Les imprimés papiers que les donneurs d'ordre ont émis ou fait émettre, expédiés hors du territoire national ou ne générant pas de déchets ménagers et assimilés, sont exclus de l'assiette de la contribution.
