# Article D543-210

Les contributions reçues par l'organisme agréé sont reversées aux communes, aux établissements publics de coopération intercommunale ou aux syndicats mixtes compétents en fonction du tonnage de déchets issus d'imprimés papiers et de papiers à usage graphique, transformés, manufacturés, conditionnés et destinés à être imprimés, collectés sur leur territoire et traités durant l'année, déduction faite, le cas échéant, des contributions en nature versées aux établissements publics de coopération intercommunale.
