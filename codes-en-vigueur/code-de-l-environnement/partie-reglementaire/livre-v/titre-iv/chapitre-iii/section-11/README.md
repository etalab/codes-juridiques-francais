# Section 11 : Déchets d'imprimés papiers et de papiers à usage graphique destinés à être imprimés

- [Sous-section 1 : Modalités de gestion de la contribution à la collecte, à la valorisation et à l'élimination des déchets d'imprimés papiers et de papiers à usage graphique destinés à être imprimés](sous-section-1)
- [Sous-section 2 : Barème et modalités de calcul de la contribution financière et de son reversement](sous-section-2)
