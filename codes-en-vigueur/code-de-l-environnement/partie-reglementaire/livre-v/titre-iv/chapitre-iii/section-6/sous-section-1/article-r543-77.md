# Article R543-77

Les équipements mis sur le marché comportent, de façon lisible et indélébile, l'indication de la nature et de la quantité de fluide frigorigène qu'ils contiennent.

Pour les équipements à circuit hermétique, préchargés en fluide frigorigène, dont la mise en service consiste exclusivement en un raccordement à des réseaux électrique, hydraulique, ou aéraulique, les mentions prévues à l'alinéa 1er sont apposées par les producteurs de ces équipements. Pour tous les autres équipements, l'indication doit être apposée par les opérateurs réalisant la mise en service des équipements.

Les dispositions du présent article ne s'appliquent pas aux équipements de climatisation des voitures particulières au sens de l'article R. 311-1 du code de la route.

Les dispositions du présent article s'appliquent également aux équipements mis sur le marché après le 8 décembre 1992 et contenant une charge en fluide frigorigène supérieure à deux kilogrammes.
