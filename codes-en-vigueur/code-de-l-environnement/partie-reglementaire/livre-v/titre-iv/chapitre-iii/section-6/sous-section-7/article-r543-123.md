# Article R543-123

Est puni de l'amende prévue pour les contraventions de la 5e classe le fait :

1° Pour les détenteurs d'équipements, de ne pas faire contrôler l'étanchéité des équipements pour lesquels ce contrôle est obligatoire et de ne pas prendre toutes mesures pour mettre fin aux fuites constatées, en méconnaissance de l'article R. 543-79 ;

2° Pour tout producteur ou distributeur, d'importer, de mettre sur le marché ou de céder à titre onéreux ou gratuit des fluides frigorigènes conditionnés dans des emballages destinés à un usage unique, en méconnaissance de l'article R. 543-86 ;

3° Pour un opérateur ou un détenteur, de procéder à toute opération de dégazage dans l'atmosphère de fluides frigorigènes, sauf cas de nécessité pour assurer la sécurité des personnes, en méconnaissance de l'article R. 543-87 ;

4° Pour un opérateur, de ne pas procéder à la récupération intégrale des fluides frigorigènes lors de l'installation, de l'entretien, de la réparation ou du démantèlement d'un équipement, en méconnaissance de l'article R. 543-88 ;

5° Pour un opérateur, de procéder à toute opération de recharge en fluide frigorigène d'équipements présentant des défauts d'étanchéité, en méconnaissance de l'article R. 543-89, sauf dans le cas des exceptions prévues à l'article R. 543-90 ;

6° Pour un opérateur, de ne pas remettre aux distributeurs les fluides frigorigènes ou leurs emballages non traités sous sa responsabilité, en méconnaissance des dispositions des articles R. 543-92 et R. 543-93 ;

7° Pour un opérateur, de ne pas faire traiter sous sa responsabilité les fluides et emballages non remis aux distributeurs, contrairement aux dispositions des articles R. 543-92 et R. 543-93 ;

8° Pour les producteurs de fluides frigorigènes et d'équipements et les distributeurs, de ne pas procéder aux opérations de reprise sans frais supplémentaires, de collecte, de retraitement pour mise en conformité avec leurs spécifications d'origine permettant leur réutilisation ou de destruction intégrale des fluides frigorigènes ou de leurs emballages, contrairement à l'article R. 543-91 et aux articles R. 543-94 à R. 543-96 ;

9° Pour un opérateur de procéder à la mise en service, à l'entretien, la réparation ou la maintenance, lorsque ces opérations nécessitent une intervention quelconque sur le circuit frigorifique, au contrôle d'étanchéité ou au démantèlement des équipements, à la récupération et à la charge des fluides frigorigènes, ou à toute autre opération nécessitant la manipulation de fluides frigorigènes, sans être titulaire de l'attestation de capacité prévue à l'article R. 543-99, ni d'un certificat équivalent délivré dans un des Etats membres de l'Union européenne et traduit en français ;

10° Pour un distributeur, de céder à titre onéreux ou gratuit des fluides frigorigènes à un opérateur ne disposant ni de l'attestation de capacité, ni d'un certificat équivalent délivré dans un des Etats membres de l'Union européenne et traduit en français, contrairement aux dispositions de l'article R. 543-84.
