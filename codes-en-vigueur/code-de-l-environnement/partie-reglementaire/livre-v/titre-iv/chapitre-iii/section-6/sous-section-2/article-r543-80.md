# Article R543-80

Le détenteur d'un équipement contenant plus de trois kilogrammes de fluide frigorigène conserve pendant au moins cinq ans les documents attestant que les contrôles d'étanchéité ont été réalisés, constatant éventuellement l'existence de fuites et faisant état de ce que les réparations nécessaires ont été réalisées, et les tient à disposition des opérateurs intervenant ultérieurement sur l'équipement et de l'administration.
