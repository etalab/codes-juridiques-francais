# Sous-section 2 : Prévention des fuites de fluides frigorigènes

- [Article R543-78](article-r543-78.md)
- [Article R543-79](article-r543-79.md)
- [Article R543-80](article-r543-80.md)
- [Article R543-81](article-r543-81.md)
- [Article R543-82](article-r543-82.md)
- [Article R543-83](article-r543-83.md)
