# Article R543-153

Les règles régissant la construction des voitures particulières et des camionnettes et tendant à limiter l'utilisation de substances dangereuses et à faciliter le démontage et la dépollution de ces véhicules, notamment en vue de favoriser la valorisation de leurs composants et matériaux, sont énoncées à l'article R. 318-10 du code de la route.
