# Article R543-158

Lorsque l'instance prévue à l'article R. 543-157-1 constate un déséquilibre économique de la filière des véhicules hors d'usage ou un risque de non-atteinte des objectifs mentionnés à l'article R. 543-160, les ministres chargés, respectivement, de l'environnement et de l'industrie, après avoir apprécié les propositions formulées par cette instance, peuvent imposer :

1° Aux producteurs de reprendre ou de faire reprendre, au moins à prix nul, aux centres VHU et broyeurs agréés des pièces, substances ou matériaux issus des véhicules hors d'usage. Les modalités de mise en œuvre de cette reprise et la liste des pièces, substances ou matériaux concernés sont fixées par arrêté conjoint des ministres chargés, respectivement, de l'environnement et de l'industrie. Chaque producteur est tenu ensuite de réutiliser ou valoriser ou de faire réutiliser ou de faire valoriser les pièces, substances ou matériaux qu'il aura repris, conformément aux dispositions des articles R. 543-159 et R. 543-160.

2° A chaque producteur de verser, aux centres VHU ou broyeurs agréés, un soutien financier dont le montant et les modalités de mise en œuvre sont déterminés par un arrêté conjoint des ministres chargés, respectivement, de l'environnement et de l'industrie.

Les obligations imposées aux producteurs au titre des mises sur le marché de véhicules neufs en application des dispositions du présent article sont réparties au prorata des quantités de véhicules arrivés en fin de vie l'année précédente.

Les producteurs se conforment aux obligations issues du 1° et du 2° du présent article dans les conditions prévues au deuxième alinéa du II de l'article L. 541-10.

Chaque producteur ou groupement de producteurs doit présenter annuellement les modalités et les résultats des dispositifs de reprise et de soutien qu'il a mis en place à l'instance d'évaluation de l'équilibre économique définie à l'article R. 543-157-1.
