# Paragraphe 9 : Attestation de conformité des équipements électriques et électroniques

- [Article R543-171-10](article-r543-171-10.md)
- [Article R543-171-11](article-r543-171-11.md)
