# Sous-section 3 : Exigences minimales applicables aux transferts transfrontaliers d'équipements électriques et électroniques usagés

- [Article R543-206-1](article-r543-206-1.md)
- [Article R543-206-2](article-r543-206-2.md)
- [Article R543-206-3](article-r543-206-3.md)
- [Article R543-206-4](article-r543-206-4.md)
