# Article R543-205

Est puni de l'amende prévue pour les contraventions de 3e classe le fait :

1° Pour un producteur ou un mandataire d'un producteur établi dans un autre Etat membre :

a) De mettre sur le marché un équipement électrique et électronique sans respecter les dispositions prévues à l'article R. 543-177 ;

b) De ne pas informer les acheteurs par une mention sur les factures de vente de tout nouvel équipement électrique et électronique ménager du coût unitaire correspondant à la gestion des déchets d'équipements électriques et électroniques ménagers mis sur le marché avant le 13 août 2005, conformément à l'article L. 541-10-2 ;

c) De ne pas communiquer les informations prévues à l'article R. 543-178, au 1° du III de l'article R. 543-195 et à l'article R. 543-202 ;

2° Pour un distributeur, y compris en cas de vente à distance :

a) De ne pas assurer la reprise d'un équipement électrique et électronique usagé dont son détenteur se défait dans les conditions définies à l'article R. 543-180 ;

b) De ne pas informer les acheteurs, dans les conditions prévues à R. 543-194, du coût correspondant à la gestion des déchets d'équipements électriques et électroniques mis sur le marché avant le 13 août 2005.

3° De ne pas communiquer les informations prévues à l'article R. 543-202-1 pour les personnes définies dans l'arrêté prévu au dernier alinéa de cet article.
