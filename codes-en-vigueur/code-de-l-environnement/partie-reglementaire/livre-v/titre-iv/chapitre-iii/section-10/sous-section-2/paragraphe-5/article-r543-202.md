# Article R543-202

Un registre national des producteurs d'équipements électriques et électroniques recueille, notamment, les informations que transmettent les producteurs en ce qui concerne les quantités d'équipements électriques et électroniques qu'ils ont mis sur le marché et les modalités de gestion des déchets de ces équipements qu'ils ont mises en oeuvre.

L'Agence de l'environnement et de la maîtrise de l'énergie est chargée de la mise en place, de la tenue et de l'exploitation de ce registre.

Un arrêté conjoint des ministres chargés, respectivement, de l'environnement et de l'industrie fixe la procédure d'inscription à ce registre, les modalités de transmission et la nature des informations qui doivent y figurer.
