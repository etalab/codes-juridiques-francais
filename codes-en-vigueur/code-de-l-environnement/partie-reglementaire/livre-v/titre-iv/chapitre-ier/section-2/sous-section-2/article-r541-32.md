# Article R541-32

Dans le cas où, dans une région, aucun plan de prévention et de gestion des déchets dangereux n'a été établi, le préfet de région peut, par lettre motivée, inviter l'autorité compétente à élaborer le plan et à procéder à son évaluation environnementale dans un délai qu'il fixe.

A l'issue de ce délai, le préfet de région peut, par demande motivée, demander à l'autorité compétente de faire approuver le plan par le conseil régional.

Si, à l'expiration d'un délai de dix-huit mois suivant cette demande, le projet de plan n'a pas été approuvé, le préfet de la région, par arrêté motivé, se substitue à l'autorité compétente pour élaborer et approuver le plan dans les conditions de la présente sous-section. Cet arrêté est publié au recueil des actes administratifs de la préfecture de région et au recueil des délibérations du conseil régional.
