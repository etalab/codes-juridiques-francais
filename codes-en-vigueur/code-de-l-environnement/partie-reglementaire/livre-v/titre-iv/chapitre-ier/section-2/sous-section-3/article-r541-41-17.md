# Article R541-41-17

Les plans de prévention et de gestion des déchets issus de chantiers du bâtiment et des travaux publics faisant l'objet d'une procédure simplifiée de révision en application de l'article R. 541-41-16 ne donnent lieu qu'à une actualisation de l'évaluation environnementale réalisée lors de leur élaboration.
