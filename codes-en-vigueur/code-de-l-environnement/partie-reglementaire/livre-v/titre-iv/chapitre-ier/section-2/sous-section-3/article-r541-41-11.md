# Article R541-41-11

I. ― Le projet de plan, accompagné du rapport environnemental, est soumis à enquête publique réalisée conformément au chapitre III du titre II du livre Ier du présent code.

II. ― Le dossier d'enquête comprend :

1° Une notice explicative précisant l'objet de l'enquête, la portée du projet de plan et les justifications des principales mesures qu'il comporte ;

2° Le rapport environnemental ainsi que les avis émis sur le projet en application des articles R. 541-41-9 et R. 541-41-10.
