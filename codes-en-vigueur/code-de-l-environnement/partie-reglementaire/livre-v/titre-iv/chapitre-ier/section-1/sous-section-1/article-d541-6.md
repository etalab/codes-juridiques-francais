# Article D541-6

Le Conseil national des déchets se réunit sur convocation de son président, en tant que de besoin, et au moins deux fois par an.

Il publie périodiquement un rapport d'activité.
