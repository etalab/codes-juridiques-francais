# Article D541-2

I. - Le Conseil national des déchets comprend 38 membres répartis en 5 collèges :

1° Collège de l'Etat :

- deux représentants du ministre chargé de l'environnement ;

- six représentants, désignés sur proposition des ministres chargés respectivement du budget, de l'intérieur, de l'agriculture, de la santé, de la consommation et de l'industrie.

Les représentants du collège de l'Etat assistent aux délibérations du Conseil national des déchets avec voix consultative.

2° Collège des élus locaux :

- deux représentants désignés par l'Association des maires de France (AMF) ;

- un représentant désigné par l'Assemblée des communautés de France (AdCF) ;

- un représentant désigné par l'Association des maires de grandes villes de France (AMGVF) ;

- un représentant désigné par l'Association des petites villes de France (APVF) ;

- un représentant désigné par l'Association des régions de France (ARF) ;

- deux représentants désignés par l'Assemblée des départements de France (ADF).

3° Collège des associations :

- trois représentants d'associations nationales de consommateurs et d'usagers sur proposition du collège des consommateurs et des usagers du Conseil national de la consommation ;

- cinq représentants d'associations agréées de protection de l'environnement.

4° Collège des professionnels :

- trois représentants des professionnels du secteur traitement et recyclage des déchets ;

- trois représentants des producteurs et distributeurs ;

- un représentant de la Fédération nationale des syndicats d'exploitations agricoles ;

- deux représentants des organismes agréés pour l'élimination des déchets issus de certains produits.

5° Collège des salariés :

- cinq représentants.

II. - Sept personnalités qualifiées, dont une représentant l'Agence de l'environnement et de la maîtrise de l'énergie, assistent aux délibérations du Conseil national des déchets avec voix consultative.

III. - Les membres du conseil, leurs suppléants et les personnalités qualifiées sont nommés par arrêté du ministre chargé de l'environnement.
