# Sous-section 6 : Transferts transfrontaliers de déchets

- [Article R541-83](article-r541-83.md)
- [Article R541-84](article-r541-84.md)
- [Article R541-85](article-r541-85.md)
