# Article R541-46

Les exploitants des installations visées à l'article L. 214-1 soumises à autorisation ou à déclaration ou des installations visées à l'article L. 511-1 soumises à autorisation, à enregistrement ou à déclaration qui traitent des substances ou objets qui sont des déchets afin qu'ils cessent d'être des déchets conformément à l'article L. 541-4-3 tiennent un registre chronologique de la nature, du traitement et de l'expédition de ces substances ou objets. Ce registre est conservé pendant au moins cinq ans.

Ils fournissent à l'administration compétente une déclaration annuelle sur la nature et les quantités de ces substances ou objets qui quittent leur installation.
