# Section 4 : Dispositions particulières relatives à la défense nationale

- [Article R532-32](article-r532-32.md)
- [Article R532-33](article-r532-33.md)
- [Article R532-34](article-r532-34.md)
