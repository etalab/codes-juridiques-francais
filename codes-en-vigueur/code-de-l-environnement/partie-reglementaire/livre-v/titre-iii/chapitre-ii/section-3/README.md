# Section 3 : Dispositions relatives à l'utilisation confinée d'organismes génétiquement modifiés à des fins de production industrielle

- [Sous-section 1 : Dispositions relatives à l'agrément](sous-section-1)
- [Sous-section 2 : Dispositions relatives à la déclaration d'utilisation](sous-section-2)
- [Sous-section 3 : Dispositions communes à l'agrément et à la déclaration d'utilisation](sous-section-3)
- [Article R532-25](article-r532-25.md)
