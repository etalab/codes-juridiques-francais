# Article R531-15

Pour l'élaboration de ses avis sur les demandes d'agrément en vue de l'utilisation confinée d'organismes génétiquement modifiés, le haut conseil définit :

- des groupes d'organismes biologiques génétiquement modifiés, au regard de leurs dangers potentiels ;

- les critères d'assimilation à un groupe déterminé pour les organismes biologiques génétiquement modifiés ;

- des classes de confinement des utilisations confinées.
