# Sous-section 1 : Composition du Haut Conseil des biotechnologies

- [Article R531-8](article-r531-8.md)
- [Article R531-9](article-r531-9.md)
- [Article R531-10](article-r531-10.md)
- [Article R531-11](article-r531-11.md)
- [Article R531-12](article-r531-12.md)
- [Article R531-13](article-r531-13.md)
- [Article R531-13-1](article-r531-13-1.md)
