# Article R531-11

Les membres du comité scientifique élisent, parmi eux,       deux vice-présidents au scrutin uninominal majoritaire à un tour.
