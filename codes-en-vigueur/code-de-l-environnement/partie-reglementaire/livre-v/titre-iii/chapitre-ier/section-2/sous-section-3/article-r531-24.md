# Article R531-24

Le Haut Conseil des biotechnologies rend publics ses avis et recommandations, notamment par voie électronique. Ceux-ci font état des positions divergentes exprimées.

Le haut conseil préserve la confidentialité des informations qu'il est amené à connaître, notamment au regard des règles relatives à la protection de la propriété intellectuelle et industrielle. Ses membres, ceux du secrétariat ainsi que les experts ou toute autre personne consultée par le haut conseil, sont tenus au secret professionnel dans les conditions et sous les peines prévues à l'
article 226-13 du code pénal
.
