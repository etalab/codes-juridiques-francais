# Article R531-22

En cas de vacance ou d'empêchement du président du Haut Conseil des biotechnologies, le président du comité scientifique assure l'intérim, et transmet notamment les avis mentionnés à l'article L. 531-4 à l'autorité administrative compétente.
