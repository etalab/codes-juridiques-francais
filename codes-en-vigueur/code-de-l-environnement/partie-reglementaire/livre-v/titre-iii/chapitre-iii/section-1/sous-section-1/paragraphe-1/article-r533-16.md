# Article R533-16

Conformément à l'article L. 535-2, elle peut exiger du responsable de la dissémination qu'il modifie les conditions de celle-ci, qu'il la suspende ou qu'il y mette fin, et elle en informe le public.
