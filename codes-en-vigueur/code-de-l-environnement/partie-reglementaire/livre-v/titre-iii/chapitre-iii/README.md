# Chapitre III : Dissémination volontaire et mise sur le marché d'organismes génétiquement modifiés

- [Section 1 : Dissémination volontaire à toute autre fin que la mise sur le marché](section-1)
- [Section 2 : Mise sur le marché](section-2)
