# Paragraphe 1 : Utilisation confinée d'organismes génétiquement modifiés à des fins de recherche, de développement ou d'enseignement

- [Article R536-1](article-r536-1.md)
- [Article R536-2](article-r536-2.md)
- [Article R536-3](article-r536-3.md)
- [Article R536-4](article-r536-4.md)
- [Article R536-4-1](article-r536-4-1.md)
