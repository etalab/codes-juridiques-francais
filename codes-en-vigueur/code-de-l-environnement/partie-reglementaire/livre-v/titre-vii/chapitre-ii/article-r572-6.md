# Article R572-6

Un arrêté conjoint des ministres chargés, respectivement, de l'environnement, des transports et de l'équipement précise, en tant que de besoin, les dispositions techniques nécessaires à l'application du présent article.
