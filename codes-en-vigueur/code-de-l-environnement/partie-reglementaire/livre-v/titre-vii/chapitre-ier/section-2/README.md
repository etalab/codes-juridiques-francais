# Section 2 : Activités bruyantes

- [Sous-section 1 : Etablissements ou locaux recevant du public et diffusant à titre habituel de la musique amplifiée](sous-section-1)
- [Sous-section 2 : Bruits de voisinages](sous-section-2)
- [Sous-section 3 : Mouvements d'hélicoptères](sous-section-3)
