# Article R571-27

Lorsque ces établissements ou locaux sont soit contigus de bâtiments comportant des locaux à usage d'habitation ou destinés à un usage impliquant la présence prolongée de personnes, soit situés à l'intérieur de tels bâtiments, l'isolement entre le local d'émission et le local ou le bâtiment de réception doit être conforme à une valeur minimale, fixée par arrêté, qui permette de respecter les valeurs maximales d'émergence mentionnées à l'article R. 1334-33 du code de la santé publique.

Dans les octaves normalisées de 125 Hz à 4 000 Hz, ces valeurs maximales d'émergence ne peuvent être supérieures à 3 dB.

Dans le cas où l'isolement du local où s'exerce l'activité est insuffisant pour respecter ces valeurs maximales d'émergence, l'activité de diffusion de musique amplifiée ne peut s'exercer qu'après la mise en place d'un limiteur de pression acoustique réglé et scellé par son installateur.
