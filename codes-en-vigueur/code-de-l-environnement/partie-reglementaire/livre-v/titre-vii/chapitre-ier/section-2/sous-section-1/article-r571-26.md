# Article R571-26

En aucun endroit, accessible au public, de ces établissements ou locaux, le niveau de pression acoustique ne doit dépasser 105 dB (A) en niveau moyen et 120 dB en niveau de crête, dans les conditions de mesurage prévues par arrêté.
