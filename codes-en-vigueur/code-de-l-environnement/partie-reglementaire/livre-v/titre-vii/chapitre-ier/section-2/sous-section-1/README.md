# Sous-section 1 : Etablissements ou locaux recevant du public et diffusant à titre habituel de la musique amplifiée

- [Article R571-25](article-r571-25.md)
- [Article R571-26](article-r571-26.md)
- [Article R571-27](article-r571-27.md)
- [Article R571-28](article-r571-28.md)
- [Article R571-29](article-r571-29.md)
- [Article R571-30](article-r571-30.md)
