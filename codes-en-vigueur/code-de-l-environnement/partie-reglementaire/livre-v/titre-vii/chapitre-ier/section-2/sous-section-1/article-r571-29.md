# Article R571-29

I.-L'exploitant d'un établissement mentionné à l'article R. 571-25 est tenu d'établir une étude de l'impact des nuisances sonores comportant les documents suivants :

1° L'étude acoustique ayant permis d'estimer les niveaux de pression acoustique, tant à l'intérieur qu'à l'extérieur des locaux, et sur le fondement de laquelle ont été effectués, par l'exploitant, les travaux d'isolation acoustique nécessaires ;

2° La description des dispositions prises pour limiter le niveau sonore et les émergences aux valeurs fixées par la présente sous-section, notamment par des travaux d'isolation phonique et l'installation d'un limiteur de pression acoustique.

II.-Ces documents doivent être mis à jour en cas de modification de l'installation.

III.-En cas de contrôle, l'exploitant doit être en mesure de présenter le dossier d'étude d'impact aux agents mentionnés aux articles L. 571-18 à L. 571-20.
