# Sous-section 2 : Sanctions

- [Paragraphe 1 : Emissions sonores des objets.](paragraphe-1)
- [Paragraphe 2 : Etablissements ou locaux recevant du public et diffusant à titre habituel de la musique amplifiée.](paragraphe-2)
- [Paragraphe 3 : Bruits de voisinage.](paragraphe-3)
- [Paragraphe 4 : Mouvements d'hélicoptères](paragraphe-4)
