# Article D510-4

Le président et le vice-président du Conseil supérieur de la prévention des risques technologiques sont nommés par arrêté du ministre chargé de l'environnement parmi les membres de ce conseil.

Son secrétaire général est nommé par arrêté du ministre chargé de l'environnement parmi les membres de la direction ou de la direction générale chargée de la prévention des risques au ministère chargé de l'environnement. Il a voix consultative.
