# Sous-section 2 : Affichage d'opinion.

- [Article R581-2](article-r581-2.md)
- [Article R581-3](article-r581-3.md)
- [Article R581-4](article-r581-4.md)
- [Article R581-5](article-r581-5.md)
