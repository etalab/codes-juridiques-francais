# Article R581-16

I.-La demande de l'autorisation d'installer une enseigne prévue à l'avant-dernier alinéa de l'article L. 581-18, comporte, outre les informations et pièces énumérées par l'article R. 581-7 :

1° Une mise en situation de l'enseigne ;

2° Une vue de l'immeuble ou du lieu concerné avec et sans l'enseigne ;

3° Une appréciation sur son intégration dans l'environnement.

II.-L'autorisation d'installer une enseigne prévue à l'avant-dernier alinéa de l'article L. 581-18 est délivrée par l'autorité compétente en matière de police :

1° Après accord de l'architecte des Bâtiments de France lorsque cette installation est envisagée sur un immeuble classé ou inscrit au titre des monuments historiques ou dans le champ de visibilité de cet immeuble défini par l'article L. 621-30 du code du patrimoine ;

2° Après accord du préfet de région, lorsque cette installation est envisagée sur un monument naturel, dans un site classé, un cœur de parc national, une réserve naturelle ou sur un arbre ;

3° Après accord de l'architecte des Bâtiments de France émis dans les conditions fixées par l'article L. 313-2 du code de l'urbanisme, lorsque cette installation est envisagée dans un secteur sauvegardé ;

4° Après avis de l'architecte des Bâtiments de France émis dans les conditions fixées par l'article L. 642-6 du code du patrimoine, lorsque cette installation est envisagée dans une zone de protection du patrimoine architectural, urbain ou paysager ou une aire de mise en valeur de l'architecture et du patrimoine.
