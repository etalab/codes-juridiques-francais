# Sous-section 3 : Procédures de déclaration et d'autorisation préalable

- [Paragraphe 1 : Déclaration préalable](paragraphe-1)
- [Paragraphe 2 : Dispositions générales applicables aux autorisations préalables](paragraphe-2)
- [Paragraphe 3 : Dispositions particulières applicables à certaines déclarations et autorisations préalables](paragraphe-3)
