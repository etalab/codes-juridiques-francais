# Section 6 : Sanctions

- [Sous-section 1 : Procédure administrative.](sous-section-1)
- [Sous-section 2 : Sanctions pénales.](sous-section-2)
