# Article R581-34

La publicité lumineuse est la publicité à la réalisation de laquelle participe une source lumineuse spécialement prévue à cet effet.

La publicité lumineuse ne peut être autorisée à l'intérieur des agglomérations de moins de 10 000 habitants ne faisant pas partie d'une unité urbaine de plus de 100 000 habitants.

A l'intérieur des agglomérations de plus de 10 000 habitants et dans celles de moins de 10 000 habitants faisant partie d'une unité urbaine de plus de 100 000 habitants, ainsi qu'à l'intérieur de l'emprise des aéroports et des gares ferroviaires situés hors agglomération, la publicité lumineuse apposée sur un mur, scellée au sol ou installée directement sur le sol ne peut avoir une surface unitaire excédant 8 mètres carrés, ni s'élever à plus de 6 mètres au-dessus du niveau du sol.

La publicité lumineuse respecte des normes techniques fixées par arrêté ministériel, portant notamment sur les seuils maximaux de luminance, exprimés en candelas par mètre carré, et sur l'efficacité lumineuse des sources utilisées, exprimée en lumens par watt.

Les dispositions des deuxième et troisième alinéas et des articles R. 581-36 à R. 581-41 ne sont pas applicables aux dispositifs de publicité lumineuse ne supportant que des affiches éclairées par projection ou par transparence, lesquels sont soumis aux dispositions des articles R. 581-26 à R. 581-33.
