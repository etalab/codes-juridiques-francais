# Sous-section 1 : Dispositions générales applicables à toutes publicités

- [Article R581-22](article-r581-22.md)
- [Article R581-23](article-r581-23.md)
- [Article R581-24](article-r581-24.md)
