# Section 1 : Dispositions générales

- [Sous-section 1 : Prélèvements, analyses et essais](sous-section-1)
- [Sous-section 2 : Protection du secret de la formule intégrale des mélanges](sous-section-2)
- [Sous-section 3 : Modalités d'application des sanctions administratives](sous-section-3)
- [Sous-section 4 : Sanctions pénales](sous-section-4)
- [Article R521-1](article-r521-1.md)
