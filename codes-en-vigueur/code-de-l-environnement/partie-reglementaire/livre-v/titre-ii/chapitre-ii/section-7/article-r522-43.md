# Article R522-43

I.-Est puni de la peine d'amende prévue pour les contraventions de la 5e classe le fait :

1° De mettre à disposition sur le marché un produit biocide sans avoir déclaré les informations prévues à l'article R. 522-32 ;

2° De mettre à disposition sur le marché un produit biocide sans mettre à jour la déclaration prévue au I de l'article L. 522-2 dans les conditions prévues au premier alinéa de l'article R. 522-33 ;

3° De mettre à disposition sur le marché un produit biocide sans faire figurer les indications d'étiquetage prévues par l'article 69 du règlement (UE) n° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 précité ou par l'arrêté prévu à l'article R. 522-38 ;

4° De mettre à disposition sur le marché un article traité sans faire figurer les indications d'étiquetage prévues par l'article 58 du règlement (UE) n° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 précité ;

5° De mettre à disposition sur le marché un produit biocide sans avoir procédé à la notification prévue au 1 de l'article 27 du règlement (UE) n° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 précité, dans les conditions prévues par cet article ;

6° De diffuser une publicité pour un produit biocide en méconnaissance des dispositions de l'article 72 du règlement (UE) n° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 précité ;

7° De mettre à disposition sur le marché un produit sans avoir fourni les informations nécessaires sur ce produit, mentionnées au II de l'article L. 522-2.

La récidive est réprimée conformément aux articles 132-11 et 132-15 du code pénal.

II.-Est puni de la peine d'amende prévue pour les contraventions de la 3e classe le fait :

1° De mettre à disposition sur le marché un produit biocide sans mettre à jour la déclaration prévue au I de l'article L. 522-2 dans les conditions prévues au second alinéa de l'article R. 522-33 ;

2° De mettre à disposition sur le marché un produit biocide sans procéder à la déclaration prévue à l'article L. 522-3 ;

3° De ne pas mettre à disposition les informations relatives au traitement biocide de l'article traité conformément au 5 de l'article 58 du règlement (UE) n° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 précité.
