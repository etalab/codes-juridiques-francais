# Article R522-22

Les modifications intervenues sur les décisions d'autorisation de mise à disposition sur le marché concernant les produits de référence, liées à des mesures de gestion des risques en vue de les atténuer ou lorsqu'elles sont prises pour des motifs de santé publique ou de protection des travailleurs ou de l'environnement, s'appliquent aux produits relevant de la procédure prévue à l'article R. 522-21 et aux produits bénéficiant d'un permis de commerce parallèle.
