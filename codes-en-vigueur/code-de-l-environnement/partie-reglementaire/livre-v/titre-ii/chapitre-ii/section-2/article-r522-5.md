# Article R522-5

Les décisions relatives aux autorisations de mise à disposition sur le marché des produits biocides ainsi que les décisions de renouvellement ou de retrait de ces autorisations prises dans le cadre des procédures prévues par le règlement (UE) n° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 précité et ses règlements d'exécution sont délivrées par le ministre chargé de l'environnement.

Sauf dispositions spécifiques prévues au présent chapitre, ces décisions sont délivrées après consultation de l'Agence nationale.

Les dossiers relatifs aux demandes d'autorisation ou de renouvellement d'autorisation de mise à disposition sur le marché de produits biocides et les éventuels éléments complémentaires nécessaires à l'examen de ces demandes sont transmis au moyen du registre mentionné au 3 de l'article 71 du règlement (UE) n° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 précité.
