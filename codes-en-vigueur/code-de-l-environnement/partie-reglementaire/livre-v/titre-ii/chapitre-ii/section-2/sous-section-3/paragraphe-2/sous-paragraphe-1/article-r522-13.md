# Article R522-13

I.-Dans le cas où la France est Etat membre de référence et où la demande de modification d'une autorisation de mise à disposition sur le marché d'un produit biocide est mineure au sens du titre 2 de l'annexe au règlement d'exécution (UE) n° 354/2013 de la Commission du 18 avril 2013 déjà cité, l'Agence nationale transmet au ministre chargé de l'environnement, dans un délai de 75 jours à compter de la validation de la demande dans les conditions précisées au 3 de l'article 7 du même règlement, le rapport d'évaluation du produit et met à jour, en français et en anglais, le projet de résumé des caractéristiques du produit issu du registre des produits biocides. Ce délai est prolongé le cas échéant du délai suspensif prévu au 5 de l'article 7 de ce même règlement.

Si l'Agence nationale conclut que les conditions prévues à l'article 19 du règlement (UE) n° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 déjà cité ne sont pas remplies, elle transmet au ministre chargé de l'environnement des conclusions motivées en ce sens et un rapport d'évaluation du produit.

II.-Lorsque l'Agence nationale n'a pas transmis les documents mentionnés au I dans les délais qui y sont mentionnés, elle est réputée avoir émis des conclusions défavorables sur la demande présentée.
