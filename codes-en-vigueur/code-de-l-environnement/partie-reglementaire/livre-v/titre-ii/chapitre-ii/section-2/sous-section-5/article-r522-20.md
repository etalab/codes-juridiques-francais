# Article R522-20

Dans un délai de 60 jours à compter de l'acceptation d'une demande d'autorisation simplifiée de mise à disposition sur le marché d'un produit biocide dans les conditions prévues au 2 de l'article 26 du règlement (UE) n° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 précité et si le produit remplit les conditions prévues à l'article 25 de ce même règlement, l'Agence nationale transmet au ministre chargé de l'environnement un projet de résumé des caractéristiques du produit en français et en anglais ainsi qu'un rapport d'évaluation de ce produit. Ce délai est prolongé le cas échéant du délai suspensif prévu au 4 de l'article 26 du règlement précité.

Si l'Agence nationale conclut que les conditions prévues à l'article 25 du règlement (UE) n° 528/2012 ne sont pas remplies, elle transmet au ministre chargé de l'environnement des conclusions motivées en ce sens ainsi qu'un rapport d'évaluation de ce produit.

Lorsque l'Agence nationale n'a pas transmis les documents prévus dans les délais mentionnés au premier alinéa, elle est réputée avoir émis des conclusions défavorables sur la demande présentée.
