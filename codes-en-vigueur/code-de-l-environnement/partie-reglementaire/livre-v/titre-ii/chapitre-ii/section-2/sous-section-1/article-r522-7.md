# Article R522-7

La demande de renouvellement d'une autorisation nationale de mise à disposition sur le marché prévue à l'article 31 du règlement (UE) n° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 précité est traitée selon la procédure prévue à l'article R. 522-6. Toutefois, si l'Agence nationale considère qu'une évaluation exhaustive n'est pas nécessaire, le délai de 305 jours mentionné à cet article est ramené à 120 jours.
