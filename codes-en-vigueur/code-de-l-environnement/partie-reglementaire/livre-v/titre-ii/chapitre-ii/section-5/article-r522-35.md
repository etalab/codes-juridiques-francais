# Article R522-35

En application de l'article L. 522-3, les quantités de produits biocides mises sur le marché entre le 1er janvier et le 31 décembre sont déclarées chaque année au ministre chargé de l'environnement par voie électronique, avant le 1er avril de l'année suivante.
