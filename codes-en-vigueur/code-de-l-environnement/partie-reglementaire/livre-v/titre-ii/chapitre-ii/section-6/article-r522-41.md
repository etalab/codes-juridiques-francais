# Article R522-41

La mise à disposition sur le marché ou l'utilisation d'un produit biocide en application du 1 de l'article 55 du règlement (UE) n° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 précité est autorisée par le ministre chargé de l'environnement qui, sauf en cas d'urgence, consulte l'Agence nationale et, le cas échéant, la commission des produits chimiques et biocides.
