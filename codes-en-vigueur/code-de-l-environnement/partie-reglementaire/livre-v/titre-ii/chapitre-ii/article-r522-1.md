# Article R522-1

I.-L'approbation des substances actives biocides, la mise à disposition sur le marché des produits biocides et des articles traités par ces produits de même que leur expérimentation dans les conditions énoncées au I de l'article L. 522-1 sont soumises aux dispositions du présent chapitre.

II.-Le ministre chargé de l'environnement est, sauf disposition contraire, l'autorité compétente mentionnée au 1 de l'article 81 du règlement (UE) n° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 modifié concernant la mise à disposition sur le marché et l'utilisation des produits biocides.
