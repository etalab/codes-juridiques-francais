# Section 1 : Autorités compétentes pour la mise en oeuvre des règlements communautaires

- [Sous-section 1 : Evaluation et contrôle des risques présentés par les substances existantes](sous-section-1)
- [Sous-section 2 : Exportations et importations de certains produits chimiques dangereux](sous-section-2)
- [Sous-section 3 : Substances qui appauvrissent la couche d'ozone](sous-section-3)
