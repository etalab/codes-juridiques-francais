# Chapitre III : Dispositions communes aux produits chimiques et biocides

- [Section 1 : Autorités compétentes pour la mise en oeuvre des règlements communautaires](section-1)
- [Section 2 : Commission des produits chimiques et biocides](section-2)
- [Section 3 : Groupe interministériel des produits chimiques](section-3)
