# Section 3 : Groupe interministériel des produits chimiques

- [Article D523-8](article-d523-8.md)
- [Article D523-9](article-d523-9.md)
- [Article D523-10](article-d523-10.md)
- [Article D523-11](article-d523-11.md)
