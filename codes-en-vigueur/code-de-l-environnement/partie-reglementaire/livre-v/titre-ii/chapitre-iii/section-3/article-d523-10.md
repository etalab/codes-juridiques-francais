# Article D523-10

Chaque année, le groupe interministériel des produits chimiques établit un rapport relatif aux applications des bonnes pratiques de laboratoire en France pour les essais mentionnés à l'article D. 523-8. Ce rapport contient une liste des laboratoires inspectés, la date à laquelle ces inspections ont été faites et un bref résumé des conclusions des inspections. Il est transmis aux services compétents de la Commission européenne et de l'Organisation de coopération et développement économique (OCDE).
