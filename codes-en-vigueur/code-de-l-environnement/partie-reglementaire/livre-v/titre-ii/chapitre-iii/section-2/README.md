# Section 2 : Commission des produits chimiques et biocides

- [Article R523-4](article-r523-4.md)
- [Article R523-5](article-r523-5.md)
- [Article R523-6](article-r523-6.md)
- [Article R523-7](article-r523-7.md)
