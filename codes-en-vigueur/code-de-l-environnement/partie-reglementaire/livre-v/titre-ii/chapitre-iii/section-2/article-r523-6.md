# Article R523-6

Le secrétariat de la commission est assuré par la direction de la prévention des pollutions et des risques du ministère chargé de l'environnement. Les membres du secrétariat assistent aux réunions de la commission.

La commission se réunit sur convocation de son président.

Pour l'examen des dossiers, le président désigne un ou plusieurs rapporteurs parmi les membres de la commission. La commission peut décider d'entendre toute personne de son choix. Elle peut créer des groupes de travail spécialisés dont elle fixe la composition et le mandat.

Les membres de la commission, ainsi que toute personne qu'elle consulte, sont tenus de respecter la confidentialité des informations qu'ils sont amenés à connaître.

Les avis de la commission sont émis à la majorité des membres présents ou représentés. En cas de partage égal des voix, la voix du président est prépondérante.

La commission adopte son règlement intérieur qui est soumis à l'approbation du ministre chargé de l'environnement.
