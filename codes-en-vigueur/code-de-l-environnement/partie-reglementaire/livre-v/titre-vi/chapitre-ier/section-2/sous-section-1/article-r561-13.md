# Article R561-13

Les ministres chargés de la prévention des risques majeurs et de l'économie fixent par arrêté conjoint, compte tenu des disponibilités du fonds, le montant des sommes à affecter au paiement ou à la consignation d'indemnités d'expropriation et au paiement de travaux.

La caisse centrale de réassurance transfère les sommes ainsi fixées au   directeur départemental ou, le cas échéant, régional des finances publiques ou, à Saint-Pierre-et-Miquelon, au directeur des finances publiques de chaque département concerné.

S'agissant des dépenses mentionnées aux 6° à 10° de l'article R. 561-8, les sommes sont fixées et transférées dans les conditions prévues aux alinéas précédents. Le préfet du département concerné engage et ordonnance ces sommes.
