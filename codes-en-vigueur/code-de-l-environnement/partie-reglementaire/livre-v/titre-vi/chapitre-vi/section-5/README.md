# Section 5 : Stratégies locales

- [Article R566-14](article-r566-14.md)
- [Article R566-15](article-r566-15.md)
- [Article R566-16](article-r566-16.md)
- [Article R566-17](article-r566-17.md)
