# Article R371-35

Les dispositions d'application des articles L. 371-1 à L. 371-4 à La Réunion, en Martinique, en Guadeloupe, en Guyane et à Mayotte sont précisées par l'article R. 4433-2-1 du code général des collectivités territoriales.
