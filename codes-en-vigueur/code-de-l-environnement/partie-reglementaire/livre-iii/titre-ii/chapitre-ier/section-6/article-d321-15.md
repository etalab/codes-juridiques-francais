# Article D321-15

La taxe prévue à l'article 285 quater du code des douanes et due par les entreprises de transport public maritime est perçue à l'occasion de l'embarquement des passagers à destination des espaces naturels protégés ou des ports les desservant exclusivement ou principalement qui figurent dans le tableau ci-après.

Pour chacun de ces espaces, le tableau précise la ou les personnes publiques dont le budget bénéficie du produit net de la taxe ainsi que, le cas échéant, la répartition de ce produit entre elles.

Liste des espaces protégés et des ports les desservant exclusivement ou principalement à destination desquels est perçue la taxe sur les passagers maritimes prévue par l'article 285 quater du code des douanes

<table>
<tbody>
<tr>
<td align="center" valign="middle">
<p align="center">Liste des espaces protégés et des ports<br/>les desservant exclusivement ou principalement </p>
</td>
<td width="182">
<p align="center">Personnes publiques bénéficiaires du produit<br/>net de la taxe </p>
</td>
<td align="center" valign="bottom">
<p align="center">Part du produit net de la taxe revenant à chaque personne publique </p>
</td>
</tr>
<tr>
<td align="center" colspan="3" valign="middle">
<p align="center">1. Parcs nationaux </p>
</td>
</tr>
<tr>
<td align="left" valign="top">
<p align="left">Parc national de la Guadeloupe : îlet Pigeon, îlets du Grand Cul de Sac marin, mangroves du Grand Cul de Sac marin classés en cœur de parc national ainsi que l'aire maritime adjacente (Guadeloupe).</p>
</td>
<td align="left" valign="top">
<p align="left">Etablissement public du parc national de la Guadeloupe. </p>
</td>
<td align="left" valign="top">
<p align="center">100 %</p>
</td>
</tr>
<tr>
<td>
<p align="left">Parc national des Calanques (Bouches-du-Rhône)</p>
</td>
<td>Etablissement public du Parc national des Calanques</td>
<td>
<p align="center">100 %</p>
</td>
</tr>
<tr>
<td align="center" colspan="3" valign="middle">
<p align="center">2. Réserves naturelles </p>
</td>
</tr>
<tr>
<td width="284">
<p>Réserve naturelle nationale du Banc d'Arguin (Gironde).</p>
</td>
<td width="182">
<p>Commune de La Teste-de-Buch.</p>
</td>
<td align="center" valign="middle">
<p align="center">100 % </p>
</td>
</tr>
<tr>
<td width="284">
<p>Réserve naturelle nationale des bouches de Bonifacio (Corse-du-Sud).</p>
</td>
<td width="182">
<p>Office de l'environnement de la Corse. </p>
</td>
<td align="center" valign="middle">
<p align="center">100 % </p>
</td>
</tr>
<tr>
<td width="284">
<p>Réserve naturelle nationale de Saint-Martin (Guadeloupe).</p>
</td>
<td width="182">
<p>Conservatoire de l'espace littoral et des rivages lacustres.</p>
</td>
<td align="center" valign="middle">
<p align="center">100 % </p>
</td>
</tr>
<tr>
<td>
<p align="left">Réserve naturelle nationale de la Désirade (Guadeloupe).</p>
</td>
<td>Office national des forêts. </td>
<td>
<p align="center">100 %</p>
</td>
</tr>
<tr>
<td>
<p align="left">Réserve naturelle nationale de l'île du Grand-Connétable (Guyane). </p>
</td>
<td>
<p align="left">Office national de la chasse et de la faune sauvage. </p>
</td>
<td>
<p align="center">100 %</p>
</td>
</tr>
<tr>
<td>
<p align="left">Réserve naturelle nationale marine de Cerbère-Banyuls (Pyrénées-Orientales). </p>
</td>
<td>Département des Pyrénées-Orientales. </td>
<td>
<p align="center">100 %</p>
</td>
</tr>
<tr>
<td align="left" valign="top">
<p align="left">Réserve naturelle nationale marine de La Réunion. </p>
</td>
<td align="left" valign="top">
<p align="left">Groupement d'intérêt public de la réserve naturelle marine de La Réunion. </p>
</td>
<td align="left" valign="top">
<p align="center">100 %</p>
</td>
</tr>
<tr>
<td align="center" colspan="3" valign="middle">
<p align="center">3.1. Sites naturels classés </p>
</td>
</tr>
<tr>
<td width="284">
<p>Sites classés de l'île de Bréhat et port de Bréhat (Côtes-d'Armor). </p>
</td>
<td width="182">
<p>Commune de l'île de Bréhat. </p>
</td>
<td align="center" valign="middle">
<p align="center">100 % </p>
</td>
</tr>
<tr>
<td width="284">
<p>Sites classés de l'île d'Ouessant et port de Lampaul (Finistère). </p>
</td>
<td width="182">
<p>Syndicat mixte du parc naturel régional d'Armorique. </p>
</td>
<td align="center" valign="middle">
<p align="center">100 % </p>
</td>
</tr>
<tr>
<td width="284">
<p>Sites classés de l'île de Sein et port de l'île de Sein (Finistère). </p>
</td>
<td width="182">
<p>Syndicat mixte du parc naturel régional d'Armorique. </p>
</td>
<td align="center" valign="middle">
<p align="center">100 % </p>
</td>
</tr>
<tr>
<td width="284">
<p>Sites classés de l'île d'Yeu, port Joinville et port de La Meule (Vendée). </p>
</td>
<td width="182">
<p>Commune de l'île d'Yeu. </p>
</td>
<td align="center" valign="middle">
<p align="center">100 % </p>
</td>
</tr>
<tr>
<td width="284">
<p>Sites classés des îles de Lérins : îles Sainte-Marguerite et Saint-Honorat (Alpes-Maritimes). </p>
</td>
<td width="182">
<p>Office national des forêts. </p>
</td>
<td align="center" valign="middle">
<p align="center">100 % </p>
</td>
</tr>
<tr>
<td width="284">
<p>Sites classés des îles Sanguinaires (Corse-du-Sud). </p>
</td>
<td width="182">
<p>Département de la Corse-du-Sud. </p>
</td>
<td align="center" valign="middle">
<p align="center">100 % </p>
</td>
</tr>
<tr>
<td width="284">
<p>Sites classés du Pain de sucre et de la baie de Pompierre à Terre-de-Haut (archipel des Saintes à la Guadeloupe). </p>
</td>
<td width="182">
<p>Commune de Terre-de-Haut. </p>
</td>
<td align="center" valign="middle">
<p align="center">100 % </p>
</td>
</tr>
<tr>
<td width="284">
<p>Sites classés des falaises nord-est de Marie-Galante (Guadeloupe). </p>
</td>
<td width="182">
<p>Communauté de communes du pays Marie-Galante. </p>
</td>
<td align="center" valign="middle">
<p align="center">100 % </p>
</td>
</tr>
<tr>
<td>
<p align="left">Sites classés du cap Oullestrell situé sur les communes de Banyuls-sur-Mer et Port-Vendres ainsi que le domaine public correspondant maritime (Pyrénées-Orientales).</p>
</td>
<td width="182">
<p>Département des Pyrénées-Orientales.</p>
</td>
<td align="center" valign="middle">
<p align="center">100 %</p>
</td>
</tr>
<tr>
<td>
<p align="left">Sites classés du cap Béar et ses abords (Pyrénées Orientales).</p>
</td>
<td>
<p align="left">Département des Pyrénées-Orientales.</p>
</td>
<td>
<p align="center">100 %</p>
</td>
</tr>
<tr>
<td>
<p align="left">Sites classés du cap de l'Abeille (Pyrénées Orientales). </p>
</td>
<td>
<p align="left">Département des Pyrénées-Orientales.</p>
</td>
<td>
<p align="center">100 %</p>
</td>
</tr>
<tr>
<td/>
<td/>
<td/>
</tr>
<tr>
<td colspan="3" width="605">
<p align="center">3.2. Sites naturels inscrits </p>
</td>
</tr>
<tr>
<td width="284">
<p>Ile d'Arz (Morbihan). </p>
</td>
<td width="182">
<p>Commune de l'île d'Arz. </p>
</td>
<td align="center" valign="middle">
<p align="center">100 % </p>
</td>
</tr>
<tr>
<td>
<p align="left">Ilet Madame (Martinique).</p>
</td>
<td>
<p align="left">Commune du Robert.</p>
</td>
<td>
<p align="center">100 %</p>
</td>
</tr>
<tr>
<td colspan="3" width="605">
<p align="center">4. Terrains du Conservatoire de l'espace littoral et des rivages lacustres </p>
</td>
</tr>
<tr>
<td width="284">
<p>Ile Tatihou (Manche). </p>
</td>
<td width="182">
<p>Conservatoire de l'espace littoral et des rivages lacustres. </p>
</td>
<td align="center" valign="middle">
<p align="center">100 % </p>
</td>
</tr>
<tr>
<td width="284">
<p>Ile-aux-Moines du golfe du Morbihan (Morbihan). </p>
</td>
<td width="182">
<p>Conservatoire de l'espace littoral et des rivages lacustres. </p>
</td>
<td align="center" valign="middle">
<p align="center">100 % </p>
</td>
</tr>
<tr>
<td>
<p align="left">Désert des Agriates et plage du Loto (Haute-Corse). </p>
</td>
<td>
<p align="left">Conservatoire de l'espace littoral et des rivages lacustres. </p>
</td>
<td>
<p align="center">100 % </p>
</td>
</tr>
<tr>
<td width="284">
<p>Iles de Petite-Terre (Guadeloupe). </p>
</td>
<td width="182">
<p>Conservatoire de l'espace littoral et des rivages lacustres. </p>
</td>
<td align="center" valign="middle">
<p align="center">100 % </p>
</td>
</tr>
<tr>
<td width="284">
<p>Iles du Salut (Guyane). </p>
</td>
<td width="182">
<p>Conservatoire de l'espace littoral et des rivages lacustres. </p>
</td>
<td align="center" valign="middle">
<p align="center">100 % </p>
</td>
</tr>
<tr>
<td colspan="3" width="605">
<p align="center">5. Espaces naturels bénéficiant de plusieurs protections </p>
</td>
</tr>
<tr>
<td align="center" rowspan="2" valign="middle">
<p align="left">Espaces terrestres et marins classés au titre de l'article L. 341-2 de Porto et de Girolata et territoires classés de la réserve naturelle nationale de la presqu'île de Scandola (Corse-du-Sud).</p>
</td>
<td width="182">
<p>Syndicat mixte du parc naturel régional de Corse. </p>
</td>
<td align="center" valign="middle">
<p align="center">67 % </p>
</td>
</tr>
<tr>
<td>
<p align="left">Commune d'Osani. </p>
</td>
<td>
<p align="center">33 %</p>
</td>
</tr>
<tr>
<td align="left" valign="top">
<p align="left">Espaces terrestres et marins classés en réserve naturelle nationale des Sept-Iles et terrains du Conservatoire de l'espace littoral et des rivages lacustres situés sur l'île aux Moines (Côtes-d'Armor).</p>
</td>
<td align="left" valign="top">
<p align="left">Conservatoire de l'espace littoral et des rivages lacustres. </p>
</td>
<td align="left" valign="top">
<p align="center">100 %</p>
</td>
</tr>
<tr>
<td>
<p align="left">Espaces terrestres et marins classés au titre de l'article L. 341-2, territoires classés de la réserve naturelle nationale d'Iroise situés dans l'archipel de Molène et port de Molène (Finistère).</p>
</td>
<td>
<p align="left">Syndicat mixte du parc naturel régional d'Armorique. </p>
</td>
<td>
<p align="center">100 %</p>
</td>
</tr>
<tr>
<td align="center" valign="top">
<p align="left">Espaces terrestres et marins classés au titre de l'article L. 341-2 et territoires classés de la réserve naturelle nationale de Saint-Nicolas-de-Glénan situés sur l'archipel de Glénan, ainsi que le port de l'île de Saint-Nicolas (Finistère).</p>
</td>
<td align="center" valign="top">
<p align="left">Département du Finistère. </p>
</td>
<td align="center" valign="top">
<p align="center">100 %</p>
</td>
</tr>
<tr>
<td rowspan="2">
<p align="left">Espaces terrestres et marins classés au titre de l'article L. 341-1 et terrains du Conservatoire de l'espace littoral et des rivages lacustres situés sur l'île de Batz (Finistère).</p>
</td>
<td>
<p align="left">Commune de Batz. </p>
</td>
<td>
<p align="center">50 %</p>
</td>
</tr>
<tr>
<td>
<p align="left">Conservatoire de l'espace littoral et des rivages lacustres.</p>
</td>
<td>
<p align="center">50 %</p>
</td>
</tr>
<tr>
<td align="left" valign="top">
<p align="left">Espaces terrestres et marins classés au titre de l'article L. 341-2, territoires classés de la réserve naturelle nationale François-le-Bail, port Tudy, port Lay, port Mélite et terrains du Conservatoire de l'espace littoral et des rivages lacustres situés sur l'île de Groix (Morbihan).</p>
</td>
<td align="left" valign="top">
<p align="left">Commune de Groix. </p>
</td>
<td align="left" valign="top">
<p align="center">100 %</p>
</td>
</tr>
<tr>
<td align="left" rowspan="2" valign="top">
<p align="left">Espaces terrestres et marins classés au titre de l'article L. 341-2, terrains du Conservatoire de l'espace littoral et des rivages lacustres situés sur Belle-Ile, ainsi que port du Palais et port de Sauzon (Morbihan).</p>
</td>
<td align="left" valign="top">
<p align="left">District de Belle-Ile-en-mer.</p>
</td>
<td align="left" valign="top">
<p align="center">80 % </p>
</td>
</tr>
<tr>
<td>
<p align="left">Conservatoire de l'espace littoral et des rivages lacustres. </p>
</td>
<td>
<p align="center">20 %</p>
</td>
</tr>
<tr>
<td align="left" rowspan="2" valign="top">
<p align="left">Espaces terrestres et marins classés au titre de l'article L. 341-2, terrains du Conservatoire de l'espace littoral et des rivages lacustres situés sur l'île d'Hoedic, ainsi que port de l'île d'Hoedic (Morbihan).</p>
</td>
<td align="left" valign="top">
<p align="left">Commune de Hoedic. </p>
</td>
<td align="left" valign="top">
<p align="center">60 %</p>
</td>
</tr>
<tr>
<td>
<p align="left">Conservatoire de l'espace littoral et des rivages lacustres. </p>
</td>
<td>
<p align="center">40 %</p>
</td>
</tr>
<tr>
<td align="left" rowspan="2" valign="top">
<p align="left">Espaces terrestres et marins classés au titre de l'article L. 341-2, terrains du Conservatoire de l'espace littoral et des rivages lacustres situés sur l'île de Houat, ainsi que port Saint-Gildas (Morbihan).</p>
</td>
<td align="left" valign="top">
<p align="left">Commune de Houat. </p>
</td>
<td align="left" valign="top">
<p align="center">80 %</p>
</td>
</tr>
<tr>
<td>
<p align="left">Conservatoire de l'espace littoral et des rivages lacustres. </p>
</td>
<td>
<p align="center">20 %</p>
</td>
</tr>
<tr>
<td>
<p align="left">Espaces terrestres classés au titre de l'article L. 341-2 et terrains du Conservatoire de l'espace littoral et des rivages lacustres situés sur l'archipel de Chausey (Manche).</p>
</td>
<td>
<p align="left">Commune de Granville.</p>
</td>
<td>
<p align="center">100 %</p>
</td>
</tr>
<tr>
<td align="left" rowspan="2" valign="top">
<p align="left">Espaces terrestres et marins classés au titre de l'article L. 341-2, terrains du Conservatoire de l'espace littoral et des rivages lacustres situés sur l'île d'Aix, ainsi que port de la Rade (Charente-Maritime).</p>
</td>
<td align="left" valign="top">
<p align="left">Commune de l'île d'Aix. </p>
</td>
<td align="left" valign="top">
<p align="center">80 %</p>
</td>
</tr>
<tr>
<td>
<p align="left">Conservatoire de l'espace littoral et des rivages lacustres. </p>
</td>
<td>
<p align="center">20 %</p>
</td>
</tr>
<tr>
<td align="left" valign="top">
<p align="left">Parc national de Port-Cros : </p>
<p>- île de Port-Cros (dont le port de Port-Cros), île de Bagaud, île de la Gabinière, classés en cœur de parc national, ainsi que l'aire maritime adjacente (Var) ; </p>
<p>- espaces terrestres et maritimes situés sur l'île de Porquerolles et classés en cœur de parc national, en aire maritime adjacente (dont le port de Porquerolles) et en site classé au titre de l'article L. 341-2 du code de l'environnement (Var).</p>
</td>
<td align="left" valign="top">
<p align="left">Etablissement public du parc national de Port-Cros.</p>
</td>
<td align="left" valign="top">
<p align="center">100 %</p>
</td>
</tr>
<tr>
<td/>
<td/>
<td/>
</tr>
</tbody>
</table>
