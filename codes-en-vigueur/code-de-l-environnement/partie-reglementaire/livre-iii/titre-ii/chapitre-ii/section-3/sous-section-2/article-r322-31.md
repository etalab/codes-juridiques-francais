# Article R322-31

La composition des conseils de rivage est fixée conformément au tableau suivant.

Les conseillers régionaux, généraux et territoriaux qui en font partie sont désignés par leur assemblée respective.

Le mandat des membres des conseils de rivage est d'une durée de trois ans. Toutefois, il prend fin de plein droit à l'expiration du mandat électif au titre duquel ils ont été désignés. En cas de vacance, le remplacement est opéré suivant les règles prévues par l'article R. 322-19. Le mandat des membres du conseil de rivage est renouvelable.

<table>
<tbody>
<tr>
<td rowspan="2" width="295"/>
<td colspan="2" width="310">
<p align="center">Nombre de conseillers </p>
</td>
</tr>
<tr>
<td width="207">
<p align="center">Régionaux </p>
</td>
<td width="103">
<p align="center">Généraux </p>
</td>
</tr>
<tr>
<td width="295">
<p align="center">I.-Rivages de Nord-Pas-de-Calais-Picardie </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Région Nord-Pas-de-Calais </p>
</td>
<td width="207">
<p align="center">4 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Nord </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Pas-de-Calais </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Région Picardie </p>
</td>
<td width="207">
<p align="center">2 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Somme </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Totaux </p>
</td>
<td width="207">
<p align="center">6 </p>
</td>
<td width="103">
<p align="center">6 </p>
</td>
</tr>
<tr>
<td width="295">
<p align="center">II.-Rivages de Normandie </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Région Haute-Normandie </p>
</td>
<td width="207">
<p align="center">4 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Seine-Maritime </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Eure </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Région Basse-Normandie </p>
</td>
<td width="207">
<p align="center">4 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Calvados </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Manche </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Totaux </p>
</td>
<td width="207">
<p align="center">8 </p>
</td>
<td width="103">
<p align="center">8 </p>
</td>
</tr>
<tr>
<td width="295">
<p align="center">III.-Rivages de Bretagne-Pays de la Loire </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Région Bretagne </p>
</td>
<td width="207">
<p align="center">4 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Ille-et-Vilaine </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Côtes-d'Armor </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Finistère </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Morbihan </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Région Pays de la Loire </p>
</td>
<td width="207">
<p align="center">2 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Loire-Atlantique (y compris le lac de Grandlieu) </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Vendée </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Totaux </p>
</td>
<td width="207">
<p align="center">6 </p>
</td>
<td width="103">
<p align="center">6 </p>
</td>
</tr>
<tr>
<td width="295">
<p align="center">IV.-Rivages de Centre-Atlantique </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Région Poitou-Charentes </p>
</td>
<td width="207">
<p align="center">2 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Charente-Maritime </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Région Aquitaine </p>
</td>
<td width="207">
<p align="center">6 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Gironde </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Landes </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Pyrénées-Atlantiques </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Totaux </p>
</td>
<td width="207">
<p align="center">8 </p>
</td>
<td width="103">
<p align="center">8 </p>
</td>
</tr>
<tr>
<td width="295">
<p align="center">V.-Rivages de la Méditerranée </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Région Provence-Alpes-Côte d'Azur </p>
</td>
<td width="207">
<p align="center">3 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Alpes-Maritimes </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Var </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Bouches-du-Rhône </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Région Languedoc-Roussillon </p>
</td>
<td width="207">
<p align="center">4 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Gard </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Hérault </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Aude </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Pyrénées-Orientales </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Totaux </p>
</td>
<td width="207">
<p align="center">7 </p>
</td>
<td width="103">
<p align="center">7 </p>
</td>
</tr>
<tr>
<td width="295">
<p align="center">VI.-Rivage de la Corse </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Collectivité territoriale de Corse </p>
</td>
<td width="207">
<p align="center">6 conseillers à l'Assemblée de Corse </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Haute-Corse </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">3 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Corse-du-Sud </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">3 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Totaux </p>
</td>
<td width="207">
<p align="center">6 </p>
</td>
<td width="103">
<p align="center">6 </p>
</td>
</tr>
<tr>
<td width="295">
<p align="center">VII.-Rivages français d'Amérique </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Région Martinique </p>
</td>
<td width="207">
<p align="center">2 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Martinique </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Région Guadeloupe </p>
</td>
<td width="207">
<p align="center">2 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Guadeloupe </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Région Guyane </p>
</td>
<td width="207">
<p align="center">2 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Guyane </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Collectivité territoriale de Saint-Pierre-et-Miquelon </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">2 </p>
</td>
</tr>
<tr>
<td>Collectivité de Saint-Barthélemy<br/>
</td>
<td/>
<td align="center">2</td>
</tr>
<tr>
<td>Collectivité de Saint-Martin<br/>
</td>
<td/>
<td align="center">2</td>
</tr>
<tr>
<td width="295">
<p>Totaux </p>
</td>
<td width="207">
<p align="center">6 </p>
</td>
<td width="103">
<p align="center">12 </p>
</td>
</tr>
<tr>
<td width="295">
<p align="center">VIII.-Rivages français de l'océan Indien </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Région Réunion </p>
</td>
<td width="207">
<p align="center">4 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Réunion </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">4 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Collectivité départementale de Mayotte </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">4 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Totaux </p>
</td>
<td width="207">
<p align="center">4 </p>
</td>
<td width="103">
<p align="center">8 </p>
</td>
</tr>
<tr>
<td width="295">
<p align="center">IX.-Rivages des lacs </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Région Midi-Pyrénées </p>
</td>
<td width="207">
<p align="center">1 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Aveyron </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Région Auvergne </p>
</td>
<td width="207">
<p align="center">2 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Cantal </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Puy-de-Dôme </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Région Limousin </p>
</td>
<td width="207">
<p align="center">3 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Corrèze </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Creuse </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Haute-Vienne </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Région Champagne-Ardenne </p>
</td>
<td width="207">
<p align="center">3 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Aube </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Haute-Marne </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Marne </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Région Franche-Comté </p>
</td>
<td width="207">
<p align="center">1 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Jura </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Région Rhône-Alpes </p>
</td>
<td width="207">
<p align="center">2 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Savoie </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Haute-Savoie </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Région Languedoc-Roussillon </p>
</td>
<td width="207">
<p align="center">1 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Lozère </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Région Lorraine </p>
</td>
<td width="207">
<p align="center">2 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Meuse </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Meurthe-et-Moselle </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Région Provence-Côte d'Azur </p>
</td>
<td width="207">
<p align="center">3 </p>
</td>
<td width="103">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="295">
<p>Hautes-Alpes </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Alpes-de-Haute-Provence </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Var </p>
</td>
<td width="207">
<br/>
<br/>
</td>
<td width="103">
<p align="center">1 </p>
</td>
</tr>
<tr>
<td width="295">
<p>Totaux </p>
</td>
<td width="207">
<p align="center">18 </p>
</td>
<td width="103">
<p align="center">18</p>
</td>
</tr>
</tbody>
</table>
