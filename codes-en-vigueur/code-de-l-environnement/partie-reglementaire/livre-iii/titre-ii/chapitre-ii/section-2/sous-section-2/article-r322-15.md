# Article R322-15

Les gardes du littoral chargés des missions prévues à l'article L. 322-10-1 sont commissionnés et assermentés dans les conditions définies aux articles R. 172-2 à R. 172-7.
