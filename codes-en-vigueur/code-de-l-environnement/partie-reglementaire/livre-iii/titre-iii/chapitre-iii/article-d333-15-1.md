# Article D333-15-1

Les indemnités maximales votées en application du III de l'article L. 333-3 par les organes délibérants des syndicats mixtes d'aménagement et de gestion des parcs naturels régionaux pour l'exercice effectif des fonctions de président ou de vice-président sont déterminées en appliquant au montant du traitement mensuel correspondant à l'indice brut terminal de l'échelle indiciaire de la fonction publique les barèmes suivants :

<table>
<tbody>
<tr>
<td rowspan="2" width="270">
<p align="center">Superficie (en hectares) </p>
</td>
<td colspan="2" width="335">
<p align="center">Taux en pourcentage de l'indice brut 1015 </p>
</td>
</tr>
<tr>
<td width="182">
<p align="center">Président </p>
</td>
<td width="153">
<p align="center">Vice-président </p>
</td>
</tr>
<tr>
<td width="270">
<p>De 0 à 49 999 </p>
</td>
<td width="182">
<p align="center">27 </p>
</td>
<td width="153">
<p align="center">11 </p>
</td>
</tr>
<tr>
<td width="270">
<p>De 50 000 à 99 999 </p>
</td>
<td width="182">
<p align="center">29 </p>
</td>
<td width="153">
<p align="center">13 </p>
</td>
</tr>
<tr>
<td width="270">
<p>De 100 000 à 199 999 </p>
</td>
<td width="182">
<p align="center">31 </p>
</td>
<td width="153">
<p align="center">15 </p>
</td>
</tr>
<tr>
<td width="270">
<p>Plus de 200 000 </p>
</td>
<td width="182">
<p align="center">33 </p>
</td>
<td width="153">
<p align="center">17 </p>
</td>
</tr>
</tbody>
</table>

La superficie prise en compte est celle cadastrée et non cadastrée " hors eaux " du territoire géré par le syndicat mixte du parc naturel régional.
