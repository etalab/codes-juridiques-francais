# Article R333-8

Le projet de charte approuvé, accompagné des accords des collectivités territoriales et des établissements mentionnés à l'article R. 333-7, est transmis par le préfet de région, avec son avis motivé, au ministre chargé de l'environnement.
