# Article R333-14

I. ― Le syndicat mixte d'aménagement et de gestion du parc naturel régional, dans le cadre fixé par la charte, assure sur le territoire du parc la cohérence et la coordination des actions de protection, de mise en valeur, de suivi, d'évaluation, de gestion, d'animation et de développement menées par les collectivités territoriales et les établissements publics de coopération intercommunale à fiscalité propre ayant approuvé la charte, par l'Etat et par les partenaires associés. Lors de la procédure de renouvellement de classement, il rédige le projet de charte et organise la concertation.

II.-Il peut participer à un programme d'actions en mer contribuant à la réalisation des orientations retenues par la charte pour les zones littorales et les zones maritimes du parc. Les modalités de cette participation sont définies par une convention passée avec les autorités de l'Etat compétentes.

III.-Il est associé à l'élaboration des schémas de cohérence territoriale et des plans locaux d'urbanisme en application de l'article L. 121-4 du code de l'urbanisme, dans les conditions définies aux chapitres II et III du titre II du livre 1er de ce code.

Il est saisi pour avis par l'autorité compétente pour prendre la décision d'autorisation, d'approbation ou d'exécution du projet du formulaire de demande d'examen au cas par cas défini à l'article R. 122-3 ou, le cas échéant, de l'étude d'impact définie à l'article R. 122-5 lorsque des travaux, ouvrages ou aménagements soumis à ces procédures en vertu de l'article R. 122-2 sont envisagés sur le territoire du parc.

Il est consulté lors de l'élaboration ou de la révision des documents figurant sur la liste fixée par l'article R. 333-15.

Il est saisi de l'étude d'impact lorsque des aménagements, ouvrages ou travaux soumis à cette procédure en vertu des articles L. 122-1 à L. 122-3 et R. 122-1 à R. 122-16 sont envisagés sur le territoire du parc.

Le comité syndical du parc naturel régional peut déléguer à son bureau ou au président du parc le soin d'émettre les avis sollicités dans les cas mentionnés aux deux alinéas précédents.
