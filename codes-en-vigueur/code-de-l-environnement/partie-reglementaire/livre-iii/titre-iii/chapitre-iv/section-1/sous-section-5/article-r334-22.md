# Article R334-22

Il peut être constitué au sein de l'agence des régies de recettes et des régies de dépenses dans les conditions prévues par le décret n° 92-681 du 20 juillet 1992 modifié relatif aux régies de recettes et d'avances des organismes publics.
