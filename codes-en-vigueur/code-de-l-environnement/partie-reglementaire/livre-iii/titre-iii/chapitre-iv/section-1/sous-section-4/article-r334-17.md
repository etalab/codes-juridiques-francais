# Article R334-17

Le conseil scientifique est consulté sur les projets de création des parcs naturels marins et leurs plans de gestion.

Il peut être consulté par le président du conseil d'administration ou le directeur de l'agence sur toute question relative aux missions de l'agence ou à un parc naturel marin.

Il fait des recommandations sur la constitution du réseau national d'aires marines protégées et sur la création d'aires marines protégées internationales, ainsi que sur toute question sur laquelle il estime nécessaire d'attirer l'attention du conseil d'administration ou du directeur de l'agence.
