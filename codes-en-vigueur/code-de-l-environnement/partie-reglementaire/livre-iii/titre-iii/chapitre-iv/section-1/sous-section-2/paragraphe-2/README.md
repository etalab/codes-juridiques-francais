# Paragraphe 2 : Attributions

- [Article R334-8](article-r334-8.md)
- [Article R334-9](article-r334-9.md)
- [Article R334-10](article-r334-10.md)
