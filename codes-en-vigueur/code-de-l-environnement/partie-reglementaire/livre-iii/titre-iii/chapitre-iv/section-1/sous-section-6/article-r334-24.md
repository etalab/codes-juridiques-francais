# Article R334-24

Le commissaire du Gouvernement reçoit les convocations adressées aux membres du conseil d'administration et du bureau et siège avec voix consultative à toutes les réunions de ces instances ainsi qu'à celles des commissions qu'ils ont constituées.

Il peut demander l'inscription de questions à l'ordre du jour du conseil d'administration.

Il reçoit copie des délibérations du conseil d'administration et, s'il le demande, des décisions prises sur délégation de ce conseil.
