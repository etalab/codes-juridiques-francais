# Sous-section 4 : Dispositions plus favorables pour certaines catégories de personnes

- [Article R331-20](article-r331-20.md)
- [Article R331-21](article-r331-21.md)
