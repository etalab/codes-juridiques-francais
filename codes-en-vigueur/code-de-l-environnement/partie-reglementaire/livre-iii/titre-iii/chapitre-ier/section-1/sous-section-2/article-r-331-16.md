# Article R*331-16

Les modifications qui ne portent pas atteinte à l'économie générale des objectifs ou orientations de la charte sont approuvées par le conseil d'administration de l'établissement public du parc à la majorité des deux tiers, après consultation des personnes mentionnées au premier alinéa de l'article R. 331-4.

Toutefois, si les modifications envisagées portent sur les règles relatives à l'affectation et l'occupation des sols, il est procédé à une enquête publique dans les communes intéressées.

La charte modifiée fait l'objet des mesures de publicité prévues à l'article R. 331-12.
