# Sous-section 4 : Modification de l'état ou de l'aspect d'une réserve naturelle

- [Paragraphe 1 : Réserves naturelles classées par la collectivité territoriale de Corse](paragraphe-1)
- [Paragraphe 2 : Réserves naturelles classées en Corse par l'Etat](paragraphe-2)
