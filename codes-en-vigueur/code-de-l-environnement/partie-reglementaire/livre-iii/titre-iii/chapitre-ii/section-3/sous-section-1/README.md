# Sous-section 1 : Classement

- [Paragraphe 1 : Classement à l'initiative de la collectivité territoriale de Corse](paragraphe-1)
- [Paragraphe 2 : Classement à la demande de l'Etat ou en vertu de son pouvoir de substitution](paragraphe-2)
- [Paragraphe 3 : Publicité](paragraphe-3)
