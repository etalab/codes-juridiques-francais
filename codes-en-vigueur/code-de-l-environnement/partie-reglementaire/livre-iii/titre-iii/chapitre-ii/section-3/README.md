# Section 3 : Réserves naturelles en Corse

- [Sous-section 1 : Classement](sous-section-1)
- [Sous-section 2 : Modification des limites ou de la réglementation - Déclassement](sous-section-2)
- [Sous-section 3 : Gestion](sous-section-3)
- [Sous-section 4 : Modification de l'état ou de l'aspect d'une réserve naturelle](sous-section-4)
- [Sous-section 5 : Périmètres de protection](sous-section-5)
