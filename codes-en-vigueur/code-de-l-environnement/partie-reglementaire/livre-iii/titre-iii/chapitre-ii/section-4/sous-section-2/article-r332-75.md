# Article R332-75

Est puni des peines prévues pour les contraventions de la 5e classe le fait de s'opposer à la visite de véhicules non clos, sacs, paniers ouverts, poches à gibier ou boîtes à herboriser, par les agents habilités à constater les infractions à la présente section.
