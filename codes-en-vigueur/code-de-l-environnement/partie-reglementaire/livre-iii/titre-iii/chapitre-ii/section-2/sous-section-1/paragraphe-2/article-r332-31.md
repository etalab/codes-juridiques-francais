# Article R332-31

Le président du conseil régional consulte le conseil scientifique régional du patrimoine naturel, les collectivités territoriales dont le territoire est affecté par le projet de classement ainsi que, en zone de montagne, le comité de massif.

Il transmet le dossier au préfet de région qui lui indique si l'Etat envisage la constitution d'une réserve naturelle nationale ou de toute autre forme de protection réglementaire sur le même site et qui l'informe des projets de grands travaux et d'équipements susceptibles d'être implantés sur le territoire de la réserve, ainsi que des servitudes d'utilité publique applicables au même territoire.
