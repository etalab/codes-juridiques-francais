# Article R133-9

En cas d'absence ou d'empêchement du membre titulaire et de son suppléant, les membres nommés ne peuvent se faire représenter aux séances du conseil que par un autre membre de celui-ci à qui ils donnent pouvoir.
