# Article R133-7

Le conseil national se réunit sur convocation de son président ou de son vice-président et au moins deux fois par an. Il peut également être réuni sur la demande de quatorze de ses membres.

Le conseil ne peut valablement délibérer que si dix-huit au moins de ses membres assistent à la séance ou, pour les membres de droit, sont représentés.
