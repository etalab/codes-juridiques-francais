# Article R133-14

Le comité se réunit toutes les fois qu'il est nécessaire et au moins quatre fois par an sur convocation de son président ou à la demande du ministre.
