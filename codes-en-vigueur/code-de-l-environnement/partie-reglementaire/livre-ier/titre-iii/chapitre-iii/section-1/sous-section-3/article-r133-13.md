# Article R133-13

Le comité élit un président, un vice-président et un secrétaire général. Ces élections sont soumises à l'approbation du ministre.
