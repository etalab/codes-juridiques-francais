# Article R133-16

Le comité permanent est chargé de procéder à l'étude préalable de toutes les questions qui sont soumises à l'avis du conseil national. Il désigne à cet effet en son sein ou au sein du conseil national un rapporteur qui peut s'adjoindre des experts pris à l'extérieur du conseil.
