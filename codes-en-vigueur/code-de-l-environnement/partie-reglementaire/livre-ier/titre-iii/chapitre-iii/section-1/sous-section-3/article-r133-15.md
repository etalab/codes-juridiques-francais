# Article R133-15

Les avis du comité sont émis à la majorité des membres présents ; en cas de partage égal des voix, la voix du président est prépondérante.
