# Chapitre unique : Taxe générale sur les activités polluantes

- [Article D151-3](article-d151-3.md)
- [Article R151-1](article-r151-1.md)
- [Article R151-2](article-r151-2.md)
