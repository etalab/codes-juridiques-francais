# Titre VI : Prévention et réparation de certains dommages causés à l'environnement

- [Chapitre Ier : Champ d'application](chapitre-ier)
- [Chapitre II : Régime de responsabilité](chapitre-ii)
- [Chapitre III : Dispositions pénales](chapitre-iii)
