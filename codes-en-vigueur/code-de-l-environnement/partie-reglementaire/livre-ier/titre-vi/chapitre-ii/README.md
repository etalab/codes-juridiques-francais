# Chapitre II : Régime de responsabilité

- [Section 1 : Principes](section-1)
- [Section 2 : Mesures de prévention ou de réparation des dommages](section-2)
- [Section 3 :  Pouvoirs de police administrative](section-3)
