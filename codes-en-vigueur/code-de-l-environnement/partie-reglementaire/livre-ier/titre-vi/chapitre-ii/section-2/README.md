# Section 2 : Mesures de prévention ou de réparation des dommages

- [Sous-section 1 : Dispositions communes](sous-section-1)
- [Sous-section 2 : Menace de dommage](sous-section-2)
- [Sous-section 3 : Mesures en cas de dommage](sous-section-3)
