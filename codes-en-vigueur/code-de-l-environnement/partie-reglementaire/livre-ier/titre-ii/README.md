# Titre II : Information et participation des citoyens

- [Chapitre Ier : Débat public relatif aux opérations d'aménagement.](chapitre-ier)
- [Chapitre II : Evaluation environnementale](chapitre-ii)
- [Chapitre III : Enquêtes publiques relatives aux opérations susceptibles d'affecter l'environnement](chapitre-iii)
- [Chapitre V : Autres modes d'information](chapitre-v)
- [Chapitre VI : Déclaration de projet](chapitre-vi)
- [Chapitre VII : De l'infrastructure d'information géographique](chapitre-vii)
- [Article D120-1](article-d120-1.md)
