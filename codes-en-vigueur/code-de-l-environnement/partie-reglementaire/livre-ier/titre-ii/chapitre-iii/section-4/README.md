# Section 4 : Etablissement des listes d'aptitude aux fonctions de commissaire enquêteur

- [Sous-section 1 : Commission départementale chargée d'établir la liste d'aptitude aux fonctions de commissaire enquêteur](sous-section-1)
- [Sous-section 2 : Liste d'aptitude aux fonctions de commissaire enquêteur](sous-section-2)
