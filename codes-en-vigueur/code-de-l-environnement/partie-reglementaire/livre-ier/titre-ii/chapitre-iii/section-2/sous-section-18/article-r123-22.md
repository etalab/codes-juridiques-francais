# Article R123-22

L'enquête publique poursuivie à la suite d'une suspension autorisée conformément au I de l'article L. 123-14 est menée, si possible, par le même commissaire enquêteur ou la même commission d'enquête. Elle fait l'objet d'un nouvel arrêté d'organisation, d'une nouvelle publicité, et, pour les projets, d'une nouvelle information des communes conformément à l'article R. 123-12.

L'enquête est prolongée d'une durée d'au moins trente jours.

Le dossier d'enquête initial est complété dans ses différents éléments, et comprend notamment :

1° Une note expliquant les modifications substantielles apportées au projet, plan ou programme par rapport à sa version initialement soumise à enquête ;

2° Lorsqu'ils sont requis, l'étude d'impact ou l'évaluation environnementale intégrant ces modifications, ainsi que l'avis de l'autorité administrative de l'Etat compétente en matière d'environnement mentionné aux articles L. 122-1 et L. 122-7 du présent code ou de l'article L. 121-12 du code de l'urbanisme portant sur cette étude d'impact ou cette évaluation environnementale actualisée.
