# Article R123-27

Dès la nomination du ou des commissaires enquêteurs, le responsable du projet, plan ou programme verse une provision, dont le montant et le délai de versement sont fixés par le président du tribunal administratif ou le conseiller délégué à cet effet.

La personne responsable du projet, plan ou programme peut s'acquitter des obligations résultant de l'alinéa précédent en versant annuellement au fonds d'indemnisation des commissaires enquêteurs un acompte à valoir sur les sommes dues et en attestant, à l'ouverture de chaque enquête effectuée à sa demande, que cet acompte garantit le paiement de celles-ci.

Le président du tribunal administratif ou le conseiller délégué par lui à cette fin peut, soit au début de l'enquête, soit au cours de celle-ci ou après le dépôt du rapport d'enquête, accorder au commissaire enquêteur, sur sa demande, une allocation provisionnelle. Cette décision ne peut faire l'objet d'aucun recours. L'allocation est versée par le fonds d'indemnisation des commissaires enquêteurs dans la limite des sommes perçues de la personne responsable du projet, plan ou programme.
