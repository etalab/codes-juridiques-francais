# Article R122-7

I.-L'autorité compétente pour prendre la décision d'autorisation, d'approbation ou d'exécution des travaux, de l'ouvrage ou de l'aménagement projetés transmet pour avis le dossier comprenant l'étude d'impact et le dossier de demande d'autorisation à l'autorité administrative de l'Etat compétente en matière d'environnement définie à l'article R. 122-6.

Celle-ci se prononce par un avis unique lorsqu'elle est saisie simultanément de plusieurs projets concourant à la réalisation d'un même programme de travaux.

Lorsque le ministre chargé de l'environnement a pris la décision de se saisir de l'étude en application du 3° du II de l'article L. 122-3, le préfet lui adresse le dossier comprenant l'étude d'impact et la demande d'autorisation.

Lorsque les travaux, les ouvrages ou aménagements sont entrepris pour le compte des services de la défense nationale, le ministre de la défense détermine les modalités de transmission de l'étude d'impact à l'autorité de l'Etat compétente en matière d'environnement compatibles avec le secret de la défense nationale qu'il lui appartient de préserver.

II.-L'autorité administrative de l'Etat compétente en matière d'environnement, lorsqu'elle tient sa compétence du I ou du II de l'article R. 122-6, se prononce dans les trois mois suivant la date de réception du dossier mentionné au premier alinéa du I et, dans les autres cas, dans les deux mois suivant cette réception. L'avis, dès sa signature, ou l'information relative à l'absence d'observations émises dans le délai, est mis en ligne sur son site internet et sur le site internet de l'autorité chargée de le recueillir lorsque cette dernière dispose d'un tel site.

L'autorité compétente pour prendre la décision d'autorisation, d'approbation ou d'exécution des travaux, de l'ouvrage ou de l'aménagement projetés transmet, dès sa réception, l'avis au pétitionnaire. L'avis ou l'information relative à l'absence d'observations émises dans le délai est joint au dossier d'enquête publique ou de la procédure équivalente de consultation du public prévue par un texte particulier.

III.-Les autorités administratives de l'Etat compétentes en matière d'environnement mentionnées à l'article R. 122-6 rendent leur avis après avoir consulté :

-le ou les préfets de département sur le territoire desquels est situé le projet, au titre de leurs attributions dans le domaine de l'environnement ;

-dans les cas mentionnés aux I et II de l'article R. 122-6, le ministre chargé de la santé ou le directeur général de l'agence régionale de santé dans les cas mentionnés au III du même article ;

-le cas échéant, le préfet maritime au titre des compétences en matière de protection de l'environnement qu'il tient du décret n° 2004-112 du 6 février 2004 relatif à l'organisation de l'action de l'Etat en mer ; le cas échéant, outre-mer, le représentant de l'Etat en mer mentionné par le décret n° 2005-1514 du 6 décembre 2005 susvisé relatif à l'organisation outre-mer de l'action de l'Etat en mer.

Ces autorités disposent d'un délai d'un mois à compter de la réception du dossier pour émettre leur avis. En cas d'urgence, l'autorité administrative de l'Etat compétente en matière d'environnement peut réduire ce délai sans que celui-ci ne puisse être inférieur à dix jours.
