# Article R122-6

I.-Sous réserve des dispositions du II, l'autorité administrative de l'Etat compétente en matière d'environnement mentionnée à l'article L. 122-1 est le ministre chargé de l'environnement :

1° Pour les projets de travaux, d'ouvrages ou d'aménagements qui donnent lieu à une décision d'autorisation, d'approbation ou d'exécution prise par décret ou par un ministre ainsi que, sauf disposition réglementaire particulière, pour les projets de travaux, d'ouvrages ou d'aménagements qui donnent lieu à une décision relevant d'une autorité administrative ou publique indépendante ;

2° Pour tout projet de travaux, d'ouvrages ou d'aménagements faisant l'objet d'une étude d'impact dont il décide de se saisir en application du 3° du II de l'article L. 122-3, le ministre chargé de l'environnement peut se saisir, de sa propre initiative ou sur proposition de toute personne physique ou morale, de toute étude d'impact relevant de la compétence du préfet de région en application du III du présent article. Il demande alors communication du dossier du projet à l'autorité compétente pour prendre la décision d'autorisation, d'approbation ou d'exécution de l'ouvrage ou de l'aménagement projeté. A réception de cette demande, l'autorité compétente fait parvenir le dossier sous quinzaine au ministre chargé de l'environnement, qui dispose d'un délai de deux mois à compter de la réception du dossier pour lui donner son avis. Lorsqu'il est fait application de cette disposition, les délais d'instruction sont prolongés de trois mois au maximum ;

3° Pour les projets de travaux, d'ouvrages ou d'aménagements appartenant à un programme de travaux au sens du II de l'article L. 122-1 lorsque l'un au moins des projets du programme relève de sa compétence en application du 1° ou du 2° ci-dessus et qu'aucun des projets du programme ne relève de la compétence de la formation d'autorité environnementale du Conseil général de l'environnement et du développement durable en application du II ;

4° Pour les projets de travaux, d'ouvrages ou d'aménagements faisant l'objet de plusieurs décisions d'autorisation lorsque l'une au moins de ces autorisations relève de sa compétence en application du 1° ou du 2° ci-dessus et qu'aucune des autorisations ne relève de la compétence de la formation d'autorité environnementale du Conseil général de l'environnement et du développement durable en application du II.

Le ministre chargé de l'environnement peut déléguer à l'autorité mentionnée au II sa compétence pour se prononcer sur certaines catégories de projets.

II.-L'autorité administrative de l'Etat compétente en matière d'environnement mentionnée à l'article L. 122-1 est la formation d'autorité environnementale du Conseil général de l'environnement et du développement durable :

1° Pour les projets de travaux, d'ouvrages ou d'aménagements qui donnent lieu à une décision du ministre chargé de l'environnement ou à un décret pris sur son rapport ;

2° Pour les projets qui sont élaborés par les services dans les domaines relevant des attributions du même ministre ou sous la maîtrise d'ouvrage d'établissements publics relevant de sa tutelle. Pour l'application du présent alinéa, est pris en compte l'ensemble des attributions du ministre chargé de l'environnement telles qu'elles résultent des textes en vigueur à la date à laquelle l'autorité administrative de l'Etat compétente en matière d'environnement est saisie ;

3° Pour les projets de travaux, d'ouvrages ou d'aménagements appartenant à un programme de travaux au sens du II de l'article L. 122-1 lorsque l'un au moins des projets du programme relève de sa compétence en application du 1° ou du 2° ci-dessus ;

4° Pour les projets de travaux, d'ouvrages ou d'aménagements faisant l'objet de plusieurs décisions d'autorisation lorsque l'une au moins de ces autorisations relève de sa compétence en application du 1°, du 2° ci-dessus.

III.-Dans les cas ne relevant pas du I ou du II, l'autorité administrative de l'Etat compétente en matière d'environnement mentionnée à l'article L. 122-1 est le préfet de la région sur le territoire de laquelle le projet de travaux, d'ouvrage ou d'aménagement doit être réalisé. Lorsque le projet est situé sur plusieurs régions ou lorsqu'il appartient à un programme de travaux au sens du II de l'article L. 122-1 situé sur plusieurs régions et ne relevant pas du I ou du II ci-dessus, la décision d'examen au cas par cas en application de l'article R. 122-3 ou l'avis sont rendus conjointement par les préfets de région concernés.
