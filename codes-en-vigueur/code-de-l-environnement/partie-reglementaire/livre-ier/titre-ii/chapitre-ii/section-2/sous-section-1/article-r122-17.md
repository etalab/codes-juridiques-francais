# Article R122-17

I.-Les plans, schémas, programmes et autres documents de planification devant faire l'objet d'une évaluation environnementale et, sous réserve du III, l'autorité de l'Etat compétente en matière d'environnement devant être consultée sont définis dans le tableau ci-dessous :

<table>
<tbody>
<tr>
<th>
<br clear="none"/>PLAN, SCHÉMA, PROGRAMME, <p>document de planification <br clear="none"/>
</p>
</th>
<th>
<br clear="none"/>AUTORITÉ ADMINISTRATIVE DE L'ÉTAT <p>compétente en matière d'environnement <br clear="none"/>
</p>
</th>
</tr>
<tr>
<td align="left" valign="top">
<p>1° Programme opérationnel mentionné à l'article 32 du règlement (CE) n° 1083/2006 du Conseil du 11 juillet 2006 portant dispositions générales sur le Fonds européen de développement régional, le Fonds social européen et le Fonds de cohésion et abrogeant le règlement (CE) n° 1260/1999 </p>
</td>
<td align="left" valign="top">
<p>Préfet de région </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">2° Schéma décennal de développement du réseau prévu par l'article L. 321-6 du code de l'énergie
</p>
</td>
<td align="center">
<p align="left">Formation d'autorité environnementale du Conseil général de l'environnement et du développement durable </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">3° Schéma régional de raccordement au réseau des énergies renouvelables prévu par l'article L. 321-7 du code de l'énergie
</p>
</td>
<td align="center">
<p align="left">Préfet de région </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">4° Schéma directeur d'aménagement et de gestion des eaux prévu par les articles L. 212-1 et L. 212-2 du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Préfet coordonnateur de bassin </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">5° Schéma d'aménagement et de gestion des eaux prévu par les articles L. 212-3 à L. 212-6 du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">6° Document stratégique de façade prévu par l'article L. 219-3 code de l'environnement et document stratégique de bassin prévu à l'article L. 219-6 du même code
</p>
</td>
<td align="center">
<p align="left">Formation d'autorité environnementale du Conseil général de l'environnement et du développement durable </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">7° Plan d'action pour le milieu marin prévu par l'article L. 219-9 du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Formation d'autorité environnementale du Conseil général de l'environnement et du développement durable </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">8° Schéma régional du climat, de l'air et de l'énergie prévu par l'article L. 222-1 du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Préfet de région </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">9° Zone d'actions prioritaires pour l'air mentionnée à l'article L. 228-3 du code de l'environnement (1) </p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">10° Charte de parc naturel régional prévue au II de l'article L. 333-1 du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Formation d'autorité environnementale du Conseil général de l'environnement et du développement durable </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">11° Charte de parc national prévue par l'article L. 331-3 du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Formation d'autorité environnementale du Conseil général de l'environnement et du développement durable </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">12° Plan départemental des itinéraires de randonnée motorisée prévu par l'article L. 361-2 du code de l'environnement

</p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">13° Orientations nationales pour la préservation et la remise en bon état des continuités écologiques prévues à l'article L. 371-2 du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Formation d'autorité environnementale du Conseil général de l'environnement et du développement durable </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">14° Schéma régional de cohérence écologique prévu par l'article L. 371-3 du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Préfet de région </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">15° Plans, schémas, programmes et autres documents de planification soumis à évaluation des incidences Natura 2000 au titre de l'article L. 414-4 du code de l'environnement à l'exception de ceux mentionnés au II de l'article L. 122-4 même du code
</p>
</td>
<td align="center">
<p align="left">Préfet de département sous réserve de la désignation d'une autre autorité par le présent article
</p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">16° Schéma mentionné à l'article L. 515-3 du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">17° Plan national de prévention des déchets prévu par l'article L. 541-11 du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Formation d'autorité environnementale du Conseil général de l'environnement et du développement durable </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">18° Plan national de prévention et de gestion de certaines catégories de déchets prévu par l'article L. 541-11-1 du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Formation d'autorité environnementale du Conseil général de l'environnement et du développement durable </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">19° Plan régional ou interrégional de prévention et de gestion des déchets dangereux prévu par l'article L. 541-13 du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Préfet de région </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">20° Plan départemental ou interdépartemental de prévention et de gestion des déchets non dangereux prévu par l'article L. 541-14 du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">21° Plan de prévention et de gestion des déchets non dangereux d'Ile-de-France prévu par l'article L. 541-14 du code de l'environnement </p>
</td>
<td align="center">
<p align="left">Préfet de région </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">22° Plan départemental ou interdépartemental de prévention et de gestion des déchets issus de chantiers du bâtiment et des travaux publics prévu par l'article L. 541-14-1 du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">23° Plan de prévention et de gestion des déchets issus de chantiers du bâtiment et des travaux publics d'Ile-de-France prévu par l'article L. 541-14-1 du code de l'environnement </p>
</td>
<td align="center">
<p align="left">Préfet de région </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">24° Plan national de gestion des matières et déchets radioactifs prévu par l'article L. 542-1-2 du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Formation d'autorité environnementale du Conseil général de l'environnement et du développement durable </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">25° Plan de gestion des risques d'inondation prévu par l'article L. 566-7 du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Préfet coordonnateur de bassin </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">26° Programme d'actions national pour la protection des eaux contre la pollution par les nitrates d'origine agricole prévu par le IV de l'article R. 211-80 du du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Formation d'autorité environnementale du Conseil général de l'environnement et du développement durable </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">27° Programme d'actions régional pour la protection des eaux contre la pollution par les nitrates d'origine agricole prévu par le IV de l'article R. 211-80 du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Préfet de région </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">28° Directives d'aménagement mentionnées au 1° de l'article L. 122-2 du code forestier
</p>
</td>
<td align="center">
<p align="left">Préfet de région </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">29° Schéma régional mentionné au 2° de l'article L. 122-2 du code forestier </p>
</td>
<td align="center">
<p align="left">Préfet de région </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">30° Schéma régional de gestion sylvicole mentionné au 3° de l'article L. 122-2 du code forestier </p>
</td>
<td align="center">
<p align="left">Préfet de région </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">31° Plan pluriannuel régional de développement forestier prévu par l'article L. 122-12 du code forestier </p>
</td>
<td align="center">
<p align="left">Préfet de région </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">32° Schéma départemental d'orientation minière prévu par l'article L. 621-1 du code minier </p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">33° 4° et 5° du projet stratégique des grands ports maritimes, prévus à l'article R. 5312-63 du code des transports </p>
</td>
<td align="center">
<p align="left">Formation d'autorité environnementale du Conseil général de l'environnement et du développement durable </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">34° Réglementation des boisements prévue par l'article L. 126-1 du code rural et de la pêche maritime </p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">35° Schéma régional de développement de l'aquaculture marine prévu par l'article L. 923-1-1 du code rural et de la pêche maritime </p>
</td>
<td align="center">
<p align="left">Préfet de région </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">36° Schéma national des infrastructures de transport prévu par l'article L. 1212-1 du code des transports </p>
</td>
<td align="center">
<p align="left">Formation d'autorité environnementale du Conseil général de l'environnement et du développement durable </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">37° Schéma régional des infrastructures de transport prévu par l'article L. 1213-1 du code des transports
</p>
</td>
<td align="center">
<p align="left">Préfet de région </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">38° Plan de déplacements urbains prévu par les articles L. 1214-1 et L. 1214-9 du code des transports
</p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">39° Contrat de plan Etat-région prévu par l'article 11 de la loi n° 82-653 du 29 juillet 1982 portant réforme de la planification </p>
</td>
<td align="center">
<p align="left">Préfet de région </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">40° Schéma régional d'aménagement et de développement du territoire prévu par l'article 34 de la loi n° 83-8 du 7 janvier 1983 relative à la répartition des compétences entre les communes, les départements et les régions </p>
</td>
<td align="center">
<p align="left">Préfet de région </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">41° Schéma de mise en valeur de la mer élaboré selon les modalités définies à l'article 57 de la loi n° 83-8 du 7 janvier 1983 relative à la répartition des compétences entre les communes, les départements et les régions </p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">42° Schéma d'ensemble du réseau de transport public du Grand Paris et contrats de développement territorial prévu par les articles 2,3 et 21 de la loi n° 2010-597 du 3 juin 2010 relative au Grand Paris </p>
</td>
<td align="center">
<p align="left">Formation d'autorité environnementale du Conseil général de l'environnement et du développement durable </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">43° Schéma des structures des exploitations de cultures marines prévu par l'article 5 du décret n° 83-228 du 22 mars 1983 fixant le régime de l'autorisation des exploitations de cultures marines </p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
</tbody>
</table>

II.-Les plans, schémas, programmes et autres documents de planification susceptibles de faire l'objet d'une évaluation environnementale après un examen au cas par cas et, sous réserve du III, l'autorité de l'Etat compétente en matière d'environnement devant être consultée sont définis dans le tableau ci-dessous :

<table>
<tbody>
<tr>
<th>
<br clear="none"/>PLAN, SCHÉMA, PROGRAMME, <p>document de planification <br clear="none"/>
</p>
</th>
<th>
<br clear="none"/>AUTORITÉ ADMINISTRATIVE DE L'ÉTAT <p>compétente en matière d'environnement <br clear="none"/>
</p>
</th>
</tr>
<tr>
<td align="center">
<p align="left">1° Directive de protection et de mise en valeur des paysages prévue par l'article L. 350-1 du code de l'environnement
</p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">2° Plan de prévention des risques technologiques prévu par l'article L. 515-15 du code de l'environnement et plan de prévention des risques naturels prévisibles prévu par l'article L. 562-1 du même code </p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">3° Stratégie locale de développement forestier prévue par l'article L. 123-1 du code forestier </p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">4° Zones mentionnées aux 1° à 4° de l'article L. 2224-10 du code général des collectivités territoriales </p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">5° Plan de prévention des risques miniers prévu par l'article L. 174-5 du code minier </p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">6° Zone spéciale de carrière prévue par l'article L. 321-1 du code minier
</p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">7° Zone d'exploitation coordonnée des carrières prévue par l'article L. 334-1 du code minier </p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">8° Aire de mise en valeur de l'architecture et du patrimoine prévue par l'article L. 642-1 du code du patrimoine
</p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">9° Plan local de déplacement prévu par l'article L. 1214-30 du code des transports </p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
<tr>
<td align="center">
<p align="left">10° Plan de sauvegarde et de mise en valeur prévu par l'article L. 313-1 du code de l'urbanisme
</p>
</td>
<td align="center">
<p align="left">Préfet de département </p>
</td>
</tr>
</tbody>
</table>

III.-Sauf disposition particulière, lorsque le plan, schéma, programme ou document de planification mentionné au I ou au II excède le ressort territorial du préfet désigné autorité administrative de l'Etat compétente en matière d'environnement, cette compétence est exercée conjointement par les préfets de département concernés ou par les préfets de région concernés.

IV.-Lorsqu'elle est prévue par la législation ou la réglementation applicable, la révision d'un plan, schéma, programme ou document de planification mentionné au I fait l'objet d'une nouvelle évaluation.

Lorsqu'elle est prévue par la législation ou la réglementation applicable, la révision d'un plan, schéma, programme ou document de planification mentionné au II fait l'objet d'une nouvelle évaluation après un examen au cas par cas.

V.-Sauf disposition particulière, les autres modifications d'un plan, schéma, programme ou document de planification mentionné au I ou au II ne font l'objet d'une évaluation environnementale qu'après un examen au cas par cas qui détermine, le cas échéant, si l'évaluation environnementale initiale doit être actualisée ou si une nouvelle évaluation environnementale est requise.
