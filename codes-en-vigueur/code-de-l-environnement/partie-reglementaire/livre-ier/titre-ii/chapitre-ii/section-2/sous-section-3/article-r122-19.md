# Article R122-19

Sans préjudice de sa responsabilité quant à la qualité de l'évaluation environnementale, la personne publique chargée de l'élaboration ou de la modification d'un plan, schéma, programme ou document de planification peut consulter l'autorité administrative de l'Etat compétente en matière d'environnement désignée aux I à III de l'article R. 122-17 sur l'ampleur et le degré de précision des informations à fournir dans le rapport environnemental.

L'autorité administrative de l'Etat compétente en matière d'environnement précise les éléments permettant d'ajuster le contenu du rapport environnemental à la sensibilité des milieux et aux impacts potentiels du plan, schéma, programme ou document de planification sur l'environnement ou la santé humaine ainsi que, s'il y a lieu, la nécessité d'étudier les incidences notables du plan, schéma, programme ou document de planification sur l'environnement d'un autre Etat membre de l'Union européenne.
