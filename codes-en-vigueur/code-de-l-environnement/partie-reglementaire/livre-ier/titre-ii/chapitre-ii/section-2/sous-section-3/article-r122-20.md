# Article R122-20

L'évaluation environnementale est proportionnée à l'importance du plan, schéma, programme et autre document de planification, aux effets de sa mise en œuvre ainsi qu'aux enjeux environnementaux de la zone considérée. Le rapport environnemental, qui rend compte de la démarche d'évaluation environnementale, comprend successivement :

1° Une présentation générale indiquant, de manière résumée, les objectifs du plan, schéma, programme ou document de planification et son contenu, son articulation avec d'autres plans, schémas, programmes ou documents de planification et, le cas échéant, si ces derniers ont fait, feront ou pourront eux-mêmes faire l'objet d'une évaluation environnementale ;

2° Une description de l'état initial de l'environnement sur le territoire concerné, les perspectives de son évolution probable si le plan, schéma, programme ou document de planification n'est pas mis en œuvre, les principaux enjeux environnementaux de la zone dans laquelle s'appliquera le plan, schéma, programme ou document de planification et les caractéristiques environnementales des zones qui sont susceptibles d'être touchées par la mise en œuvre du plan, schéma, programme ou document de planification. Lorsque l'échelle du plan, schéma, programme ou document de planification le permet, les zonages environnementaux existants sont identifiés ;

3° Les solutions de substitution raisonnables permettant de répondre à l'objet du plan, schéma, programme ou document de planification dans son champ d'application territorial. Chaque hypothèse fait mention des avantages et inconvénients qu'elle présente, notamment au regard des 1° et 2° ;

4° L'exposé des motifs pour lesquels le projet de plan, schéma, programme ou document de planification a été retenu notamment au regard des objectifs de protection de l'environnement ;

5° L'exposé :

a) Des effets notables probables de la mise en œuvre du plan, schéma, programme ou autre document de planification sur l'environnement, et notamment, s'il y a lieu, sur la santé humaine, la population, la diversité biologique, la faune, la flore, les sols, les eaux, l'air, le bruit, le climat, le patrimoine culturel architectural et archéologique et les paysages.

Les effets notables probables sur l'environnement sont regardés en fonction de leur caractère positif ou négatif, direct ou indirect, temporaire ou permanent, à court, moyen ou long terme ou encore en fonction de l'incidence née du cumul de ces effets. Ils prennent en compte les effets cumulés du plan, schéma, programme avec d'autres plans, schémas, programmes ou documents de planification ou projets de plans, schémas, programmes ou documents de planification connus ;

b) De l'évaluation des incidences Natura 2000 mentionnée à l'article L. 414-4 ;

6° La présentation successive des mesures prises pour :

a) Eviter les incidences négatives sur l'environnement du plan, schéma, programme ou autre document de planification sur l'environnement et la santé humaine ;

b) Réduire l'impact des incidences mentionnées au a ci-dessus n'ayant pu être évitées ;

c) Compenser, lorsque cela est possible, les incidences négatives notables du plan, schéma, programme ou document de planification sur l'environnement ou la santé humaine qui n'ont pu être ni évités ni suffisamment réduits. S'il n'est pas possible de compenser ces effets, la personne publique responsable justifie cette impossibilité.

Les mesures prises au titre du b du 5° sont identifiées de manière particulière.

La description de ces mesures est accompagnée de l'estimation des dépenses correspondantes et de l'exposé de leurs effets attendus à l'égard des impacts du plan, schéma, programme ou document de planification identifiés au 5° ;

7° La présentation des critères, indicateurs et modalités-y compris les échéances-retenus :

a) Pour vérifier, après l'adoption du plan, schéma, programme ou document de planification, la correcte appréciation des effets défavorables identifiés au 5° et le caractère adéquat des mesures prises au titre du 6° ;

b) Pour identifier, après l'adoption du plan, schéma, programme ou document de planification, à un stade précoce, les impacts négatifs imprévus et permettre, si nécessaire, l'intervention de mesures appropriées ;

8° Une présentation des méthodes utilisées pour établir le rapport environnemental et, lorsque plusieurs méthodes sont disponibles, une explication des raisons ayant conduit au choix opéré ;

9° Un résumé non technique des informations prévues ci-dessus.
