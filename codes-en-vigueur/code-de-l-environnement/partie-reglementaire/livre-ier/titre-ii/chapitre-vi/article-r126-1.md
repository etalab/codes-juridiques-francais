# Article R126-1

La déclaration de projet prévue à l'article L. 126-1 est publiée dans les conditions définies au présent chapitre.

Toutefois, lorsque la déclaration de projet nécessite la mise en compatibilité d'un schéma de cohérence territoriale ou d'un plan local d'urbanisme, elle est publiée dans les conditions prévues, selon le cas, à l'article R. 122-13 ou à l'article R. 123-25 du code de l'urbanisme.
