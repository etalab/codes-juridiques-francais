# Chapitre Ier : Dispositions générales

- [Article D641-3](article-d641-3.md)
- [Article R641-1](article-r641-1.md)
- [Article R641-2](article-r641-2.md)
