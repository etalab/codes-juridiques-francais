# Chapitre V : Prévention des pollutions, des risques et des nuisances

- [Section 1 : Installations classées pour la protection de l'environnement](section-1)
- [Section 2 : Produits chimiques et biocides](section-2)
- [Section 3 : Organismes génétiquement modifiés](section-3)
- [Section 4 : Déchets](section-4)
- [Section 6 : Prévention des nuisances sonores](section-6)
- [Article R655-1](article-r655-1.md)
