# Article R653-1

Le livre III est applicable à la collectivité départementale de Mayotte à l'exception des articles R. 321-3, R. 321-5 à D. 321-15 et R. 332-30 à R. 332-67.
