# Chapitre II : Eaux marines et voies ouvertes à la navigation maritime

- [Article R622-1](article-r622-1.md)
- [Article R622-2](article-r622-2.md)
