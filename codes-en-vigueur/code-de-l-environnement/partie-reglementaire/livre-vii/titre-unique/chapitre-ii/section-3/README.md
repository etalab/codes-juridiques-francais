# Section 3 : Activités soumises à autorisation

- [Article R712-9](article-r712-9.md)
- [Article R712-10](article-r712-10.md)
- [Article R712-11](article-r712-11.md)
- [Article R712-12](article-r712-12.md)
- [Article R712-13](article-r712-13.md)
- [Article R712-14](article-r712-14.md)
