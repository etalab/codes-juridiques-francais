# Sous-section 1 : Dispositions communes

- [Article R414-1](article-r414-1.md)
- [Article R414-2](article-r414-2.md)
- [Article R414-2-1](article-r414-2-1.md)
