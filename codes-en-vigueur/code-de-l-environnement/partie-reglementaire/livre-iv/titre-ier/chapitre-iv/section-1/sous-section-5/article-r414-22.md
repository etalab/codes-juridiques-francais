# Article R414-22

L'évaluation environnementale, l'étude d'impact ainsi que le document d'incidences mentionnés respectivement au 1°, 3° et 4° du I de l'article R. 414-19 tiennent lieu de dossier d'évaluation des incidences Natura 2000 s'ils satisfont aux prescriptions de l'article R. 414-23.
