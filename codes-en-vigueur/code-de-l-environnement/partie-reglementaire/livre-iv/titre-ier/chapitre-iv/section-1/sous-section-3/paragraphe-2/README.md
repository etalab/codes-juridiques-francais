# Paragraphe 2 : Dispositions applicables aux sites Natura 2000 majoritairement marins

- [Article R414-9](article-r414-9.md)
- [Article R414-9-1](article-r414-9-1.md)
- [Article R414-9-2](article-r414-9-2.md)
- [Article R414-9-3](article-r414-9-3.md)
- [Article R414-9-4](article-r414-9-4.md)
- [Article R414-9-5](article-r414-9-5.md)
- [Article R414-9-6](article-r414-9-6.md)
- [Article R414-9-7](article-r414-9-7.md)
