# Sous-section 3 : Comités de pilotage et documents d'objectifs

- [Paragraphe 1 : Dispositions applicables aux sites Natura 2000 majoritairement terrestres](paragraphe-1)
- [Paragraphe 2 : Dispositions applicables aux sites Natura 2000 majoritairement marins](paragraphe-2)
- [Paragraphe 3 : Dispositions particulières applicables à certains sites Natura 2000](paragraphe-3)
- [Paragraphe 4 : Contenu du document d'objectifs](paragraphe-4)
