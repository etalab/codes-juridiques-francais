# Article R414-7

L'arrêté portant désignation d'un site Natura 2000 est publié au Journal officiel de la République française.

Sont transmis aux maires des communes consultées en application du III de l'article R. 414-3, par le ou les préfets ayant procédé à cette consultation, l'arrêté de désignation du site Natura et ses annexes comportant notamment la carte du site, sa dénomination, sa délimitation, ainsi que l'identification des habitats naturels et des espèces qui justifient la désignation du site. Ces documents sont tenus à la disposition du public dans les services du ministère chargé de l'environnement, à la préfecture et dans les mairies des communes situées dans le périmètre du site.
