# Article D416-4

L'agrément est accordé pour un territoire constitué d'un ensemble de départements présentant des caractéristiques biologiques et géographiques communes. Peuvent bénéficier de l'agrément des personnes morales publiques ou privées, à l'exception des sociétés commerciales.
