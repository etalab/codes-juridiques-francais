# Article D416-6

L'agrément vaut autorisation d'utiliser, dans le cadre des activités du conservatoire, la dénomination " Conservatoire botanique national " et son identité graphique enregistrées par le ministre chargé de la protection de la nature à l'Institut national de la propriété industrielle sous forme de marque collective. Les modalités de cet usage sont fixées par le règlement joint au dépôt de marque.

Le retrait de l'agrément emporte interdiction pour l'établissement d'utiliser la marque collective déposée.

L'usage de la marque collective mentionnée au premier alinéa peut également être confié par le ministre à la Fédération des conservatoires botaniques nationaux, regroupant exclusivement des conservatoires botaniques nationaux. Il peut lui être retiré dans les conditions prévues au deuxième alinéa de l'article R. * 416-5.
