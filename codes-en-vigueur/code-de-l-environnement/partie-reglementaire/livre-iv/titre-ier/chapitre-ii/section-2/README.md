# Section 2 : Régime propre à la capture, au ramassage et à la cession de certaines espèces

- [Article R412-8](article-r412-8.md)
- [Article R412-9](article-r412-9.md)
- [Article R412-10](article-r412-10.md)
