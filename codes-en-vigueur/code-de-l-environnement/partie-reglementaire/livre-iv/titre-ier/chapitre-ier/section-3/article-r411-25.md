# Article R411-25

Le conseil scientifique régional du patrimoine naturel ne peut délibérer que si la moitié des membres assiste à la séance. Lorsque le quorum n'est pas atteint, il délibère valablement sans condition de quorum après une nouvelle convocation portant sur le même ordre du jour.

Ses avis sont émis à la majorité des membres présents. En cas de partage égal des voix, celle du président est prépondérante. Les avis sont transmis au préfet de région, au président du conseil régional ou, en Corse, au président du conseil exécutif.
