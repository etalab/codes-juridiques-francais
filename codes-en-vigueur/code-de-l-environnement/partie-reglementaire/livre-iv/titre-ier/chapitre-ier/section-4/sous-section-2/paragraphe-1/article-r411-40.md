# Article R411-40

I.-Lorsqu'à des fins agricoles, piscicoles, forestières ou pour des motifs d'intérêt général, l'Etat envisage d'introduire, dans le milieu naturel, des animaux ou des végétaux appartenant à des espèces figurant sur les listes établies pour l'application du I de l'article L. 411-3, le préfet du département dans lequel cette opération doit être réalisée procède à l'évaluation de ses conséquences et met à disposition des collectivités territoriales intéressées un dossier de présentation pour recueillir leur avis sur le projet.

Ce dossier comporte les éléments d'information prévus aux 2° à 8° du II de l'article R. 411-32.

II.-La décision de procéder à l'introduction, dans le milieu naturel, des animaux ou des végétaux ne peut intervenir avant l'expiration d'un délai de trois mois à compter de la mise à la disposition du public du dossier de présentation du projet dans le cadre de la procédure prévue à l'article L. 120-1-1 et de la saisine des collectivités territoriales intéressées.

Cette décision est prise dans les conditions prévues à l'article R. 411-36.
