# Section 4 : Introduction dans le milieu naturel de spécimens appartenant à des espèces animales non domestiques ou à des espèces végétales non cultivées

- [Sous-section 1 : Instruction des demandes d'autorisation d'une introduction menée par une personne autre que l'Etat](sous-section-1)
- [Sous-section 2 : Introduction par l'Etat de spécimens appartenant à des espèces animales non domestiques ou à des espèces végétales non cultivées et mesures d'urgence](sous-section-2)
- [Article R411-31](article-r411-31.md)
