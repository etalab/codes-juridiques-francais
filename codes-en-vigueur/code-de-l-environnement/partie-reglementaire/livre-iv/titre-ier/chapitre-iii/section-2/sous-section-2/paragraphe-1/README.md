# Paragraphe 1 : Demande d'autorisation

- [Article R413-31](article-r413-31.md)
- [Article R413-32](article-r413-32.md)
- [Article R413-33](article-r413-33.md)
- [Article R413-34](article-r413-34.md)
