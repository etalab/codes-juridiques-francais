# Article R413-16

Lorsque l'établissement est soumis à autorisation en application de l'article L. 512-1, le préfet procède à l'enquête publique et aux consultations conformément aux dispositions des articles R. 512-14 à R. 512-25.
