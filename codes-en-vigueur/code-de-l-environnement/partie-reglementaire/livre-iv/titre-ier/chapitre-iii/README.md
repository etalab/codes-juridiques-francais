# Chapitre III : Etablissements détenant des animaux d'espèces non domestiques

- [Section 1 : Etablissements soumis à autorisation d'ouverture autres que les établissements d'élevage, de vente et de transit des espèces de gibier dont la chasse est autorisée](section-1)
- [Section 2 : Autorisation d'ouverture pour les établissements d'élevage, de vente et de transit des espèces de gibier dont la chasse est autorisée](section-2)
- [Section 3 : Etablissements soumis à déclaration](section-3)
- [Section 4 : Contrôle de l'autorité administrative](section-4)
- [Section 5 : Sanctions administratives](section-5)
- [Article R413-1](article-r413-1.md)
- [Article R413-2](article-r413-2.md)
