# Sous-section 3 : Dispositions communes

- [Article R413-50](article-r413-50.md)
- [Article R413-51](article-r413-51.md)
