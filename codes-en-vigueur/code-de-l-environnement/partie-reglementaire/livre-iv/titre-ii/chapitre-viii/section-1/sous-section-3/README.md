# Sous-section 3 : Exercice de la chasse

- [Paragraphe 1 : Protection du gibier](paragraphe-1)
- [Paragraphe 2 : Temps de chasse](paragraphe-2)
- [Paragraphe 4 : Transport et commercialisation](paragraphe-4)
- [Paragraphe 5 : Obstruction à un acte de chasse](paragraphe-5)
