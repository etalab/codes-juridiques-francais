# Article R422-86

L'arrêté d'institution de la réserve prévoit l'exécution d'un plan de chasse ou d'un plan de gestion cynégétique lorsque celui-ci est nécessaire au maintien des équilibres biologiques et agro-sylvo-cynégétiques. Les conditions d'exécution de ce plan doivent être compatibles avec la protection du gibier et la préservation de sa tranquillité. Son exécution doit être autorisée chaque année, selon les cas, par l'arrêté attributif du plan de chasse ou par l'arrêté approuvant le plan de gestion cynégétique.

Tout autre acte de chasse est interdit.
