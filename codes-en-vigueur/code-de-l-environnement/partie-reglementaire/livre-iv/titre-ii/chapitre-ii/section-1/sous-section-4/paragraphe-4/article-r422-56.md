# Article R422-56

Si l'acquéreur d'un terrain exclu du territoire de l'association communale de chasse agréée en application du 5° de l'article L. 422-10 n'a pas, dans les conditions prévues à l'article L. 422-19, notifié au préfet, par lettre recommandée avec demande d'avis de réception, son intention de maintenir cette opposition, le terrain est, par arrêté du préfet, à la diligence du président de l'association, incorporé dans le territoire de celle-ci. Le préfet informe préalablement le nouveau propriétaire de la demande du président de l'association et recueille ses observations.
