# Article R422-73

Après vérification du respect par les statuts, par le règlement intérieur et par le règlement de chasse des dispositions obligatoires mentionnées aux articles R. 422-75 à R. 422-77, l'association intercommunale est agréée par un arrêté du préfet, qui est affiché dans chacune des communes intéressées, aux emplacements utilisés habituellement par l'administration.
