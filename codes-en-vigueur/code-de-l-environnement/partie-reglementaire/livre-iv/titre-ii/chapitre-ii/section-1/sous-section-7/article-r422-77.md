# Article R422-77

Les statuts, le règlement intérieur et le règlement de chasse de chacune des associations constitutives d'une union sont, si nécessaire, mis en harmonie avec les dispositions qui régissent cette union.
