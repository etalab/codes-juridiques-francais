# Article R422-79

Les propriétaires, possesseurs ou fermiers peuvent déléguer à l'association communale ou intercommunale de chasse agréée les droits qui leur sont conférés par l'article L. 427-8 vis-à-vis des animaux nuisibles sur les territoires dont le droit de chasse a été apporté à l'association.
