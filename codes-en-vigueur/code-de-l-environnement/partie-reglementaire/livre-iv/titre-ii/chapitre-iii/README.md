# Chapitre III : Permis de chasser

- [Section 1 : Examen pour la délivrance du permis de chasser](section-1)
- [Section 2 : Délivrance et validation du permis de chasser](section-2)
- [Section 4 : Dispositions diverses](section-4)
- [Article R423-1](article-r423-1.md)
