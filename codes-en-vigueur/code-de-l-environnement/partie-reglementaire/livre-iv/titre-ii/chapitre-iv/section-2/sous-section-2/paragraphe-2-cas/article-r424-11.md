# Article R424-11

Dans le département de la Martinique, la période d'ouverture générale de la chasse doit être comprise entre les dates suivantes :

Date d'ouverture générale au plus tôt le dernier dimanche de juillet ;

Date de clôture générale au plus tard le 15 février.

Les espèces de gibier figurant au tableau ci-après ne peuvent être chassées que pendant les périodes comprises entre les dates suivantes :

<div>
<table>
<thead>
<tr>
<th align="center" bgcolor="#efeff7" width="195">
<br/>
</th>
<th align="center" bgcolor="#efeff7" width="130">
<br/>
<font size="2">DATE D'OUVERTURE SPÉCIFIQUE au plus tôt le</font>
</th>
<th align="center" bgcolor="#efeff7" width="130">
<br/>
<font size="2">DATE DE CLÔTURE SPÉCIFIQUE au plus tard le</font>
</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left">
<br/>Tourterelle, ortolan</td>
<td align="center">
<br/>Ouverture générale</td>
<td align="center">
<br/>30 septembre</td>
</tr>
<tr>
<td align="left">
<br/>Ramier, perdrix, grive</td>
<td align="center">
<br/>Ouverture générale</td>
<td align="center">
<br/>30 novembre</td>
</tr>
</tbody>
</table>
</div>
