# Paragraphe 2 : Cas particuliers de la Guadeloupe, de la Martinique, de la Réunion et de Saint-Pierre-et-Miquelon

- [Article R424-10](article-r424-10.md)
- [Article R424-11](article-r424-11.md)
- [Article R424-12](article-r424-12.md)
- [Article R424-13](article-r424-13.md)
