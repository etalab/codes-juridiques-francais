# Article R424-8

Par exception aux dispositions de l'article R. 424-7, le préfet ne peut fixer les périodes d'ouverture de la chasse aux espèces de gibier figurant au tableau suivant qu'entre les dates et sous réserve des conditions spécifiques de chasse mentionnées dans ce tableau.

Toute personne autorisée à chasser le chevreuil ou le sanglier avant l'ouverture générale peut également chasser le renard dans les conditions spécifiques figurant au même tableau pour le chevreuil et pour le sanglier :

<table>
<tbody>
<tr>
<td width="86">
<p align="center">Espèces </p>
</td>
<td width="101">
<p align="center">Date d'ouverture spécifique au plus tôt le </p>
</td>
<td width="115">
<p align="center">Date de clôture spécifique au plus tard le </p>
</td>
<td width="305">
<p align="center">Conditions spécifiques de chasse </p>
</td>
</tr>
<tr>
<td width="86">
<p align="center">Chevreuil </p>
</td>
<td width="101">
<p align="center">1er juin </p>
</td>
<td width="115">
<p align="center">Dernier jour de février </p>
</td>
<td width="305">
<p>Avant la date d'ouverture générale, ces espèces ne peuvent être chassées qu'à l'approche ou à l'affût, après autorisation préfectorale délivrée au détenteur du droit de chasse. </p>
</td>
</tr>
<tr>
<td width="86">
<p align="center">Cerf </p>
</td>
<td width="101">
<p align="center">1er septembre </p>
</td>
<td width="115">
<p align="center">Dernier jour de février </p>
</td>
<td width="305"/>
</tr>
<tr>
<td width="86">
<p align="center">Daim </p>
</td>
<td width="101">
<p align="center">1er juin </p>
</td>
<td width="115">
<p align="center">Dernier jour de février </p>
</td>
<td width="305"/>
</tr>
<tr>
<td width="86">
<p align="center">Mouflon </p>
</td>
<td width="101">
<p align="center">1er septembre </p>
</td>
<td width="115">
<p align="center">Dernier jour de février </p>
</td>
<td width="305"/>
</tr>
<tr>
<td width="86">
<p align="center">Chamois </p>
</td>
<td width="101">
<p align="center">1er septembre </p>
</td>
<td width="115">
<p align="center">Dernier jour de février </p>
</td>
<td width="305"/>
</tr>
<tr>
<td>
<p align="center">Isard </p>
</td>
<td>
<p align="center">1er septembre </p>
</td>
<td>
<p align="center">Dernier jour de février </p>
</td>
<td/>
</tr>
<tr>
<td width="86">
<p align="center">Sanglier </p>
<br/>
</td>
<td width="101">
<p align="center">1er juin </p>
<br/>
</td>
<td width="115">
<p align="center">Dernier jour de février </p>
<br/>
</td>
<td width="305">
<p>Du 1er juin au 14 août, la chasse du sanglier ne peut être pratiquée qu'en battue, à l'affût ou à l'approche, après autorisation préfectorale délivrée au détenteur du droit de chasse et dans les conditions fixées par l'arrêté du préfet. </p>
<p>Le bénéficiaire de l'autorisation adresse au préfet, avant le 15 septembre de la même année, le bilan des effectifs prélevés. </p>
<p>Du 15 août à l'ouverture générale et de la clôture générale au dernier jour de février, la chasse du sanglier ne peut être pratiquée qu'en battue, ou à l'affût, ou à l'approche, dans les conditions fixées par l'arrêté du préfet. </p>
</td>
</tr>
<tr>
<td width="86">
<p align="center">Grand tétras </p>
</td>
<td width="101">
<p align="center">Troisième dimanche de septembre </p>
</td>
<td width="115">
<p align="center">1er novembre </p>
</td>
<td width="305"/>
</tr>
<tr>
<td width="86">
<p align="center">Petit tétras </p>
</td>
<td width="101">
<p align="center">Troisième dimanche de septembre </p>
</td>
<td width="115">
<p align="center">11 novembre </p>
</td>
<td width="305"/>
</tr>
<tr>
<td width="86">
<p align="center">Lagopède des Alpes </p>
</td>
<td width="101">
<p align="center">Ouverture générale </p>
</td>
<td width="115">
<p align="center">11 novembre </p>
</td>
<td width="305"/>
</tr>
<tr>
<td width="86">
<p align="center">Perdrix bartavelle </p>
</td>
<td width="101">
<p align="center">Ouverture générale </p>
</td>
<td width="115">
<p align="center">11 novembre </p>
</td>
<td width="305"/>
</tr>
<tr>
<td width="86">
<p align="center">Gélinotte </p>
</td>
<td width="101">
<p align="center">Ouverture générale </p>
</td>
<td width="115">
<p align="center">11 novembre </p>
</td>
<td width="305"/>
</tr>
<tr>
<td width="86">
<p align="center">Lièvre variable </p>
</td>
<td width="101">
<p align="center">Ouverture générale </p>
</td>
<td width="115">
<p align="center">11 novembre </p>
</td>
<td width="305"/>
</tr>
<tr>
<td width="86">
<p align="center">Marmotte </p>
</td>
<td width="101">
<p align="center">Ouverture générale </p>
</td>
<td width="115">
<p align="center">11 novembre </p>
</td>
<td width="305"/>
</tr>
<tr>
<td width="86">
<p align="center">Perdrix grise de plaine </p>
</td>
<td width="101">
<p align="center">Premier dimanche de septembre </p>
</td>
<td width="115">
<p align="center">Clôture générale </p>
</td>
<td width="305">
<p>L'ouverture anticipée du premier dimanche de septembre à l'ouverture générale n'est possible que pour les populations naturelles, sur les territoires couverts pour toute la période d'ouverture par un plan de gestion cynégétique approuvé en application de l'article L. 425-15 du code de l'environnement ou par un plan de chasse et si, du 1er septembre à l'ouverture générale, la chasse est pratiquée avec un chien d'arrêt, un chien leveur ou rapporteur de gibier. </p>
</td>
</tr>
<tr>
<td width="86">
<br/>
</td>
<td width="101">
<br/>
</td>
<td width="115">
<br/>
</td>
<td width="305">
<p>Cette possibilité n'est ouverte que dans les départements de l'Aisne, des Ardennes, de l'Aube, de la Marne, du Nord, du Pas-de-Calais et de la Somme.</p>
</td>
</tr>
</tbody>
</table>
