# Article R421-25

Des comptables secondaires peuvent être nommés par le directeur général après agrément de l'agent comptable. Des ordonnateurs secondaires peuvent être nommés par décision du directeur général, après accord du conseil d'administration.
