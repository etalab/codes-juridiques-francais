# Article R421-26

Il peut être constitué auprès de l'établissement des régies de recettes et des régies d'avances dans les conditions prévues par le décret n° 92-681 du 20 juillet 1992 relatif aux régies de recettes et aux régies d'avances des organismes publics.
