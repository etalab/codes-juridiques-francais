# Article R421-18

Les missions de police administrative de l'Office national de la chasse et de la faune sauvage sont assurées par les agents techniques et les techniciens de l'environnement de la spécialité milieux et faune sauvage ainsi que par des agents de la filière technique définie à l'article 2 du décret n° 98-1262 du 29 décembre 1998 portant statut des personnels de l'Office national de la chasse et de la faune sauvage.

Lorsqu'ils sont assermentés, ces agents exercent les missions de police judiciaire pour lesquelles ils sont habilités par la loi. Dans ce cadre, ils recherchent et constatent les infractions de jour, de nuit, les dimanches et les jours fériés. Leurs effectifs sont répartis par arrêté du ministre chargé de la protection de la nature.
