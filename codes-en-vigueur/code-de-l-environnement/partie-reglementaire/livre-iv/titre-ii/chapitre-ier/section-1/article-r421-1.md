# Article R421-1

I. - L'organisme consultatif, dénommé Conseil national de la chasse et de la faune sauvage, placé auprès du ministre chargé de la chasse, est chargé de donner au ministre son avis sur les moyens propres à :

1° Préserver la faune sauvage ;

2° Développer le capital cynégétique dans le respect des équilibres biologiques ;

3° Améliorer les conditions d'exercice de la chasse.

II. - Le conseil est consulté sur les projets de loi et de décret modifiant les dispositions législatives et réglementaires du présent titre.
