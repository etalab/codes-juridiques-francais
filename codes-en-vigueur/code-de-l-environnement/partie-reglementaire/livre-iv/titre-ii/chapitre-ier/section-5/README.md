# Section 5 : Fédérations interdépartementales des chasseurs

- [Article R421-40](article-r421-40.md)
- [Article R421-41](article-r421-41.md)
- [Article R421-42](article-r421-42.md)
