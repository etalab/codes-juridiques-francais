# Article R425-5

Les demandes de plan de chasse individuel, accompagnées de l'avis du président de la fédération départementale ou interdépartementale des chasseurs et, le cas échéant, de celui du responsable territorial de l'Office national des forêts, sont transmises au préfet dans les délais fixés par arrêté du ministre chargé de la chasse.

Le préfet examine ces demandes au vu, le cas échéant, des désaccords exprimés par des propriétaires dans les conditions prévues au III de l'article R. 425-4.

Les demandes de plan de chasse individuel portant sur un territoire s'étendant sur plusieurs départements sont transmises aux préfets intéressés.
