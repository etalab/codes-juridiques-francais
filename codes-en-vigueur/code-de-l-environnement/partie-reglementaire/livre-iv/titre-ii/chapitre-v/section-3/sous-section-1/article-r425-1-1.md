# Article R425-1-1

Le plan de chasse est obligatoire pour les cerfs, daims, mouflons, chamois, isards et chevreuils.

Après avis de la commission départementale de la chasse et de la faune sauvage, le préfet peut décider que le plan de chasse est, sur tout ou partie du département, obligatoire pour une espèce de gibier autre que celles mentionnées au premier alinéa. S'agissant des sangliers, l'instauration d'un plan de chasse est en outre soumise à l'avis de la fédération départementale ou interdépartementale des chasseurs.

Le plan de chasse est annuel. Pour le grand gibier, il peut être fixé, après avis de la commission départementale de la chasse et de la faune sauvage, pour une période de trois ans. Dans ce dernier cas, il peut faire l'objet d'une révision annuelle.

Lorsqu'un territoire cynégétique s'étend sur plusieurs départements et constitue une unité cohérente pour la gestion cynégétique, les décisions mentionnées aux deuxième et troisième alinéas font l'objet sur ce territoire de décisions conjointes des préfets intéressés.
