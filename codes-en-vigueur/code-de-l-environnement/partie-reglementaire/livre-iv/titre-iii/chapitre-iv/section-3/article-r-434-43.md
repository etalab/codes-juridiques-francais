# Article R*434-43

Les statuts de ces associations agréées sont approuvés par arrêté du ministre chargé de la pêche en eau douce. Toute modification des statuts d'une association agréée ou de son ressort territorial doit être communiquée au préfet du département du siège social, qui transmet la proposition à ce ministre. Celui-ci fait connaître son avis dans les trois mois suivant sa saisine.
