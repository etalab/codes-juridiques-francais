# Article Annexe (1) à l'article R511-9

NOMENCLATURE DES INSTALLATIONS CLASSÉES POUR LA PROTECTION

DE L'ENVIRONNEMENT ET TAXE GÉNÉRALE SUR LES ACTIVITÉS POLLUANTES

<table>
<tbody>
<tr>
<td rowspan="2" width="21">
<p align="center">N°</p>
</td>
<td colspan="3" width="498">
<p align="center">A-NOMENCLATURE DES INSTALLATIONS CLASSEES</p>
</td>
<td colspan="2" width="180">
<p align="center">B-TAXE GENERALE SUR LES ACTIVITES POLLUANTES</p>
</td>
</tr>
<tr>
<td width="433">
<p align="center">Désignation de la rubrique</p>
</td>
<td width="31">
<p align="center">A, E, D, S, C (1)</p>
</td>
<td width="34">
<p align="center">Rayon (2)</p>
</td>
<td width="148">
<p align="center">Capacité de l'activité</p>
</td>
<td width="33">
<p align="center">Coef.</p>
</td>
</tr>
<tr>
<td rowspan="3" valign="top" width="21">
<p align="center">47 </p>
</td>
<td valign="top" width="433">
<p>Aluminium (fabrication du sulfate d') et fabrication d'aluns </p>
</td>
<td valign="top" width="31">
<br/>
<br/>
</td>
<td valign="top" width="34">
<br/>
<br/>
</td>
<td rowspan="4" valign="top" width="148"/>
<td rowspan="4" valign="top" width="33">
<br/>
<br/>
</td>
</tr>
<tr>
<td valign="top" width="433">
<p>1° par le lavage des terres alumineuses grillées </p>
</td>
<td valign="top" width="31">
<p align="center">A</p>
</td>
<td valign="top" width="34">
<p align="center">0,5</p>
</td>
</tr>
<tr>
<td valign="top" width="433">
<p align="left">2° par l'action de l'acide sulfurique sur la bauxite (voir 2546) </p>
</td>
<td valign="top" width="31">
<br/>
<br/>
</td>
<td valign="top" width="34">
<br/>
<br/>
</td>
</tr>
<tr>
<td valign="top" width="21">
<p align="center">70 </p>
</td>
<td valign="top" width="433">
<p>Bains et boues provenant du dérochage des métaux (traitement des) par l'acide nitrique </p>
</td>
<td valign="top" width="31">
<p align="center">A</p>
</td>
<td valign="top" width="34">
<p align="center">0,5</p>
</td>
</tr>
<tr>
<td valign="top" width="21">
<p align="center">187 </p>
</td>
<td valign="top" width="433">
<p>Etamage des glaces (ateliers d') </p>
</td>
<td valign="top" width="31">
<p align="center">D</p>
</td>
<td valign="top" width="34">
<br/>
<br/>
</td>
<td valign="top" width="148"/>
<td valign="top" width="33">
<br/>
<br/>
</td>
</tr>
<tr>
<td valign="top" width="21">
<p align="center">195 </p>
</td>
<td valign="top" width="433">
<p>Ferro-silicium (dépôts de) </p>
</td>
<td valign="top" width="31">
<p align="center">D</p>
</td>
<td valign="top" width="34">
<br/>
<br/>
</td>
<td valign="top" width="148"/>
<td valign="top" width="33">
<br/>
<br/>
</td>
</tr>
<tr>
<td rowspan="2" valign="top" width="21">
<p>1312 </p>
</td>
<td valign="top" width="433">
<p>Produits explosifs (mise en œuvre de) à des fins industrielles telles que découpage, formage, emboutissage, placage de métaux. </p>
</td>
<td valign="top" width="31">
<br/>
<br/>
</td>
<td valign="top" width="34">
<br/>
<br/>
</td>
<td valign="top" width="148"/>
<td valign="top" width="33">
<br/>
<br/>
</td>
</tr>
<tr>
<td valign="top" width="433">
<p>La quantité unitaire étant supérieure à 10 g </p>
</td>
<td valign="top" width="31">
<p align="center">A</p>
</td>
<td valign="top" width="34">
<p align="center">3</p>
</td>
<td valign="top" width="148"/>
<td valign="top" width="33">
<br/>
<br/>
</td>
</tr>
</tbody>
</table>

(1) A : autorisation, E : enregistrement, D : déclaration, S : servitude d'utilité publique, C : soumis au contrôle périodique prévu par l'article L. 512-11 du code de l'environnement.

(2) Rayon d'affichage en kilomètres.

Nota.-Les activités nucléaires visées par la présente nomenclature sont les activités soumises aux rubriques 1716, 1735, 2797 et 2798.
