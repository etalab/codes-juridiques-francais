# Article Annexe II de l'article R541-8

LISTE DE DÉCHETS

Dispositions générales

1. La présente liste est non exhaustive et sera réexaminée périodiquement.

2. L'inscription sur la liste ne signifie pas que la matière ou l'objet en question soit un déchet dans tous les cas. L'inscription ne vaut que si la matière ou l'objet répond à la définition du terme " déchet " figurant à l'article L. 541-1 du code de l'environnement.

3. Les différents types de déchets figurant sur la liste sont définis de manière complète par le code à six chiffres pour les rubriques de déchets et par les codes à deux ou quatre chiffres pour les titres des chapitres et sections. Pour trouver la rubrique de classement d'un déchet dans la liste, il faut dès lors procéder par étapes de la manière suivante :

a) Repérer la source produisant le déchet dans les chapitres 01 à 12 ou 17 à 20 et repérer ensuite le code à six chiffres approprié (à l'exception des codes de ces chapitres se terminant par 99). Une installation spécifique peut devoir classer ses activités dans plusieurs chapitres. Par exemple, une usine de voitures peut produire des déchets relevant des chapitres 12 (Déchets provenant de la mise en forme et du traitement de surface des métaux), 11 (Déchets inorganiques contenant des métaux, provenant du traitement et du revêtement des métaux) et 08 (Déchets provenant de l'utilisation de produits de revêtement), car les différents chapitres correspondent aux différentes étapes du processus de production.

Remarque : les déchets d'emballages collectés séparément (y compris les mélanges de différents matériaux d'emballage) sont classés à la section 15 01 et non 20 01.

b) Si aucun code approprié de déchets ne peut être trouvé dans les chapitres 01 à 12 ou 17 à 20, on examine ensuite si un des chapitres 13,14 ou 15 convient pour classer le déchet.

c) Si aucun de ces codes de déchets ne s'applique, le classement du déchet doit se faire dans le chapitre 16.

d) Si le déchet ne relève pas non plus du chapitre 16, on le classe sous la rubrique dont le code se termine par 99 (déchets non spécifiés ailleurs) dans le chapitre de la liste correspondant à l'activité repérée à la première étape.

4. Aux fins des articles R. 541-7 à R. 541-10, on entend par " substance dangereuse " une substance classée comme telle par arrêté pris en application de l'article R. 231-51 du code du travail ; par " métal lourd ", on entend tout composé d'antimoine, d'arsenic, de cadmium, de chrome (VI), de cuivre, de plomb, de mercure, de nickel, de sélénium, de tellure, de thallium et d'étain ainsi que ces matériaux sous forme métallique, pour autant qu'ils soient classés comme substances dangereuses.

5. Si des déchets sont indiqués comme dangereux par une mention spécifique ou générale de substances dangereuses, ces déchets ne sont dangereux que si ces substances sont présentes dans des concentrations (pourcentage en poids) suffisantes pour que les déchets présentent une ou plusieurs des propriétés énumérées à l'annexe I de l'article R. 541-8.

6. Les déchets classés comme dangereux sont indiqués avec un astérisque.

INDEX

CHAPITRES DE LA LISTE

01. Déchets provenant de l'exploration et de l'exploitation des mines et des carrières ainsi que du traitement physique et chimique des minéraux.

02. Déchets provenant de l'agriculture, de l'horticulture, de l'aquaculture, de la sylviculture, de la chasse et de la pêche ainsi que de la préparation et de la transformation des aliments.

03. Déchets provenant de la transformation du bois et de la production de panneaux et de meubles, de pâte à papier, de papier et de carton.

04. Déchets provenant des industries du cuir, de la fourrure et du textile.

05. Déchets provenant du raffinage du pétrole, de la purification du gaz naturel et du traitement pyrolytique du charbon.

06. Déchets des procédés de la chimie minérale.

07. Déchets des procédés de la chimie organique.

08. Déchets provenant de la fabrication, de la formulation, de la distribution et de l'utilisation (FFDU) de produits de revêtement (peintures, vernis et émaux vitrifiés), mastics et encres d'impression.

09. Déchets provenant de l'industrie photographique.

10. Déchets provenant de procédés thermiques.

11. Déchets provenant du traitement chimique de surface et du revêtement des métaux et autres matériaux, et de l'hydrométallurgie des métaux non ferreux.

12. Déchets provenant de la mise en forme et du traitement physique et mécanique de surface des métaux et matières plastiques.

13. Huiles et combustibles liquides usagés (sauf huiles alimentaires et huiles figurant aux chapitres 05,12 et 19).

14. Déchets de solvants organiques, d'agents réfrigérants et propulseurs (sauf chapitres 07 et 08).

15. Emballages et déchets d'emballages, absorbants, chiffons d'essuyage, matériaux filtrants et vêtements de protection non spécifiés ailleurs.

16. Déchets non décrits ailleurs dans la liste.

17. Déchets de construction et de démolition (y compris déblais provenant de sites contaminés).

18. Déchets provenant des soins médicaux ou vétérinaires et/ ou de la recherche associée (sauf déchets de cuisine et de restauration ne provenant pas directement des soins médicaux).

19. Déchets provenant des installations de gestion des déchets, des stations d'épuration des eaux usées hors site et de la préparation d'eau destinée à la consommation humaine et d'eau à usage industriel.

20. Déchets municipaux (déchets ménagers et déchets assimilés provenant des commerces, des industries et des administrations) y compris les fractions collectées séparément.

<table>
<tbody>
<tr>
<td width="119">
<p align="center">N° RUBRIQUE </p>
</td>
<td width="480">
<p align="center">DÉCHETS </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 </p>
</td>
<td valign="top" width="480">
<p align="center">DÉCHETS PROVENANT DE L'EXPLORATION ET DE L'EXPLOITATION DES MINES ET DES CARRIÈRES AINSI QUE DU TRAITEMENT PHYSIQUE ET CHIMIQUE DES MINÉRAUX </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'extraction des minéraux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 01 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'extraction des minéraux métallifères. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 01 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'extraction des minéraux non métallifères. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la transformation physique et chimique des minéraux métallifères. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 03 04* </p>
</td>
<td valign="top" width="480">
<p>Stériles acidogènes provenant de la transformation du sulfure. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 03 05* </p>
</td>
<td valign="top" width="480">
<p>Autres stériles contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 03 06 </p>
</td>
<td valign="top" width="480">
<p>Stériles autres que ceux visés aux rubriques 01 03 04 et 01 03 05.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 03 07* </p>
</td>
<td valign="top" width="480">
<p>Autres déchets contenant des substances dangereuses provenant de la transformation physique et chimique des minéraux métallifères. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 03 08 </p>
</td>
<td valign="top" width="480">
<p>Déchets de poussières et de poudres autres que ceux visés à la rubrique 01 03 07.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 03 09 </p>
</td>
<td valign="top" width="480">
<p>Boues rouges issues de la production d'alumine autres que celles visées à la rubrique 01 03 07.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 03 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 04 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la transformation physique et chimique des minéraux non métallifères. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 04 07* </p>
</td>
<td valign="top" width="480">
<p>Déchets contenant des substances dangereuses provenant de la transformation physique et chimique des minéraux non métallifères. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 04 08 </p>
</td>
<td valign="top" width="480">
<p>Déchets de graviers et débris de pierres autres que ceux visés à la rubrique 01 04 07.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 04 09 </p>
</td>
<td valign="top" width="480">
<p>Déchets de sable et d'argile. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 04 10 </p>
</td>
<td valign="top" width="480">
<p>Déchets de poussières et de poudres autres que ceux visés à la rubrique 01 04 07.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 04 11 </p>
</td>
<td valign="top" width="480">
<p>Déchets de la transformation de la potasse et des sels minéraux autres que ceux visés à la rubrique 01 04 07.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 04 12 </p>
</td>
<td valign="top" width="480">
<p>Stériles et autres déchets provenant du lavage et du nettoyage des minéraux, autres que ceux visés aux rubriques 01 04 07 et 01 04 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 04 13 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la taille et du sciage des pierres autres que ceux visés à la rubrique 01 04 07.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 04 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 05 </p>
</td>
<td valign="top" width="480">
<p>Boues de forage et autres déchets de forage. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 05 04 </p>
</td>
<td valign="top" width="480">
<p>Boues et autres déchets de forage contenant de l'eau douce. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 05 05* </p>
</td>
<td valign="top" width="480">
<p>Boues et autres déchets de forage contenant des hydrocarbures. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 05 06* </p>
</td>
<td valign="top" width="480">
<p>Boues et autres déchets de forage contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 05 07 </p>
</td>
<td valign="top" width="480">
<p>Boues et autres déchets de forage contenant des sels de baryum, autres que ceux visés aux rubriques 01 05 05 et 01 05 06.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 05 08 </p>
</td>
<td valign="top" width="480">
<p>Boues et autres déchets de forage contenant des chlorures, autres que ceux visés aux rubriques 01 05 05 et 01 05 06.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>01 05 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 </p>
</td>
<td valign="top" width="480">
<p align="center">DÉCHETS PROVENANT DE L'AGRICULTURE, DE L'HORTICULTURE, DE L'AQUACULTURE, DE LA SYLVICULTURE, DE LA CHASSE ET DE LA PÊCHE AINSI QUE DE LA PRÉPARATION ET DE LA TRANSFORMATION DES ALIMENTS </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'agriculture, de l'horticulture, de l'aquaculture, de la sylviculture, de la chasse et de la pêche. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 01 01 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du lavage et du nettoyage. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 01 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets de tissus animaux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 01 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets de tissus végétaux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 01 04 </p>
</td>
<td valign="top" width="480">
<p>Déchets de matières plastiques (à l'exclusion des emballages). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 01 06 </p>
</td>
<td valign="top" width="480">
<p>Fèces, urine et fumier (y compris paille souillée), affluents, collectés séparément et traités hors site. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 01 07 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la sylviculture. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 01 08* </p>
</td>
<td valign="top" width="480">
<p>Déchets agrochimiques contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 01 09 </p>
</td>
<td valign="top" width="480">
<p>Déchets agrochimiques autres que ceux visés à la rubrique 02 01 08.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 01 10 </p>
</td>
<td valign="top" width="480">
<p>Déchets métalliques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 01 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la préparation et de la transformation de la viande, des poissons et autres aliments d'origine animale. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 02 01 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du lavage et du nettoyage. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 02 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets de tissus animaux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 02 03 </p>
</td>
<td valign="top" width="480">
<p>Matières impropres à la consommation ou à la transformation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 02 04 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 02 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la préparation et de la transformation des fruits, des légumes, des céréales, des huiles alimentaires, du cacao, du café, du thé et du tabac, de la production de conserves, de la production de levures et d'extraits de levures, de la préparation et de la fermentation de mélasses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 03 01 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du lavage, du nettoyage, de l'épluchage, de la centrifugation et de la séparation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 03 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets d'agents de conservation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 03 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets de l'extraction aux solvants. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 03 04 </p>
</td>
<td valign="top" width="480">
<p>Matières impropres à la consommation ou à la transformation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 03 05 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 03 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 04 </p>
</td>
<td valign="top" width="480">
<p>Déchets de la transformation du sucre. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 04 01 </p>
</td>
<td valign="top" width="480">
<p>Terre provenant du lavage et du nettoyage des betteraves. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 04 02 </p>
</td>
<td valign="top" width="480">
<p>Carbonate de calcium déclassé. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 04 03 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 04 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 05 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'industrie des produits laitiers. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 05 01 </p>
</td>
<td valign="top" width="480">
<p>Matières impropres à la consommation ou à la transformation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 05 02 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 05 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 06 </p>
</td>
<td valign="top" width="480">
<p>Déchets de boulangerie, pâtisserie, confiserie. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 06 01 </p>
</td>
<td valign="top" width="480">
<p>Matières impropres à la consommation ou à la transformation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 06 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets d'agents de conservation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 06 03 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 06 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 07 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la production de boissons alcooliques et non alcooliques (sauf café, thé et cacao). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 07 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du lavage, du nettoyage et de la réduction mécanique des matières premières. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 07 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets de la distillation de l'alcool. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 07 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets de traitements chimiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 07 04 </p>
</td>
<td valign="top" width="480">
<p>Matières impropres à la consommation ou à la transformation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 07 05 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>02 07 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 </p>
</td>
<td valign="top" width="480">
<p align="center">DÉCHETS PROVENANT DE LA TRANSFORMATION DU BOIS ET DE LA PRODUCTION DE PANNEAUX ET DE MEUBLES, DE PÂTE À PAPIER, DE PAPIER ET DE CARTON </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la transformation du bois et de la fabrication de panneaux et de meubles. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 01 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets d'écorce et de liège. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 01 04* </p>
</td>
<td valign="top" width="480">
<p>Sciure de bois, copeaux, chutes, bois, panneaux de particules et placages contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 01 05 </p>
</td>
<td valign="top" width="480">
<p>Sciure de bois, copeaux, chutes, bois, panneaux de particules et placages autres que ceux visés à la rubrique 03 01 04.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 01 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets des produits de protection du bois. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 02 01* </p>
</td>
<td valign="top" width="480">
<p>Composés organiques non halogénés de protection du bois. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 02 02* </p>
</td>
<td valign="top" width="480">
<p>Composés organochlorés de protection du bois. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 02 03* </p>
</td>
<td valign="top" width="480">
<p>Composés organométalliques de protection du bois. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 02 04* </p>
</td>
<td valign="top" width="480">
<p>Composés inorganiques de protection du bois. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 02 05* </p>
</td>
<td valign="top" width="480">
<p>Autres produits de protection du bois contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 02 99 </p>
</td>
<td valign="top" width="480">
<p>Produits de protection du bois non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la production et de la transformation de papier, de carton et de pâte à papier. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 03 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets d'écorce et de bois. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 03 02 </p>
</td>
<td valign="top" width="480">
<p>Boues vertes (provenant de la récupération de liqueur de cuisson). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 03 05 </p>
</td>
<td valign="top" width="480">
<p>Boues de désencrage provenant du recyclage du papier. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 03 07 </p>
</td>
<td valign="top" width="480">
<p>Refus séparés mécaniquement provenant du recyclage de déchets de papier et de carton. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 03 08 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du tri de papier et de carton destinés au recyclage. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 03 09 </p>
</td>
<td valign="top" width="480">
<p>Boues carbonatées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 03 10 </p>
</td>
<td valign="top" width="480">
<p>Refus fibreux, boues de fibres, de charge et de couchage provenant d'une séparation mécanique. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 03 11 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents autres que celles visées à la rubrique 03 03 10.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>03 03 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 </p>
</td>
<td valign="top" width="480">
<p align="center">DÉCHETS PROVENANT DES INDUSTRIES DU CUIR, DE LA FOURRURE ET DU TEXTILE </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'industrie du cuir et de la fourrure. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 01 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets d'écharnage et refentes. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 01 02 </p>
</td>
<td valign="top" width="480">
<p>Résidus de pelanage. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 01 03* </p>
</td>
<td valign="top" width="480">
<p>Déchets de dégraissage contenant des solvants sans phase liquide. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 01 04 </p>
</td>
<td valign="top" width="480">
<p>Liqueur de tannage contenant du chrome. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 01 05 </p>
</td>
<td valign="top" width="480">
<p>Liqueur de tannage sans chrome. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 01 06 </p>
</td>
<td valign="top" width="480">
<p>Boues, notamment provenant du traitement in situ des effluents, contenant du chrome. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 01 07 </p>
</td>
<td valign="top" width="480">
<p>Boues, notamment provenant du traitement in situ des effluents, sans chrome. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 01 08 </p>
</td>
<td valign="top" width="480">
<p>Déchets de cuir tanné (refentes sur bleu, dérayures, échantillonnages, poussières de ponçage), contenant du chrome. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 01 09 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'habillage et des finitions. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 01 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets de l'industrie textile. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 02 09 </p>
</td>
<td valign="top" width="480">
<p>Matériaux composites (textile imprégné, élastomère, plastomère). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 02 10 </p>
</td>
<td valign="top" width="480">
<p>Matières organiques issues de produits naturels (par exemple : graisse, cire). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 02 14* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant des finitions contenant des solvants organiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 02 15 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant des finitions autres que ceux visés à la rubrique 04 02 14.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 02 16* </p>
</td>
<td valign="top" width="480">
<p>Teintures et pigments contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 02 17 </p>
</td>
<td valign="top" width="480">
<p>Teintures et pigments autres que ceux visés à la rubrique 04 02 16.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 02 19* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 02 20 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents autres que celles visées à la rubrique 04 02 19.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 02 21 </p>
</td>
<td valign="top" width="480">
<p>Fibres textiles non ouvrées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 02 22 </p>
</td>
<td valign="top" width="480">
<p>Fibres textiles ouvrées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>04 02 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 </p>
</td>
<td valign="top" width="480">
<p align="center">DÉCHETS PROVENANT DU RAFFINAGE DU PÉTROLE, DE LA PURIFICATION DU GAZ NATUREL ET DU TRAITEMENT PYROLYTIQUE DU CHARBON </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du raffinage du pétrole. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 01 02* </p>
</td>
<td valign="top" width="480">
<p>Boues de dessalage. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 01 03* </p>
</td>
<td valign="top" width="480">
<p>Boues de fond de cuves. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 01 04* </p>
</td>
<td valign="top" width="480">
<p>Boues d'alkyles acides. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 01 05* </p>
</td>
<td valign="top" width="480">
<p>Hydrocarbures accidentellement répandus. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 01 06* </p>
</td>
<td valign="top" width="480">
<p>Boues contenant des hydrocarbures provenant des opérations de maintenance de l'installation ou des équipements. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 01 07* </p>
</td>
<td valign="top" width="480">
<p>Goudrons acides. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 01 08* </p>
</td>
<td valign="top" width="480">
<p>Autres goudrons et bitumes. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 01 09* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 01 10 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents autres que celles visées à la rubrique 05 01 09.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 01 11* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du nettoyage d'hydrocarbures avec des bases. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 01 12* </p>
</td>
<td valign="top" width="480">
<p>Hydrocarbures contenant des acides. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 01 13 </p>
</td>
<td valign="top" width="480">
<p>Boues du traitement de l'eau d'alimentation des chaudières. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 01 14* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant des colonnes de refroidissement. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 01 15* </p>
</td>
<td valign="top" width="480">
<p>Argiles de filtration usées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 01 16 </p>
</td>
<td valign="top" width="480">
<p>Déchets contenant du soufre provenant de la désulfuration du pétrole. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 01 17 </p>
</td>
<td valign="top" width="480">
<p>Mélanges bitumineux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 01 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 06 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du traitement pyrolytique du charbon. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 06 01* </p>
</td>
<td valign="top" width="480">
<p>Goudrons acides. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 06 03* </p>
</td>
<td valign="top" width="480">
<p>Autres goudrons. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 06 04 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant des colonnes de refroidissement. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 06 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 07 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la purification et du transport du gaz naturel. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 07 01* </p>
</td>
<td valign="top" width="480">
<p>Déchets contenant du mercure. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 07 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets contenant du soufre. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>05 07 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 </p>
</td>
<td valign="top" width="480">
<p align="center">DÉCHETS DES PROCÉDÉS DE LA CHIMIE MINÉRALE </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la fabrication, formulation, distribution et utilisation (FFDU) d'acides. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 01 01* </p>
</td>
<td valign="top" width="480">
<p>Acide sulfurique et acide sulfureux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 01 02* </p>
</td>
<td valign="top" width="480">
<p>Acide chlorhydrique. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 01 03* </p>
</td>
<td valign="top" width="480">
<p>Acide fluorhydrique. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 01 04* </p>
</td>
<td valign="top" width="480">
<p>Acide phosphorique et acide phosphoreux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 01 05* </p>
</td>
<td valign="top" width="480">
<p>Acide nitrique et acide nitreux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 01 06* </p>
</td>
<td valign="top" width="480">
<p>Autres acides. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 01 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la FFDU de bases. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 02 01* </p>
</td>
<td valign="top" width="480">
<p>Hydroxyde de calcium. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 02 03* </p>
</td>
<td valign="top" width="480">
<p>Hydroxyde d'ammonium. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 02 04* </p>
</td>
<td valign="top" width="480">
<p>Hydroxyde de sodium et hydroxyde de potassium. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 02 05* </p>
</td>
<td valign="top" width="480">
<p>Autres bases. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 02 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la FFDU de sels et leurs solutions et d'oxydes métalliques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 03 11* </p>
</td>
<td valign="top" width="480">
<p>Sels solides et solutions contenant des cyanures. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 03 13* </p>
</td>
<td valign="top" width="480">
<p>Sels solides et solutions contenant des métaux lourds. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 03 14 </p>
</td>
<td valign="top" width="480">
<p>Sels solides et solutions autres que ceux visés aux rubriques 06 03 11 et 06 03 13.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 03 15* </p>
</td>
<td valign="top" width="480">
<p>Oxydes métalliques contenant des métaux lourds. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 03 16 </p>
</td>
<td valign="top" width="480">
<p>Oxydes métalliques autres que ceux visés à la rubrique 06 03 15.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 03 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 04 </p>
</td>
<td valign="top" width="480">
<p>Déchets contenant des métaux autres que ceux visés à la section 06 03.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 04 03* </p>
</td>
<td valign="top" width="480">
<p>Déchets contenant de l'arsenic. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 04 04* </p>
</td>
<td valign="top" width="480">
<p>Déchets contenant du mercure. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 04 05* </p>
</td>
<td valign="top" width="480">
<p>Déchets contenant d'autres métaux lourds. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 04 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 05 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 05 02* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 05 03 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents autres que celles visées à la rubrique 06 05 02.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 06 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la FFDU de produits chimiques contenant du soufre, de la chimie du soufre et des procédés de désulfuration. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 06 02* </p>
</td>
<td valign="top" width="480">
<p>Déchets contenant des sulfures dangereux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 06 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets contenant des sulfures autres que ceux visés à la rubrique 06 06 02.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 06 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 07 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la FFDU des halogènes et de la chimie des halogènes. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 07 01* </p>
</td>
<td valign="top" width="480">
<p>Déchets contenant de l'amiante provenant de l'électrolyse. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 07 02* </p>
</td>
<td valign="top" width="480">
<p>Déchets de charbon actif utilisé pour la production du chlore. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 07 03* </p>
</td>
<td valign="top" width="480">
<p>Boues de sulfate de baryum contenant du mercure. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 07 04* </p>
</td>
<td valign="top" width="480">
<p>Solutions et acides, par exemple, acide de contact. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 07 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 08 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la FFDU du silicium et des dérivés du silicium. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 08 02* </p>
</td>
<td valign="top" width="480">
<p>Déchets contenant des chlorosilanes dangereux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 08 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 09 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la FFDU des produits chimiques contenant du phosphore et de la chimie du phosphore. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 09 02 </p>
</td>
<td valign="top" width="480">
<p>Scories phosphoriques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 09 03* </p>
</td>
<td valign="top" width="480">
<p>Déchets de réactions basées sur le calcium contenant des substances dangereuses ou contaminées par de telles substances. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 09 04 </p>
</td>
<td valign="top" width="480">
<p>Déchets de réactions basées sur le calcium autres que ceux visés à la rubrique 06 09 03.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 09 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 10 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la FFDU de produits chimiques contenant de l'azote, de la chimie de l'azote et de la production d'engrais. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 10 02* </p>
</td>
<td valign="top" width="480">
<p>Déchets contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 10 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 11 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la fabrication des pigments inorganiques et des opacifiants. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 11 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets de réactions basées sur le calcium provenant de la production de dioxyde de titane. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 11 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 13 </p>
</td>
<td valign="top" width="480">
<p>Déchets des procédés de la chimie minérale non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 13 01* </p>
</td>
<td valign="top" width="480">
<p>Produits phytosanitaires inorganiques, agents de protection du bois et autres biocides. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 13 02* </p>
</td>
<td valign="top" width="480">
<p>Charbon actif usé (sauf rubrique 06 07 02). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 13 03 </p>
</td>
<td valign="top" width="480">
<p>Noir de carbone. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 13 04* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la transformation de l'amiante. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 13 05* </p>
</td>
<td valign="top" width="480">
<p>Suies. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>06 13 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 </p>
</td>
<td valign="top" width="480">
<p align="center">DÉCHETS DES PROCÉDÉS DE LA CHIMIE ORGANIQUE </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la fabrication, formulation, distribution et utilisation (FFDU) de produits organiques de base. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 01 01* </p>
</td>
<td valign="top" width="480">
<p>Eaux de lavage et liqueurs mères aqueuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 01 03* </p>
</td>
<td valign="top" width="480">
<p>Solvants, liquides de lavage et liqueurs mères organiques halogénés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 01 04* </p>
</td>
<td valign="top" width="480">
<p>Autres solvants, liquides de lavage et liqueurs mères organiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 01 07* </p>
</td>
<td valign="top" width="480">
<p>Résidus de réaction et résidus de distillation halogénés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 01 08* </p>
</td>
<td valign="top" width="480">
<p>Autres résidus de réaction et résidus de distillation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 01 09* </p>
</td>
<td valign="top" width="480">
<p>Gâteaux de filtration et absorbants usés halogénés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 01 10* </p>
</td>
<td valign="top" width="480">
<p>Autres gâteaux de filtration et absorbants usés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 01 11* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 01 12 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents autres que celles visées à la rubrique 07 01 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 01 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la FFDU de matières plastiques, caoutchouc et fibres synthétiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 02 01* </p>
</td>
<td valign="top" width="480">
<p>Eaux de lavage et liqueurs mères aqueuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 02 03* </p>
</td>
<td valign="top" width="480">
<p>Solvants, liquides de lavage et liqueurs mères organiques halogénés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 02 04* </p>
</td>
<td valign="top" width="480">
<p>Autres solvants, liquides de lavage et liqueurs mères organiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 02 07* </p>
</td>
<td valign="top" width="480">
<p>Résidus de réaction et résidus de distillation halogénés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 02 08* </p>
</td>
<td valign="top" width="480">
<p>Autres résidus de réaction et résidus de distillation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 02 09* </p>
</td>
<td valign="top" width="480">
<p>Gâteaux de filtration et absorbants usés halogénés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 02 10* </p>
</td>
<td valign="top" width="480">
<p>Autres gâteaux de filtration et absorbants usés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 02 11* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 02 12 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents autres que celles visées à la rubrique 07 02 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 02 13 </p>
</td>
<td valign="top" width="480">
<p>Déchets plastiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 02 14* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant d'additifs contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 02 15 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant d'additifs autres que ceux visés à la rubrique 07 02 14.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 02 16* </p>
</td>
<td valign="top" width="480">
<p>Déchets contenant des silicones dangereux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 02 17 </p>
</td>
<td valign="top" width="480">
<p>Déchets contenant des silicones autres que ceux mentionnés à la rubrique 07 02 16.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 02 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la FFDU de teintures et pigments organiques (sauf section 06 11). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 03 01* </p>
</td>
<td valign="top" width="480">
<p>Eaux de lavage et liqueurs mères aqueuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 03 03* </p>
</td>
<td valign="top" width="480">
<p>Solvants, liquides de lavage et liqueurs mères organiques halogénés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 03 04* </p>
</td>
<td valign="top" width="480">
<p>Autres solvants, liquides de lavage et liqueurs mères organiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 03 07* </p>
</td>
<td valign="top" width="480">
<p>Résidus de réaction et résidus de distillation halogénés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 03 08* </p>
</td>
<td valign="top" width="480">
<p>Autres résidus de réaction et résidus de distillation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 03 09* </p>
</td>
<td valign="top" width="480">
<p>Gâteaux de filtration et absorbants usés halogénés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 03 10* </p>
</td>
<td valign="top" width="480">
<p>Autres gâteaux de filtration et absorbants usés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 03 11* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 03 12 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents autres que celles visées à la rubrique 07 03 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 03 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 04 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la FFDU de produits phytosanitaires organiques (sauf rubriques 02 01 08 et 02 01 09), d'agents de protection du bois (sauf section 03 02) et d'autres biocides. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 04 01* </p>
</td>
<td valign="top" width="480">
<p>Eaux de lavage et liqueurs mères aqueuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 04 03* </p>
</td>
<td valign="top" width="480">
<p>Solvants, liquides de lavage et liqueurs mères organiques halogénés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 04 04* </p>
</td>
<td valign="top" width="480">
<p>Autres solvants, liquides de lavage et liqueurs mères organiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 04 07* </p>
</td>
<td valign="top" width="480">
<p>Résidus de réaction et résidus de distillation halogénés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 04 08* </p>
</td>
<td valign="top" width="480">
<p>Autres résidus de réaction et résidus de distillation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 04 09* </p>
</td>
<td valign="top" width="480">
<p>Gâteaux de filtration et absorbants usés halogénés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 04 10* </p>
</td>
<td valign="top" width="480">
<p>Autres gâteaux de filtration et absorbants usés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 04 11* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 04 12 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents autres que celles visées à la rubrique 07 04 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 04 13* </p>
</td>
<td valign="top" width="480">
<p>Déchets solides contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 04 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 05 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la FFDU des produits pharmaceutiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 05 01* </p>
</td>
<td valign="top" width="480">
<p>Eaux de lavage et liqueurs mères aqueuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 05 03* </p>
</td>
<td valign="top" width="480">
<p>Solvants, liquides de lavage et liqueurs mères organiques halogénés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 05 04* </p>
</td>
<td valign="top" width="480">
<p>Autres solvants, liquides de lavage et liqueurs mères organiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 05 07* </p>
</td>
<td valign="top" width="480">
<p>Résidus de réaction et résidus de distillation halogénés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 05 08* </p>
</td>
<td valign="top" width="480">
<p>Autres résidus de réaction et résidus de distillation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 05 09* </p>
</td>
<td valign="top" width="480">
<p>Gâteaux de filtration et absorbants usés halogénés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 05 10* </p>
</td>
<td valign="top" width="480">
<p>Autres gâteaux de filtration et absorbants usés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 05 11* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 05 12 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents autres que celles visées à la rubrique 07 05 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 05 13* </p>
</td>
<td valign="top" width="480">
<p>Déchets solides contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 05 14 </p>
</td>
<td valign="top" width="480">
<p>Déchets solides autres que ceux visés à la rubrique 07 05 13.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 05 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 06 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la FFDU des corps gras, savons, détergents, désinfectants et cosmétiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 06 01* </p>
</td>
<td valign="top" width="480">
<p>Eaux de lavage et liqueurs mères aqueuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 06 03* </p>
</td>
<td valign="top" width="480">
<p>Solvants, liquides de lavage et liqueurs mères organiques halogénés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 06 04* </p>
</td>
<td valign="top" width="480">
<p>Autres solvants, liquides de lavage et liqueurs mères organiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 06 07* </p>
</td>
<td valign="top" width="480">
<p>Résidus de réaction et résidus de distillation halogénés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 06 08* </p>
</td>
<td valign="top" width="480">
<p>Autres résidus de réaction et résidus de distillation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 06 09* </p>
</td>
<td valign="top" width="480">
<p>Gâteaux de filtration et absorbants usés halogénés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 06 10* </p>
</td>
<td valign="top" width="480">
<p>Autres gâteaux de filtration et absorbants usés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 06 11* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 06 12 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents autres que celles visées à la rubrique 07 06 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 06 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 07 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la FFDU de produits chimiques issus de la chimie fine et de produits chimiques non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 07 01* </p>
</td>
<td valign="top" width="480">
<p>Eaux de lavage et liqueurs mères aqueuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 07 03* </p>
</td>
<td valign="top" width="480">
<p>Solvants, liquides de lavage et liqueurs mères organiques halogènes. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 07 04* </p>
</td>
<td valign="top" width="480">
<p>Autres solvants, liquides de lavage et liqueurs mères organiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 07 07* </p>
</td>
<td valign="top" width="480">
<p>Résidus de réaction et résidus de distillation halogènes. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 07 08* </p>
</td>
<td valign="top" width="480">
<p>Autres résidus de réaction et résidus de distillation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 07 09* </p>
</td>
<td valign="top" width="480">
<p>Gâteaux de filtration et absorbants usés halogènes. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 07 10* </p>
</td>
<td valign="top" width="480">
<p>Autres gâteaux de filtration et absorbants usés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 07 11* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 07 12 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents autres que celles visées à la rubrique 07 07 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>07 07 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 </p>
</td>
<td valign="top" width="480">
<p align="center">DÉCHETS PROVENANT DE LA FABRICATION, DE LA FORMULATION, DE LA DISTRIBUTION ET DE L'UTILISATION (FFDU) DE PRODUITS DE REVÊTEMENT (PEINTURES, VERNIS ET ÉMAUX VITRIFIÉS), MASTICS ET ENCRES D'IMPRESSION </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la FFDU et du décapage de peintures et vernis. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 01 11* </p>
</td>
<td valign="top" width="480">
<p>Déchets de peintures et vernis contenant des solvants organiques ou d'autres substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 01 12 </p>
</td>
<td valign="top" width="480">
<p>Déchets de peintures ou vernis autres que ceux visés à la rubrique 08 01 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 01 13* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant de peintures ou vernis contenant des solvants organiques ou autres substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 01 14 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant de peintures ou vernis autres que celles visées à la rubrique 08 01 13.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 01 15* </p>
</td>
<td valign="top" width="480">
<p>Boues aqueuses contenant de la peinture ou du vernis contenant des solvants organiques ou autres substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 01 16 </p>
</td>
<td valign="top" width="480">
<p>Boues aqueuses contenant de la peinture ou du vernis autres que celles visées à la rubrique 08 01 15.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 01 17* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du décapage de peintures ou vernis contenant des solvants organiques ou autres substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 01 18 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du décapage de peintures ou vernis autres que ceux visés à la rubrique 08 01 17.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 01 19* </p>
</td>
<td valign="top" width="480">
<p>Suspensions aqueuses contenant de la peinture ou du vernis contenant des solvants organiques ou autres substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 01 20 </p>
</td>
<td valign="top" width="480">
<p>Suspensions aqueuses contenant de la peinture ou du vernis autres que celles visées à la rubrique 08 01 19.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 01 21* </p>
</td>
<td valign="top" width="480">
<p>Déchets de décapants de peintures ou vernis. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 01 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la FFDU d'autres produits de revêtement (y compris des matériaux céramiques). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 02 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets de produits de revêtement en poudre. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 02 02 </p>
</td>
<td valign="top" width="480">
<p>Boues aqueuses contenant des matériaux céramiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 02 03 </p>
</td>
<td valign="top" width="480">
<p>Suspensions aqueuses contenant des matériaux céramiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 02 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la FFDU d'encres d'impression. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 03 07 </p>
</td>
<td valign="top" width="480">
<p>Boues aqueuses contenant de l'encre. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 03 08 </p>
</td>
<td valign="top" width="480">
<p>Déchets liquides aqueux contenant de l'encre. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 03 12* </p>
</td>
<td valign="top" width="480">
<p>Déchets d'encres contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 03 13 </p>
</td>
<td valign="top" width="480">
<p>Déchets d'encres autres que ceux visés à la rubrique 08 03 12.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 03 14* </p>
</td>
<td valign="top" width="480">
<p>Boues d'encre contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 03 15 </p>
</td>
<td valign="top" width="480">
<p>Boues d'encre autres que celles visées à la rubrique 08 03 14.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 03 16* </p>
</td>
<td valign="top" width="480">
<p>Déchets de solutions de gravure à l'eau-forte. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 03 17* </p>
</td>
<td valign="top" width="480">
<p>Déchets de toner d'impression contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 03 18 </p>
</td>
<td valign="top" width="480">
<p>Déchets de toner d'impression autres que ceux visés à la rubrique 08 03 17.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 03 19* </p>
</td>
<td valign="top" width="480">
<p>Huiles dispersées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 03 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 04 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la FFDU de colles et mastics (y compris produits d'étanchéité). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 04 09* </p>
</td>
<td valign="top" width="480">
<p>Déchets de colles et mastics contenant des solvants organiques ou d'autres substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 04 10 </p>
</td>
<td valign="top" width="480">
<p>Déchets de colles et mastics autres que ceux visés à la rubrique 08 04 09.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 04 11* </p>
</td>
<td valign="top" width="480">
<p>Boues de colles et mastics contenant des solvants organiques ou d'autres substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 04 12 </p>
</td>
<td valign="top" width="480">
<p>Boues de colles et mastics autres que celles visées à la rubrique 08 04 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 04 13* </p>
</td>
<td valign="top" width="480">
<p>Boues aqueuses contenant des colles ou mastics contenant des solvants organiques ou d'autres substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 04 14 </p>
</td>
<td valign="top" width="480">
<p>Boues aqueuses contenant des colles et mastics autres que celles visées à la rubrique 08 04 13.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 04 15* </p>
</td>
<td valign="top" width="480">
<p>Déchets liquides aqueux contenant des colles ou mastics contenant des solvants organiques ou d'autres substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 04 16 </p>
</td>
<td valign="top" width="480">
<p>Déchets liquides aqueux contenant des colles ou mastics autres que ceux visés à la rubrique 08 04 15.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 04 17* </p>
</td>
<td valign="top" width="480">
<p>Huile de résine </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 04 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 05 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs dans le chapitre 08.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>08 05 01* </p>
</td>
<td valign="top" width="480">
<p>Déchets d'isocyanates. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>09 </p>
</td>
<td valign="top" width="480">
<p align="center">DÉCHETS PROVENANT DE L'INDUSTRIE PHOTOGRAPHIQUE </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>09 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets de l'industrie photographique. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>09 01 01* </p>
</td>
<td valign="top" width="480">
<p>Bains de développement aqueux contenant un activateur. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>09 01 02* </p>
</td>
<td valign="top" width="480">
<p>Bains de développement aqueux pour plaques offset. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>09 01 03* </p>
</td>
<td valign="top" width="480">
<p>Bains de développement contenant des solvants. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>09 01 04* </p>
</td>
<td valign="top" width="480">
<p>Bains de fixation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>09 01 05* </p>
</td>
<td valign="top" width="480">
<p>Bains de blanchiment et bains de blanchiment/ fixation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>09 01 06* </p>
</td>
<td valign="top" width="480">
<p>Déchets contenant de l'argent provenant du traitement in situ des déchets photographiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>09 01 07 </p>
</td>
<td valign="top" width="480">
<p>Pellicules et papiers photographiques contenant de l'argent ou des composés de l'argent. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>09 01 08 </p>
</td>
<td valign="top" width="480">
<p>Pellicules et papiers photographiques sans argent ni composés de l'argent. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>09 01 10 </p>
</td>
<td valign="top" width="480">
<p>Appareils photographiques à usage unique sans piles. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>09 01 11* </p>
</td>
<td valign="top" width="480">
<p>Appareils photographiques à usage unique contenant des piles visées aux rubriques 16 06 01,16 06 02 ou 16 06 03.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>09 01 12 </p>
</td>
<td valign="top" width="480">
<p>Appareils photographiques à usage unique contenant des piles autres que ceux visés à la rubrique 09 01 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>09 01 13* </p>
</td>
<td valign="top" width="480">
<p>Déchets liquides aqueux provenant de la récupération in situ de l'argent autres que ceux visés à la rubrique 09 01 06.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>09 01 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 </p>
</td>
<td valign="top" width="480">
<p align="center">DÉCHETS PROVENANT DE PROCÉDÉS THERMIQUES </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de centrales électriques et autres installations de combustion (sauf chapitre 19). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 01 </p>
</td>
<td valign="top" width="480">
<p>Mâchefers, scories et cendres sous chaudière (sauf cendres sous chaudière visées à la rubrique 10 01 04). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 02 </p>
</td>
<td valign="top" width="480">
<p>Cendres volantes de charbon. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 03 </p>
</td>
<td valign="top" width="480">
<p>Cendres volantes de tourbe et de bois non traité. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 04* </p>
</td>
<td valign="top" width="480">
<p>Cendres volantes et cendres sous chaudière d'hydrocarbures. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 05 </p>
</td>
<td valign="top" width="480">
<p>Déchets solides de réactions basées sur le calcium, provenant de la désulfuration des gaz de fumée. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 07 </p>
</td>
<td valign="top" width="480">
<p>Boues de réactions basées sur le calcium, provenant de la désulfuration des gaz de fumée. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 09* </p>
</td>
<td valign="top" width="480">
<p>Acide sulfurique. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 13* </p>
</td>
<td valign="top" width="480">
<p>Cendres volantes provenant d'hydrocarbures émulsifiés employés comme combustibles. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 14* </p>
</td>
<td valign="top" width="480">
<p>Mâchefers, scories et cendres sous chaudière provenant de la coïncinération contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 15 </p>
</td>
<td valign="top" width="480">
<p>Mâchefers, scories et cendres sous chaudière provenant de la coïncinération autres que ceux visés à la rubrique 10 01 14.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 16* </p>
</td>
<td valign="top" width="480">
<p>Cendres volantes provenant de la coïncinération contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 17 </p>
</td>
<td valign="top" width="480">
<p>Cendres volantes provenant de la coïncinération autres que celles visées à la rubrique 10 01 16.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 18* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des gaz contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 19 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des gaz autres que ceux visés aux rubriques 10 01 05,10 01 07 et 10 01 18.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 20* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 21 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents autres que celles visées à la rubrique 10 01 20.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 22* </p>
</td>
<td valign="top" width="480">
<p>Boues aqueuses provenant du nettoyage des chaudières contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 23 </p>
</td>
<td valign="top" width="480">
<p>Boues aqueuses provenant du nettoyage des chaudières autres que celles visées à la rubrique 10 01 22.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 24 </p>
</td>
<td valign="top" width="480">
<p>Sables provenant de lits fluidisés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 25 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du stockage et de la préparation des combustibles des centrales à charbon. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 26 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des eaux de refroidissement. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 01 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'industrie du fer et de l'acier. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 02 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets de laitiers de hauts-fourneaux et d'aciéries. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 02 02 </p>
</td>
<td valign="top" width="480">
<p>Laitiers non traités. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 02 07* </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant de l'épuration des fumées contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 02 08 </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant de l'épuration des fumées autres que ceux visés à la rubrique 10 02 07.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 02 10 </p>
</td>
<td valign="top" width="480">
<p>Battitures de laminoir. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 02 11* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des eaux de refroidissement contenant des hydrocarbures. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 02 12 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des eaux de refroidissement autres que ceux visés à la rubrique 10 02 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 02 13* </p>
</td>
<td valign="top" width="480">
<p>Boues et gâteaux de filtration provenant de l'épuration des fumées contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 02 14 </p>
</td>
<td valign="top" width="480">
<p>Boues et gâteaux de filtration provenant de l'épuration des fumées autres que ceux visés à la rubrique 10 02 13.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 02 15 </p>
</td>
<td valign="top" width="480">
<p>Autres boues et gâteaux de filtration. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 02 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets de la pyrométallurgie de l'aluminium. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets d'anodes. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 04* </p>
</td>
<td valign="top" width="480">
<p>Scories provenant de la production primaire. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 05 </p>
</td>
<td valign="top" width="480">
<p>Déchets d'alumine. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 08* </p>
</td>
<td valign="top" width="480">
<p>Scories salées de production secondaire. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 09* </p>
</td>
<td valign="top" width="480">
<p>Crasses noires de production secondaire. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 15* </p>
</td>
<td valign="top" width="480">
<p>Ecumes inflammables ou émettant, au contact de l'eau, des gaz inflammables en quantités dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 16 </p>
</td>
<td valign="top" width="480">
<p>Ecumes autres que celles visées à la rubrique 10 03 15.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 17* </p>
</td>
<td valign="top" width="480">
<p>Déchets goudronnés provenant de la fabrication des anodes. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 18 </p>
</td>
<td valign="top" width="480">
<p>Déchets carbonés provenant de la fabrication des anodes autres que ceux visés à la rubrique 10 03 17.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 19* </p>
</td>
<td valign="top" width="480">
<p>Poussières de filtration des fumées contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 20 </p>
</td>
<td valign="top" width="480">
<p>Poussières de filtration des fumées autres que celles visées à la rubrique 10 03 19.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 21* </p>
</td>
<td valign="top" width="480">
<p>Autres fines de poussières (y compris fines de broyage de crasses) contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 22 </p>
</td>
<td valign="top" width="480">
<p>Autres fines et poussières (y compris fines de broyage de crasses) autres que celles visées à la rubrique 10 03 21.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 23* </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant de l'épuration des fumées contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 24 </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant de l'épuration des fumées autres que ceux visés à la rubrique 10 03 23.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 25 </p>
</td>
<td valign="top" width="480">
<p>Boues et gâteaux de filtration provenant de l'épuration des fumées contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 26 </p>
</td>
<td valign="top" width="480">
<p>Boues et gâteaux de filtration provenant de l'épuration des fumées autres que ceux visés à la rubrique 10 03 25.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 27* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des eaux de refroidissement contenant des hydrocarbures. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 28 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des eaux de refroidissement autres que ceux visés à la rubrique 10 03 27.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 29* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du traitement des scories salées et du traitement des crasses noires contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 30 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du traitement des scories salées et du traitement des crasses noires autres que ceux visés à la rubrique 10 03 29.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 03 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 04 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la pyrométallurgie du plomb. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 04 01* </p>
</td>
<td valign="top" width="480">
<p>Scories provenant de la production primaire et secondaire. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 04 02* </p>
</td>
<td valign="top" width="480">
<p>Crasses et écumes provenant de la production primaire et secondaire. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 04 03* </p>
</td>
<td valign="top" width="480">
<p>Arséniate de calcium. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 04 04* </p>
</td>
<td valign="top" width="480">
<p>Poussières de filtration des fumées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 04 05* </p>
</td>
<td valign="top" width="480">
<p>Autres fines et poussières. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 04 06* </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant de l'épuration des fumées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 04 07* </p>
</td>
<td valign="top" width="480">
<p>Boues et gâteaux de filtration provenant de l'épuration des fumées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 04 09* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des eaux de refroidissement contenant des hydrocarbures. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 04 10 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des eaux de refroidissement autres que ceux visés à la rubrique 10 04 09.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 04 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 05 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la pyrométallurgie du zinc. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 05 01 </p>
</td>
<td valign="top" width="480">
<p>Scories provenant de la production primaire et secondaire. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 05 03* </p>
</td>
<td valign="top" width="480">
<p>Poussières de filtration des fumées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 05 04 </p>
</td>
<td valign="top" width="480">
<p>Autres fines et poussières. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 05 05* </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant de l'épuration des fumées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 05 06* </p>
</td>
<td valign="top" width="480">
<p>Boues et gâteaux de filtration provenant de l'épuration des fumées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 05 08* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des eaux de refroidissement contenant des hydrocarbures. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 05 09 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des eaux de refroidissement autres que ceux visés à la rubrique 10 05 08.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 05 10* </p>
</td>
<td valign="top" width="480">
<p>Crasses et écumes inflammables ou émettant, au contact de l'eau, des gaz inflammables en quantités dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 05 11 </p>
</td>
<td valign="top" width="480">
<p>Crasses et écumes autres que celles visées à la rubrique 10 05 10.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 05 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 06 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la pyrométallurgie du cuivre. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 06 01 </p>
</td>
<td valign="top" width="480">
<p>Scories provenant de la production primaire et secondaire. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 06 02 </p>
</td>
<td valign="top" width="480">
<p>Crasses et écumes provenant de la production primaire et secondaire. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 06 03* </p>
</td>
<td valign="top" width="480">
<p>Poussières de filtration des fumées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 06 04 </p>
</td>
<td valign="top" width="480">
<p>Autres fines et poussières. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 06 06* </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant de l'épuration des fumées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 06 07* </p>
</td>
<td valign="top" width="480">
<p>Boues et gâteaux de filtration provenant de l'épuration des fumées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 06 09* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des eaux de refroidissement contenant des hydrocarbures. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 06 10 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des eaux de refroidissement autres que ceux visés à la rubrique 10 06 09.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 06 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 07 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la pyrométallurgie de l'argent, de l'or et du platine. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 07 01 </p>
</td>
<td valign="top" width="480">
<p>Scories provenant de la production primaire et secondaire. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 07 02 </p>
</td>
<td valign="top" width="480">
<p>Crasses et écumes provenant de la production primaire et secondaire. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 07 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant de l'épuration des fumées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 07 04 </p>
</td>
<td valign="top" width="480">
<p>Autres fines et poussières. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 07 05 </p>
</td>
<td valign="top" width="480">
<p>Boues et gâteaux de filtration provenant de l'épuration des fumées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 07 07* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des eaux de refroidissement contenant des hydrocarbures. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 07 08 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des eaux de refroidissement autres que ceux visés à la rubrique 10 07 07.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 07 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 08 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la pyrométallurgie d'autres métaux non ferreux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 08 04 </p>
</td>
<td valign="top" width="480">
<p>Fines et poussières. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 08 08* </p>
</td>
<td valign="top" width="480">
<p>Scories salées provenant de la production primaire et secondaire. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 08 09 </p>
</td>
<td valign="top" width="480">
<p>Autres scories. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 08 10* </p>
</td>
<td valign="top" width="480">
<p>Crasses et écumes inflammables ou émettant, au contact de l'eau, des gaz inflammables en quantités dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 08 11 </p>
</td>
<td valign="top" width="480">
<p>Crasses et écumes autres que celles visées à la rubrique 10 08 10.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 08 12* </p>
</td>
<td valign="top" width="480">
<p>Déchets goudronnés provenant de la fabrication des anodes. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 08 13 </p>
</td>
<td valign="top" width="480">
<p>Déchets carbonés provenant de la fabrication des anodes autres que ceux visés à la rubrique 10 08 12.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 08 14 </p>
</td>
<td valign="top" width="480">
<p>Déchets d'anodes. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 08 15* </p>
</td>
<td valign="top" width="480">
<p>Poussières de filtration des fumées contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 08 16 </p>
</td>
<td valign="top" width="480">
<p>Poussières de filtration des fumées autres que celles visées à la rubrique 10 08 15.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 08 17* </p>
</td>
<td valign="top" width="480">
<p>Boues et gâteaux de filtration provenant de l'épuration des fumées contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 08 18 </p>
</td>
<td valign="top" width="480">
<p>Boues et gâteaux de filtration provenant de l'épuration des fumées autres que ceux visés à la rubrique 10 08 17.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 08 19* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des eaux de refroidissement contenant des hydrocarbures. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 08 20 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des eaux de refroidissement autres que ceux visés à la rubrique 10 08 19.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 08 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 09 </p>
</td>
<td valign="top" width="480">
<p>Déchets de fonderie de métaux ferreux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 09 03 </p>
</td>
<td valign="top" width="480">
<p>Laitiers de four de fonderie. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 09 05* </p>
</td>
<td valign="top" width="480">
<p>Noyaux et moules de fonderie n'ayant pas subi la coulée contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 09 06 </p>
</td>
<td valign="top" width="480">
<p>Noyaux et moules de fonderie n'ayant pas subi la coulée autres que ceux visés à la rubrique 10 09 05.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 09 07* </p>
</td>
<td valign="top" width="480">
<p>Noyaux et moules de fonderie ayant subi la coulée contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 09 08 </p>
</td>
<td valign="top" width="480">
<p>Noyaux et moules de fonderie ayant subi la coulée autres que ceux visés à la rubrique 10 09 07.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 09 09* </p>
</td>
<td valign="top" width="480">
<p>Poussières de filtration des fumées contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 09 10 </p>
</td>
<td valign="top" width="480">
<p>Poussières de filtration des fumées autres que celles visées à la rubrique 10 09 09.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 09 11* </p>
</td>
<td valign="top" width="480">
<p>Autres fines contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 09 12 </p>
</td>
<td valign="top" width="480">
<p>Autres fines non visées à la rubrique 10 09 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 09 13* </p>
</td>
<td valign="top" width="480">
<p>Déchets de liants contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 09 14 </p>
</td>
<td valign="top" width="480">
<p>Déchets de liants autres que ceux visés à la rubrique 10 09 13.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 09 15* </p>
</td>
<td valign="top" width="480">
<p>Révélateur de criques usagé contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 09 16 </p>
</td>
<td valign="top" width="480">
<p>Révélateur de criques usagé autre que celui visé à la rubrique 10 09 15.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 09 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 10 </p>
</td>
<td valign="top" width="480">
<p>Déchets de fonderie de métaux non ferreux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 10 03 </p>
</td>
<td valign="top" width="480">
<p>Laitiers de four de fonderie. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 10 05* </p>
</td>
<td valign="top" width="480">
<p>Noyaux et moules de fonderie n'ayant pas subi la coulée contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 10 06 </p>
</td>
<td valign="top" width="480">
<p>Noyaux et moules de fonderie n'ayant pas subi la coulée autres que ceux visés à la rubrique 10 10 05.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 10 07* </p>
</td>
<td valign="top" width="480">
<p>Noyaux et moules de fonderie ayant subi la coulée contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 10 08 </p>
</td>
<td valign="top" width="480">
<p>Noyaux et moules de fonderie ayant subi la coulée autres que ceux visés à la rubrique 10 10 07.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 10 09* </p>
</td>
<td valign="top" width="480">
<p>Poussières de filtration des fumées contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 10 10 </p>
</td>
<td valign="top" width="480">
<p>Poussières de filtration des fumées autres que celles visées à la rubrique 10 10 09.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 10 11* </p>
</td>
<td valign="top" width="480">
<p>Autres fines contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 10 12 </p>
</td>
<td valign="top" width="480">
<p>Autres fines non visées à la rubrique 10 10 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 10 13* </p>
</td>
<td valign="top" width="480">
<p>Déchets de liants contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 10 14 </p>
</td>
<td valign="top" width="480">
<p>Déchets de liants autres que ceux visés à la rubrique 10 10 13.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 10 15* </p>
</td>
<td valign="top" width="480">
<p>Révélateur de criques usagé contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 10 16 </p>
</td>
<td valign="top" width="480">
<p>Révélateur de criques usagé autre que celui visé à la rubrique 10 10 15.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 10 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 11 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la fabrication du verre et des produits verriers. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 11 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets de matériaux à base de fibre de verre. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 11 05 </p>
</td>
<td valign="top" width="480">
<p>Fines et poussières. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 11 09* </p>
</td>
<td valign="top" width="480">
<p>Déchets de préparation avant cuisson contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 11 10 </p>
</td>
<td valign="top" width="480">
<p>Déchets de préparation avant cuisson autres que ceux visés à la rubrique 10 11 09.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 11 11* </p>
</td>
<td valign="top" width="480">
<p>Petites particules de déchets de verre et poudre de verre contenant des métaux lourds (par exemple : tubes cathodiques). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 11 12 </p>
</td>
<td valign="top" width="480">
<p>Déchets de verre autres que ceux visés à la rubrique 10 11 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 11 13* </p>
</td>
<td valign="top" width="480">
<p>Boues de polissage et de meulage du verre contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 11 14 </p>
</td>
<td valign="top" width="480">
<p>Boues de polissage et de meulage du verre autres que celles visées à la rubrique 10 11 13.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 11 15* </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant de l'épuration des fumées contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 11 16 </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant de l'épuration des fumées autres que ceux visés à la rubrique 10 11 15.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 11 17* </p>
</td>
<td valign="top" width="480">
<p>Boues et gâteaux de filtration provenant de l'épuration des fumées contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 11 18 </p>
</td>
<td valign="top" width="480">
<p>Boues et gâteaux de filtration provenant de l'épuration des fumées autres que ceux visés à la rubrique 10 11 17.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 11 19* </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant du traitement in situ des effluents contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 11 20 </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant du traitement in situ des effluents autres que ceux visés à la rubrique 10 11 19.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 11 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 12 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la fabrication des produits en céramique, briques, carrelage et matériaux de construction. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 12 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets de préparation avant cuisson. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 12 03 </p>
</td>
<td valign="top" width="480">
<p>Fines et poussières. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 12 05 </p>
</td>
<td valign="top" width="480">
<p>Boues et gâteaux de filtration provenant de l'épuration des fumées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 12 06 </p>
</td>
<td valign="top" width="480">
<p>Moules déclassés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 12 08 </p>
</td>
<td valign="top" width="480">
<p>Déchets de produits en céramique, briques, carrelage et matériaux de construction (après cuisson). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 12 09* </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant de l'épuration des fumées contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 12 10 </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant de l'épuration des fumées autres que ceux visés à la rubrique 10 12 09.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 12 11* </p>
</td>
<td valign="top" width="480">
<p>Déchets d'émaillage contenant des métaux lourds. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 12 12 </p>
</td>
<td valign="top" width="480">
<p>Déchets d'émaillage autres que ceux visés à la rubrique 10 12 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 12 13 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 12 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 13 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la fabrication de ciment, chaux et plâtre et d'articles et produits dérivés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 13 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets de préparation avant cuisson. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 13 04 </p>
</td>
<td valign="top" width="480">
<p>Déchets de calcination et d'hydratation de la chaux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 13 06 </p>
</td>
<td valign="top" width="480">
<p>Fines et poussières (sauf rubriques 10 13 12 et 10 13 13). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 13 07 </p>
</td>
<td valign="top" width="480">
<p>Boues et gâteaux de filtration de provenant de l'épuration des fumées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 13 09* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la fabrication d'amiante-ciment contenant de l'amiante. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 13 10 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la fabrication d'amiante-ciment autres que ceux visés à la rubrique 10 13 09.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 13 11 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la fabrication de matériaux composites à base de ciment autres que ceux visés aux rubriques 10 13 09 et 10 13 10.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 13 12* </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant de l'épuration des fumées contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 13 13 </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant de l'épuration des fumées autres que ceux visés à la rubrique 10 13 12.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 13 14 </p>
</td>
<td valign="top" width="480">
<p>Déchets et boues de béton. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 13 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 14 </p>
</td>
<td valign="top" width="480">
<p>Déchets de crématoires. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>10 14 01* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des fumées contenant du mercure. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 </p>
</td>
<td valign="top" width="480">
<p align="center">DÉCHETS PROVENANT DU TRAITEMENT CHIMIQUE DE SURFACE ET DU REVÊTEMENT DES MÉTAUX ET AUTRES MATÉRIAUX, ET DE L'HYDROMÉTALLURGIE DES MÉTAUX NON FERREUX </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du traitement chimique de surface et du revêtement des métaux et autres matériaux (par exemple : procédés de galvanisation, de revêtement de zinc, de décapage, de gravure, de phosphatation, de dégraissage alcalin et d'anodisation.) </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 01 05* </p>
</td>
<td valign="top" width="480">
<p>Acides de décapage. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 01 06* </p>
</td>
<td valign="top" width="480">
<p>Acides non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 01 07* </p>
</td>
<td valign="top" width="480">
<p>Bases de décapage. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 01 08* </p>
</td>
<td valign="top" width="480">
<p>Boues de phosphatation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 01 09* </p>
</td>
<td valign="top" width="480">
<p>Boues et gâteaux de filtration contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 01 10 </p>
</td>
<td valign="top" width="480">
<p>Boues et gâteaux de filtration autres que ceux visés à la rubrique 11 01 09.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 01 11* </p>
</td>
<td valign="top" width="480">
<p>Liquides aqueux de rinçage contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 01 12 </p>
</td>
<td valign="top" width="480">
<p>Liquides aqueux de rinçage autres que ceux visés à la rubrique 11 01 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 01 13* </p>
</td>
<td valign="top" width="480">
<p>Déchets de dégraissage contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 01 14 </p>
</td>
<td valign="top" width="480">
<p>Déchets de dégraissage autres que ceux visés à la rubrique 11 01 13.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 01 15* </p>
</td>
<td valign="top" width="480">
<p>Eluats et boues provenant des systèmes à membrane et des systèmes d'échange d'ions contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 01 16* </p>
</td>
<td valign="top" width="480">
<p>Résines échangeuses d'ions saturées ou usées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 01 98* </p>
</td>
<td valign="top" width="480">
<p>Autres déchets contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 01 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant des procédés hydrométallurgiques des métaux non ferreux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 02 02* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant de l'hydrométallurgie de zinc (y compris jarosite et goethite). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 02 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la production d'anodes pour les procédés d'électrolyse aqueuse. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 02 05* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant des procédés hydrométallurgiques du cuivre contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 02 06 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant des procédés hydrométallurgiques du cuivre autres que ceux visés à la la rubrique 11 02 05.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 02 07* </p>
</td>
<td valign="top" width="480">
<p>Autres déchets contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 02 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 03 </p>
</td>
<td valign="top" width="480">
<p>Boues et solides provenant de la trempe. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 03 01* </p>
</td>
<td valign="top" width="480">
<p>Déchets cyanurés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 03 02* </p>
</td>
<td valign="top" width="480">
<p>Autres déchets. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 05 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la galvanisation à chaud. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 05 01 </p>
</td>
<td valign="top" width="480">
<p>Mattes. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 05 02 </p>
</td>
<td valign="top" width="480">
<p>Cendres de zinc. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 05 03* </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant de l'épuration des fumées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 05 04* </p>
</td>
<td valign="top" width="480">
<p>Flux utilisé. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>11 05 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 </p>
</td>
<td valign="top" width="480">
<p align="center">DÉCHETS PROVENANT DE LA MISE EN FORME ET DU TRAITEMENT PHYSIQUE ET MÉCANIQUE DE SURFACE DES MÉTAUX ET MATIÈRES PLASTIQUES </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la mise en forme et du traitement mécanique et physique de surface des métaux et matières plastiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 01 </p>
</td>
<td valign="top" width="480">
<p>Limaille et chutes de métaux ferreux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 02 </p>
</td>
<td valign="top" width="480">
<p>Fines et poussières de métaux ferreux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 03 </p>
</td>
<td valign="top" width="480">
<p>Limaille et chutes de métaux non ferreux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 04 </p>
</td>
<td valign="top" width="480">
<p>Fines et poussières de métaux non ferreux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 05 </p>
</td>
<td valign="top" width="480">
<p>Déchets de matières plastiques d'ébarbage et de tournage. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 06* </p>
</td>
<td valign="top" width="480">
<p>Huiles d'usinage à base minérale contenant des halogènes (pas sous forme d'émulsions ou de solutions). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 07* </p>
</td>
<td valign="top" width="480">
<p>Huiles d'usinage à base minérale sans halogènes (pas sous forme d'émulsions ou de solutions). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 08* </p>
</td>
<td valign="top" width="480">
<p>Emulsions et solutions d'usinage contenant des halogènes. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 09* </p>
</td>
<td valign="top" width="480">
<p>Emulsions et solutions d'usinage sans halogènes. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 10* </p>
</td>
<td valign="top" width="480">
<p>Huiles d'usinage de synthèse. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 12* </p>
</td>
<td valign="top" width="480">
<p>Déchets de cires et graisses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 13 </p>
</td>
<td valign="top" width="480">
<p>Déchets de soudure. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 14* </p>
</td>
<td valign="top" width="480">
<p>Boues d'usinage contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 15 </p>
</td>
<td valign="top" width="480">
<p>Boues d'usinage autres que celles visées à la rubrique 12 01 14.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 16* </p>
</td>
<td valign="top" width="480">
<p>Déchets de grenaillage contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 17 </p>
</td>
<td valign="top" width="480">
<p>Déchets de grenaillage autres que ceux visés à la rubrique 12 01 16.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 18* </p>
</td>
<td valign="top" width="480">
<p>Boues métalliques (provenant du meulage et de l'affûtage) contenant des hydrocarbures. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 19* </p>
</td>
<td valign="top" width="480">
<p>Huiles d'usinage facilement biodégradables. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 20* </p>
</td>
<td valign="top" width="480">
<p>Déchets de meulage et matériaux de meulage contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 21 </p>
</td>
<td valign="top" width="480">
<p>Déchets de meulage et matériaux de meulage autres que ceux visés à la rubrique 12 01 20.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 01 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du dégraissage à l'eau et à la vapeur (sauf chapitre 11). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 03 01* </p>
</td>
<td valign="top" width="480">
<p>Liquides aqueux de nettoyage. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>12 03 02* </p>
</td>
<td valign="top" width="480">
<p>Déchets du dégraissage à la vapeur. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 </p>
</td>
<td valign="top" width="480">
<p align="center">HUILES ET COMBUSTIBLES LIQUIDES USAGÉS (SAUF HUILES ALIMENTAIRES ET HUILES FIGURANT AUX CHAPITRES 05,12 ET 19) </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 01 </p>
</td>
<td valign="top" width="480">
<p>Huiles hydrauliques usagées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 01 01* </p>
</td>
<td valign="top" width="480">
<p>Huiles hydrauliques contenant des PCB (1). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 01 04* </p>
</td>
<td valign="top" width="480">
<p>Autres huiles hydrauliques chlorées (émulsions). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 01 05* </p>
</td>
<td valign="top" width="480">
<p>Huiles hydrauliques non chlorées (émulsions). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 01 09* </p>
</td>
<td valign="top" width="480">
<p>Huiles hydrauliques chlorées à base minérale. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 01 10* </p>
</td>
<td valign="top" width="480">
<p>Huiles hydrauliques non chlorées à base minérale. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 01 11* </p>
</td>
<td valign="top" width="480">
<p>Huiles hydrauliques synthétiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 01 12* </p>
</td>
<td valign="top" width="480">
<p>Huiles hydrauliques facilement biodégradables. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 01 13* </p>
</td>
<td valign="top" width="480">
<p>Autres huiles hydrauliques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 02 </p>
</td>
<td valign="top" width="480">
<p>Huiles moteur, de boîte de vitesses et de lubrification usagées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 02 04* </p>
</td>
<td valign="top" width="480">
<p>Huiles moteur, de boîte de vitesses et de lubrification chlorées à base minérale. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 02 05* </p>
</td>
<td valign="top" width="480">
<p>Huiles moteur, de boîte de vitesses et de lubrification non chlorées à base minérale. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 02 06* </p>
</td>
<td valign="top" width="480">
<p>Huiles moteur, de boîte de vitesses et de lubrification synthétiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 02 07* </p>
</td>
<td valign="top" width="480">
<p>Huiles moteur, de boîte de vitesses et de lubrification facilement biodègradables. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 02 08* </p>
</td>
<td valign="top" width="480">
<p>Autres huiles moteur, de boîte de vitesses et de lubrification </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 03 </p>
</td>
<td valign="top" width="480">
<p>Huiles isolantes et fluides caloporteurs usagés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 03 01* </p>
</td>
<td valign="top" width="480">
<p>Huiles isolantes et fluides caloporteurs contenant des PCB. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 03 06* </p>
</td>
<td valign="top" width="480">
<p>Huiles isolantes et fluides caloporteurs chlorés à base minérale autre que ceux visés à la rubrique 13 03 01.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 03 07* </p>
</td>
<td valign="top" width="480">
<p>Huiles isolantes et fluides caloporteurs non chlorés à base minérale. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 03 08* </p>
</td>
<td valign="top" width="480">
<p>Huiles isolantes et fluides caloporteurs synthétiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 03 09* </p>
</td>
<td valign="top" width="480">
<p>Huiles isolantes et fluides caloporteurs facilement biodégradables. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 03 10* </p>
</td>
<td valign="top" width="480">
<p>Autres huiles isolantes et fluides caloporteurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 04 </p>
</td>
<td valign="top" width="480">
<p>Hydrocarbures de fond de cale. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 04 01* </p>
</td>
<td valign="top" width="480">
<p>Hydrocarbures de fond de cale provenant de la navigation fluviale. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 04 02* </p>
</td>
<td valign="top" width="480">
<p>Hydrocarbures de fond de cale provenant de canalisations de môles. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 04 03* </p>
</td>
<td valign="top" width="480">
<p>Hydrocarbures de fond de cale provenant d'un autre type de navigation </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 05 </p>
</td>
<td valign="top" width="480">
<p>Contenu de séparateur eau/ hydrocarbures. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 05 01* </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant de dessableurs et de séparateurs eau/ hydrocarbures. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 05 02* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant de séparateurs eau/ hydrocarbures. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 05 03* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant de déshuileurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 05 06* </p>
</td>
<td valign="top" width="480">
<p>Hydrocarbures provenant de séparateurs eau/ hydrocarbures. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 05 07* </p>
</td>
<td valign="top" width="480">
<p>Eau mélangée à des hydrocarbures provenant de séparateurs eau/ hydrocarbures. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 05 08* </p>
</td>
<td valign="top" width="480">
<p>Mélanges de déchets provenant de dessableurs et de séparateurs </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 07 </p>
</td>
<td valign="top" width="480">
<p>Combustibles liquides usagés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 07 01* </p>
</td>
<td valign="top" width="480">
<p>Fioul et gazole. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 07 02* </p>
</td>
<td valign="top" width="480">
<p>Essence. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 07 03* </p>
</td>
<td valign="top" width="480">
<p>Autres combustibles (y compris mélanges). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 08 </p>
</td>
<td valign="top" width="480">
<p>Huiles usagées non spécifiées ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 08 01* </p>
</td>
<td valign="top" width="480">
<p>Boues ou émulsions de dessalage. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 08 02* </p>
</td>
<td valign="top" width="480">
<p>Autres émulsions. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>13 08 99* </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>14 </p>
</td>
<td valign="top" width="480">
<p align="center">DÉCHETS DE SOLVANTS ORGANIQUES, D'AGENTS RÉFRIGÉRANTS ET PROPULSEURS (SAUF CHAPITRES 07 ET 08) </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>14 06 </p>
</td>
<td valign="top" width="480">
<p>Déchets de solvants, d'agents réfrigérants et d'agents propulseurs d'aérosols/ de mousses organiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>14 06 01* </p>
</td>
<td valign="top" width="480">
<p>Chlorofluorocarbones, HCFC, HFC. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>14 06 02* </p>
</td>
<td valign="top" width="480">
<p>Autres solvants et mélanges de solvants halogènes. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>14 06 03* </p>
</td>
<td valign="top" width="480">
<p>Autres solvants et mélanges de solvants. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>14 06 04* </p>
</td>
<td valign="top" width="480">
<p>Boues ou déchets solides contenant des solvants halogènes. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>14 06 05* </p>
</td>
<td valign="top" width="480">
<p>Boues ou déchets solides contenant d'autres solvants. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>15 </p>
</td>
<td valign="top" width="480">
<p align="center">EMBALLAGES ET DÉCHETS D'EMBALLAGES, ABSORBANTS, CHIFFONS D'ESSUYAGE, MATÉRIAUX FILTRANTS ET VÊTEMENTS DE PROTECTION NON SPÉCIFIÉS AILLEURS </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>15 01 </p>
</td>
<td valign="top" width="480">
<p>Emballages et déchets d'emballages (y compris les déchets d'emballages municipaux collectés séparément). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>15 01 01 </p>
</td>
<td valign="top" width="480">
<p>Emballages en papier/ carton. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>15 01 02 </p>
</td>
<td valign="top" width="480">
<p>Emballages en matières plastiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>15 01 03 </p>
</td>
<td valign="top" width="480">
<p>Emballages en bois. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>15 01 04 </p>
</td>
<td valign="top" width="480">
<p>Emballages métalliques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>15 01 05 </p>
</td>
<td valign="top" width="480">
<p>Emballages composites. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>15 01 06 </p>
</td>
<td valign="top" width="480">
<p>Emballages en mélange. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>15 01 07 </p>
</td>
<td valign="top" width="480">
<p>Emballages en verre. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>15 01 09 </p>
</td>
<td valign="top" width="480">
<p>Emballages textiles. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>15 01 10* </p>
</td>
<td valign="top" width="480">
<p>Emballages contenant des résidus de substances dangereuses ou contaminés par de tels résidus. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>15 01 11* </p>
</td>
<td valign="top" width="480">
<p>Emballages métalliques contenant une matrice poreuse solide dangereuse (par exemple amiante), y compris des conteneurs à pression vides. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>15 02 </p>
</td>
<td valign="top" width="480">
<p>Absorbants, matériaux filtrants, chiffons d'essuyage et vêtements de protection. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>15 02 02* </p>
</td>
<td valign="top" width="480">
<p>Absorbants, matériaux filtrants (y compris les filtres à huile non spécifiés ailleurs), chiffons d'essuyage et vêtements de protection contaminés par des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>15 02 03 </p>
</td>
<td valign="top" width="480">
<p>Absorbants, matériaux filtrants, chiffons d'essuyage et vêtements de protection autres que ceux visés à la rubrique 15 02 02.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 </p>
</td>
<td valign="top" width="480">
<p align="center">DÉCHETS NON DÉCRITS AILLEURS DANS LA LISTE </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 </p>
</td>
<td valign="top" width="480">
<p>Véhicules hors d'usage de différents moyens de transport (y compris machines tout-terrain) et déchets provenant du démontage de véhicules hors d'usage et de l'entretien de véhicules (sauf chapitres 13,14, et sections 16 06 et 16 08). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 03 </p>
</td>
<td valign="top" width="480">
<p>Pneus hors d'usage. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 04* </p>
</td>
<td valign="top" width="480">
<p>Véhicules hors d'usage. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 06 </p>
</td>
<td valign="top" width="480">
<p>Véhicules hors d'usage ne contenant ni liquides ni autres composants dangereux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 07* </p>
</td>
<td valign="top" width="480">
<p>Filtres à huile </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 08* </p>
</td>
<td valign="top" width="480">
<p>Composants contenant du mercure. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 09* </p>
</td>
<td valign="top" width="480">
<p>Composants contenant des PCB. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 10* </p>
</td>
<td valign="top" width="480">
<p>Composants explosifs (par exemple : coussins gonflables de sécurité). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 11* </p>
</td>
<td valign="top" width="480">
<p>Patins de freins contenant de l'amiante. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 12 </p>
</td>
<td valign="top" width="480">
<p>Patins de freins autres que ceux visés à la rubrique 16 01 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 13* </p>
</td>
<td valign="top" width="480">
<p>Liquides de frein. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 14* </p>
</td>
<td valign="top" width="480">
<p>Antigels contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 15 </p>
</td>
<td valign="top" width="480">
<p>Antigels autres que ceux visés à la rubrique 16 01 14.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 16 </p>
</td>
<td valign="top" width="480">
<p>Réservoirs de gaz liquéfié. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 17 </p>
</td>
<td valign="top" width="480">
<p>Métaux ferreux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 18 </p>
</td>
<td valign="top" width="480">
<p>Métaux non ferreux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 19 </p>
</td>
<td valign="top" width="480">
<p>Matières plastiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 20 </p>
</td>
<td valign="top" width="480">
<p>Verre. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 21* </p>
</td>
<td valign="top" width="480">
<p>Composants dangereux autres que ceux visés aux rubriques 16 01 07 à 16 01 11,16 01 13 et 16 01 14.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 22 </p>
</td>
<td valign="top" width="480">
<p>Composants non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 01 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant d'équipements électriques ou électroniques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 02 09* </p>
</td>
<td valign="top" width="480">
<p>Transformateurs et accumulateurs contenant des PCB. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 02 10* </p>
</td>
<td valign="top" width="480">
<p>Equipements mis au rebut contenant des PCB ou contaminés par de telles substances autres que ceux visés à la rubrique 16 02 09.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 02 11* </p>
</td>
<td valign="top" width="480">
<p>Equipements mis au rebut contenant des chlorofluorocarbones, des HCFC ou des HFC. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 02 12* </p>
</td>
<td valign="top" width="480">
<p>Equipements mis au rebut contenant de l'amiante libre. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 02 13* </p>
</td>
<td valign="top" width="480">
<p>Equipements mis au rebut contenant des composants dangereux (2) autres que ceux visés aux rubriques 16 02 09 à 16 02 12.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 02 14 </p>
</td>
<td valign="top" width="480">
<p>Equipements mis au rebut autres que ceux visés aux rubriques 16 02 09 à 16 02 13.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 02 15* </p>
</td>
<td valign="top" width="480">
<p>Composants dangereux retirés des équipements mis au rebut. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 02 16 </p>
</td>
<td valign="top" width="480">
<p>Composants retirés des équipements mis au rebut autres que ceux visés à la rubrique 16 02 15.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 03 </p>
</td>
<td valign="top" width="480">
<p>Loupés de fabrication et produits non utilisés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 03 03* </p>
</td>
<td valign="top" width="480">
<p>Déchets d'origine minérale contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 03 04 </p>
</td>
<td valign="top" width="480">
<p>Déchets d'origine minérale autres que ceux visés à la rubrique 16 03 03.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 03 05* </p>
</td>
<td valign="top" width="480">
<p>Déchets d'origine organique contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 03 06 </p>
</td>
<td valign="top" width="480">
<p>Déchets d'origine organique autres que ceux visés à la rubrique 16 03 05.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 04 </p>
</td>
<td valign="top" width="480">
<p>Déchets d'explosifs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 04 01* </p>
</td>
<td valign="top" width="480">
<p>Déchets de munitions. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 04 02* </p>
</td>
<td valign="top" width="480">
<p>Déchets de feux d'artifice. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 04 03* </p>
</td>
<td valign="top" width="480">
<p>Autres déchets d'explosifs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 05 </p>
</td>
<td valign="top" width="480">
<p>Gaz en récipients à pression et produits chimiques mis au rebut. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 05 04* </p>
</td>
<td valign="top" width="480">
<p>Gaz en récipients à pression (compris les halons) contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 05 05 </p>
</td>
<td valign="top" width="480">
<p>Gaz en récipients à pression autres que ceux visés à la rubrique 16 05 04.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 05 06* </p>
</td>
<td valign="top" width="480">
<p>Produits chimiques de laboratoire à base de ou contenant des substances dangereuses, y compris les mélanges de produits chimiques de laboratoire. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 05 07* </p>
</td>
<td valign="top" width="480">
<p>Produits chimiques d'origine minérale à base de ou contenant des substances dangereuses, mis au rebut. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 05 08* </p>
</td>
<td valign="top" width="480">
<p>Produits chimiques d'origine organique à base de ou contenant des substances dangereuses, mis au rebut. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 05 09 </p>
</td>
<td valign="top" width="480">
<p>Produits chimiques mis au rebut autres que ceux visés aux rubriques 16 05 06,16 05 07 ou 16 05 08.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 06 </p>
</td>
<td valign="top" width="480">
<p>Piles et accumulateurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 06 01* </p>
</td>
<td valign="top" width="480">
<p>Accumulateurs au plomb. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 06 02* </p>
</td>
<td valign="top" width="480">
<p>Accumulateurs Ni-Cd. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 06 03* </p>
</td>
<td valign="top" width="480">
<p>Piles contenant du mercure. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 06 04 </p>
</td>
<td valign="top" width="480">
<p>Piles alcalines (sauf rubrique 16 06 03). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 06 05 </p>
</td>
<td valign="top" width="480">
<p>Autres piles et accumulateurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 06 06* </p>
</td>
<td valign="top" width="480">
<p>Electrolytes de piles et accumulateurs collectés séparément </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 07 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du nettoyage de cuves et fûts de stockage et de transport (sauf chapitres 05 et 13). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 07 08* </p>
</td>
<td valign="top" width="480">
<p>Déchets contenant des hydrocarbures. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 07 09* </p>
</td>
<td valign="top" width="480">
<p>Déchets contenant d'autres substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 07 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 08 </p>
</td>
<td valign="top" width="480">
<p>Catalyseurs usés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 08 01 </p>
</td>
<td valign="top" width="480">
<p>Catalyseurs usés contenant de l'or, de l'argent, du rhénium, du rhodium, du palladium, de l'iridium ou du platine (sauf rubrique 16 08 07). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 08 02* </p>
</td>
<td valign="top" width="480">
<p>Catalyseurs usés contenant des métaux ou composés de métaux de transition (3) dangereux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 08 03 </p>
</td>
<td valign="top" width="480">
<p>Catalyseurs usés contenant des métaux ou composés de métaux de transition non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 08 04 </p>
</td>
<td valign="top" width="480">
<p>Catalyseurs usés de craquage catalytique sur lit fluide (sauf rubrique 16 08 07). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 08 05* </p>
</td>
<td valign="top" width="480">
<p>Catalyseurs usés contenant de l'acide phosphorique. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 08 06* </p>
</td>
<td valign="top" width="480">
<p>Liquides usés employés comme catalyseurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 08 07* </p>
</td>
<td valign="top" width="480">
<p>Catalyseurs usés contaminés par des substances dangereuses </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 09 </p>
</td>
<td valign="top" width="480">
<p>Substances oxydantes. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 09 01* </p>
</td>
<td valign="top" width="480">
<p>Permanganates (par exemple : permanganate de potassium). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 09 02* </p>
</td>
<td valign="top" width="480">
<p>Chromates (par exemple : chromate de potassium, dichromate de sodium ou de potassium). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 09 03* </p>
</td>
<td valign="top" width="480">
<p>Peroxydes (par exemple : peroxyde d'hydrogène). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 09 04* </p>
</td>
<td valign="top" width="480">
<p>Substances oxydantes non spécifiées ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 10 </p>
</td>
<td valign="top" width="480">
<p>Déchets liquides aqueux destinés à un traitement hors site. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 10 01* </p>
</td>
<td valign="top" width="480">
<p>Déchets liquides aqueux contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 10 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets liquides aqueux autres que ceux visés à la rubrique 16 10 01.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 10 03* </p>
</td>
<td valign="top" width="480">
<p>Concentrés aqueux contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 10 04 </p>
</td>
<td valign="top" width="480">
<p>Concentrés aqueux autres que ceux visés à la rubrique 16 10 03.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 11 </p>
</td>
<td valign="top" width="480">
<p>Déchets de revêtements de fours et réfractaires. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 11 01* </p>
</td>
<td valign="top" width="480">
<p>Revêtements de fours et réfractaires à base de carbone provenant de procédés métallurgiques contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 11 02 </p>
</td>
<td valign="top" width="480">
<p>Revêtements de fours et réfractaires à base de carbone provenant de procédés métallurgiques autres que ceux visés à la rubrique 16 11 01.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 11 03* </p>
</td>
<td valign="top" width="480">
<p>Autres revêtements de fours et réfractaires provenant de procédés métallurgiques contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 11 04 </p>
</td>
<td valign="top" width="480">
<p>Autres revêtements de fours et réfractaires provenant de procédés métallurgiques non visés à la rubrique 16 11 03.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 11 05* </p>
</td>
<td valign="top" width="480">
<p>Revêtements de fours et réfractaires provenant de procédés non métallurgiques contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>16 11 06 </p>
</td>
<td valign="top" width="480">
<p>Revêtements de fours et réfractaires provenant de procédés non métallurgiques autres que ceux visés à la rubrique 16 11 05.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 </p>
</td>
<td valign="top" width="480">
<p align="center">DÉCHETS DE CONSTRUCTION ET DE DÉMOLITION (Y COMPRIS DÉBLAIS PROVENANT DE SITES CONTAMINÉS) </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 01 </p>
</td>
<td valign="top" width="480">
<p>Béton, briques, tuiles et céramiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 01 01 </p>
</td>
<td valign="top" width="480">
<p>Béton. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 01 02 </p>
</td>
<td valign="top" width="480">
<p>Briques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 01 03 </p>
</td>
<td valign="top" width="480">
<p>Tuiles et céramiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 01 06* </p>
</td>
<td valign="top" width="480">
<p>Mélanges ou fractions séparées de béton, briques, tuiles et céramiques contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 01 07 </p>
</td>
<td valign="top" width="480">
<p>Mélanges de béton, briques, tuiles et céramiques autres que ceux visés à la rubrique 17 01 06.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 02 </p>
</td>
<td valign="top" width="480">
<p>Bois, verre et matières plastiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 02 01 </p>
</td>
<td valign="top" width="480">
<p>Bois. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 02 02 </p>
</td>
<td valign="top" width="480">
<p>Verre. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 02 03 </p>
</td>
<td valign="top" width="480">
<p>Matières plastiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 02 04* </p>
</td>
<td valign="top" width="480">
<p>Bois, verre et matières plastiques contenant des substances dangereuses ou contaminés par de telles substances </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 03 </p>
</td>
<td valign="top" width="480">
<p>Mélanges bitumineux, goudron et produits goudronnés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 03 01* </p>
</td>
<td valign="top" width="480">
<p>Mélanges bitumineux contenant du goudron. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 03 02 </p>
</td>
<td valign="top" width="480">
<p>Mélanges bitumineux autres que ceux visés à la rubrique 17 03 01.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 03 03* </p>
</td>
<td valign="top" width="480">
<p>Goudron et produits goudronnés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 04 </p>
</td>
<td valign="top" width="480">
<p>Métaux (y compris leurs alliages). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 04 01 </p>
</td>
<td valign="top" width="480">
<p>Cuivre, bronze, laiton. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 04 02 </p>
</td>
<td valign="top" width="480">
<p>Aluminium. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 04 03 </p>
</td>
<td valign="top" width="480">
<p>Plomb. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 04 04 </p>
</td>
<td valign="top" width="480">
<p>Zinc. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 04 05 </p>
</td>
<td valign="top" width="480">
<p>Fer et acier. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 04 06 </p>
</td>
<td valign="top" width="480">
<p>Etain. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 04 07 </p>
</td>
<td valign="top" width="480">
<p>Métaux en mélange. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 04 09* </p>
</td>
<td valign="top" width="480">
<p>Déchets métalliques contaminés par des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 04 10* </p>
</td>
<td valign="top" width="480">
<p>Câbles contenant des hydrocarbures, du goudron ou d'autres substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 04 11 </p>
</td>
<td valign="top" width="480">
<p>Câbles autres que ceux visés à la rubrique 17 04 10.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 05 </p>
</td>
<td valign="top" width="480">
<p>Terres (y compris déblais provenant de sites contaminés), cailloux et boues de dragage. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 05 03* </p>
</td>
<td valign="top" width="480">
<p>Terres et cailloux contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 05 04 </p>
</td>
<td valign="top" width="480">
<p>Terres et cailloux autres que ceux visés à la rubrique 17 05 03.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 05 05* </p>
</td>
<td valign="top" width="480">
<p>Boues de dragage contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 05 06 </p>
</td>
<td valign="top" width="480">
<p>Boues de dragage autres que celles visées à la rubrique 17 05 05.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 05 07* </p>
</td>
<td valign="top" width="480">
<p>Ballast de voie contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 05 08 </p>
</td>
<td valign="top" width="480">
<p>Ballast de voie autre que celui visé à la rubrique 17 05 07.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 06 </p>
</td>
<td valign="top" width="480">
<p>Matériaux d'isolation et matériaux de construction contenant de l'amiante. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 06 01* </p>
</td>
<td valign="top" width="480">
<p>Matériaux d'isolation contenant de l'amiante. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 06 03* </p>
</td>
<td valign="top" width="480">
<p>Autres matériaux d'isolation à base de ou contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 06 04 </p>
</td>
<td valign="top" width="480">
<p>Matériaux d'isolation autres que ceux visés aux rubriques 17 06 01 et 17 06 03.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 06 05* </p>
</td>
<td valign="top" width="480">
<p>Matériaux de construction contenant de l'amiante. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 08 </p>
</td>
<td valign="top" width="480">
<p>Matériaux de construction à base de gypse. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 08 01* </p>
</td>
<td valign="top" width="480">
<p>Matériaux de construction à base de gypse contaminés par des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 08 02 </p>
</td>
<td valign="top" width="480">
<p>Matériaux de construction à base de gypse autres que ceux visés à la rubrique 17 08 01.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 09 </p>
</td>
<td valign="top" width="480">
<p>Autres déchets de construction et de démolition. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 09 01* </p>
</td>
<td valign="top" width="480">
<p>Déchets de construction et de démolition contenant du mercure. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 09 02* </p>
</td>
<td valign="top" width="480">
<p>Déchets de construction et de démolition contenant des PCB (par exemple : mastics, sols à base de résines, double vitrage, condensateurs contenant des PCB). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 09 03* </p>
</td>
<td valign="top" width="480">
<p>Autres déchets de construction et de démolition (y compris en mélange) contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>17 09 04 </p>
</td>
<td valign="top" width="480">
<p>Déchets de construction et de démolition en mélange autres que ceux visés aux rubriques 17 09 01,17 09 02 et 17 09 03.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 </p>
</td>
<td valign="top" width="480">
<p align="center">DÉCHETS PROVENANT DES SOINS MÉDICAUX OU VÉTÉRINAIRES ET/ OU DE LA RECHERCHE ASSOCIÉE (SAUF DÉCHETS DE CUISINE ET DE RESTAURATION NE PROVENANT PAS DIRECTEMENT DES SOINS MÉDICAUX) </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant des maternités, du diagnostic, du traitement ou de la prévention des maladies de l'homme. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 01 01 </p>
</td>
<td valign="top" width="480">
<p>Objets piquants et coupants (sauf rubrique 18 01 03). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 01 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets anatomiques et organes, y compris sacs de sang et réserves de sang (sauf rubrique 18 10 03). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 01 03* </p>
</td>
<td valign="top" width="480">
<p>Déchets dont la collecte et l'élimination font l'objet de prescriptions particulières vis-à-vis des risques d'infection. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 01 04 </p>
</td>
<td valign="top" width="480">
<p>Déchets dont la collecte et l'élimination ne font pas l'objet de prescriptions particulières vis-à-vis des risques d'infection (par exemple : vêtements, plâtres, draps, vêtements jetables, langes). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 01 06* </p>
</td>
<td valign="top" width="480">
<p>Produits chimiques à base de ou contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 01 07 </p>
</td>
<td valign="top" width="480">
<p>Produits chimiques autres que ceux visés à la rubrique 18 01 06.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 01 08* </p>
</td>
<td valign="top" width="480">
<p>Médicaments cytotoxiques et cytostatiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 01 09 </p>
</td>
<td valign="top" width="480">
<p>Médicaments autres que ceux visés à la rubrique 18 01 08.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 01 10* </p>
</td>
<td valign="top" width="480">
<p>Déchets d'amalgame dentaire. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la recherche, du diagnostic, du traitement ou de la prévention des maladies des animaux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 02 01 </p>
</td>
<td valign="top" width="480">
<p>Objets piquants et coupants (sauf rubrique 18 02 02). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 02 02* </p>
</td>
<td valign="top" width="480">
<p>Déchets dont la collecte et l'élimination font l'objet de prescriptions particulières vis-à-vis des risques d'infection. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 02 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets dont la collecte et l'élimination ne font pas l'objet de prescriptions particulières vis-à-vis des risques d'infection. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 02 05* </p>
</td>
<td valign="top" width="480">
<p>Produits chimiques à base de ou contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 02 06 </p>
</td>
<td valign="top" width="480">
<p>Produits chimiques autres que ceux visés à la rubrique 18 02 05.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 02 07* </p>
</td>
<td valign="top" width="480">
<p>Médicaments cytotoxiques et cytostatiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>18 02 08 </p>
</td>
<td valign="top" width="480">
<p>Médicaments autres que ceux visés à la rubrique 18 02 07.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 </p>
</td>
<td valign="top" width="480">
<p align="center">DÉCHETS PROVENANT DES INSTALLATIONS DE GESTION DES DÉCHETS, DES STATIONS D'ÉPURATION DES EAUX USÉES HORS SITE ET DE LA PRÉPARATION D'EAU DESTINÉE À LA CONSOMMATION HUMAINE ET D'EAU À USAGE INDUSTRIEL </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets de l'incinération ou de la pyrolyse de déchets. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 01 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets de déferraillage des mâchefers. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 01 05* </p>
</td>
<td valign="top" width="480">
<p>Gâteau de filtration provenant de l'épuration des fumées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 01 06* </p>
</td>
<td valign="top" width="480">
<p>Déchets liquides aqueux de l'épuration des fumées et autres déchets liquides aqueux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 01 07* </p>
</td>
<td valign="top" width="480">
<p>Déchets secs de l'épuration des fumées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 01 10* </p>
</td>
<td valign="top" width="480">
<p>Charbon actif usé provenant de l'épuration des gaz de fumées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 01 11* </p>
</td>
<td valign="top" width="480">
<p>Mâchefers contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 01 12 </p>
</td>
<td valign="top" width="480">
<p>Mâchefers autres que ceux visés à la rubrique 19 01 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 01 13* </p>
</td>
<td valign="top" width="480">
<p>Cendres volantes contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 01 14 </p>
</td>
<td valign="top" width="480">
<p>Cendres volantes autres que celles visées à la rubrique 19 01 13.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 01 15* </p>
</td>
<td valign="top" width="480">
<p>Cendres sous chaudière contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 01 16 </p>
</td>
<td valign="top" width="480">
<p>Cendres sous chaudière autres que celles visées à la rubrique 19 01 15.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 01 17* </p>
</td>
<td valign="top" width="480">
<p>Déchets de pyrolyse contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 01 18 </p>
</td>
<td valign="top" width="480">
<p>Déchets de pyrolyse autres que ceux visés à la rubrique 19 01 17.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 01 19 </p>
</td>
<td valign="top" width="480">
<p>Sables provenant de lits fluidisés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 01 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant des traitements physico-chimiques des déchets (y compris déchromatation, décyanuration, neutralisation). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 02 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets prémélangés composés seulement de déchets non dangereux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 02 04* </p>
</td>
<td valign="top" width="480">
<p>Déchets prémélangés contenant au moins un déchet dangereux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 02 05* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant des traitements physico-chimiques contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 02 06 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant des traitements physico-chimiques autres que celles visées à la rubrique 19 02 05.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 02 07* </p>
</td>
<td valign="top" width="480">
<p>Hydrocarbures et concentrés provenant d'une séparation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 02 08* </p>
</td>
<td valign="top" width="480">
<p>Déchets combustibles liquides contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 02 09* </p>
</td>
<td valign="top" width="480">
<p>Déchets combustibles solides contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 02 10 </p>
</td>
<td valign="top" width="480">
<p>Déchets combustibles autres que ceux visés aux rubriques 19 02 08 et 19 02 09.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 02 11* </p>
</td>
<td valign="top" width="480">
<p>Autres déchets contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 02 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets stabilisés/ solidifiés (4). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 03 04* </p>
</td>
<td valign="top" width="480">
<p>Déchets catalogués comme dangereux, partiellement (5) stabilisés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 03 05 </p>
</td>
<td valign="top" width="480">
<p>Déchets stabilisés autres que ceux visés à la rubrique 19 03 04.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 03 06* </p>
</td>
<td valign="top" width="480">
<p>Déchets catalogués comme dangereux, solidifiés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 03 07 </p>
</td>
<td valign="top" width="480">
<p>Déchets solidifiés autres que ceux visés à la rubrique 19 03 06.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 04 </p>
</td>
<td valign="top" width="480">
<p>Déchets vitrifiés et déchets provenant de la vitrification. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 04 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets vitrifiés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 04 02* </p>
</td>
<td valign="top" width="480">
<p>Cendres volantes et autres déchets du traitement des gaz de fumée. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 04 03* </p>
</td>
<td valign="top" width="480">
<p>Phase solide non vitrifiée. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 04 04 </p>
</td>
<td valign="top" width="480">
<p>Déchets liquides aqueux provenant de la trempe des déchets vitrifiés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 05 </p>
</td>
<td valign="top" width="480">
<p>Déchets de compostage. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 05 01 </p>
</td>
<td valign="top" width="480">
<p>Fraction non compostée des déchets municipaux et assimilés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 05 02 </p>
</td>
<td valign="top" width="480">
<p>Fraction non compostée des déchets animaux et végétaux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 05 03 </p>
</td>
<td valign="top" width="480">
<p>Compost déclassé. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 05 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 06 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du traitement anaérobie des déchets. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 06 03 </p>
</td>
<td valign="top" width="480">
<p>Liqueurs provenant du traitement anaérobie des déchets municipaux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 06 04 </p>
</td>
<td valign="top" width="480">
<p>Digestats provenant du traitement anaérobie des déchets municipaux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 06 05 </p>
</td>
<td valign="top" width="480">
<p>Liqueurs provenant du traitement anaérobie des déchets animaux et végétaux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 06 06 </p>
</td>
<td valign="top" width="480">
<p>Digestats provenant du traitement anaérobie des déchets animaux et végétaux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 06 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 07 </p>
</td>
<td valign="top" width="480">
<p>Lixiviats de décharges. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 07 02* </p>
</td>
<td valign="top" width="480">
<p>Lixiviats de décharges contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 07 03 </p>
</td>
<td valign="top" width="480">
<p>Lixiviats de décharges autres que ceux visés à la rubrique 19 07 02.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 08 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant d'installations de traitement des eaux usées non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 08 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets de dégrillage. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 08 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets de dessablage. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 08 05 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement des eaux usées urbaines. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 08 06* </p>
</td>
<td valign="top" width="480">
<p>Résines échangeuses d'ions saturées ou usées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 08 07* </p>
</td>
<td valign="top" width="480">
<p>Solutions et boues provenant de la régénération des échangeurs d'ions. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 08 08* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant des systèmes à membrane contenant des métaux lourds. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 08 09 </p>
</td>
<td valign="top" width="480">
<p>Mélanges de graisse et d'huile provenant de la séparation huile/ eaux usées ne contenant que des huiles et graisses alimentaires. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 08 10* </p>
</td>
<td valign="top" width="480">
<p>Mélanges de graisse et d'huile provenant de la séparation huile/ eaux usées autres que ceux visés à la rubrique 19 08 09.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 08 11* </p>
</td>
<td valign="top" width="480">
<p>Boues contenant des substances dangereuses provenant du traitement biologique des eaux usées industrielles. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 08 12 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement biologique des eaux usées industrielles autres que celles visées à la rubrique 19 08 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 08 13* </p>
</td>
<td valign="top" width="480">
<p>Boues contenant des substances dangereuses provenant d'autres traitements des eaux usées industrielles. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 08 14 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant d'autres traitements des eaux usées industrielles autres que celles divisées à la rubrique 19 08 13.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 08 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 09 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la préparation d'eau destinée à la consommation humaine ou d'eau à usage industriel. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 09 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets solides de première filtration et de dégrillage. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 09 02 </p>
</td>
<td valign="top" width="480">
<p>Boues de clarification de l'eau. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 09 03 </p>
</td>
<td valign="top" width="480">
<p>Boues de décarbonatation. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 09 04 </p>
</td>
<td valign="top" width="480">
<p>Charbon actif usé. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 09 05 </p>
</td>
<td valign="top" width="480">
<p>Résines échangeuses d'ions saturées ou usées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 09 06 </p>
</td>
<td valign="top" width="480">
<p>Solutions et boues provenant de la régénération des échangeurs d'ions. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 09 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 10 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du broyage de déchets contenant des métaux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 10 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets de fer ou d'acier. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 10 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets de métaux non ferreux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 10 03* </p>
</td>
<td valign="top" width="480">
<p>Fraction légère des résidus de broyage et poussières contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 10 04 </p>
</td>
<td valign="top" width="480">
<p>Fraction légère des résidus de broyage et poussières autres que celle visée à la rubrique 19 10 03.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 10 05* </p>
</td>
<td valign="top" width="480">
<p>Autres fractions contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 10 06 </p>
</td>
<td valign="top" width="480">
<p>Autres fractions autres que celles visées à la rubrique 19 10 05.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 11 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la régénération de l'huile. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 11 01* </p>
</td>
<td valign="top" width="480">
<p>Argiles de filtration usées. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 11 02* </p>
</td>
<td valign="top" width="480">
<p>Goudrons acides. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 11 03* </p>
</td>
<td valign="top" width="480">
<p>Déchets liquides aqueux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 11 04* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du nettoyage d'hydrocarbures avec des bases. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 11 05* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 11 06 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant du traitement in situ des effluents autres que celles visées à la rubrique 19 11 05.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 11 07* </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de l'épuration des gaz de combustion. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 11 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 12 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du traitement mécanique des déchets (par exemple : tri, broyage, compactage, granulation) non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 12 01 </p>
</td>
<td valign="top" width="480">
<p>Papier et carton. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 12 02 </p>
</td>
<td valign="top" width="480">
<p>Métaux ferreux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 12 03 </p>
</td>
<td valign="top" width="480">
<p>Métaux non ferreux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 12 04 </p>
</td>
<td valign="top" width="480">
<p>Matières plastiques et caoutchouc. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 12 05 </p>
</td>
<td valign="top" width="480">
<p>Verre. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 12 06* </p>
</td>
<td valign="top" width="480">
<p>Bois contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 12 07 </p>
</td>
<td valign="top" width="480">
<p>Bois autres que ceux visés à la rubrique 19 12 06.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 12 08 </p>
</td>
<td valign="top" width="480">
<p>Textiles. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 12 09 </p>
</td>
<td valign="top" width="480">
<p>Minéraux (par exemple : sable, cailloux). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 12 10 </p>
</td>
<td valign="top" width="480">
<p>Déchets combustibles (combustible issu de déchets). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 12 11* </p>
</td>
<td valign="top" width="480">
<p>Autres déchets (y compris mélanges) provenant du traitement mécanique des déchets contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 12 12 </p>
</td>
<td valign="top" width="480">
<p>Autres déchets (y compris mélanges) provenant du traitement mécanique des déchets autres que ceux visés à la rubrique 19 12 11.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 13 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant de la décontamination des sols et des eaux souterraines. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 13 01* </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant de la décontamination des sols contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 13 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets solides provenant de la décontamination des sols autres que ceux visés à la rubrique 19 13 01.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 13 03* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant de la décontamination des sols contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 13 04 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant de la décontamination des sols autres que celles visées à la rubrique 19 13 03.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 13 05* </p>
</td>
<td valign="top" width="480">
<p>Boues provenant de la décontamination des eaux souterraines contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 13 06 </p>
</td>
<td valign="top" width="480">
<p>Boues provenant de la décontamination des eaux souterraines autres que celles visées à la rubrique 19 13 05.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 13 07* </p>
</td>
<td valign="top" width="480">
<p>Déchets liquides aqueux et concentrés aqueux provenant de la décontamination des eaux souterraines contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>19 13 08 </p>
</td>
<td valign="top" width="480">
<p>Déchets liquides aqueux et concentrés aqueux provenant de la décontamination des eaux souterraines autres que ceux visés à la rubrique 19 13 07.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 </p>
</td>
<td valign="top" width="480">
<p align="center">DÉCHETS MUNICIPAUX (DÉCHETS MÉNAGERS ET DÉCHETS ASSIMILÉS PROVENANT DES COMMERCES, DES INDUSTRIES ET DES ADMINISTRATIONS), Y COMPRIS LES FRACTIONS COLLECTÉES SÉPARÉMENT </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 </p>
</td>
<td valign="top" width="480">
<p>Fractions collectées séparément (sauf section 15 01). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 01 </p>
</td>
<td valign="top" width="480">
<p>Papier et carton. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 02 </p>
</td>
<td valign="top" width="480">
<p>Verre. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 08 </p>
</td>
<td valign="top" width="480">
<p>Déchets de cuisine et de cantine biodégradables. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 10 </p>
</td>
<td valign="top" width="480">
<p>Vêtements. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 11 </p>
</td>
<td valign="top" width="480">
<p>Textiles. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 13* </p>
</td>
<td valign="top" width="480">
<p>Solvants. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 14* </p>
</td>
<td valign="top" width="480">
<p>Acides. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 15* </p>
</td>
<td valign="top" width="480">
<p>Déchets basiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 17* </p>
</td>
<td valign="top" width="480">
<p>Produits chimiques de la photographie. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 19* </p>
</td>
<td valign="top" width="480">
<p>Pesticides. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 21* </p>
</td>
<td valign="top" width="480">
<p>Tubes fluorescents et autres déchets contenant du mercure. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 23* </p>
</td>
<td valign="top" width="480">
<p>Equipements mis au rebut contenant des chlorofluorocarbones. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 25 </p>
</td>
<td valign="top" width="480">
<p>Huiles et matières grasses alimentaires. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 26* </p>
</td>
<td valign="top" width="480">
<p>Huiles et matières grasses autres que celles visées à la rubrique 20 01 25.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 27* </p>
</td>
<td valign="top" width="480">
<p>Peinture, encres, colles et résines contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 28 </p>
</td>
<td valign="top" width="480">
<p>Peinture, encres, colles et résines autres que celles visées à la rubrique 20 01 27.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 29* </p>
</td>
<td valign="top" width="480">
<p>Détergents contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 30 </p>
</td>
<td valign="top" width="480">
<p>Détergents autres que ceux visés à la rubrique 20 01 29.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 31* </p>
</td>
<td valign="top" width="480">
<p>Médicaments cytotoxiques et citostatiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 32 </p>
</td>
<td valign="top" width="480">
<p>Médicaments autres que ceux visés à la rubrique 20 01 31.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 33* </p>
</td>
<td valign="top" width="480">
<p>Piles et accumulateurs visés aux rubriques 16 06 01,16 06 02 ou 16 06 03, et piles et accumulateurs non triés contenant ces piles. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 34 </p>
</td>
<td valign="top" width="480">
<p>Piles et accumulateurs autres que ceux visés à la rubrique 20 01 33.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 35* </p>
</td>
<td valign="top" width="480">
<p>Equipements électriques et électroniques mis au rebut contenant des composants dangereux (6), autres que ceux visés aux rubriques 20 01 21 et 20 01 23.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 36 </p>
</td>
<td valign="top" width="480">
<p>Equipements électriques et électroniques mis au rebut autres que ceux visés aux rubriques 20 01 21,20 01 23 et 20 01 35.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 37* </p>
</td>
<td valign="top" width="480">
<p>Bois contenant des substances dangereuses. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 38 </p>
</td>
<td valign="top" width="480">
<p>Bois autres que ceux visés à la rubrique 20 01 37.</p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 39 </p>
</td>
<td valign="top" width="480">
<p>Matières plastiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 40 </p>
</td>
<td valign="top" width="480">
<p>Métaux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 41 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du ramonage de cheminée. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 01 99 </p>
</td>
<td valign="top" width="480">
<p>Autres fractions non spécifiées ailleurs. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets de jardins et de parcs (y compris les déchets de cimetière). </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 02 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets biodégradables. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 02 02 </p>
</td>
<td valign="top" width="480">
<p>Terres et pierres. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 02 03 </p>
</td>
<td valign="top" width="480">
<p>Autres déchets non biodégradables. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 03 </p>
</td>
<td valign="top" width="480">
<p>Autres déchets municipaux. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 03 01 </p>
</td>
<td valign="top" width="480">
<p>Déchets municipaux en mélange. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 03 02 </p>
</td>
<td valign="top" width="480">
<p>Déchets de marchés. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 03 03 </p>
</td>
<td valign="top" width="480">
<p>Déchets de nettoyage des rues. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 03 04 </p>
</td>
<td valign="top" width="480">
<p>Boues de fosses septiques. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 03 06 </p>
</td>
<td valign="top" width="480">
<p>Déchets provenant du nettoyage des égouts. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 03 07 </p>
</td>
<td valign="top" width="480">
<p>Déchets encombrants. </p>
</td>
</tr>
<tr>
<td valign="top" width="119">
<p>20 03 99 </p>
</td>
<td valign="top" width="480">
<p>Déchets municipaux non spécifiés ailleurs. </p>
</td>
</tr>
<tr>
<td colspan="2" valign="top" width="599">
<p>(1) Aux fins de la présente liste de déchets, les PCB sont définis comme dans le décret no 87-59 du 2 février 1987 relatif à la mise sur le marché, à l'utilisation et à l'élimination des polychlorobiphényles et polychloroterphényles, modifié. </p>
</td>
</tr>
<tr>
<td colspan="2" valign="top" width="599">
<p>(2) Par composants dangereux provenant d'équipements électriques et électroniques, on entend notamment des piles et accumulateurs visés à la section 16 06 et considérés comme dangereux, des commutateurs au mercure, du verre provenant de tubes cathodiques et autres verres activés, etc. </p>
</td>
</tr>
<tr>
<td colspan="2" valign="top" width="599">
<p>(3) Aux fins de cette entrée, les métaux de transition sont les suivants : scandium, vanadium, manganèse, cobalt, cuivre, yttrium, niobium, hafnium, tungstène, titane, chrome, fer, nickel, zinc, zirconium, molybdène et tantale. Ces métaux ou leurs composés sont dangereux s'ils sont classés comme substances dangereuses. La classification des substances dangereuses détermine les métaux de transition et les composés de métaux de transition qui sont dangereux. </p>
</td>
</tr>
<tr>
<td colspan="2" valign="top" width="599">
<p>(4) Les processus de stabilisation modifient la dangerosité des constituants des déchets et transforment ainsi des déchets dangereux en déchets non dangereux. Les processus de solidification modifient seulement l'état physique des déchets au moyen d'additifs (par exemple : passage de l'état liquide à l'état solide) sans modifier leurs propriétés chimiques. </p>
</td>
</tr>
<tr>
<td colspan="2" valign="top" width="599">
<p>(5) Un déchet est considéré comme partiellement stabilisé si, après le processus de stabilisation, il est encore, à court, moyen ou long terme, susceptible de libérer dans l'environnement des constituants dangereux qui n'ont pas été entièrement transformés en constituants non dangereux. </p>
</td>
</tr>
<tr>
<td colspan="2" valign="top" width="599">
<p>(6) Par composants dangereux provenant d'équipements électriques et électroniques, on entend notamment des piles et accumulateurs visés à la section 16 06 et considérés comme dangereux, des commutateurs au mercure, du verre provenant de tubes cathodiques et autres verres activés, etc.</p>
</td>
</tr>
</tbody>
</table>
