# Article R211-8

Lorsque le contrat comporte une possibilité expresse de révision du prix, dans les limites prévues à l'article L. 211-12, il doit mentionner les modalités précises de calcul, tant à la hausse qu'à la baisse, des variations des prix, et notamment le montant des frais de transport et taxes y afférentes, la ou les devises qui peuvent avoir une incidence sur le prix du voyage ou du séjour, la part du prix à laquelle s'applique la variation, le cours de la ou des devises retenu comme référence lors de l'établissement du prix figurant au contrat.
