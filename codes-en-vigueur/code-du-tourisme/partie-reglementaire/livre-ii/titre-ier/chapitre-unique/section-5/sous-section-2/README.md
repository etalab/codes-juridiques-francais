# Sous-section 2 : Gestion du registre des agents de voyage et autres opérateurs de la vente de voyages et de séjours.

- [Article R211-23](article-r211-23.md)
- [Article R211-24](article-r211-24.md)
- [Article R211-25](article-r211-25.md)
