# Article R211-18

Après la conclusion du contrat, le transporteur contractuel ou l'organisateur du voyage informe le consommateur de toute modification de l'identité du transporteur assurant effectivement le ou les tronçons de vols figurant au contrat.

Cette modification est portée à la connaissance du consommateur, y compris par l'intermédiaire de la personne physique ou morale ayant vendu le titre de transport aérien, dès qu'elle est connue. Le consommateur en est informé au plus tard, obligatoirement, au moment de l'enregistrement ou avant les opérations d'embarquement lorsque la correspondance s'effectue sans enregistrement préalable.
