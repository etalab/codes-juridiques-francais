# Section 9 : Contrat de jouissance d'immeuble à temps partagé.

- [Article R211-42](article-r211-42.md)
- [Article R211-43](article-r211-43.md)
- [Article R211-44](article-r211-44.md)
- [Article R211-45](article-r211-45.md)
- [Article R211-46](article-r211-46.md)
- [Article R211-47](article-r211-47.md)
- [Article R211-48](article-r211-48.md)
- [Article R211-49](article-r211-49.md)
