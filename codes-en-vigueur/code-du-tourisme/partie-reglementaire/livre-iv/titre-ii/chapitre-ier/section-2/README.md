# Section 2 : Dispositions particulières relatives à l'investissement dans l'immobilier de loisirs.

- [Article D421-2](article-d421-2.md)
- [Article D421-3](article-d421-3.md)
