# Article R363-1

Les titres Ier à III du présent livre sont applicables à Mayotte, sous réserve des dispositions du présent chapitre.
