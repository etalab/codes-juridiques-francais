# Article R363-3

Pour l'application de l'article D. 332-6, les références aux articles L. 411-1 et R. 412-16 du code forestier sont respectivement remplacées par celles aux articles L. 411-1 et R. * 412-14 du code forestier de Mayotte.

Pour l'application de l'article D. 332-9, la référence au 2° de l'article R. 412-17 du code forestier est remplacée par celle au dernier alinéa de l'article R. * 412-15 du code forestier de Mayotte.
