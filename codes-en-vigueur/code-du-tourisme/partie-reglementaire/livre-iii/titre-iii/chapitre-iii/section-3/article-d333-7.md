# Article D333-7

Les règles relatives à l'installation des résidences mobiles de loisirs sont fixées par les articles R. * 111-30 et R. * 111-33 à R. * 111-36 du code de l'urbanisme.
