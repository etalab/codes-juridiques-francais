# Article D331-5

Les règles relatives à l'aménagement d'un terrain de camping et à l'installation des caravanes sont fixées par les articles R. * 111-30, R. * 111-37 à R. * 111-45, R. * 421-19 et R. * 421-23 du code de l'urbanisme.
