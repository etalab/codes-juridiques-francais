# Article R133-38

La délibération sollicitant le classement en station de tourisme, accompagnée du dossier de demande, est adressée par le maire au préfet par voie électronique ou, à défaut, par voie postale.

La délibération délimite le territoire faisant l'objet de la demande de classement. Un plan lui est annexé.
