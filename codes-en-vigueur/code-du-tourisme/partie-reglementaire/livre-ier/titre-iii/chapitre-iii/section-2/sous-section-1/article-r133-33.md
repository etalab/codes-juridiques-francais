# Article R133-33

La capacité d'hébergement d'une population non permanente mentionnée à l'article R. 133-32 est estimée par le cumul suivant :

-nombre de chambres en hôtellerie classée et non classée multiplié par deux ;

-nombre de lits en résidence de tourisme répondant à des critères déterminés par décret ;

-nombre de logements meublés multiplié par quatre ;

-nombre d'emplacements situés en terrain de camping multiplié par trois ;

-nombre de lits en village de vacances et maisons familiales de vacances ;

-nombre de résidences secondaires multiplié par cinq ;

-nombre de chambres d'hôtes multiplié par deux ;

-nombre d'anneaux de plaisance dans les ports de plaisance multiplié par quatre.

La population municipale de la commune à laquelle se rapporte la capacité d'hébergement d'une population non permanente est celle qui résulte du dernier recensement authentifié.

Le tableau ci-après précise par strate démographique de population municipale de la commune le pourcentage minimal exigé de capacité d'hébergement d'une population non permanente :

<table>
<tbody>
<tr>
<th>
<br/>POPULATION MUNICIPALE DE LA COMMUNE <p>(habitants) <br/>
</p>
</th>
<th>
<br/>POURCENTAGE MINIMUM EXIGÉ DE CAPACITÉ <p>d'hébergement d'une population </p>
<p>non permanente <br/>
</p>
</th>
</tr>
<tr>
<td align="center">
<br/>Jusqu'à 1 999 <br/>
</td>
<td align="center">
<br/>15 % <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>De 2 000 à 3 499 <br/>
</td>
<td align="center">
<br/>12, 5 % <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>De 3 500 à 4 999 <br/>
</td>
<td align="center">
<br/>10, 5 % <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>De 5 000 à 9 999 <br/>
</td>
<td align="center">
<br/>8, 5 % <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>A partir de 10 000 <br/>
</td>
<td align="center">
<br/>4, 5 %<br/>
</td>
</tr>
</tbody>
</table>
