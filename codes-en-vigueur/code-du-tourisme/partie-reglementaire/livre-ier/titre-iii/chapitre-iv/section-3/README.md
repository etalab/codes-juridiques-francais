# Section 3 : Offices de tourisme de groupements de collectivités territoriales

- [Sous-section 2 : Dispositions particulières applicables aux offices de tourisme intercommunaux constitués sous la forme d'un établissement public industriel et commercial.](sous-section-2)
- [Sous-section 3 : Dispositions particulières applicables aux offices de tourisme intercommunaux constitués sous une forme autre que celle d'un établissement public industriel et commercial.](sous-section-3)
- [Sous-section 4 : Dispositions particulières applicables aux offices de tourisme intercommunaux dans les stations classées.](sous-section-4)
- [Sous-section 5 : Classement.](sous-section-5)
