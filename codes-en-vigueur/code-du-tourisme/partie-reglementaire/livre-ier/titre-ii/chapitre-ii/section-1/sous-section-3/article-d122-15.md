# Article D122-15

Participent aux travaux du Conseil national du tourisme et du comité stratégique, à titre consultatif, le chef du service du contrôle général économique et financier ou son représentant, le sous-directeur du tourisme ou son représentant ainsi que le président du groupement d'intérêt économique Agence de développement touristique de la France, ou son représentant.
