# Article D122-14

Des conseillers techniques peuvent être nommés auprès d'une section par arrêté du ministre chargé du tourisme pour une durée de cinq ans.
