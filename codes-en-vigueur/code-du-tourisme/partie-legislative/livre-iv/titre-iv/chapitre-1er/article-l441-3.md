# Article L441-3

Les règles relatives au tarif de la taxe de séjour à Saint-Martin et à Saint-Barthélemy sont fixées par l'article L. 2564-1 du code général des collectivités territoriales  ci-après reproduit :

" Art. L. 2564-1 du code général des collectivités territoriales.

Pour l'application aux communes de Mayotte de la deuxième partie du présent code :

1° La référence au département ou à la région est remplacée par la référence au Département de Mayotte ;

2° La référence au conseil régional ou aux conseils généraux est remplacée par la référence au conseil général ;

3° La référence à la valeur horaire du salaire minimum de croissance est remplacée par la référence au taux horaire du salaire minimum interprofessionnel garanti en vigueur à Mayotte."
