# TITRE III : LES COLLECTIVITÉS TERRITORIALES ET LEURS GROUPEMENTS.

- [Chapitre 1er : La région](chapitre-1er)
- [Chapitre 2 : Le département.](chapitre-2)
- [Chapitre 3 : La commune](chapitre-3)
- [Chapitre 4 : Groupements intercommunaux.](chapitre-4)
- [Chapitre 5 : La métropole de Lyon](chapitre-5)
