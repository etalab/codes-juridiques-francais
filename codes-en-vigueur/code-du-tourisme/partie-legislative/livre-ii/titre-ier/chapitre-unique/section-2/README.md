# Section 2 : Contrat de vente de voyages et de séjours

- [Article L211-7](article-l211-7.md)
- [Article L211-8](article-l211-8.md)
- [Article L211-9](article-l211-9.md)
- [Article L211-10](article-l211-10.md)
- [Article L211-11](article-l211-11.md)
- [Article L211-12](article-l211-12.md)
- [Article L211-13](article-l211-13.md)
- [Article L211-14](article-l211-14.md)
- [Article L211-15](article-l211-15.md)
