# Article L211-13

Lorsque, avant le départ, le respect d'un des éléments essentiels du contrat est rendu impossible par suite d'un événement extérieur qui s'impose au vendeur, celui-ci doit le plus rapidement possible en avertir l'acheteur et informer ce dernier de la faculté dont il dispose soit de résilier le contrat, soit d'accepter la modification proposée par le vendeur.

Cet avertissement et cette information doivent être confirmés par écrit à l'acheteur, qui doit faire connaître son choix dans les meilleurs délais. Lorsqu'il résilie le contrat, l'acheteur a droit, sans supporter de pénalités ou de frais, au remboursement de la totalité des sommes qu'il a versées.

Le présent article s'applique également en cas de modification significative du prix du contrat intervenant conformément aux conditions prévues à l'article L. 211-12.
