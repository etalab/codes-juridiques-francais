# Article L211-22

La déclaration visée à l'article L. 211-21 vaut immatriculation automatique et temporaire au registre mentionné au I de l'article L. 211-18.
