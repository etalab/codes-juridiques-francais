# Article L211-20

Tout ressortissant d'un Etat membre de la Communauté européenne ou d'un autre Etat partie à l'accord sur l'Espace économique européen, légalement établi dans l'un de ces Etats, pour l'exercice d'activités mentionnées au I de l'article L. 211-1, peut exercer ces activités de façon temporaire et occasionnelle en France.

Toutefois, lorsque les activités mentionnées à l'article L. 211-1 ou la formation y conduisant ne sont pas réglementées dans l'Etat dans lequel est établi le prestataire, celui-ci doit avoir exercé cette activité dans cet Etat pendant au moins deux ans au cours des dix années qui précèdent la prestation.
