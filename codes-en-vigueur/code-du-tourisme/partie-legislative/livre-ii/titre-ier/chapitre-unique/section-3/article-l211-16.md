# Article L211-16

Toute personne physique ou morale qui se livre aux opérations mentionnées à l'article L. 211-1 est responsable de plein droit à l'égard de l'acheteur de la bonne exécution des obligations résultant du contrat, que ce contrat ait été conclu à distance ou non et que ces obligations soient à exécuter par elle-même ou par d'autres prestataires de services, sans préjudice de son droit de recours contre ceux-ci et dans la limite des dédommagements prévus par les conventions internationales.

Toutefois, elle peut s'exonérer de tout ou partie de sa responsabilité en apportant la preuve que l'inexécution ou la mauvaise exécution du contrat est imputable soit à l'acheteur, soit au fait, imprévisible et insurmontable, d'un tiers étranger à la fourniture des prestations prévues au contrat, soit à un cas de force majeure.
