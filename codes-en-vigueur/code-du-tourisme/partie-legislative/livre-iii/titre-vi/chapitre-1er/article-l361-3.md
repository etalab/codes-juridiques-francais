# Article L361-3

Les règles relatives à l'affectation à des équipements touristiques et hôteliers dans la bande littorale sont fixées par les articles L. 156-2 à L. 156-4 du code de l'urbanisme.
