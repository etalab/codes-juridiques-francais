# TITRE Ier : HÔTELS, RESTAURANTS, CAFÉS ET DÉBITS DE BOISSONS

- [Chapitre 1er : Hôtels](chapitre-1er)
- [Chapitre 3 : Cafés et débits de boissons](chapitre-3)
- [Chapitre 4 : Débits de boissons ayant pour activité principale l'exploitation d'une piste de danse](chapitre-4)
