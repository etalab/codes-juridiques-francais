# Article L342-7

Sont dénommés " remontées mécaniques " tous les appareils de transports publics de personnes par chemin de fer funiculaire ou à crémaillère, par téléphérique, par téléskis ou par tout autre engin utilisant des câbles porteurs ou tracteurs.
