# Article L343-4

Les règles relatives aux parcs naturels régionaux sont fixées par les articles L. 333-2 à L. 333-4 du code de l'environnement.
