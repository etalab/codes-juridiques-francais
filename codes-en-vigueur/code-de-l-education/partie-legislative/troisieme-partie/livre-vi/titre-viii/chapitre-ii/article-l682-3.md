# Article L682-3

<div align="left">Pour l'application à Mayotte de l'article L. 611-3, les mots : " les régions " sont remplacés par les mots : " le Département de Mayotte ".<br/>
</div>
