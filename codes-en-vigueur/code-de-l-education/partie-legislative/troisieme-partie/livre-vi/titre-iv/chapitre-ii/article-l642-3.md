# Article L642-3

La commission des titres d'ingénieur, dont les membres sont nommés par le ministre chargé de l'enseignement supérieur, est consultée sur toutes les questions concernant les titres d'ingénieur diplômé.

La composition de cette commission est fixée par décret en Conseil d'Etat ; elle comprend notamment une représentation des universités, des instituts, des écoles et des grands établissements ainsi que des organisations professionnelles.
