# Article L613-4

La validation prévue à l'article L. 613-3 est prononcée par un jury dont les membres sont désignés par le président de l'université ou le chef de l'établissement d'enseignement supérieur en fonction de la nature de la validation demandée. Pour la validation des acquis de l'expérience, ce jury comprend, outre les enseignants-chercheurs qui en constituent la majorité, des personnes compétentes pour apprécier la nature des acquis, notamment professionnels, dont la validation est sollicitée. Les jurys sont composés de façon à concourir à une représentation équilibrée entre les femmes et les hommes.

Le jury se prononce au vu d'un dossier constitué par le candidat, à l'issue d'un entretien avec ce dernier et, le cas échéant, d'une mise en situation professionnelle réelle ou reconstituée, lorsque cette procédure est prévue par l'autorité qui délivre la certification. Il se prononce également sur l'étendue de la validation et, en cas de validation partielle, sur la nature des connaissances et aptitudes devant faire l'objet d'un contrôle complémentaire.

La validation produit les mêmes effets que le succès à l'épreuve ou aux épreuves de contrôle des connaissances et des aptitudes qu'elle remplace.

Un décret en Conseil d'Etat fixe les conditions d'application de l'article L. 613-3 et du présent article. ;
