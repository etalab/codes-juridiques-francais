# Article L613-3

Toute personne justifiant d'une activité professionnelle salariée, non salariée, bénévole ou de volontariat ou ayant exercé des responsabilités syndicales, un mandat électoral local ou une fonction élective locale en rapport direct avec le contenu du diplôme ou du titre visé peut demander la validation des acquis de son expérience prévue à l'article L. 6411-1 du code du travail pour justifier de tout ou partie des connaissances et des aptitudes exigées pour l'obtention d'un diplôme ou titre délivré, au nom de l'Etat, par un établissement d'enseignement supérieur.

La durée minimale d'activité requise pour que la demande de validation soit recevable est de trois ans, que l'activité ait été exercée de façon continue ou non. Pour apprécier cette durée, l'autorité ou l'organisme qui se prononce sur la recevabilité de la demande mentionnée à l'article L. 6412-2 du même code peut prendre en compte des activités mentionnées au premier alinéa du présent article, de nature différente, exercées sur une même période.

Lorsqu'une demande de validation des acquis de l'expérience émane d'un membre bénévole d'une association, le conseil d'administration de l'association ou, à défaut, l'assemblée générale peut émettre un avis pour éclairer le jury sur l'engagement du membre bénévole.

Les périodes de formation initiale ou continue en milieu professionnel, suivie de façon continue ou non par les personnes n'ayant pas atteint le niveau V de qualification pour la préparation d'un titre ou d'un diplôme délivré, au nom de l'Etat, par un établissement d'enseignement supérieur, sont prises en compte dans le calcul de la durée minimale d'activité requise.

Toute personne peut également demander la validation des études supérieures qu'elle a accomplies, notamment à l'étranger.
