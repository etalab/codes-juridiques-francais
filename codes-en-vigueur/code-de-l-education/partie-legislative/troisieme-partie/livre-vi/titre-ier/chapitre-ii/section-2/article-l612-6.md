# Article L612-6

L'admission dans les formations du deuxième cycle est ouverte à tous les titulaires des diplômes sanctionnant les études de premier cycle ainsi qu'à ceux qui peuvent bénéficier des dispositions de l'article L. 613-5 ou des dérogations prévues par les textes réglementaires.

La liste limitative des formations dans lesquelles cette admission peut dépendre des capacités d'accueil des établissements et, éventuellement, être subordonnée au succès à un concours ou à l'examen du dossier du candidat, est établie par décret après avis du Conseil national de l'enseignement supérieur et de la recherche. La mise en place de ces formations prend en compte l'évolution prévisible des qualifications et des besoins, qui font l'objet d'une évaluation régionale et nationale.
