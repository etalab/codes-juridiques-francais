# Titre VIII : Dispositions applicables aux universités implantées dans une ou plusieurs régions et départements d'outre-mer.

- [Chapitre unique : Dispositions applicables à l'université des Antilles et de la Guyane.](chapitre-unique)
