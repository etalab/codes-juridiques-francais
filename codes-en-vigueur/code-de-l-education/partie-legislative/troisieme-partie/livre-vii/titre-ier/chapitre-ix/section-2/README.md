# Section 2 : Régime financier.

- [Article L719-4](article-l719-4.md)
- [Article L719-5](article-l719-5.md)
- [Article L719-6](article-l719-6.md)
