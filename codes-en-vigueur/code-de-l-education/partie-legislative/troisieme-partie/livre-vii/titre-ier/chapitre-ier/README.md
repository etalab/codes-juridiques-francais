# Chapitre Ier : Principes relatifs à la création et à l'autonomie des établissements publics à caractère scientifique, culturel et professionnel.

- [Article L711-1](article-l711-1.md)
- [Article L711-2](article-l711-2.md)
- [Article L711-3](article-l711-3.md)
- [Article L711-4](article-l711-4.md)
- [Article L711-5](article-l711-5.md)
- [Article L711-6](article-l711-6.md)
- [Article L711-7](article-l711-7.md)
- [Article L711-8](article-l711-8.md)
- [Article L711-9](article-l711-9.md)
- [Article L711-10](article-l711-10.md)
