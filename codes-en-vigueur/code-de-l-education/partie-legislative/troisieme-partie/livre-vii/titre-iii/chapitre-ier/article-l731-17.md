# Article L731-17

Les dispositions des articles L. 443-2 à L. 443-4 sont applicables aux écoles d'enseignement technique supérieur privées.
