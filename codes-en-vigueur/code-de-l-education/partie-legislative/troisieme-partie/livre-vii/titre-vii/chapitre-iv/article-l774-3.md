# Article L774-3

Pour l'application de l'article L. 712-3 à la Nouvelle-Calédonie, les mots : " de la région " sont remplacés par les mots : " de la Nouvelle-Calédonie ".

Pour l'application de l'article L. 718-2 à la Nouvelle-Calédonie, les mots : " qui peut être académique ou interacadémique " sont supprimés.

Pour l'application du quatrième alinéa de l'article L. 718-5 à la Nouvelle-Calédonie, les mots : " la ou les régions et les autres collectivités territoriales " sont remplacés par les mots : " la Nouvelle-Calédonie, les provinces et les communes " et la deuxième phrase est supprimée.

Pour l'application à la Nouvelle-Calédonie, le 3° de l'article L. 718-11 est ainsi rédigé : " 3° Des représentants des entreprises, de la Nouvelle-Calédonie, des provinces et des communes concernées et des associations " ;

Pour l'application du 1° de l'article L. 719-3 à la Nouvelle-Calédonie, les mots : " des représentants de collectivités territoriales " sont remplacés par les mots : " de la Nouvelle-Calédonie et des provinces " ;

Pour l'application de l'article L. 719-4 à la Nouvelle-Calédonie, les mots : " régions " et " départements " sont remplacés par les mots : " de la Nouvelle-Calédonie, des provinces ".

Le ministre chargé de l'enseignement supérieur exerce les compétences dévolues par le présent livre au recteur d'académie, chancelier des universités.

Les références à des dispositions législatives ne s'appliquant pas en Nouvelle-Calédonie sont remplacées par les références aux dispositions, ayant le même objet, qui y sont applicables.
