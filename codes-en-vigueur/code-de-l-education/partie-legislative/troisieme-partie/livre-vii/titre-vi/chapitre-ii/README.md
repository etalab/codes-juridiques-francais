# Chapitre II : Dispositions communes aux établissements publics.

- [Article L762-1](article-l762-1.md)
- [Article L762-2](article-l762-2.md)
- [Article L762-3](article-l762-3.md)
