# Article L916-1

Des assistants d'éducation peuvent être recrutés par les établissements d'enseignement mentionnés au chapitre II du titre Ier et au titre II du livre IV pour exercer des fonctions d'assistance à l'équipe éducative en lien avec le projet d'établissement, notamment pour l'encadrement et la surveillance des élèves.

A l'issue de leur contrat, les assistants d'éducation peuvent demander à faire valider l'expérience acquise dans les conditions définies par les articles L. 2323-33, L. 6111-1, L. 6311-1, L. 6411-1 et L. 6422-1 du code du travail.

Les assistants d'éducation peuvent exercer leurs fonctions dans l'établissement qui les a recrutés, dans un ou plusieurs autres établissements ainsi que, compte tenu des besoins appréciés par l'autorité administrative, dans une ou plusieurs écoles. Dans ce dernier cas, les directeurs d'école peuvent participer à la procédure de recrutement.

Les assistants d'éducation sont recrutés par des contrats d'une durée maximale de trois ans, renouvelables dans la limite d'une période d'engagement totale de six ans.

Le dispositif des assistants d'éducation est destiné à bénéficier en priorité à des étudiants boursiers.

Les conditions d'application du présent article sont fixées par décret pris après avis du comité technique ministériel du ministère chargé de l'éducation. Ce décret précise les conditions dans lesquelles est aménagé le temps de travail des assistants d'éducation, en particulier pour ceux qui sont astreints à un service de nuit. Il précise également les droits reconnus à ces agents au titre des articles L. 970-1 et suivants du code du travail. Il peut déroger, dans la mesure justifiée par la nature de leurs missions, aux dispositions générales prises pour l'application de l'article 7 de la loi n° 84-16 du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat.
