# Chapitre VI : Dispositions relatives aux assistants d'éducation.

- [Article L916-1](article-l916-1.md)
- [Article L916-2](article-l916-2.md)
