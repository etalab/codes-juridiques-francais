# Chapitre II : Dispositions propres aux personnels enseignants.

- [Article L912-1](article-l912-1.md)
- [Article L912-1-1](article-l912-1-1.md)
- [Article L912-1-2](article-l912-1-2.md)
- [Article L912-1-3](article-l912-1-3.md)
- [Article L912-2](article-l912-2.md)
- [Article L912-3](article-l912-3.md)
- [Article L912-4](article-l912-4.md)
