# Chapitre Ier : Dispositions communes.

- [Article L951-1](article-l951-1.md)
- [Article L951-1-1](article-l951-1-1.md)
- [Article L951-2](article-l951-2.md)
- [Article L951-3](article-l951-3.md)
- [Article L951-4](article-l951-4.md)
