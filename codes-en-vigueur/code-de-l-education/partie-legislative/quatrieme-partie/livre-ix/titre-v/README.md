# Titre V : Les personnels de l'enseignement supérieur

- [Chapitre Ier : Dispositions communes.](chapitre-ier)
- [Chapitre II : Les enseignants-chercheurs, les enseignants et les chercheurs](chapitre-ii)
- [Chapitre III : Les personnels ingénieurs, administratifs, techniques, ouvriers et de service.](chapitre-iii)
- [Chapitre IV : Dispositions applicables aux universités bénéficiant de responsabilités et de compétences élargies mentionnées à l'article L. 712-8.](chapitre-iv)
