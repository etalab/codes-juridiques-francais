# Section 3 : Dispositions propres aux personnels enseignants et hospitaliers.

- [Article L952-21](article-l952-21.md)
- [Article L952-22](article-l952-22.md)
- [Article L952-23](article-l952-23.md)
