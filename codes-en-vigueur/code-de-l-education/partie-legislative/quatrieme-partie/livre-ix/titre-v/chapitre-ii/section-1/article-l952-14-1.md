# Article L952-14-1

Dans le respect des dispositions de l'article 432-12 du code pénal, et par dérogation à l'interdiction d'exercer à titre professionnel une activité privée lucrative fixée au premier alinéa de l'article 25 de la loi n° 83-634 du 13 juillet 1983 portant droits et obligations des fonctionnaires, les enseignants-chercheurs autorisés à accomplir une période de service à temps partiel peuvent être autorisés à exercer, en sus de leurs fonctions, une activité dans une entreprise exerçant une ou plusieurs des missions définies à l'article L. 952-3.
