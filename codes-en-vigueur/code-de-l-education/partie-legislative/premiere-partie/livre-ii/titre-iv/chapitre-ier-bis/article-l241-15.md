# Article L241-15

Un décret précise les modalités d'application du présent chapitre.
