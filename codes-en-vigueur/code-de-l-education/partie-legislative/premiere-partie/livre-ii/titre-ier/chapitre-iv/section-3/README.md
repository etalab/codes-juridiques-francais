# Section 3 :  Orientation, formation professionnelle et apprentissage.

- [Article L214-12](article-l214-12.md)
- [Article L214-12-1](article-l214-12-1.md)
- [Article L214-13](article-l214-13.md)
- [Article L214-13-1](article-l214-13-1.md)
- [Article L214-14](article-l214-14.md)
- [Article L214-15](article-l214-15.md)
- [Article L214-16](article-l214-16.md)
- [Article L214-16-1](article-l214-16-1.md)
- [Article L214-16-2](article-l214-16-2.md)
