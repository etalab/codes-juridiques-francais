# Article L214-2

La région coordonne, sous réserve des missions de l'Etat et dans le cadre de la stratégie nationale de recherche, les initiatives territoriales visant à développer et diffuser la culture scientifique, technique et industrielle, notamment auprès des jeunes publics, et participe à leur financement.

L'Etat transfère aux régions les crédits qu'il accordait à ces initiatives.

En cohérence avec les stratégies nationales de l'enseignement supérieur et de recherche, la région définit un schéma régional de l'enseignement supérieur, de la recherche et de l'innovation qui détermine les principes et les priorités de ses interventions. Ce schéma inclut un volet relatif à l'intervention des établissements d'enseignement supérieur au titre de la formation professionnelle continue, en cohérence avec le contrat de plan régional de développement des formations et de l'orientation professionnelles mentionné à l'article L. 214-13.

Les collectivités territoriales et les établissements publics de coopération intercommunale qui accueillent des sites universitaires ou des établissements de recherche sont associés à l'élaboration du schéma régional.

La région fixe les objectifs des programmes pluriannuels d'intérêt régional en matière de recherche et détermine les investissements qui y concourent. Les orientations du schéma régional de l'enseignement supérieur, de la recherche et de l'innovation sont prises en compte par les autres schémas établis par la région en matière de formation, d'innovation et de développement économique. La région est consultée sur les aspects régionaux de la carte des formations supérieures et de la recherche.
