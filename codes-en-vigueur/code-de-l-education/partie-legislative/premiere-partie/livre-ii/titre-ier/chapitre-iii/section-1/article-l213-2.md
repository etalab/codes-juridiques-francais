# Article L213-2

Le département a la charge des collèges. Il en assure la construction, la reconstruction, l'extension, les grosses réparations, l'équipement et le fonctionnement. A ce titre, l'acquisition et la maintenance des infrastructures et des équipements, dont les matériels informatiques et les logiciels prévus pour leur mise en service, nécessaires à l'enseignement et aux échanges entre les membres de la communauté éducative sont à la charge du département.

Le département assure l'accueil, la restauration, l'hébergement ainsi que l'entretien général et technique, à l'exception des missions d'encadrement et de surveillance des élèves, dans les collèges dont il a la charge.

Pour la construction, la reconstruction, l'extension, les grosses réparations ainsi que l'équipement de ces établissements, le département peut confier à l'Etat, dans les conditions définies par les articles 3 et 5 de la loi n° 85-704 du 12 juillet 1985 relative à la maîtrise d'ouvrage publique et à ses rapports avec la maîtrise d'oeuvre privée, l'exercice, en son nom et pour son compte, de tout ou partie de certaines attributions de la maîtrise d'ouvrage.

Dans ce cas, le département bénéficie du fonds de compensation pour la taxe sur la valeur ajoutée au titre des dépenses d'investissement correspondantes.

Le département bénéficie également du fonds de compensation pour la taxe sur la valeur ajoutée au titre des dépenses d'investissement qu'il verse aux établissements publics locaux d'enseignement qui lui sont rattachés, en vue de la construction, la reconstruction et les grosses réparations de ces établissements.
