# Livre II : L'administration de l'éducation

- [Titre Ier : La répartition des compétences entre l'Etat et les collectivités territoriales](titre-ier)
- [Titre II : L'organisation des services de l'administration de l'éducation](titre-ii)
- [Titre III : Les organismes collégiaux nationaux et locaux](titre-iii)
- [Titre IV : L'inspection et l'évaluation de l'éducation](titre-iv)
- [Titre V : Dispositions relatives à Saint-Pierre-et-Miquelon.](titre-v)
- [Titre VI : Dispositions applicables dans les îles Wallis et Futuna, à Mayotte, en Polynésie française et en Nouvelle-Calédonie](titre-vi)
