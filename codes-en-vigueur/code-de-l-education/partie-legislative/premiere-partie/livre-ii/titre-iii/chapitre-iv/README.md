# Chapitre IV : Les conseils académiques de l'éducation nationale.

- [Article L234-1](article-l234-1.md)
- [Article L234-2](article-l234-2.md)
- [Article L234-6](article-l234-6.md)
- [Article L234-7](article-l234-7.md)
- [Article L234-8](article-l234-8.md)
