# Article L263-1

Sont applicables en Polynésie française les articles L. 216-10, L. 231-1 à L. 231-5, L. 231-14, les premier, deuxième, quatrième et cinquième alinéas de l'article L. 231-15, les articles L. 231-16, L. 231-17, L. 232-1 à L. 232-7, L. 233-1, L. 233-2, L. 236-1, L. 23-10-1, L. 241-1 à L. 241-4, L. 241-12 à L. 241-15 et L. 242-1.
