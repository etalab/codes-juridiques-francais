# Chapitre Ier : Dispositions applicables dans les îles Wallis et Futuna.

- [Article L261-1](article-l261-1.md)
- [Article L261-2](article-l261-2.md)
