# Article L131-2

L'instruction obligatoire peut être donnée soit dans les établissements ou écoles publics ou privés, soit dans les familles par les parents, ou l'un d'entre eux, ou toute personne de leur choix.

Dans le cadre du service public de l'enseignement et afin de contribuer à ses missions, un service public du numérique éducatif et de l'enseignement à distance est organisé pour, notamment :

1° Mettre à disposition des écoles et des établissements scolaires une offre diversifiée de services numériques permettant de prolonger l'offre des enseignements qui y sont dispensés, d'enrichir les modalités d'enseignement et de faciliter la mise en œuvre d'une aide personnalisée à tous les élèves ;

2° Proposer aux enseignants une offre diversifiée de ressources pédagogiques, des contenus et des services contribuant à leur formation ainsi que des outils de suivi de leurs élèves et de communication avec les familles ;

3° Assurer l'instruction des enfants qui ne peuvent être scolarisés dans une école ou dans un établissement scolaire, notamment ceux à besoins éducatifs particuliers. Des supports numériques adaptés peuvent être fournis en fonction des besoins spécifiques de l'élève ;

4° Contribuer au développement de projets innovants et à des expérimentations pédagogiques favorisant les usages du numérique à l'école et la coopération.

Dans le cadre de ce service public, la détermination du choix des ressources utilisées tient compte de l'offre de logiciels libres et de documents au format ouvert, si elle existe.
