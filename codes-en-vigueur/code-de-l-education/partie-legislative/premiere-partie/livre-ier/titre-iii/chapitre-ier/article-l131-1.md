# Article L131-1

L'instruction est obligatoire pour les enfants des deux sexes, français et étrangers, entre six ans et seize ans.

La présente disposition ne fait pas obstacle à l'application des prescriptions particulières imposant une scolarité plus longue.
