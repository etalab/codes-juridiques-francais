# Titre III : L'obligation scolaire, la gratuité et l'accueil des élèves des écoles maternelles et élémentaires

- [Chapitre Ier : L'obligation scolaire.](chapitre-ier)
- [Chapitre II : La gratuité de l'enseignement scolaire public.](chapitre-ii)
- [Chapitre III : L'accueil des élèves des écoles maternelles et élémentaires](chapitre-iii)
