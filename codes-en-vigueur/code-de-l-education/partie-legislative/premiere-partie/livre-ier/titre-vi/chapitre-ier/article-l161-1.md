# Article L161-1

Sont applicables dans les îles Wallis et Futuna les premier, deuxième, troisième, quatrième et septième alinéas de l'article L. 111-1, les articles L. 111-1-1 à L. 111-5, L. 112-2, L. 112-4, le premier alinéa de l'article L. 113-1, les articles L. 121-1 à L. 121-4, le I de l'article L. 121-4-1, les articles L. 121-5, L. 121-6, L. 122-1, L. 122-1-1, L. 122-5, L. 123-1 à L. 123-9, L. 131-1-1, L. 131-2, L. 131-4, L. 132-1, L. 132-2, L. 141-2, L. 141-4, L. 141-5-1, L. 141-6, L. 151-1, L. 151-3 et L. 151-6.

Les dispositions de l'article L. 131-1 sont applicables à compter du 1er janvier 2001.
