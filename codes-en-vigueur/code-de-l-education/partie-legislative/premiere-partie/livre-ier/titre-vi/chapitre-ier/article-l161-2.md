# Article L161-2

Pour son application dans les îles Wallis et Futuna, le deuxième alinéa de l'article L. 151-3 est ainsi rédigé :

" Les établissements publics sont fondés et entretenus par l'Etat. "
