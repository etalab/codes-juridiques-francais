# Chapitre Ier : Dispositions applicables dans les îles Wallis et Futuna.

- [Article L161-1](article-l161-1.md)
- [Article L161-2](article-l161-2.md)
- [Article L161-3](article-l161-3.md)
