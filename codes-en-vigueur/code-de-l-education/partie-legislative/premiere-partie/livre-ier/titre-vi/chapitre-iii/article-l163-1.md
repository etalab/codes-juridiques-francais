# Article L163-1

Sont applicables en Polynésie française les premier, deuxième, troisième, quatrième et septième alinéas de l'article L. 111-1, les articles L. 111-1-1, L. 111-3 à L. 111-5, L. 112-2, le premier alinéa de l'article L. 113-1, les articles L. 121-1, L. 121-3, L. 121-4, la première phrase du I de l'article L. 121-4-1, les articles L. 122-1, L. 122-5, L. 123-1 à L. 123-9, L. 131-1, L. 131-1-1, L. 131-2, L. 131-4, L. 132-1,
L. 132-2,
L. 141-2, L. 141-4, L. 141-5, L. 141-6, L. 151-1, L. 151-3 et L. 151-6.

Les articles L. 111-2 et L. 121-2, ainsi que l'article L. 122-1-1, à l'exception de la dernière phrase de son premier alinéa, sont applicables en Polynésie française sans préjudice de l'exercice de leurs compétences par les autorités locales.
