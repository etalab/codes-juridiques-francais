# Chapitre IV : Dispositions applicables en Nouvelle-Calédonie.

- [Article L164-1](article-l164-1.md)
- [Article L164-2](article-l164-2.md)
- [Article L164-3](article-l164-3.md)
