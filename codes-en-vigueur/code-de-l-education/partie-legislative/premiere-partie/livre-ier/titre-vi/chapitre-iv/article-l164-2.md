# Article L164-2

Pour son application en Nouvelle-Calédonie, le deuxième alinéa de l'article L. 151-3 est ainsi rédigé :

" Les établissements publics sont fondés par l'Etat, la Nouvelle-Calédonie, les provinces ou les communes. "
