# Article L491-1

Sont applicables dans les îles Wallis et Futuna les articles L. 401-1, L. 401-2, L. 401-2-1, L. 411-1 et L. 411-3, L. 421-7 à L. 421-10 et L. 423-1 à L. 423-3.
