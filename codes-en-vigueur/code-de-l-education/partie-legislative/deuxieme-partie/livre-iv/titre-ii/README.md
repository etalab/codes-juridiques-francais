# Titre II : Les collèges et les lycées

- [Chapitre Ier : Organisation et fonctionnement des établissements publics locaux d'enseignement.](chapitre-ier)
- [Chapitre II : Organisation et fonctionnement des collèges et des lycées ne constituant pas des établissements publics locaux d'enseignement](chapitre-ii)
- [Chapitre III : Les groupements d'établissements scolaires publics.](chapitre-iii)
- [Chapitre IV : Les écoles de métiers.](chapitre-iv)
