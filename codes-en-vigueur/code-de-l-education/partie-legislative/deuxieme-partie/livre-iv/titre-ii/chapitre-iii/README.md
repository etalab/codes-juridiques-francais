# Chapitre III : Les groupements d'établissements scolaires publics.

- [Article L423-1](article-l423-1.md)
- [Article L423-3](article-l423-3.md)
