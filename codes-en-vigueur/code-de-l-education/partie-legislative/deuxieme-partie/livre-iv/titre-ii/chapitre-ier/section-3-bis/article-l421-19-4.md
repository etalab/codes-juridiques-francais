# Article L421-19-4

Le conseil d'administration de l'Ecole européenne de Strasbourg exerce les compétences du conseil d'administration mentionné à l'article L. 421-4 ainsi que les compétences du conseil d'école mentionné à l'article L. 411-1.
