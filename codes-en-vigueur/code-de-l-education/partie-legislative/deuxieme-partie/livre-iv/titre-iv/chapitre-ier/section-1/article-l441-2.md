# Article L441-2

Le demandeur adresse la déclaration mentionnée à l'article L. 441-1 au représentant de l'Etat dans le département, à l'autorité de l'Etat compétente en matière d'éducation et au procureur de la République ; il y joint en outre, pour l'autorité de l'Etat compétente en matière d'éducation, son acte de naissance, ses diplômes, l'extrait de son casier judiciaire, l'indication des lieux où il a résidé et des professions qu'il a exercées pendant les dix années précédentes, le plan des locaux affectés à l'établissement et, s'il appartient à une association, une copie des statuts de cette association.

L'autorité de l'Etat compétente en matière d'éducation, soit d'office, soit sur la requête du procureur de la République, peut former opposition à l'ouverture d'une école privée, dans l'intérêt des bonnes moeurs ou de l'hygiène.

Si le demandeur est un instituteur public révoqué désireux de s'installer dans la commune où il exerçait, l'opposition peut être faite dans l'intérêt de l'ordre public.

A défaut d'opposition, l'école est ouverte à l'expiration d'un délai d'un mois à compter du dépôt de la déclaration d'ouverture, sans aucune formalité.
