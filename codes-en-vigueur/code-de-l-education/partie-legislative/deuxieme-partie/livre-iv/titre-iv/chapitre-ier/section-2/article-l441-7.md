# Article L441-7

Pendant le mois qui suit le dépôt des pièces requises par l'article L. 441-5, le recteur, le représentant de l'Etat dans le département et le procureur de la République peuvent s'opposer à l'ouverture de l'établissement, dans l'intérêt des bonnes moeurs ou de l'hygiène. Après ce délai, s'il n'est intervenu aucune opposition, l'établissement peut être immédiatement ouvert.
