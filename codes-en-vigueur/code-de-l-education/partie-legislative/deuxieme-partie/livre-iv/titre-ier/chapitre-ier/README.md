# Chapitre Ier : Organisation et fonctionnement des écoles maternelles et élémentaires.

- [Article L411-1](article-l411-1.md)
- [Article L411-3](article-l411-3.md)
