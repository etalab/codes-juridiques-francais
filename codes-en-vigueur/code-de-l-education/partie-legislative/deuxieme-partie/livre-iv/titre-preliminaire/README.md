# Titre préliminaire : Dispositions communes.

- [Article L401-1](article-l401-1.md)
- [Article L401-2](article-l401-2.md)
- [Article L401-2-1](article-l401-2-1.md)
- [Article L401-3](article-l401-3.md)
- [Article L401-4](article-l401-4.md)
