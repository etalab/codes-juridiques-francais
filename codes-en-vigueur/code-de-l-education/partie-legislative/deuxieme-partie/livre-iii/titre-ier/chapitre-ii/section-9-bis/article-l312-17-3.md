# Article L312-17-3

Une information et une éducation à l'alimentation, cohérentes avec les orientations du programme national relatif à la nutrition et à la santé mentionné à l'article L. 3231-1 du code de la santé publique et du programme national pour l'alimentation mentionné à l'article L. 1 du code rural et de la pêche maritime, sont dispensées dans les écoles, dans le cadre des enseignements ou du projet éducatif territorial mentionné à l'article L. 551-1 du présent code.
