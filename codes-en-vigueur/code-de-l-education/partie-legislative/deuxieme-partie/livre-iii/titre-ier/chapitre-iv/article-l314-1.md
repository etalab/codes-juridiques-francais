# Article L314-1

Les expériences de recherche pédagogique peuvent se dérouler dans des établissements publics ou privés selon des conditions dérogatoires précisées par décret.
