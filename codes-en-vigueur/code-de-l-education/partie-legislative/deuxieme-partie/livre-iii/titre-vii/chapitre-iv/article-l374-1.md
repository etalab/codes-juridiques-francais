# Article L374-1

Sont applicables en Nouvelle-Calédonie la seconde phrase de l'article L. 311-4, les articles L. 312-7,
L. 312-12, L. 312-13-1, L. 312-15, L. 312-19, L. 313-1 à L. 313-3, L. 331-1 à L. 331-4, L. 331-8,
L. 332-2, L. 332-5, les premier, deuxième et quatrième alinéas de l'article L. 332-6, les articles L. 333-1 à L. 333-4, L. 334-1, L. 335-3 à L. 335-5, les deux premiers alinéas de l'article L. 335-6, les articles L. 335-9 à L. 335-11, L. 335-14 à L. 335-16, L. 336-1, L. 336-2 et L. 337-1.

Le troisième alinéa de l'article L. 332-6 est applicable en Nouvelle-Calédonie sans préjudice de l'exercice de leurs compétences par les autorités locales.
