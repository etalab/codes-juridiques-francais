# Chapitre II : Dispositions applicables à Mayotte.

- [Article L372-1](article-l372-1.md)
- [Article L372-1-1](article-l372-1-1.md)
- [Article L372-2](article-l372-2.md)
