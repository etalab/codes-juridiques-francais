# Article L373-3

Les références à des dispositions législatives ne s'appliquant pas en Polynésie française sont remplacées par les références aux dispositions, ayant le même objet, qui y sont applicables.
