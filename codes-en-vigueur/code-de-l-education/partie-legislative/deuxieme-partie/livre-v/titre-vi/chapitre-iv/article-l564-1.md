# Article L564-1

Sont applicables en Nouvelle-Calédonie les articles L. 511-1 à L. 511-4,
L. 533-1, L. 542-1 et L. 542-3.
