# Article L552-3

Les associations visées à l'article L. 552-2 sont affiliées à des fédérations ou à des unions sportives scolaires et universitaires. Les statuts de ces unions et fédérations sont approuvés par décret en Conseil d'Etat.
