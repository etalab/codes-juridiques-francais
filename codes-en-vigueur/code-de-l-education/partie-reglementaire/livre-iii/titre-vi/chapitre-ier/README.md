# Chapitre Ier : Les formations dispensées dans les établissements d'enseignement artistique.

- [Section 1 : Dispositions générales.](section-1)
- [Section 2 : L'enseignement du théâtre.](section-2)
- [Section 3 : Le cycle d'enseignement professionnel initial et les diplômes nationaux d'orientation professionnelle de musique, de danse et d'art dramatique](section-3)
