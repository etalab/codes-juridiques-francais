# Article D338-37

Un examen unique est organisé à chaque session pour l'ensemble des niveaux de chaque spécialité du diplôme de compétence en langue.
