# Sous-section 3 : Organisation de l'examen

- [Article D338-39](article-d338-39.md)
- [Article D338-40](article-d338-40.md)
- [Article D338-41](article-d338-41.md)
- [Article D338-42](article-d338-42.md)
