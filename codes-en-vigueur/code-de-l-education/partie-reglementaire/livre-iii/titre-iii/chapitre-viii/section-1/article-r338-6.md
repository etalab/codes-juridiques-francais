# Article R338-6

Le jury du titre professionnel et des certificats complémentaires est désigné par le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi. Il est composé de professionnels du secteur d'activité concerné par le titre.

Se prononcent sur l'obtention des certificats de compétences mentionnés à l'article R. 338-3 un des professionnels membres du jury ainsi qu'un formateur du secteur d'activité concerné, à l'exception de celui ayant assuré directement la préparation ou la formation du candidat.

Les membres salariés des jurys prévus au présent article bénéficient des dispositions prévues aux articles L. 3142-3 à L. 3142-6 du code du travail.
