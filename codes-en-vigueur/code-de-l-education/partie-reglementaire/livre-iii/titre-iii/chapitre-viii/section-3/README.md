# Section 3 : Diplôme initial de langue française.

- [Article D338-23](article-d338-23.md)
- [Article D338-24](article-d338-24.md)
- [Article D338-25](article-d338-25.md)
- [Article D338-26](article-d338-26.md)
- [Article D338-27](article-d338-27.md)
- [Article D338-28](article-d338-28.md)
- [Article D338-29](article-d338-29.md)
- [Article D338-30](article-d338-30.md)
- [Article D338-31](article-d338-31.md)
- [Article D338-32](article-d338-32.md)
