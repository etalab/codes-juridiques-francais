# Section 3 : Le certificat de formation générale.

- [Article D332-23](article-d332-23.md)
- [Article D332-24](article-d332-24.md)
- [Article D332-25](article-d332-25.md)
- [Article D332-26](article-d332-26.md)
- [Article D332-27](article-d332-27.md)
- [Article D332-29](article-d332-29.md)
