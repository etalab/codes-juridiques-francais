# Article D332-17

Pour les candidats scolaires issus des classes de troisième des établissements d'enseignement publics ou privés sous contrat et pour les candidats ayant préparé le diplôme national du brevet par la voie de la formation professionnelle continue dans un établissement public, le diplôme est attribué, dans des conditions fixées par un arrêté du ministre chargé de l'éducation nationale, sur la base des notes obtenues à un examen, des résultats acquis en cours de formation et l'évaluation des compétences du socle commun défini à l'article D. 122-1.

Les modalités d'attribution du diplôme national du brevet sont adaptées afin de tenir compte de la spécificité des formations dispensées à certains candidats, dans les conditions définies par un arrêté du ministre chargé de l'éducation.
