# Sous-section 1 : Conditions de délivrance.

- [Article D336-4](article-d336-4.md)
- [Article D336-5](article-d336-5.md)
- [Article D336-6](article-d336-6.md)
- [Article D336-7](article-d336-7.md)
- [Article D336-8](article-d336-8.md)
- [Article D336-9](article-d336-9.md)
- [Article D336-10](article-d336-10.md)
- [Article D336-11](article-d336-11.md)
- [Article D336-12](article-d336-12.md)
- [Article D336-13](article-d336-13.md)
- [Article D336-14](article-d336-14.md)
