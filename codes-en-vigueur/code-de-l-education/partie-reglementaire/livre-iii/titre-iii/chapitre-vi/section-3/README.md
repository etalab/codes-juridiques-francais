# Section 3 : Dispositions particulières au baccalauréat technologique série " techniques de la musique et de la danse ".

- [Article D336-39](article-d336-39.md)
- [Article D336-39-1](article-d336-39-1.md)
- [Article D336-40](article-d336-40.md)
- [Article D336-41](article-d336-41.md)
- [Article D336-42](article-d336-42.md)
- [Article D336-43](article-d336-43.md)
- [Article D336-44](article-d336-44.md)
- [Article D336-45](article-d336-45.md)
- [Article D336-46](article-d336-46.md)
- [Article D336-46-1](article-d336-46-1.md)
- [Article D336-47](article-d336-47.md)
- [Article D336-48](article-d336-48.md)
