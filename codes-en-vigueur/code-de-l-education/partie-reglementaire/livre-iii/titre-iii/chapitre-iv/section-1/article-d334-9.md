# Article D334-9

Au cours de la session d'examen organisée à la fin de l'année scolaire, les membres du jury ne peuvent pas examiner leurs élèves de l'année en cours.

Les épreuves écrites sont corrigées sous couvert de l'anonymat. Les noms des candidats sont portés à la connaissance du jury au moment de la délibération.
