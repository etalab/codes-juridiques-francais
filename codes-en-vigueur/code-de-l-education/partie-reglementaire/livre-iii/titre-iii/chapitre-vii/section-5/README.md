# Section 5 : Le diplôme national du brevet des métiers d'art.

- [Sous-section 1 : Définition du diplôme](sous-section-1)
- [Sous-section 2 : Modalités de préparation](sous-section-2)
- [Sous-section 3 : Conditions de délivrance](sous-section-3)
- [Sous-section 4 : Organisation de l'examen](sous-section-4)
