# Article D337-40

Pour les candidats autres que ceux relevant des articles D. 337-38 et D. 337-39, l'examen a lieu en totalité sous forme d'épreuves ponctuelles terminales.
