# Article D337-42

Les sessions d'examen du brevet d'études professionnelles sont organisées par le recteur dans le cadre de l'académie, ou peuvent l'être dans un cadre interacadémique, sous l'autorité des recteurs intéressés.
