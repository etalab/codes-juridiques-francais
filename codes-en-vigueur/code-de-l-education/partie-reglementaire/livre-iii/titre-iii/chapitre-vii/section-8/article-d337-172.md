# Article D337-172

Les formations en alternance, sous statut scolaire, en centre de formation d'apprentis, prévues par l'article L. 337-3-1, concernent les élèves ayant au moins atteint l'âge de 15 ans à la date d'entrée dans la formation. Elles sont dénommées " dispositif d'initiation aux métiers en alternance ” et sont destinées à faire découvrir un environnement professionnel correspondant à un projet d'entrée en apprentissage.
