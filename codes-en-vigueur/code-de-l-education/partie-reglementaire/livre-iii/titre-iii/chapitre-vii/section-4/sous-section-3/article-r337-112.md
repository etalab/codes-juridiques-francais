# Article R337-112

Les habilitations prévues aux premier et troisième alinéas de l'article D. 337-111 sont réputées acquises si, dans un délai de trois mois, aucune décision de refus n'a été notifiée aux intéressés. Les conditions relatives à l'octroi et au retrait de ces habilitations sont précisées par arrêté du ministre chargé de l'éducation.
