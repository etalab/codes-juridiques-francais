# Article D337-146

La durée des périodes de formation en milieu professionnel est comprise entre douze et dix-huit semaines. L'organisation et la durée de ces périodes sont précisées par chaque arrêté de spécialité.

Cette durée de formation peut être réduite dans les conditions fixées par chaque arrêté de spécialité ou par une décision de positionnement prise par le recteur après avis de l'équipe pédagogique.

Pour les candidats préparant l'examen par la voie scolaire, la durée des périodes de formation en milieu professionnel ne peut être inférieure à huit semaines.
