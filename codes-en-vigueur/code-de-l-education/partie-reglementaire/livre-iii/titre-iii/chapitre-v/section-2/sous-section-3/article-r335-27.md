# Article R335-27

La Commission nationale de la certification professionnelle délibère à la majorité des membres présents. En cas de partage égal des voix, le président a voix prépondérante.

La commission établit un règlement intérieur.

La Commission nationale de la certification professionnelle se réunit au moins quatre fois par an.
