# Article R335-19

La Commission nationale de la certification professionnelle est tenue informée par les ministres compétents de l'actualisation ou de la suppression des diplômes et titres enregistrés de droit en application des dispositions de l'article R. 335-16.

Pour les autres certifications, la première demande d'enregistrement ainsi que la demande de renouvellement ou de suppression d'enregistrement peuvent être déposées par l'autorité, l'organisme ou l'instance qui les délivre, soit auprès de la Commission nationale de la certification professionnelle, puis auprès du ministre compétent pour le champ professionnel des activités concernées par la certification, soit auprès du ministre chargé de la formation professionnelle. S'il s'agit d'un organisme à vocation régionale, la demande est déposée auprès du préfet de région.

Lorsque la demande s'exprime au niveau régional, le préfet de région communique le dossier au correspondant de la commission nationale pour la région prévu à l'article R. 335-29. Ce dernier instruit la demande avec le concours des services déconcentrés de l'Etat dans la région et rapporte devant la commission spécialisée du comité régional de l'emploi, de la formation de l'orientation professionnelles. La commission spécialisée se prononce dans un délai de trois mois à compter de la date de transmission du dossier par le préfet de région. A défaut de réponse dans ce délai, l'avis de la commission est réputé rendu.

Le correspondant de la commission nationale pour la région transmet le dossier de l'organisme, accompagné de son rapport et de l'avis du comité régional de l'emploi, de la formation de l'orientation professionnelles, au président de la commission.

Lorsque la demande s'exprime au niveau national, le dossier est instruit par la commission nationale.

Dans les deux cas, le président de la commission nationale peut désigner un expert pour compléter l'information de la commission.

Le président de la Commission nationale de la certification professionnelle transmet à l'issue de chaque réunion trimestrielle de la commission au ministre chargé de la formation professionnelle les avis de la commission afin qu'il puisse prendre l'arrêté mentionné à l'article R. * 335-20.
