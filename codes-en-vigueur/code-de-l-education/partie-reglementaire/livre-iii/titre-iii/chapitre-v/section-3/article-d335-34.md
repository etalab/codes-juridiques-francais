# Article D335-34

Le recteur d'académie et le président du conseil régional proposent conjointement à la labellisation des projets de campus des métiers et des qualifications, après consultation du conseil académique de l'éducation nationale et du comité régional de l'emploi, de la formation et de l'orientation professionnelles.

Le label " campus des métiers et des qualifications " est attribué après l'examen des projets de campus par un groupe d'experts et l'avis du conseil national éducation économie, au regard des dispositions de l'article D. 335-33 et du projet pédagogique, liant formation, recherche et développement économique.

Il est délivré, pour une durée de quatre ans, par arrêté conjoint des ministres chargés de l'éducation nationale, de la formation professionnelle, de l'enseignement supérieur et de l'économie. Cet arrêté fixe la liste des campus des métiers et des qualifications et précise l'intitulé de chacun. Cet intitulé doit comporter le secteur d'activité concerné ainsi que, le cas échéant, la mention de la dimension internationale des formations.

Le label peut être renouvelé dans les conditions définies aux alinéas précédents.
