# Article R335-49

Doivent comporter un enseignement à l'accessibilité aux personnes handicapées du cadre bâti, lorsqu'ils relèvent de l'article R. 335-48, les formations conduisant :

-aux diplômes et titres délivrés par l'Etat ou en son nom, acquis conformément aux dispositions de l'article L. 335-5, mentionnés au I de l'article L. 335-6, et enregistrés de droit au répertoire national des certifications professionnelles en vertu du deuxième alinéa du II de l'article L. 335-6 ;

-aux autres diplômes, titres et certifications enregistrés au répertoire national des certifications professionnelles selon les modalités définies au premier alinéa du II de l'article L. 335-6.
