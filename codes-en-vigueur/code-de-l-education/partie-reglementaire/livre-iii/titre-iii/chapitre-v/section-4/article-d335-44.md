# Article D335-44

Il est procédé à la nomination des conseillers de l'enseignement technologique par arrêté du recteur d'académie, pris après avis du préfet de département du domicile des intéressés.

Cet arrêté précise l'étendue de la mission de chaque conseiller qui peut, si nécessaire, être modifiée en cours de mandat.
