# Article D335-47

Les fonctions de conseiller de l'enseignement technologique sont gratuites. Elles donnent toutefois lieu à paiement d'indemnités pour frais de déplacement et, éventuellement, d'indemnités compensatrices de perte de salaires, dans les conditions fixées conjointement par le ministre chargé de l'éducation et le ministre chargé du budget.
