# Article D335-43

Les conseillers de l'enseignement technologique sont répartis en autant de groupes qu'il existe de commissions professionnelles consultatives.

Des arrêtés du ministre chargé de l'éducation, pris sur proposition du recteur après avis du comité régional de l'emploi, de la formation de l'orientation professionnelles, fixent le nombre des conseillers à désigner dans chaque académie et leur répartition par département, dans chacun des groupes ci-dessus, en respectant dans toute la mesure du possible la parité entre employeurs et salariés.
