# Article R342-8

Les titres de formation professionnelle maritime sont définis dans les décrets n° 93-1342 du 28 décembre 1993 relatif aux conditions d'exercice des fonctions de capitaine et d'officier à bord des navires de commerce, de pêche et de plaisance, n° 99-439 du 25 mai 1999 précité, n° 2003-169 du 28 février 2003 portant création du brevet d'officier électronicien et systèmes de la marine marchande et n° 2007-1377 du 21 septembre 2007 portant diverses dispositions relatives aux titres de formation professionnelle maritime.
