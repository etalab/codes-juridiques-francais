# Article D341-44

Le jury départemental défini par l'article D. 332-19 s'adjoint des enseignants des établissements publics d'enseignement agricole.
