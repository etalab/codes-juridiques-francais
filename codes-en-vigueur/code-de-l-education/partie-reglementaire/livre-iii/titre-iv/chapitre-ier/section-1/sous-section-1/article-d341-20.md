# Article D341-20

Tout élève ayant échoué à l'examen du brevet de technicien supérieur agricole, du brevet de technicien agricole, du baccalauréat, du brevet d'études professionnelles agricoles ou du certificat d'aptitude professionnelle agricole se voit offrir le droit d'une nouvelle préparation de cet examen, le cas échéant, selon des modalités adaptées au niveau des connaissances qu'il a acquises dans les matières d'enseignement correspondant aux épreuves de l'examen.

Pour la classe terminale de chaque cycle, ce droit s'exerce dans la limite des places demeurées vacantes après l'admission des élèves issus de la classe précédente de l'établissement scolaire et peut entraîner un changement d'établissement après qu'ont été explorées toutes les possibilités d'un maintien sur place de l'élève.

Le changement éventuel d'établissement scolaire relève de la compétence du         directeur régional de l'alimentation, de l'agriculture et de la forêt.
