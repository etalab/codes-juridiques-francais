# Article D351-32

Les 3° et 4° de l'article D. 351-27 entrent en vigueur à compter de la rentrée scolaire 2006 pour les examens et concours ne comportant pas, au 1er janvier 2006, de dispositifs équivalents.
