# Article D311-7

Le livret personnel de compétences comporte :

1° La mention de la validation du socle commun de connaissances, de compétences et de culture pour chacun des paliers :

-à la fin du cycle des apprentissages fondamentaux pour ce qui relève de la maîtrise de la langue française, des principaux éléments de mathématiques et des compétences sociales et civiques ;

-à la fin de l'école primaire et à la fin du collège ou de la scolarité obligatoire pour chacune des sept compétences du socle commun de connaissances, de compétences et de culture ;

2° Les attestations mentionnées sur une liste définie par arrêté du ministre chargé de l'éducation nationale.
