# Article D313-5

Au niveau départemental,               le directeur académique des services de l'éducation nationale agissant sur délégation du recteur d'académie, assure la responsabilité des activités d'information et d'orientation.
