# Article D312-9

Outre le ministre chargé de la culture et le ministre chargé de l'éducation, présidents, le Haut Conseil de l'éducation artistique et culturelle comprend vingt-quatre  membres, soit :

1° Huit  représentants de l'Etat :

a) Deux représentants du ministre chargé de la culture, dont un directeur régional des affaires culturelles ;

b) Deux représentants du ministre chargé de l'éducation, dont un recteur d'académie ;

c) Un représentant du ministre chargé de la jeunesse ;

d) Un représentant du ministre chargé de l'agriculture ;

e) Un représentant du ministre chargé de l'enseignement supérieur ;

f) Un représentant du ministre chargé de la ville.

2° Huit  représentants des collectivités territoriales, dont :

a) Deux représentants de  l'Association des maires de France ;

b) Deux représentants de  l'Assemblée des départements de France ;

c) Deux représentants de  l'Association des régions de France ;

d) Un représentant de la Fédération nationale des collectivités territoriales pour la culture ;

e) Un représentant du Réseau français des villes éducatrices.

3° Huit personnalités désignées en raison de leurs compétences, dont :

a) Six personnalités issues du monde de l'éducation ou de la culture ;

b) Deux représentants des parents d'élèves.
