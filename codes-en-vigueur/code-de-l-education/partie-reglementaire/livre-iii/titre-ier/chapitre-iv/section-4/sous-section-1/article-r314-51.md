# Article R314-51

Le Centre international d'études pédagogiques, établissement public à caractère administratif placé sous la tutelle du ministre chargé de l'éducation, est constitué d'un service central dont le siège est à Sèvres et d'un centre local à la Réunion.
