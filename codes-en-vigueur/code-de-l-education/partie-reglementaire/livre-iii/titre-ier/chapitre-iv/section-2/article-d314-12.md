# Article D314-12

Suivant la nature des actions menées dans ce domaine, les établissements intéressés sont classés en deux catégories :

1° Etablissements privés expérimentaux de plein exercice ;

2° Etablissements privés chargés d'expérimentation.
