# Article D718-5

Les établissements ou organismes concourant aux missions du service public de l'enseignement supérieur et de la recherche, dont la liste figure au présent article, sont associés à un ou plusieurs établissements publics à caractère scientifique, culturel et professionnel en application de l'article L. 718-16 :

1° L'Ecole nationale d'ingénieurs de Metz à l'université de Lorraine par le décret n° 2014-1529 du 17 décembre 2014 portant association de l'Ecole nationale d'ingénieurs de Metz à l'université de Lorraine ;

2° L'institut d'administration des entreprises de Paris à l'université Paris-I par le décret n° 2014-1549 du 19 décembre 2014 portant association de l'institut d'administration des entreprises de Paris à l'université Paris-I.
