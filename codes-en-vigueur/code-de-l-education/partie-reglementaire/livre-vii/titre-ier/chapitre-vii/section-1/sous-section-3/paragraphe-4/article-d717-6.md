# Article D717-6

Les dispositions relatives à l'Ecole nationale supérieure maritime, grand établissement placé sous la tutelle du ministre chargé de la mer, sont fixées par le décret n° 2010-1129 du 28 septembre 2010 portant création de l'Ecole nationale supérieure maritime.
