# Chapitre VII : Les grands établissements

- [Section 1 : Catégories de grands établissements](section-1)
- [Section 2 : Gouvernance](section-2)
