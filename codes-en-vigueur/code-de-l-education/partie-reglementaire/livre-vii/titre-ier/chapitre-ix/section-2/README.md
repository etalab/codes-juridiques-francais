# Section 2 : Régime financier

- [Sous-section 1 : Droits d'inscription](sous-section-1)
- [Sous-section 2 : Budget et régime financier des établissements publics à caractère scientifique, culturel et professionnel](sous-section-2)
- [Sous-section 4 : Rémunération des services de formation proposés dans le cadre de leur mission de coopération internationale par les établissements publics d'enseignement supérieur](sous-section-4)
