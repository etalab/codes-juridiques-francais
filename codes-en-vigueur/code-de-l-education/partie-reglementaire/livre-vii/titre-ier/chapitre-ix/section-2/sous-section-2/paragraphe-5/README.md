# Paragraphe 5 : Pilotage et performance

- [Sous-paragraphe 1 : Audit interne et pilotage financier et patrimonial](sous-paragraphe-1)
- [Sous-paragraphe 2 : Contrôle budgétaire](sous-paragraphe-2)
