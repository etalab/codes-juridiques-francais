# Sous-paragraphe 2 : Modifications du budget en cours d'exercice

- [Article R719-73](article-r719-73.md)
- [Article R719-74](article-r719-74.md)
