# Section 6 : La délivrance de titres constitutifs de droits réels  par les établissements publics d'enseignement supérieur

- [Article R719-206](article-r719-206.md)
- [Article R719-207](article-r719-207.md)
- [Article R719-208](article-r719-208.md)
