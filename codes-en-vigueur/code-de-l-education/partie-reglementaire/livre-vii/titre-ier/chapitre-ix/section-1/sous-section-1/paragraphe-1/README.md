# Paragraphe 1 : Composition des collèges électoraux

- [Sous-paragraphe 1 : Composition des collèges électoraux pour l'élection des membres des conseils d'unités de formation et de recherche et des membres des conseils des instituts et écoles internes](sous-paragraphe-1)
- [Sous-paragraphe 2 : Composition des collèges électoraux pour l'élection des membres du conseil d'administration](sous-paragraphe-2)
- [Sous-paragraphe 3 : Composition des collèges électoraux pour l'élection  des membres  du conseil académique ou des membres du conseil scientifique et du conseil des études et de la vie universitaire ou des organes en tenant lieu](sous-paragraphe-3)
- [Article D719-1](article-d719-1.md)
- [Article D719-2](article-d719-2.md)
- [Article D719-3](article-d719-3.md)
