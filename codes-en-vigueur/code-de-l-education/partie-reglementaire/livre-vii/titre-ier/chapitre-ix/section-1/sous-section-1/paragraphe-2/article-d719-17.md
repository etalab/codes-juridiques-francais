# Article D719-17

Les électeurs qui ne peuvent voter personnellement ont la possibilité d'exercer leur droit de vote par un mandataire, en lui donnant procuration écrite pour voter en leur lieu et place.

Le mandataire doit être inscrit sur la même liste électorale que le mandant. Nul ne peut être porteur de plus de deux mandats. Le mandataire doit présenter selon le cas soit la carte d'étudiant, soit la justification de la qualité professionnelle de son mandat.
