# Article D719-41

Les articles D. 719-42 à D. 719-47 fixent les modalités de désignation des personnalités extérieures visées à l'article L. 719-3, sous réserve de dispositions réglementaires particulières.
