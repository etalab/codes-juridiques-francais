# Article D719-46

Les collectivités territoriales, institutions et organismes, figurant sur la liste établie conformément aux dispositions du 3° de l'article D. 719-42 désignent nommément la ou les personnes qui les représentent ainsi que les suppléants appelés à les remplacer en cas d'empêchement.

Les représentants titulaires des collectivités territoriales doivent être membres de leurs organes délibérants.

Lorsque ces personnes perdent la qualité au titre de laquelle elles ont été appelées à représenter ces institutions ou organismes, ceux-ci désignent de nouveaux représentants.
