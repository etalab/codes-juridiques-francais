# Article D719-191

Les instituts d'études politiques ont pour missions :

1° De contribuer, tant en formation initiale qu'en formation continue, à la formation des cadres supérieurs des secteurs public, parapublic et privé de la nation, et notamment des fonctions publiques de l'Etat et des collectivités territoriales ;

2° De développer, notamment en relation avec les établissements d'enseignement supérieur, la Fondation nationale des sciences politiques et le Centre national de la recherche scientifique, la recherche en sciences politique et administrative.

A cet effet ils délivrent un diplôme propre. Ils peuvent également participer à la préparation de diplômes nationaux et de diplômes d'université.

Lorsque les instituts d'études politiques ont un statut d'établissement public administratif rattaché, les conditions de cette participation sont prévues par convention avec leur université de rattachement.
