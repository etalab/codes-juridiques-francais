# Article R712-17

Le président de l'université ne peut être membre d'une section disciplinaire.
