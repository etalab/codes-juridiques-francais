# Article R712-19

Quand les membres élus du conseil académique appartenant au collège des usagers, défini au 4° de l'article R. 712-14, sont en nombre inférieur ou égal au nombre de sièges à pourvoir pour chaque sexe, ils sont d'office membres de la section disciplinaire. L'ordre dans lequel ils sont appelés à siéger dans les formations de jugement est alors déterminé par tirage au sort effectué au moment de leur désignation, respectivement pour les femmes et pour les hommes.

Lorsque, après application des dispositions de l'alinéa précédent, l'effectif du collège des usagers de la section disciplinaire est incomplet pour un sexe, les représentants élus des usagers au conseil académique élisent au scrutin majoritaire à deux tours les membres appelés à compléter la section disciplinaire parmi les usagers de ce sexe inscrits dans l'établissement.

Lorsque, après application des dispositions prévues aux alinéas précédents, l'effectif du collège des usagers de la section disciplinaire est incomplet pour un sexe, les représentants élus des usagers au conseil académique élisent au scrutin majoritaire à deux tours les membres appelés à compléter la section disciplinaire parmi les représentants élus des usagers de ce sexe au conseil académique d'autres établissements publics d'enseignement supérieur.
