# Article R712-12

Les usagers mentionnés au c du 2° de l'article R. 712-10 relèvent de la section disciplinaire de l'un des établissements publics d'enseignement supérieur placés sous la tutelle du ministère chargé de l'enseignement supérieur dont le siège est situé dans le ressort de l'académie où la fraude ou la tentative de fraude a été commise. Cet établissement est désigné chaque année par le recteur d'académie.
