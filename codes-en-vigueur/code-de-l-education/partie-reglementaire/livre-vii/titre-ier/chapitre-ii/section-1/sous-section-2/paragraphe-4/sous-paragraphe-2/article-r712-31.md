# Article R712-31

Dès réception du document mentionné à l'article R. 712-30 et des pièces jointes, le président de la section disciplinaire en transmet copie par lettre recommandée, avec demande d'avis de réception, à chacune des personnes poursuivies ainsi qu'au président ou au directeur de l'établissement, au recteur d'académie et au médiateur de la République. S'il s'agit de mineurs, copie est en outre adressée aux personnes qui exercent à leur égard l'autorité parentale ou la tutelle.

Le président fait savoir aux intéressés qu'ils peuvent se faire assister d'un conseil de leur choix et qu'ils peuvent prendre connaissance du dossier pendant le déroulement de l'instruction.
