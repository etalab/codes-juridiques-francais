# Article R712-10

Relèvent du régime disciplinaire prévu aux articles R. 712-9 à R. 712-46 :

1° Les enseignants-chercheurs et les personnels exerçant des fonctions d'enseignement dans l'université, à l'exception des membres du personnel médical et scientifique des centres hospitaliers et universitaires, soumis aux dispositions des articles L. 952-21 et L. 952-22 ;

2° Tout usager de l'université lorsqu'il est auteur ou complice, notamment :

a) D'une fraude ou d'une tentative de fraude commise à l'occasion d'une inscription, d'une épreuve de contrôle continu, d'un examen ou d'un concours ;

b) D'un fait de nature à porter atteinte à l'ordre ou au bon fonctionnement de l'université ;

c) D'une fraude ou d'une tentative de fraude commise à l'occasion d'une inscription dans un établissement d'enseignement supérieur privé lorsque cette inscription ouvre l'accès à un examen de l'enseignement supérieur public ou d'une fraude ou tentative de fraude commise dans cette catégorie d'établissement ou dans une université, à l'occasion d'un examen conduisant à l'obtention d'un diplôme national.
