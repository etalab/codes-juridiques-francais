# Article D715-10

Les dispositions relatives aux autres écoles ou instituts extérieurs au sens de l'article L. 715-1 sont les suivantes :

1° Institut supérieur de mécanique de Paris : décret n° 90-928 du 10 octobre 1990 relatif à l'Institut supérieur de mécanique de Paris ;

2° Ecole nationale supérieure des arts et industries textiles : décret n° 2003-1089 du 13 novembre 2003 relatif à l'Ecole nationale supérieure des arts et industries textiles ;

3° Ecole nationale d'ingénieurs de Saint-Etienne : décret n° 2009-1513 du 7 décembre 2009 relatif à l'Ecole nationale d'ingénieurs de Saint-Etienne.
