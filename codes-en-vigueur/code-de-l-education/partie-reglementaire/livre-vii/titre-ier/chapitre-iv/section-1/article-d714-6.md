# Article D714-6

La création, par délibération statutaire d'un service commun interuniversitaire d'accueil, d'orientation et d'insertion professionnelle des étudiants, est décidée par les conseils d'administration des établissements concernés, après avis du conseil des études et de la vie universitaire.

La mise en place du service est subordonnée à la conclusion d'une convention passée entre les établissements.

Le service est dirigé par un directeur qui doit appartenir à un corps d'enseignants-chercheurs et être en exercice dans l'un des établissements parties à la convention.

Ce service exerce notamment tout ou partie des missions énumérées à l'article D. 714-2, à l'exception de l'élaboration du rapport visé au 4° de cet article, lequel est de la responsabilité de chaque établissement.

Une convention peut être conclue entre l'Etat et l'établissement de rattachement pour attribuer au service commun des moyens spécifiques en crédits de fonctionnement et en emplois destinés à permettre l'accomplissement de ses missions.

A la demande des universités concernées et sur décision du recteur, des conseillers d'orientation peuvent, dans la limite de la moitié de leur temps de service, contribuer au fonctionnement du service commun interuniversitaire.
