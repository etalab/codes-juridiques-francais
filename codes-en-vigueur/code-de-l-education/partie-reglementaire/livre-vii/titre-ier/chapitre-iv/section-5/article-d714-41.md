# Article D714-41

Afin d'assurer dans les meilleures conditions l'organisation et l'animation des activités physiques et sportives dans l'enseignement supérieur, les universités donnent une place à ces activités dans l'organisation pédagogique générale, notamment par l'aménagement des programmes et des horaires, et procèdent, conformément aux dispositions des articles L. 714-1 et L. 714-2, à la création de services communs aux unités de formation et de recherche d'une université. En outre, lorsqu'une agglomération urbaine comporte plusieurs universités, elles créent, en vue d'une meilleure utilisation des moyens, des services communs à plusieurs universités.

Ces services prennent respectivement le nom de " service universitaire des activités physiques et sportives " et de " service interuniversitaire des activités physiques et sportives ".

Les services universitaires ou interuniversitaires des activités physiques et sportives peuvent être liés par convention avec les services universitaires des activités physiques et sportives situés dans une autre agglomération de l'académie.

Ces services ont pour mission d'établir les programmes d'activités, d'en informer les étudiants et de veiller au bon déroulement de ces activités. Dans le cas où des installations sportives sont affectées à l'université ou aux universités cocontractantes, celles-ci gèrent ces installations dont l'ouverture à d'autres utilisateurs est assurée.
