# Article D714-53

Le conseil du service élabore le budget propre du service interuniversitaire. Ce budget est proposé par le président du conseil de l'université de rattachement à l'adoption du conseil de l'université.
