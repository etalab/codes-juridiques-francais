# Article D714-52

Le directeur du service interuniversitaire, choisi parmi les professeurs d'éducation physique et sportive affectés aux universités cocontractantes, est nommé, sur proposition du conseil des sports, par le président de l'université de rattachement après accord des présidents des universités concernées. Il gère le service sous l'autorité du président de l'université de rattachement.
