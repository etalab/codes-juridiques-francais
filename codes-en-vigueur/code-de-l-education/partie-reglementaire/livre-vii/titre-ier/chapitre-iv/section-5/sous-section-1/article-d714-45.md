# Article D714-45

Le service universitaire des activités physiques et sportives est dirigé par un directeur, choisi parmi les professeurs d'éducation physique et sportive affectés à l'université ou à l'établissement.

Le directeur est nommé, sur proposition du conseil des sports, par le président de l'université ou de l'établissement. Il gère le service sous l'autorité du président de l'université.
