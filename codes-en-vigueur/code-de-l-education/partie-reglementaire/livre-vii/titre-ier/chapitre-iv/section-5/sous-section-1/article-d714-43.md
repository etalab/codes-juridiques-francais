# Article D714-43

L'organisation et les missions du service universitaire des activités physiques et sportives sont fixées par les statuts de l'université, dans le respect des dispositions de la présente section.
