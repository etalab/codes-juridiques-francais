# Section 9 : Les services d'activités industrielles et commerciales des établissements publics à caractère scientifique, culturel et professionnel

- [Sous-section 1 : Les services d'activités industrielles  et commerciales des universités](sous-section-1)
- [Sous-section 2 : Les services d'activités industrielles et commerciales communs à plusieurs établissements publics à caractère scientifique, culturel et professionnel](sous-section-2)
