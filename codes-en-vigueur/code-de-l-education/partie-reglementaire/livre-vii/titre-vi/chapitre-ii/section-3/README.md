# Section 3 : Dispositions applicables au patrimoine mobilier  des établissements publics d'enseignement supérieur

- [Article D762-20](article-d762-20.md)
- [Article R762-15](article-r762-15.md)
- [Article R762-16](article-r762-16.md)
- [Article R762-17](article-r762-17.md)
- [Article R762-18](article-r762-18.md)
- [Article R762-19](article-r762-19.md)
