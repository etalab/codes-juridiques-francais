# Article D721-4

<div align="left">Le conseil de l'école supérieure du professorat et de l'éducation et le conseil d'orientation scientifique et pédagogique comprennent autant de femmes que d'hommes dans les conditions suivantes : <br/>
<br/>Pour l'application du deuxième alinéa de l'article L. 721-3 et conformément aux dispositions de l'article L. 719-1, les listes de candidats pour l'élection au conseil de l'école sont composées alternativement d'un candidat de chaque sexe. Lorsque la répartition des sièges entre les listes, au sein de chaque collège mentionné à l'article D. 721-1, n'aboutit pas à l'élection d'un nombre égal de candidats de chaque sexe, il est procédé ainsi pour rétablir la parité : <br/>
<br/>1° Le dernier siège revenant à un candidat du sexe majoritairement représenté est attribué au candidat suivant de liste qui est déclaré élu ; cette opération est répétée, si nécessaire, avec le siège précédemment attribué à un candidat du même sexe, jusqu'à ce que la parité soit atteinte ; <br/>
<br/>2° Si un siège devant être attribué au suivant de liste en application du 1° revient simultanément à plusieurs listes ayant obtenu le même nombre de suffrages, il est procédé à un tirage au sort pour déterminer celle des listes dont le dernier élu est remplacé par le suivant de liste. <br/>
<br/>Si nécessaire, la parité entre les femmes et les hommes est rétablie au sein de chaque conseil par la désignation des personnalités prévues au d du 3° de l'article D. 721-1 pour le conseil d'école et par la désignation des personnalités extérieures prévues au 2° de l'article D. 721-3 pour le conseil d'orientation scientifique et pédagogique.<br/>
<br/>
</div>
