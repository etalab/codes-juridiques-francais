# Article D774-9

Pour l'application de l'article D. 714-11 en Nouvelle-Calédonie :

a) Le 3° est supprimé ;

b) Au 4°, les mots : " dans l'académie " sont remplacés par les mots : " en Nouvelle-Calédonie ".
