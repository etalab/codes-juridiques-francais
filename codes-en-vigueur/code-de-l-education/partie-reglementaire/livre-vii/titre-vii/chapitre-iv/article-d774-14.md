# Article D774-14

Pour l'application des articles D. 719-41 à D. 719-47 en Nouvelle-Calédonie, une personnalité extérieure ne peut être membre que du conseil d'administration ou du conseil scientifique constitués au sein de l'établissement créé en application de l'article L. 774-2.
