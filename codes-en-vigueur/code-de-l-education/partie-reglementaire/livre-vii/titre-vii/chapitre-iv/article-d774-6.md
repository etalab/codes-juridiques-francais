# Article D774-6

Pour l'application en Nouvelle-Calédonie du deuxième alinéa de l'article D. 713-5, les mots : " le préfet de la région " sont remplacés par les mots : " le représentant de l'Etat en Nouvelle-Calédonie ".
