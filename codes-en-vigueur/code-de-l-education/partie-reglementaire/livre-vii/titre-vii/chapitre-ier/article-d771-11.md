# Article D771-11

Pour l'application du 2° de l'article D. 714-74 dans les îles Wallis et Futuna, les mots : " dans le ressort de l'académie " sont remplacés par les mots : " dans le ressort de la collectivité ".
