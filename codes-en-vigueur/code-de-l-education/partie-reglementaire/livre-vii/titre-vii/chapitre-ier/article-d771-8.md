# Article D771-8

Pour l'application du 1° de l'article D. 714-2 dans les îles Wallis et Futuna, les mots : " les délégations régionales de l'ONISEP " sont remplacés par les mots : " les services de l'ONISEP ".
