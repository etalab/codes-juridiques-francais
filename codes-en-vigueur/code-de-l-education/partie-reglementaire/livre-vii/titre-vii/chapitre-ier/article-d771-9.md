# Article D771-9

Pour l'application de l'article D. 714-11 dans les îles Wallis et Futuna :

a) Le 3° est supprimé ;

b) Au 4°, les mots : " dans l'académie " sont remplacés par les mots : " dans la collectivité ".
