# Article D771-7

Pour l'application du deuxième alinéa de l'article D. 713-9 dans les îles Wallis et Futuna, les mots : " et un représentant de la région " sont remplacés par les mots : " et un représentant de l'assemblée territoriale ".
