# Article D773-12

Pour l'application du premier alinéa de l'article D. 719-38 en Polynésie française, les mots : "dans chaque académie" sont remplacés par les mots : "en Polynésie française".
