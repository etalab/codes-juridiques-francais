# Article R773-16

Pour l'application du deuxième alinéa de l'article R. 719-74 en Polynésie française, les mots : " le délai de quinze jours " sont remplacés par les mots : " le mois ".
