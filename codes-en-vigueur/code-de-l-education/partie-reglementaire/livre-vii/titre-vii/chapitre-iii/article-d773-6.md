# Article D773-6

Pour l'application en Polynésie française du deuxième alinéa de l'article D. 713-5, les mots : " le préfet de la région " sont remplacés par les mots : " le représentant de l'Etat en Polynésie française ".
