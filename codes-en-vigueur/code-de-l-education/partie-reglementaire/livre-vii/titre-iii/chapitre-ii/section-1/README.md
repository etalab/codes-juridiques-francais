# Section 1 : La qualification d'établissement d'enseignement supérieur privé d'intérêt général

- [Article D732-3](article-d732-3.md)
- [Article D732-4](article-d732-4.md)
- [Article R732-1](article-r732-1.md)
- [Article R732-2](article-r732-2.md)
