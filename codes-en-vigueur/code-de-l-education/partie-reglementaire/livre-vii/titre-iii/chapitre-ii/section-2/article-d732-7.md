# Article D732-7

Le comité consultatif pour l'enseignement supérieur privé se réunit au moins trois fois par an sur convocation de son président. Il se réunit également sur demande d'au moins la moitié de ses membres ou du ministre chargé de l'enseignement supérieur.

Le secrétariat du comité est assuré par les services du ministre chargé de l'enseignement supérieur.

Les autres modalités de fonctionnement du comité consultatif pour l'enseignement supérieur privé sont celles fixées par le décret n° 2006-672 du 8 juin 2006 relatif à la création, à la composition et au fonctionnement de commissions administratives à caractère consultatif.
