# Chapitre II : Rapports entre l'Etat et les établissements d'enseignement supérieur privés à but non lucratif

- [Section 1 : La qualification d'établissement d'enseignement supérieur privé d'intérêt général](section-1)
- [Section 2 : Le comité consultatif pour l'enseignement supérieur privé](section-2)
