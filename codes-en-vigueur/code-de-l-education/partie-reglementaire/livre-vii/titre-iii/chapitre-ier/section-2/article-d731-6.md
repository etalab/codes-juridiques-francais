# Article D731-6

Constituent des établissements d'enseignement supérieur privés rattachés à un établissement public à caractère scientifique, culturel et professionnel les établissements suivants :

1° Ecole supérieure d'électricité rattachée à l'université Paris-XI par décret du 13 février 1974 ;

2° Ecole supérieure d'optique rattachée à l'université Paris-XI par le décret du 9 décembre 1974 portant rattachement de l'Ecole supérieure d'optique à l'université de Paris-XI ;

3° Ecole spéciale des travaux publics du bâtiment et de l'industrie rattachée à l'Ecole nationale supérieure d'arts et métiers par le décret n° 99-1020 du 30 novembre 1999 portant rattachement de l'Ecole spéciale des travaux publics du bâtiment et de l'industrie à l'Ecole nationale supérieure d'arts et métiers ;

4° Ecole supérieure de commerce de Lille rattachée à l'Ecole centrale de Lille par le décret n° 2001-324 du 9 avril 2001 portant rattachement de l'Ecole supérieure de commerce de Lille à l'Ecole centrale de Lille ;

5° Ecole d'enseignement supérieur privé ICN rattachée à l'université de Lorraine par le décret n° 2003-383 du 23 avril 2003 portant rattachement de l'école d'enseignement supérieur privé ICN à l'université Nancy-II ;

6° Ecole supérieure des technologies industrielles avancées rattachée à l'université Bordeaux-I et à l'université de Pau par le décret n° 2005-1654 du 26 décembre 2005 portant rattachement de l'Ecole supérieure des technologies industrielles avancées aux universités Bordeaux-I et Pau ;

7° Ecole internationale des sciences du traitement de l'information rattachée à l'Institut supérieur de mécanique de Paris par le décret n° 2006-264 du 1er mars 2006 portant rattachement de l'Ecole internationale des sciences du traitement de l'information à l'Institut supérieur de mécanique de Paris ;

8° Ecole supérieure de chimie organique et minérale rattachée à l'université de technologie de Compiègne par le décret n° 2008-1148 du 6 novembre 2008 portant rattachement de l'Ecole supérieure de chimie organique et minérale à l'université de technologie de Compiègne ;

9° Ecole supérieure de chimie-physique-électrique de Lyon rattachée à l'université Lyon-I par le décret n° 2009-534 du 12 mai 2009 portant rattachement de l'Ecole supérieure de chimie-physique-électronique de Lyon à l'université Lyon-I ;

10° Ecole supérieure de fonderie et de forge rattachée à l'Institut supérieur de mécanique de Paris par le décret n° 2010-1517 du 8 décembre 2010 portant rattachement de l'Ecole supérieure de fonderie et de forge à l'Institut supérieur de mécanique de Paris ;

11° Ecole d'ingénieurs de Purpan rattachée à l'Institut national polytechnique de Toulouse par le décret n° 2010-1682 du 28 décembre 2010 portant rattachement de l'Ecole d'ingénieurs de Purpan à l'Institut national polytechnique de Toulouse ;

12° Institut supérieur du bâtiment et des travaux publics rattaché à l'université d'Aix-Marseille par le décret n° 2011-326 du 24 mars 2011 portant rattachement de l'Institut supérieur du bâtiment et des travaux publics à l'université d'Aix-Marseille ;

13° Ecole polytechnique féminine rattachée à l'université de technologie de Troyes par le décret n° 2011-547 du 18 mai 2011 portant rattachement de l'Ecole polytechnique féminine à l'université de technologie de Troyes ;

14° Institut d'ingénierie informatique (3iL) rattaché à l'université de Limoges par le décret n° 2012-815 du 22 juin 2012 portant rattachement de l'institut d'ingénierie informatique (3iL) à l'université de Limoges.
