# Section 1 : Modalités d'ouverture des établissements d'enseignement supérieur privés

- [Article R731-1](article-r731-1.md)
- [Article R731-2](article-r731-2.md)
- [Article R731-3](article-r731-3.md)
- [Article R731-4](article-r731-4.md)
- [Article R731-5](article-r731-5.md)
