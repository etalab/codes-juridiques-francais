# Titre III : Les établissements d'enseignement supérieur privés

- [Chapitre Ier : Ouverture des établissements d'enseignement supérieur privés](chapitre-ier)
- [Chapitre II : Rapports entre l'Etat et les établissements d'enseignement supérieur privés à but non lucratif](chapitre-ii)
