# Article D675-20

Le diplôme de l'Ecole militaire interarmes fait l'objet d'une évaluation nationale périodique.

Un arrêté du ministre chargé de l'enseignement supérieur, pris après avis conforme du ministre de la défense et après avis du Conseil national de l'enseignement supérieur et de la recherche, fixe la durée pendant laquelle le grade de licence est attribué à ce diplôme.
