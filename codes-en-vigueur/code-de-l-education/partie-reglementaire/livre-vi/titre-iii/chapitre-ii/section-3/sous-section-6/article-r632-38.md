# Article R632-38

Pour l'obtention du diplôme d'Etat de docteur en médecine prévu à l'article R. 632-23, les internes des hôpitaux des armées qui réunissent les conditions fixées au deuxième alinéa de l'article R. 632-1 effectuent le troisième cycle des études médicales dans les conditions fixées par la présente sous-section.
