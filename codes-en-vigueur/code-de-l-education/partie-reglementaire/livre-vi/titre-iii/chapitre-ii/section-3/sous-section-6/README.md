# Sous-section 6 : Dispositions applicables aux internes des hôpitaux des armées

- [Article R632-37](article-r632-37.md)
- [Article R632-38](article-r632-38.md)
- [Article R632-39](article-r632-39.md)
- [Article R632-40](article-r632-40.md)
- [Article R632-41](article-r632-41.md)
- [Article R632-42](article-r632-42.md)
- [Article R632-43](article-r632-43.md)
- [Article R632-44](article-r632-44.md)
- [Article R632-45](article-r632-45.md)
