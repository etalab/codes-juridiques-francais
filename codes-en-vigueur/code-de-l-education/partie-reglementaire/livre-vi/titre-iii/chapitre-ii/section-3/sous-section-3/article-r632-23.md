# Article R632-23

Le diplôme d'Etat de docteur en médecine ne peut être délivré qu'aux candidats ayant à la fois soutenu avec succès leur thèse et obtenu le diplôme d'études spécialisées mentionné à l'article R. 632-24, délivré par les universités habilitées à cet effet.

Les ressortissants d'un des Etats mentionnés à l'article R. 632-1, ayant validé en France le deuxième cycle des études médicales et inscrits dans un de ces Etats dans une formation médicale spécialisée mentionnée à l'article 25 de la directive européenne mentionnée à ce même article, peuvent se voir délivrer le diplôme d'Etat de docteur en médecine après avoir soutenu avec succès leur thèse, dans les conditions prévues à l'article R. 632-22, et obtenu le titre de médecin spécialiste mentionné à l'article 26 de cette même directive.
