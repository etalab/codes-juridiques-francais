# Article R632-11

Le droit du candidat à effectuer deux fois le choix prévu à l'article R. 632-9 est maintenu en cas d'empêchement à participer aux épreuves classantes nationales résultant d'un congé de maternité, de paternité ou d'adoption, d'un cas de force majeure ou pour raison médicale dûment justifiée.
