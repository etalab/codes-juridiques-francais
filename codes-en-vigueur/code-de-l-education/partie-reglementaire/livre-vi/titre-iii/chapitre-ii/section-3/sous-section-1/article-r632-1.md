# Article R632-1

Peuvent accéder au troisième cycle des études médicales :

1° Les étudiants ayant validé le deuxième cycle des études médicales en France ;

2° Les étudiants ressortissants des Etats membres de l'Union européenne ou des autres Etats parties à l'accord sur l'Espace économique européen, de la Confédération helvétique ou de la Principauté d'Andorre ayant validé une formation médicale de base mentionnée à l'article 24 de la directive 2005/36/CE du Parlement européen et du Conseil de l'Europe du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles. Un arrêté des ministres chargés de l'enseignement supérieur et de la santé détermine les conditions dans lesquelles sont appréciées ces équivalences.
