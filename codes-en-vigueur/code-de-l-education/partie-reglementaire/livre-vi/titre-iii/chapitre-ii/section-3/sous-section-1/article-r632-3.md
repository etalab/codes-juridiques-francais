# Article R632-3

Le troisième cycle des études médicales est organisé dans des circonscriptions géographiques dénommées " interrégions ", comprenant au moins trois centres hospitaliers universitaires (CHU).

Les subdivisions d'internat créées à l'intérieur de ces interrégions constituent un espace géographique comportant un ou plusieurs CHU.

La liste des interrégions et des subdivisions d'internat est arrêtée par les ministres chargés, respectivement, de l'enseignement supérieur et de la santé.

L'ensemble de la formation est assuré sous le contrôle de la ou des unités de formation et de recherche médicale de la subdivision.

Pour l'application des dispositions de la présente section, la région Ile-de-France, d'une part, le département de la Guadeloupe et les collectivités territoriales de la Guyane et de la Martinique, d'autre part, sont considérés comme une interrégion et une subdivision.

La subdivision de l'océan Indien comprend le département de La Réunion et le Département de Mayotte. En l'absence de CHU, elle est rattachée à un ou plusieurs CHU métropolitains dans les conditions définies par arrêté des ministres chargés de l'enseignement supérieur, de la santé et de l'outre-mer.
