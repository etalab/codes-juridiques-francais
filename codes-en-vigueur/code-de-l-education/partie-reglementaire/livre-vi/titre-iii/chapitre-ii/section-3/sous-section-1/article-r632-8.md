# Article R632-8

Un arrêté du ministre chargé de l'enseignement supérieur et du ministre chargé de la santé détermine chaque année pour une période de cinq ans le nombre d'internes en médecine à former par spécialité et par subdivision telle que prévue à l'article R. 632-3, compte tenu de la situation de la démographie médicale dans les différentes spécialités et de son évolution au regard des besoins de prise en charge spécialisée.

Un arrêté du ministre chargé de l'enseignement supérieur et du ministre chargé de la santé détermine le nombre de postes d'internes offerts chaque année par discipline ou spécialité et par centre hospitalier universitaire. Le choix effectué par chaque étudiant est subordonné au rang de classement aux épreuves classantes nationales mentionnées à l'article R. 632-4.

Les étudiants ayant signé un contrat d'engagement de service public, défini à l'article L. 632-6, choisissent, en fonction de leur rang de classement, un poste d'interne au sein d'une liste établie, par spécialité et par subdivision, en fonction de la situation de la démographie médicale, par arrêté du ministre chargé de l'enseignement supérieur et du ministre chargé de la santé.
