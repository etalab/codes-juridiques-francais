# Article R632-12

Si, lors de la procédure de choix, le candidat est dans l'impossibilité d'exprimer sa volonté pour des raisons de force majeure ou pour une raison médicale dûment justifiée, il participe à la procédure de choix, selon des modalités fixées par arrêté des ministres chargés de l'enseignement supérieur et de la santé.
