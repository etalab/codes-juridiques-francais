# Article R632-17

Les internes en médecine suivent une formation d'un ou plusieurs semestres dans des lieux de stages agréés ou auprès de praticiens agréés-maîtres de stage des universités, déterminés pour chaque diplôme d'études spécialisées et chaque diplôme d'études spécialisées complémentaires dans les maquettes de formation.

Les internes de médecine générale suivent une formation d'au moins un semestre de formation dans les lieux des stages au sein des CHU agréés au titre de la discipline médecine générale. Toutefois, le coordonnateur local du diplôme d'études spécialisées de médecine générale peut, après avis du directeur de l'unité de formation et de recherche, dispenser l'interne de ce stage, dans le cas où les capacités de formation de la subdivision dont il relève s'avèrent insuffisantes.

Les internes autres que ceux qui suivent une formation spécialisée de médecine générale ou commune à la médecine et à l'odontologie exercent leurs fonctions durant au moins deux semestres dans des lieux de stages agréés d'établissements de santé autres que les CHU, ou auprès de praticiens agréés-maîtres de stage des universités. Toutefois, l'enseignant coordonnateur du diplôme d'études spécialisées peut, en fonction des exigences de la formation et des capacités de formation de la subdivision dont relève l'interne, limiter cette durée à un semestre.
