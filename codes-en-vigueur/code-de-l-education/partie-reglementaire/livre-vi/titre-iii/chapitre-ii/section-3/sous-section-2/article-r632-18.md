# Article R632-18

La formation pratique prévue à l'article R. 632-15 comporte des fonctions hospitalières et extrahospitalières.

Les fonctions hospitalières sont exercées dans les lieux de stages agréés des centres hospitaliers universitaires (CHU) et des autres établissements de santé, liés par convention à ces centres.

Les fonctions extrahospitalières sont exercées soit auprès de praticiens agréés-maîtres de stage des universités, soit dans les lieux de stages agréés par des organismes extrahospitaliers, des laboratoires de recherche, des centres de santé ou des structures de soins alternatives à l'hospitalisation, liés par convention aux CHU.

L'interne en médecine est placé sous l'autorité du responsable médical du lieu de stage agréé dans lequel il est affecté ou du praticien agréé-maître de stage des universités.

Un arrêté des ministres chargés de l'enseignement supérieur et de la santé précise les conditions d'organisation de ces stages et le contenu des conventions prévues aux alinéas précédents.

Chaque stage de formation pratique fait l'objet d'une validation dans des conditions fixées par arrêté des ministres chargés de l'enseignement supérieur et de la santé.

Nul ne peut poursuivre le troisième cycle des études médicales dès lors qu'il n'a pas validé ses semestres de formation dans un délai correspondant à deux fois la durée réglementaire de la maquette de formation suivie, qu'il s'agisse d'une formation dans le cadre d'un diplôme d'études spécialisées ou d'un diplôme d'études spécialisées complémentaires. Toutefois, une dérogation exceptionnelle, en raison de la situation particulière de l'interne, peut être accordée par le président de l'université après avis du directeur de l'unité de formation et de recherche.
