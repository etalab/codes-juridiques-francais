# Article R632-27

Le directeur de l'unité de formation et de recherche de médecine concerné et le directeur général de l'agence régionale de santé vérifient chaque année que les lieux de stage et les praticiens agréés correspondent au nombre d'internes en médecine à former par spécialité et par subdivision.
