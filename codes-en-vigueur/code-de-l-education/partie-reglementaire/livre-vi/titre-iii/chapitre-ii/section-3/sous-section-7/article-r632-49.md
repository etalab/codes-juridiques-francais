# Article R632-49

Le nombre de postes offerts aux concours de l'assistanat des hôpitaux des armées ainsi que leur répartition par spécialité et par centre hospitalier universitaire (CHU) de rattachement sont fixés chaque année par arrêté des ministres mentionnés à l'article R. 632-48. Ces postes viennent en sus de ceux ouverts au titre des choix prévus aux articles R. 632-9 et R. 632-39.

Les candidats reçus à ces concours choisissent, selon leur rang de classement, le CHU mentionné au premier alinéa du présent article dans des conditions fixées par arrêté des ministres mentionnés à l'article R. 632-48.
