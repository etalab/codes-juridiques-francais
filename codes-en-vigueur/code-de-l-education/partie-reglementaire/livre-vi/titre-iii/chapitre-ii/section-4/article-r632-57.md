# Article R632-57

Nul candidat au concours organisé dans le cadre de la présente section ne peut concourir au titre de plus de deux années. Les concours organisés au titre de l'article R. 632-8 auxquels un candidat s'est présenté le cas échéant sont pris en compte dans l'appréciation des droits à concourir.

Les candidats font connaître, lors de leur inscription au concours, la discipline et la spécialité médicales au titre desquelles ils concourent.
