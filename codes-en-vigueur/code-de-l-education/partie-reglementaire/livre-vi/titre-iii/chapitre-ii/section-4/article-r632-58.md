# Article R632-58

Le directeur général du Centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière régi par le décret n° 2007-704 du 4 mai 2007 relatif à l'organisation et au fonctionnement du Centre national de gestion des praticiens hospitaliers et des personnels de la fonction publique hospitalière et modifiant le code de la santé publique (partie réglementaire) :

1° Assure l'organisation et le déroulement des épreuves du concours ;

2° Met en œuvre la procédure nationale de choix de la subdivision, de la discipline et de la spécialité médicales.

Un arrêté du ministre chargé de l'enseignement supérieur et du ministre chargé de la santé fixe, chaque année, le nombre de postes offerts au concours et leur répartition par discipline et par spécialité pour chacune des subdivisions mentionnées à l'article R. 632-3. La région Ile-de-France est considérée comme une interrégion de troisième cycle et une subdivision d'internat pour l'application de la présente section.

Toutefois, dans la limite des postes offerts, ne peuvent être déclarés reçus au concours que les candidats dont la note est au moins égale à la note minimale fixée par le jury.
