# Article R632-73

Le versement de l'allocation cesse à la date à laquelle a été obtenu le diplôme d'Etat de docteur en médecine. L'exercice professionnel est considéré comme débutant à compter de cette même date. Le directeur général du Centre national de gestion établit le nombre de mois d'engagement du signataire.
