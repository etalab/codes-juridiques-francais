# Article R632-68

Dans chaque unité de formation et de recherche de médecine, une commission de sélection, présidée par le directeur de l'unité ou son représentant, comprend :

1° Le directeur général de l'agence régionale de santé ou son représentant ;

2° Le président du conseil régional de l'ordre des médecins ou son représentant ;

3° Le président de l'union régionale des professionnels de santé compétente pour les médecins libéraux ou son représentant ;

4° Un directeur d'un établissement public de santé de la région désigné par la Fédération hospitalière de France ;

5° Un interne en médecine générale et un interne en médecine d'une autre spécialité, désignés par le directeur de l'unité sur proposition des organisations représentatives ;

6° Un étudiant en médecine désigné par le directeur de l'unité sur proposition des organisations représentatives de ces étudiants.
