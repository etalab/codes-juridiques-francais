# Article D631-1

Les études en vue du diplôme d'études spécialisées de biologie médicale durent quatre ans. Elles comportent deux parties nommées respectivement niveau 1 et niveau 2.

Le niveau 1 correspond aux quatre premiers semestres de l'internat et le niveau 2 aux quatre autres semestres.

Ce diplôme est délivré par les universités habilitées à cet effet par arrêté du ministre chargé de l'enseignement supérieur et du ministre chargé de la santé.

Le diplôme comporte deux options :

1° Biologie polyvalente ;

2° Biologie orientée vers une spécialisation.
