# Article D633-2

Un conseil scientifique en pharmacie, placé auprès du ministre chargé de l'enseignement supérieur, prépare et vérifie les sujets susceptibles de faire l'objet des épreuves du concours de l'internat mentionné à l'article D. 633-1. Il est composé de huit membres, choisis parmi les enseignants titulaires des universités relevant du groupe des disciplines pharmaceutiques du Conseil national des universités pour les disciplines médicales, odontologiques et pharmaceutiques et nommés par arrêté des ministres chargés de l'enseignement supérieur et de la santé. Il désigne en son sein un président et un secrétaire général.

Le président désigne des experts, chargés de proposer ces sujets au conseil scientifique.

Les sujets des épreuves sont tirés au sort par le président du conseil scientifique en pharmacie, à partir d'une banque de sujets constituée par ce conseil.

Un arrêté des ministres chargés de l'enseignement supérieur et de la santé précise les modalités de fonctionnement du conseil scientifique en pharmacie.
