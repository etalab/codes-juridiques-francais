# Article D633-32

Pour l'application du quatrième alinéa de l'article D. 633-15, les stages effectués avant le 5 février 2012 sont pris en compte.
