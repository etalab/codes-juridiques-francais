# Article D633-21

La formation en vue des diplômes d'études spécialisées complémentaires de pharmacie est dispensée à temps plein. Elle comporte un enseignement théorique et une formation pratique accomplie dans des lieux de stage agréés dans les mêmes conditions que celles qui s'appliquent à la formation en vue des diplômes d'études spécialisées.

Les dispositions de l'article D. 633-13 sont applicables aux diplômes d'études spécialisées complémentaires.
