# Article R633-27

Les candidats nommés assistants à l'issue de ce concours sont soumis aux dispositions des articles D. 633-9 à D. 633-16, à l'exception de l'article D. 633-13, des articles D. 633-19, D. 633-20 à D. 633-22 et D. 633-23 à D. 633-31 du présent code ainsi que de l'article R. 6153-45 du code de la santé publique. Les dispositions des articles R. 633-35 à R. 633-39, R. 633-17 et R. 633-18 du présent code ne leur sont pas applicables.
