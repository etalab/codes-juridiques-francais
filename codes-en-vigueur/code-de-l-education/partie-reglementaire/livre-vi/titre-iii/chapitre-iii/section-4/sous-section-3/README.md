# Sous-section 3 : Dispositions applicables aux pharmaciens des armées

- [Article D633-23](article-d633-23.md)
- [Article D633-29](article-d633-29.md)
- [Article D633-30](article-d633-30.md)
- [Article D633-31](article-d633-31.md)
- [Article R633-24](article-r633-24.md)
- [Article R633-25](article-r633-25.md)
- [Article R633-26](article-r633-26.md)
- [Article R633-27](article-r633-27.md)
- [Article R633-28](article-r633-28.md)
