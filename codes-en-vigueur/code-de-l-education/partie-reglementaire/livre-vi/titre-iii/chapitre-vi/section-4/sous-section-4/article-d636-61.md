# Article D636-61

L'évaluation des connaissances et des compétences intervient à l'issue de chaque semestre soit par un contrôle continu et régulier, soit par un examen terminal, soit par ces deux modes de contrôle combinés.

Les modalités de contrôle continu prévoient la communication régulière des notes et résultats à l'étudiant.

Le jury, prévu à l'article D. 636-66, se prononce sur la validation de chaque semestre.
