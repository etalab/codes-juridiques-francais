# Article D636-60

Des périodes d'études peuvent être effectuées à l'étranger, dans des conditions définies par convention entre l'établissement d'origine de l'étudiant et l'établissement d'accueil, notamment en ce qui concerne la reconnaissance mutuelle des connaissances et compétences acquises ainsi que leur validation.
