# Chapitre VI : Les autres formations de santé

- [Section 1 : Les études d'audioprothèse](section-1)
- [Section 4 : Les études de technicien supérieur en imagerie médicale  et radiologie thérapeutique](section-4)
- [Section 5 : Les formations relevant du ministre chargé de la santé](section-5)
