# Article R634-22

Les internes nommés à l'issue du concours d'internat à titre européen sont soumis aux dispositions pédagogiques prévues à la présente sous-section et à l'ensemble des dispositions applicables aux internes en odontologie.

Il est tenu compte des compétences acquises, des fonctions de troisième cycle déjà accomplies ainsi que de la formation déjà suivie dans le cadre de la formation odontologique continue, selon des modalités déterminées par le conseil d'administration de l'université, sur proposition du conseil des études et de la vie universitaire et après avis du conseil de l'unité de formation et de recherche d'odontologie.

Les internes bénéficiant, pour la durée de leur formation pratique, des dispositions prévues au deuxième alinéa du présent article sont réputés avoir une ancienneté augmentée du nombre de semestres admis en équivalence.
