# Sous-section 1 : Organisation

- [Paragraphe 1 : Accès au troisième cycle long](paragraphe-1)
- [Paragraphe 2 : Formation](paragraphe-2)
- [Paragraphe 3 : Obtention et délivrance des diplômes](paragraphe-3)
- [Paragraphe 4 : Accès au troisième cycle long pour les praticiens français ou ressortissants des autres Etats membres de l'Union européenne, des autres Etats parties à l'accord sur l'Espace économique européen, de la Confédération helvétique ou de la Principauté d'Andorre](paragraphe-4)
