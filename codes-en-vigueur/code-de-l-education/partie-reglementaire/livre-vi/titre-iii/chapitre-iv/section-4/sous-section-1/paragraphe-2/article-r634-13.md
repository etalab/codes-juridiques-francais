# Article R634-13

Au cours de leur formation, les internes en odontologie peuvent bénéficier d'une année-recherche dont les modalités d'organisation sont fixées par arrêté des ministres chargés du budget, de l'enseignement supérieur et de la santé. Un arrêté de ces ministres fixe, chaque année, le nombre de postes offerts. L'année-recherche est attribuée en tenant compte de la qualité du projet de recherche de l'interne.

Pendant l'année-recherche, les internes en odontologie demeurent soumis au statut qui leur est applicable.

Les stages effectués au cours de l'année-recherche ne sont pas pris en compte dans les obligations de formation pratique prévues pour l'obtention du diplôme postulé dans le cadre de l'internat.
