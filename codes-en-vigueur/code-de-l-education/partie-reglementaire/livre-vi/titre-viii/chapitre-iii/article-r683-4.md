# Article R683-4

La convention prévue à l'article L. 683-3 fixe notamment les règles de choix des lieux de stage agréés et des praticiens agréés-maîtres de stage des universités proposés aux internes ainsi que les modalités de leur affectation.
