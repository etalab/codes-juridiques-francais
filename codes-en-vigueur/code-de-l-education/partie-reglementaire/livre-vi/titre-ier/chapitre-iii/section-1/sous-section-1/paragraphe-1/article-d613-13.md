# Article D613-13

Les diplômes de formation générale en sciences médicales, en sciences pharmaceutiques, en sciences odontologiques et en sciences maïeutiques mentionnés à l'article D. 613-7 confèrent à leur titulaire le grade de licence.
