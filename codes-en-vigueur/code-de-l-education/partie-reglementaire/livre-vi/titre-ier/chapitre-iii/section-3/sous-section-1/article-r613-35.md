# Article R613-35

Le dossier de demande de validation présenté par le candidat explicite par référence au diplôme postulé les connaissances, compétences et aptitudes acquises au cours des études ou par l'expérience.

Pour la validation des études, le dossier comprend les diplômes, certificats et toutes autres pièces permettant au jury de validation d'apprécier la nature et le niveau de ces études. En particulier, lorsque les études ont été suivies dans le cadre défini par l'Union européenne pour favoriser la mobilité, dans un autre Etat européen, le dossier comprend l'annexe descriptive du diplôme et les attestations certifiant les crédits européens obtenus représentatifs des études suivies.

Pour la validation des acquis de l'expérience, le dossier comprend les documents rendant compte de cette expérience et de la durée des différentes activités dans lesquelles le candidat l'a acquise ainsi que, le cas échéant, les attestations correspondant aux formations suivies et aux diplômes obtenus antérieurement.
