# Sous-section 3 : Admission en section de techniciens supérieurs  et en institut universitaire de technologie

- [Article D612-30](article-d612-30.md)
- [Article D612-31](article-d612-31.md)
- [Article D612-32](article-d612-32.md)
