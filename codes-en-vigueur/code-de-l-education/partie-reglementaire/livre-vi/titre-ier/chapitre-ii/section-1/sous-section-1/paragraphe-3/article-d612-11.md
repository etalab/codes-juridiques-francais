# Article D612-11

Outre les dispositions des paragraphes 1 et 2 de la présente sous-section, celles du présent paragraphe sont applicables aux étudiants étrangers non ressortissants d'un Etat membre de l'Union européenne, d'un Etat partie à l'accord sur l'Espace économique européen ou de la Confédération helvétique.
