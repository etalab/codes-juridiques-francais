# Article D612-10

Pour l'application des dispositions du deuxième alinéa de l'article L. 612-3, sont considérées comme formant un seul ensemble les académies de Paris, Créteil et Versailles.

Les titulaires du baccalauréat français ayant passé les épreuves dans un centre d'examen à l'étranger sont considérés comme bacheliers de l'académie de rattachement de ce centre pour l'application de l'article L. 612-3.

Les non-titulaires du baccalauréat français candidats à une première inscription dans les universités françaises et dont la résidence se situe à l'étranger au moment des démarches d'inscription bénéficient d'une priorité d'inscription dans l'académie où ils déclarent fixer leur résidence en France, sans que puisse leur être opposée leur résidence actuelle.
