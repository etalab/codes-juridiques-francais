# Article D643-13

Le brevet de technicien supérieur est délivré au vu des résultats obtenus à un examen sanctionnant l'acquisition par le candidat des capacités, compétences et savoirs et savoir-faire constitutifs des unités prévues par le référentiel de certification de chaque spécialité du diplôme.

Tout candidat peut présenter à titre facultatif une ou deux unités choisies parmi celles proposées, le cas échéant, par le référentiel.
