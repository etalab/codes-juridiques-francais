# Article D643-19

Passent l'examen sous forme d'au moins trois épreuves ponctuelles et, le cas échéant, d'épreuves qui peuvent être validées totalement ou partiellement par contrôle en cours de formation, dans les conditions fixées par le règlement particulier du diplôme :

1° Les candidats ayant préparé le brevet de technicien supérieur par la voie scolaire dans un établissement public ou privé sous contrat ;

2° Ceux qui l'ont préparé par la voie de la formation professionnelle continue dans un établissement public habilité ;

3° Ceux qui l'ont préparé par la voie de l'apprentissage dans un centre de formation d'apprentis habilité ou une section d'apprentissage habilitée.

Passent l'examen sous forme d'épreuves ponctuelles, dans les conditions fixées par le règlement particulier du diplôme :

1° Les candidats ayant préparé un brevet de technicien supérieur par la voie scolaire dans un établissement privé hors contrat ;

2° Ceux qui l'ont préparé par la voie de la formation professionnelle continue dans un établissement non habilité ;

3° Ceux qui l'ont préparé par la voie de l'apprentissage dans un centre de formation d'apprentis non habilité ou une section d'apprentissage non habilitée ;

4° Les candidats ayant suivi une préparation par la voie de l'enseignement à distance, quel que soit leur statut, ainsi que les candidats qui se présentent au titre de leur expérience professionnelle en application du troisième alinéa (2°) de l'article D. 643-16.
