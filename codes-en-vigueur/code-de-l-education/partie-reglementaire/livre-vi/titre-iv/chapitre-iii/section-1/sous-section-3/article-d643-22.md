# Article D643-22

Les candidats ayant préparé le brevet de technicien supérieur par la voie scolaire ou par la voie de l'apprentissage présentent obligatoirement l'examen sous la forme globale à l'issue de leur formation, sauf dérogation qui peut être accordée par le recteur d'académie pour les candidats bénéficiant des dispositions du troisième alinéa de l'article D. 643-6, de l'article D. 643-7 ou de l'article D. 643-8.

Les candidats ayant préparé le brevet de technicien supérieur par la voie de la formation professionnelle continue, ceux ayant suivi une préparation par la voie de l'enseignement à distance, quel que soit leur statut, ainsi que ceux qui se présentent au titre de leur expérience professionnelle en application du troisième alinéa (2°) de l'article D. 643-16 optent, lors de leur inscription à l'examen, soit pour la forme globale, soit pour la forme progressive, sous réserve des dispositions de l'article D. 643-20. Le choix pour l'une ou l'autre de ces modalités est définitif.

Le diplôme est délivré aux candidats qui ont obtenu une moyenne générale supérieure ou égale à 10 sur 20 à l'ensemble des évaluations affectées de leur coefficient.

Les notes obtenues aux épreuves facultatives ne sont prises en compte que pour leur part excédant la note 10 sur 20. Les points supplémentaires sont ajoutés au total des points obtenus aux épreuves obligatoires en vue de la délivrance du diplôme.
