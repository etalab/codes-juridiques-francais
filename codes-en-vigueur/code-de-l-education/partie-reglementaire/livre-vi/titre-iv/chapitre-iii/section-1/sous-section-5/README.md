# Sous-section 5 : Inscription du brevet de technicien supérieur  dans le dispositif européen d'enseignement supérieur

- [Article D643-33](article-d643-33.md)
- [Article D643-34](article-d643-34.md)
- [Article D643-35](article-d643-35.md)
- [Article D643-35-1](article-d643-35-1.md)
