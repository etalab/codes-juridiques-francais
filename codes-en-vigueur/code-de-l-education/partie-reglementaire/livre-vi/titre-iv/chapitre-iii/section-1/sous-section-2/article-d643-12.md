# Article D643-12

La décision de positionnement peut réduire, en fonction de la situation professionnelle du candidat, la durée des stages de formation dans les conditions fixées par le règlement particulier du diplôme.
