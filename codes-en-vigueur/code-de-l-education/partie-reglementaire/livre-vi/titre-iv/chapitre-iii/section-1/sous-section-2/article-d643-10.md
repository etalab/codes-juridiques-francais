# Article D643-10

La décision de positionnement fixe la durée de formation requise lors de l'inscription au diplôme. Elle est prononcée par le recteur d'académie, à la demande du candidat, après son admission dans un établissement et selon des modalités fixées par arrêté du ministre chargé de l'enseignement supérieur.

Elle est prise au titre du brevet de technicien supérieur que le candidat souhaite préparer et vaut jusqu'à l'obtention de ce diplôme.
