# Sous-section 1 : Dispositions générales

- [Article D643-1](article-d643-1.md)
- [Article D643-2](article-d643-2.md)
- [Article D643-3](article-d643-3.md)
- [Article D643-4](article-d643-4.md)
