# Sous-section 2 : Modalités de préparation

- [Article D642-17](article-d642-17.md)
- [Article D642-18](article-d642-18.md)
- [Article D642-19](article-d642-19.md)
- [Article D642-20](article-d642-20.md)
- [Article D642-21](article-d642-21.md)
- [Article D642-22](article-d642-22.md)
- [Article D642-23](article-d642-23.md)
- [Article R642-16](article-r642-16.md)
