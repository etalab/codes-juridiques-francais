# Article R642-9

Lorsqu'elle exerce une compétence consultative, la commission des titres d'ingénieur remplit ses fonctions dans les conditions prévues par le décret n° 2006-672 du 8 juin 2006 relatif à la création, à la composition et au fonctionnement des commissions administratives à caractère consultatif et par les deuxième et troisième alinéas du présent article.

Les délibérations sont prises à la majorité absolue des votants. En cas de partage des voix, la voix du président est prépondérante.

Tout membre de la commission empêché d'assister à tout ou partie d'une séance peut donner par écrit procuration à un autre membre. La procuration est remise au secrétaire-greffier de la commission avant le premier des votes pour lesquels elle prend effet. Nul ne peut détenir plus d'une procuration.
