# Section 1 : Le Conseil national de l'enseignement supérieur et de la recherche délibérant en matière consultative.

- [Sous-section 1 : Composition.](sous-section-1)
- [Sous-section 2 : Fonctionnement.](sous-section-2)
- [Article D232-1](article-d232-1.md)
