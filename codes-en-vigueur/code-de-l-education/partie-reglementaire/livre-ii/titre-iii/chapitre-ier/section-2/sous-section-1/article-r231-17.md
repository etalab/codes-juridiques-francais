# Article R231-17

Les douze membres titulaires du Conseil supérieur de l'éducation statuant en matière contentieuse et disciplinaire mentionnés à l'article L. 231-7 ainsi que leurs suppléants sont élus au scrutin secret majoritaire plurinominal à deux tours par les représentants au conseil des enseignants et des enseignants-chercheurs de l'enseignement public mentionnés au 1° (a) et au 1° (c) de l'article R. 231-2 ou leur suppléant réunis en collège électoral.

Les six représentants des établissements d'enseignement privés et de leurs personnels appelés à siéger, conformément à l'article L. 231-8, pour les affaires contentieuses et disciplinaires concernant les établissements d'enseignement privés ou leurs personnels, sont élus, ainsi que leurs suppléants, par les représentants des établissements d'enseignement privés et de leurs personnels mentionnés au 1° (g) de l'article R. 231-2 ou leur suppléant selon le mode de scrutin prévu au premier alinéa du présent article.

Les conseillers titulaires sont élus parmi les conseillers titulaires du conseil, les conseillers suppléants peuvent être élus parmi les suppléants. Chaque candidat à la fonction de conseiller titulaire se présente aux suffrages avec un suppléant nommément désigné.
