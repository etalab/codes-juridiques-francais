# Article R231-23

Les décisions sont prises à la majorité des membres présents.

En cas de partage égal des voix, la voix du président est prépondérante.

Les décisions sont rendues dans la forme suivante : " à la majorité des membres présents, la majorité des membres du conseil étant présents ".
