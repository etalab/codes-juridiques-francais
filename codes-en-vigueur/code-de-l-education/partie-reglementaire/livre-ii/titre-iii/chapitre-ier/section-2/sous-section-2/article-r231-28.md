# Article R231-28

Si la demande est formée par une personne appartenant ou ayant appartenu à l'enseignement du second degré, le ministre en transmet, dans un délai de quinze jours à dater de l'enregistrement, la copie au recteur de l'académie dans le ressort de laquelle cette personne est actuellement domiciliée.

Si la demande a été formée par une personne appartenant ou ayant appartenu à l'enseignement du premier degré, le ministre en transmet, dans le même délai, la copie au préfet dans le département duquel cette personne est actuellement domiciliée. Le préfet fait parvenir cette pièce au     directeur académique des services de l'éducation nationale agissant sur délégation du recteur d'académie dans le délai de huit jours.
