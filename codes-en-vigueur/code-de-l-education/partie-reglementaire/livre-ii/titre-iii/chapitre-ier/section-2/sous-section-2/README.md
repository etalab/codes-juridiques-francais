# Sous-section 2 : Relèvement des exclusions, déchéances et incapacités.

- [Article R231-27](article-r231-27.md)
- [Article R231-28](article-r231-28.md)
- [Article R231-29](article-r231-29.md)
- [Article R231-30](article-r231-30.md)
- [Article R231-31](article-r231-31.md)
- [Article R231-32](article-r231-32.md)
- [Article R231-33](article-r231-33.md)
