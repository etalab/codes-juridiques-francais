# Section 1 : La Conférence des présidents d'université.

- [Article D233-1](article-d233-1.md)
- [Article D233-2](article-d233-2.md)
- [Article D233-3](article-d233-3.md)
- [Article D233-4](article-d233-4.md)
- [Article D233-5](article-d233-5.md)
- [Article D233-6](article-d233-6.md)
