# Article R234-26

Le conseil de l'éducation nationale, selon que les questions soumises à délibération sont de la compétence de l'Etat, de la région ou du département, est présidé par le préfet de région, par le président du conseil régional ou par le président du conseil départemental.

Les présidents des conseils de l'éducation nationale sont suppléés dans les conditions ci-après :

1° En cas d'empêchement du préfet de région, le conseil est présidé par le recteur d'académie ou, en cas d'empêchement de ce dernier, par le directeur académique des services de l'éducation nationale agissant sur délégation du recteur d'académie. Lorsque les questions examinées concernent l'enseignement agricole, le préfet est suppléé par le directeur départemental de l'agriculture ;

2° En cas d'empêchement du président du conseil régional, le conseil de l'éducation nationale est présidé par un conseiller régional délégué à cet effet par le président du conseil régional ;

3° En cas d'empêchement du président du conseil départemental, le conseil de l'éducation nationale est présidé par un conseiller départemental délégué à cet effet par le président du conseil départemental.

Les suppléants des présidents ainsi que le directeur départemental des affaires maritimes ont la qualité de vice-président. Les présidents et les vice-présidents sont membres de droit du conseil. Ils ne participent pas aux votes.
