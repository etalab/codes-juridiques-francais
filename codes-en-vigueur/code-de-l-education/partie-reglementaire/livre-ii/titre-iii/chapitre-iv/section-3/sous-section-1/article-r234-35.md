# Article R234-35

Les élections professionnelles sur la base desquelles est déterminée la représentativité des organisations syndicales mentionnées au 4° de l'article L. 234-2 sont les élections aux commissions consultatives mixtes départementales et aux commissions consultatives mixtes académiques créées respectivement par les articles 8 et 9 du décret n° 60-745 du 28 juillet 1960 relatif aux conditions financières de fonctionnement (personnel et matériel) des classes sous contrat d'association.
