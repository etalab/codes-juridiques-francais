# Sous-section 3 : Dispositions particulières au conseil de l'éducation nationale de Mayotte

- [Article R234-44](article-r234-44.md)
- [Article R234-45](article-r234-45.md)
