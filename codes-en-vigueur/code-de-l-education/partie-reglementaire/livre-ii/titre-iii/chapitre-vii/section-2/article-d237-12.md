# Article D237-12

Les dispositions relatives au comité départemental de l'emploi sont fixées par les articles D. 910-7 à D. 910-13 du code du travail.
