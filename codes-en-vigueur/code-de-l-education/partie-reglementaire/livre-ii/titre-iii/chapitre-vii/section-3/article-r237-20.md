# Article R237-20

Le secrétariat-greffe de la Commission spéciale de la taxe d'apprentissage est assuré par des fonctionnaires du ministère de l'éducation nationale.
