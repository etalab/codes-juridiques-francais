# Article R237-19

Les décisions de chaque formation de jugement sont prises à la majorité de ses membres. En cas de partage égal des voix, celle du président est prépondérante.
