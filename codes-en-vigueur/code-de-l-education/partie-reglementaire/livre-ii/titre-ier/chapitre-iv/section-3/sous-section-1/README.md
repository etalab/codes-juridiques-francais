# Sous-section 1 : Le fonds régional de l'apprentissage et de la formation professionnelle continue.

- [Article R214-2](article-r214-2.md)
- [Article R214-3](article-r214-3.md)
- [Article R214-4](article-r214-4.md)
