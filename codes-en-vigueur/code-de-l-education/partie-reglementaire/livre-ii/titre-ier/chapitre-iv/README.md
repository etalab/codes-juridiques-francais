# Chapitre IV : Les compétences des régions

- [Section 2 : Lycées, établissements d'éducation spéciale, lycées professionnels maritimes et établissements d'enseignement agricole.](section-2)
- [Section 3 : Formation professionnelle et apprentissage](section-3)
- [Section 4 : Ecoles de la deuxième chance.](section-4)
