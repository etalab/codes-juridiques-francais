# Article D211-11

Les collèges et les lycées accueillent les élèves résidant dans leur zone de desserte.

Le directeur académique des services de l'éducation nationale agissant sur délégation du recteur d'académie, détermine pour chaque rentrée scolaire l'effectif maximum d'élèves pouvant être accueillis dans chaque établissement en fonction des installations et des moyens dont il dispose.

Dans la limite des places restant disponibles après l'inscription des élèves résidant dans la zone normale de desserte d'un établissement, des élèves ne résidant pas dans cette zone peuvent y être inscrits sur l'autorisation du directeur académique des services de l'éducation nationale agissant sur délégation du recteur d'académie, dont relève cet établissement.

Lorsque les demandes de dérogation excèdent les possibilités d'accueil, l'ordre de priorité de celles-ci est arrêté par le directeur académique des services de l'éducation nationale agissant sur délégation du recteur d'académie, conformément aux procédures d'affectation en vigueur.

Toute dérogation concernant un élève résidant dans un département autre que celui où se trouve l'établissement sollicité ne peut être accordée qu'après avis favorable du directeur académique des services de l'éducation nationale agissant sur délégation du recteur d'académie du département de résidence.
