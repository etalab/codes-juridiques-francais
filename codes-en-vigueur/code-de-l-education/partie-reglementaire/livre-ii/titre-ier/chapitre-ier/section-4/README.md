# Section 4 : Liste des dépenses pédagogiques à la charge de l'Etat.

- [Article D211-14](article-d211-14.md)
- [Article D211-15](article-d211-15.md)
- [Article D211-16](article-d211-16.md)
