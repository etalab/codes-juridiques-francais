# Article R213-14

Les frais de transport mentionnés à l'article R. 213-13 sont remboursés directement aux familles ou aux intéressés s'ils sont majeurs ou, le cas échéant, à l'organisme qui en a fait l'avance.
