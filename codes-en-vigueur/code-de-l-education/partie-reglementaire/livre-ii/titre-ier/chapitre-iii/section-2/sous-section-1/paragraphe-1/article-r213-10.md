# Article R213-10

L'arbitrage du préfet de département prévu au cinquième alinéa de l'article L. 213-11 intervient à la demande du président de l'organe exécutif de l'autorité compétente pour l'organisation des transports urbains ou du président du conseil départemental.
