# Article D222-21

Le recteur de l'académie de Paris peut déléguer sa signature pour les questions relatives aux enseignements supérieurs et pour les questions communes aux enseignements secondaires et supérieurs :

1° Au vice-chancelier des universités de Paris ;

2° Au secrétaire général de la chancellerie en cas d'absence ou d'empêchement du vice-chancelier.
