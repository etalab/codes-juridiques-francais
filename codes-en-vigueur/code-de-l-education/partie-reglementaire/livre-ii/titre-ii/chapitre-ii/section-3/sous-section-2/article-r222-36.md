# Article R222-36

Sont prises par le recteur d'académie :

a) Les décisions de règlement amiable des demandes d'indemnité mettant en cause la responsabilité de l'Etat, pour les litiges relevant de la compétence des services déconcentrés et portant sur un montant inférieur à 10 000 euros ;

b) Les décisions à caractère financier prises pour l'exécution des décisions de justice portant sur les litiges mettant en cause la responsabilité des services déconcentrés.
