# Sous-section 4 : Dispositions communes.

- [Article D222-11](article-d222-11.md)
- [Article R222-12](article-r222-12.md)
