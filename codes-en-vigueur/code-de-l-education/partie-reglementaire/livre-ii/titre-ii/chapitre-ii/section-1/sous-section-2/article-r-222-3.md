# Article R*222-3

Lorsque la conférence administrative régionale examine des questions de la compétence du ministre chargé de l'éducation ou du ministre chargé de l'enseignement supérieur, il est fait appel, conformément aux dispositions de l'article 27 du décret n° 66-614 du 10 août 1966 relatif à l'organisation des services de l'Etat dans la région parisienne, au recteur de Paris, lequel est accompagné, pour les affaires qui les concernent, par le ou les autres recteurs de la région.

Pour les autres organismes régionaux, il est fait appel aux recteurs des trois académies, chacun pour ce qui le concerne.
