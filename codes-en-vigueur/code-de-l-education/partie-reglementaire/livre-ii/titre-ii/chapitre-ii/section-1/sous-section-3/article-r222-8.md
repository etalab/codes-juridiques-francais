# Article R222-8

Les limites territoriales de chacune des académies de La Réunion, de la Martinique, de la Guadeloupe et de la Guyane sont celles de la région correspondante.
