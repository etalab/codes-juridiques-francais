# Article R261-5

Pour l'application des articles R. 232-38, R. 232-41, R. 232-42 et R. 232-43, les compétences qui relèvent de la compétence de l'Etat conférées en métropole aux recteurs sont exercées dans les îles Wallis et Futuna par le ministre chargé de l'enseignement supérieur.
