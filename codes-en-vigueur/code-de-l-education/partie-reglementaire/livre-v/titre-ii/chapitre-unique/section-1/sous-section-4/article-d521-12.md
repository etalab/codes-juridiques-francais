# Article D521-12

Lorsqu'il arrête l'organisation de la semaine scolaire d'une école, le directeur académique des services de l'éducation nationale agissant par délégation du recteur d'académie veille au respect des conditions mentionnées aux articles D. 521-10 et D. 521-11. Il s'assure de la compatibilité de cette organisation avec l'intérêt du service et, le cas échéant, de sa cohérence avec le projet éducatif territorial élaboré conjointement par la collectivité, les services de l'Etat et les autres partenaires intéressés. Il s'assure également que cette organisation ne porte pas atteinte à l'exercice de la liberté de l'instruction religieuse mentionnée au second alinéa de l'article L. 141-2.

Le directeur académique des services de l'éducation nationale peut donner son accord à une dérogation aux dispositions du deuxième alinéa de l'article D. 521-10 lorsqu'elle est justifiée par les particularités du projet éducatif territorial et que l'organisation proposée présente des garanties pédagogiques suffisantes.

La décision d'organisation de la semaine scolaire prise par le directeur académique des services de l'éducation nationale ne peut porter sur une durée supérieure à trois ans. A l'issue de cette période, cette décision peut être renouvelée tous les trois ans après un nouvel examen, en respectant la même procédure.

Les décisions prises par le directeur académique des services de l'éducation nationale pour fixer les heures d'entrée et de sortie de chaque école sont annexées au règlement type départemental mentionné à l'article R. 411-5, après consultation du conseil départemental de l'éducation nationale, sans préjudice du pouvoir de modification conféré au maire de la commune par les dispositions de l'article L. 521-3.
