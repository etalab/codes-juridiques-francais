# Sous-section 4 : Dispositions particulières aux écoles maternelles et élémentaires

- [Article D521-10](article-d521-10.md)
- [Article D521-11](article-d521-11.md)
- [Article D521-12](article-d521-12.md)
- [Article D521-13](article-d521-13.md)
