# Section 1 : Aménagement du temps scolaire

- [Sous-section 1 : Dispositions communes à l’ensemble des académies](sous-section-1)
- [Sous-section 2 : Dispositions particulières aux académies de Corse et d’outre-mer et à Saint-Pierre-et-Miquelon](sous-section-2)
- [Sous-section 3 : Dispositions particulières à l'enseignement et à la formation professionnelle aux métiers de l'agriculture, de la forêt, de la nature et des territoires](sous-section-3)
- [Sous-section 4 : Dispositions particulières aux écoles maternelles et élémentaires](sous-section-4)
