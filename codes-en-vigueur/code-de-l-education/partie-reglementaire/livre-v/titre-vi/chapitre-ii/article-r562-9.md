# Article R562-9

Pour l'application de l'article R. 531-25 à Mayotte, les mots : "sur le rapport de               le directeur académique des services de l'éducation nationale agissant sur délégation du recteur d'académie" et " sous couvert du directeur académique des services de l'éducation nationale agissant sur délégation du recteur d'académie" sont supprimés.
