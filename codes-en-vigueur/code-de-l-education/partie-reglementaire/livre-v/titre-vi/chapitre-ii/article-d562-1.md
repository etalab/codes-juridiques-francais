# Article D562-1

Les articles D. 511-51, D. 521-1 à D. 521-5, le deuxième alinéa de l'article D. 551-4 et les articles D. 551-10 et D. 551-11 ne sont pas applicables à Mayotte.
