# Article D561-11

Pour l'application de l'article D. 542-1 dans les îles Wallis et Futuna, les mots : "en France" sont remplacés par les mots : "à Wallis et Futuna".
