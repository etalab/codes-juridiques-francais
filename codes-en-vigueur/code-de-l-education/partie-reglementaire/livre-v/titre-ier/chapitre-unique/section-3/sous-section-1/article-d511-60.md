# Article D511-60

Le Conseil national de la vie lycéenne est présidé par le ministre chargé de l'éducation ou son représentant, nommé par arrêté du ministre.

Il se compose de trente-trois membres répartis de la manière suivante :

1° Trente membres élus, en leur sein, pour deux ans, par les représentants lycéens aux conseils académiques de la vie lycéenne, à raison d'un titulaire et d'un suppléant ;

2° Les trois représentants des lycéens au sein du Conseil supérieur de l'éducation ou leurs suppléants, pour la durée de leur mandat au titre de ce conseil.

Pour l'application du 1°, lorsque le titulaire est en dernière année de cycle d'études, le suppléant doit être inscrit dans une classe de niveau inférieur.
