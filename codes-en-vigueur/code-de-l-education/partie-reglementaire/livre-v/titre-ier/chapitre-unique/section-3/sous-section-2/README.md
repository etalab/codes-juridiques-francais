# Sous-section 2 : Le conseil académique de la vie lycéenne

- [Article D511-63](article-d511-63.md)
- [Article D511-64](article-d511-64.md)
- [Article D511-65](article-d511-65.md)
- [Article D511-66](article-d511-66.md)
- [Article D511-67](article-d511-67.md)
- [Article D511-68](article-d511-68.md)
- [Article D511-69](article-d511-69.md)
- [Article D511-70](article-d511-70.md)
- [Article D511-71](article-d511-71.md)
- [Article D511-72](article-d511-72.md)
- [Article D511-73](article-d511-73.md)
