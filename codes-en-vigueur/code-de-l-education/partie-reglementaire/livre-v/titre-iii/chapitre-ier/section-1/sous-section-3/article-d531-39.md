# Article D531-39

Les chefs d'établissement intéressés transmettent au directeur académique des services de l'éducation nationale agissant sur délégation du recteur d'académie, le dossier des élèves proposés par les conseils de classe pour l'obtention d'une bourse au mérite.

La commission départementale examine ces dossiers et formule ses avis en veillant à ce que les parcours des élèves méritants soient pris en considération quelle que soit l'orientation vers les trois voies de formation du lycée.
