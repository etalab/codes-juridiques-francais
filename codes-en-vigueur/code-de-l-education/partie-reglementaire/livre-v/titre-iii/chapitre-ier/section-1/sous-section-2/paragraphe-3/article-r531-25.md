# Article R531-25

Les décisions d'attribution ou de refus de bourses nationales d'études du second degré de lycée sont prises par le recteur d'académie sur le rapport du directeur académique des services de l'éducation nationale agissant sur délégation du recteur d'académie.

Ces décisions sont notifiées dans un délai de trois jours aux représentants légaux des demandeurs. Elles mentionnent les voies de recours.

En cas de rejet, ceux-ci peuvent, dans le délai de huit jours qui suit la notification, former un recours sous couvert du directeur académique des services de l'éducation nationale agissant sur délégation du recteur d'académie, auprès du recteur.
