# Paragraphe 2 : Critères d’attribution

- [Article D531-21](article-d531-21.md)
- [Article D531-22](article-d531-22.md)
- [Article R531-18](article-r531-18.md)
- [Article R531-19](article-r531-19.md)
- [Article R531-20](article-r531-20.md)
