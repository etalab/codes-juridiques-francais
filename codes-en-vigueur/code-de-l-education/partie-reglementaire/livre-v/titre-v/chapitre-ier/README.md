# Chapitre Ier : Les activités périscolaires

- [Section 1 : Agrément des associations éducatives
complémentaires de l’enseignement public](section-1)
- [Section 2 : Le conseil national et les conseils académiques des associations éducatives complémentaires de l’enseignement public](section-2)
