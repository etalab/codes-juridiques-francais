# Article D123-19

Tout établissement ayant l'intention de contracter avec une institution étrangère ou internationale, universitaire ou non, communique le projet d'accord au ministre chargé de l'enseignement supérieur, qui en saisit le ministre des affaires étrangères.

Le projet d'accord fait l'objet d'un examen conjoint du ministre chargé de l'enseignement supérieur et du ministre des affaires étrangères.

Si, à l'expiration d'un délai de trois mois à compter de la réception du projet, le ministre chargé de l'enseignement supérieur n'a pas notifié une opposition totale ou partielle de l'un ou l'autre ministre, l'accord envisagé peut être conclu.

Cet accord est établi pour une durée de cinq ans, renouvelable. En cas de renouvellement, il est à nouveau soumis à la procédure de communication.
