# Sous-section 1 : Coopération internationale des établissements.

- [Article D123-15](article-d123-15.md)
- [Article D123-16](article-d123-16.md)
- [Article D123-17](article-d123-17.md)
- [Article D123-18](article-d123-18.md)
- [Article D123-19](article-d123-19.md)
- [Article D123-20](article-d123-20.md)
- [Article D123-21](article-d123-21.md)
