# Section 2 : Mission de formation continue des adultes.

- [Article D122-4](article-d122-4.md)
- [Article D122-5](article-d122-5.md)
- [Article D122-6](article-d122-6.md)
- [Article D122-7](article-d122-7.md)
- [Article D122-8](article-d122-8.md)
- [Article D122-9](article-d122-9.md)
