# Section 1 bis : Mission de formation des sortants du système éducatif

- [Article D122-3-1](article-d122-3-1.md)
- [Article D122-3-2](article-d122-3-2.md)
- [Article D122-3-3](article-d122-3-3.md)
- [Article D122-3-4](article-d122-3-4.md)
- [Article D122-3-5](article-d122-3-5.md)
- [Article D122-3-6](article-d122-3-6.md)
- [Article D122-3-7](article-d122-3-7.md)
- [Article D122-3-8](article-d122-3-8.md)
