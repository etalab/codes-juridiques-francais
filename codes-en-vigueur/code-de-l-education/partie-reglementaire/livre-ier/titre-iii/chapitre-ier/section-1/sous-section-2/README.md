# Sous-section 2 : Contrôle de l'assiduité.

- [Article R131-5](article-r131-5.md)
- [Article R131-6](article-r131-6.md)
- [Article R131-7](article-r131-7.md)
- [Article R131-8](article-r131-8.md)
- [Article R131-9](article-r131-9.md)
- [Article R131-10](article-r131-10.md)
