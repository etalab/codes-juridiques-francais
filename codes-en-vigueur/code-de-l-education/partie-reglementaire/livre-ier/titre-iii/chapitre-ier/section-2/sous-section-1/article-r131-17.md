# Article R131-17

Tout personnel enseignant ou tout directeur d'un établissement d'enseignement privé qui, malgré un avertissement écrit du directeur académique des services de l'éducation nationale agissant sur délégation du recteur d'académie ou de son délégué, ne s'est pas conformé aux dispositions des articles R. 131-2 à R. 131-9 est, à la diligence du directeur académique des services de l'éducation nationale agissant sur délégation du recteur d'académie, déféré au conseil académique de l'éducation nationale qui peut prononcer les peines suivantes :

a) Le blâme avec ou sans publicité ;

b) En cas de récidive dans l'année scolaire, l'interdiction d'exercer sa profession soit temporairement soit définitivement.
