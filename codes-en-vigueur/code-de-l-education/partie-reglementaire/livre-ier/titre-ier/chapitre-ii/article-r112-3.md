# Article R112-3

Les conditions d'application des dispositions de l'article L. 112-2-2, relatives à l'éducation des jeunes sourds, sont fixées par les articles R. 351-21 à R. 351-26.
