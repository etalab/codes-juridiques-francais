# Paragraphe 1 : Dispositions relatives aux enseignants 
des classes sous contrat d'association.

- [Article R914-44](article-r914-44.md)
- [Article R914-45](article-r914-45.md)
- [Article R914-46](article-r914-46.md)
- [Article R914-47](article-r914-47.md)
- [Article R914-48](article-r914-48.md)
- [Article R914-49](article-r914-49.md)
- [Article R914-50](article-r914-50.md)
- [Article R914-51](article-r914-51.md)
- [Article R914-52](article-r914-52.md)
