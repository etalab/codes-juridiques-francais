# Article R914-15

Les maîtres qui exercent dans les classes de l'enseignement du premier degré doivent posséder soit le diplôme exigé pour l'accès définitif à l'échelle de rémunération des instituteurs, soit le certificat d'aptitude au professorat des écoles.
