# Sous-section 4 : Classement.

- [Paragraphe 1 : Classement des maîtres contractuels ou agréés.](paragraphe-1)
- [Paragraphe 2 : Reclassement pour motif médical.](paragraphe-2)
