# Article R914-72

Les promotions susceptibles d'être accordées à une catégorie de maîtres conformément au deuxième alinéa de l'article R. 914-66 qui ne pourraient être prononcées au titre de cette catégorie peuvent être transférées dans l'une des deux autres catégories et prononcées au titre de celles-ci.
