# Section 6 : Charges sociales.

- [Sous-section 1 : Dispositions générales.](sous-section-1)
- [Sous-section 2 : Dispositions relatives au régime des retraites complémentaires des personnels enseignants.](sous-section-2)
- [Sous-section 3 : Dispositions relatives au régime additionnel de retraite des personnels enseignants.](sous-section-3)
