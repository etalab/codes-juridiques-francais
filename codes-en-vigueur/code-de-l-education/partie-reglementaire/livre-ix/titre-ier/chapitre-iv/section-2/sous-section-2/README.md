# Sous-section 2 : La commission consultative mixte académique.

- [Article R914-7](article-r914-7.md)
- [Article R914-8](article-r914-8.md)
