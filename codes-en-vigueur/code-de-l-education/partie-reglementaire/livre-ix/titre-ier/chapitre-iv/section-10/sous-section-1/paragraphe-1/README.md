# Paragraphe 1 : Conditions requises et âge d'ouverture 
du droit aux avantages temporaires de retraite.

- [Article R914-121](article-r914-121.md)
- [Article R914-122](article-r914-122.md)
- [Article R914-123](article-r914-123.md)
