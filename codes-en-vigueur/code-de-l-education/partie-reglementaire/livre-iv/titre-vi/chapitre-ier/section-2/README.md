# Section 2 : Les établissements d'enseignement privés.

- [Article R461-8](article-r461-8.md)
- [Article R461-9](article-r461-9.md)
- [Article R461-10](article-r461-10.md)
- [Article R461-11](article-r461-11.md)
- [Article R461-13](article-r461-13.md)
- [Article R461-15](article-r461-15.md)
- [Article R461-16](article-r461-16.md)
- [Article R461-17](article-r461-17.md)
