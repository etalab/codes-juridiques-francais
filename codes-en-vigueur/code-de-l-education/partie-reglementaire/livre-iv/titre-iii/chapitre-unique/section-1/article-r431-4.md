# Article R431-4

Les règles relatives au recrutement et aux qualifications exigées du directeur et du personnel enseignant des centres de formation d'apprentis sont fixées par les articles R. 116-26 à R. 116-29 du code du travail.
