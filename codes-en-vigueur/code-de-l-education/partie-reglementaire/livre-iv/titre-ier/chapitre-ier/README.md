# Chapitre Ier : Organisation et fonctionnement  des écoles maternelles et élémentaires.

- [Article D411-1](article-d411-1.md)
- [Article D411-2](article-d411-2.md)
- [Article D411-3](article-d411-3.md)
- [Article D411-4](article-d411-4.md)
- [Article D411-6](article-d411-6.md)
- [Article D411-7](article-d411-7.md)
- [Article D411-8](article-d411-8.md)
- [Article D411-9](article-d411-9.md)
- [Article R411-5](article-r411-5.md)
