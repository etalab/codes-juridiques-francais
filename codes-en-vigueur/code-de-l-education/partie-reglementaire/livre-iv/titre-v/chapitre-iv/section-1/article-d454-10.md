# Article D454-10

A l'issue de chaque séance du conseil d'école, un procès-verbal de la réunion est dressé par son président, signé par celui-ci et contresigné par le secrétaire de séance et consigné dans un registre spécial conservé à l'école. Deux exemplaires du procès-verbal sont adressés au délégué à l'enseignement et un exemplaire est adressé au maire de la paroisse intéressée. Un exemplaire du procès-verbal est affiché en un lieu accessible aux parents d'élèves.
