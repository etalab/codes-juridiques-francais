# Chapitre Ier : L'ouverture des établissements  d'enseignement privés.

- [Section 1 : L'ouverture des établissements  d'enseignement du premier degré privés.](section-1)
- [Section 2 : L'ouverture des établissements d'enseignement  du second degré privés.](section-2)
- [Section 3 : L'ouverture des établissements d'enseignement technique privés.](section-3)
