# Section 2 : L'ouverture des établissements d'enseignement  du second degré privés.

- [Sous-section 1 : Délivrance des certificats de stage.](sous-section-1)
- [Sous-section 2 : Opposition à l'ouverture  d'un établissement d'enseignement secondaire privé.](sous-section-2)
- [Sous-section 3 : Dispositions particulières.](sous-section-3)
