# Section 1 : L'ouverture des établissements  d'enseignement du premier degré privés.

- [Sous-section 1 : Conditions générales d'ouverture.](sous-section-1)
- [Sous-section 2 : Conditions particulières d'ouverture  d'école primaire privée avec pensionnat.](sous-section-2)
