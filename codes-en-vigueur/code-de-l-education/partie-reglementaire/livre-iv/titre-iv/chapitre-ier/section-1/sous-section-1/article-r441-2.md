# Article R441-2

A l'expiration d'un délai de huit jours à compter de la réception de la déclaration, le maire fait savoir par écrit au recteur d'académie, qui en informe le préfet, au directeur académique des services de l'éducation nationale agissant sur délégation du recteur d'académie, ainsi qu'au demandeur, s'il s'oppose ou non à l'ouverture de l'école. Dans le cas où il fait opposition, sa décision est motivée.
