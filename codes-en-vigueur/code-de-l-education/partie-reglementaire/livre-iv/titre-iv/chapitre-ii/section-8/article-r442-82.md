# Article R442-82

Les compétences attribuées au recteur d'académie, au directeur académique des services de l'éducation nationale agissant sur délégation du recteur d'académie ou aux services académiques sont exercées sur le territoire des îles Saint-Pierre-et-Miquelon par le chef du service de l'enseignement.
