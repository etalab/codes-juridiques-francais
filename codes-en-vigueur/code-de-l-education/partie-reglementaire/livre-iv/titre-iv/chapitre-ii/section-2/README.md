# Section 2 : Demandes d'intégration d'établissements d'enseignement privés dans l'enseignement public.

- [Sous-section 1 : Dispositions générales.](sous-section-1)
- [Sous-section 2 : Dispositions relatives au personnel.](sous-section-2)
- [Sous-section 3 : Dispositions relatives  aux immeubles et au matériel.](sous-section-3)
