# Sous-section 1 : Dispositions générales.

- [Article R442-23](article-r442-23.md)
- [Article R442-24](article-r442-24.md)
- [Article R442-25](article-r442-25.md)
- [Article R442-26](article-r442-26.md)
