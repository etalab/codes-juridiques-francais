# Article R442-13

Le remboursement total ou partiel des charges sociales et fiscales, prévu par l'article 5 du décret n° 60-746 du 28 juillet 1960 mentionné à l'article R. 442-12, fait l'objet d'un titre de perception établi par l'ordonnateur. Ce titre de perception est recouvré par le trésorier-payeur général assignataire des dépenses et imputé au compte Dépenses des ministères annulées par suite de reversements de fonds ».
