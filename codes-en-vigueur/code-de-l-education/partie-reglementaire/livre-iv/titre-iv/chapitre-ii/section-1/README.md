# Section 1 : Contrôle de l'Etat sur les établissements  d'enseignement privés.

- [Sous-section 1 : Dispositions applicables à tous les établissements d'enseignement privés.](sous-section-1)
- [Sous-section 2 : Dispositions applicables aux établissements d'enseignement privés placés sous contrat d'association ou sous contrat simple.](sous-section-2)
- [Sous-section 3 : Dispositions applicables aux établissements d'enseignement privés hors contrat.](sous-section-3)
