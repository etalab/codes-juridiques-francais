# Article R442-61

Le préfet de département instruit la demande, en liaison avec l'autorité académique, et signe le contrat.
