# Article D492-13

Les règles relatives au conseil de discipline des collèges et des lycées de Mayotte sont fixées par l'article R. 562-3.
