# Chapitre IV : Dispositions applicables en Nouvelle-Calédonie.

- [Section 1 : Les collèges et les lycées.](section-1)
- [Section 2 : Les établissements d'enseignement privés.](section-2)
