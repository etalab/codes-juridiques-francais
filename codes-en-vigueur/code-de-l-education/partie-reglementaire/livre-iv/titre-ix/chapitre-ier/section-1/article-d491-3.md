# Article D491-3

Pour l'application à Wallis et Futuna de l'article D. 411-2, les quatorzième (6°) et quinzième (7°) alinéas sont supprimés.
