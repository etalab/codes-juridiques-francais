# Section 1 : Dispositions générales.

- [Article R425-1](article-r425-1.md)
- [Article R425-2](article-r425-2.md)
- [Article R425-3](article-r425-3.md)
- [Article R425-4](article-r425-4.md)
- [Article R425-5](article-r425-5.md)
- [Article R425-6](article-r425-6.md)
