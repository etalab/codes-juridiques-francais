# Article R425-12

Les décisions d'admission mentionnées à l'article R. 425-11 sont prises par le ministre de la défense.
