# Chapitre V : Les lycées de la défense.

- [Section 1 : Dispositions générales.](section-1)
- [Section 2 : Modalités d'admission et scolarité.](section-2)
- [Section 3 : Droits et obligations des élèves.](section-3)
- [Section 4 : Frais de trousseau et de pension.](section-4)
- [Section 5 : Comptes nominatifs des élèves.](section-5)
