# Sous-section 3 : La commission permanente.

- [Paragraphe 1 : Composition.](paragraphe-1)
- [Paragraphe 2 : Compétences.](paragraphe-2)
