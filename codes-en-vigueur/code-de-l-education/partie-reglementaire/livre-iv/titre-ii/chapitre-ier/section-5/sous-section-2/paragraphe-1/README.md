# Paragraphe 1 : Le chef d'établissement.

- [Article R421-83](article-r421-83.md)
- [Article R421-84](article-r421-84.md)
- [Article R421-85](article-r421-85.md)
- [Article R421-85-1](article-r421-85-1.md)
- [Article R421-86](article-r421-86.md)
- [Article R421-87](article-r421-87.md)
- [Article R421-88](article-r421-88.md)
