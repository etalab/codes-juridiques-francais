# Sous-paragraphe 4 : Election et désignation.

- [Article R421-97](article-r421-97.md)
- [Article R421-98](article-r421-98.md)
- [Article R421-99](article-r421-99.md)
- [Article R421-100](article-r421-100.md)
- [Article R421-101](article-r421-101.md)
- [Article R421-102](article-r421-102.md)
- [Article R421-103](article-r421-103.md)
- [Article R421-104](article-r421-104.md)
