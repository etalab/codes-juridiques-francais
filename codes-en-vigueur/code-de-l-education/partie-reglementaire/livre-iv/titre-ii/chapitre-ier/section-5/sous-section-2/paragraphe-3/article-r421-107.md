# Article R421-107

La composition et les compétences du conseil de discipline ainsi que les modalités d'appel de ses décisions sont fixées aux articles R. 511-24, R. 511-28 et R. 511-57.
