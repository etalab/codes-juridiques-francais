# Paragraphe 2 : La commission d'hygiène et de sécurité.

- [Sous-paragraphe 1 : Composition et désignation.](sous-paragraphe-1)
- [Sous-paragraphe 2 : Fonctionnement et compétences.](sous-paragraphe-2)
