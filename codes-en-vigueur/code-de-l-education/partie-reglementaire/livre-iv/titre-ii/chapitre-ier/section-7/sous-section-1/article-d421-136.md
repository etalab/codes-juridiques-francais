# Article D421-136

Les dispositions relatives à l'organisation générale des établissements, au déroulement de la scolarité, notamment en ce qui concerne la répartition des élèves dans les classes ou les groupes, au règlement intérieur et à la participation des parents d'élèves s'appliquent aux sections internationales. L'organisation des emplois du temps de l'ensemble des classes de l'établissement permet de regrouper les élèves des sections internationales pour les enseignements qui leur sont propres.
