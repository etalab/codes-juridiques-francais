# Sous-section 1 : Les groupements d'établissements.

- [Paragraphe 1 : Les groupements d'établissements relevant du ministère de l'éducation nationale](paragraphe-1)
- [Paragraphe 2 : Dispositions relatives aux établissements  d'enseignement agricole.](paragraphe-2)
- [Paragraphe 3 : Dispositions communes aux groupements d'établissements relevant du ministère de l'éducation nationale et du ministère de l'agriculture.](paragraphe-3)
