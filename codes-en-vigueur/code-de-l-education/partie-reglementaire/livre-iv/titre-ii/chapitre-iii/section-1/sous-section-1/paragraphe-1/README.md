# Paragraphe 1 : Les groupements d'établissements relevant du ministère de l'éducation nationale

- [Article D423-1](article-d423-1.md)
- [Article D423-2](article-d423-2.md)
- [Article D423-3](article-d423-3.md)
- [Article D423-4](article-d423-4.md)
- [Article D423-5](article-d423-5.md)
- [Article D423-6](article-d423-6.md)
- [Article D423-7](article-d423-7.md)
- [Article D423-8](article-d423-8.md)
- [Article D423-9](article-d423-9.md)
- [Article D423-10](article-d423-10.md)
- [Article D423-11](article-d423-11.md)
- [Article D423-12](article-d423-12.md)
