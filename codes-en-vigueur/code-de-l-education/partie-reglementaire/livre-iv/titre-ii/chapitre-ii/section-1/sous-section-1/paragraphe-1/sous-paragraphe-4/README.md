# Sous-paragraphe 4 : L'assemblée générale des délégués des élèves, le conseil des délégués  pour la vie lycéenne et le conseil de section internationale.

- [Article D422-34](article-d422-34.md)
- [Article D422-35](article-d422-35.md)
- [Article D422-36](article-d422-36.md)
- [Article D422-37](article-d422-37.md)
- [Article D422-38](article-d422-38.md)
- [Article D422-39](article-d422-39.md)
