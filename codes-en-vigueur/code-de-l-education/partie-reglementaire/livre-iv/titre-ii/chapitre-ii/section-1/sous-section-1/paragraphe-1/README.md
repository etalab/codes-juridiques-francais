# Paragraphe 1 : Organisation administrative.

- [Sous-paragraphe 1 : Le chef d'établissement.](sous-paragraphe-1)
- [Sous-paragraphe 2 : Le conseil d'administration.](sous-paragraphe-2)
- [Sous-paragraphe 3 : La commission permanente.](sous-paragraphe-3)
- [Sous-paragraphe 4 : L'assemblée générale des délégués des élèves, le conseil des délégués  pour la vie lycéenne et le conseil de section internationale.](sous-paragraphe-4)
- [Sous-paragraphe 5 : Autres conseils compétents en matière de scolarité.](sous-paragraphe-5)
