# Article D422-35

Dans les lycées, le conseil des délégués pour la vie lycéenne est composé de dix lycéens élus au scrutin plurinominal à un tour, dont trois élus pour un an par les délégués des élèves et sept élus pour deux ans par l'ensemble des élèves de l'établissement. En cas d'égalité des voix, le plus jeune des candidats est déclaré élu.

Pour chaque titulaire, un suppléant est élu dans les mêmes conditions. Lorsque le titulaire élu par l'ensemble des élèves de l'établissement est en dernière année de cycle d'études, son suppléant doit être inscrit dans une classe de niveau inférieur. Un membre suppléant ne peut siéger qu'en l'absence du titulaire. Lorsqu'un membre titulaire cesse d'être élève de l'établissement ou démissionne, il est remplacé par son suppléant pour la durée du mandat restant à courir.

Le mandat des membres du conseil expire le jour de la première réunion qui suit l'élection de la catégorie à laquelle ils appartiennent.

Le conseil est présidé par le chef d'établissement. Les représentants des lycéens élisent, parmi eux, un vice-président pour une durée d'un an.
