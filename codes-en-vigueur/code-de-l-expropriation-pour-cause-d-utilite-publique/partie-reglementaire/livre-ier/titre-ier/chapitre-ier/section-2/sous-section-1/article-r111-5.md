# Article R111-5

L'indemnisation du commissaire enquêteur ou des membres de la commission d'enquête est assurée dans les conditions prévues aux articles R. 123-25 à R. 123-27 du code de l'environnement.
