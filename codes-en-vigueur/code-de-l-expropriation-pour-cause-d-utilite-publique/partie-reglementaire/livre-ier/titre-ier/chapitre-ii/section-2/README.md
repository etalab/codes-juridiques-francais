# Section 2 : Dossier d'enquête

- [Article R112-4](article-r112-4.md)
- [Article R112-5](article-r112-5.md)
- [Article R112-6](article-r112-6.md)
- [Article R112-7](article-r112-7.md)
