# Section 1 : Autorité compétente pour ouvrir et organiser l'enquête

- [Article R112-1](article-r112-1.md)
- [Article R112-2](article-r112-2.md)
- [Article R112-3](article-r112-3.md)
