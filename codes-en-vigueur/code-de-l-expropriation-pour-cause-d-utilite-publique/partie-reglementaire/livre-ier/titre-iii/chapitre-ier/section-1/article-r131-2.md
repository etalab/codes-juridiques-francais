# Article R131-2

L'indemnisation du commissaire enquêteur ou des membres de la commission d'enquête est assurée soit dans les conditions prévues à l'article R. 111-5, lorsque l'enquête parcellaire est conduite en vue d'une expropriation pour cause d'utilité publique, soit dans les conditions prévues aux articles R. 111-6 à R. 111-9, lorsque l'enquête parcellaire n'est pas engagée à une telle fin.
