# Article R411-3

L'action en nullité prévue à l'article L. 411-3 est dispensée du ministère d'avocat.

.
