# Article R423-10

Les contestations relatives au relogement des locataires ou des occupants de locaux d'habitation ou à usage professionnel, en application du présent titre, relèvent de la compétence du juge de l'expropriation statuant en la forme des référés.
