# Section 3 : Dispositions communes

- [Article R323-13](article-r323-13.md)
- [Article R323-14](article-r323-14.md)
