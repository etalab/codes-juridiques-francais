# Section 6 : Dispositions diverses

- [Article R311-30](article-r311-30.md)
- [Article R311-31](article-r311-31.md)
- [Article R311-32](article-r311-32.md)
