# TITRE  IV : DROIT DE DÉLAISSEMENT ET DEMANDE D'EMPRISE TOTALE D'UN BIEN PARTIELLEMENT EXPROPRIÉ

- [Chapitre Ier : Droit de délaissement](chapitre-ier)
- [Chapitre II : Demande d'emprise totale d'un bien partiellement exproprié](chapitre-ii)
