# Article L122-4

Par dérogation aux principes du présent code, l'utilité publique des opérations secrètes intéressant la défense nationale peut, eu égard aux impératifs de la défense nationale, être régulièrement déclarée sans enquête préalable, sur l'avis conforme d'une commission.
