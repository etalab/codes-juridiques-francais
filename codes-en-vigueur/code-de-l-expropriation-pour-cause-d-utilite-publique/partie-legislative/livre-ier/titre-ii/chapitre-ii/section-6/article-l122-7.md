# Article L122-7

Lorsque les travaux ou les opérations à réaliser intéressent plusieurs personnes publiques, l'acte déclarant l'utilité publique précise celle qui est chargée de conduire la procédure d'expropriation.
