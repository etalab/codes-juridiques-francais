# Section 1 : Opération ayant une incidence sur l'environnement ou le patrimoine culturel

- [Article L122-1](article-l122-1.md)
- [Article L122-2](article-l122-2.md)
