# Chapitre Ier : Droit de délaissement

- [Article L241-1](article-l241-1.md)
- [Article L241-2](article-l241-2.md)
