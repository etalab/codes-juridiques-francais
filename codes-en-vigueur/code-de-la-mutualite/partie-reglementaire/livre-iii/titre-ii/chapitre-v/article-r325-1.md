# Article R325-1

Chaque adhérent reçoit un exemplaire de son contrat contenant les dispositions du règlement de la caisse autonome qui lui sont applicables.

En cas d'adhésion collective, il reçoit, à défaut des documents susmentionnés, une note d'information explicative détaillée.
