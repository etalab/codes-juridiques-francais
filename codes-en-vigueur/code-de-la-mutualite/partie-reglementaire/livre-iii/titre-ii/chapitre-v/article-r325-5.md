# Article R325-5

Tout adhérent à titre individuel à une garantie annuelle couvrant les risques mentionnés aux 2° et 3° de l'article R. 321-1 a la faculté d'y renoncer par lettre recommandée, avec demande d'avis de réception dans les trente jours suivant le paiement de la première cotisation.

Le règlement de la caisse, le contrat ou la note d'information explicative doivent comporter des indications précises sur les conditions d'exercice de cette renonciation. Le défaut de communication de ces documents proroge le délai prévu au premier alinéa ci-dessus, jusqu'au trentième jour suivant la date de leur remise effective à l'adhérent.

La renonciation entraîne la restitution de l'intégralité des cotisations versées, dans les trente jours à compter de la réception de la lettre recommandée. Les intérêts de retard au taux légal courent de plein droit à l'expiration de ce délai.
