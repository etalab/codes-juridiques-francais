# Article R321-1

Pour l'application des dispositions du présent titre, les risques mentionnés à l'article L. 321-1 sont classés comme suit :

1. Accidents.

2. Incapacité de travail ou invalidité résultant de la maladie et autres risques comportant le service de prestations au-delà d'un an.

3. Opérations comportant des engagements dont l'exécution dépend de la durée de la vie humaine (vieillesse, vie, décès).

4. Prévoyance collective mentionnée à l'article L. 121-1 (2e alinéa).

5. Réassurance d'opérations pratiquées par les caisses autonomes mutualistes.
