# Article R231-2

Le commissaire aux comptes est désigné par le ministre de la défense, en application de l'article L. 231-4 et par dérogation aux dispositions de l'article R. 125-4.
