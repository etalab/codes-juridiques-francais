# Article R231-1

Il est fait application aux mutuelles des militaires des articles L. 122-5, L. 122-7, L. 126-1, L. 126-4, L. 126-5, L. 411-6, L. 531-1, L. 531-2, L. 531-3, L. 531-4, L. 531-5 et des articles R. 122-1, R. 122-2, R. 126-1, R. 126-3, R. 126-4, R. 411-1, R. 531-1, R. 531-2 et R. 531-3, après avis du ministre de la défense.
