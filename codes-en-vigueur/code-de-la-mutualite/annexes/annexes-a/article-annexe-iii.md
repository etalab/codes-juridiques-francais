# Article Annexe III

État consolidé, établi sur la base du dernier exercice disponible et des engagements reçus par l'organisme repreneur à la date de dépôt du dossier, des éléments de la marge de solvabilité prévisionnelle des mutuelles et unions reprenant les engagements d'autres organismes mutualistes (en euros)

<table>
<tbody>
<tr>
<td colspan="2" valign="top" width="25%">
<p align="center">ORGANISMES</p>
<p align="center">(nom et numéro d'identification)</p>
</td>
<td valign="top" width="25%">
<p align="center">FONDS PROPRES à la fin du dernier exercice (hors augmentation prévue à la colonne suivante)</p>
</td>
<td valign="top" width="25%">
<p align="center">AUGMENTATION DES FONDS PROPRES par apports d'actifs au repreneur (1) </p>
<p align="center">avant la fin de l'exercice suivant celui de la reprise des engagements</p>
</td>
<td valign="top" width="25%">
<p align="center">TITRES PARTICIPATIFS</p>
<p align="center">ou subordonnés </p>
<p align="center">émis par le repreneur et non remboursés avant la fin de l'exercice suivant la reprise</p>
<p align="center">des engagements</p>
</td>
</tr>
<tr>
<td colspan="2" valign="top" width="25%">
<p align="center">Organisme repreneur</p>
<p align="center">(union ou mutuelle appelée à se substituer à d'autres organismes mutualistes)</p>
</td>
<td valign="top" width="25%">
<br/>
<br/>
</td>
<td valign="top" width="25%">
<br/>
<br/>
</td>
<td valign="top" width="25%">
<br/>
<br/>
</td>
</tr>
<tr>
<td valign="top" width="20%">
<p>Mutuelles et unions passant une convention de substitution avec l'organisme repreneur</p>
</td>
<td valign="top" width="5%">
<p align="center">A</p>
</td>
<td valign="top" width="25%"/>
<td valign="top" width="25%"/>
<td valign="top" width="25%">
<p>dont à durée indéterminée:</p>
</td>
</tr>
<tr>
<td valign="top" width="20%"/>
<td valign="top" width="5%">
<p align="center">B</p>
</td>
<td valign="top" width="25%"/>
<td valign="top" width="25%"/>
<td valign="top" width="25%">
<p>dont à durée indéterminée:</p>
</td>
</tr>
<tr>
<td valign="top" width="20%"/>
<td valign="top" width="5%">
<p align="center">Etc.</p>
</td>
<td valign="top" width="25%"/>
<td valign="top" width="25%"/>
<td valign="top" width="25%"/>
</tr>
<tr>
<td valign="top" width="20%">
<p>Mutuelles et unions ayant décidé de fusionner avec le repreneur</p>
</td>
<td valign="top" width="5%">
<p align="center">1</p>
</td>
<td valign="top" width="25%"/>
<td valign="top" width="25%"/>
<td valign="top" width="25%"/>
</tr>
<tr>
<td valign="top" width="20%"/>
<td valign="top" width="5%">
<p align="center">2</p>
</td>
<td valign="top" width="25%"/>
<td valign="top" width="25%"/>
<td valign="top" width="25%"/>
</tr>
<tr>
<td valign="top" width="20%"/>
<td valign="top" width="5%">
<p align="center">Etc.</p>
</td>
<td valign="top" width="25%"/>
<td valign="top" width="25%"/>
<td valign="top" width="25%"/>
</tr>
<tr>
<td valign="top" width="20%">
<p>Autres organismes souscripteurs de titres émis par le repreneur ou lui apportant des fonds propres</p>
</td>
<td valign="top" width="5%">
<p align="center">1°</p>
</td>
<td valign="top" width="25%"/>
<td valign="top" width="25%"/>
<td valign="top" width="25%">
<p>dont à durée indéterminée:</p>
</td>
</tr>
<tr>
<td valign="top" width="20%"/>
<td valign="top" width="5%">
<p align="center">2°</p>
</td>
<td valign="top" width="25%"/>
<td valign="top" width="25%"/>
<td valign="top" width="25%">
<p>dont à durée indéterminée:</p>
</td>
</tr>
<tr>
<td valign="top" width="20%"/>
<td valign="top" width="5%">
<p align="center">Etc.</p>
</td>
<td valign="top" width="25%"/>
<td valign="top" width="25%"/>
<td valign="top" width="25%"/>
</tr>
<tr>
<td valign="top" width="20%">
<p>Total consolidé</p>
<p>pour l'organisme repreneur</p>
</td>
<td valign="top" width="5%">
<br/>
<br/>
</td>
<td valign="top" width="25%"/>
<td valign="top" width="25%"/>
<td valign="top" width="25%">
<p>dont à durée indéterminée:</p>
</td>
</tr>
</tbody>
</table>

<div>
<table>
<tbody>
<tr>
<td valign="top" width="14%">
<p align="center">MARGE</p>
<p align="center">prévisionnelle</p>
</td>
<td valign="top" width="14%">
<p align="center">dont Fonds propres</p>
<p align="center">du dernier exercice (hors emprunts pour FE et FD)</p>
</td>
<td valign="top" width="14%">
<p align="center">dont EMPRUNTS</p>
<p align="center">pour fonds d'établissement (FE) ou de développement (FD)</p>
</td>
<td valign="top" width="14%">
<p align="center">TITRES PARTICIPATIFS</p>
<p align="center">ou subordonnés</p>
<p align="center">(émis ou à émettre)</p>
</td>
<td valign="top" width="14%">
<p align="center">APPORTS ultérieurs (réalisés</p>
<p align="center">ou promis)</p>
</td>
<td valign="top" width="14%">
<p align="center">dont RÉSULTATS prévisionnels</p>
</td>
<td valign="top" width="14%">
<p align="center">dont PLUS-VALUES LATENTES </p>
<p align="center">(2)</p>
</td>
</tr>
<tr>
<td colspan="7" valign="top" width="100%">
<br/>
<p>(1) Les cessions d'actifs au titre des provisions techniques ne sont pas comprises dans cette catégorie, ni les apports antérieurs de fonds propres (déjà inclus dans la colonne précédente).</p>
<p>(2) Détaillées par catégories de valeurs mobilières et justifiées par une attestation d'un commissaire aux comptes. </p>
</td>
</tr>
</tbody>
</table>
</div>
