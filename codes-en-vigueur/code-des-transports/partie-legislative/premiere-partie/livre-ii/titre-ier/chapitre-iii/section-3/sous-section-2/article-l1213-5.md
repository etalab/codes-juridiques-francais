# Article L1213-5

Les dispositions propres au schéma régional des infrastructures et des transports et au schéma régional de l'intermodalité de la collectivité territoriale de Corse sont énoncées au II de l'article L. 4424-10du code général des collectivités territoriales.
