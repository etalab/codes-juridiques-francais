# Article L1241-1

Le Syndicat des transports d'Ile-de-France est l'autorité organisatrice des services de transports publics réguliers de personnes dans la région Ile-de-France, y compris des services de transports publics réguliers de personnes fluviaux, sous réserve des pouvoirs dévolus à l'Etat en matière de police de la navigation.

Le syndicat peut y organiser des services de transports à la demande.

En outre, il y assure les missions et y développe les services mentionnés aux articles L. 1231-1, L. 1231-8 et L. 1231-14 à L. 1231-16.
