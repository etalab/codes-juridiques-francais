# Article L1251-2

Les règles relatives au transport par remontées mécaniques situées dans les zones de montagne définies à l'article 3 de la loi n° 85-30 du 9 janvier 1985 relative au développement et à la protection de la montagne sont fixées par les dispositions de la section 3 du chapitre 2 du titre IV du livre III du code du tourisme.
