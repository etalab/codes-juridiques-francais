# Article L1512-9

Le conseil d'administration règle, par ses délibérations, les affaires de l'établissement.

Il délibère, notamment, sur son budget, sur son compte financier ainsi que sur ses opérations financières.

Il arrête les concours financiers qu'il accorde en application de l'article L. 1512-7.

Le budget de l'établissement et ses comptes annuels font l'objet d'une approbation expresse par l'autorité compétente de l'Etat.

Le conseil d'administration établit son règlement intérieur.
