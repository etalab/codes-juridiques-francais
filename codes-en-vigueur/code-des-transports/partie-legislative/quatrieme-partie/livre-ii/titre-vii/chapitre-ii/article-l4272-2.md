# Article L4272-2

Les infractions définies par les règlements de police de la navigation intérieure peuvent être constatées par les personnels de Voies navigables de France commissionnés et assermentés dans des conditions fixées par décret en Conseil d'Etat.
