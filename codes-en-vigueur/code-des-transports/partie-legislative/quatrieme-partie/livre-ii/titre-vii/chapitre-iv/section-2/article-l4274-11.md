# Article L4274-11

Est puni de six mois d'emprisonnement et de 4 500 € d'amende le fait de conduire un bateau sans être titulaire d'un titre de conduite valable pour la voie d'eau parcourue et pour la catégorie du bateau conduit.
