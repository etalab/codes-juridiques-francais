# Chapitre IV : Sanctions pénales

- [Section 1 : Bateau et équipage](section-1)
- [Section 2 : Circulation](section-2)
- [Section 3 : Autres sanctions](section-3)
- [Article L4274-1](article-l4274-1.md)
