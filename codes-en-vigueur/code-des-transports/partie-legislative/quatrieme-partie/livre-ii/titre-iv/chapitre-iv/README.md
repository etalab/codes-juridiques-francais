# Chapitre IV : Déplacement d'office

- [Article L4244-1](article-l4244-1.md)
- [Article L4244-2](article-l4244-2.md)
