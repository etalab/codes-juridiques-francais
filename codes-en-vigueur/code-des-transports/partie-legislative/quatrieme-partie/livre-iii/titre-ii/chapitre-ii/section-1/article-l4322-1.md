# Article L4322-1

L'établissement public de l'Etat dénommé Port autonome de Paris est chargé, à l'intérieur de sa circonscription et dans les conditions définies par le présent chapitre :

1° De l'exploitation, de l'entretien et de la police de la conservation de toutes les installations portuaires publiques utilisées par la navigation de commerce ;

2° De la création, de l'extension, de l'amélioration, du renouvellement et de la reconstruction de ces installations portuaires.

Il veille à assurer une bonne desserte, notamment ferroviaire, des installations portuaires. Il peut par ailleurs entreprendre toute action susceptible de favoriser ou de promouvoir le développement de l'activité sur ces installations.

Il peut, après accord des collectivités territoriales intéressées, participer à toutes activités ayant pour objet l'utilisation ou la mise en valeur du domaine public fluvial dans le périmètre de sa circonscription.

Il peut créer, aménager, gérer et exploiter des installations utilisées par la navigation de plaisance.

Il est chargé de la gestion des immeubles qui lui sont affectés.

Il peut exercer, notamment par l'intermédiaire de prises de participations dans des personnes morales, des activités ou réaliser des acquisitions dont l'objet est de nature à concourir, à l'intérieur ou à l'extérieur de sa circonscription, au développement du port.
