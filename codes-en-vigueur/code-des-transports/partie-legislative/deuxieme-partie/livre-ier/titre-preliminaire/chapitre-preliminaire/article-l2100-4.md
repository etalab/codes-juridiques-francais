# Article L2100-4

Il est institué auprès de SNCF Réseau un comité des opérateurs du réseau, composé de représentants des entreprises ferroviaires, des exploitants d'infrastructures de service reliées au réseau ferré national, des autorités organisatrices de transport ferroviaire, des personnes autorisées à demander des capacités d'infrastructure ferroviaire et des personnes mentionnées au dernier alinéa de l'article L. 2111-1.

SNCF Réseau en assure le secrétariat.

Le comité des opérateurs du réseau constitue l'instance permanente de consultation et de concertation entre SNCF Réseau et ses membres. Il est informé des choix stratégiques effectués par les gestionnaires d'infrastructure mentionnés au dernier alinéa du même article L. 2111-1, dont SNCF Réseau, relatifs à l'accès au réseau ferré national et à son optimisation opérationnelle. Le contrat mentionné à l'article L. 2111-10 lui est transmis.

Selon des modalités fixées par décret en Conseil d'Etat, il adopte une charte du réseau destinée à faciliter les relations entre SNCF Réseau et les membres du comité et à favoriser une utilisation optimale du réseau ferré national, dans un souci d'efficacité économique et sociale et d'optimisation du service rendu aux utilisateurs. Cette charte et ses modifications sont soumises pour avis à l'Autorité de régulation des activités ferroviaires.

Sans préjudice des compétences exercées par l'Autorité de régulation des activités ferroviaires en application des articles L. 2134-1 à L. 2134-3 ou des voies de recours prévues par les lois, règlements et contrats, le comité des opérateurs du réseau peut être saisi, à fin de règlement amiable, des différends afférents à l'interprétation et à l'application de la charte du réseau mentionnée au quatrième alinéa du présent article.

Le comité se réunit au moins quatre fois par an et à l'initiative de SNCF Réseau ou d'un tiers au moins de ses membres.
