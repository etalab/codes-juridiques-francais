# Article L2102-1

L'établissement public national à caractère industriel et commercial dénommé " SNCF " a pour objet d'assurer :

1° Le contrôle et le pilotage stratégiques, la cohérence économique, l'intégration industrielle, l'unité et la cohésion sociales du groupe public ferroviaire ;

2° Des missions transversales nécessaires au bon fonctionnement du système de transport ferroviaire national, exercées au bénéfice de l'ensemble des acteurs de ce système, notamment en matière de gestion de crise et de préservation de la sûreté des personnes, des biens et du réseau ferroviaire, et de la sécurité, sans préjudice des missions de l'Etablissement public de sécurité ferroviaire définies à l'article L. 2221-1 ainsi qu'en matière de coordination des acteurs pour la mise en accessibilité du système de transport ferroviaire national aux personnes handicapées ou à mobilité réduite ;

3° La définition et l'animation des politiques de ressources humaines du groupe public ferroviaire, dont les politiques de gestion prévisionnelle des emplois et des compétences et de mobilité entre les différents établissements publics du groupe public ferroviaire ainsi que la négociation sociale d'entreprise, en veillant au respect de l'article L. 2101-2 ;

4° Des fonctions mutualisées exercées au bénéfice de l'ensemble du groupe public ferroviaire, dont la gestion des parcours professionnels et des mobilités internes au groupe pour les métiers à vocation transversale, l'action sociale, la santé, la politique du logement, la gestion administrative de la paie, l'audit et le contrôle des risques.

La SNCF ne peut exercer aucune des missions mentionnées aux articles L. 2111-9 et L. 2141-1.

Un décret en Conseil d'Etat précise les missions de la SNCF et leurs modalités d'exercice.
