# Article L2102-15

La SNCF coordonne la gestion domaniale au sein du groupe public ferroviaire. Elle est l'interlocuteur unique de l'Etat, des collectivités territoriales et des groupements de collectivités territoriales lorsque ceux-ci souhaitent acquérir, après déclassement, un bien immobilier appartenant à la SNCF ou à SNCF Réseau ou géré par SNCF Mobilités.
