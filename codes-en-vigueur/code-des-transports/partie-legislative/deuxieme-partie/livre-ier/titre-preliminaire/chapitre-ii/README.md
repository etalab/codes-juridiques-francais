# Chapitre II :  SNCF

- [Section 1 : Objet et missions](section-1)
- [Section 2 : Organisation](section-2)
- [Section 3 : Gestion financière et comptable](section-3)
- [Section 4 : Gestion domaniale](section-4)
- [Section 5 : Contrôle de l'Etat](section-5)
- [Section 6 : Ressources](section-6)
- [Section 7 : Réglementation sociale](section-7)
