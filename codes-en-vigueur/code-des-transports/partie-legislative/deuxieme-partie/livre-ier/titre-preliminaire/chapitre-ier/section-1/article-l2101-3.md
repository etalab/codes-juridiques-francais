# Article L2101-3

Par dérogation aux articles L. 2233-1 et L. 2233-3 du code du travail, pour les personnels de la SNCF, de SNCF Réseau et de SNCF Mobilités régis par un statut particulier, une convention de branche ou un accord professionnel ou interprofessionnel ayant fait l'objet d'un arrêté d'extension ou d'élargissement peut compléter les dispositions statutaires ou en déterminer les modalités d'application, dans les limites fixées par le statut particulier.
