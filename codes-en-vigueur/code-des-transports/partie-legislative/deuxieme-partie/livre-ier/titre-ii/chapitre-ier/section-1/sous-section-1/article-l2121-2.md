# Article L2121-2

La région est consultée sur les modifications de la consistance des services assurés dans son ressort territorial par la SNCF Mobilités, autres que les services d'intérêt régional au sens de l'article L. 2121-3.

Toute création ou suppression par SNCF Mobilités de la desserte d'un itinéraire par un service de transport d'intérêt national ou de la desserte d'un point d'arrêt par un service national ou international est soumise pour avis aux départements et communes concernés.

Toute suppression du service d'embarquement des vélos non démontés à bord des services de transport ferroviaire de voyageurs d'intérêt national est soumise pour avis aux régions concernées.
