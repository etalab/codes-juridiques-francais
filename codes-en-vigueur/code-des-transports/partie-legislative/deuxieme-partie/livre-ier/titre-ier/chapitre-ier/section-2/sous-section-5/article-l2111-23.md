# Article L2111-23

SNCF Réseau  est soumis au contrôle économique, financier et technique de l'Etat dans les conditions déterminées par voie réglementaire.
