# Article L2111-10-1

Les règles de financement des investissements de SNCF Réseau sont établies en vue de maîtriser sa dette, selon les principes suivants :

1° Les investissements de maintenance du réseau ferré national sont financés selon des modalités prévues par le contrat mentionné au premier alinéa de l'article L. 2111-10 ;

2° Les investissements de développement du réseau ferré national sont évalués au regard de ratios définis par le Parlement.

En cas de dépassement d'un de ces ratios, les projets d'investissements de développement sont financés par l'Etat, les collectivités territoriales ou tout autre demandeur.

En l'absence de dépassement d'un de ces ratios, les projets d'investissements de développement font l'objet, de la part de l'Etat, des collectivités territoriales ou de tout autre demandeur, de concours financiers propres à éviter toute conséquence négative sur les comptes de SNCF Réseau au terme de la période d'amortissement des investissements projetés.

Les règles de financement et les ratios mentionnés au premier alinéa et au 2° visent à garantir une répartition durable et soutenable du financement du système de transport ferroviaire entre gestionnaires d'infrastructure et entreprises ferroviaires, en prenant en compte les conditions de la concurrence intermodale.

Pour chaque projet d'investissement dont la valeur excède un seuil fixé par décret, l'Autorité de régulation des activités ferroviaires émet un avis motivé sur le montant global des concours financiers devant être apportés à SNCF Réseau et sur la part contributive de SNCF Réseau, au regard notamment des stipulations du contrat mentionné au premier alinéa de l'article L. 2111-10. Cet avis porte notamment sur la pertinence des prévisions de recettes nouvelles, en particulier au regard de leur soutenabilité pour les entreprises ferroviaires, ainsi que sur l'adéquation du niveau de ces recettes avec celui des dépenses d'investissement projetées.
