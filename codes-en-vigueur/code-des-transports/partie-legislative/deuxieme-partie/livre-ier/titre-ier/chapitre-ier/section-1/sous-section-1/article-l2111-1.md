# Article L2111-1

La consistance et les caractéristiques principales du réseau ferré national sont fixées par voie réglementaire dans les conditions prévues aux articles L. 1511-1 à L. 1511-3, L. 1511-6, L. 1511-7 et L. 1512-1.

SNCF Réseau est le propriétaire unique de l'ensemble des lignes du réseau ferré national.

Le gestionnaire du réseau ferré national mentionné à l'article L. 2111-9, les titulaires des concessions de travaux, des contrats de partenariat ou des délégations de service public mentionnés aux articles L. 2111-11 et L. 2111-12 ont la qualité de gestionnaire d'infrastructure.
