# Article L2141-8

Le président du conseil d'administration de SNCF Mobilités dirige l'établissement.
