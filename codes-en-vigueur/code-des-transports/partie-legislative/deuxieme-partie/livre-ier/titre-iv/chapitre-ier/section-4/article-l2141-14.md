# Article L2141-14

Les biens immobiliers acquis par       SNCF Mobilités  le sont au nom de l'Etat. Il verse à l'Etat une indemnité égale à la valeur vénale des biens appartenant déjà à l'Etat et qui sont incorporés dans le domaine public qu'il gère.
