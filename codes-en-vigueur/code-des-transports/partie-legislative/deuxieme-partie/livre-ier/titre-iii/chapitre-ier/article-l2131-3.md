# Article L2131-3

L'Autorité de régulation des activités ferroviaires veille en particulier à ce que les conditions d'accès au réseau ferroviaire par les entreprises ferroviaires n'entravent pas le développement de la concurrence.

Elle assure une mission générale d'observation des conditions d'accès au réseau ferroviaire et peut, à ce titre, après avoir procédé à toute consultation qu'elle estime utile des acteurs du secteur des transports ferroviaires, formuler et publier toute recommandation.
