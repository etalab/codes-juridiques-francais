# Article L2132-9

Le collège ne peut délibérer que si quatre au moins de ses membres sont présents. Les avis, décisions et recommandations sont pris à la majorité des membres présents. En cas de partage égal des voix, celle du président est prépondérante.
