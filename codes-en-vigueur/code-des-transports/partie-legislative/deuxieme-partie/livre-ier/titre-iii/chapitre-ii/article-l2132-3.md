# Article L2132-3

Les propositions, avis et décisions de l'Autorité de régulation des activités ferroviaires sont motivés et rendus publics, sous réserve des secrets protégés par la loi.
