# Article L2134-3

L'Autorité de régulation des activités ferroviaires examine toutes les demandes formulées au titre de l'article L. 2134-2. Elle engage l'instruction de chaque demande dans un délai d'un mois à compter de sa réception. Elle sollicite toutes informations utiles à l'instruction et procède aux consultations des parties concernées. Elle se prononce dans un délai maximal de six semaines à compter de la réception de l'ensemble des informations utiles à l'instruction de la demande.

Les décisions prises par l'Autorité de régulation des activités ferroviaires au titre de l'article L. 2134-2 sont susceptibles de recours en annulation ou en réformation dans un délai d'un mois à compter de leur notification. Ces recours relèvent de la compétence de la cour d'appel de Paris.

Le recours n'est pas suspensif. Toutefois, le sursis à exécution de la décision peut être ordonné par le juge, si celle-ci est susceptible d'entraîner des conséquences irréparables ou manifestement excessives ou s'il est survenu, postérieurement à sa notification, des faits nouveaux d'une exceptionnelle gravité.

Le pourvoi en cassation formé contre l'arrêt de la cour d'appel est exercé dans le délai d'un mois suivant la notification ou la signification de cet arrêt.
