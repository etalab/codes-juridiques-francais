# Sous-section 1 : Recherche et constatation des infractions

- [Article L2211-2](article-l2211-2.md)
- [Article L2211-3](article-l2211-3.md)
- [Article L2211-4](article-l2211-4.md)
