# Article L5112-2

Tous les navires battant pavillon français sont jaugés à l'exception des navires de plaisance dont la longueur, au sens de la convention internationale du 23 juin 1969 sur le jaugeage des navires, est inférieure à 24 mètres.

Les certificats de jauge sont délivrés par l'autorité administrative ou par des sociétés de classification habilitées dans des conditions fixées par décret en Conseil d'Etat. Ils peuvent faire l'objet de mesures de retrait.

Leur délivrance peut donner lieu à perception d'une rémunération.
