# Article L5623-9

Les rémunérations des gens de mer résidant hors de France ne peuvent être inférieures aux montants fixés par décret, après consultation des organisations professionnelles représentatives des armateurs et des organisations syndicales représentatives des gens de mer, par référence aux rémunérations généralement pratiquées ou recommandées sur le plan international.
