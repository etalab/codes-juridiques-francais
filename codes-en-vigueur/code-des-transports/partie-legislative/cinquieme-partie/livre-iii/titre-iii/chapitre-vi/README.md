# Chapitre VI : Sanctions administratives et dispositions pénales

- [Section 1 : Sanctions administratives](section-1)
- [Section 2 : Recherche, constatation et poursuite des infractions pénales](section-2)
- [Section 3 : Sanctions pénales](section-3)
