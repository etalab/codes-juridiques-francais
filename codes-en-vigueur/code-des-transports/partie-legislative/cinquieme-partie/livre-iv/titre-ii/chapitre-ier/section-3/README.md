# Section 3 : La responsabilité pour dommage aux bagages

- [Section 4 :  Droits et obligations des passagers](section-4)
- [Article L5421-9](article-l5421-9.md)
- [Article L5421-10](article-l5421-10.md)
- [Article L5421-11](article-l5421-11.md)
- [Article L5421-12](article-l5421-12.md)
