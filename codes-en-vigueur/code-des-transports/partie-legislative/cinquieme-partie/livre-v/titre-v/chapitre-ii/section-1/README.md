# Section 1 : Dispositions générales

- [Article L5552-1](article-l5552-1.md)
- [Article L5552-2](article-l5552-2.md)
- [Article L5552-3](article-l5552-3.md)
