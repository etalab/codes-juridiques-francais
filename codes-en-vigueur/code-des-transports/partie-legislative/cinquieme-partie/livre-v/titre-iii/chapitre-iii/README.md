# Chapitre III : Responsabilité de l'armateur

- [Article L5533-1](article-l5533-1.md)
- [Article L5533-2](article-l5533-2.md)
- [Article L5533-3](article-l5533-3.md)
- [Article L5533-4](article-l5533-4.md)
