# Article L5543-1-1

I. - La Commission nationale de la négociation collective maritime est chargée, sans préjudice des missions confiées à la commission prévue à l'
article L. 2271-1 du code du travail
:

1° De proposer au ministre chargé des gens de mer toutes mesures de nature à faciliter le développement de la négociation collective dans le secteur maritime ;

2° D'émettre un avis sur les projets de loi, d'ordonnance et de décret relatifs aux règles générales portant sur les relations individuelles et collectives du travail des gens de mer ;

3° De donner un avis motivé aux ministres chargés des gens de mer et du travail sur l'extension et l'élargissement des conventions et accords collectifs relevant de sa compétence, ainsi que sur l'abrogation des arrêtés d'extension ou d'élargissement ;

4° De donner, à la demande d'au moins la moitié des membres de la commission d'interprétation compétente préalablement saisie, un avis sur l'interprétation des clauses d'une convention ou d'un accord collectif ;

5° De suivre l'évolution des salaires effectifs et des rémunérations minimales déterminées par les conventions et accords collectifs relevant de sa compétence ;

6° D'examiner le bilan annuel de la négociation collective dans le secteur maritime ;

7° De suivre annuellement l'application dans les conventions collectives relevant de sa compétence du principe "à travail égal, salaire égal", du principe de l'égalité professionnelle entre les femmes et les hommes et du principe d'égalité de traitement entre les salariés, ainsi que des mesures prises en faveur du droit au travail des personnes en situation de handicap, de constater les inégalités éventuellement persistantes et d'en analyser les causes. La Commission nationale de la négociation collective maritime a qualité pour faire au ministre chargé des gens de mer toute proposition pour promouvoir dans les faits et dans les textes ces principes d'égalité.

II. - La Commission nationale de la négociation collective maritime comprend des représentants de l'Etat, du Conseil d'Etat, ainsi que des représentants des organisations d'employeurs représentatives au niveau national et des organisations syndicales de gens de mer représentatives au niveau national.

III. - Un décret en Conseil d'Etat détermine les modalités d'organisation et de fonctionnement de la Commission nationale de la négociation collective maritime.

IV. - Pour l'application de l'
article L. 2222-1 du code du travail
au présent livre, les conventions ou accords collectifs de travail concernant les gens de mer tiennent compte des conventions ou accords collectifs de travail conclus pour les personnels susceptibles de se voir appliquer plusieurs régimes conventionnels selon leur situation, à terre ou embarquée.
