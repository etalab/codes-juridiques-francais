# Section 5 : Salaire et avantages divers

- [Sous-section 1 : Détermination du salaire](sous-section-1)
- [Sous-section 2 : Paiement du salaire](sous-section-2)
- [Sous-section 3 : Protection du salaire](sous-section-3)
- [Sous-section 4 : Dispositions particulières au capitaine](sous-section-4)
