# Sous-section 1 : Obligations de l'employeur et des gens de mer

- [Article L5545-1](article-l5545-1.md)
- [Article L5545-2](article-l5545-2.md)
- [Article L5545-3](article-l5545-3.md)
- [Article L5545-3-1](article-l5545-3-1.md)
