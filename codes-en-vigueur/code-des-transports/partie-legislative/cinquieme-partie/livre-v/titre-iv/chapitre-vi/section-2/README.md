# Section 2 : Service public de l'emploi, recrutement et placement des gens de mer

- [Sous-section 1 : Services de recrutement et de placement privés](sous-section-1)
- [Sous-section 2 : Dispositions diverses](sous-section-2)
- [Article L5546-1](article-l5546-1.md)
