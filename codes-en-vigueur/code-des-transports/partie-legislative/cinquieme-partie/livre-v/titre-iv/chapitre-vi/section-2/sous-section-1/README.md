# Sous-section 1 : Services de recrutement et de placement privés

- [Article L5546-1-1](article-l5546-1-1.md)
- [Article L5546-1-2](article-l5546-1-2.md)
- [Article L5546-1-3](article-l5546-1-3.md)
- [Article L5546-1-4](article-l5546-1-4.md)
- [Article L5546-1-5](article-l5546-1-5.md)
- [Article L5546-1-6](article-l5546-1-6.md)
- [Article L5546-1-7](article-l5546-1-7.md)
