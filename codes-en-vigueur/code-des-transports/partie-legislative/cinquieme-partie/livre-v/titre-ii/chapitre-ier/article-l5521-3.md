# Article L5521-3

I. - A bord d'un navire battant pavillon français, l'accès aux fonctions de capitaine et d'officier chargé de sa suppléance est subordonné à :

1° La possession de qualifications professionnelles ;

2° La vérification d'un niveau de connaissance de la langue française ;

3° La vérification d'un niveau de connaissance des matières juridiques permettant la tenue de documents de bord et l'exercice des prérogatives de puissance publique dont le capitaine est investi.

II. - Un décret en Conseil d'Etat, pris après avis des organisations les plus représentatives d'armateurs et de gens de mer intéressées, précise les conditions d'application du présent article. Il détermine notamment les types de navigation ou de navire pour lesquels la présence à bord d'un officier chargé de la suppléance du capitaine n'est pas exigée.
