# Section 1 : Dispositions générales

- [Article L5243-1](article-l5243-1.md)
- [Article L5243-2](article-l5243-2.md)
- [Article L5243-2-1](article-l5243-2-1.md)
- [Article L5243-2-2](article-l5243-2-2.md)
- [Article L5243-2-3](article-l5243-2-3.md)
- [Article L5243-2-4](article-l5243-2-4.md)
- [Article L5243-3](article-l5243-3.md)
- [Article L5243-4](article-l5243-4.md)
- [Article L5243-5](article-l5243-5.md)
