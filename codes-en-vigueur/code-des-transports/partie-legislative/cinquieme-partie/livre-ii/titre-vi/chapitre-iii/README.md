# Chapitre III : Abordage, échouement et abandon

- [Article L5263-1](article-l5263-1.md)
- [Article L5263-2](article-l5263-2.md)
- [Article L5263-3](article-l5263-3.md)
- [Article L5263-4](article-l5263-4.md)
- [Article L5263-5](article-l5263-5.md)
- [Article L5263-6](article-l5263-6.md)
