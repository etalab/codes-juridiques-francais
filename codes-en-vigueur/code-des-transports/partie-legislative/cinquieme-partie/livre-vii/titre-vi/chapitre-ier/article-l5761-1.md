# Article L5761-1

Le livre Ier est applicable en Nouvelle-Calédonie, à l'exception du chapitre II du titre Ier et du chapitre III du titre II.

Le titre IV est applicable en Nouvelle-Calédonie, sous réserve des compétences dévolues à la collectivité en matière de police et de sécurité de la circulation maritime et de sauvegarde de la vie humaine en mer.
