# Article L5795-4

Pour l'application dans les Terres australes et antarctiques françaises de l'article L. 5542-18 :

1° A la fin du premier alinéa, les mots : " inscription au rôle d'équipage " sont remplacés par le mot : " embarquement " ;

2° A la fin du quatrième alinéa, les mots : " mentionné au III de l'article L. 5542-3" sont remplacés par les mots : " à la part ".
