# Article L3222-3

Pour prendre en compte la taxe prévue aux articles 269 à 283 quater du code des douanes acquittée par le transporteur, le prix de la prestation de transport routier de marchandises contractuellement défini fait l'objet de plein droit, pour la partie du transport effectuée sur le territoire métropolitain, quel que soit l'itinéraire emprunté, d'une majoration résultant de l'application d'un taux qui est fonction des régions de chargement et de déchargement des marchandises transportées et, pour les transports internationaux, à défaut de régions de chargement et de déchargement, des régions où se situent les points d'entrée et de sortie du territoire métropolitain.

Un taux uniforme est fixé, pour chaque région, pour les transports effectués à l'intérieur de cette seule région et pour les transports internationaux dont la partie effectuée sur le territoire métropolitain l'est à l'intérieur de cette seule région.

Un taux unique est fixé pour les transports effectués entre régions et pour les transports internationaux dont la partie effectuée sur le territoire métropolitain l'est sur plusieurs régions.

Les taux mentionnés aux deuxième et troisième alinéas du présent article sont compris entre 0 % et 7 %. Ils correspondent à l'évaluation de l'incidence moyenne de la taxe mentionnée au premier alinéa sur les coûts de transport compte tenu de la consistance du réseau soumis à cette taxe, des trafics de poids lourds et des itinéraires observés ainsi que du barème de cette taxe. Ils tiennent compte également des frais de gestion afférents à cette taxe supportés par les transporteurs. Ils sont fixés par arrêté du ministre chargé des transports.

La facture établie par le transporteur fait apparaître la majoration instituée par le premier alinéa du présent article.
