# Article L3521-3

Pour l'application du livre III de la présente partie du code à Mayotte :

1° Au premier alinéa de l'article L. 3312-1, les mots : "de l'article L. 3122-31 du code du travail" sont remplacés par les mots : "de l'article L. 213-3 du code du travail applicable à Mayotte" ;

2° L'article L. 3312-3 n'est pas applicable ;

3° L'article L. 3313-2 n'est pas applicable ;

4° Au premier alinéa de l'article L. 3315-1, les mots : "du livre Ier de la troisième partie du code du travail" sont remplacés par les mots : "aux dispositions des chapitres II et III du titre Ier et du titre II du livre II du code du travail applicable à Mayotte" ;

5° Au premier alinéa de l'article L. 3315-6, les mots : "aux titres II et III du livre Ier de la troisième partie du code du travail" sont remplacés par les mots : "aux chapitres II et III du titre Ier et aux chapitres Ier et II du titre II du livre II du code du travail applicable à Mayotte".
