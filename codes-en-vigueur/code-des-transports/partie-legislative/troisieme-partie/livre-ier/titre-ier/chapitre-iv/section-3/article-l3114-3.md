# Article L3114-3

Les articles L. 2242-4 (2° et 5°) et L. 2242-5 à L. 2242-7 sont applicables aux services de transport public routier de personnes réguliers et à la demande.
