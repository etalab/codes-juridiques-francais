# Article L3120-4

Les personnes qui fournissent des prestations mentionnées à l'article L. 3120-1 et celles qui les mettent en relation avec des clients, directement ou indirectement, doivent pouvoir justifier à tout moment de l'existence d'un contrat d'assurance couvrant leur responsabilité civile professionnelle.
