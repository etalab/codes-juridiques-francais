# Article L3122-2

Les conditions mentionnées à l'article L. 3122-1 incluent le prix total de la prestation, qui est déterminé lors de la réservation préalable mentionnée au 1° du II de l'article L. 3120-2. Toutefois, s'il est calculé uniquement en fonction de la durée de la prestation, le prix peut être, en tout ou partie, déterminé après la réalisation de cette prestation, dans le respect de l'article L. 113-3-1 du code de la consommation.
