# Section 3 : Dispositions relatives aux intermédiaires

- [Article L3122-5](article-l3122-5.md)
- [Article L3122-6](article-l3122-6.md)
