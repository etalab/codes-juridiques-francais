# Article L3121-11-2

Un intermédiaire proposant à des clients de réserver un taxi ne peut interdire à l'exploitant ou au conducteur d'un taxi de prendre en charge un client en étant arrêté ou stationné ou en circulant sur la voie ouverte à la circulation publique, y compris quand la sollicitation du taxi par le client est intervenue par voie de communications électroniques ou par l'intermédiaire d'un tiers.

Toute stipulation contractuelle contraire est réputée non écrite.

Les dispositions du présent article sont d'ordre public.
