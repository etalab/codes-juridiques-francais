# Section 1 : Constatation et poursuite des infractions

- [Article L6142-1](article-l6142-1.md)
- [Article L6142-2](article-l6142-2.md)
- [Article L6142-3](article-l6142-3.md)
