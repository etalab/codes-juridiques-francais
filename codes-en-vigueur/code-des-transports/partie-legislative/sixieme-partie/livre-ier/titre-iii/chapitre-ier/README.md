# Chapitre Ier : Responsabilité des équipages et des exploitants

- [Article L6131-1](article-l6131-1.md)
- [Article L6131-2](article-l6131-2.md)
- [Article L6131-3](article-l6131-3.md)
- [Article L6131-4](article-l6131-4.md)
