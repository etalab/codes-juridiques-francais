# Article L6221-1

Sont soumis au contrôle de l'autorité administrative les aéronefs et les autres produits, pièces et équipements, ainsi que les organismes et personnes soumis aux exigences techniques de sécurité et de sûreté fixées, par le présent livre, par le règlement (CE) n° 216/2008 du 20 février 2008 du Parlement européen et du Conseil concernant des règles communes dans le domaine de l'aviation civile et instituant une Agence européenne de la sécurité aérienne, ou le règlement (CE) n° 550/2004 du Parlement européen et du Conseil du 10 mars 2004 relatif à la fourniture de services de navigation aérienne dans le ciel unique européen.

L'autorité administrative peut soumettre à autorisation ces aéronefs, produits, pièces et équipements préalablement à leur utilisation ainsi que ces organismes et personnes préalablement à l'exercice de leurs activités.
