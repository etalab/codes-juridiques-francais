# Article A4241-35-3

<div align="center">Modalités d'information des préfets </div>
<div align="left"/>
<div align="left"/>
<div align="left">
<br/>
<p>Lorsque le déplacement couvre plusieurs départements, les préfets des départements traversés par le bateau bénéficiaire de l'autorisation spéciale de transport sont préalablement informés de la délivrance de ce document. </p>
<p>
<br/>
</p>
</div>
