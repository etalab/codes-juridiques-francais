# Article A4241-38-2

<div align="center">Demande d'autorisation </div>
<div align="left"/>
<div align="left"/>
<div align="left">
<br/>
<p>La demande d'autorisation est adressée, au moins trois mois avant la manifestation, par l'organisateur de la manifestation à l'autorité compétente mentionnée à l'article R. 4241-38, qui en accuse réception.</p>
<br/>
</div>
