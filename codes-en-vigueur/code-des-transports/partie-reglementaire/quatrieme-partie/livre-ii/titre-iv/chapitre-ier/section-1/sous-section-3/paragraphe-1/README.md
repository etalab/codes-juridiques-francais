# Paragraphe 1 : Dispositions générales

- [Article A4241-48-1](article-a4241-48-1.md)
- [Article A4241-48-2](article-a4241-48-2.md)
- [Article A4241-48-3](article-a4241-48-3.md)
- [Article A4241-48-4](article-a4241-48-4.md)
- [Article A4241-48-5](article-a4241-48-5.md)
- [Article A4241-48-6](article-a4241-48-6.md)
- [Article A4241-48-7](article-a4241-48-7.md)
