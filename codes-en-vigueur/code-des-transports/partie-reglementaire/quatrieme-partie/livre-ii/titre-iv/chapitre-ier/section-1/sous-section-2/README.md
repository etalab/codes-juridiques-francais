# Sous-section 2 : Dispositions relatives aux marques 
et aux échelles de tirant d'eau

- [Article A4241-47-1](article-a4241-47-1.md)
- [Article A4241-47-2](article-a4241-47-2.md)
