# Article A4241-63

<div align="center">Dépôt aux stations de réception </div>
<div align="left"/>
<div align="left">
<br/>
</div>
<div align="left">
<br/>
<p>Les déchets visés à l'article R. 4241-63 sont déposés, contre justificatif, à des intervalles réguliers, déterminés par l'état et l'exploitation du bateau. Ce justificatif consiste en une mention portée dans le carnet de contrôle des huiles usées par la station de réception.</p>
<p>
<br/>
</p>
</div>
