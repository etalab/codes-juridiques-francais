# Paragraphe 1 : Dispositions générales

- [Article A4241-53-1](article-a4241-53-1.md)
- [Article A4241-53-2](article-a4241-53-2.md)
- [Article A4241-53-3](article-a4241-53-3.md)
