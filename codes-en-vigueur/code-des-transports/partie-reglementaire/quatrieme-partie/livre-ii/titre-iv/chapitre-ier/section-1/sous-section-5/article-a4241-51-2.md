# Article A4241-51-2

<div align="center">Balisage </div>
<div align="left"/>
<div align="left">
<br/>
</div>
<div align="left">
<br/>
<p>En application de l'article R. 4241-51, l'annexe 8 définit les règles de balisage qui s'appliquent en amont du premier obstacle à la navigation des navires, déterminé en application de l'article L. 5000-1. Elle précise également dans quelles conditions les différentes marques de balisage sont utilisées.</p>
<p>
<br/>
</p>
</div>
