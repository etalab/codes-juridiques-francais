# Section 4 : Dispositions spéciales.

- [Article 242](article-242.md)
- [Article 246](article-246.md)
- [Article 247](article-247.md)
- [Article 250-1](article-250-1.md)
