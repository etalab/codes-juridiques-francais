# Chapitre Ier : Délégués mineurs du fond

- [Section 1 : Fonctions.](section-1)
- [Section 2 : Circonscriptions.](section-2)
- [Section 3 : Elections.](section-3)
- [Section 4 : Dispositions spéciales.](section-4)
