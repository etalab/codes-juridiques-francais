# Article L330-5

Sous réserve des dispositions des alinéas suivants, les informations nominatives figurant dans les pièces administratives exigées pour la circulation des véhicules ne peuvent être communiquées qu'aux destinataires mentionnés aux articles L. 330-2 à L. 330-4.

Ces informations nominatives sont également communicables à des tiers préalablement agréés par l'autorité administrative afin d'être réutilisées dans les conditions prévues au chapitre II du titre Ier de la loi n° 78-753 du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal :

-à des fins statistiques, ou à des fins de recherche scientifique ou historique, sans qu'il soit nécessaire de recueillir l'accord préalable des personnes concernées mais sous réserve que les études réalisées ne fassent apparaître aucune information nominative ;

-à des fins d'enquêtes et de prospections commerciales, sauf opposition des personnes concernées selon les modalités prévues au deuxième alinéa de l'article 38 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés ;

- à des fins de sécurisation des activités économiques qui nécessitent une utilisation de caractéristiques techniques des véhicules fiables, sans communication des nom, prénom et adresse des personnes concernées.

La décision d'agrément mentionnée au deuxième alinéa peut être précédée d'une enquête administrative, dans les conditions prévues par l'article 17-1 de la loi n° 95-73 du 21 janvier 1995 d'orientation et de programmation relative à la sécurité, pour des motifs d'intérêt général liés à la protection des personnes et des biens.
