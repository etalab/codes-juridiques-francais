# Article L323-1

I.-Lorsqu'en application du présent code, des véhicules sont astreints à un contrôle technique, celui-ci est effectué par les services de l'Etat ou par des contrôleurs agréés par l'Etat.

Cet agrément peut être délivré soit à des contrôleurs indépendants, soit à des contrôleurs organisés en réseaux d'importance nationale, sous réserve qu'ils n'aient fait l'objet d'aucune condamnation inscrite au bulletin n° 2 de leur casier judiciaire.

Les fonctions de contrôleur ainsi que les autres fonctions exercées dans ces réseaux sont exclusives de toute autre activité exercée dans la réparation ou le commerce automobile.

Les frais de contrôle sont à la charge du propriétaire du véhicule.

Un décret en Conseil d'Etat fixe les modalités de fonctionnement du système de contrôle et en particulier les conditions d'agrément des contrôleurs, des installations nécessaires au contrôle et des réseaux mentionnés au deuxième alinéa.

II.-Par dérogation au I, tout ressortissant d'un Etat membre de la Communauté européenne ou d'un autre Etat partie à l'Espace économique européen, légalement établi, pour l'exercice de la profession de contrôleur technique de véhicules, dans un de ces Etats, peut exercer cette activité de façon temporaire et occasionnelle en France.

Toutefois, lorsque cette activité ou la formation y conduisant n'est pas réglementée dans l'Etat d'établissement, le prestataire doit avoir exercé cette activité dans cet Etat pendant au moins deux ans au cours des dix années qui précèdent la prestation.

Lorsque le prestataire fournit pour la première fois des services en France, il en informe au préalable l'autorité compétente par une déclaration écrite, dans les conditions fixées par décret en Conseil d'Etat.
