# Article L211-1

En cas de commission des délits de violences ou d'outrage prévus par les articles 222-9 à 222-13 et 433-5 du code pénal contre un inspecteur du permis de conduire et de la sécurité routière dans l'exercice ou à l'occasion de l'exercice de ses fonctions, le tribunal peut prononcer la peine complémentaire d'interdiction de se présenter à l'examen du permis de conduire pour une durée de trois ans au plus.

Cette condamnation est portée à la connaissance du préfet du département concerné.
