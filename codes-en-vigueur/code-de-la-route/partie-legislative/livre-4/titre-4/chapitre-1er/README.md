# Chapitre 1er : Dispositions particulières à la collectivité territoriale de Saint-Pierre-et-Miquelon.

- [Article L441-1](article-l441-1.md)
- [Article L441-2](article-l441-2.md)
