# Article L130-7

Lorsqu'ils ne sont pas déjà assermentés, les agents qui ont compétence pour constater par procès-verbal les contraventions prévues à l'article L. 130-4 prêtent serment devant le juge du tribunal d'instance.

Ce serment, dont la formule est fixée par décret en Conseil d'Etat, est renouvelé en cas de changement de lieu d'affectation de l'intéressé.
