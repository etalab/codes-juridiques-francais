# Article R234-2

Les opérations de dépistage de l'imprégnation alcoolique par l'air expiré, prévues par les articles L. 234-3 à L. 234-5 et L. 234-9 sont effectuées au moyen d'un appareil conforme à un type homologué selon des modalités définies par arrêté du ministre chargé de la santé publique, après avis du ministre chargé des transports, du ministre de l'intérieur et du ministre de la défense.
