# Article R235-8

En cas de décès du ou des conducteurs impliqués, le prélèvement des échantillons biologiques et l'examen du corps sont effectués soit dans les conditions fixées par les articles R. 235-5 et R. 235-6, soit par un médecin légiste au cours de l'autopsie judiciaire.

Les méthodes particulières de prélèvement et de conservation des échantillons biologiques applicables en cas de décès du ou des conducteurs impliqués sont fixées par arrêté du ministre chargé de la santé, après avis du directeur général de l'Agence nationale de sécurité du médicament et des produits de santé.
