# Article R235-4

Les épreuves de dépistage réalisées à la suite d'un recueil de liquide biologique sont effectuées conformément aux méthodes et dans les conditions prescrites par un arrêté du ministre chargé de la santé, après avis du directeur général de l'Agence nationale de sécurité du médicament et des produits de santé, qui précise notamment les critères de choix des réactifs et le modèle des fiches présentant les résultats. Lorsqu'il s'agit d'un recueil salivaire, cet arrêté est également pris par le ministre de la justice et par le ministre de l'intérieur.

Ces fiches sont remises à l'officier ou l'agent de police judiciaire ou à l'agent de police judiciaire adjoint ou complétées par ces derniers lorsqu'il s'agit d'un recueil salivaire.
