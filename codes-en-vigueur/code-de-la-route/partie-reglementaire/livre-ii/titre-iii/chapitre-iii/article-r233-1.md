# Article R233-1

I.-Lorsque les dispositions du présent code l'exigent, tout conducteur ou, le cas échéant, tout accompagnateur d'un apprenti conducteur, est tenu de présenter à toute réquisition des agents de l'autorité compétente :

1° Tout titre justifiant de son autorisation de conduire ;

2° Le certificat d'immatriculation du véhicule et, le cas échéant, celui de la remorque si le poids total autorisé en charge (PTAC) de cette dernière excède 500 kilogrammes, ou de la semi-remorque s'il s'agit d'un véhicule articulé, ou les récépissés provisoires, ou les photocopies des certificats d'immatriculation dans les cas et dans les conditions prévues par un arrêté du ministre de la justice et du ministre de l'intérieur ;

3° Pour l'accompagnateur d'un apprenti conducteur assujetti à une obligation de formation, le permis de conduire de la catégorie exigée pour la conduite du véhicule, obtenu depuis au moins cinq ans, accompagné d'une attestation certifiant qu'il a suivi la formation spécifique prévue au 4° de l'article R. 211-3.

Pour les titulaires d'une autorisation d'enseigner la conduite en cours de validité, les délégués et inspecteurs du permis de conduire et de la sécurité routière, en leur qualité d'accompagnateur à titre non onéreux, l'attestation délivrée dans les conditions fixées par arrêté du ministre chargé de la sécurité routière ;

4° Dans les cas mentionnés aux II et III de l'article R. 221-8, une attestation de la formation pratique ou le document attestant d'une expérience de la conduite conforme aux conditions prévues par ces dispositions ;

5° Les documents attestant de l'équipement du véhicule d'un dispositif homologué d'antidémarrage par éthylotest électronique et de la vérification de son fonctionnement, lorsque le conducteur :

a) A été condamné à une peine d'interdiction de conduire un véhicule qui ne soit pas équipé par un professionnel agréé ou par construction d'un tel dispositif ; ou

b) Est soumis à l'obligation prévue au 4° bis de l'article 41-2 du code de procédure pénale ;

6° Un éthylotest dans les conditions prévues à l'article R. 234-7 ;

7° Le procès-verbal de contrôle technique périodique pour les véhicules mentionnés aux articles R. 323-23 et R. 323-25.

II.-En cas de perte ou de vol du titre justifiant de l'autorisation de conduire le récépissé de déclaration de perte ou de vol tient lieu de titre pendant un délai de deux mois au plus.

III.-Hors le cas prévu au 6° du I, le fait de ne pas présenter immédiatement aux agents de l'autorité compétente les éléments exigés par le présent article est puni de l'amende prévue pour les contraventions de la première classe.

IV.-Le fait, pour toute personne invitée à justifier dans un délai de cinq jours de la possession de son brevet de sécurité routière, de ne pas présenter ce document avant l'expiration de ce délai est puni de l'amende prévue pour les contraventions de la deuxième classe.

V.-Hors le cas prévu au 6° du I, le fait, pour toute personne invitée à justifier dans un délai de cinq jours de la possession des autorisations et pièces exigées par le présent article, de ne pas présenter ces documents avant l'expiration de ce délai est puni de l'amende prévue pour les contraventions de la quatrième classe.
