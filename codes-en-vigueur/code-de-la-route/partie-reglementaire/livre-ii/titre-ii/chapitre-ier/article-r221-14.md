# Article R221-14

I. - Postérieurement à la délivrance du permis, le préfet peut enjoindre à un conducteur de se soumettre à un contrôle médical :

1° Dans le cas où les informations en sa possession lui permettent d'estimer que l'état physique du titulaire du permis peut être incompatible avec le maintien de ce permis de conduire. Cet examen médical est réalisé par un médecin agréé consultant hors commission médicale ; au vu de l'avis médical émis, le préfet prononce, s'il y a lieu, soit la restriction de validité, la suspension ou l'annulation du permis de conduire, soit le changement de catégorie de ce titre ;

2° A tout conducteur impliqué dans un accident corporel de la circulation routière ;

3° Avant la restitution de son permis, à tout conducteur ou accompagnateur d'un élève conducteur à l'encontre duquel il a prononcé une mesure restrictive ou suspensive du droit de conduire pour l'une des infractions prévues par les articles L. 234-1 et L. 234-8, afin de déterminer si l'intéressé dispose des aptitudes physiques nécessaires à la conduite du véhicule. Cette mesure est prononcée, selon le cas, par le préfet du département de résidence du conducteur ou de l'accompagnateur de l'élève conducteur.

II. - Lorsque le titulaire du permis de conduire néglige ou refuse de se soumettre, dans les délais qui lui sont prescrits, au contrôle médical dans les conditions du présent article, le préfet peut prononcer ou maintenir la suspension du permis de conduire jusqu'à ce qu'un avis médical soit émis par le médecin agréé consultant hors commission médicale, ou par la commission médicale.
