# Chapitre IV : Dispositions applicables à la Polynésie française.

- [Article R244-1](article-r244-1.md)
- [Article R244-2](article-r244-2.md)
