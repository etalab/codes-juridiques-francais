# Chapitre II : Dispositions applicables à Mayotte.

- [Article R242-1](article-r242-1.md)
- [Article R242-2](article-r242-2.md)
- [Article R242-3](article-r242-3.md)
- [Article R242-4](article-r242-4.md)
- [Article R242-6](article-r242-6.md)
- [Article R242-7](article-r242-7.md)
