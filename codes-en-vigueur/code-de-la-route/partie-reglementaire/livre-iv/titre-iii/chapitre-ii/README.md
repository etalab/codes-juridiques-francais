# Chapitre II : Véhicules d'intérêt général

- [Section 1 : Véhicules d'intérêt général prioritaires.](section-1)
- [Section 2 : Véhicules d'intérêt général bénéficiant de facilités de passage.](section-2)
- [Section 3 : Autres véhicules d'intérêt général.](section-3)
