# Article R433-19

I. - Le programme, la durée et les modalités de mise en œuvre des formations prévues à l'article R. 433-18 sont fixés par arrêté du ministre chargé des transports.

II. - Les formations mentionnées à l'article R. 433-18 sont dispensées dans le cadre des établissements agréés mentionnés à l'article 15 du décret n° 2007-1340 du 11 septembre 2007 relatif à la qualification initiale et à la formation continue des conducteurs de certains véhicules affectés aux transports routiers de marchandises ou de voyageurs.

III. - L'organisme de formation délivre au conducteur ayant satisfait aux obligations de formation initiale ou continue mentionnées à l'article R. 433-18 une attestation dont le contenu est défini par arrêté du ministre chargé des transports.
