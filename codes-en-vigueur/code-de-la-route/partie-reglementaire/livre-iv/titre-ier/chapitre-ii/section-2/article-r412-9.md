# Article R412-9

En marche normale, tout conducteur doit maintenir son véhicule près du bord droit de la chaussée, autant que le lui permet l'état ou le profil de celle-ci.

Toutefois, un conducteur qui pénètre sur un carrefour à sens giratoire comportant plusieurs voies de circulation en vue d'emprunter une sortie située sur sa gauche par rapport à son axe d'entrée peut serrer à gauche.

Chaque manoeuvre de changement de voie à l'intérieur du carrefour à sens giratoire reste soumise aux règles de la priorité et doit être signalée aux autres conducteurs.

Le fait, pour tout conducteur, de ne pas maintenir, en marche normale, son véhicule près du bord droit de la chaussée est puni de l'amende prévue pour les contraventions de la deuxième classe.

Le fait, pour tout conducteur, de circuler, en marche normale, sur la partie gauche d'une chaussée à double sens de circulation est puni de l'amende prévue pour les contraventions de la quatrième classe.

Tout conducteur coupable de cette dernière infraction encourt également la peine complémentaire de suspension du permis de conduire pour une durée de trois ans au plus, cette suspension pouvant être limitée à la conduite en dehors de l'activité professionnelle.

Cette dernière contravention donne lieu de plein droit à la réduction de trois points du permis de conduire.
