# Article R412-17

Tout usager d'un ouvrage routier ouvert à la circulation publique et régulièrement soumis à péage doit, s'il n'est muni d'une autorisation spéciale, acquitter le montant du péage autorisé correspondant au parcours et à la catégorie du véhicule qu'il utilise.

Le fait, pour tout conducteur, de refuser d'acquitter le montant du péage ou de se soustraire d'une manière quelconque à ce paiement est puni de l'amende prévue pour les contraventions de la deuxième classe.
