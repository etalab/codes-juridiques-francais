# Titre III : Recherche et constatation des infractions.

- [Article R130-1](article-r130-1.md)
- [Article R130-1-1](article-r130-1-1.md)
- [Article R130-1-2](article-r130-1-2.md)
- [Article R130-2](article-r130-2.md)
- [Article R130-3](article-r130-3.md)
- [Article R130-4](article-r130-4.md)
- [Article R130-5](article-r130-5.md)
- [Article R130-6](article-r130-6.md)
- [Article R130-7](article-r130-7.md)
- [Article R130-8](article-r130-8.md)
- [Article R130-9](article-r130-9.md)
- [Article R130-10](article-r130-10.md)
