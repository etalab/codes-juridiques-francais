# Livre Ier : Dispositions générales

- [Titre Ier : Définitions.](titre-ier)
- [Titre II : Responsabilité.](titre-ii)
- [Titre III : Recherche et constatation des infractions.](titre-iii)
- [Titre IV : Dispositions relatives à l'outre-mer.](titre-iv)
