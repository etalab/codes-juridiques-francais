# Section 2 : Opposition au transfert du certificat d'immatriculation.

- [Article R322-15](article-r322-15.md)
- [Article R322-16](article-r322-16.md)
- [Article R322-17](article-r322-17.md)
- [Article R322-18](article-r322-18.md)
