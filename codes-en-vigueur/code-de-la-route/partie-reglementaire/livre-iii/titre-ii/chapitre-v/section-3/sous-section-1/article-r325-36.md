# Article R325-36

L'autorité dont relève la fourrière ne peut s'opposer à la demande d'autorisation provisoire de sortie de fourrière présentée par le propriétaire du véhicule en vue exclusivement de faire procéder aux travaux reconnus indispensables par l'expert. Il en est de même lorsque le propriétaire du véhicule fait procéder à une contre-expertise, aux réparations remettant le véhicule en état de circuler dans des conditions normales de sécurité ainsi qu'au contrôle technique du véhicule dans un centre agréé.

Cette autorisation provisoire de sortie de fourrière, dont le modèle est fixé par arrêté du ministre de l'intérieur, qui tient lieu de pièce de circulation et qui est limitée au temps des parcours nécessaires et des opérations précitées, peut prescrire un itinéraire et des conditions de sécurité.

Le réparateur doit remettre au propriétaire du véhicule une facture détaillée certifiant l'exécution des travaux prescrits en application du 2° du I de l'article R. 325-30.
