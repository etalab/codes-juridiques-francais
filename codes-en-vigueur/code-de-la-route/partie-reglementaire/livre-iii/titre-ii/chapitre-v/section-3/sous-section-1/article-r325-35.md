# Article R325-35

En cas de désaccord sur l'état du véhicule ou sur la décision de classement visée à l'article R. 325-30, le propriétaire a la faculté de faire procéder à une contre-expertise.

La contre-expertise est faite par un expert choisi sur la liste visée à l'article R. 325-30.

Dans le cas où la contre-expertise confirme l'expertise initiale, les frais d'expertise et de contre-expertise sont à la charge du propriétaire. Dans le cas contraire, ces frais incombent à l'autorité dont relève la fourrière.
