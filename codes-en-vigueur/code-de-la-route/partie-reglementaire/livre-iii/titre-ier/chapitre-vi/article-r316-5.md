# Article R316-5

A l'exception des quadricycles, des véhicules à deux ou trois roues et des véhicules ou matériels agricoles ou de travaux publics autres que les tracteurs agricoles, tout véhicule à moteur dont le poids à vide excède 350 kilogrammes doit être muni de dispositifs de marche arrière.

Le fait de contrevenir aux dispositions du présent article est puni de l'amende prévue pour les contraventions de la troisième classe.
