# Article R313-6

Feux de position latéraux.

I. - Tout véhicule à moteur ou toute remorque, dont la longueur est supérieure à 6 mètres, à l'exception des châssis-cabines et des véhicules agricoles ou forestiers, doit être muni de feux de position latéraux.

II. - Tout véhicule à moteur ou toute remorque, d'une longueur inférieure ou égale à 6 mètres, tout autobus peut être muni de ces feux.

III. - Le fait, pour tout conducteur, de contrevenir aux dispositions du I ci-dessus est puni de l'amende prévue pour les contraventions de la troisième classe.
