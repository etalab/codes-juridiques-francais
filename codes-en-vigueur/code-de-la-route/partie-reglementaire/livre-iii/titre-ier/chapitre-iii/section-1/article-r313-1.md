# Article R313-1

Tout véhicule ne peut être pourvu que des dispositifs d'éclairage ou de signalisation prévus au présent code. Ceux-ci doivent être installés conformément aux prescriptions du présent chapitre.

Ces dispositions ne concernent pas l'éclairage intérieur des véhicules sous réserve qu'il ne soit pas gênant pour les autres conducteurs.

Le fait, pour tout conducteur d'un véhicule à moteur ou à traction animale, de contrevenir aux dispositions du présent article est puni de l'amende prévue pour les contraventions de la troisième classe.

Le fait, pour tout conducteur d'un cycle, de contrevenir aux dispositions du présent article est puni de l'amende prévue pour les contraventions de la première classe.
