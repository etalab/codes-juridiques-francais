# Article R313-19

Catadioptres latéraux.

I. - Tout véhicule à moteur dont la longueur dépasse 6 mètres, toute remorque, tout cyclomoteur à deux roues doit être muni d'un ou de deux catadioptres latéraux, non triangulaires, de couleur orangée.

II. - Tout autre véhicule à moteur peut être muni d'un ou de deux catadioptres latéraux, non triangulaires, de couleur orangée.

III. - Tout cycle doit être muni de catadioptres orange visibles latéralement.

IV. - Le fait, pour tout conducteur d'un véhicule à moteur, de contrevenir aux dispositions du présent article est puni de l'amende prévue pour les contraventions de la troisième classe.

V. - Le fait, pour tout conducteur d'un cycle, de contrevenir aux dispositions du présent article est puni de l'amende prévue pour les contraventions de la première classe.
