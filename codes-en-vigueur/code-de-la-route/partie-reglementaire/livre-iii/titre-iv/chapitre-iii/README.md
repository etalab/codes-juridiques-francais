# Chapitre III : Dispositions applicables à la Polynésie française

- [Section 1 : Dispositions générales.](section-1)
- [Section 2 : Immobilisation.](section-2)
- [Section 3 : Fourrière.](section-3)
