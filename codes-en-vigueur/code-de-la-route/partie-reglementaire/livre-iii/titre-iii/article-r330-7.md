# Article R330-7

Les personnes souhaitant bénéficier des dispositions des trois derniers alinéas de l'article L. 330-5 demandent au ministre de l'intérieur la délivrance d'une licence dans les conditions prévues à l'article 16 de la loi n° 78-753 du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal. La licence vaut agrément au sens de l'article L. 330-5.

La licence est dite statistique si elle est demandée aux fins prévues au troisième alinéa de l'article L. 330-5. Elle est dite commerciale si elle est demandée aux fins prévues à son quatrième alinéa.
