# Section II : Taxes sur le chiffre d'affaires et taxes assimilées

- [I : Régime général](i)
- [II : Régime spécial des acomptes provisionnels](ii)
- [II bis : Régime spécial des exploitants agricoles](ii-bis)
- [II quinquies : Régime spécial des redevables de la taxe sur la publicité diffusée par les chaînes de télévision](ii-quinquies)
- [II sexies : Régime spécial des redevables de la taxe sur les services fournis par les opérateurs de communications électroniques](ii-sexies)
- [III bis : Régime simplifié](iii-bis)
- [IV ter : Paiement de la taxe sur la valeur ajoutée par virement ou par télérèglement](iv-ter)
- [V : Modalités d'application](v)
