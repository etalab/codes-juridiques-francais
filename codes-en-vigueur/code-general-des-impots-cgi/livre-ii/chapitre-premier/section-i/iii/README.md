# III : Paiement de l'impôt

- [1 : Dispositions générales](1)
- [4 : Paiement de la taxe d'habitation, des taxes foncières et de la contribution à l'audiovisuel public due par les particuliers](4)
- [4 bis : Paiement de la cotisation foncière des entreprises et des taxes additionnelles](4-bis)
- [6 : Impôts acquittés par télérèglement](6)
