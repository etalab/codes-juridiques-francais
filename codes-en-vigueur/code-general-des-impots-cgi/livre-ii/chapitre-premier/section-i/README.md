# Section I : Impôts directs et taxes assimilées

- [I : Rôles et avis d'imposition](i)
- [II : Exigibilité de l'impôt](ii)
- [III : Paiement de l'impôt](iii)
- [IV : Obligations des tiers](iv)
- [V : Solidarité entre époux et partenaires liés par un pacte civil de solidarité](v)
