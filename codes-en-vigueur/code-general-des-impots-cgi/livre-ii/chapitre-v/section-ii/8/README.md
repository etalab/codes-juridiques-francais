# 8 : Dispositions particulières aux droits d'enregistrement, à la taxe de publicité foncière, aux droits de timbre et à la taxe spéciale sur les conventions d'assurances

- [Article 1961](article-1961.md)
- [Article 1961 bis](article-1961-bis.md)
- [Article 1961 ter](article-1961-ter.md)
- [Article 1962](article-1962.md)
- [Article 1963](article-1963.md)
- [Article 1964](article-1964.md)
- [Article 1965](article-1965.md)
- [Article 1965 C](article-1965-c.md)
- [Article 1965 A](article-1965-a.md)
- [Article 1965 B](article-1965-b.md)
- [Article 1965 E](article-1965-e.md)
