# Article 1929 septies

Conformément au quatrième alinéa de l'article L. 626-6 du code de commerce, et au I de l'article L. 631-19 du même code, les administrations financières peuvent, dans le cadre du plan de sauvegarde ou du plan de redressement prévus respectivement aux articles L. 626-1 et L. 631-2 du même code ou, en vertu de l'article L. 351-4 du code rural et de la pêche maritime, lorsqu'un règlement amiable prévu à l'article L. 351-1 de ce code est arrêté, décider des cessions de rang de privilège ou d'hypothèque ou de l'abandon de ces sûretés.
