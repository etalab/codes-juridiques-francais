# 3 : Sanctions pénales

- [Article 1771](article-1771.md)
- [Article 1772](article-1772.md)
- [Article 1773](article-1773.md)
- [Article 1774](article-1774.md)
- [Article 1775](article-1775.md)
- [Article 1776](article-1776.md)
- [Article 1777](article-1777.md)
- [Article 1778](article-1778.md)
- [Article 1783 A](article-1783-a.md)
- [Article 1783 B](article-1783-b.md)
