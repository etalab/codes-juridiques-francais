# 4 : Autres sanctions et mesures diverses

- [Article 1822](article-1822.md)
- [Article 1823](article-1823.md)
- [Article 1824](article-1824.md)
- [Article 1825](article-1825.md)
- [Article 1825 C](article-1825-c.md)
- [Article 1825 D](article-1825-d.md)
- [Article 1825 A](article-1825-a.md)
- [Article 1825 B](article-1825-b.md)
- [Article 1825 F](article-1825-f.md)
