# Section I : Dispositions communes

- [C : Sanctions pénales](c)
- [D : Recouvrement et contentieux des pénalités et solidarité](d)
- [A : Intérêt de retard](a)
- [E : Mesures diverses](e)
