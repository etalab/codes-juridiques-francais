# Code général des impôts

- [Livre II : Recouvrement de l'impôt](livre-ii)
- [Livre premier : Assiette et liquidation de l'impôt](livre-premier)
