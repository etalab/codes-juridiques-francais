# Chapitre premier : Commissions administratives des impôts et comité de l'abus de droit fiscal

- [I : Commission communale des impôts directs](i)
- [II : Commission départementale des impôts directs et des taxes sur le chiffre d'affaires](ii)
- [II bis : Commission nationale des impôts directs et des taxes sur le chiffre d'affaires](ii-bis)
- [II ter : Dispositions communes aux commissions mentionnées aux articles 1651 et 1651 H](ii-ter)
- [III : Commissions centrales des impôts directs](iii)
- [IV : Dispositions communes aux commissions visées aux articles 1650 à 1652 bis](iv)
- [V : Commission départementale de Conciliation (Voir les articles 349 à 350 C de l'annexe III)](v)
- [VI : Comité de l'abus de droit fiscal](vi)
