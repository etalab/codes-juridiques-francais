# Article 1065

Sous réserve des dispositions de l'article 1020, les transferts de portefeuilles de contrats et des réserves mobilières ou immobilières afférentes à ces contrats sont exonérés de tous droits d'enregistrement lorsqu'ils sont faits en vertu des dispositions des articles L. 324-1 et L. 326-13 du code des assurances.

Lorsqu'ils sont faits en vertu des dispositions du 5° du I de l'article L. 612-33 du code monétaire et financier, les transferts de portefeuilles de contrats et des réserves mobilières ou immobilières afférentes à ces contrats sont exonérés de tous droits d'enregistrement et de la taxe de publicité foncière.
