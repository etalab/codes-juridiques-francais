# A : Obligations des redevables

- [1° : Actes sous seings privés - Dépôt d'un double au bureau](1)
- [2° : Affirmation de sincérité](2)
- [3° : Déclaration estimative](3)
