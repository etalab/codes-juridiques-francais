# Article 810 bis

Les apports réalisés lors de la constitution de sociétés sont exonérés des droits fixes de 375 € ou de 500 € prévus au I bis de l'article 809 et à l'article 810.

Les autres dispositions figurant dans les actes et déclarations ainsi que leurs annexes établis à l'occasion de la constitution de sociétés dont les apports sont exonérés en application du premier alinéa sont dispensées du droit fixe prévu à l'article 680.
