# B : Régimes spéciaux et exonérations

- [1 : Mutations d'une nature particulière](1)
- [2 : Mutations soumises à une taxation réduite ou exonérées](2)
