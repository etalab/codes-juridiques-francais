# Cession par acte passé à l'étranger de participations dans des sociétés immobilières étrangères dont l'actif est principalement constitué d'immeubles ou de droits immobiliers situés en france

- [Article 718 bis](article-718-bis.md)
