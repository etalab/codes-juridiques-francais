# 3° : Titres, sommes ou valeurs en dépôt. Sommes dues à raison du décès. Obligations des dépositaires ou débiteurs

- [Article 806](article-806.md)
- [Article 807](article-807.md)
- [Article 808](article-808.md)
