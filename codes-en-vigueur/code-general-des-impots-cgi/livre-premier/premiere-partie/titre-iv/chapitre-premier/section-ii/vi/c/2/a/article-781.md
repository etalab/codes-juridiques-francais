# Article 781

Est compté comme enfant vivant ou représenté de l'héritier, donataire ou légataire pour l'application de l'article 780, l'enfant qui :

1° Est décédé après avoir atteint l'âge de 16 ans révolus ;

2° Etant âgé de moins de 16 ans, a été tué par l'ennemi au cours des hostilités, ou est décédé des suites de faits de guerre, soit durant les hostilités, soit dans l'année à compter de leur cessation.

Le bénéfice de cette disposition est subordonné à la production dans le premier cas, d'une expédition de l'acte de décès de l'enfant et, dans le second cas, d'un acte de notoriété délivré sans frais par le juge du tribunal d'instance du domicile du défunt et établissant les circonstances de la blessure ou de la mort.
