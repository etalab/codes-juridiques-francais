# 2 : Dispositions spéciales aux successions

- [1° : Immeubles et fonds de commerce situés en France et dévolus à des personnes domiciliées à l'étranger - Obligations imposées à l'acquéreur](1)
- [2° : Polices d'assurances contre l'incendie souscrites par les personnes décédées - Avis à donner par les assureurs](2)
- [3° : Titres, sommes ou valeurs en dépôt. Sommes dues à raison du décès. Obligations des dépositaires ou débiteurs](3)
- [Article 800](article-800.md)
- [Article 801](article-801.md)
- [Article 802](article-802.md)
