# Article 780

Lorsqu'un héritier, donataire ou légataire a trois enfants ou plus, vivants ou représentés au jour de la donation ou au moment de l'ouverture de ses droits à la succession, il bénéficie, sur l'impôt à sa charge liquidé conformément aux dispositions des articles 777,779,
788,790 B, 790 D, 790 E et 790 F d'une réduction de 100 % qui ne peut, toutefois, excéder 305 € par enfant en sus du deuxième. Ce maximum est porté à 610 € en ce qui concerne les donations et successions en ligne directe et les donations entre époux ou partenaires liés par un pacte civil de solidarité.

Le bénéfice de cette disposition est subordonné à la production soit d'un certificat de vie dispensé d'enregistrement, pour chacun des enfants vivants des héritiers, donataires ou légataires et des représentants de ceux prédécédés, soit d'une expédition de l'acte de décès de tout enfant décédé depuis l'ouverture de la succession.
