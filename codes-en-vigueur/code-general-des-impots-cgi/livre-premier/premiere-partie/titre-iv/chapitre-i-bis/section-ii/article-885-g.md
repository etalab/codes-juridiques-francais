# Article 885 G

Les biens ou droits grevés d'un usufruit, d'un droit d'habitation ou d'un droit d'usage accordé à titre personnel sont compris dans le patrimoine de l'usufruitier ou du titulaire du droit pour leur valeur en pleine propriété. Toutefois, les biens grevés de l'usufruit ou du droit d'usage ou d'habitation sont compris respectivement dans les patrimoines de l'usufruitier ou du nu-propriétaire suivant les proportions fixées par l'article 669 dans les cas énumérés ci-après, et à condition, pour l'usufruit, que le droit constitué ne soit ni vendu, ni cédé à titre gratuit par son titulaire :

a. Lorsque la constitution de l'usufruit résulte de l'application des articles 767,1094 ou 1098 du code civil. Les biens dont la propriété est démembrée en application d'autres dispositions, et notamment de l'article 1094-1 du code civil, ne peuvent faire l'objet de cette imposition répartie.

b. Lorsque le démembrement de propriété résulte de la vente d'un bien dont le vendeur s'est réservé l'usufruit, le droit d'usage ou d'habitation et que l'acquéreur n'est pas l'une des personnes visées à l'article 751 ;

c. Lorsque l'usufruit ou le droit d'usage ou d'habitation a été réservé par le donateur d'un bien ayant fait l'objet d'un don ou legs à l'Etat, aux départements, aux communes ou syndicats de communes et à leurs établissements publics, aux établissements publics nationaux à caractère administratif et aux associations reconnues d'utilité publique.
