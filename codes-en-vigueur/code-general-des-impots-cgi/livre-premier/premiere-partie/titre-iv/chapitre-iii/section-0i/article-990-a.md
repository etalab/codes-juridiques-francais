# Article 990 A

Les bons mentionnés au 2° du III bis de l'article 125 A et les titres de même nature, quelle que soit leur date d'émission, sont, lorsque le détenteur n'autorise pas l'établissement qui assure le paiement des intérêts à communiquer son identité et son domicile fiscal à l'administration fiscale, soumis d'office à un prélèvement. Ce prélèvement est assis sur leur montant nominal.

Les bons et titres mentionnés au 2° du III bis de l'article 125 A ainsi que les bons et contrats de capitalisation mentionnés à l'article 125-0 A et les placements de même nature émis ou souscrits à compter du 1er janvier 1998 sont soumis d'office à un prélèvement assis sur leur montant nominal, lorsque le souscripteur et le bénéficiaire, s'il est différent, n'ont pas autorisé, lors de la souscription, l'établissement auprès duquel les bons, titres ou contrats ont été souscrits à communiquer leur identité et leur domicile fiscal à l'administration fiscale ou lorsque le bon, titre ou contrat a été cédé.

Les dispositions du deuxième alinéa ne sont applicables que si la cession des bons ou contrats de capitalisation souscrits à titre nominatif par une personne physique ne résulte pas d'une transmission entre vifs ou à cause de mort ayant fait l'objet d'une déclaration à l'administration fiscale.

Un décret fixe les modalités d'application des deuxième et troisième alinéas.
