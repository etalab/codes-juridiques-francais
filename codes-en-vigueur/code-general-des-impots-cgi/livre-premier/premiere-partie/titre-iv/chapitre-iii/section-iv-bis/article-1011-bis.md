# Article 1011 bis

I. ― Il est institué une taxe additionnelle à la taxe sur les certificats d'immatriculation des véhicules prévue à l'article 1599 quindecies.

La taxe est due sur le premier certificat d'immatriculation délivré en France pour un véhicule de tourisme au sens de l'article 1010.

La taxe n'est pas due :

a) Sur les certificats d'immatriculation des véhicules immatriculés dans le genre " Véhicule automoteur spécialisé " ou voiture particulière carrosserie " Handicap " ;

b) Sur les certificats d'immatriculation des véhicules acquis par une personne titulaire de la carte d'invalidité mentionnée à l'article L. 241-3 du code de l'action sociale et des familles ou par une personne dont au moins un enfant mineur ou à charge, et du même foyer fiscal, est titulaire de cette carte.

Le b ne s'applique qu'à un seul véhicule par bénéficiaire.

II. ― La taxe est assise :

a) Pour les véhicules de tourisme au sens de l'article 1010 qui ont fait l'objet d'une réception communautaire au sens de la directive 2007/46/ CE du Parlement européen et du Conseil, du 5 septembre 2007, précitée, sur le nombre de grammes de dioxyde de carbone émis par kilomètre ;

b) Pour les véhicules de tourisme au sens de l'article 1010 autres que ceux mentionnés au a, sur la puissance administrative.

III. ― Le tarif de la taxe est le suivant :

a) Pour les véhicules de tourisme au sens de l'article 1010 mentionnés au a du II :

<table>
<tbody>
<tr>
<th>
<br/>TAUX D'ÉMISSION DE DIOXYDE DE CARBONE <br/>
<p>(en grammes par kilomètre) <br/>
</p>
</th>
<th>
<br/>TARIF DE LA TAXE <br/>
<p>(en euros) <br/>
</p>
</th>
</tr>
<tr>
<td align="center">
<br/>Taux ≤ 130 <br/>
</td>
<td align="center">
<br/>0 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>130 &lt; taux ≤ 135 <br/>
</td>
<td align="center">
<br/>150 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>135 &lt; taux ≤ 140 <br/>
</td>
<td align="center">
<br/>250 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>140 &lt; taux ≤ 145 <br/>
</td>
<td align="center">
<br/>500 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>145 &lt; taux ≤ 150 <br/>
</td>
<td align="center">
<br/>900 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>150 &lt; taux ≤ 155 <br/>
</td>
<td align="center">
<br/>1 600 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>155 &lt; taux ≤ 175 <br/>
</td>
<td align="center">
<br/>2 200 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>175 &lt; taux ≤ 180 <br/>
</td>
<td align="center">
<br/>3 000 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>180 &lt; taux ≤ 185 <br/>
</td>
<td align="center">
<br/>3 600 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>185 &lt; taux ≤ 190 <br/>
</td>
<td align="center">
<br/>4 000 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>190 &lt; taux ≤ 200 <br/>
</td>
<td align="center">
<br/>6 500 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>200 &lt; taux <br/>
</td>
<td align="center">
<br/>8 000 <br/>
</td>
</tr>
</tbody>
</table>

Pour la détermination des tarifs mentionnés au tableau ci-dessus, le taux d'émission de dioxyde de carbone des véhicules est diminué de 20 grammes par kilomètre par enfant à charge au sens de l'article L. 521-1 du code de la sécurité sociale, à compter du troisième enfant et pour un seul véhicule de cinq places assises et plus par foyer.

Cette réduction fait l'objet d'une demande de remboursement auprès du service mentionné sur l'avis d'impôt sur le revenu du redevable de la taxe mentionnée au I. Le remboursement est égal à la différence entre le montant de la taxe acquitté au moment de l'immatriculation du véhicule et le montant de la taxe effectivement dû après application de la réduction du taux d'émission de dioxyde de carbone prévue par enfant à charge. Un décret fixe les conditions dans lesquelles sont adressées les demandes de remboursement, et notamment les pièces justificatives à produire.

b) Pour les véhicules de tourisme au sens de l'article 1010 mentionnés au b du II :

<table>
<tbody>
<tr>
<th>
<br/>PUISSANCE FISCALE <br/>
<p>(en chevaux-vapeur) <br/>
</p>
</th>
<th>
<br/>TARIF DE LA TAXE <br/>
<p>(en euros) <br/>
</p>
</th>
</tr>
<tr>
<td align="center">
<br/>Puissance fiscale ≤ 5 <br/>
</td>
<td align="center">
<br/>0 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>6 ≤ puissance fiscale ≤ 7 <br/>
</td>
<td align="center">
<br/>1 500 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>8 ≤ puissance fiscale ≤ 9 <br/>
</td>
<td align="center">
<br/>2 000 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>10 ≤ puissance fiscale ≤ 11 <br/>
</td>
<td align="center">
<br/>3 600 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>12 ≤ puissance fiscale ≤ 16 <br/>
</td>
<td align="center">
<br/>6 000 <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>16 &lt; puissance fiscale <br/>
</td>
<td align="center">
<br/>8 000 <br/>
</td>
</tr>
</tbody>
</table>

<div align="left">Pour les véhicules introduits en France après avoir été immatriculés dans un autre pays, la taxe est réduite d'un dixième par année entamée depuis cette immatriculation. <br/>
</div>
<div align="left">Les véhicules spécialement équipés pour fonctionner au moyen du superéthanol E85 mentionné au tableau B du 1 de l'article 265 du code des douanes bénéficient d'un abattement de 40 % sur les taux d'émissions de dioxyde de carbone, au sens de la directive 2007/46/ CE, du 5 septembre 2007, précitée, figurant dans le tableau mentionné au a. Cet abattement ne s'applique pas aux véhicules dont les émissions de dioxyde de carbone sont supérieures à 250 grammes par kilomètre. <br/>
</div>
<div align="left">IV. ― La taxe est recouvrée selon les mêmes règles et dans les mêmes conditions que la taxe prévue à l'article 1599 quindecies.</div>
