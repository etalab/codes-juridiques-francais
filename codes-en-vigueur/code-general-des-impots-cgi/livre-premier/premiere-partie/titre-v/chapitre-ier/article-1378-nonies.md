# Article 1378 nonies

Si la commission nationale des comptes de campagne et des financements politiques constate qu'un parti ou groupement politique manque aux obligations prévues à l'article 11-7 de la loi n° 88-227 du 11 mars 1988 relative à la transparence financière de la vie politique, les dons et cotisations à son profit ne peuvent, à compter de l'année suivante, ouvrir droit à la réduction d'impôt prévue au 3 de l'article 200 du présent code.
