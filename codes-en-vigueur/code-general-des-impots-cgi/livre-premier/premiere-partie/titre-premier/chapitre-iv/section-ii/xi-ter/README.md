# XI ter : Régime fiscal de certaines sociétés anonymes, sociétés par actions simplifiées et sociétés à responsabilité limitée. Option pour le régime des sociétés de personnes

- [Article 239 bis AB](article-239-bis-ab.md)
