# 1° : Financement en capital d'oeuvres cinématographiques ou audiovisuelles

- [Article 238 bis HE](article-238-bis-he.md)
- [Article 238 bis HF](article-238-bis-hf.md)
- [Article 238 bis HG](article-238-bis-hg.md)
- [Article 238 bis HH](article-238-bis-hh.md)
- [Article 238 bis HI](article-238-bis-hi.md)
- [Article 238 bis HJ](article-238-bis-hj.md)
- [Article 238 bis HK](article-238-bis-hk.md)
- [Article 238 bis HL](article-238-bis-hl.md)
- [Article 238 bis HM](article-238-bis-hm.md)
