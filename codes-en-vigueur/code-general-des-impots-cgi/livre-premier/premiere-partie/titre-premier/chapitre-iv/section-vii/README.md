# Section VII : Dispositions spéciales applicables aux opérations de privatisation

- [1° : Plus-values réalisées par les entreprises](1)
- [2° : Plus-values réalisées par les particuliers](2)
