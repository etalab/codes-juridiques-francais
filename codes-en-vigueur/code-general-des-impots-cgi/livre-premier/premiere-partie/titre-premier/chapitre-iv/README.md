# Chapitre IV : Dispositions communes aux impôts et taxes, revenus et bénéfices visés aux chapitres I à III

- [Section II : Impôt sur le revenu et impôt sur les sociétés](section-ii)
- [Section V : Impôt sur le revenu, impôt sur les sociétés et taxes visées au chapitre III](section-v)
- [Section VI : Dispositions spéciales applicables aux opérations de nationalisation](section-vi)
- [Section VII : Dispositions spéciales applicables aux opérations de privatisation](section-vii)
