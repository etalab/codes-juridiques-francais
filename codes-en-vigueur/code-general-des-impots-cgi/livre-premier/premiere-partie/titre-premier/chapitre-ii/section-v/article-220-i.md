# Article 220 I

Le crédit d'impôt défini à l'article 244 quater H est imputé sur l'impôt sur les sociétés dû par l'entreprise au titre des exercices au cours desquels les dépenses définies au II de l'article 244 quater H ont été exposées. Si le montant du crédit d'impôt excède l'impôt dû au titre dudit exercice, l'excédent est restitué.
