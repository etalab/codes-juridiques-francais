# Article 216

I. Les produits nets des participations, ouvrant droit à l'application du régime des sociétés mères et visées à l'article 145, touchés au cours d'un exercice par une société mère, peuvent être retranchés du bénéfice net total de celle-ci, défalcation faite d'une quote-part de frais et charges.

La quote-part de frais et charges visée au premier alinéa est fixée uniformément à 5 % du produit total des participations, crédit d'impôt compris.

Dans le cas mentionné au dernier alinéa du 1 de l'article 145, les deux premiers alinéas du présent I s'appliquent à la part de bénéfice du constituant déterminée dans les conditions prévues à l'article 238 quater F correspondant aux produits nets des titres de participation ouvrant droit à l'application du régime des sociétés mères précité.

II. (Abrogé à compter de la détermination des résultats des exercices ouverts à compter du 1er janvier 1993).

III. (Périmé).
