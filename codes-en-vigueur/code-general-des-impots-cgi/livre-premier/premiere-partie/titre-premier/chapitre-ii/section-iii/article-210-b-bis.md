# Article 210 B bis

1. Les titres représentatifs d'un apport partiel d'actif ou d'une scission grevés de l'engagement de conservation de trois ans mentionné à l'article 210 B peuvent être apportés, sans remise en cause du régime prévu à l'article 210 A ou sans que l'amende prévue à l'article 1734 ter A ne soit appliquée, sous réserve du respect des conditions suivantes :

a. Les titres sont apportés dans le cadre d'une fusion, d'une scission ou d'un apport partiel d'actif placé sous le régime de l'article 210 A ;

b. La société bénéficiaire de l'apport conserve les titres reçus jusqu'à l'expiration du délai de conservation prévu à l'article 210 B.

L'engagement de conservation est souscrit dans l'acte d'apport par les sociétés apporteuse et bénéficiaire de l'apport.

En cas d'apports successifs au cours du délai de conservation prévu à l'article 210 B, toutes les sociétés apporteuses et bénéficiaires des apports doivent souscrire cet engagement dans le même acte pour chaque opération d'apport.

2. Le non-respect de l'une des dispositions prévues au 1 entraîne la déchéance rétroactive du régime de l'article 210 A appliqué à l'opération initiale d'apport partiel d'actif rémunérée par les titres grevés de l'engagement de conservation.

La société bénéficiaire de l'apport qui ne souscrit pas l'engagement de conservation ou ne respecte pas, totalement ou partiellement, l'obligation de conservation des titres représentatifs d'une scission est seule redevable de l'amende prévue à l'article 1734 ter A. La société apporteuse, ou les sociétés apporteuses en cas d'apports successifs, sont solidairement responsables du paiement de cette amende.
