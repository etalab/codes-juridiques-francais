# Section II : Champ d'application de l'impôt

- [I : Sociétés et collectivités imposables.](i)
- [II : Exonérations et régimes particuliers.](ii)
