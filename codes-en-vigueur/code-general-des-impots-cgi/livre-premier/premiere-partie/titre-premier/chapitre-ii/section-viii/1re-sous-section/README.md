# 1re Sous-section : Dispositions générales

- [1° : Résultat d'ensemble](1)
- [2° : Plus-values ou moins-values d'ensemble](2)
- [3° : Non-imputation des déficits et des moins-values par les sociétés du groupe](3)
- [4° : Cessions d'immobilisations entre sociétés du groupe](4)
- [5° : Report en arrière des déficits](5)
- [Article 223 A](article-223-a.md)
- [Article 223 A bis](article-223-a-bis.md)
