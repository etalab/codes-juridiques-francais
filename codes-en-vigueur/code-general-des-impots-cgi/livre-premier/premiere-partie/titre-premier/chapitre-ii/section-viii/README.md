# Section VIII : Groupes de sociétés

- [1re Sous-section : Dispositions générales](1re-sous-section)
- [2e Sous-section : Sort des déficits et moins-values subis par la société avant son entrée ou après sa sortie du groupe](2e-sous-section)
- [3e Sous-section : Dispositions diverses](3e-sous-section)
