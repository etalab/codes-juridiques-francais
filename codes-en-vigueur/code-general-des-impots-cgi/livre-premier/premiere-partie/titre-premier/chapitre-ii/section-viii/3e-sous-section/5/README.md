# 5° : Conséquences de la sortie du groupe d'une société ou de la cessation du régime de groupe

- [Article 223 R](article-223-r.md)
- [Article 223 S](article-223-s.md)
