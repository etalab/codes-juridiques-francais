# Article 200 quater

1. Les contribuables domiciliés en France au sens de l'article 4 B peuvent bénéficier d'un crédit d'impôt sur le revenu au titre des dépenses effectivement supportées pour la contribution à la transition énergétique du logement dont ils sont propriétaires, locataires ou occupants à titre gratuit et qu'ils affectent à leur habitation principale. (1)

Ce crédit d'impôt s'applique :

a. (Abrogé)

b. Aux dépenses afférentes à un immeuble achevé depuis plus de deux ans, payées entre le 1er janvier 2005 et le 31 décembre 2015, au titre de :

1° L'acquisition de chaudières à condensation ;

2° L'acquisition de matériaux d'isolation thermique des parois vitrées, de volets isolants ou de portes d'entrée donnant sur l'extérieur ;

3° L'acquisition et la pose de matériaux d'isolation thermique des parois opaques, dans la limite d'un plafond de dépenses par mètre carré, fixé par arrêté conjoint des ministres chargés de l'énergie, du logement et du budget, ainsi que l'acquisition de matériaux de calorifugeage de tout ou partie d'une installation de production ou de distribution de chaleur ou d'eau chaude sanitaire ;

4° L'acquisition d'appareils de régulation de chauffage ;

c. Au coût des équipements de production d'énergie utilisant une source d'énergie renouvelable, à l'exception des équipements de production d'électricité utilisant l'énergie radiative du soleil, ou des pompes à chaleur, autres que air/ air, dont la finalité essentielle est la production de chaleur ou d'eau chaude sanitaire, ainsi qu'au coût de la pose de l'échangeur de chaleur souterrain des pompes à chaleur géothermiques, dans la limite d'un plafond de dépenses par mètre carré pour les équipements de production d'énergie utilisant l'énergie solaire thermique, fixé par arrêté conjoint des ministres chargés de l'énergie, du logement et du budget :

1° Payés entre le 1er janvier 2005 et le 31 décembre 2012 dans le cadre de travaux réalisés dans un logement achevé ;

2° Intégrés à un logement acquis neuf entre le 1er janvier 2005 et le 31 décembre 2012 ;

3° Intégrés à un logement acquis en l'état futur d'achèvement ou que le contribuable fait construire, achevé entre le 1er janvier 2005 et le 31 décembre 2012 ;

4° Payés entre le 1er janvier 2013 et le 31 décembre 2015 dans le cadre de travaux réalisés dans un logement achevé depuis plus de deux ans ;

d) Au coût des équipements de raccordement à un réseau de chaleur, alimenté majoritairement par des énergies renouvelables ou par une installation de cogénération ou, dans un département d'outre-mer, par des équipements de raccordement à un réseau de froid, alimenté majoritairement par du froid d'origine renouvelable ou de récupération : (1)

1° Payés entre le 1er janvier 2006 et le 31 décembre 2012 dans le cadre de travaux réalisés dans un logement achevé ;

2° Intégrés à un logement acquis neuf entre le 1er janvier 2006 et le 31 décembre 2012 ;

3° Intégrés à un logement acquis en l'état futur d'achèvement ou que le contribuable fait construire, achevé entre le 1er janvier 2006 et le 31 décembre 2012 ;

4° Payés entre le 1er janvier 2013 et le 31 décembre 2015 dans le cadre de travaux réalisés dans un logement achevé depuis plus de deux ans ;

e) Abrogé

f) Aux dépenses afférentes à un immeuble achevé depuis plus de deux ans, payées entre le 1er janvier 2009 et le 31 décembre 2015, au titre de :

1° (Abrogé)

2° La réalisation, en dehors des cas où la réglementation le rend obligatoire, du diagnostic de performance énergétique défini à l'article L. 134-1 du code de la construction et de l'habitation. Pour un même logement, un seul diagnostic de performance énergétique ouvre droit au crédit d'impôt par période de cinq ans.

g) Aux dépenses afférentes à un immeuble achevé depuis plus de deux ans, payées entre le 1er janvier 2012 et le 31 décembre 2015, au titre de chaudières à micro-cogénération gaz d'une puissance de production électrique inférieure ou égale à 3 kilovolt-ampères par logement ;

h) Aux dépenses afférentes à un immeuble achevé depuis plus de deux ans, payées entre le 1er septembre 2014 et le 31 décembre 2015, au titre de l'acquisition d'appareils permettant d'individualiser les frais de chauffage ou d'eau chaude sanitaire dans un bâtiment équipé d'une installation centrale ou alimenté par un réseau de chaleur ;

i) Aux dépenses afférentes à un immeuble achevé depuis plus de deux ans, payées entre le 1er septembre 2014 et le 31 décembre 2015, au titre de l'acquisition d'un système de charge pour véhicule électrique ;

j) Aux dépenses afférentes à un immeuble achevé depuis plus de deux ans situé à La Réunion, en Guyane, en Martinique, en Guadeloupe ou à Mayotte, payées entre le 1er septembre 2014 et le 31 décembre 2015, au titre de l'acquisition d'équipements ou de matériaux de protection des parois vitrées ou opaques contre les rayonnements solaires ;

k) Aux dépenses afférentes à un immeuble achevé depuis plus de deux ans situé à La Réunion, en Guyane, en Martinique, en Guadeloupe ou à Mayotte, payées entre le 1er septembre 2014 et le 31 décembre 2015, au titre de l'acquisition d'équipements ou de matériaux visant à l'optimisation de la ventilation naturelle, notamment les brasseurs d'air. (1)

1 bis. (Sans objet).

2. Un arrêté conjoint des ministres chargés de l'énergie, du logement et du budget fixe la liste des équipements, matériaux et appareils qui ouvrent droit au crédit d'impôt. Il précise les caractéristiques techniques et les critères de performances minimales requis pour l'application du crédit d'impôt.

Afin de garantir la qualité de l'installation ou de la pose des équipements, matériaux et appareils, un décret précise les travaux pour lesquels est exigé, pour l'application du crédit d'impôt, le respect de critères de qualification de l'entreprise.

3. Le crédit d'impôt s'applique pour le calcul de l'impôt dû au titre de l'année du paiement de la dépense par le contribuable ou, dans les cas prévus aux 2° et 3° des c et d du 1, au titre de l'année d'achèvement du logement ou de son acquisition si elle est postérieure.

4. Pour un même logement que le propriétaire, le locataire ou l'occupant à titre gratuit affecte à son habitation principale, le montant des dépenses ouvrant droit au crédit d'impôt ne peut excéder, au titre d'une période de cinq années consécutives comprises entre le 1er janvier 2005 et le 31 décembre 2015, la somme de 8 000 € pour une personne célibataire, veuve ou divorcée et de 16 000 € pour un couple soumis à imposition commune. Cette somme est majorée de 400 € par personne à charge au sens des articles 196 à 196 B. La somme de 400 € est divisée par deux lorsqu'il s'agit d'un enfant réputé à charge égale de l'un et l'autre de ses parents.

5. Le crédit d'impôt est égal à 30 % du montant des matériaux, équipements, appareils et dépenses de diagnostic de performance énergétique mentionnés au 1. (1)

5 bis. Abrogé.

5 ter. Pour les dépenses payées du 1er janvier au 31 août 2014, le crédit d'impôt s'applique dans les conditions prévues au présent article, dans sa rédaction antérieure à la loi n° 2014-1654 du 29 décembre 2014 de finances pour 2015.

Toutefois, au titre de ces mêmes dépenses, lorsque l'application du crédit d'impôt est conditionnée à la réalisation de dépenses selon les modalités prévues au 5 bis, dans sa rédaction antérieure à la même loi, le crédit d'impôt s'applique dans les conditions prévues au présent article, dans sa rédaction antérieure à ladite loi, sous réserve que des dépenses relevant d'au moins deux des catégories prévues au même 5 bis soient réalisées au cours de l'année 2014 ou des années 2014 et 2015. Dans ce dernier cas, les deux derniers alinéas dudit 5 bis s'appliquent dans leur rédaction antérieure à la même loi.

6. a. Les équipements, matériaux, appareils et travaux de pose mentionnés au 1 s'entendent de ceux figurant sur la facture d'une entreprise ou, le cas échéant, dans les cas prévus aux 2° et 3° des c et d du 1, des équipements figurant sur une attestation fournie par le vendeur ou le constructeur du logement. Les dépenses de diagnostic de performance énergétique mentionnées au 2° du f du 1 s'entendent de celles figurant sur la facture délivrée par une personne mentionnée à l'article L. 271-6 du code de la construction et de l'habitation. Cette facture comporte la mention que le diagnostic de performance énergétique a été réalisé en dehors des cas où la réglementation le rend obligatoire.

b. Les dépenses mentionnées au 1 ouvrent droit au bénéfice du crédit d'impôt, sous réserve que le contribuable soit en mesure de présenter, à la demande de l'administration fiscale, l'attestation du vendeur ou du constructeur du logement ou la facture, autre que des factures d'acompte, de l'entreprise qui a procédé à la fourniture et à l'installation des équipements, matériaux et appareils ou de la personne qui a réalisé le diagnostic de performance énergétique.

Cette facture comporte, outre les mentions prévues à l'article 289 :

1° Le lieu de réalisation des travaux ou du diagnostic de performance énergétique ;

2° La nature de ces travaux ainsi que la désignation, le montant et, le cas échéant, les caractéristiques et les critères de performances, mentionnés à la deuxième phrase du premier alinéa du 2, des équipements, matériaux et appareils ;

3° Dans le cas de l'acquisition et de la pose de matériaux d'isolation thermique des parois opaques, la surface en mètres carrés des parois opaques isolées, en distinguant ce qui relève de l'isolation par l'extérieur de ce qui relève de l'isolation par l'intérieur ;

4° Dans le cas de l'acquisition d'équipements de production d'énergie utilisant une source d'énergie renouvelable, la surface en mètres carrés des équipements de production d'énergie utilisant l'énergie solaire thermique ;

5° Lorsque les travaux d'installation des équipements, matériaux et appareils y sont soumis, les critères de qualification de l'entreprise ;

6° Abrogé.

c. Lorsque le bénéficiaire du crédit d'impôt n'est pas en mesure de produire une facture ou une attestation comportant les mentions prévues au b selon la nature des travaux, équipements, matériaux et appareils concernés, il fait l'objet, au titre de l'année d'imputation et dans la limite du crédit d'impôt obtenu, d'une reprise égale au montant de l'avantage fiscal accordé à raison de la dépense non justifiée.

6 bis. Abrogé.

6 ter. Un contribuable ne peut, pour une même dépense, bénéficier à la fois des dispositions du présent article et de l'aide prévue à l'article 199 sexdecies ou d'une déduction de charge pour la détermination de ses revenus catégoriels. (1)

7. Le crédit d'impôt est imputé sur l'impôt sur le revenu après imputation des réductions d'impôt mentionnées aux articles 199 quater C à 200 bis, des crédits d'impôt et des prélèvements ou retenues non libératoires. S'il excède l'impôt dû, l'excédent est restitué.

Lorsque le bénéficiaire du crédit d'impôt est remboursé dans un délai de cinq ans de tout ou partie du montant des dépenses qui ont ouvert droit à cet avantage, il fait l'objet, au titre de l'année de remboursement et dans la limite du crédit d'impôt obtenu, d'une reprise égale au montant de l'avantage fiscal accordé à raison de la somme qui a été remboursée. Toutefois, aucune reprise n'est pratiquée lorsque le remboursement fait suite à un sinistre survenu après que les dépenses ont été payées.
