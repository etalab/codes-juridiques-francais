# 12° : Réduction d'impôt accordée au titre de certains investissements réalisés outre-mer

- [Article 199 undecies C](article-199-undecies-c.md)
- [Article 199 undecies D](article-199-undecies-d.md)
- [Article 199 undecies A](article-199-undecies-a.md)
- [Article 199 undecies B](article-199-undecies-b.md)
- [Article 199 undecies E](article-199-undecies-e.md)
- [Article 199 undecies F](article-199-undecies-f.md)
