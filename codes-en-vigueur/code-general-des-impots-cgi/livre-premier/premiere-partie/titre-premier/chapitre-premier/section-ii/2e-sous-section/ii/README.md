# II : Revenu imposable des étrangers et des personnes n'ayant pas leur domicile fiscal en France

- [Article 164 C](article-164-c.md)
- [Article 164 D](article-164-d.md)
- [Article 164 A](article-164-a.md)
- [Article 164 B](article-164-b.md)
- [Article 165 bis](article-165-bis.md)
