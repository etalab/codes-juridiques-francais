# IV : Bénéfices de l'exploitation agricole

- [1 : Définition du bénéfice de l'exploitation agricole](1)
- [2 : Evaluation forfaitaire](2)
- [3 : Imposition d'après le bénéfice réel](3)
- [4 : Dispositifs de lissage ou d'étalement](4)
- [5 : Régime spécial applicable aux exploitations forestières](5)
- [5 bis : Régime spécial des cultures agréées dans les départements d'outre-mer](5-bis)
- [6 : Répartition du bénéfice en cas de bail à portion de fruits, de changement d'exploitant ou de décès de l'exploitant](6)
- [6 bis : Associés d'exploitation](6-bis)
- [7 : Renseignements à fournir par le propriétaire](7)
