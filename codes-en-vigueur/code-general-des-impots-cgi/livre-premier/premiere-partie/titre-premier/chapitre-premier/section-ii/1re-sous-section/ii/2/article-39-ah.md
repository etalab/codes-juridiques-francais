# Article 39 AH

Les manipulateurs multi-applications reprogrammables commandés automatiquement, programmables dans trois axes ou plus, qui sont fixés ou mobiles et destinés à une utilisation dans des applications industrielles d'automation, acquis ou créés entre le 1er octobre 2013 et le 31 décembre 2015, peuvent faire l'objet d'un amortissement exceptionnel sur vingt-quatre mois à compter de la date de leur mise en service.

Le premier alinéa s'applique aux petites et moyennes entreprises, au sens du règlement (CE) n° 800/2008 de la Commission du 6 août 2008 déclarant certaines catégories d'aide compatibles avec le marché commun en application des articles 87 et 88 du traité (Règlement général d'exemption par catégorie).

Le bénéfice de l'amortissement exceptionnel est subordonné au respect du règlement (UE) n° 1407/2013 de la Commission, du 18 décembre 2013, relatif à l'application des articles 107 et 108 du traité sur le fonctionnement de l'Union européenne aux aides de minimis.
