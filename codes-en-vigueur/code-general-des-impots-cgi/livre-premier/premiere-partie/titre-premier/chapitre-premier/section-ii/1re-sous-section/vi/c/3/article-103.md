# Article 103

Sous réserve des dispositions de l'article 218 bis, le bénéfice imposable des associés en nom, des commandités et des membres des sociétés visées aux articles 8 et 8 ter, est déterminé dans les conditions prévues à l'article 60, deuxième alinéa, et conformément aux dispositions des articles 96 à 100 bis et de l'article L. 53 du livre des procédures fiscales.
