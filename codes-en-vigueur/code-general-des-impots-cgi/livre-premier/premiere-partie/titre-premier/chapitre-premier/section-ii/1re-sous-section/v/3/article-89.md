# Article 89

Dans le cas de cession ou de cessation en totalité ou en partie de l'entreprise ou de cessation de l'exercice de la profession, la déclaration visée à l'article 87 doit être produite, en ce qui concerne les rémunérations payées pendant l'année de la cession ou de la cessation, dans le délai indiqué à l'article 201 ou 202.

Il en est de même de l'état concernant les rémunérations versées au cours de l'année précédente s'il n'a pas encore été produit.

Lorsqu'il s'agit de la cession ou de la cessation d'une exploitation agricole, le délai de soixante jours commence à courir d jour où la cession ou la cessation est devenue effective.

En cas de décès de l'employeur ou du débirentier, la déclaration des traitements, salaires, pensions et rentes viagères payés par le défunt pendant l'année au cours de laquelle il est décédé doit être souscrite par les héritiers dans les six mois du décès. Ce délai ne peut, toutefois, s'étendre au-delà du 31 janvier de l'année suivante.
