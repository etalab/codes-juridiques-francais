# Article 65

Par dérogation aux dispositions des 2 et 3 de l'article 64, le bénéfice imposable correspondant aux propriétés appartenant à l'exploitant et affectées à l'exploitation est obtenu en ajoutant au bénéfice visé au 4 dudit article, une somme égale au revenu ayant servi de base à la taxe foncière établie sur ces propriétés au titre de l'année de l'imposition.
