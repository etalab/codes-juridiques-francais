# 1 quater : Plus-values réalisées dans le cadre d'une activité agricole, artisanale, commerciale, industrielle ou libérale

- [Article 151 sexies](article-151-sexies.md)
- [Article 151 septies](article-151-septies.md)
- [Article 151 septies A](article-151-septies-a.md)
- [Article 151 septies B](article-151-septies-b.md)
