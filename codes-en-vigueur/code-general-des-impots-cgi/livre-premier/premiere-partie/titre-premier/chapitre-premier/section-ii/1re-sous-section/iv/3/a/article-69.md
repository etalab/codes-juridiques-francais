# Article 69

I. Lorsque les recettes d'un exploitant agricole, pour l'ensemble de ses exploitations, dépassent une moyenne de 76 300 € mesurée sur deux années consécutives, l'intéressé est obligatoirement imposé d'après un régime réel d'imposition à compter de la première année suivant la période biennale considérée.

II. Un régime simplifié d'imposition s'applique aux petits et moyens exploitants agricoles relevant de l'impôt sur le revenu :

a. Sur option, aux exploitants normalement placés sous le régime du forfait ;

b. De plein droit, aux autres exploitants, y compris ceux dont le forfait a été dénoncé par l'administration, dont la moyenne des recettes, mesurée sur deux années consécutives, n'excède pas 350 000 €.

III. En cas de dépassement de la limite mentionnée au b du II, les intéressés sont soumis de plein droit au régime réel normal d'imposition à compter du premier exercice suivant la période biennale considérée.

Les deux catégories d'exploitants prévues au II ainsi que celles soumises au régime simplifié d'imposition en application de la deuxième phrase du premier alinéa de l'article 69 B et de l'article 69 C peuvent opter pour le régime réel normal.

IV. Les options mentionnées au a du II et au deuxième alinéa du III doivent être formulées dans le délai de déclaration prévu à l'article 65 A ou dans le délai de déclaration des résultats, de l'année ou de l'exercice précédant celui au titre duquel elles s'appliquent.

Pour les exploitants qui désirent opter pour un régime réel d'imposition dès leur premier exercice d'activité, l'option doit être exercée dans un délai de quatre mois à compter de la date du début de l'activité. Toutefois, lorsque la durée du premier exercice est inférieure à quatre mois, l'option doit être exercée au plus tard à la date de clôture de cet exercice.

V. Les recettes à retenir pour l'appréciation des limites prévues au b du II correspondent aux créances acquises déterminées dans les conditions prévues au 2 bis de l'article 38.

Pour l'application des dispositions du présent article et des II et IV de l'article 151 septies, les recettes provenant d'opérations d'élevage ou de culture portant sur des animaux ou des produits appartenant à des tiers sont multipliées par cinq.
