# Article 154 bis A

Les prestations servies par les régimes ou au titre des contrats visés au deuxième alinéa du I de l'article 154 bis sous forme de revenus de remplacement sont prises en compte pour la détermination du revenu imposable de leur bénéficiaire.
