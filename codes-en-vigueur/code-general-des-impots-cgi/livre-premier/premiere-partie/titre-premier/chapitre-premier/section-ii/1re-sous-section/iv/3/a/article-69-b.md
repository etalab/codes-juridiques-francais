# Article 69 B

Les exploitants agricoles imposés, en raison de leurs recettes, d'après un régime réel d'imposition au titre de l'année 1984 ou d'une année ultérieure, sont soumis définitivement à un régime de cette nature. Ce régime continue de s'appliquer au conjoint survivant ou à l'indivision successorale qui poursuit l'exploitation.

Toutefois, lorsque les recettes d'un exploitant agricole individuel, mesurées sur la moyenne de deux années consécutives, s'abaissent en dessous de 46 000 € l'intéressé peut, sur option, être soumis au régime du forfait à compter du 1er janvier de l'année qui suit la période biennale de référence. L'option doit être formulée dans le délai de déclaration des résultats de l'exercice précédant celui au titre duquel elle s'applique.
