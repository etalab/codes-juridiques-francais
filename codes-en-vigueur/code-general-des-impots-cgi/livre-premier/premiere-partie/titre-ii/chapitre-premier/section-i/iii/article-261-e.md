# Article 261 E

Sont exonérés de la taxe sur la valeur ajoutée :

1° L'organisation de jeux de hasard ou d'argent soumis au prélèvement progressif mentionné à l'article L 2333-56 du code général des collectivités territoriales ou à l'impôt sur les spectacles, jeux et divertissements ;

2° Le produit de l'exploitation de la loterie nationale, du loto national, des paris mutuels hippiques, des paris sur des compétitions sportives et des jeux de cercle en ligne, à l'exception des rémunérations perçues par les organisateurs et les intermédiaires qui participent à l'organisation de ces jeux et paris ;

3° (Abrogé).
