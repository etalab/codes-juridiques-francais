# Section VI : Redevables de la taxe

- [Article 283](article-283.md)
- [Article 283-0](article-283-0.md)
- [Article 284](article-284.md)
- [Article 285 A](article-285-a.md)
