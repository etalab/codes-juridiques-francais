# Chapitre XX : Prélèvements sur les jeux et paris

- [Article 302 bis ZG](article-302-bis-zg.md)
- [Article 302 bis ZH](article-302-bis-zh.md)
- [Article 302 bis ZI](article-302-bis-zi.md)
- [Article 302 bis ZJ](article-302-bis-zj.md)
- [Article 302 bis ZK](article-302-bis-zk.md)
- [Article 302 bis ZL](article-302-bis-zl.md)
- [Article 302 bis ZM](article-302-bis-zm.md)
- [Article 302 bis ZN](article-302-bis-zn.md)
- [Article 302 bis ZO](article-302-bis-zo.md)
