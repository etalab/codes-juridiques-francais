# Titre III : Impositions perçues au profit de certains établissements publics et d'organismes divers

- [Chapitre I bis : Taxes sur le chiffre d'affaires et taxes diverses assimilées](chapitre-i-bis)
- [Chapitre II : Contributions indirectes](chapitre-ii)
- [Chapitre III : Enregistrement, publicité foncière et timbre](chapitre-iii)
- [Chapitre IV : Dispositions communes](chapitre-iv)
- [Chapitre premier : Impôts directs et taxes assimilées](chapitre-premier)
