# Article 1618 septies

Il est institué au profit de l'organisme mentionné à l'article L. 731-1 du code rural et de la pêche maritime une taxe portant sur les quantités de farines, semoules et gruaux de blé tendre livrées ou mises en oeuvre en vue de la consommation humaine, ainsi que sur les mêmes produits introduits en provenance d'autres Etats membres de la Communauté européenne ou importés de pays tiers.

Les farines, semoules et gruaux de blé tendre expédiés vers d'autres Etats membres de la Communauté européenne ou vers les départements de Corse, exportés ou destinés à être exportés vers des pays tiers ou vers les départements d'outre-mer, par l'acquéreur, ainsi que les farines utilisées pour la fabrication d'amidon, sont exonérés de la taxe.

La taxe est perçue en France continentale auprès des meuniers, des opérateurs qui procèdent à l'introduction des produits sur ce territoire et des importateurs de produits en provenance de pays tiers.

Le montant de la taxe est fixé à 15,24 € par tonne de farine, semoule ou gruaux.

Des modalités particulières de liquidation peuvent être déterminées par un décret qui précise également les obligations déclaratives des assujettis.

La taxe est recouvrée et les infractions sont recherchées, constatées, poursuivies et sanctionnées selon les règles et sous les garanties prévues en matière de contributions indirectes.

Toutefois, à l'importation en provenance de pays non membres de la Communauté européenne, la taxe est recouvrée et les infractions sont recherchées, constatées, poursuivies et sanctionnées selon les règles, privilèges et garanties prévus en matière de douane.
