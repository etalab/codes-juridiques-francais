# Chapitre III : Enregistrement, publicité foncière et timbre

- [Section I : Taxes à percevoir pour l'alimentation du fonds commun des accidents du travail agricole](section-i)
- [Section I bis : Droit de timbre perçu au profit de l'Agence nationale des titres sécurisés](section-i-bis)
- [Section V : Fonds national de gestion des risques en agriculture](section-v)
- [Section V bis : Fonds de garantie des calamités agricoles dans les départements d'outre-mer](section-v-bis)
- [Section V quater : Fonds de prévention des risques naturels majeurs](section-v-quater)
- [Section V quinquies : Droits perçus au profit de la Caisse nationale de l'assurance maladie des travailleurs salariés](section-v-quinquies)
- [Section IX : Association pour le développement de la formation professionnelle dans les transports](section-ix)
- [Section X : Droit de timbre perçu au profit de l'Office national de la chasse et de la faune sauvage](section-x)
- [Section XII : Droit affecté au fonds d'indemnisation de la profession d'avoués près les cours d'appel](section-xii)
