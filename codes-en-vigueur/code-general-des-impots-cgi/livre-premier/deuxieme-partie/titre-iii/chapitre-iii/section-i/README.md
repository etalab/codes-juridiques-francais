# Section I : Taxes à percevoir pour l'alimentation du fonds commun des accidents du travail agricole

- [Article 1622](article-1622.md)
- [Article 1623](article-1623.md)
