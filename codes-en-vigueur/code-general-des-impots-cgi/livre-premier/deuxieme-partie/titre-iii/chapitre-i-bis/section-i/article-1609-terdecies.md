# Article 1609 terdecies

La taxe sur les appareils de reproduction ou d'impression est due pour les opérations suivantes :

Sous réserve de présenter toutes justifications nécessaires, ventes et livraisons à soi-même, à l'exception des exportations et des livraisons exonérées en vertu du I de l'article 262 ter ou les livraisons dans un lieu situé dans un autre Etat membre de la Communauté européenne en application de l'article 258 A, d'appareils de reproduction ou d'impression réalisées par les entreprises qui les ont fabriqués ou fait fabriquer en France.

Importations et acquisitions intracommunautaires des mêmes appareils.

Un arrêté conjoint du ministre de l'économie et des finances et du ministre de l'industrie et de la recherche fixe la liste de ces appareils.

La taxe est perçue au taux de 3,25 %.
