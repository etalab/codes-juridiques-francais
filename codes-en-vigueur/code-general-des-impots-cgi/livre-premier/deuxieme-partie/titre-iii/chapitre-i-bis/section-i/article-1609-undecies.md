# Article 1609 undecies

Il est perçu :

a Une taxe sur l'édition des ouvrages de librairie ;

b Une taxe sur les appareils de reproduction ou d'impression ;

Le produit de chacune de ces taxes est affecté au Centre national du livre dans la limite du plafond fixé au I de l'article 46 de la loi n° 2011-1977 du 28 décembre 2011 de finances pour 2012.
