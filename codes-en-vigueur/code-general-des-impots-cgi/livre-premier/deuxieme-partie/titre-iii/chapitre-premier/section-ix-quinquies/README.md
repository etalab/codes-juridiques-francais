# Section IX quinquies : Taxe spéciale d'équipement perçue au profit de l'agence pour la mise en valeur des espaces urbains de la zone dite des cinquante pas géométriques en Martinique.

- [Article 1609 D](article-1609-d.md)
