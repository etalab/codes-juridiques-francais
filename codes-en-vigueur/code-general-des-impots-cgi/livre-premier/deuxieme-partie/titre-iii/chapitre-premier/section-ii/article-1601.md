# Article 1601

Une taxe additionnelle à la cotisation foncière des entreprises est perçue au profit des chambres régionales de métiers et de l'artisanat ou des chambres de métiers et de l'artisanat de région et de l'assemblée permanente des chambres de métiers et de l'artisanat.

Le produit de cette taxe est affecté à chacun des bénéficiaires mentionnés au premier alinéa dans la limite du plafond prévu au I de l'article 46 de la loi n° 2011-1977 du 28 décembre 2011 de finances pour 2012.

Ce plafond prévu au même I est décomposé en deux sous-plafonds : un sous-plafond relatif à la somme des produits du droit fixe défini au a du présent article, du droit additionnel défini au b du présent article et de l'article 3 de la loi n° 48-977 du 16 juin 1948 relative à la taxe pour frais de chambre de métiers applicable dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle et un sous-plafond relatif au produit du droit additionnel pour le financement d'actions de formation défini au c du présent article.

Ces deux sous-plafonds sont obtenus en répartissant le plafond prévu au I de l'article 46 de la loi n° 2011-1977 du 28 décembre 2011 précitée au prorata des émissions perçues entre les différentes composantes de la taxe figurant dans les rôles généraux de l'année précédant l'année de référence.

Pour l'application du premier sous-plafond susmentionné, il est opéré en fin d'exercice, au profit du budget général, un prélèvement sur le fonds mentionné au 6° de l'article 5-8 du code de l'artisanat, correspondant à la différence entre le premier sous-plafond susmentionné et la somme des ressources fiscales perçues par l'ensemble des bénéficiaires au titre du droit fixe défini au a et du droit additionnel défini au b du présent article et de l'article 3 de la loi n° 48-977 du 16 juin 1948 précitée.

En 2014, le fonds mentionné au 6° de l'article 5-8 du code de l'artisanat est alimenté par un prélèvement sur les chambres de métiers et de l'artisanat de région, les chambres de métiers et de l'artisanat départementales, les chambres de métiers d'Alsace et de Moselle et la chambre de métiers et de l'artisanat de Mayotte, dont le fonds de roulement constaté fin 2012, hors réserves affectées à des investissements votés et formellement validés par la tutelle, est supérieur à quatre mois de charges. Le prélèvement est fixé pour tous les établissements concernés à 50 % de la partie excédant quatre mois de charges, hors réserves affectées. Dans chaque région, le prélèvement sur chaque établissement concerné est effectué par titre de perception émis par l'ordonnateur compétent. Il est recouvré comme en matière de créances étrangères à l'impôt et au domaine. Son produit est reversé au fonds de financement et d'accompagnement.

Le fonds de roulement est défini, pour chaque établissement, par différence entre les ressources stables (capitaux propres, provisions, dettes d'emprunt) et les emplois durables (actif immobilisé). Les charges prises en compte pour ramener le fonds de roulement à une durée sont les charges décaissables non exceptionnelles (charges d'exploitation moins provisions pour dépréciation, moins dotations aux amortissements et plus les charges financières).

Pour l'application du second sous-plafond susmentionné, un sous-plafond individuel relatif au produit du droit additionnel pour le financement d'actions de formation est obtenu, pour chaque bénéficiaire, en répartissant ce sous-plafond au prorata des émissions perçues figurant dans les rôles généraux de l'année précédant l'année de référence.

Par dérogation au II du même article 46, les plafonds individuels portent sur les émissions rattachées aux rôles de l'année de référence sans prise en compte des remboursements et dégrèvements relatifs à cette taxe.

Cette taxe pourvoit à une partie des dépenses des établissements publics constituant le réseau des chambres de métiers et de l'artisanat. Elle est employée, dans le respect des règles de concurrence nationales et communautaires, pour remplir les missions qui leur sont confiées par les lois et les règlements, à l'exclusion des activités marchandes.

La taxe est acquittée par les chefs d'entreprises individuelles ou les sociétés soumis à l'obligation de s'inscrire au répertoire des métiers ou qui y demeurent immatriculés. Les personnes physiques titulaires de l'allocation de solidarité aux personnes âgées mentionnée à l'article L. 815-1 du code de la sécurité sociale ou de l'allocation supplémentaire d'invalidité mentionnée à l'article L. 815-24 du même code sont dégrevées d'office de la taxe.

Cette taxe est composée :

a) D'un droit fixe par ressortissant égal à la somme des droits arrêtés par l'assemblée permanente des chambres de métiers et de l'artisanat ainsi que par la chambre régionale de métiers et de l'artisanat ou par la chambre de métiers et de l'artisanat de région, dans la limite d'un montant maximal fixé dans le tableau suivant en proportion du montant annuel du plafond de la sécurité sociale en vigueur au 1er janvier de l'année d'imposition :

(En %)

<table>
<tbody>
<tr>
<td width="378"/>
<td width="76">
<p align="center">2011 </p>
</td>
<td width="76">
<p align="center">2012 </p>
</td>
<td width="76">
<p align="center">2013 </p>
</td>
<td width="76">
<p align="center">2014 <br/>et années <br/>suivantes </p>
</td>
</tr>
<tr>
<td width="378">
<p>Assemblée permanente des chambres de métiers et de l'artisanat </p>
</td>
<td width="76">
<p align="center">
<br/>0,0436 </p>
</td>
<td width="76">
<p align="center">
<br/>0,0425 </p>
</td>
<td width="76">
<p align="center">
<br/>0,0414 </p>
</td>
<td width="76">
<p align="center">
<br/>0,0403 </p>
</td>
</tr>
<tr>
<td width="378">
<p>Chambres régionales de métiers et de l'artisanat ou chambres de métiers et de l'artisanat de région </p>
</td>
<td width="76">
<p align="center">
<br/>0,3112 </p>
</td>
<td width="76">
<p align="center">
<br/>0,3032 </p>
</td>
<td width="76">
<p align="center">
<br/>0,2952 </p>
</td>
<td width="76">
<p align="center">
<br/>0,2872 </p>
</td>
</tr>
<tr>
<td width="378">
<p>Chambre régionale de métiers et de l'artisanat ou chambre de métiers et de l'artisanat de région de Lorraine : droit fixe applicable aux ressortissants du département de la Moselle </p>
</td>
<td width="76">
<p align="center">
<br/>0,0274 </p>
</td>
<td width="76">
<p align="center">
<br/>0,0267 </p>
</td>
<td width="76">
<p align="center">
<br/>0,0254 </p>
</td>
<td width="76">
<p align="center">
<br/>0,0247 </p>
</td>
</tr>
</tbody>
</table>

b) D'un droit additionnel à la cotisation foncière des entreprises, dont le produit est arrêté par les chambres mentionnées au a ; celui-ci ne peut excéder 60 % du produit du droit fixe revenant aux chambres mentionnées au a.

Toutefois, les chambres mentionnées au a sont autorisées à porter le produit du droit additionnel jusqu'à 90 % du produit du droit fixe, afin de mettre en œuvre des actions ou de réaliser des investissements, dans des conditions définies par décret en Conseil d'Etat.

A compter du 1er janvier 2013, la part du produit du droit additionnel dépassant 60 % du produit du droit fixe fait l'objet d'une convention d'objectifs et de moyens conclue avec l'Etat dans des conditions fixées par décret en Conseil d'Etat ;

c) D'un droit additionnel par ressortissant, affecté par les chambres mentionnées au a au financement d'actions de formation, au sens des articles L. 6313-1 à L. 6313-11 et L. 6353-1 du code du travail, des chefs d'entreprises artisanales dans la gestion et le développement de celles-ci. Ces actions de formation font l'objet d'une comptabilité analytique et sont gérées sur un compte annexe. Ce droit est fixé à 0,12 % du montant annuel du plafond de la sécurité sociale en vigueur au 1er janvier de l'année d'imposition.

Le présent article n'est applicable dans les départements du Bas-Rhin et du Haut-Rhin qu'en ce qui concerne le droit fixe arrêté par l'assemblée permanente des chambres de métiers et de l'artisanat. Il n'est applicable dans le département de la Moselle qu'en ce qui concerne le droit fixe arrêté par l'assemblée permanente des chambres de métiers et de l'artisanat et par la chambre régionale de métiers et de l'artisanat ou la chambre de métiers et de l'artisanat de région de Lorraine et le droit additionnel figurant au c.
