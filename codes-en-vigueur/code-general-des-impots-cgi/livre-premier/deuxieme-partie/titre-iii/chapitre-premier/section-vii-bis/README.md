# Section VII bis : Taxe spéciale d'équipement perçue au profit des établissements publics fonciers et de l'office foncier de Corse

- [Article 1607 bis](article-1607-bis.md)
- [Article 1607 ter](article-1607-ter.md)
