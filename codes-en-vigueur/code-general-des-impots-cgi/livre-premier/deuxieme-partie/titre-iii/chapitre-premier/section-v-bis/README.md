# Section V bis :  Prélèvement spécial sur les bénéfices résultant de la vente, la location ou l'exploitation d'œuvres pornographiques ou d'incitation à la violence

- [Article 1605 sexies](article-1605-sexies.md)
- [Article 1605 septies](article-1605-septies.md)
- [Article 1605 octies](article-1605-octies.md)
