# III : Exonération

- [Article 1594-0 G](article-1594-0-g.md)
- [Article 1594 I](article-1594-i.md)
- [Article 1594 I bis](article-1594-i-bis.md)
- [Article 1594 I ter](article-1594-i-ter.md)
- [Article 1594 I quater](article-1594-i-quater.md)
- [Article 1594 G](article-1594-g.md)
- [Article 1594 H](article-1594-h.md)
- [Article 1594 H-0 bis](article-1594-h-0-bis.md)
- [Article 1594 H bis](article-1594-h-bis.md)
- [Article 1594 J](article-1594-j.md)
- [Article 1594 J bis](article-1594-j-bis.md)
