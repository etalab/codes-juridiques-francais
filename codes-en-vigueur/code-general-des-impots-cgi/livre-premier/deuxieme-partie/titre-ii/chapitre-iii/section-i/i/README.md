# I : Dispositions générales.

- [Article 1594 D](article-1594-d.md)
- [Article 1594 A](article-1594-a.md)
- [Article 1594 B](article-1594-b.md)
- [Article 1594 E](article-1594-e.md)
