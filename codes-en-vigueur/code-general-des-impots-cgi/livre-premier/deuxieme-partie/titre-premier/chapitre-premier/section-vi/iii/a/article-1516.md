# Article 1516

Les valeurs locatives des propriétés bâties et non bâties sont mises à jour suivant une procédure comportant :

- la constatation annuelle des changements affectant ces propriétés ;

- l'actualisation, tous les trois ans, des évaluations résultant de la précédente révision générale ;

- l'exécution de révisions générales tous les six ans. Les conditions d'exécution de ces révisions seront fixées par la loi.
