# Article 1498

La valeur locative de tous les biens autres que les locaux visés au I de l'article 1496 et que les établissements industriels visés à l'article 1499 est déterminée au moyen de l'une des méthodes indiquées ci-après :

1° Pour les biens donnés en location à des conditions de prix normales, la valeur locative est celle qui ressort de cette location ;

2° a. Pour les biens loués à des conditions de prix anormales ou occupés par leur propriétaire, occupés par un tiers à un autre titre que la location, vacants ou concédés à titre gratuit, la valeur locative est déterminée par comparaison.

Les termes de comparaison sont choisis dans la commune. Ils peuvent être choisis hors de la commune pour procéder à l'évaluation des immeubles d'un caractère particulier ou exceptionnel ;

b. La valeur locative des termes de comparaison est arrêtée :

Soit en partant du bail en cours à la date de référence de la révision lorsque l'immeuble type était loué normalement à cette date,

Soit, dans le cas contraire, par comparaison avec des immeubles similaires situés dans la commune ou dans une localité présentant, du point de vue économique, une situation analogue à celle de la commune en cause et qui faisaient l'objet à cette date de locations consenties à des conditions de prix normales ;

3° A défaut de ces bases, la valeur locative est déterminée par voie d'appréciation directe.
