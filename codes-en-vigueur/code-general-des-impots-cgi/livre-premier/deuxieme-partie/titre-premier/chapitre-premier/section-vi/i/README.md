# I : Évaluation des propriétés bâties

- [C : Locaux commerciaux et biens divers](c)
- [D : Etablissements industriels](d)
- [A : Généralités](a)
- [B : Locaux d'habitation et à usage professionnel](b)
- [E : Dispositions communes aux établissements industriels et aux locaux commerciaux](e)
- [F : Procédure d'évaluation](f)
