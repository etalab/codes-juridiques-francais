# Article 1495

Chaque propriété ou fraction de propriété est appréciée d'après sa consistance, son affectation, sa situation et son état, à la date de l'évaluation (1).
