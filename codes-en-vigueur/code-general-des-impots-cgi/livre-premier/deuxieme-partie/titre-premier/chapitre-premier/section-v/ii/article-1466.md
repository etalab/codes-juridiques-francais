# Article 1466

Les exonérations appliquées antérieurement à la création d'une agglomération nouvelle, en exécution des délibérations des conseils des communes, sont maintenues pour la quotité et la durée initialement prévues.
