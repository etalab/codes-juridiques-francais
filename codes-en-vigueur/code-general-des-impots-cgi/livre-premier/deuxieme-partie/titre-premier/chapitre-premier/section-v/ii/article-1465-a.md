# Article 1465 A

I.-Sauf délibération contraire de la commune ou de l'établissement public de coopération intercommunale doté d'une fiscalité propre, dans les zones de revitalisation rurale dont le périmètre est défini par décret, les entreprises qui procèdent aux opérations mentionnées au premier alinéa de l'article 1465 dans les conditions et sous réserve, le cas échéant, de l'agrément prévu à cet article sont exonérées de cotisation foncière des entreprises. Cette exonération ne peut avoir pour effet de reporter de plus de cinq ans l'application du régime d'imposition de droit commun.

Cette exonération s'applique également aux créations d'activités dans les zones de revitalisation rurale réalisées par des artisans qui effectuent principalement des travaux de fabrication, de transformation, de réparation ou des prestations de services et pour lesquels la rémunération du travail représente plus de 50 % du chiffre d'affaires global, tous droits et taxes compris, ou par des entreprises qui exercent une activité professionnelle au sens du premier alinéa de l'article 92. Dans les communes de moins de deux mille habitants, l'exonération s'applique également aux créations d'activités commerciales et aux reprises d'activités commerciales, artisanales ou professionnelles au sens du 1 de l'article 92, réalisées par des entreprises exerçant le même type d'activité, dès lors qu'au cours de la période de référence prise en compte pour la première année d'imposition, l'activité est exercée dans l'établissement avec moins de cinq salariés.

II.-Les zones de revitalisation rurale comprennent les communes membres d'un établissement public de coopération intercommunale à fiscalité propre, incluses dans un arrondissement ou un canton caractérisé par une très faible densité de population ou par une faible densité de population et satisfaisant à l'un des trois critères socio-économiques suivants :

a. un déclin de la population constaté sur l'ensemble de l'arrondissement ou du canton ou dans une majorité de leurs communes dont le chef-lieu ;

b. un déclin de la population active ;

c. une forte proportion d'emplois agricoles.

En outre, les établissements publics de coopération intercommunale à fiscalité propre dont au moins la moitié de la population est incluse en zone de revitalisation rurale en application des critères définis aux alinéas précédents sont, pour l'ensemble de leur périmètre, inclus dans ces zones.

Les zones de revitalisation rurale comprennent également les communes appartenant au 1er janvier 2005 à un établissement public de coopération intercommunale à fiscalité propre dont le territoire présente une faible densité de population et satisfait à l'un des trois critères socio-économiques définis aux a, b et c. Si ces communes intègrent un établissement public de coopération intercommunale à fiscalité propre non inclus dans les zones de revitalisation rurale, elles conservent le bénéfice de ce classement jusqu'au 31 décembre 2009.

La modification du périmètre de l'établissement public de coopération intercommunale en cours d'année n'emporte d'effet, le cas échéant, qu'à compter du 1er janvier de l'année suivante.

Les dispositions des cinquième, sixième, septième et neuvième alinéas de l'article 1465 sont applicables aux exonérations prévues au premier alinéa du I. Toutefois, pour l'application du neuvième alinéa de l'article 1465, l'imposition est établie au profit de l'Etat.

III.-Un décret en Conseil d'Etat précise les conditions d'application du II et en particulier les critères et seuils visant à déterminer le périmètre des zones de revitalisation rurale.

IV.-Le bénéfice des exonérations accordées à compter du 1er janvier 2014 est subordonné au respect du règlement (UE) n° 1407/2013 de la Commission, du 18 décembre 2013, relatif à l'application des articles 107 et 108 du traité sur le fonctionnement de l'Union européenne (1) aux aides de minimis. Toutefois, sur option des entreprises qui procèdent entre le 1er juillet 2014 et le 31 décembre 2015 aux opérations mentionnées au I dans les zones d'aide à finalité régionale, le bénéfice des exonérations est subordonné au respect de l'article 14 du règlement (UE) n° 651/2014 de la Commission, du 17 juin 2014, déclarant certaines catégories d'aides compatibles avec le marché intérieur en application des articles 107 et 108 du traité (2).

Cette option, exercée distinctement pour chacun des établissements concernés, est irrévocable pour la durée de l'exonération. Elle doit être exercée, selon le cas, dans le délai prévu pour le dépôt de la déclaration annuelle afférente à la première année au titre de laquelle l'exonération prend effet ou de la déclaration provisoire de cotisation foncière des entreprises visée à l'article 1477.
