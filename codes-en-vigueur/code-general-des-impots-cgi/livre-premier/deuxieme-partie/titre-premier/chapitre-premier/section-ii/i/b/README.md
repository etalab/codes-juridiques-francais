# B : Exonérations permanentes

- [Article 1382](article-1382.md)
- [Article 1382 C](article-1382-c.md)
- [Article 1382 D](article-1382-d.md)
- [Article 1382 B](article-1382-b.md)
- [Article 1382 E](article-1382-e.md)
