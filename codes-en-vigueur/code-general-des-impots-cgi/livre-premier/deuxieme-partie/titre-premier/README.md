# Titre premier : Impositions communales

- [Chapitre II : Contributions indirectes](chapitre-ii)
- [Chapitre III : Enregistrement](chapitre-iii)
- [Chapitre premier : Impôts directs et taxes assimilées](chapitre-premier)
