# II : Impôt sur les cercles et maisons de jeux

- [1° : Champ d'application.](1)
- [2° : Tarif](2)
- [6° : Assiette et liquidation](6)
- [7° : Obligations des exploitants.](7)
- [8° : Répartition de l'impôt](8)
