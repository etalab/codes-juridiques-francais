# Chapitre II : Contributions indirectes

- [Section I : Taxes obligatoires](section-i)
- [Section II : Taxes facultatives](section-ii)
