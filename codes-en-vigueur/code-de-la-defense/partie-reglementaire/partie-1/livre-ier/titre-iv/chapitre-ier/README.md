# Chapitre Ier : Dispositions communes à l'ensemble des ministres

- [Article R*1141-1](article-r-1141-1.md)
- [Article R*1141-2](article-r-1141-2.md)
- [Article R*1141-3](article-r-1141-3.md)
