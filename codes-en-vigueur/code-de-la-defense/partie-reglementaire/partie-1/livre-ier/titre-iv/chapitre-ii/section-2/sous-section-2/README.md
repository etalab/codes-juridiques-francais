# Sous-section 2 : Commission permanente de défense civile

- [Article D*1142-8](article-d-1142-8.md)
- [Article D*1142-9](article-d-1142-9.md)
- [Article D*1142-10](article-d-1142-10.md)
- [Article D*1142-11](article-d-1142-11.md)
- [Article D*1142-11-1](article-d-1142-11-1.md)
