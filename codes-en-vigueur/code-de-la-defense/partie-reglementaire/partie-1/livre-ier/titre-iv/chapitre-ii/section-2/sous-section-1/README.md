# Sous-section 1 : Dispositions générales

- [Article R*1142-5](article-r-1142-5.md)
- [Article R*1142-6](article-r-1142-6.md)
- [Article R*1142-7](article-r-1142-7.md)
