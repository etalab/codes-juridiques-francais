# Chapitre II : Dispositions particulières à certains ministres

- [Section 1 : Défense](section-1)
- [Section 2 : Intérieur](section-2)
- [Section 3 : Economie, finances et industrie](section-3)
- [Section 5 : Santé et affaires sociales](section-5)
- [Section 7 : Outre-mer](section-7)
