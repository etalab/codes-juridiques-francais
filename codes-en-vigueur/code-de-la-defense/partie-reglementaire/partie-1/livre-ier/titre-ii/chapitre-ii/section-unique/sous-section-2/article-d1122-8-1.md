# Article D1122-8-1

Les services spécialisés de renseignement, mentionnés à l'article 6 nonies de l'ordonnance n° 58-1100 du 17 novembre 1958 relative au fonctionnement des assemblées parlementaires, sont la direction générale de la sécurité extérieure, la direction de la protection et de la sécurité de la défense, la direction du renseignement militaire, la direction générale de la sécurité intérieure, le service à compétence nationale dénommé " direction nationale du renseignement et des enquêtes douanières " et le service à compétence nationale dénommé " traitement du renseignement et action contre les circuits financiers clandestins ".

Ces services forment avec le coordonnateur national du renseignement et l'académie du renseignement la communauté française du renseignement.
