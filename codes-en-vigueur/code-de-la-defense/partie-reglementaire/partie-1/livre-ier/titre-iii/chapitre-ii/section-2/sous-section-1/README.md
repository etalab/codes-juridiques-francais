# Sous-section 1 : Dispositions générales

- [Article R1132-12](article-r1132-12.md)
- [Article R1132-13](article-r1132-13.md)
- [Article R1132-14](article-r1132-14.md)
- [Article R1132-15](article-r1132-15.md)
- [Article R1132-16](article-r1132-16.md)
- [Article R1132-17](article-r1132-17.md)
- [Article R1132-18](article-r1132-18.md)
