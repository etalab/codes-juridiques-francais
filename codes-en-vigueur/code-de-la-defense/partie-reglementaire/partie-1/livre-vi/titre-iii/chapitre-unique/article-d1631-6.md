# Article D1631-6

Sont applicables à Mayotte, sous réserve des adaptations prévues à l'article D. 1631-7 et au titre VIII du présent livre :

1° Au livre Ier, les dispositions des articles D. 1132-53, D. 1132-54,
D. 1142-30 à D. 1142-34,
D. 1143-9 à D. 1143-13 ;

2° Au livre III, les dispositions des articles D. 1313-7 à D. 1313-10, D. 1321-2 à D. 1321-18, D. 1332-39 à D. 1332-41, D. 1334-5 à D. 1334-14, D. 1337-14 à D. 1337-17, D. 1338-6 ;

3° Au livre IV, les dispositions des articles D. 1443-2 à D. 1443-4 ;

4° Au titre VIII du présent livre, les dispositions des articles D. 1681-7 à D. 1681-12, D. 1681-15 et D. 1681-16.
