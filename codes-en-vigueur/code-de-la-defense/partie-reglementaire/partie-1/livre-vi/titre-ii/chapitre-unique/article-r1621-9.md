# Article R1621-9

Pour l'application à Saint-Pierre-et-Miquelon :

1° A l'article R. 1333-3 et au 1° du IV de l'article R. 1333-17, les mots : " le ministre de l'intérieur " sont remplacés par les mots : " le ministre chargé de l'outre-mer " ;

2° A l'avant-dernier alinéa de l'article R. 1333-4, les mots : " et du ministre chargé de l'énergie " sont remplacés par les mots : " du ministre chargé de l'énergie et du ministre chargé de l'outre-mer ", et à l'article R. 1333-18, les mots : " et du ministre chargé des transports " sont remplacés par les mots : " du ministre chargé des transports et du ministre chargé de l'outre-mer ".
