# Article D1681-8

Les commandants supérieurs sont placés sous l'autorité du chef d'état-major des armées.
