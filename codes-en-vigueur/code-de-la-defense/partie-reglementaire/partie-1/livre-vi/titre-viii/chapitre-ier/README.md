# Chapitre Ier : Organisation territoriale et opérationnelle de la défense

- [Section 1 : Dispositions générales.](section-1)
- [Section 2 : Organisation générale](section-2)
- [Section 3 : Attributions des commandants supérieurs.](section-3)
