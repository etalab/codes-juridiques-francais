# Article R1681-3

Les pouvoirs du haut fonctionnaire de zone de défense et de sécurité, dont les attributions sont définies à l'article L. 1311-1 du présent code, sont exercés par les autorités civiles mentionnées dans le tableau figurant à l'article R. 1681-2 du même code dans les conditions prévues par le présent code et par le titre V du livre Ier du code de la sécurité intérieure.
