# Article R1681-2

La composition et l'organisation des zones de défense et de sécurité prévues à l'article L. 1311-1 sont fixées conformément au tableau suivant :

<table>
<thead>
<tr>
<td width="104">
<p>ZONE DE DÉFENSE ET DE SECURITE</p>
</td>
<td width="104">
<p>COMPOSITION</p>
</td>
<td width="104">
<p>HAUT FONCTIONNAIRE<br/>de zone de défense et de sécurité</p>
</td>
<td width="216">
<p>COMMANDANT<br/>de zone de défense et de sécurité</p>
</td>
</tr>
</thead>
<tbody>
<tr>
<td valign="top" width="104">
<p align="left">Antilles (siège à Fort-de-France).</p>
<br/>
</td>
<td valign="top" width="104">
<p>Martinique.</p>
<p>Guadeloupe.</p>
</td>
<td valign="top" width="104">
<p>Préfet de la Martinique. </p>
</td>
<td valign="top" width="216">
<p align="left">Commandant supérieur des forces armées aux Antilles.</p>
<br/>
</td>
</tr>
<tr>
<td valign="top" width="104">
<p align="left">Guyane (Siège à Cayenne)</p>
<br/>
</td>
<td valign="top" width="104">
<p>Guyane.</p>
</td>
<td valign="top" width="104">
<p>Préfet de la Guyane.</p>
</td>
<td valign="top" width="216">
<p align="left">Commandant supérieur des forces armées en Guyane.</p>
<br/>
</td>
</tr>
<tr>
<td valign="top" width="104">
<p align="left">Sud de l'océan Indien (siège à Saint-Denis-de-la-Réunion).</p>
<br/>
</td>
<td valign="top" width="104">
<p>Réunion. </p>
<p>Mayotte.</p>
<p>Terres australes et antarctiques françaises.</p>
<p>Iles Tromelin, Glorieuses, Juan de Nova, Europa et Bassas da India.</p>
</td>
<td valign="top" width="104">
<p>Préfet de la Réunion.</p>
</td>
<td valign="top" width="216">
<p align="left">Commandant supérieur des forces armées dans la zone sud de l'océan Indien.</p>
<br/>
</td>
</tr>
<tr>
<td valign="top" width="104">
<p>Nouvelle-Calédonie (siège à Nouméa).</p>
</td>
<td valign="top" width="104">
<p>Nouvelle-Calédonie. </p>
<p>Wallis et Futuna.</p>
</td>
<td valign="top" width="104">
<p>Haut commissaire de la République en Nouvelle-Calédonie.</p>
</td>
<td valign="top" width="216">
<p>Commandant supérieur des forces armées de la Nouvelle-Calédonie.</p>
</td>
</tr>
<tr>
<td valign="top" width="104">
<p>Polynésie française (siège à Papeete).</p>
</td>
<td valign="top" width="104">
<p>Polynésie française.</p>
</td>
<td valign="top" width="104">
<p>Haut commissaire de la République en Polynésie française.</p>
</td>
<td valign="top" width="216">
<p>Commandant supérieur des forces armées de la Polynésie française.</p>
</td>
</tr>
</tbody>
</table>
