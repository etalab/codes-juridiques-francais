# Article R1682-1

Les dispositions des articles R. 1682-2 à R. 1682-4 sont applicables dans les départements d'outre-mer, à Saint-Pierre-et-Miquelon, à Mayotte, dans les îles Wallis et Futuna, en Polynésie française, en Nouvelle-Calédonie et dans les Terres australes et antarctiques françaises.
