# Section 2 : Répartition des ressources industrielles

- [Article R1682-5](article-r1682-5.md)
- [Article R1682-6](article-r1682-6.md)
- [Article R1682-7](article-r1682-7.md)
- [Article R1682-8](article-r1682-8.md)
