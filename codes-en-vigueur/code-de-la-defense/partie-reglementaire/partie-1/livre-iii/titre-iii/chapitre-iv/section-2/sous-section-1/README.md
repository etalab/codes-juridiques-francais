# Sous-section 1 : Dispositions générales.

- [Article D1334-5](article-d1334-5.md)
- [Article D1334-6](article-d1334-6.md)
- [Article D1334-7](article-d1334-7.md)
- [Article D1334-8](article-d1334-8.md)
