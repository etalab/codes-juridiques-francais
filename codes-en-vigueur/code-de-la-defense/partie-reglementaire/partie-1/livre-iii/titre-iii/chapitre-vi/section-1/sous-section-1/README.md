# Sous-section 1 : Dispositions générales

- [Article R*1336-1](article-r-1336-1.md)
- [Article R*1336-2](article-r-1336-2.md)
- [Article R*1336-3](article-r-1336-3.md)
