# Sous-section 2 : Stocks stratégiques

- [Article D1336-47](article-d1336-47.md)
- [Article D1336-48](article-d1336-48.md)
- [Article D1336-49](article-d1336-49.md)
- [Article D1336-50](article-d1336-50.md)
- [Article D1336-51](article-d1336-51.md)
- [Article D1336-52](article-d1336-52.md)
- [Article D1336-53](article-d1336-53.md)
- [Article D1336-54](article-d1336-54.md)
- [Article D1336-55](article-d1336-55.md)
- [Article D1336-56](article-d1336-56.md)
