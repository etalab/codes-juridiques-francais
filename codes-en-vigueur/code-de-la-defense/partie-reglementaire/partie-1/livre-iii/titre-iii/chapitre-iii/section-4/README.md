# Section 4 : Dispositions diverses

- [Sous-section 1 : Classement des matières nucléaires pour leur protection contre la perte, le vol et le détournement](sous-section-1)
- [Sous-section 2 : Exercice du contrôle](sous-section-2)
- [Sous-section 3 : Sanctions pénales et administratives](sous-section-3)
