# Article R1333-1

I.-Les dispositions de la présente section tendent à la protection des matières nucléaires contre la perte, le vol, le détournement ou tout acte visant à les altérer, les détériorer ou les disperser.

Cet impératif de protection s'étend aux installations où elles sont détenues, aux dispositifs de sécurité qui équipent ces installations et à ceux qui sont utilisés pour le transport de ces matières.

On entend par " installations " les locaux ou ouvrages dans lesquels les matières nucléaires sont détenues.

II.-La liste des matières fusibles, fissiles ou fertiles mentionnée à l'article L. 1333-1 du présent code comprend : le plutonium, l'uranium, le thorium, le deutérium, le tritium et le lithium 6.

III.-Sont soumises aux dispositions de la présente section les matières dites nucléaires énumérées au II ci-dessus et les composés chimiques comportant un de ces éléments à l'exception des minerais.

Les matières nucléaires affectées aux moyens nécessaires à la mise en œuvre de la politique de dissuasion sont régies par les dispositions de la section 2 du chapitre Ier du titre Ier du livre IV.
