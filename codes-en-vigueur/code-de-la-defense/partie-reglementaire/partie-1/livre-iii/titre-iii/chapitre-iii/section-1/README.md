# Section 1 : Protection et contrôle des matières nucléaires non affectées aux moyens nécessaires à la mise en œuvre de la politique de dissuasion

- [Paragraphe 1 : Champ d'application](paragraphe-1)
- [Paragraphe 2 : Autorisation et déclaration](paragraphe-2)
- [Paragraphe 3 : Suivi et comptabilité des matières nucléaires](paragraphe-3)
- [Paragraphe 4 : Confinement, surveillance et protection des matières nucléaires dans les établissements et installations](paragraphe-4)
- [Paragraphe 5 : Transports](paragraphe-5)
