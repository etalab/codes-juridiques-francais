# Article R1333-19

Tout incident ou accident affectant un transport de matières nucléaires est porté sans délai par le transporteur à la connaissance de l'Institut de radioprotection et de sûreté nucléaire, lequel informe sans délai les services de police ou de gendarmerie, ainsi que le ministre compétent.
