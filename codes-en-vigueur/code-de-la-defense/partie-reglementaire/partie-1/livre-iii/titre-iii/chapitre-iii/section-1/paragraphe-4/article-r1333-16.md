# Article R1333-16

Les renseignements, procédés, objets, documents, données informatisées ou fichiers relatifs au suivi physique et à la comptabilité des matières nucléaires et à leur protection en cours de transport ou au sein d'une installation ainsi qu'aux infrastructures, dispositifs et équipements concourant à leur protection sont protégés dans les conditions prévues aux articles R. 2311-1 à R. 2311-8 du présent code.
