# Article R*1333-47-1

Les autorisations et les déclarations concernant les équipements et installations situées dans le périmètre de l'installation nucléaire de base secrète et nécessaires au fonctionnement de l'installation, mentionnés à l'article L. 1333-17, sont instruites et délivrées par le délégué à la sûreté nucléaire et à la radioprotection pour les installations et activités intéressant la défense mentionné à l'article R. * 1333-67-5.

Les demandes d'autorisation et les déclarations concernant les équipements et installations mentionnés à l'article L. 1333-18 sont adressées au délégué. Ce dernier transmet les demandes d'autorisation au préfet pour qu'il procède ou fasse procéder aux consultations et enquêtes prévues, suivant le cas, au chapitre 4 du titre Ier du livre II ou à l'article L. 512-2 du code de l'environnement. Le préfet transmet au délégué, avec son avis, les résultats des consultations et des enquêtes effectuées.
