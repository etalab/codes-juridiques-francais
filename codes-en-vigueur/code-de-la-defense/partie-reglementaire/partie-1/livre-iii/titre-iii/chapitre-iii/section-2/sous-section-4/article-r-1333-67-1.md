# Article R*1333-67-1

Les sites et installations d'expérimentations nucléaires intéressant la défense, mentionnés au 3° de l'article L. 1333-15 et dont la liste est fixée par un arrêté conjoint du ministre de la défense et du ministre chargé de l'industrie, sont définis par leur périmètre figurant sur un plan dont un exemplaire est transmis au préfet.

Toute modification de ce périmètre est soumise, selon le cas, à décision du ministre de la défense ou du ministre chargé de l'industrie, prise sur avis du délégué à la sûreté nucléaire et à la radioprotection pour les installations et activités intéressant la défense mentionné à l'article R. * 1333-67-5.

Le délégué y assure la surveillance en matière de protection de l'environnement, de sûreté nucléaire et de radioprotection.
