# Article R*1333-40

I.-Le classement en installation nucléaire de base secrète, mentionnée au 1° de l'article L. 1333-15, est décidé par le Premier ministre sur proposition du ministre compétent. Celui-ci étant, selon le cas, le ministre de la défense ou le ministre chargé de l'industrie pour leurs installations respectives.

Ce classement est prononcé lorsqu'une au moins des installations comprises dans le périmètre, dénommée installation individuelle, présente les caractéristiques techniques fixées par arrêté conjoint du ministre de la défense et du ministre chargé de l'industrie, intéresse la défense nationale et justifie d'une protection particulière contre la prolifération nucléaire, la malveillance ou la divulgation d'informations classifiées.

II.-Les installations nucléaires de base secrètes sont définies par leur périmètre fixé par le plan annexé à la décision de classement. En font partie l'ensemble des installations et équipements, nucléaires ou non, compris dans le périmètre susmentionné.
