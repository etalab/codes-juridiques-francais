# Sous-section 2 : Elaboration et approbation du plan particulier de protection

- [Article R1332-23](article-r1332-23.md)
- [Article R1332-24](article-r1332-24.md)
- [Article R1332-25](article-r1332-25.md)
- [Article R1332-26](article-r1332-26.md)
- [Article R1332-27](article-r1332-27.md)
