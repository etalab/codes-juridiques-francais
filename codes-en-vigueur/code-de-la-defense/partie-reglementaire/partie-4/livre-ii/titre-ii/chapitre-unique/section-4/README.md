# Section 4 : Souscription et exécution de la clause de réactivité

- [Article R4221-11](article-r4221-11.md)
- [Article R4221-12](article-r4221-12.md)
- [Article R4221-13](article-r4221-13.md)
- [Article R4221-14](article-r4221-14.md)
