# Section 5 : Exécution de l'engagement à servir  dans la réserve auprès d'une entreprise

- [Article R4221-15](article-r4221-15.md)
- [Article R4221-16](article-r4221-16.md)
- [Article R4221-17](article-r4221-17.md)
