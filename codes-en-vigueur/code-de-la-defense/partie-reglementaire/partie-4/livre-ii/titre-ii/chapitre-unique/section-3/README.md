# Section 3 : Exécution de l'engagement à servir  dans la réserve opérationnelle

- [Article R4221-9](article-r4221-9.md)
- [Article R4221-10](article-r4221-10.md)
- [Article R4221-10-1](article-r4221-10-1.md)
