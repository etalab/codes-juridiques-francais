# Chapitre unique

- [Section 5-1 : Exécution de l'engagement à servir dans la réserve auprès d'une administration de l'Etat, d'un établissement public administratif, d'un établissement public à caractère scientifique, culturel et professionnel ou d'une organisation internationale](section-5-1)
- [Section 1 : Souscription de l'engagement à servir  dans la réserve opérationnelle](section-1)
- [Section 2 : Prolongation de la durée d'activité  au-delà de trente jours par an](section-2)
- [Section 3 : Exécution de l'engagement à servir  dans la réserve opérationnelle](section-3)
- [Section 4 : Souscription et exécution de la clause de réactivité](section-4)
- [Section 5 : Exécution de l'engagement à servir  dans la réserve auprès d'une entreprise](section-5)
- [Section 6 : Suspension ou résiliation de l'engagement à servir  dans la réserve opérationnelle](section-6)
- [Section 7 : Dispositions relatives à la nomination et à l'avancement](section-7)
