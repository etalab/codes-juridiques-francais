# Article D4221-6

La durée des activités dans la réserve opérationnelle peut, dans les mêmes conditions qu'à l'article R. 4221-5, être portée à soixante jours :

1° Pour l'encadrement des périodes militaires d'initiation ou de perfectionnement à la défense nationale, et de la journée défense et citoyenneté ;

2° Ou lorsque le réserviste a suivi une formation dans l'année en cours.

Le contrôle général des armées, chaque armée et formation rattachée, dans la limite de 15 % de l'effectif de la réserve opérationnelle sous contrat d'engagement au 1er janvier de l'année en cours, déterminent le nombre de réservistes qui, ne participant pas aux activités définies aux alinéas précédents, sont autorisés à porter la durée de leur activité à soixante jours, afin de faire bénéficier le ministère de la défense, ou pour les réservistes de la gendarmerie nationale le ministère de la défense et le ministère de l'intérieur d'un renfort temporaire ou de compétences spécifiques nécessaires à l'accomplissement de missions requérant une présence d'une durée supérieure à trente jours.
