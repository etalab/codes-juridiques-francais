# Section 2 : Composition et organisation

- [Sous-section 1 : L'assemblée plénière](sous-section-1)
- [Sous-section 2 : Le conseil restreint](sous-section-2)
- [Sous-section 3 : Les commissions](sous-section-3)
- [Sous-section 4 : Les groupes de travail](sous-section-4)
- [Article D4261-2](article-d4261-2.md)
- [Article D4261-3](article-d4261-3.md)
- [Article D4261-4](article-d4261-4.md)
- [Article D4261-5](article-d4261-5.md)
- [Article D4261-6](article-d4261-6.md)
