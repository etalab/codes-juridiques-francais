# Article D4261-9

Le contrôle général des armées est informé des réunions du conseil restreint, auxquelles un de ses membres peut assister.
