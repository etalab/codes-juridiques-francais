# Article D4261-1

Le Conseil supérieur de la réserve militaire a pour missions :

1° De participer à la réflexion sur le rôle des réserves militaires au service de la défense et de la sécurité nationale ;

2° De constituer un lieu de consultation et d'échange sur toute question d'ordre général relative à la mise en œuvre du présent livre ;

3° De favoriser le développement d'un partenariat durable entre les armées et formations rattachées, les réservistes et leurs employeurs ;

4° De contribuer à la promotion de l'esprit de défense et au développement du lien entre la nation et ses forces armées ;

5° D'établir pour le ministre de la défense un rapport annuel, transmis au Parlement, évaluant l'état de la réserve militaire.
