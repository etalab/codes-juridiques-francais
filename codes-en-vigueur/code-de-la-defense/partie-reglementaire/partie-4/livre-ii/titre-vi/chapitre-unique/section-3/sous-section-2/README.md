# Sous-section 2 : Le conseil restreint

- [Article D4261-17](article-d4261-17.md)
- [Article D4261-18](article-d4261-18.md)
- [Article D4261-19](article-d4261-19.md)
