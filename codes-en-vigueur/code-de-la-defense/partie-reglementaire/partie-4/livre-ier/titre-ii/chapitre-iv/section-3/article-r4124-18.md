# Article R4124-18

Les secrétaires généraux du Conseil supérieur de la fonction militaire et des conseils de la fonction militaire reçoivent les propositions d'inscription à l'ordre du jour formulées par les membres de ces conseils.

Les secrétaires généraux des conseils de la fonction militaire recueillent au préalable l'avis du vice-président du conseil auquel ils appartiennent.

Après s'être assurés que ces propositions relèvent de la compétence respective du Conseil supérieur de la fonction militaire et des conseils de la fonction militaire, les secrétaires généraux les soumettent au ministre de la défense, qui arrête l'ordre du jour.

Par dérogation à l'alinéa précédent, l'ordre du jour du conseil de la fonction militaire de la gendarmerie nationale est arrêté conjointement par les ministres de la défense et de l'intérieur ou par l'une de ces deux autorités lorsque son contenu ne relève que de ses seules attributions.

Sont inscrites d'office à l'ordre du jour les questions, entrant dans la compétence du conseil, dont l'examen a été demandé par la majorité des membres dudit conseil. Le ministre de la défense ou, pour le conseil de la fonction militaire de la gendarmerie nationale, le ministre de la défense et le ministre de l'intérieur peuvent inscrire à l'ordre du jour toute question de la compétence d'un conseil.

Sauf cas d'urgence, l'ordre du jour et le dossier de travail sont adressés au moins trente jours avant l'ouverture de la session aux membres convoqués et aux personnes appelées à assister à la session.
