# Article R4124-10

Les membres titulaires des conseils de la fonction militaire et les suppléants sont désignés par voie de tirage au sort parmi les militaires ayant fait acte de volontariat au sein d'une population déterminée pour chaque armée ou formation rattachée, selon des modalités fixées par arrêté du ministre de la défense.

Ne peuvent se porter volontaires les membres du corps militaire du contrôle général des armées, les officiers généraux, les secrétaires généraux des conseils mentionnés au présent chapitre, leurs adjoints et les volontaires dans les armées.

Le renouvellement des membres intervient par moitié tous les deux ans, conformément à une répartition en deux groupes fixée par arrêté du ministre de la défense.

Les membres reçoivent une formation spécifique en vue de l'accomplissement de leur fonction.
