# Section 1 : Dispositions générales

- [Sous-section 1 : Devoirs et responsabilités du chef  et du subordonné militaires](sous-section-1)
- [Sous-section 2 : Respect des règles du droit international  applicable aux conflits armés](sous-section-2)
- [Sous-section 3 : Respect de la neutralité des forces armées  et protection du moral et de la discipline](sous-section-3)
- [Sous-section 4 : Vaccination](sous-section-4)
