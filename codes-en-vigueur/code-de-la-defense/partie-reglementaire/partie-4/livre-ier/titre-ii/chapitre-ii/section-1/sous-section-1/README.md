# Sous-section 1 : Devoirs et responsabilités du chef  et du subordonné militaires

- [Article D4122-1](article-d4122-1.md)
- [Article D4122-2](article-d4122-2.md)
- [Article D4122-3](article-d4122-3.md)
- [Article D4122-4](article-d4122-4.md)
- [Article D4122-5](article-d4122-5.md)
- [Article D4122-6](article-d4122-6.md)
