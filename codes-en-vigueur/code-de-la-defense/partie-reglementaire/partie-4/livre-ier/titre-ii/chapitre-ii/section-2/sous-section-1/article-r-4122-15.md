# Article R*4122-15

Tout changement d'activité privée lucrative pendant la durée de la disponibilité, du congé, du placement en deuxième section ou pendant le délai prévu à l'article 432-13 du code pénal, à compter de la cessation définitive de fonctions, est porté dans les mêmes conditions à la connaissance du ministre de la défense, ou du ministre de l'intérieur pour les militaires de la gendarmerie nationale, dans les conditions prévues à l'article R. * 4122-14.
