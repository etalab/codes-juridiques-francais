# Sous-section 1 : Exercice d'activités privées lucratives par certains militaires.

- [Article R*4122-14](article-r-4122-14.md)
- [Article R*4122-15](article-r-4122-15.md)
- [Article R*4122-16](article-r-4122-16.md)
- [Article R*4122-17](article-r-4122-17.md)
- [Article R*4122-18](article-r-4122-18.md)
- [Article R*4122-19](article-r-4122-19.md)
- [Article R*4122-20](article-r-4122-20.md)
- [Article R*4122-21](article-r-4122-21.md)
- [Article R*4122-22](article-r-4122-22.md)
- [Article R*4122-23](article-r-4122-23.md)
- [Article R*4122-24](article-r-4122-24.md)
