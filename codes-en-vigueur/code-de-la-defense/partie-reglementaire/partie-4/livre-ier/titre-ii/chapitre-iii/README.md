# Chapitre III : Rémunération, garanties et protections

- [Section 1 : Rémunération](section-1)
- [Section 2 : Garanties et couverture des risques](section-2)
- [Section 3 : Dispositions au bénéfice d'enfants mineurs de militaires  tués ou blessés accidentellement en temps de paix](section-3)
