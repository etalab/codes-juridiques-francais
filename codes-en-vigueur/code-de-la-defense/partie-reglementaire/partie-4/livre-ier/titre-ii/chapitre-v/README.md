# Chapitre V : Recours administratif préalable

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Dispositions particulières aux militaires rattachés organiquement à un ministre autre que le ministre de la défense](section-2)
- [Article R4125-1](article-r4125-1.md)
