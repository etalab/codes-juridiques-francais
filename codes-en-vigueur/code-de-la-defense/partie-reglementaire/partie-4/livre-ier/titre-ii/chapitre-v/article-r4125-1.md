# Article R4125-1

I.-Tout recours contentieux formé par un militaire à l'encontre d'actes relatifs à sa situation personnelle est précédé d'un recours administratif préalable, à peine d'irrecevabilité du recours contentieux.

Ce recours administratif préalable est examiné par la commission des recours des militaires, placée auprès du ministre de la défense.

La saisine de la commission est seule de nature à conserver le délai de recours contentieux jusqu'à l'intervention de la décision prévue à l'article R. 4125-10.

II.-Les dispositions de la présente section ne sont pas applicables aux recours contentieux formés à l'encontre d'actes ou de décisions :

1° Concernant le recrutement du militaire ou l'exercice du pouvoir disciplinaire ;

2° Pris en application du code des pensions militaires d'invalidité et des victimes de la guerre et du code des pensions civiles et militaires de retraite ainsi que ceux qui relèvent de la procédure organisée par les articles 112 à 124 du décret n° 2012-1246 du 7 novembre 2012 relatif à la gestion budgétaire et comptable publique.
