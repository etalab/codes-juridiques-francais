# Article R4139-43

Pour être admis au bénéfice d'un pécule les officiers doivent :

1° Avoir accompli à la date de leur radiation des cadres moins de dix-huit ans de services ouvrant droit à une pension du code des pensions civiles et militaires de retraite ;

2° Ne pas bénéficier d'une mesure de reclassement dans un emploi public en application des dispositions de l'article L. 4139-2.
