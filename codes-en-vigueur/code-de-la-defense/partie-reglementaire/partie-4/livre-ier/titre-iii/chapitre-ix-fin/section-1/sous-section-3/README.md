# Sous-section 3 :  Dispositions particulières aux modalités spécifiques de détachement et d'intégration des militaires dans un corps relevant de la fonction publique de l'Etat

- [Article R*4139-14](article-r-4139-14.md)
- [Article R*4139-15](article-r-4139-15.md)
- [Article R*4139-16](article-r-4139-16.md)
- [Article R*4139-17](article-r-4139-17.md)
- [Article R*4139-18](article-r-4139-18.md)
- [Article R*4139-19](article-r-4139-19.md)
- [Article R*4139-20](article-r-4139-20.md)
- [Article R4139-20-1](article-r4139-20-1.md)
- [Article R*4139-21](article-r-4139-21.md)
- [Article R*4139-22](article-r-4139-22.md)
