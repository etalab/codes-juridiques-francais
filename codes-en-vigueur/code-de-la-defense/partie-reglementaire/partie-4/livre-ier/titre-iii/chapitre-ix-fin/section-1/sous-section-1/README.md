# Sous-section 1 :  Dispositions relatives au détachement ou au classement des militaires lauréats de concours de la fonction publique ou de la magistrature

- [Article R4139-1](article-r4139-1.md)
- [Article R4139-3](article-r4139-3.md)
- [Article R4139-4](article-r4139-4.md)
- [Article R4139-5](article-r4139-5.md)
- [Article R4139-6](article-r4139-6.md)
- [Article R4139-7](article-r4139-7.md)
- [Article R4139-8](article-r4139-8.md)
- [Article R4139-9](article-r4139-9.md)
