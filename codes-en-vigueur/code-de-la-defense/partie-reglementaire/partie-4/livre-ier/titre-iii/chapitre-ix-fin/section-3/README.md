# Section 3 : Radiation des cadres ou des contrôles

- [Sous-section 1 : Dispositions générales](sous-section-1)
- [Sous-section 2 : Lien au service](sous-section-2)
- [Sous-section 3 : Commission de réforme](sous-section-3)
