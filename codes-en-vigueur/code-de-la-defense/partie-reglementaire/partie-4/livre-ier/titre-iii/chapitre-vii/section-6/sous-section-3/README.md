# Sous-section 3 : Constitution du conseil supérieur d'armée  ou de formation rattachée siégeant disciplinairement

- [Article R4137-96](article-r4137-96.md)
- [Article R4137-97](article-r4137-97.md)
- [Article R4137-98](article-r4137-98.md)
- [Article R4137-99](article-r4137-99.md)
- [Article R4137-100](article-r4137-100.md)
