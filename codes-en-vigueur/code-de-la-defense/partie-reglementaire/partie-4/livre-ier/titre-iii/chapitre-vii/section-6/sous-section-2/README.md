# Sous-section 2 : Composition du conseil supérieur d'armée  ou de formation rattachée siégeant disciplinairement

- [Article R4137-94](article-r4137-94.md)
- [Article R4137-95](article-r4137-95.md)
