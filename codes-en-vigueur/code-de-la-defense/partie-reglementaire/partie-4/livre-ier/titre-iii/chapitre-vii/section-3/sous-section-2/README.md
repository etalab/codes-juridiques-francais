# Sous-section 2 : Sanctions disciplinaires du premier groupe

- [Article R4137-25](article-r4137-25.md)
- [Article R4137-26](article-r4137-26.md)
- [Article R4137-27](article-r4137-27.md)
- [Article R4137-28](article-r4137-28.md)
- [Article R4137-29](article-r4137-29.md)
- [Article R4137-30](article-r4137-30.md)
- [Article R4137-31](article-r4137-31.md)
- [Article R4137-32](article-r4137-32.md)
- [Article R4137-33](article-r4137-33.md)
