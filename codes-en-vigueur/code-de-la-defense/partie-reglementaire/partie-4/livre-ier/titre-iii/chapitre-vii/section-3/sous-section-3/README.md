# Sous-section 3 : Sanctions disciplinaires du deuxième groupe

- [Article R4137-34](article-r4137-34.md)
- [Article R4137-35](article-r4137-35.md)
- [Article R4137-36](article-r4137-36.md)
- [Article R4137-37](article-r4137-37.md)
- [Article R4137-38](article-r4137-38.md)
- [Article R4137-39](article-r4137-39.md)
- [Article R4137-40](article-r4137-40.md)
