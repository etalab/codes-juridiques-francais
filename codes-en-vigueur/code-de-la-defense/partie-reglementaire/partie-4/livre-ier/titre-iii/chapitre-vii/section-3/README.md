# Section 3 : Sanctions disciplinaires

- [Sous-section 1 : Principes](sous-section-1)
- [Sous-section 2 : Sanctions disciplinaires du premier groupe](sous-section-2)
- [Sous-section 3 : Sanctions disciplinaires du deuxième groupe](sous-section-3)
- [Sous-section 4 : Sanctions disciplinaires du troisième groupe](sous-section-4)
- [Sous-section 5 : Suspension de fonctions](sous-section-5)
