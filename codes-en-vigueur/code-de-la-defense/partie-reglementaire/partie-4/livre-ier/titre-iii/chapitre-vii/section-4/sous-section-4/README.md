# Sous-section 4 : Fonctionnement du conseil de discipline

- [Article R4137-57](article-r4137-57.md)
- [Article R4137-58](article-r4137-58.md)
- [Article R4137-59](article-r4137-59.md)
- [Article R4137-60](article-r4137-60.md)
- [Article R4137-61](article-r4137-61.md)
- [Article R4137-62](article-r4137-62.md)
- [Article R4137-63](article-r4137-63.md)
- [Article R4137-64](article-r4137-64.md)
- [Article R4137-65](article-r4137-65.md)
