# Chapitre VIII : Positions statutaires

- [Section 1 : Activité](section-1)
- [Section 2 : Détachement](section-2)
- [Section 3 : Hors cadres](section-3)
- [Section 4 : Non-activité](section-4)
- [Section 5 :  Délégations de pouvoirs et de signature en matière de mesures individuelles et notification des changements de positions ou de situations statutaires](section-5)
