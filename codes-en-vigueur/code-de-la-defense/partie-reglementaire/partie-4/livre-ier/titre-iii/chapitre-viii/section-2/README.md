# Section 2 : Détachement

- [Article R4138-34](article-r4138-34.md)
- [Article R4138-35](article-r4138-35.md)
- [Article R4138-36](article-r4138-36.md)
- [Article R4138-37](article-r4138-37.md)
- [Article R4138-38](article-r4138-38.md)
- [Article R4138-39](article-r4138-39.md)
- [Article R4138-40](article-r4138-40.md)
- [Article R4138-41](article-r4138-41.md)
- [Article R4138-42](article-r4138-42.md)
- [Article R4138-43](article-r4138-43.md)
- [Article R4138-44](article-r4138-44.md)
