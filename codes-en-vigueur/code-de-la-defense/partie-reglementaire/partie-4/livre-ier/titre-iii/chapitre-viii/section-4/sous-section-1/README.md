# Sous-section 1 : Congé de longue durée pour maladie

- [Article R4138-47](article-r4138-47.md)
- [Article R4138-48](article-r4138-48.md)
- [Article R4138-49](article-r4138-49.md)
- [Article R4138-50](article-r4138-50.md)
- [Article R4138-51](article-r4138-51.md)
- [Article R4138-52](article-r4138-52.md)
- [Article R4138-53](article-r4138-53.md)
- [Article R4138-54](article-r4138-54.md)
- [Article R4138-55](article-r4138-55.md)
- [Article R4138-56](article-r4138-56.md)
- [Article R4138-57](article-r4138-57.md)
