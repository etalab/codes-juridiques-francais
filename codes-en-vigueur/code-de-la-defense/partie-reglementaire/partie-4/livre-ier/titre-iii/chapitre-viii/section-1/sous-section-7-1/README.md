# Sous-section 7-1 : Le congé pour création ou reprise d'entreprise

- [Article R4138-29-1](article-r4138-29-1.md)
- [Article R4138-29-2](article-r4138-29-2.md)
- [Article R4138-29-3](article-r4138-29-3.md)
