# Article R4138-2

I.-Les congés prévus aux articles R. 4138-4 à R. 4138-6, R. 4138-27 et R. 4138-28 sont accordés par le ministre de la défense.

Le congé d'accompagnement d'une personne en fin de vie prévu à l'article L. 4138-6 du code de la défense est accordé par le ministre de la défense.

II.-Pour les militaires de la gendarmerie nationale, les congés prévus aux articles L. 4138-6, R. 4138-4 à R. 4138-6 et R. 4138-27 sont accordés par le ministre de l'intérieur, le congé prévu à l'article R. 4138-28 est accordé par décision conjointe du ministre de la défense et du ministre de l'intérieur.
