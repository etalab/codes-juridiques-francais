# Article R2335-7

L'autorisation d'importation peut être suspendue, modifiée, abrogée ou retirée par le ministre chargé des douanes, après avis favorable, en fonction de leurs attributions respectives, du ministre de la défense, du ministre de l'intérieur, ou du ministre des affaires étrangères, pour l'un des motifs mentionnés au IV de l'article L. 2335-1.

En cas d'urgence, le ministre chargé des douanes peut suspendre l'autorisation d'importation sans délai.

La modification, l'abrogation ou le retrait de l'autorisation d'importation ne peut intervenir qu'après que le titulaire de l'autorisation a été mis à même de faire valoir ses observations, dans un délai de quinze jours, selon les modalités prévues à l'article 24 de la loi n° 2000-321 du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations.

La décision portant suspension, modification, abrogation ou retrait de l'autorisation d'importation est notifiée au titulaire par le ministre chargé des douanes.
