# Article R2335-40-1

<div align="left">I. ― En application du V de l'article L. 2335-10, le transfert à partir d'un autre Etat membre de l'Union européenne vers la France des armes, munitions et leurs éléments des 1° et 2° de la catégorie A2 énumérés à
l' article R. 311-2 du code de la sécurité intérieure  est soumis à l'autorisation mentionnée à l'article L. 2335-1 et à ses textes d'application. <br/>
<br/>II. ― Le transfert des armes, munitions et leurs éléments mentionnés au I renvoyés vers la France après exposition ou réparation est dispensé d'autorisation. <br/>
<br/>Une copie de cette autorisation accompagne les armes, les éléments d'arme, les munitions et les éléments de munition. Ce document doit être présenté à toute réquisition des autorités habilitées. <br/>
<br/>Lors de la réception des armes, des munitions et de leurs éléments, le destinataire inscrit leur nature et leur quantité sur la copie de l'autorisation correspondante.</div>
