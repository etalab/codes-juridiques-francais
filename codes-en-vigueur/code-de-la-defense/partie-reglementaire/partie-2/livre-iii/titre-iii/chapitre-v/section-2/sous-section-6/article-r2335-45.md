# Article R2335-45

L'autorisation de transit peut être modifiée, suspendue, abrogée ou retirée par le Premier ministre, après avis des ministres représentés de façon permanente à la commission interministérielle instituée par le décret n° 55-965 du 16 juillet 1955 portant réorganisation de la commission interministérielle pour l'étude des exportations de matériels de guerre, du ministre chargé des douanes et du ministre de l'intérieur, pour l'un des motifs mentionnés au IV de l'article L. 2335-1 et à l'article L. 2335-4.

En cas d'urgence, le Premier ministre peut suspendre l'autorisation de transit sans délai.

La modification, l'abrogation ou le retrait de l'autorisation de transit ne peut intervenir qu'après que son titulaire a été mis à même de faire valoir ses observations, dans un délai de quinze jours, selon les modalités prévues à l'article 24 de la loi n° 2000-321 du 12 avril 2000 relative aux droits des citoyens dans leurs relation avec les administrations.

La décision portant suspension, modification, abrogation ou retrait de l'autorisation de transit est notifiée à son titulaire par le ministre chargé des douanes.
