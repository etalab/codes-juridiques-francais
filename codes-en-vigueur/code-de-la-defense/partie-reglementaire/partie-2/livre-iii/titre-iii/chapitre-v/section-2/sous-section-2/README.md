# Sous-section 2 : Obligations des fournisseurs et des destinataires

- [Article R2335-28](article-r2335-28.md)
- [Article R2335-29](article-r2335-29.md)
- [Article R2335-30](article-r2335-30.md)
- [Article R2335-31](article-r2335-31.md)
