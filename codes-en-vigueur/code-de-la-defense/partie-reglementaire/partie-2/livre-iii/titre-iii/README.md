# TITRE III : MATERIELS DE GUERRE,  ARMES ET MUNITIONS SOUMIS A AUTORISATION

- [Chapitre V : Importations et exportations. ― Transferts au sein de l'Union européenne](chapitre-v)
- [Chapitre IX : Sanctions pénales](chapitre-ix)
