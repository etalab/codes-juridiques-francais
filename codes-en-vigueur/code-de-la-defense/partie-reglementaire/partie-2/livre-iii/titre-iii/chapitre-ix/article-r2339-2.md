# Article R2339-2

<div align="left">Est puni de la peine d'amende prévue pour les contraventions de la 5e classe le fait pour toute personne de ne pas inscrire sur les exemplaires des autorisations prévues au I de l'article R. 2235-40-1 les quantités d'armes, d'éléments d'arme, munitions ou éléments de munition qu'elle a reçus conformément aux dispositions du III du même article.</div>
