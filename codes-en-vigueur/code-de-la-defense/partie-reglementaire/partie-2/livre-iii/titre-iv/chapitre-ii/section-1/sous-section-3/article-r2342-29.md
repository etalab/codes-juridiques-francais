# Article R2342-29

Les autorisations d'exportation prévues au premier alinéa de l'article L. 2342-16 sont délivrées par le ministre chargé des douanes.
