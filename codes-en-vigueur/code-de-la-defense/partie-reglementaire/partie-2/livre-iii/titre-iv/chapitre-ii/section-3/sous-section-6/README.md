# Sous-section 6 : Activités avant l'inspection

- [Article D2342-80](article-d2342-80.md)
- [Article D2342-81](article-d2342-81.md)
- [Article D2342-82](article-d2342-82.md)
- [Article D2342-83](article-d2342-83.md)
