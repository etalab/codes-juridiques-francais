# Sous-section 8 : Prélèvements

- [Article D2342-88](article-d2342-88.md)
- [Article D2342-89](article-d2342-89.md)
- [Article D2342-90](article-d2342-90.md)
- [Article D2342-91](article-d2342-91.md)
