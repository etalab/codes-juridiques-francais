# Sous-section 4 : Observateur représentant l'Etat requérant

- [Article D2342-73](article-d2342-73.md)
- [Article D2342-74](article-d2342-74.md)
- [Article D2342-75](article-d2342-75.md)
