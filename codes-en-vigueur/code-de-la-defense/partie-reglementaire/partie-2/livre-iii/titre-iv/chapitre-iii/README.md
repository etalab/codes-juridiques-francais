# Chapitre III : Mines antipersonnel

- [Section 1 : Commission nationale pour l'élimination des mines antipersonnel](section-1)
- [Section 2 : Contrôles](section-2)
