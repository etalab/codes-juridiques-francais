# Article R2363-7

Lorsque la mise en œuvre des mesures prévues à l'article L. 131-3 du code de l'aviation civile ne suffit pas à faire cesser le survol irrégulier d'une zone de défense hautement sensible et que l'aéronef est utilisé ou sur le point d'être utilisé pour une agression armée contre cette zone, il est procédé à un tir de semonce.

Si ce tir n'est pas suivi d'effet, il peut être recouru à la force armée.

Il en est de même en cas de survol au moyen d'un parachute.
