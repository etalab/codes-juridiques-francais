# Article R2352-110

L'exploitation d'un dépôt, d'un débit ou d'une installation mobile de produits explosifs est subordonnée, indépendamment de l'agrément technique prévu à l'article R. 2352-97, à la délivrance par le préfet d'une autorisation individuelle à la personne physique qui entend se livrer à cette exploitation, ou, dans le cas d'une personne morale, à la personne physique ayant qualité pour représenter celle-ci.

Est dispensée de l'obligation d'autorisation individuelle l'exploitation :

1° Des installations de l'Etat relevant du ministre de la défense et de celles du Commissariat à l'énergie atomique ;

2° Des installations couvertes par le secret défense nationale et exploitées par des entreprises publiques ou privées travaillant pour la défense ;

3° Des installations de l'Etat relevant du ministre de l'intérieur ;

4° Des dépôts et débits de munitions et éléments de munitions des armes énumérées au décret n° 95-589 du 6 mai 1995 relatif à l'application du décret du 18 avril 1939 fixant le régime des matériels de guerre, armes et munitions ;

5° Des dépôts ou débits remplissant les conditions mentionnées à l'article R. 2352-92.
