# Article R2352-37

L'exportation de produits explosifs de France vers un pays tiers à la Communauté européenne est soumise à autorisation d'exportation délivrée par le ministre chargé des douanes après avis conforme du ministre chargé de l'industrie. La décision d'autorisation ou de refus est notifiée par ses soins au pétitionnaire.
