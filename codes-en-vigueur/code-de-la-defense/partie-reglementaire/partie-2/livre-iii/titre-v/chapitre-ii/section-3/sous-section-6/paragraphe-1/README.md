# Paragraphe 1 : Règles générales

- [Article R2352-89](article-r2352-89.md)
- [Article R2352-90](article-r2352-90.md)
- [Article R2352-91](article-r2352-91.md)
- [Article R2352-92](article-r2352-92.md)
- [Article R2352-93](article-r2352-93.md)
- [Article R2352-94](article-r2352-94.md)
- [Article R2352-95](article-r2352-95.md)
- [Article R2352-96](article-r2352-96.md)
