# Article R2352-42

La décision d'autorisation ou de refus est notifiée au pétitionnaire par le ministre chargé des douanes, qui en adresse copie au ministre chargé de l'intérieur.
