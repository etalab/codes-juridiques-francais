# Section 1 : Dispositions communes

- [Article R2352-1](article-r2352-1.md)
- [Article R2352-2](article-r2352-2.md)
- [Article R2352-3](article-r2352-3.md)
- [Article R2352-4](article-r2352-4.md)
- [Article R2352-5](article-r2352-5.md)
- [Article R2352-6](article-r2352-6.md)
