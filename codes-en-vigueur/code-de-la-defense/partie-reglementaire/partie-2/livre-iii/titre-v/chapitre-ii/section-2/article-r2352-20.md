# Article R2352-20

A l'expiration d'un délai de huit mois, les avis mentionnés à l'article R. 2352-19 sont réputés avoir été rendus.

Les conditions et la procédure de délivrance des autorisations mentionnées à l'article R. 2352-19 sont précisées par arrêtés conjoints des ministres chargés des douanes, de la défense et de l'intérieur et, s'agissant de l'autorisation d'exportation, du ministre des affaires étrangères.
