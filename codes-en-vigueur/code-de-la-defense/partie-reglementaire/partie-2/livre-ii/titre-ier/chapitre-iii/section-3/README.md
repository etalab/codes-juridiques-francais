# Section 3 : Réquisition de marchandises

- [Article R2213-15](article-r2213-15.md)
- [Article R*2213-16](article-r-2213-16.md)
- [Article R*2213-17](article-r-2213-17.md)
- [Article R*2213-18](article-r-2213-18.md)
- [Article R*2213-19](article-r-2213-19.md)
