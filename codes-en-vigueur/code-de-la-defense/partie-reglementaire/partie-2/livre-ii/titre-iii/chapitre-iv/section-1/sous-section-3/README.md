# Sous-section 3 : Evaluation des indemnités par voie de barèmes

- [Article R2234-36](article-r2234-36.md)
- [Article R2234-37](article-r2234-37.md)
- [Article R2234-38](article-r2234-38.md)
- [Article R2234-39](article-r2234-39.md)
- [Article R2234-40](article-r2234-40.md)
- [Article R2234-41](article-r2234-41.md)
- [Article R2234-42](article-r2234-42.md)
