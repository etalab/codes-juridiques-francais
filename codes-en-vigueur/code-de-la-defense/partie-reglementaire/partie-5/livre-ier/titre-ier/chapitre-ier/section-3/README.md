# Section 3 : Autorisation de construction dans un polygone d'isolement

- [Article R5111-6](article-r5111-6.md)
- [Article R5111-7](article-r5111-7.md)
- [Article R5111-7-1](article-r5111-7-1.md)
- [Article R5111-8](article-r5111-8.md)
- [Article R5111-9](article-r5111-9.md)
- [Article R5111-10](article-r5111-10.md)
