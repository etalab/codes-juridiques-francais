# Article R5111-7-1

Les autorisations prévues à l'article L. 5111-6 sont délivrées, pour leur zone ou leur domaine de compétence, par :

1° Le délégué général pour l'armement ;

2° L'inspecteur de l'armement pour les poudres et explosifs ;

3° Les commandants de zone terre ;

4° Les commandants d'arrondissement maritime ;

5° Le commandant de la défense aérienne et des opérations aériennes ;

6° Les commandants supérieurs des forces armées dans les départements et régions d'outre-mer, dans les collectivités d'outre-mer régies par l'article 74 de la Constitution et en Nouvelle-Calédonie.

Les autorités mentionnées aux 1°, 2°, 3°, 4°, 5° et 6° peuvent déléguer leur signature à leurs adjoints ou leurs subordonnés.

Les autorités mentionnées aux 3°, 4° et 5° recueillent l'avis du service interarmées des munitions pour les autorisations de construire dans les polygones d'isolement établis autour des établissements relevant de ce service.
