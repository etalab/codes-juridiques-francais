# Section 2 : Conditions d'autorisation de certaines
constructions dans les zones de servitudes

- [Sous-section 1 : Secteurs de construction réglementée](sous-section-1)
- [Sous-section 2 : Constructions soumises au régime de l'autorisation ministérielle préalable](sous-section-2)
- [Sous-section 3 : Dispositions communes](sous-section-3)
