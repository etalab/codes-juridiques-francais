# Chapitre IV : Autres installations de défense

- [Section 1 : Etablissement des servitudes des installations de défense](section-1)
- [Section 2 : Conditions d'autorisation de certaines
constructions dans les zones de servitudes](section-2)
