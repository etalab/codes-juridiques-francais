# Article R5112-2

L'article 3 du décret du 25 avril 1991 mentionné à l'article précédent est applicable aux servitudes de protection des ouvrages de défense des côtes et des installations de sécurité maritime.

Ces servitudes sont inscrites, dans chaque commune où elles s'appliquent, à l'annexe au plan local d'urbanisme prescrite par l'article R. * 126-1 du code de l'urbanisme.
