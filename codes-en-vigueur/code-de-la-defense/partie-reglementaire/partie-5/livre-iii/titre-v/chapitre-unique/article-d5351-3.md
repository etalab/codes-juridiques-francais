# Article D5351-3

Pour l'application de la présente partie du code à la Polynésie française :

1° La référence au         directeur de l'établissement du service d'infrastructure de la défense ou à l'établissement du service d'infrastructure de la défense est remplacée par la référence à la direction d'infrastructure de la défense ;

2° La référence au préfet est remplacée par la référence au haut-commissaire de la République en Polynésie française ;

3° La référence à la préfecture est remplacée par la référence au siège du haut-commissaire de la République en Polynésie française.
