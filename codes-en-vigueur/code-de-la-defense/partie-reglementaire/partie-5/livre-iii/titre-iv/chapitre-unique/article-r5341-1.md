# Article R5341-1

Sont applicables dans les îles Wallis et Futuna les articles R. 5111-1 à R. 5131-3, R. 5131-5, R. 5131-11 et R. 5131-16.
