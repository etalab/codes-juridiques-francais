# Article D5381-1

Pour l'application de la présente partie du code à Saint-Barthélemy, la référence au         directeur de l'établissement du service d'infrastructure de la défense ou à l'établissement du service d'infrastructure de la défense est remplacée par la référence à la direction d'infrastructure de la défense.
