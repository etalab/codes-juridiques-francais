# Partie réglementaire

- [PARTIE 1 : PRINCIPES GÉNÉRAUX DE LA DÉFENSE](partie-1)
- [PARTIE 2 : REGIMES JURIDIQUES DE DEFENSE](partie-2)
- [PARTIE 3 : LE MINISTERE DE LA DEFENSE ET LES ORGANISMES SOUS TUTELLE](partie-3)
- [PARTIE 4 : LE PERSONNEL MILITAIRE](partie-4)
- [PARTIE 5 : DISPOSITIONS ADMINISTRATIVES ET FINANCIERES](partie-5)
