# Article R3322-2

Le conseil supérieur de l'armement et le conseil supérieur du service d'infrastructure de la défense sont présidés par le ministre de la défense.

Le Conseil supérieur de la gendarmerie nationale est coprésidé par le ministre de la défense et le ministre de l'intérieur.

Les conseils supérieurs du service de santé des armées, du service des essences des armées, et du service du commissariat des armées sont présidés par le chef d'état-major des armées.
