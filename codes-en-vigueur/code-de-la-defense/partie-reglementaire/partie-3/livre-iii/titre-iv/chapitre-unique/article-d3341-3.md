# Article D3341-3

La présidence du comité consultatif de santé des armées est assurée par l'inspecteur général du service de santé des armées et, en cas d'absence ou d'empêchement, par le plus ancien en grade des inspecteurs du service de santé pour l'armée de terre, la marine ou l'armée de l'air.
