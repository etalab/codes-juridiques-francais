# Section 1 : Dispositions générales

- [Article R3423-1](article-r3423-1.md)
- [Article R3423-2](article-r3423-2.md)
- [Article R3423-3](article-r3423-3.md)
- [Article R3423-4](article-r3423-4.md)
