# Chapitre II : L'Institution de gestion sociale des armées

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Organisation et fonctionnement](section-2)
- [Section 3 : Dispositions financières](section-3)
