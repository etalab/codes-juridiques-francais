# Chapitre II : Cercles et foyers

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Organisation administrative et financière](section-2)
- [Section 3 : Dispositions spécifiques aux foyers](section-3)
- [Section 4 : Dispositions spécifiques au Cercle national des armées](section-4)
