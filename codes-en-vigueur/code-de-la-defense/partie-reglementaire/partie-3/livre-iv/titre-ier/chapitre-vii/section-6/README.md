# Section 6 : Régime financier et comptable

- [Article R3417-27](article-r3417-27.md)
- [Article R3417-29](article-r3417-29.md)
- [Article R3417-30](article-r3417-30.md)
- [Article R3417-31](article-r3417-31.md)
- [Article R3417-32](article-r3417-32.md)
