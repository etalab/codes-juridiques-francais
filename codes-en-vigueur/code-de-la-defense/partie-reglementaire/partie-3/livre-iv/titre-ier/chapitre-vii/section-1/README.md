# Section 1 : Dispositions générales

- [Article R3417-1](article-r3417-1.md)
- [Article R3417-2](article-r3417-2.md)
- [Article R3417-3](article-r3417-3.md)
