# Section 2 : Organisation et fonctionnement

- [Article R3417-4](article-r3417-4.md)
- [Article R3417-5](article-r3417-5.md)
- [Article R3417-6](article-r3417-6.md)
- [Article R3417-7](article-r3417-7.md)
- [Article R3417-8](article-r3417-8.md)
- [Article R3417-9](article-r3417-9.md)
- [Article R3417-10](article-r3417-10.md)
- [Article R3417-11](article-r3417-11.md)
- [Article R3417-12](article-r3417-12.md)
- [Article R3417-13](article-r3417-13.md)
- [Article R3417-14](article-r3417-14.md)
- [Article R3417-15](article-r3417-15.md)
