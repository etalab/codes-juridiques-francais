# Section 3 : Régime financier et comptable

- [Article R3416-23](article-r3416-23.md)
- [Article R3416-26](article-r3416-26.md)
- [Article R3416-27](article-r3416-27.md)
- [Article R3416-28](article-r3416-28.md)
