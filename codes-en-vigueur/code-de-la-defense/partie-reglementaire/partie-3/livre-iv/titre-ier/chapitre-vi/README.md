# Chapitre VI : L'établissement public administratif Service hydrographique et océanographique de la marine (SHOM)

- [Section 1 : Missions](section-1)
- [Section 2 : Organisation et fonctionnement](section-2)
- [Section 3 : Régime financier et comptable](section-3)
- [Section 4 : Personnel](section-4)
