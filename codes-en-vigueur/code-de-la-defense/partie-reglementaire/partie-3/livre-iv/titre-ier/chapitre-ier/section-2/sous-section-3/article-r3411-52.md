# Article R3411-52

L'Ecole nationale supérieure de techniques avancées peut prendre des participations financières et créer des filiales dans le cadre des missions définies à l'article R. 3411-30 en vue notamment d'assurer la valorisation de ses recherches.
