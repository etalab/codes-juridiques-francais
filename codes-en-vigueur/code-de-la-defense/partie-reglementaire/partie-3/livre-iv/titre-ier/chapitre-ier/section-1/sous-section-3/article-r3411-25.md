# Article R3411-25

L'ISAE est soumis au contrôle budgétaire de l'Etat prévu à l'article L. 719-9 du code de l'éducation. Les modalités de ce contrôle sont fixées par arrêté conjoint du ministre de la défense et du ministre chargé du budget.
