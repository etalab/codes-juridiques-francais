# Article R3411-66

Le directeur de             l'Ecole nationale supérieure de techniques avancées Bretagne est responsable devant le ministre de la défense de l'observation des règlements militaires à l'intérieur de l'établissement.
