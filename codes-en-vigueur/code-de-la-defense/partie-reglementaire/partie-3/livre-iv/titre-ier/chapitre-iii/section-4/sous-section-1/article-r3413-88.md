# Article R3413-88

L'Académie de marine est un établissement public national à caractère administratif, placé sous la tutelle du ministre de la défense. Le chef d'état-major de la marine exerce cette tutelle au nom du ministre.
