# Chapitre unique : Commandements de forces  françaises à l'étranger

- [Section 1 : Commandement des éléments français au Sénégal](section-1)
- [Section 2 : Commandement des forces françaises stationnées à Djibouti](section-2)
- [Section 3 : Commandement des forces françaises stationnées au Gabon](section-3)
- [Section 4 : Commandement des forces françaises et éléments civils stationnés en Allemagne](section-4)
- [Section 5 : Commandement des forces françaises stationnées aux Emirats arabes unis](section-5)
