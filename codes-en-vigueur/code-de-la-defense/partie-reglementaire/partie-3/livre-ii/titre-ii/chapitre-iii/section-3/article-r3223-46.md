# Article R3223-46

I. - Les commandements maritimes à compétence territoriale comprennent :

1° Les commandements d'arrondissement maritime ;

2° Les commandements de la marine en un lieu déterminé.

Ces commandements exercent le commandement organique des forces maritimes qui leur sont affectées ; ils peuvent, en outre, en exercer le commandement opérationnel.

II.-Les limites des arrondissements maritimes sont fixées par l'article R. * 1212-5.

III. - En dehors des chefs-lieux des arrondissements maritimes, là où les missions de la marine nationale le justifient, un commandement de la marine est constitué.

Dans les ports où il n'existe pas de commandement de la marine, un administrateur des affaires maritimes territorialement compétent peut représenter la marine nationale et assurer la suppléance de ses services.

IV.-Pour l'exercice des attributions prévues par les dispositions relatives à l'action de l'Etat en mer, les commandants d'arrondissement maritime sont préfets maritimes.
