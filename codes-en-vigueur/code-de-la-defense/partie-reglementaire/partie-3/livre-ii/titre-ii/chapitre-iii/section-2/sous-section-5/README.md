# Sous-section 5 : Dispositions particulières en cas de tension, de crise, de conflit armé ou de guerre

- [Article D3223-40](article-d3223-40.md)
- [Article D3223-41](article-d3223-41.md)
- [Article D3223-42](article-d3223-42.md)
- [Article D3223-43](article-d3223-43.md)
- [Article D3223-44](article-d3223-44.md)
- [Article D3223-45](article-d3223-45.md)
