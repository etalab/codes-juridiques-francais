# Section 6 : Dispositions particulières à certaines formations

- [Sous-section 1 : Les organismes de formation](sous-section-1)
- [Sous-section 2 : Le bataillon des marins-pompiers de Marseille](sous-section-2)
