# Sous-section 5 : Le commandement du service militaire adapté

- [Article D3222-19](article-d3222-19.md)
- [Article D3222-20](article-d3222-20.md)
- [Article D3222-21](article-d3222-21.md)
- [Article D3222-22](article-d3222-22.md)
