# Article D3222-20

Le commandant du service militaire adapté relève du chef d'état-major des armées. Il tient informé chaque commandant supérieur dans les départements et collectivités d'outre-mer et en Nouvelle-Calédonie des décisions concernant le service militaire adapté dans sa zone de compétence.
