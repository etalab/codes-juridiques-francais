# Section 5 : Dispositions particulières à certaines formations

- [Sous-section 1 : Les bases aériennes](sous-section-1)
- [Sous-section 2 : La direction des ressources humaines de l'armée de l'air](sous-section-2)
