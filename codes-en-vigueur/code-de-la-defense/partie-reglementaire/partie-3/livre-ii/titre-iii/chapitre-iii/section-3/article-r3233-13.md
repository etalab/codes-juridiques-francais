# Article R3233-13

La DIRISI participe à la conception et à la conduite des programmes d'équipement qui relèvent de sa compétence pour ce qui concerne l'exploitation et le soutien, en liaison avec les directeurs de programmes désignés en application du décret n° 2005-72 du 31 janvier 2005, fixant les attributions et l'organisation de la     direction générale de l'armement, et l'équipe intégrée que chacun d'eux anime.

Elle participe à la coordination et à l'expertise globale des systèmes d'information et de communication au sein du ministère de la défense.
