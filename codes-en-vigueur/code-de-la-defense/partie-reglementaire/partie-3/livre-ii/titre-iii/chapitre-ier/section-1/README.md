# Section 1 : Dispositions générales

- [Article R3231-1](article-r3231-1.md)
- [Article R3231-2](article-r3231-2.md)
- [Article R3231-3](article-r3231-3.md)
- [Article R3231-4](article-r3231-4.md)
- [Article R3231-5](article-r3231-5.md)
- [Article R3231-6](article-r3231-6.md)
- [Article R3231-7](article-r3231-7.md)
- [Article R3231-8](article-r3231-8.md)
- [Article R3231-9](article-r3231-9.md)
