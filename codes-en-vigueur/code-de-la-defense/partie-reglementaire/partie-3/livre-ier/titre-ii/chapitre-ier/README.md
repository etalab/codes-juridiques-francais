# Chapitre Ier : Les états-majors

- [Section 1 :  Responsabilités générales du chef d'état-major des armées](section-1)
- [Section 2 :  L'état-major des armées](section-2)
