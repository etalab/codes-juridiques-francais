# Article D3124-12

<div align="left">Les inspecteurs généraux mentionnés aux articles D. 3124-1 et D. 3124-7 exercent, outre les attributions prévues au présent chapitre, la fonction de médiateur militaire.</div>
