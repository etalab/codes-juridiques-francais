# Section 2 : Inspection générale de la gendarmerie nationale

- [Article D3122-12](article-d3122-12.md)
- [Article D3122-14](article-d3122-14.md)
