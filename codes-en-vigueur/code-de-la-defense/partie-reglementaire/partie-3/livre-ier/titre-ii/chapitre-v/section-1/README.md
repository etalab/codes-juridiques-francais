# Section 1 : Dispositions générales

- [Article R3125-1](article-r3125-1.md)
- [Article R3125-2](article-r3125-2.md)
- [Article R3125-3](article-r3125-3.md)
- [Article R3125-4](article-r3125-4.md)
- [Article R3125-5](article-r3125-5.md)
