# Chapitre II : Obligations et responsabilités

- [Article L4122-1](article-l4122-1.md)
- [Article L4122-2](article-l4122-2.md)
