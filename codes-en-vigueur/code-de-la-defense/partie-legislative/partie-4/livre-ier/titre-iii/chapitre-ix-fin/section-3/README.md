# Section 3 : Radiation des cadres ou des contrôles

- [Article L4139-12](article-l4139-12.md)
- [Article L4139-13](article-l4139-13.md)
- [Article L4139-14](article-l4139-14.md)
- [Article L4139-15](article-l4139-15.md)
