# Article L4138-4

Les congés     de maternité, de paternité et d'accueil de l'enfant ou d'adoption  sont d'une durée égale à celle prévue par la législation sur la sécurité sociale.
