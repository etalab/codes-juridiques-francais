# Article L4138-3-1

Le congé du blessé, d'une durée maximale de dix-huit mois, est attribué, après épuisement des droits à congés de maladie fixés à l'article L. 4138-3, au militaire blessé ou ayant contracté une maladie, en opération de guerre, au cours d'une opération qualifiée d'opération extérieure dans les conditions prévues à l'article L. 4123-4, sauf faute détachable du service, s'il se trouve dans l'impossibilité d'exercer ses fonctions et s'il présente une probabilité objective de réinsertion ou de reconversion au sein du ministère de la défense ou, pour les militaires de la gendarmerie nationale, au sein du ministère de l'intérieur.

Un décret en Conseil d'Etat fixe les modalités d'attribution de ce congé.
