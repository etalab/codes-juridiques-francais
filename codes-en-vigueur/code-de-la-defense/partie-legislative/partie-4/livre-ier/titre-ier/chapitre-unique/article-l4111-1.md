# Article L4111-1

L'armée de la République est au service de la Nation. Sa mission est de préparer et d'assurer par la force des armes la défense de la patrie et des intérêts supérieurs de la Nation.

L'état militaire exige en toute circonstance esprit de sacrifice, pouvant aller jusqu'au sacrifice suprême, discipline, disponibilité, loyalisme et neutralité. Les devoirs qu'il comporte et les sujétions qu'il implique méritent le respect des citoyens et la considération de la Nation.

Le statut énoncé au présent livre assure à ceux qui ont choisi cet état les garanties répondant aux obligations particulières imposées par la loi. Il prévoit des compensations aux contraintes et exigences de la vie dans les forces armées. Il offre à ceux qui quittent l'état militaire les moyens d'un retour à une activité professionnelle dans la vie civile et assure aux retraités militaires le maintien d'un lien avec l'institution.

Un Haut Comité d'évaluation de la condition militaire établit un rapport annuel, adressé au Président de la République et transmis au Parlement. La composition du Haut Comité d'évaluation de la condition militaire et ses attributions sont fixées par décret.
