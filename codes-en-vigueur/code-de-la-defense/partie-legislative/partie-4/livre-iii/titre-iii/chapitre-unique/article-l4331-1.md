# Article L4331-1

Sont applicables à Mayotte les dispositions des articles L. 4111-1 à L. 4271-5.
