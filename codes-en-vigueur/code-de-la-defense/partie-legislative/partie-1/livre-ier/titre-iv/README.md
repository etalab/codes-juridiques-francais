# TITRE IV : RESPONSABILITÉS DES MINISTRES EN MATIÈRE DE DÉFENSE

- [Chapitre Ier : Dispositions communes à l'ensemble des ministres](chapitre-ier)
- [Chapitre II : Dispositions particulières à certains ministres](chapitre-ii)
