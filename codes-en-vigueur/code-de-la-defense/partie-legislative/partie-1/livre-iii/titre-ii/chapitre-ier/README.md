# Chapitre Ier : Participation militaire à la défense et à la sécurité civiles

- [Article L1321-1](article-l1321-1.md)
- [Article L1321-2](article-l1321-2.md)
- [Article L1321-3](article-l1321-3.md)
