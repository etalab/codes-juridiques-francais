# Section 3 : Mesures prises à l'encontre des personnes à bord des navires

- [Article L1521-11](article-l1521-11.md)
- [Article L1521-12](article-l1521-12.md)
- [Article L1521-13](article-l1521-13.md)
- [Article L1521-14](article-l1521-14.md)
- [Article L1521-15](article-l1521-15.md)
- [Article L1521-16](article-l1521-16.md)
- [Article L1521-17](article-l1521-17.md)
- [Article L1521-18](article-l1521-18.md)
