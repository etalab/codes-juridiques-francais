# Article L1641-1

Sont applicables dans les îles Wallis et Futuna les dispositions des articles L. 1111-1 à L. 1333-20, L. 1411-1 à L. 1411-10 et L. 1521-1 à L. 1521-10.
