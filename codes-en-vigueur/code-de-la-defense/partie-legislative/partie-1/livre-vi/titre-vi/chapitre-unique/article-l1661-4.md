# Article L1661-4

Pour l'application de l'article L. 1322-2, la référence aux dispositions du code général des collectivités territoriales est remplacée par la référence aux disposition du code des communes de Nouvelle-Calédonie.
