# Article L1671-1

Sont applicables aux Terres australes et antarctiques françaises, sous réserve des stipulations du traité sur l'Antarctique publié par le décret n° 61-1300 du 30 novembre 1961, les dispositions des articles L. 1111-1 à L. 1333-20, L. 1411-1 à L. 1411-10 et L. 1521-1 à L. 1521-10.
