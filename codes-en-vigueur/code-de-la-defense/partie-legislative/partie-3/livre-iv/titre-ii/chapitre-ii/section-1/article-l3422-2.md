# Article L3422-2

L'institution gère les établissements sociaux ou médico-sociaux dépendant du ministre de la défense et dont la liste est arrêtée par celui-ci. Elle exerce en outre des activités à caractère social ou médico-social. Par dérogation aux dispositions de l'article L. 15, premier alinéa, du code du domaine de l'Etat, l'institution ne peut accepter qu'après autorisation du ministre de la défense les dons et legs qui lui sont faits sans charges, conditions, ni affectations immobilières.
