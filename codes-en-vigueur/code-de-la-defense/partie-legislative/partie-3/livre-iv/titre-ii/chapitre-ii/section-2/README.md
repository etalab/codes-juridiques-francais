# Section 2 : Organisation administrative et financière

- [Article L3422-3](article-l3422-3.md)
- [Article L3422-4](article-l3422-4.md)
- [Article L3422-5](article-l3422-5.md)
- [Article L3422-6](article-l3422-6.md)
- [Article L3422-7](article-l3422-7.md)
