# Article L3211-1

Les forces armées comprennent :

1° L'armée de terre, la marine nationale et l'armée de l'air, qui constituent les armées au sens du présent code ;

2° La gendarmerie nationale ;

3° Des services de soutien interarmées.
