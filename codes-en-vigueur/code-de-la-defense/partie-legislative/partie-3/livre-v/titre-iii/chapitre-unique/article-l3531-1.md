# Article L3531-1

Sont applicables à Mayotte les dispositions des articles L. 3125-1 à L. 3125-4, L. 3211-1 à L. 3211-3, L. 3225-1 et L. 3421-1 à L. 3422-7 du code de la défense.
