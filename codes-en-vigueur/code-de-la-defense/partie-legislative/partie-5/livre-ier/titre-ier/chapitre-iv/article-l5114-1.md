# Article L5114-1

Les installations de défense, dont les conditions de sécurité rendent nécessaire l'application des servitudes définies au présent chapitre, sont désignées par décret, pris après enquête conduite selon les modalités définies par les articles L. 1, L. 110-1, L. 110-2 et L. 122-4 du code de l'expropriation pour cause d'utilité publique.
