# Chapitre II : Ouvrages de défense des côtes ou de sécurité maritime

- [Article L5112-1](article-l5112-1.md)
- [Article L5112-2](article-l5112-2.md)
- [Article L5112-3](article-l5112-3.md)
