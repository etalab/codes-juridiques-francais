# Article L5371-1

Sous réserve des stipulations du traité sur l'Antarctique publié par le décret n° 61-1300 du 30 novembre 1961, sont applicables aux Terres australes et antarctiques françaises les dispositions des articles L. 5111-1 à L. 5112-3, L. 5114-1 à L. 5141-1 et L. 5221-1.

Les dispositions de l'article L. 5141-1 sont applicables dans leur rédaction résultant de l'ordonnance n° 2014-1567 du 22 décembre 2014 portant application de l'article 55 de la loi n° 2013-1168 du 18 décembre 2013 relative à la programmation militaire pour les années 2014 à 2019 et portant diverses dispositions concernant la défense et la sécurité nationale.
