# Article L2312-8

Dans le délai de quinze jours francs à compter de la réception de l'avis de la commission, ou à l'expiration du délai de deux mois mentionné à l'article L. 2312-7, l'autorité administrative notifie sa décision, assortie du sens de l'avis, à la juridiction ayant demandé la déclassification et la communication d'informations classifiées.

Le sens de l'avis de la commission est publié au Journal officiel de la République française.
