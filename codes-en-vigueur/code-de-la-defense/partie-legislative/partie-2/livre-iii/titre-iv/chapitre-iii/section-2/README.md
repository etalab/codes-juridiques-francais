# Section 2 : Régime juridique

- [Article L2343-2](article-l2343-2.md)
- [Article L2343-3](article-l2343-3.md)
- [Article L2343-4](article-l2343-4.md)
