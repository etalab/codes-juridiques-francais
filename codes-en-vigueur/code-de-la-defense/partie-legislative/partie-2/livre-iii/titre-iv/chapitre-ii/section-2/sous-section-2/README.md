# Sous-section 2 : Produits chimiques du tableau 2

- [Article L2342-12](article-l2342-12.md)
- [Article L2342-13](article-l2342-13.md)
- [Article L2342-14](article-l2342-14.md)
