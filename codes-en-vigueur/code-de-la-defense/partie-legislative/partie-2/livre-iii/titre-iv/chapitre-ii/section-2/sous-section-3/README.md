# Sous-section 3 : Produits chimiques du tableau 3

- [Article L2342-15](article-l2342-15.md)
- [Article L2342-16](article-l2342-16.md)
- [Article L2342-17](article-l2342-17.md)
