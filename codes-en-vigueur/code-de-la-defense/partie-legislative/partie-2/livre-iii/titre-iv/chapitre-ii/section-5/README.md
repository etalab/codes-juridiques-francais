# Section 5 : Dispositions pénales et sanctions administratives

- [Sous-section 1 : Agents habilités à constater les infractions](sous-section-1)
- [Sous-section 2 : Sanctions pénales](sous-section-2)
- [Sous-section 3 : Sanctions administratives](sous-section-3)
