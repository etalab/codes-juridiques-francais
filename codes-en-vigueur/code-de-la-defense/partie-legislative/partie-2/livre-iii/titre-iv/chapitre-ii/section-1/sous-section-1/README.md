# Sous-section 1 : Interdictions.

- [Article L2342-3](article-l2342-3.md)
- [Article L2342-4](article-l2342-4.md)
