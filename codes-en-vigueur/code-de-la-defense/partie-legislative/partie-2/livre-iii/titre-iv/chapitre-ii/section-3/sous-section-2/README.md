# Sous-section 2 : Exécution de la vérification internationale

- [Article L2342-31](article-l2342-31.md)
- [Article L2342-32](article-l2342-32.md)
- [Article L2342-33](article-l2342-33.md)
- [Article L2342-34](article-l2342-34.md)
- [Article L2342-35](article-l2342-35.md)
- [Article L2342-36](article-l2342-36.md)
- [Article L2342-37](article-l2342-37.md)
- [Article L2342-38](article-l2342-38.md)
- [Article L2342-39](article-l2342-39.md)
