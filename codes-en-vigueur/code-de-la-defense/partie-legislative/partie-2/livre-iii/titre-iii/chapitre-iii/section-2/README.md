# Section 2 : Modalités du contrôle

- [Sous-section 1 : Principes généraux](sous-section-1)
- [Sous-section 2 : Commissaires du Gouvernement](sous-section-2)
- [Sous-section 3 : Obligations des entreprises assujetties](sous-section-3)
