# Article L2339-14

Les infractions définies au premier alinéa du I de l'article L. 2339-2, à l'article L. 2339-4 et au premier alinéa de l'article L. 2339-10 du présent code, ainsi qu'au premier alinéa des articles L. 317-4 et L. 317-7 et au 1° de l'article L. 317-8 du code de la sécurité intérieure, sont punies de quinze ans de réclusion criminelle et un million et demi d'euros d'amende lorsqu'elles concernent des missiles, fusées ou autres systèmes sans pilote capables de conduire à leur cible des armes nucléaires telles que définies au III de l'article L. 1333-13-4, chimiques ou biologiques et spécialement conçus à cet usage.

Ces faits sont punis de vingt ans de réclusion criminelle et de trois millions d'euros d'amende lorsqu'ils sont commis en bande organisée.
