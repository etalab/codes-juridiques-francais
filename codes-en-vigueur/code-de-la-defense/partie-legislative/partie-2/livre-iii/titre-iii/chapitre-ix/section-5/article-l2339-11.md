# Article L2339-11

Est puni d'un emprisonnement de deux ans et d'une amende de 30 000 € l'usage, par une personne non qualifiée, du poinçon mentionné à l'article L. 2332-8-1.

Les contrefaçons d'un poinçon d'épreuve et l'usage frauduleux des poinçons contrefaits sont punis d'un emprisonnement de cinq ans et d'une amende de 75 000 €.
