# Sous-section 3 : Obligations des fournisseurs et des destinataires

- [Article L2335-13](article-l2335-13.md)
- [Article L2335-14](article-l2335-14.md)
- [Article L2335-15](article-l2335-15.md)
