# Article L2335-18

I. ― Est soumis à une autorisation préalable le transfert effectué depuis la France vers les autres Etats membres de l'Union européenne des matériels suivants :

1° Les satellites de détection ou d'observation, leurs équipements d'observation et de prises de vue ainsi que leurs stations au sol d'exploitation, conçus ou modifiés pour un usage militaire ou auxquels leurs caractéristiques confèrent des capacités militaires ;

2° Les véhicules spatiaux, les autres satellites, leurs stations au sol d'exploitation, leurs équipements spécialement conçus ou modifiés pour un usage militaire ;

3° Les moteurs et systèmes de propulsion spécialement conçus ou modifiés pour les matériels mentionnés aux 1° et 2° ;

4° Les fusées et les lanceurs spatiaux à capacité balistique militaire, leurs équipements et composants ainsi que les moyens spécialisés de production, d'essai et de lancement ;

5° Les parties, composants, accessoires et matériels spécifiques d'environnement, y compris les équipements de maintenance, des matériels mentionnés aux 1° à 3° ;

6° Les outillages spécialisés de fabrication des matériels mentionnés aux 1° à 4°.

L'autorisation est refusée lorsque le transfert est de nature à compromettre les intérêts essentiels de la sécurité.

II. ― Les articles L. 2335-12 à L. 2335-15 sont applicables aux transferts régis par le I du présent article.

III. ― Un décret en Conseil d'Etat détermine les conditions et la procédure de délivrance de cette autorisation ainsi que les éventuelles dérogations à cette obligation d'autorisation.
