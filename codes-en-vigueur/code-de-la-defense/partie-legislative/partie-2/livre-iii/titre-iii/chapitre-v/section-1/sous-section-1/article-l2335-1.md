# Article L2335-1

I.-L'importation sans autorisation préalable des matériels des catégories A, B ainsi que des matériels des catégories C et D figurant sur une liste fixée par un décret en Conseil d'Etat mentionnés à l'article L. 2331-1         provenant des Etats non membres de l'Union européenne ainsi que des territoires exclus du territoire douanier de l'Union européenne est prohibée.

L'autorité administrative détermine les conditions dans lesquelles il peut être dérogé à cette prohibition et les conditions dans lesquelles une autorisation d'importation peut être délivrée.

II.-Aucun des matériels de catégories A ou B mentionnés au même article L. 2331-1 dont l'importation en France est prohibée ne peut figurer dans une vente publique à moins d'avoir été au préalable rendu impropre à son usage normal.

III.-Aucun importateur des matériels appartenant aux catégories A et B mentionnées audit article L. 2331-1 ne peut obtenir une autorisation d'importation s'il n'est pas déjà titulaire de l'autorisation prévue au I de l'article L. 2332-1.

Les personnes non titulaires de cette autorisation peuvent, à titre exceptionnel, demander à bénéficier d'une autorisation d'importation des matériels des catégories A et B dans des conditions définies par décret en Conseil d'Etat.

IV.-L'autorité administrative peut à tout moment, dans les conditions fixées par décret en Conseil d'Etat, suspendre, modifier, abroger ou retirer les autorisations d'importation qu'elle a délivrées, pour des raisons de respect des engagements internationaux de la France, de protection des intérêts essentiels de sécurité, d'ordre public ou de sécurité publique, ou pour non-respect des conditions spécifiées dans l'autorisation.
