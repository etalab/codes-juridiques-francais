# Chapitre III : Réquisitions de biens et services

- [Article L2213-1](article-l2213-1.md)
- [Article L2213-2](article-l2213-2.md)
- [Article L2213-3](article-l2213-3.md)
- [Article L2213-4](article-l2213-4.md)
- [Article L2213-5](article-l2213-5.md)
- [Article L2213-6](article-l2213-6.md)
- [Article L2213-7](article-l2213-7.md)
- [Article L2213-8](article-l2213-8.md)
