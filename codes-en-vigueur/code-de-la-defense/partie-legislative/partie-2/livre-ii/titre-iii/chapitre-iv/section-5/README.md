# Section 5 : Procédure de règlement des indemnités

- [Article L2234-20](article-l2234-20.md)
- [Article L2234-21](article-l2234-21.md)
- [Article L2234-22](article-l2234-22.md)
- [Article L2234-23](article-l2234-23.md)
- [Article L2234-24](article-l2234-24.md)
- [Article L2234-25](article-l2234-25.md)
