# Article L2471-2

Pour l'application de la présente partie du code aux Terres australes et antarctiques françaises, les termes énumérés ci-après sont remplacés ainsi :

1° Le mot : " préfet " par les mots : " représentant de l'Etat " ;

2° Le mot : " département " par les mots : " Terres australes et antarctiques françaises ".
