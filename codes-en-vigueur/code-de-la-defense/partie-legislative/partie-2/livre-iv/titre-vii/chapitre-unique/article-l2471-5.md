# Article L2471-5

En l'absence d'adaptation, les références faites, par des dispositions de la présente partie du code applicables aux Terres australes et antarctiques françaises, à des dispositions qui n'y sont pas applicables sont remplacées par les références aux dispositions ayant le même objet applicables localement.
