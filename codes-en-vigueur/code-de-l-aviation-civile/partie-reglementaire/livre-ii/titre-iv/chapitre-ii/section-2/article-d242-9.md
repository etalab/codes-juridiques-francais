# Article D242-9

Par dérogation à l'article D. 242-7, le représentant de l'Etat territorialement compétent peut autoriser, dans les mêmes zones, et pour une durée limitée qu'il précise, des constructions ou installations nécessaires à la conduite de travaux sous réserve qu'une étude technique approuvée par le ministre chargé de l'aviation civile et, le cas échéant, le ministre de la défense démontre que la sécurité de l'exploitation des aéronefs n'est pas compromise.
