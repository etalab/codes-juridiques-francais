# CHAPITRE III : POLICE DES AERODROMES ET DES INSTALLATIONS A USAGE AERONAUTIQUE.

- [Section 1 : Service de sauvetage et de lutte contre l'incendie des aéronefs](section-1)
- [Section 2 : Prévention du péril animalier](section-2)
- [Section 3 : Sûreté de l'aviation civile](section-3)
- [Section 4 : Dispositions financières](section-4)
