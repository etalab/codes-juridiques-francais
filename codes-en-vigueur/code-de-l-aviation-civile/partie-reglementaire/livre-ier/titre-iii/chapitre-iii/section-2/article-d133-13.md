# Article D133-13

A tout moment les appareils, pellicules, films et supports divers et leurs reproductions peuvent être examinés, à titre de contrôle, par les services de police, qu'il s'agisse des titulaires d'autorisations ou des opérateurs occasionnels.

Les objets contrôlés sont restitués dans un délai qui ne saurait excéder quinze jours. En cas de détérioration des supports, les propriétaires ne sont pas fondés à réclamer une indemnité.

Les supports utilisés en violation des dispositions de l'article D. 133-10 ne sont pas restitués.
