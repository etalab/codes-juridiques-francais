# Article D424-4

Les membres du conseil médical sont convoqués individuellement à chaque séance par le président.

En cas d'absence ou d'empêchement, le président est remplacé par le vice-président.

Le conseil ne peut valablement délibérer que si cinq au moins de ses membres à voix délibérative sont présents, compte tenu des cas d'incompatibilité prévus à l'alinéa ci-après.

Les membres du conseil exercent leurs fonctions en toute indépendance. Lorsque le conseil délibère dans le cadre des recours visés au 5 de l'article D. 424-2, ils ne peuvent prendre part aux délibérations et aux votes portant sur un recours contre une décision dont ils ont déjà eu à connaître à l'occasion de leur activité extérieure au conseil.

Les délibérations ont lieu à huis clos. Les décisions et avis sont prononcés à la majorité des voix. En cas de partage égal des voix, celle du président est prépondérante.
