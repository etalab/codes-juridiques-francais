# Article D424-2

Le conseil médical de l'aéronautique civile :

1. Etudie et coordonne toutes les questions d'ordre physiologique, médical, médico-social et d'hygiène intéressant l'aéronautique civile, notamment en ce qui concerne le personnel navigant, les passagers et, d'une façon générale, le contrôle sanitaire. Il assure en cette matière la liaison avec les organismes similaires étrangers.

2. Se prononce sur le caractère définitif des inaptitudes déclarées lors des renouvellements d'aptitude par les différents centres d'expertise de médecine aéronautique à l'égard :

-des personnels navigants titulaires d'un titre aéronautique ;

-des candidats à l'obtention d'un de ces titres et détenteurs d'une carte de stagiaire.

3. Prend les décisions prévues aux articles L. 424-1,
L. 424-2,
L. 424-5 et par l'article R. 426-17 en matière de reconnaissance d'imputabilité au service aérien d'une maladie ayant entraîné une incapacité temporaire ou permanente de travail ou le décès.

4. Prend les décisions prévues aux articles L. 424-1, L. 424-2, L. 424-5 et par l'article R. 426-17 en matière de reconnaissance d'imputabilité au service aérien d'un accident aérien survenu en service ayant entraîné une incapacité temporaire ou permanente de travail ou le décès.

5. Se prononce sur :

a) Les recours interjetés par les candidats à la qualité de personnel navigant professionnel et non professionnel et par les personnels navigants professionnels et non professionnels déclarés médicalement inaptes au titre de l'aéronautique civile par un centre d'expertise de médecine aéronautique ou par un médecin examinateur ;

b) Les recours interjetés par les employeurs contre les décisions prononcées par les centres d'expertise de médecine aéronautique en matière d'aptitude à une fonction du personnel navigant professionnel ;

c) Les recours interjetés par le ministre chargé de l'aviation civile contre les décisions prononcées par les centres d'expertise de médecine aéronautique et les médecins examinateurs en matière d'aptitude à une fonction de personnel navigant.

Les recours mentionnés aux a, b et c ci-dessus sont exercés dans un délai de deux mois suivant la date de la décision d'aptitude ou d'inaptitude.

6. Se prononce sur les demandes visant à obtenir une dérogation aux conditions d'aptitude médicale prévues par les règlements en vigueur présentées par les candidats à la qualité de personnel navigant professionnel et non professionnel et par les personnels navigants professionnels et non professionnels déclarés médicalement inaptes par un centre d'expertise de médecine aéronautique ou un médecin examinateur.

Toutefois, en cas de légère déficience par rapport à une norme médicale restant compatible avec la sécurité aérienne, le médecin-chef d'un centre d'expertise de médecine aéronautique ou le médecin examinateur peut, pour une durée maximale de quarante-cinq jours, déclarer les personnes visées à l'alinéa précédent aptes à exercer leurs fonctions jusqu'à la décision du conseil médical de l'aéronautique civile.

7. Se prononce sur les affaires soumises par des médecins-chefs des centres d'expertise de médecine aéronautique et par des médecins examinateurs qui, en présence d'un cas litigieux ou non prévu par les règlements d'aptitude physique et mentale en vigueur, estiment devoir prendre l'avis du conseil médical de l'aéronautique civile avant de formuler une décision d'aptitude ou d'inaptitude à une fonction du personnel navigant de l'aéronautique civile.
