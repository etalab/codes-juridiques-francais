# Article D424-2-1

Les recours interjetés en vertu du b du 5 de l'article D. 424-2 font l'objet d'un examen préalable par une commission nommée par le ministre chargé de l'aviation civile et composée :

- d'une personnalité qualifiée, choisie en raison de ses compétences dans le domaine de l'aviation civile, nommée président par le ministre chargé de l'aviation civile pour une durée de trois ans renouvelable ;

- de deux personnes désignées par le ministre chargé de l'aviation civile pour une même période, l'une sur proposition des exploitants du transport aérien, l'autre sur proposition des organisations représentatives au niveau national du personnel navigant professionnel de l'aviation civile. Chacune d'entre elles dispose d'un suppléant désigné dans les mêmes conditions ;

- de deux membres docteurs en médecine, désignés par le ministre chargé de l'aviation civile pour chaque affaire, l'un sur proposition de l'employeur, l'autre sur proposition du navigant concerné.

La commission doit s'assurer que la procédure prévue au b du 5 de l'article D. 424-2 n'est pas utilisée à des fins autres que la sécurité des vols. Ses membres siègent en toute indépendance et ses délibérations demeurent secrètes.
