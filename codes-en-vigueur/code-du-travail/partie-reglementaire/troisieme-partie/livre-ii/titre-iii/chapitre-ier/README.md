# Chapitre Ier : Salaire minimum  interprofessionnel de croissance

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Modalités de fixation](section-2)
- [Section 3 : Minimum garanti](section-3)
