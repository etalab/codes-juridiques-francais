# Chapitre II : Saisies et cessions

- [Section 1 : Dispositions communes](section-1)
- [Section 2 : Saisie des sommes dues à titre de rémunération](section-2)
- [Section 3 : Cession des sommes dues à titre de rémunération](section-3)
