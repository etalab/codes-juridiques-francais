# Section 3 : Cession des sommes dues à titre de rémunération

- [Article R3252-45](article-r3252-45.md)
- [Article R3252-46](article-r3252-46.md)
- [Article R3252-47](article-r3252-47.md)
- [Article R3252-48](article-r3252-48.md)
- [Article R3252-49](article-r3252-49.md)
