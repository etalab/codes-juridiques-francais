# Sous-section 3 : Effets de la saisie

- [Article R3252-27](article-r3252-27.md)
- [Article R3252-28](article-r3252-28.md)
- [Article R3252-29](article-r3252-29.md)
