# Section 2 : Formation économique,  financière et juridique des représentants des salariés.

- [Article D3341-3](article-d3341-3.md)
- [Article D3341-4](article-d3341-4.md)
