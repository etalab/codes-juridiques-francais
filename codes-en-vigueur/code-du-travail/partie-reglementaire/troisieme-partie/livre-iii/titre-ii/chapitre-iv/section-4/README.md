# Section 4 : Disponibilité des droits des bénéficiaires.

- [Article D3324-21-2](article-d3324-21-2.md)
- [Article R3324-21-1](article-r3324-21-1.md)
- [Article R3324-22](article-r3324-22.md)
- [Article R3324-23](article-r3324-23.md)
- [Article R3324-24](article-r3324-24.md)
