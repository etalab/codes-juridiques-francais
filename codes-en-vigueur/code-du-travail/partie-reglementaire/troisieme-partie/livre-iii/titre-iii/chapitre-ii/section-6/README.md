# Section 6 : Indisponibilité des sommes, déblocage anticipé et liquidation.

- [Article R3332-28](article-r3332-28.md)
- [Article R3332-29](article-r3332-29.md)
- [Article R3332-30](article-r3332-30.md)
