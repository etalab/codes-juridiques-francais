# Article D3141-6

L'ordre des départs en congé est communiqué à chaque salarié un mois avant son départ, et affiché dans les locaux normalement accessibles aux salariés.
