# TITRE IV : CONGÉS PAYÉS ET AUTRES CONGÉS

- [Chapitre Ier : Congés payés](chapitre-ier)
- [Chapitre II : Autres congés](chapitre-ii)
- [Chapitre III : Dispositions pénales](chapitre-iii)
