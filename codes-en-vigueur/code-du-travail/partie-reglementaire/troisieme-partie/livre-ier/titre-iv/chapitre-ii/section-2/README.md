# Section 2 : Congés non rémunérés

- [Sous-section 1 : Congé de solidarité familiale](sous-section-1)
- [Sous-section 2 : Congé de soutien familial](sous-section-2)
- [Sous-section 3 : Congé de solidarité internationale](sous-section-3)
- [Sous-section 4 : Congés de formation de cadres et d'animateurs pour la jeunesse](sous-section-4)
- [Sous-section 5 : Congé mutualiste de formation](sous-section-5)
- [Sous-section 6 : Congé de représentation](sous-section-6)
- [Sous-section 7 : Congés des salariés candidats  ou élus à un mandat parlementaire ou local](sous-section-7)
- [Sous-section 8 : Réserve opérationnelle et service national](sous-section-8)
- [Sous-section 9 : Congé et période de travail à temps partiel  pour la création ou la reprise d'entreprise et congé sabbatique](sous-section-9)
