# Sous-section 1 : Suspension et report du repos hebdomadaire

- [Paragraphe 1 : Industries traitant des matières périssables  ou ayant à répondre à un surcroît extraordinaire de travail](paragraphe-1)
- [Paragraphe 2 : Travaux dans les ports, débarcadères et stations](paragraphe-2)
- [Paragraphe 3 : Activités saisonnières](paragraphe-3)
