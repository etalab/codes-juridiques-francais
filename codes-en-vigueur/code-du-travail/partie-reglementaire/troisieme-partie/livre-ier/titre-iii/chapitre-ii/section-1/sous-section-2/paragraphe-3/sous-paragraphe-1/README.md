# Sous-paragraphe 1 : Dérogations accordées par le préfet

- [Article R3132-16](article-r3132-16.md)
- [Article R3132-17](article-r3132-17.md)
- [Article R3132-19](article-r3132-19.md)
- [Article R3132-20](article-r3132-20.md)
