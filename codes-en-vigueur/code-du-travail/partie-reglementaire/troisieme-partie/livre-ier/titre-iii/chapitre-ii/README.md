# Chapitre II : Repos hebdomadaire

- [Section 1 : Dérogations](section-1)
- [Section 2 : Décisions de fermeture](section-2)
- [Section 3 : Procédure de référé de l'inspecteur du travail](section-3)
