# Section 1 : Travail à temps partiel

- [Sous-section 1 : Mise en œuvre à l'initiative de l'employeur](sous-section-1)
- [Sous-section 2 : Mise en œuvre à la demande du salarié](sous-section-2)
