# Paragraphe 3 : Dérogation à la durée hebdomadaire maximale moyenne

- [Article R3121-24](article-r3121-24.md)
- [Article R3121-25](article-r3121-25.md)
- [Article R3121-26](article-r3121-26.md)
- [Article R3121-27](article-r3121-27.md)
- [Article R3121-28](article-r3121-28.md)
