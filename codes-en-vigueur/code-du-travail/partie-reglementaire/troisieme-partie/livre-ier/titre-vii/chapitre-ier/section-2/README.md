# Section 2 : Documents fournis à l'inspecteur du travail

- [Article D3171-16](article-d3171-16.md)
- [Article D3171-17](article-d3171-17.md)
