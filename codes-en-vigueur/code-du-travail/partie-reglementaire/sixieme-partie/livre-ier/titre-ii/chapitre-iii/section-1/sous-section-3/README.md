# Sous-section 3 : Organisation et fonctionnement

- [Article R6123-2](article-r6123-2.md)
- [Article R6123-2-1](article-r6123-2-1.md)
- [Article R6123-2-2](article-r6123-2-2.md)
- [Article R6123-2-3](article-r6123-2-3.md)
- [Article R6123-2-4](article-r6123-2-4.md)
- [Article R6123-2-5](article-r6123-2-5.md)
- [Article R6123-2-6](article-r6123-2-6.md)
