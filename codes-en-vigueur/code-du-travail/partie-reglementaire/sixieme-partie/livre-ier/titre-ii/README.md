# TITRE II : RÔLE DES RÉGIONS, DE L'ÉTAT ET DES INSTITUTIONS  DE LA FORMATION PROFESSIONNELLE

- [Chapitre Ier : Rôle des régions](chapitre-ier)
- [Chapitre II : Rôle de l'État](chapitre-ii)
- [Chapitre III : Coordination des politiques de l'emploi, de l'orientation et de la formation professionnelles](chapitre-iii)
