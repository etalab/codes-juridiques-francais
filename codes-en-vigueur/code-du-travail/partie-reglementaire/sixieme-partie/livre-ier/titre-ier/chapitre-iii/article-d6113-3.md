# Article D6113-3

<div align="left">Le socle de connaissances et de compétences professionnelles mentionné à l'article D. 6113-1 fait l'objet, sur proposition du Comité paritaire interprofessionnel national pour l'emploi et la formation, d'une certification. <br/>
<br/>Cette certification s'appuie sur un référentiel qui précise les connaissances et les compétences mentionnées à l'article D. 6113-2 et sur un référentiel de certification qui détermine les conditions d'évaluation des acquis. <br/>
<br/>Le référentiel de certification prévoit les principes directeurs permettant une mise en perspective du socle de connaissances et compétences pour prendre en compte les spécificités des différents secteurs d'activité professionnelle. <br/>
<br/>Le Comité paritaire interprofessionnel national pour l'emploi et la formation définit les modalités de délivrance de la certification. Dans ce cadre, il s'assure notamment que la délivrance de la certification s'effectue dans le respect : <br/>
<br/>1° De la transparence de l'information donnée au public ; <br/>
<br/>2° De la qualité du processus de certification. <br/>
<br/>Cette certification est recensée à l'inventaire prévu à l'article L. 335-6 du code de l'éducation, sous réserve de la transmission à la Commission nationale de la certification professionnelle des référentiels prévus au présent article.<br/>
<br/>
<br/>
</div>
