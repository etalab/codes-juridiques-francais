# Article D6113-4

<div align="left">Les modules complémentaires mentionnés au II de l'article D. 6113-2 sont définis par arrêté du ministre en charge de la formation professionnelle sur proposition de l'Association des régions de France.<br/>
<br/>
<br/>
</div>
