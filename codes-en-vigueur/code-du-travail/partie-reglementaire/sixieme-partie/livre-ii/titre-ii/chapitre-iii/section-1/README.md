# Section 1 : Organisation de l'apprentissage

- [Sous-section 1 : Déclaration de l'employeur](sous-section-1)
- [Sous-section 2 : Nombre maximal d'apprentis](sous-section-2)
- [Sous-section 3 : Obligations envers les représentants de l'apprenti](sous-section-3)
- [Sous-section 4 : Conventionnement avec une entreprise d'accueil](sous-section-4)
- [Sous-section 5 : Conventionnement avec une entreprise d'un autre Etat membre  de la Communauté européenne](sous-section-5)
