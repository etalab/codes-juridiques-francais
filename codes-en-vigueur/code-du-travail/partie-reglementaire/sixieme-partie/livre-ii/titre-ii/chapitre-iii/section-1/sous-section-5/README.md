# Sous-section 5 : Conventionnement avec une entreprise d'un autre Etat membre  de la Communauté européenne

- [Article R6223-17](article-r6223-17.md)
- [Article R6223-18](article-r6223-18.md)
- [Article R6223-19](article-r6223-19.md)
- [Article R6223-20](article-r6223-20.md)
- [Article R6223-21](article-r6223-21.md)
