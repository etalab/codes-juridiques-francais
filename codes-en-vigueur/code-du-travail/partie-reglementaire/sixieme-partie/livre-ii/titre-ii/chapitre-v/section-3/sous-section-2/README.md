# Sous-section 2 : Interdiction de recrutement de nouveaux apprentis

- [Article R6225-10](article-r6225-10.md)
- [Article R6225-11](article-r6225-11.md)
- [Article R6225-12](article-r6225-12.md)
