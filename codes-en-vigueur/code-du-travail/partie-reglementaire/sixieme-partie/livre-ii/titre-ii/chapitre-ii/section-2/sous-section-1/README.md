# Sous-section 1 : Durée du travail

- [Article R6222-24](article-r6222-24.md)
- [Article R6222-25](article-r6222-25.md)
