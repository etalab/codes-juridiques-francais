# Section 4 : Carte d'étudiant des métiers

- [Article D6222-42](article-d6222-42.md)
- [Article D6222-43](article-d6222-43.md)
- [Article D6222-44](article-d6222-44.md)
