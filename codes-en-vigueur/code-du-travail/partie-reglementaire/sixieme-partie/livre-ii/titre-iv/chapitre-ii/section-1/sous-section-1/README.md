# Sous-section 1 : Principes

- [Article R6242-1](article-r6242-1.md)
- [Article R6242-2](article-r6242-2.md)
- [Article R6242-3](article-r6242-3.md)
