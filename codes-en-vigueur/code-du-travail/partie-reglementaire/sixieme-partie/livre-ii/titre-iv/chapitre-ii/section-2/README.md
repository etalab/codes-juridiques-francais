# Section 2 : Dispositions financières

- [Article R6242-12](article-r6242-12.md)
- [Article R6242-13](article-r6242-13.md)
- [Article R6242-14](article-r6242-14.md)
- [Article R6242-15](article-r6242-15.md)
- [Article R6242-15-1](article-r6242-15-1.md)
- [Article R6242-16](article-r6242-16.md)
