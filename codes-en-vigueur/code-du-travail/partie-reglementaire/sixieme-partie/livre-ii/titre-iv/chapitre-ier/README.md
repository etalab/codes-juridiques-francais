# Chapitre Ier : Taxe d'apprentissage

- [Section 1 : Principes](section-1)
- [Section 3 : Versements libératoires](section-3)
- [Section 4 : Affectation des fonds](section-4)
