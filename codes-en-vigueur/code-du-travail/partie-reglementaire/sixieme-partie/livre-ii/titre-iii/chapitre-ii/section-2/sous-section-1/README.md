# Sous-section 1 : Sections d'apprentissage

- [Article D6232-17](article-d6232-17.md)
- [Article R6232-18](article-r6232-18.md)
- [Article R6232-19](article-r6232-19.md)
- [Article R6232-20](article-r6232-20.md)
- [Article R6232-21](article-r6232-21.md)
