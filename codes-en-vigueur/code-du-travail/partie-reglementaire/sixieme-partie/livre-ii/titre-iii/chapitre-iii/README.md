# Chapitre III : Fonctionnement des centres de formation d'apprentis  et des sections d'apprentissage

- [Section 1 : Ressources](section-1)
- [Section 2 : Personnel](section-2)
- [Section 3 : Organisation](section-3)
- [Section 4 : Fonctionnement pédagogique des centres de formation d'apprentis et des sections d'apprentissage](section-4)
