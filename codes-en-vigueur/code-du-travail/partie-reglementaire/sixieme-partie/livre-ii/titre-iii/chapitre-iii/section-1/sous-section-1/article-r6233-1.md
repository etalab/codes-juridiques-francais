# Article R6233-1

La convention de création prévoit les conditions dans lesquelles est établi le budget du centre ou de la section d'apprentissage.
