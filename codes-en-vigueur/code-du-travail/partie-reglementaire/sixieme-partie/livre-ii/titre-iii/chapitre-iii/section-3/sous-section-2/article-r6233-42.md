# Article R6233-42

Le conseil de perfectionnement suit l'application des dispositions arrêtées dans les différents domaines mentionnés aux articles R. 6233-40 et R. 6233-41.
