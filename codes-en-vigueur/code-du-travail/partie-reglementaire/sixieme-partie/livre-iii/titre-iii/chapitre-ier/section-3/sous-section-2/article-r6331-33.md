# Article R6331-33

En cas de cession ou de cessation d'entreprise, la déclaration relative à l'année en cours et, le cas échéant, celle relative à l'année précédente, sont déposées dans les soixante jours de la cession ou de la cessation.

En cas de décès de l'employeur, ces déclarations sont déposées dans les six mois qui suivent la date du décès.

En cas de procédure de sauvegarde, de redressement ou de liquidation judiciaire, elles sont produites dans les soixante jours de la date du jugement.
