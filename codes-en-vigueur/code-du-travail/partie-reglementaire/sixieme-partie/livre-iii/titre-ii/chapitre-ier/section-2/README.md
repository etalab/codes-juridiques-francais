# Section 2 : Régimes applicables aux heures de formation  de développement des compétences

- [Article D6321-5](article-d6321-5.md)
- [Article D6321-6](article-d6321-6.md)
- [Article D6321-7](article-d6321-7.md)
- [Article D6321-8](article-d6321-8.md)
- [Article D6321-9](article-d6321-9.md)
- [Article D6321-10](article-d6321-10.md)
- [Article R6321-4](article-r6321-4.md)
