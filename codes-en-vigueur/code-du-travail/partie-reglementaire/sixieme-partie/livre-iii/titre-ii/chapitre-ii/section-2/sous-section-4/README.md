# Sous-section 4 : Conditions de prise en charge du congé  de bilan de compétences et rémunération

- [Paragraphe 1 : Conditions de prise en charge](paragraphe-1)
- [Paragraphe 2 : Rémunération](paragraphe-2)
