# Sous-section 1 : Congés d'enseignement ou de recherche

- [Paragraphe 1 : Condition d'ouverture](paragraphe-1)
- [Paragraphe 2 : Obligations du salarié](paragraphe-2)
