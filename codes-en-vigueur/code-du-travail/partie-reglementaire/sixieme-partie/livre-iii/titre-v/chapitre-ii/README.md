# Chapitre II : Fonctionnement

- [Section 1 : Règlement intérieur](section-1)
- [Section 2 : Droit disciplinaire](section-2)
- [Section 3 : Représentation des stagiaires](section-3)
- [Section 4 : Obligations comptables](section-4)
- [Section 5 : Bilan pédagogique et financier](section-5)
- [Section 6 : Centres de formation professionnelle](section-6)
