# Sous-section 1 : Election et scrutin

- [Article R6352-9](article-r6352-9.md)
- [Article R6352-10](article-r6352-10.md)
- [Article R6352-11](article-r6352-11.md)
- [Article R6352-12](article-r6352-12.md)
