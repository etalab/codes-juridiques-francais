# Section 6 : Centres de formation professionnelle

- [Sous-section 1 : Objet, organisation et fonctionnement](sous-section-1)
- [Sous-section 2 : Stagiaires](sous-section-2)
- [Sous-section 3 : Subventions](sous-section-3)
