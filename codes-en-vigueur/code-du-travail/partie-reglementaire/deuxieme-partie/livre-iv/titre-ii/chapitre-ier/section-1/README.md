# Section 1 : Procédure applicable en cas de licenciement

- [Sous-section 1 : Délégué syndical, salarié mandaté et conseiller du salarié](sous-section-1)
- [Sous-section 2 : Délégué du personnel, membre du comité d'entreprise  et membre du comité d'hygiène, de sécurité et des conditions de travail](sous-section-2)
