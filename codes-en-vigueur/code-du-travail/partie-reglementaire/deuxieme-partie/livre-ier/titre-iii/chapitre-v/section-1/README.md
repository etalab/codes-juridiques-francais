# Section 1 : Certification et publicité des comptes des organisations syndicales de salariés et professionnelles d'employeurs

- [Article D2135-1](article-d2135-1.md)
- [Article D2135-2](article-d2135-2.md)
- [Article D2135-3](article-d2135-3.md)
- [Article D2135-4](article-d2135-4.md)
- [Article D2135-5](article-d2135-5.md)
- [Article D2135-6](article-d2135-6.md)
- [Article D2135-7](article-d2135-7.md)
- [Article D2135-8](article-d2135-8.md)
- [Article D2135-9](article-d2135-9.md)
- [Article D2135-34](article-d2135-34.md)
