# Paragraphe 1er : Composition du conseil d'administration de l'association paritaire

- [Article R2135-10](article-r2135-10.md)
- [Article R2135-11](article-r2135-11.md)
- [Article R2135-12](article-r2135-12.md)
- [Article R2135-13](article-r2135-13.md)
