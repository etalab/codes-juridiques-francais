# Sous-section 6 : Modalités de vote

- [Paragraphe 1er : Dispositions communes](paragraphe-1er)
- [Paragraphe 2 : Bureau de vote](paragraphe-2)
- [Paragraphe 3 : Vote électronique à distance](paragraphe-3)
- [Paragraphe 4 : Vote par correspondance](paragraphe-4)
