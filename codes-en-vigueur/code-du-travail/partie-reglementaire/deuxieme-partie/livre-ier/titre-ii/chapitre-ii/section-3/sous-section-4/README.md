# Sous-section 4 : Candidatures des organisations syndicales de salariés

- [Article R2122-33](article-r2122-33.md)
- [Article R2122-34](article-r2122-34.md)
- [Article R2122-35](article-r2122-35.md)
- [Article R2122-36](article-r2122-36.md)
- [Article R2122-37](article-r2122-37.md)
- [Article R2122-38](article-r2122-38.md)
- [Article R2122-39](article-r2122-39.md)
- [Article R2122-40](article-r2122-40.md)
- [Article R2122-41](article-r2122-41.md)
- [Article R2122-42](article-r2122-42.md)
