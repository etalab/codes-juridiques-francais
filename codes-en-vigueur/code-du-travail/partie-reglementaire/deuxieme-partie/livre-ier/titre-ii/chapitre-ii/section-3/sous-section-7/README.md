# Sous-section 7  : Dépouillement

- [Paragraphe 1er : Dépouillement du vote électronique à distance](paragraphe-1er)
- [Paragraphe 2 : Dépouillement du vote par correspondance](paragraphe-2)
- [Paragraphe 3 : Centralisation et proclamation des résultats](paragraphe-3)
