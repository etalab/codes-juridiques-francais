# Chapitre Ier : Critères de représentativité

- [Article R2121-1](article-r2121-1.md)
- [Article R2121-2](article-r2121-2.md)
