# Chapitre III : Délégué syndical

- [Section 1 : Conditions de désignation](section-1)
- [Section 2 : Mandat](section-2)
