# Sous-section 1 : Entreprises de cinquante salariés et plus

- [Article R2143-1](article-r2143-1.md)
- [Article R2143-2](article-r2143-2.md)
- [Article R2143-3](article-r2143-3.md)
