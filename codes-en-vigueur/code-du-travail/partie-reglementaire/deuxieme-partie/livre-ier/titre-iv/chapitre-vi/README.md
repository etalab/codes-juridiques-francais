# Chapitre VI : Dispositions pénales

- [Article R2146-1](article-r2146-1.md)
- [Article R2146-2](article-r2146-2.md)
- [Article R2146-3](article-r2146-3.md)
- [Article R2146-4](article-r2146-4.md)
- [Article R2146-5](article-r2146-5.md)
