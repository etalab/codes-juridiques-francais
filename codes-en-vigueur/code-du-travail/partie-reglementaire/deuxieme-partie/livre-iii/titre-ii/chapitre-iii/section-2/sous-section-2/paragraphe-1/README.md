# Paragraphe 1 : Gestion par le comité d'entreprise

- [Article R2323-21](article-r2323-21.md)
- [Article R2323-22](article-r2323-22.md)
- [Article R2323-23](article-r2323-23.md)
- [Article R2323-24](article-r2323-24.md)
- [Article R2323-25](article-r2323-25.md)
- [Article R2323-26](article-r2323-26.md)
- [Article R2323-27](article-r2323-27.md)
