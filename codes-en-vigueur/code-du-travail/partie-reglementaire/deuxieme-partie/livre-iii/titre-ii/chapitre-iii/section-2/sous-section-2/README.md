# Sous-section 2 : Modalités de gestion

- [Paragraphe 1 : Gestion par le comité d'entreprise](paragraphe-1)
- [Paragraphe 2 : Gestion par le comité interentreprises](paragraphe-2)
