# Article R2323-12

Le rapport annuel prévu à l'article L. 2323-57 comporte des indicateurs permettant d'analyser la situation comparée des femmes et des hommes dans l'entreprise et son évolution ainsi que des indicateurs permettant d'analyser les conditions dans lesquelles s'articulent l'activité professionnelle et l'exercice de la responsabilité familiale des salariés. Ces indicateurs énumérés ci-dessous comprennent des données chiffrées permettant de mesurer les écarts et des données explicatives sur les évolutions constatées.

Ce rapport établit un plan d'action destiné à assurer l'égalité professionnelle entre les hommes et les femmes fondé sur des critères clairs, précis et opérationnels.

I. ― Indicateurs sur la situation comparée des femmes

et des hommes dans l'entreprise

<div align="center">

<table>
<tbody>
<tr>
<td align="left" valign="top">1° Conditions générales d'emploi. <br/>
</td>
<td align="left" valign="top">a) Effectifs : <br/>Données chiffrées par sexe : <br/>- Répartition par catégorie professionnelle selon les différents contrats de travail (CDI ou CDD) ; <br/>- Age moyen par catégorie professionnelle ; <br/>
</td>
</tr>
<tr>
<td align="left" valign="top">
<br/>
</td>
<td align="left" valign="top">b) Durée et organisation du travail : <br/>Données chiffrées par sexe : <br/>- Répartition des effectifs selon la durée du travail : temps complet, temps partiel (compris entre 20 et 30 heures et autres formes de temps partiel) ; <br/>- Répartition des effectifs selon l'organisation du travail : travail posté, travail de nuit, horaires variables, travail atypique dont travail durant le week-end ; <br/>
</td>
</tr>
<tr>
<td align="left" valign="top">
<br/>
</td>
<td align="left" valign="top">c) Données sur les congés : <br/>Données chiffrées par sexe : <br/>- Répartition par catégorie professionnelle ; <br/>- Selon le nombre et le type de congés dont la durée est supérieure à six mois : compte épargne-temps, congé parental, congé sabbatique ; <br/>
</td>
</tr>
<tr>
<td align="left" valign="top">
<br/>
</td>
<td align="left" valign="top">d) Données sur les embauches et les départs : <br/>Données chiffrées par sexe : <br/>- Répartition des embauches par catégorie professionnelle et type de contrat de travail ; <br/>- Répartition des départs par catégorie professionnelle et motifs : retraite, démission, fin de contrat de travail à durée déterminée, licenciement ; <br/>
</td>
</tr>
<tr>
<td align="left" valign="top">
<br/>
</td>
<td align="left" valign="top">e) Positionnement dans l'entreprise : <br/>Données chiffrées par sexe : <br/>- Répartition des effectifs par catégorie professionnelle ; <br/>
</td>
</tr>
<tr>
<td align="left" valign="top">
<br/>
</td>
<td align="left" valign="top">f) Promotion : <br/>Données chiffrées par sexe : <br/>- Nombre de promotions par catégorie professionnelle ; <br/>- Durée moyenne entre deux promotions. <br/>
</td>
</tr>
<tr>
<td align="left" valign="top"/>
<td align="left" valign="top">
<p align="left">g) Ancienneté :</p>
<p align="left">Données chiffrées par sexe : </p>
<p>- Ancienneté moyenne dans l'entreprise par catégorie professionnelle ; </p>
<p>- Ancienneté moyenne dans la catégorie professionnelle. </p>
</td>
</tr>
<tr>
<td align="left" valign="top">2° Rémunérations. <br/>
</td>
<td align="left" valign="top">Données chiffrées par sexe et par catégorie professionnelle : <br/>- Eventail des rémunérations ; <br/>- Rémunération moyenne ou médiane mensuelle ; <br/>- Nombre de femmes dans les dix plus hautes rémunérations. <br/>
</td>
</tr>
<tr>
<td align="left" valign="top">3° Formation. <br/>
</td>
<td align="left" valign="top">Données chiffrées par sexe : <p>Répartition par catégorie professionnelle selon : </p>
<p>- le nombre moyen d'heures d'actions de formation par salarié et par an ; </p>
<p>- la répartition par type d'action : adaptation au poste, maintien dans l'emploi, développement des compétences. </p>
</td>
</tr>
<tr>
<td align="left" valign="top">4° Conditions de travail. <br/>
</td>
<td align="left" valign="top">Données générales par sexe : <br/>Répartition par poste de travail selon : <br/>- L'exposition à des risques professionnels ; <br/>- La pénibilité, dont le caractère répétitif des tâches. <br/>
</td>
</tr>
</tbody>
</table>

</div>

II. ― Indicateurs relatifs à l'articulation entre l'activité

professionnelle et l'exercice de la responsabilité familiale

<div align="center">

<table>
<tbody>
<tr>
<td align="left" valign="top">1° Congés. <br/>
</td>
<td align="left" valign="top">a) Existence d'un complément de salaire versé par l'employeur pour le congé de paternité, le congé de maternité, le congé d'adoption ; <br/>
</td>
</tr>
<tr>
<td align="left" valign="top">
<br/>
</td>
<td align="left" valign="top">b) Données chiffrées par catégorie professionnelle : <br/>- Nombre de jours de congés de paternité pris par le salarié par rapport au nombre de jours de congés théoriques. <br/>
</td>
</tr>
<tr>
<td align="left" valign="top">2° Organisation du temps de travail dans l'entreprise. <br/>
</td>
<td align="left" valign="top">a) Existence de formules d'organisation du travail facilitant l'articulation de la vie familiale et de la vie professionnelle ; <br/>
</td>
</tr>
<tr>
<td align="left" valign="top">
<br/>
</td>
<td align="left" valign="top">b) Données chiffrées par sexe et par catégorie professionnelle : <br/>- Nombre de salariés ayant accédé au temps partiel choisi ; <br/>- Nombre de salariés à temps partiel choisi ayant repris un travail à temps plein. <br/>
</td>
</tr>
<tr>
<td align="left" valign="top">
<br/>
</td>
<td align="left" valign="top">c) Services de proximité : <br/>- Participation de l'entreprise et du comité d'entreprise aux modes d'accueil de la petite enfance ; <br/>- Evolution des dépenses éligibles au crédit d'impôt famille. <br/>
</td>
</tr>
</tbody>
</table>

</div>

Concernant la notion de catégorie professionnelle, il peut s'agir de fournir des données distinguant :

a) Les ouvriers, les employés, les cadres et les emplois intermédiaires ;

b) Ou les catégories d'emplois définies par la classification ;

c) Ou les métiers repères ;

d) Ou les emplois types.

Toutefois, l'indicateur relatif à la rémunération moyenne ou médiane mensuelle comprend au moins deux niveaux de comparaison dont celui mentionné au a ci-dessus.

III. - Plan d'action :

- mesures prises au cours de l'année écoulée en vue d'assurer l'égalité professionnelle. Bilan des actions de l'année écoulée et, le cas échéant, de l'année précédente lorsqu'un plan d'actions a été antérieurement mis en œuvre par l'entreprise par accord collectif ou de manière unilatérale. Evaluation du niveau de réalisation des objectifs sur la base des indicateurs retenus. Explications sur les actions prévues non réalisées ;

- objectifs de progression pour l'année à venir et indicateurs associés. Définition qualitative et quantitative des mesures permettant de les atteindre conformément à l'article R. 2242-2. Evaluation de leur coût. Echéancier des mesures prévues.
