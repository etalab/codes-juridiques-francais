# Sous-section 4 : Information et consultation en matière  de formation professionnelle et d'apprentissage

- [Paragraphe 1 : Orientation de la formation professionnelle](paragraphe-1)
- [Paragraphe 2 : Plan de formation](paragraphe-2)
