# Chapitre V : Fonctionnement

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Réunions](section-2)
- [Section 3 : Commissions](section-3)
- [Section 4 : Recours à un expert](section-4)
- [Section 5 : Formation des membres du comité d'entreprise](section-5)
- [Section 6 : Etablissement et contrôle des comptes du comité d'entreprise](section-6)
