# Article R2325-18

Le secrétaire du comité d'entreprise répond par tout moyen propre à donner date certaine à la réception de sa réponse dans les trente jours qui suivent la réception de l'information mentionnée à l'article R. 2325-17. Il donne une analyse de la situation et précise, le cas échéant, les mesures envisagées.
