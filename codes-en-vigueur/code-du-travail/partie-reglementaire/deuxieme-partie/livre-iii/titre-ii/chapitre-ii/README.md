# Chapitre II : Conditions de mise en place  et de suppression

- [Article R2322-1](article-r2322-1.md)
- [Article R2322-2](article-r2322-2.md)
