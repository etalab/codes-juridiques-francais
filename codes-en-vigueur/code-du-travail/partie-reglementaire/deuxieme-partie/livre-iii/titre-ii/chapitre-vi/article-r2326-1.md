# Article R2326-1

Les dispositions relatives au nombre de délégués constituant la délégation unique du personnel sont prévues par l'article R. 2314-3.
