# Sous-section 3 : Mode de scrutin et résultat des élections

- [Paragraphe 1 : Vote électronique](paragraphe-1)
- [Paragraphe 2 : Attribution des sièges](paragraphe-2)
- [Paragraphe 3 : Résultat](paragraphe-3)
