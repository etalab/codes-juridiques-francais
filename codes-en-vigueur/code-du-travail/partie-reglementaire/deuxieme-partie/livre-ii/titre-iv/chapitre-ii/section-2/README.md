# Section 2 : Egalité professionnelle entre les femmes et les hommes

- [Article R2242-2](article-r2242-2.md)
- [Article R2242-3](article-r2242-3.md)
- [Article R2242-4](article-r2242-4.md)
- [Article R2242-5](article-r2242-5.md)
- [Article R2242-6](article-r2242-6.md)
- [Article R2242-7](article-r2242-7.md)
- [Article R2242-8](article-r2242-8.md)
