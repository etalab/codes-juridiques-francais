# Sous-section 1 : Dispositions communes

- [Article D2232-2](article-d2232-2.md)
- [Article D2232-3](article-d2232-3.md)
- [Article D2232-4](article-d2232-4.md)
- [Article R2232-5](article-r2232-5.md)
