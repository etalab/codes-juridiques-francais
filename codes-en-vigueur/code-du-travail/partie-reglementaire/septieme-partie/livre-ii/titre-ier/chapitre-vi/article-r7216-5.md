# Article R7216-5

Le fait de méconnaître les dispositions des articles R. 7214-9 et R. 7214-10, est puni de l'amende prévue pour les contraventions de la cinquième classe.
