# Chapitre III : Mise en œuvre

- [Section 1 : Comptabilité](section-1)
- [Section 2 : Rupture du contrat de travail](section-2)
- [Section 3 : Dispositions pénales](section-3)
