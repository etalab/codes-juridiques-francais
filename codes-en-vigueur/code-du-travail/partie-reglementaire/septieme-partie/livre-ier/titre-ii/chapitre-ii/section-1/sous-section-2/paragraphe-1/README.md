# Paragraphe 1 : Entrepreneur de spectacles vivants établi en France

- [Article R7122-2](article-r7122-2.md)
- [Article R7122-3](article-r7122-3.md)
- [Article R7122-4](article-r7122-4.md)
- [Article R7122-5](article-r7122-5.md)
