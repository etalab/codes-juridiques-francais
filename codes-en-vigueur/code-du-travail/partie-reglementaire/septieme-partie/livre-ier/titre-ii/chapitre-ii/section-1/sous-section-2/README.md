# Sous-section 2 : Licence d'entrepreneur de spectacles vivants

- [Paragraphe 1 : Entrepreneur de spectacles vivants établi en France](paragraphe-1)
- [Paragraphe 2 : Entrepreneur de spectacles vivants non établi en France](paragraphe-2)
- [Paragraphe 3 : Dispositions communes à l'instruction des licences](paragraphe-3)
