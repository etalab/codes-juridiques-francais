# Section 2 : Congés payés

- [Sous-section 1 : Champ d'application](sous-section-1)
- [Sous-section 2 : Droit au congé](sous-section-2)
- [Sous-section 3 : Caisse de congés payés](sous-section-3)
