# Sous-section 2 : Droit au congé

- [Article D7121-30](article-d7121-30.md)
- [Article D7121-31](article-d7121-31.md)
- [Article D7121-32](article-d7121-32.md)
- [Article D7121-33](article-d7121-33.md)
- [Article D7121-34](article-d7121-34.md)
- [Article D7121-35](article-d7121-35.md)
- [Article D7121-36](article-d7121-36.md)
- [Article D7121-37](article-d7121-37.md)
