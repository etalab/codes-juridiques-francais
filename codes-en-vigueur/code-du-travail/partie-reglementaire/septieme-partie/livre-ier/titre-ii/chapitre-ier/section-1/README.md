# Section 1 : Agents artistiques

- [Sous-section 1 : Dispositions générales](sous-section-1)
- [Sous-section 2 : Le mandat](sous-section-2)
- [Sous-Section 3 : Rémunérations](sous-section-3)
