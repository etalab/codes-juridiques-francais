# Section 7 : Inscription sur la liste des demandeurs d'emploi

- [Article R5221-47](article-r5221-47.md)
- [Article R5221-48](article-r5221-48.md)
- [Article R5221-49](article-r5221-49.md)
- [Article R5221-50](article-r5221-50.md)
