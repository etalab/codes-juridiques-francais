# Article R5221-49

Afin de procéder à la vérification prévue à l'article L. 5411-4,                Pôle emploi adresse une copie du titre de séjour du travailleur étranger qui sollicite son inscription sur la liste des demandeurs d'emploi à la préfecture qui l'a délivré. A la demande du préfet, il peut être exigé la production par le travailleur étranger du document original.

Cette démarche est accomplie par lettre recommandée avec avis réception ou par courrier électronique.
