# Sous-section 1 : Procédure de renouvellement

- [Article R5221-32](article-r5221-32.md)
- [Article R5221-33](article-r5221-33.md)
- [Article R5221-34](article-r5221-34.md)
- [Article R5221-35](article-r5221-35.md)
- [Article R5221-36](article-r5221-36.md)
