# Sous-section 3 : Salariés en mission

- [Article R5221-30](article-r5221-30.md)
- [Article R5221-31](article-r5221-31.md)
