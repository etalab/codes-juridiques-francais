# Section 3 : Délivrance des autorisations de travail

- [Article R5221-17](article-r5221-17.md)
- [Article R5221-18](article-r5221-18.md)
- [Article R5221-19](article-r5221-19.md)
- [Article R5221-20](article-r5221-20.md)
- [Article R5221-21](article-r5221-21.md)
- [Article R5221-22](article-r5221-22.md)
