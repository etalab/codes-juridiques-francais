# Article R5223-31

Le directeur général de l'Office ou son représentant assiste aux séances du comité consultatif avec voix consultative.
