# Section 3 : Ressources

- [Article R5223-35](article-r5223-35.md)
- [Article R5223-37](article-r5223-37.md)
- [Article R5223-38](article-r5223-38.md)
- [Article R5223-39](article-r5223-39.md)
