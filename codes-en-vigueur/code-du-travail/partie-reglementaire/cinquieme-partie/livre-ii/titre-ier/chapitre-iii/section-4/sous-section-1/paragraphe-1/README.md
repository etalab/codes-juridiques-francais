# Paragraphe 1 : Aide pour l'adaptation du lieu de travail

- [Article R5213-32](article-r5213-32.md)
- [Article R5213-33](article-r5213-33.md)
- [Article R5213-34](article-r5213-34.md)
- [Article R5213-35](article-r5213-35.md)
