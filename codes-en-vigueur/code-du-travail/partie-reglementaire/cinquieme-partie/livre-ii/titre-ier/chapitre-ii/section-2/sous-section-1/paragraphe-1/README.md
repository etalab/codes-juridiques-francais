# Paragraphe 1 : Mise en œuvre par la passation de contrats

- [Article D5212-5-1](article-d5212-5-1.md)
- [Article R5212-5](article-r5212-5.md)
- [Article R5212-6](article-r5212-6.md)
- [Article R5212-7](article-r5212-7.md)
- [Article R5212-8](article-r5212-8.md)
- [Article R5212-9](article-r5212-9.md)
