# Sous-section 2 : Mise en œuvre par application d'un accord

- [Article R5212-12](article-r5212-12.md)
- [Article R5212-13](article-r5212-13.md)
- [Article R5212-14](article-r5212-14.md)
- [Article R5212-15](article-r5212-15.md)
- [Article R5212-16](article-r5212-16.md)
- [Article R5212-17](article-r5212-17.md)
- [Article R5212-18](article-r5212-18.md)
