# Section 2 : Modalités de mise en œuvre de l'obligation

- [Sous-section 1 : Mise en œuvre partielle](sous-section-1)
- [Sous-section 2 : Mise en œuvre par application d'un accord](sous-section-2)
- [Sous-section 3 : Mise en œuvre par le versement d'une contribution annuelle](sous-section-3)
- [Sous-section 4 : Sanction administrative](sous-section-4)
