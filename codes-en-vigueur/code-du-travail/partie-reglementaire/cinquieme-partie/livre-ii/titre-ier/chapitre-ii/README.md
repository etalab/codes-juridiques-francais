# Chapitre II : Obligation d'emploi des travailleurs handicapés,  mutilés de guerre et assimilés

- [Section 1 : Obligation d'emploi](section-1)
- [Section 2 : Modalités de mise en œuvre de l'obligation](section-2)
