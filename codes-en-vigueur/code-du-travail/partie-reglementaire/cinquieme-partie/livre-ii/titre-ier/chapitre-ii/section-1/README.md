# Section 1 : Obligation d'emploi

- [Article D5212-3](article-d5212-3.md)
- [Article R5212-1](article-r5212-1.md)
- [Article R5212-1-1](article-r5212-1-1.md)
- [Article R5212-1-2](article-r5212-1-2.md)
- [Article R5212-1-3](article-r5212-1-3.md)
- [Article R5212-1-4](article-r5212-1-4.md)
- [Article R5212-2](article-r5212-2.md)
- [Article R5212-2-1](article-r5212-2-1.md)
- [Article R5212-2-2](article-r5212-2-2.md)
- [Article R5212-4](article-r5212-4.md)
