# Section 1 : Statut et missions de Pôle emploi

- [Article R5312-1](article-r5312-1.md)
- [Article R5312-1-1](article-r5312-1-1.md)
- [Article R5312-2](article-r5312-2.md)
- [Article R5312-3](article-r5312-3.md)
- [Article R5312-4](article-r5312-4.md)
- [Article R5312-5](article-r5312-5.md)
