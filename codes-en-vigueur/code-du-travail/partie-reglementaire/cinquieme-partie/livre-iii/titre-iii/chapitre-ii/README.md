# Chapitre II : Conditions de publication et de diffusion  des offres d'emploi

- [Article R5332-1](article-r5332-1.md)
- [Article R5332-2](article-r5332-2.md)
