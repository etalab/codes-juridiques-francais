# Paragraphe 4 : Aides et exonérations.

- [Sous-paragraphe 1 : Aide forfaitaire.](sous-paragraphe-1)
- [Sous-paragraphe 2 : Exonérations.](sous-paragraphe-2)
- [Sous-paragraphe 3 : Aide à la formation.](sous-paragraphe-3)
