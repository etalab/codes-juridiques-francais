# Paragraphe 2 : Versement, renouvellement et prolongation.

- [Article R5423-8](article-r5423-8.md)
- [Article R5423-9](article-r5423-9.md)
- [Article R5423-12](article-r5423-12.md)
- [Article R5423-13](article-r5423-13.md)
