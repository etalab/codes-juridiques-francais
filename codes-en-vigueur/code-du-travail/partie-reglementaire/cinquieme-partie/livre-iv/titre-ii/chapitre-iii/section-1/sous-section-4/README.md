# Sous-section 4 : Allocation forfaitaire du contrat nouvelles embauches.

- [Article D5423-38](article-d5423-38.md)
- [Article D5423-39](article-d5423-39.md)
- [Article D5423-40](article-d5423-40.md)
- [Article R5423-41](article-r5423-41.md)
- [Article R5423-42](article-r5423-42.md)
- [Article R5423-43](article-r5423-43.md)
- [Article R5423-44](article-r5423-44.md)
- [Article R5423-46](article-r5423-46.md)
- [Article R5423-47](article-r5423-47.md)
