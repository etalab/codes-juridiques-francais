# Article R5423-33

L'Office français de protection des réfugiés et apatrides communique, chaque mois, à Pôle emploi, des informations relatives à l'état d'avancement de la procédure d'examen du dossier de demande d'asile et les décisions devenues définitives.
