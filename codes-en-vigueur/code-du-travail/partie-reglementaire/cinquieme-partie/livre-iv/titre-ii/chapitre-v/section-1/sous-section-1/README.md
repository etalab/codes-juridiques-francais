# Sous-section 1 : Exercice d'une activité professionnelle.

- [Article R5425-1](article-r5425-1.md)
- [Article R5425-2](article-r5425-2.md)
- [Article R5425-3](article-r5425-3.md)
- [Article R5425-4](article-r5425-4.md)
- [Article R5425-5](article-r5425-5.md)
- [Article R5425-6](article-r5425-6.md)
- [Article R5425-7](article-r5425-7.md)
- [Article R5425-8](article-r5425-8.md)
