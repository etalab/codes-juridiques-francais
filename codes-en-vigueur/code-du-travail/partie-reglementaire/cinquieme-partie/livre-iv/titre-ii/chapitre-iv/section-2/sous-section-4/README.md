# Sous-section 4 : Calcul de l'indemnité.

- [Article D5424-15](article-d5424-15.md)
- [Article D5424-16](article-d5424-16.md)
