# Section 2 : Entreprises du bâtiment et des travaux publics privées d'emploi  par suite d'intempéries.

- [Sous-section 1 : Champ d'application.](sous-section-1)
- [Sous-section 2 : Périodes d'arrêt saisonnier.](sous-section-2)
- [Sous-section 3 : Conditions d'attribution de l'indemnité.](sous-section-3)
- [Sous-section 4 : Calcul de l'indemnité.](sous-section-4)
- [Sous-section 5 : Situation des salariés.](sous-section-5)
- [Sous-section 6 : Remboursement de l'employeur.](sous-section-6)
- [Sous-section 7 : Cotisations et péréquation des charges.](sous-section-7)
- [Sous-section 8 : Contrôles et contestations.](sous-section-8)
- [Sous-section 9 : Salariés employés en régie par l'Etat.](sous-section-9)
