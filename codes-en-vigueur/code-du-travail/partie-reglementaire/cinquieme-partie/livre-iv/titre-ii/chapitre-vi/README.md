# Chapitre VI : Contrôle et sanctions

- [Section 1 : Agents chargés du contrôle de la condition d'aptitude  au travail et de recherche d'emploi.](section-1)
- [Section 2 : Réduction, suspension ou suppression du revenu de remplacement.](section-2)
- [Section 3 : Pénalité administrative.](section-3)
- [Section 4 : Répétition des prestations indues](section-4)
