# Article R5411-2

Pour demander son inscription sur la liste des demandeurs d'emploi, le travailleur recherchant un emploi se présente personnellement auprès des services de                Pôle emploi.

Dans les localités où les services mentionnés au premier alinéa n'existent pas, le travailleur recherchant un emploi se présente personnellement auprès des services de la mairie de son domicile.
