# Article R5411-5

La personne qui demande son inscription moins de six mois après avoir cessé d'être inscrite ou après avoir été radiée de la liste des demandeurs d'emploi n'est pas tenue de se présenter personnellement aux services mentionnés à l'article R. 5411-2.

Dans ce cas, l'inscription est faite par voie postale ou électronique, dans des conditions fixées par un arrêté du ministre chargé de l'emploi. Cet arrêté précise notamment les modalités selon lesquelles le service destinataire adresse à cette personne la preuve de sa demande.
