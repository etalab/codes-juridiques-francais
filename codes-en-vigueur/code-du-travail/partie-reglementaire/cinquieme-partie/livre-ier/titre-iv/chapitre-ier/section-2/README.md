# Section 2 : Exonérations de charges sociales

- [Article R5141-7](article-r5141-7.md)
- [Article R5141-8](article-r5141-8.md)
- [Article R5141-9](article-r5141-9.md)
- [Article R5141-10](article-r5141-10.md)
- [Article R5141-11](article-r5141-11.md)
- [Article R5141-12](article-r5141-12.md)
