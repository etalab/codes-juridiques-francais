# Section 4 : Contrat de génération

- [Sous-section 1 : Dispositions générales](sous-section-1)
- [Sous-section 2 : Accords collectifs et plans d'actions](sous-section-2)
- [Sous-section 3 : Pénalités](sous-section-3)
- [Sous-section 4 : Modalités de l'aide](sous-section-4)
