# Sous-section 2 : Accords collectifs et plans d'actions

- [Article D5121-27](article-d5121-27.md)
- [Article R5121-28](article-r5121-28.md)
- [Article R5121-29](article-r5121-29.md)
- [Article R5121-31](article-r5121-31.md)
- [Article R5121-32](article-r5121-32.md)
