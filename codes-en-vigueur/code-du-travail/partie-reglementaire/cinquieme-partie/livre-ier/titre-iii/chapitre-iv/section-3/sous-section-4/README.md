# Sous-section 4 : Périodes de mise en situation en milieu professionnel

- [Article D5134-71-1](article-d5134-71-1.md)
- [Article D5134-71-2](article-d5134-71-2.md)
- [Article D5134-71-3](article-d5134-71-3.md)
