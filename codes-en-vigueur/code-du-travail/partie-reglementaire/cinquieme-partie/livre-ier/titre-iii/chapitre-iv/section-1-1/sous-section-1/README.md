# Sous-section 1 : Dispositions générales

- [Article R5134-14](article-r5134-14.md)
- [Article R5134-15](article-r5134-15.md)
- [Article R5134-16](article-r5134-16.md)
- [Article R5134-17](article-r5134-17.md)
- [Article R5134-17-1](article-r5134-17-1.md)
