# Chapitre Ier : Accompagnement personnalisé  pour l'accès à l'emploi

- [Section 1 : Objet et conventions](section-1)
- [Section 2 : Plan local pluriannuel pour l'insertion et l'emploi](section-2)
- [Section 3 : Accompagnement des jeunes vers l'emploi](section-3)
