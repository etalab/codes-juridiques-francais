# Sous-section 2 : Contrat d'insertion dans la vie sociale

- [Paragraphe 1 : Conventions](paragraphe-1)
- [Paragraphe 2 : Bénéficiaires](paragraphe-2)
- [Paragraphe 3 : Modalités de l'accompagnement et engagement des parties](paragraphe-3)
- [Paragraphe 4 : Durée maximale, renouvellement et fin du contrat](paragraphe-4)
- [Paragraphe 5 : Montant et modalités de versement de l'allocation](paragraphe-5)
