# Section 4 : Fonds départemental d'insertion

- [Article R5132-44](article-r5132-44.md)
- [Article R5132-45](article-r5132-45.md)
- [Article R5132-46](article-r5132-46.md)
- [Article R5132-47](article-r5132-47.md)
