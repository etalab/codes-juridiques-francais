# Chapitre II : Insertion par l'activité économique

- [Section 1 : Entreprises d'insertion](section-1)
- [Section 1 bis : Entreprises de travail temporaire d'insertion](section-1-bis)
- [Section 2 : Associations intermédiaires](section-2)
- [Section 3 : Ateliers et chantiers d'insertion](section-3)
- [Section 4 : Fonds départemental d'insertion](section-4)
