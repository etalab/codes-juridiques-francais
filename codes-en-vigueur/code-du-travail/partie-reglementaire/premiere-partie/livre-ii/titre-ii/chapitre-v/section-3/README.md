# Section 3 : Congés d'adoption

- [Article R1225-9](article-r1225-9.md)
- [Article R1225-10](article-r1225-10.md)
- [Article R1225-11](article-r1225-11.md)
