# Chapitre V : Maternité, paternité, adoption et éducation des enfants

- [Section 1 : Protection de la grossesse et de la maternité](section-1)
- [Section 2 : Congé de paternité](section-2)
- [Section 3 : Congés d'adoption](section-3)
- [Section 4 : Congé d'éducation des enfants](section-4)
