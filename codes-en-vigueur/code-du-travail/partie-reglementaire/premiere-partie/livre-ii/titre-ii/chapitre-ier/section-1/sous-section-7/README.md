# Sous-section 7 : Obligations de l'organisme destinataire

- [Article R1221-14](article-r1221-14.md)
- [Article R1221-15](article-r1221-15.md)
- [Article R1221-16](article-r1221-16.md)
- [Article R1221-17](article-r1221-17.md)
- [Article R1221-18](article-r1221-18.md)
