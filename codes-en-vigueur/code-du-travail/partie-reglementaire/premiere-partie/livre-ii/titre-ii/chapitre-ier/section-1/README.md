# Section 1 : Déclaration préalable à l'embauche

- [Sous-section 1 : Mentions obligatoires et portée de la déclaration](sous-section-1)
- [Sous-section 2 : Organisme destinataire](sous-section-2)
- [Sous-section 3 : Transmission](sous-section-3)
- [Sous-section 4 : Preuve de la déclaration préalable à l'embauche](sous-section-4)
- [Sous-section 5 : Documents à remettre au salarié](sous-section-5)
- [Sous-section 6 : Contrôle et sanctions administratives](sous-section-6)
- [Sous-section 7 : Obligations de l'organisme destinataire](sous-section-7)
- [Sous-section 8 : Obligation de dématérialisation](sous-section-8)
