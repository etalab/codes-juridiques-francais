# Chapitre Ier : Dispositions générales

- [Article R1261-1](article-r1261-1.md)
- [Article R1261-2](article-r1261-2.md)
