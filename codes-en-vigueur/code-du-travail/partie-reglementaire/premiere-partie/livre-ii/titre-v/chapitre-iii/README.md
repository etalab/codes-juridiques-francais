# Chapitre III : Contrats conclus avec un groupement d'employeurs

- [Section 1 : Groupement d'employeurs entrant dans le champ d'application  d'une même convention collective](section-1)
- [Section 2 : Groupement d'employeurs n'entrant pas  dans le champ d'application d'une même convention collective](section-2)
- [Section 3 : Groupement d'employeurs pour le remplacement de chefs d'exploitation agricole ou d'entreprises artisanales, industrielles ou commerciales ou de personnes physiques exerçant une profession libérale](section-3)
- [Section 4 : Groupements d'employeurs constitués  au sein d'une société coopérative existante](section-4)
- [Section 5 : Groupement d'employeurs composé d'adhérents  de droit privé et de collectivités territoriales](section-5)
