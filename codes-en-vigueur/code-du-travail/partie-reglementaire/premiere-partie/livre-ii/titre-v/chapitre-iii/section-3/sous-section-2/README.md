# Sous-section 2 : Agrément

- [Paragraphe 1 : Demande d'agrément](paragraphe-1)
- [Paragraphe 2 : Délivrance de l'agrément](paragraphe-2)
- [Paragraphe 3 : Retrait d'agrément](paragraphe-3)
- [Paragraphe 4 : Recours hiérarchique](paragraphe-4)
