# Chapitre VII : Autres cas de rupture

- [Section 1 : Retraite](section-1)
- [Section 2 : Rupture conventionnelle](section-2)
