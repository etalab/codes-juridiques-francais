# Sous-section 4 : Intervention de l'autorité administrative

- [Article D1233-11](article-d1233-11.md)
- [Article D1233-12](article-d1233-12.md)
