# Sous-section 3 : Attestation d'assurance chômage

- [Article R1234-9](article-r1234-9.md)
- [Article R1234-10](article-r1234-10.md)
- [Article R1234-11](article-r1234-11.md)
- [Article R1234-12](article-r1234-12.md)
