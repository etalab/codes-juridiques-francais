# Section 2 : Actions en justice des organisations syndicales  en cas de licenciement économique

- [Article D1235-18](article-d1235-18.md)
- [Article D1235-19](article-d1235-19.md)
- [Article D1235-20](article-d1235-20.md)
