# Sous-section 2 : Interdictions

- [Article D1242-4](article-d1242-4.md)
- [Article D1242-5](article-d1242-5.md)
