# Section 2 : Composition

- [Article R1431-4](article-r1431-4.md)
- [Article R1431-5](article-r1431-5.md)
- [Article R1431-6](article-r1431-6.md)
- [Article R1431-7](article-r1431-7.md)
- [Article R1431-8](article-r1431-8.md)
