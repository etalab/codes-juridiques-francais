# Chapitre VII : Récusation

- [Article R1457-1](article-r1457-1.md)
- [Article R1457-2](article-r1457-2.md)
