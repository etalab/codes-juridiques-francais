# Section 1 : Formation

- [Article D1442-1](article-d1442-1.md)
- [Article D1442-3](article-d1442-3.md)
- [Article D1442-4](article-d1442-4.md)
- [Article D1442-5](article-d1442-5.md)
- [Article D1442-6](article-d1442-6.md)
- [Article D1442-7](article-d1442-7.md)
- [Article D1442-8](article-d1442-8.md)
- [Article D1442-9](article-d1442-9.md)
- [Article D1442-10](article-d1442-10.md)
- [Article R1442-2](article-r1442-2.md)
