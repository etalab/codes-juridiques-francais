# Chapitre III : Dispositions pénales

- [Article R1443-1](article-r1443-1.md)
- [Article R1443-2](article-r1443-2.md)
- [Article R1443-3](article-r1443-3.md)
