# Paragraphe 1 : Opérations préparatoires au scrutin

- [Article D1441-77](article-d1441-77.md)
- [Article D1441-78](article-d1441-78.md)
- [Article D1441-79](article-d1441-79.md)
