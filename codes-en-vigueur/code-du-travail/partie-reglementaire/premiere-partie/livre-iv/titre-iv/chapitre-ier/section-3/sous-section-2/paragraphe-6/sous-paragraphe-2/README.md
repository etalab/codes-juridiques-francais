# Sous-paragraphe 2 : Comptage des votes

- [Article D1441-149](article-d1441-149.md)
- [Article D1441-150](article-d1441-150.md)
- [Article D1441-151](article-d1441-151.md)
- [Article D1441-152](article-d1441-152.md)
- [Article D1441-153](article-d1441-153.md)
- [Article D1441-154](article-d1441-154.md)
- [Article R1441-148](article-r1441-148.md)
