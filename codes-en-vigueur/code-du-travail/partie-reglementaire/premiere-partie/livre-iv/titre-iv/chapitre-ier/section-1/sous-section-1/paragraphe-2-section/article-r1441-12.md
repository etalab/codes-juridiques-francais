# Article R1441-12

Les employés de maison ainsi que leurs employeurs sont électeurs au titre de la section des activités diverses.
