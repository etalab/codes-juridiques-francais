# Paragraphe 3 : Commune d'inscription

- [Article R1441-15](article-r1441-15.md)
- [Article R1441-16](article-r1441-16.md)
- [Article R1441-17](article-r1441-17.md)
- [Article R1441-18](article-r1441-18.md)
- [Article R1441-19](article-r1441-19.md)
