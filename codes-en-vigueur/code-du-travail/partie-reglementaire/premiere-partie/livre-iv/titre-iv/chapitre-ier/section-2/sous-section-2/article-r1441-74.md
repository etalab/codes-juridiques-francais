# Article R1441-74

Les électeurs mineurs peuvent présenter un recours sans autorisation.
