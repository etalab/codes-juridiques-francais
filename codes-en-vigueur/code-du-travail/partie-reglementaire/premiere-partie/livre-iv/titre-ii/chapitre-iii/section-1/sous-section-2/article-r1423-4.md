# Article R1423-4

L'activité principale de l'employeur détermine son appartenance à l'une des sections.
