# Article D1423-67

Le nombre d'heures indemnisables qu'un conseiller prud'hommes peut déclarer avoir consacré à la rédaction de décisions qui présentent entre elles un lien caractérisé, notamment du fait de l'identité d'une partie, de l'objet ou de la cause, et qui n'auraient pas fait l'objet d'une jonction, ne peut dépasser les durées fixées au tableau ci-après :
<table>
<tbody>
<tr>
<td>
<p align="center"> NOMBRE DE DÉCISIONS </p>
<p align="center">à rédiger </p>
</td>
<td>
<p align="center"> NOMBRE MAXIMUM </p>
<p align="center">d'heures indemnisables </p>
</td>
</tr>
<tr>
<td align="center">
<br/>2 à 25 <br/>
</td>
<td align="center">
<br/>3 heures <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>26 à 50 <br/>
</td>
<td align="center">
<br/>5 heures <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>51 à 100 <br/>
</td>
<td align="center">
<br/>7 heures <br/>
</td>
</tr>
<tr>
<td align="center">
<br/>Au-delà de 100 <br/>
</td>
<td align="center">
<br/>Durée de 9 heures augmentée de 3 heures par tranche de 100 décisions. <br/>
</td>
</tr>
</tbody>
</table>

Les durées fixées au tableau ci-dessus s'ajoutent au nombre d'heures indemnisables de la décision initiale, qui reste soumis aux dispositions de l'article D. 1423-66.
