# Chapitre IV : Solidarité financière du donneur d'ordre

- [Section 1 : Vérifications préalables](section-1)
- [Section 2 : Méconnaissance de l'obligation](section-2)
