# Sous-section 3 : Recouvrement forcé des salaires et indemnités dus au salarié étranger sans titre

- [Article R8252-8](article-r8252-8.md)
- [Article R8252-9](article-r8252-9.md)
