# Sous-section 1 : Dispositions générales

- [Article R8252-4](article-r8252-4.md)
- [Article R8252-5](article-r8252-5.md)
