# Section 1 : Dispositions communes

- [Article R8222-1](article-r8222-1.md)
- [Article R8222-2](article-r8222-2.md)
- [Article R8222-3](article-r8222-3.md)
