# Chapitre V : Sanctions administratives

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Dispositions particulières](section-2)
