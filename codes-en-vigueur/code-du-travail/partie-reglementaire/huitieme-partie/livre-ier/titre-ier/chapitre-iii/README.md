# Chapitre III : Prérogatives et moyens d'intervention

- [Section 1 : Information sur les lieux de travail à caractère temporaire](section-1)
- [Section 2 : Accès aux documents](section-2)
- [Section 3 : Mises en demeure et demandes de vérification](section-3)
- [Section 4 : Constats dans les établissements de l'Etat, les collectivités  territoriales et leurs établissements publics administratifs](section-4)
- [Section 5 : Prestation de serment](section-5)
