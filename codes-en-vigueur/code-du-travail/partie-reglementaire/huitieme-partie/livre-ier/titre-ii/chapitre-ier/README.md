# Chapitre Ier : Échelon central

- [Section 1 : Conseil national de l'inspection du travail](section-1)
- [Section 2 : Direction générale du travail](section-2)
- [Section 3 : Groupe national de veille, d'appui et de contrôle](section-3)
