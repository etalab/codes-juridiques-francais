# Sous-section 3 : Fonctionnement

- [Article D8121-9](article-d8121-9.md)
- [Article D8121-10](article-d8121-10.md)
- [Article D8121-11](article-d8121-11.md)
- [Article D8121-12](article-d8121-12.md)
