# Sous-section 2 :  Dispositions communes à toutes les activités comportant des risques d'exposition à l'amiante

- [Paragraphe 1 : Evaluation initiale des risques](paragraphe-1)
- [Paragraphe 2 : Valeur limite d'exposition professionnelle](paragraphe-2)
- [Paragraphe 3 : Conditions de mesurage des empoussièrements et de contrôle de la valeur limite d'exposition professionnelle](paragraphe-3)
- [Paragraphe 4 : Principes et moyens de prévention](paragraphe-4)
- [Paragraphe 5 : Information et formation des travailleurs](paragraphe-5)
- [Paragraphe 6 : Organisation du travail](paragraphe-6)
- [Paragraphe 7 : Suivi de l'exposition](paragraphe-7)
- [Paragraphe 8 : Traitement des déchets](paragraphe-8)
- [Paragraphe 9 : Protection de l'environnement du chantier](paragraphe-9)
