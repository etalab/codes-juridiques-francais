# Paragraphe 3 : Certification des entreprises

- [Article R4412-129](article-r4412-129.md)
- [Article R4412-130](article-r4412-130.md)
- [Article R4412-131](article-r4412-131.md)
- [Article R4412-132](article-r4412-132.md)
