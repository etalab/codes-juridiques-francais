# Paragraphe 2 : Surveillance médicale

- [Sous-paragraphe 1 : Examens médicaux et fiche d'aptitude](sous-paragraphe-1)
- [Sous-paragraphe 2 : Dossier médical](sous-paragraphe-2)
