# Section 3 : Information des autorités pour la prévention des risques

- [Article R4411-42](article-r4411-42.md)
- [Article R4411-43](article-r4411-43.md)
- [Article R4411-44](article-r4411-44.md)
- [Article R4411-45](article-r4411-45.md)
- [Article R4411-46](article-r4411-46.md)
