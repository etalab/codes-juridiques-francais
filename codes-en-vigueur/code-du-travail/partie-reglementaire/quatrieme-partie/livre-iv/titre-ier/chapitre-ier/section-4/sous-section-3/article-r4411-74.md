# Article R4411-74

Lorsque le responsable de la mise sur le marché d'un mélange peut apporter la preuve que la divulgation sur l'étiquette ou sur la fiche de données de sécurité de l'identité chimique d'une substance, à l'exception des substances actives contenues dans les produits phytopharmaceutiques définis au 1 de l'article 2 du règlement (CE) n° 1107/2009, porte atteinte au secret industriel, il peut être autorisé à désigner cette substance, sur l'étiquette ainsi que sur la fiche de données de sécurité, à l'aide d'une dénomination de remplacement, qui peut identifier les groupes chimiques fonctionnels les plus importants.

La dénomination de remplacement doit fournir suffisamment d'informations sur la substance pour que les précautions nécessaires en matière de santé et de sécurité puissent être prises.
