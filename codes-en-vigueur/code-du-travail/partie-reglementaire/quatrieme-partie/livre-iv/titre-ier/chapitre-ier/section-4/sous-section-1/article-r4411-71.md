# Article R4411-71

Toute substance ou mélange, qui ne fait pas l'objet d'un des arrêtés mentionnés à l'article R. 4411-69 mais donne lieu à la fourniture des informations mentionnées à l'article L. 4411-4, est étiqueté et emballé par le fabricant, l'importateur ou le vendeur sur la base de ces informations et des règles générales fixées par ces arrêtés.
