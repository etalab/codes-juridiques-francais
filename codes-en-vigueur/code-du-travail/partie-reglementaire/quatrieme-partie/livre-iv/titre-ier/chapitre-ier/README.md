# Chapitre Ier : Mise sur le marché des substances et mélanges

- [Section 1 : Dispositions générales](section-1)
- [Section 2 : Définitions et principes de classement](section-2)
- [Section 3 : Information des autorités pour la prévention des risques](section-3)
- [Section 4 : Protection des utilisateurs et acheteurs](section-4)
- [Section 5 : Exemptions pour les intérêts de la défense](section-5)
