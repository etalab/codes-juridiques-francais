# Section 1 : Définitions et dispositions générales

- [Article R4461-1](article-r4461-1.md)
- [Article R4461-2](article-r4461-2.md)
