# Sous-section 3 : Habilitation, accréditation et certification

- [Paragraphe 1 : Habilitation](paragraphe-1)
- [Paragraphe 2 : Accréditation et certification](paragraphe-2)
