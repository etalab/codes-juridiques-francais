# Paragraphe 4 : Exploitation des résultats

- [Article R4451-35](article-r4451-35.md)
- [Article R4451-36](article-r4451-36.md)
- [Article R4451-37](article-r4451-37.md)
