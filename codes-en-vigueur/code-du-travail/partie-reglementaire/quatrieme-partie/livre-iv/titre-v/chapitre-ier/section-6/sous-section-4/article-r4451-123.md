# Article R4451-123

Les entreprises de travail temporaire qui mettent à disposition des travailleurs pour la réalisation de travaux mentionnés à l'article R. 4451-122 sont soumises aux obligations de ce même article.
