# Chapitre II : Demandes de vérifications, d'analyses et de mesures

- [Section 1 : Aération et assainissement des locaux de travail.](section-1)
- [Section 2 : Éclairage des lieux de travail.](section-2)
- [Section 3 : Équipements de travail et moyens de protection.](section-3)
- [Section 4 : Risques chimiques.](section-4)
- [Section 5 : Bruit.](section-5)
- [Section 6 : Vibrations mécaniques.](section-6)
- [Section 7 : Rayonnements .](section-7)
- [Section 8 : Travaux du bâtiment et du génie civil.](section-8)
- [Section 9 : Installations électriques](section-9)
- [Section 10 : Dispositions communes](section-10)
