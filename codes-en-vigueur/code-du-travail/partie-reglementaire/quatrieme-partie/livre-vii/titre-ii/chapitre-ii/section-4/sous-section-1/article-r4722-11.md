# Article R4722-11

<div>
<p>L'employeur transmet les résultats des analyses à l'inspecteur du travail, qui en transmet copie au médecin inspecteur du travail et à l'organisme désigné en application de l'article
R. 4411-61
.</p>
</div>
