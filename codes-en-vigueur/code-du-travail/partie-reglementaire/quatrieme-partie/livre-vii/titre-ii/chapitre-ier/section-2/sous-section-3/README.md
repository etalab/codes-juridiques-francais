# Sous-section 3 : Mise en demeure de réduction d'intervalle  entre les vérifications périodiques.

- [Article R4721-11](article-r4721-11.md)
- [Article R4721-12](article-r4721-12.md)
