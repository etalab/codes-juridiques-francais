# Section 2 : Dérogations

- [Article D4154-2](article-d4154-2.md)
- [Article D4154-3](article-d4154-3.md)
- [Article D4154-4](article-d4154-4.md)
- [Article D4154-6](article-d4154-6.md)
- [Article R4154-5](article-r4154-5.md)
