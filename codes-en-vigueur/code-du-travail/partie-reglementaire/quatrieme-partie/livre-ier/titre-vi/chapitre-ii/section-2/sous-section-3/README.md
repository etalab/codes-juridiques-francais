# Sous-section 3 : Utilisation du compte pour le passage à temps partiel

- [Article D4162-18](article-d4162-18.md)
- [Article D4162-19](article-d4162-19.md)
- [Article D4162-20](article-d4162-20.md)
- [Article D4162-21](article-d4162-21.md)
- [Article D4162-22](article-d4162-22.md)
