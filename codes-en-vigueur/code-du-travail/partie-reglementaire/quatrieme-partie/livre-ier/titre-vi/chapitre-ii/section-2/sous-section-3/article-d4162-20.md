# Article D4162-20

<div align="left">Une fois l'accord de son employeur obtenu, le salarié formule sa demande d'utilisation des points au titre du 2° du I de l'article L. 4162-4 dans les conditions fixées à l'article R. 4162-8.<br/>
<br/>
<br/>
</div>
