# Sous-section 3 : Dépenses du fonds

- [Article D4162-51](article-d4162-51.md)
- [Article D4162-52](article-d4162-52.md)
- [Article D4162-53](article-d4162-53.md)
