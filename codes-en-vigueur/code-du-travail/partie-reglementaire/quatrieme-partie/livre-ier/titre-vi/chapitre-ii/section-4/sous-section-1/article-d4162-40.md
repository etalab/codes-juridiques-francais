# Article D4162-40

<div align="left">Le fonds est administré par un conseil d'administration composé de trente-sept membres, désignés par arrêté des ministres chargés du travail, de la sécurité sociale et du budget et comprenant : <p>1° Deux représentants du ministre chargé du travail ; </p>
<p>2° Deux représentants du ministre chargé de la sécurité sociale ; </p>
<p>3° Deux représentants du ministre chargé du budget ; </p>
<p>4° Treize représentants des assurés sociaux désignés par les organisations syndicales de salariés interprofessionnelles représentatives au plan national, à raison de : </p>
<p>- trois représentants de la Confédération générale du travail ; </p>
<p>- trois représentants de la Confédération générale du travail-Force ouvrière ; </p>
<p>- trois représentants de la Confédération française démocratique du travail ; </p>
<p>- deux représentants de la Confédération française des travailleurs chrétiens ; </p>
<p>- deux représentants de la Confédération française de l'encadrement-Confédération générale des cadres ; </p>
<p>5° Treize représentants des employeurs désignés par les organisations professionnelles nationales d'employeurs représentatives, à raison de : </p>
<p>- sept représentants du Mouvement des entreprises de France ; </p>
<p>- trois représentants de la Confédération générale des petites et moyennes entreprises ; </p>
<p>- trois représentants de l'Union professionnelle artisanale ; </p>
<p>6° Cinq personnalités qualifiées. </p>
<p>Le président du conseil d'administration du fonds est désigné parmi les personnalités qualifiées mentionnées au 6°. </p>
<p>Les membres du conseil d'administration sont désignés pour une durée de quatre ans renouvelable. Leurs fonctions sont assurées à titre gratuit. Les frais de déplacement sont remboursés dans les conditions prévues par le décret n° 2006-781 du 3 juillet 2006 fixant les conditions et les modalités de règlement des frais occasionnés par les déplacements temporaires des personnels civils de l'Etat. </p>
</div>
