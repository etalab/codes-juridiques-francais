# Section 4 : Fonds chargé du financement des droits liés au compte personnel de prévention de la pénibilité

- [Sous-section 1 : Organisation et fonctionnement du fonds](sous-section-1)
- [Sous-section 2 : Gestion administrative, financière et comptable du fonds](sous-section-2)
- [Sous-section 3 : Dépenses du fonds](sous-section-3)
- [Sous-section 4 : Recettes du fonds](sous-section-4)
