# Article D4162-39

<div align="left">Le fonds chargé du financement des droits liés au compte personnel de prévention de la pénibilité est placé sous la tutelle des ministres chargés du travail, de la sécurité sociale et du budget. <br/>
<br/>
</div>
