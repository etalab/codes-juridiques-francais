# Section 1 : Ouverture et abondement du compte personnel de prévention de la pénibilité

- [Article R4162-1](article-r4162-1.md)
- [Article R4162-2](article-r4162-2.md)
- [Article R4162-3](article-r4162-3.md)
