# Chapitre V : Dispositions applicables aux travailleurs indépendants

- [Section 1 : Prescriptions techniques durant l'exécution  de travaux de bâtiment et de génie civil.](section-1)
- [Section 2 : Utilisation d'équipements de travail  et de protection individuelle.](section-2)
- [Section 3 : Risques chimiques.](section-3)
- [Section 4 : Risques électriques](section-4)
- [Section 5 : Risque hyperbare](section-5)
