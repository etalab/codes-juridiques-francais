# Section 3 : Risques chimiques.

- [Sous-section 1 : Mesures générales de prévention  des risques chimiques.](sous-section-1)
- [Sous-section 2 : Agents cancérogènes, mutagènes  ou toxiques pour la reproduction.](sous-section-2)
- [Sous-section 3 : Activités de confinement et de retrait d'amiante et activités et interventions sur des matériaux et appareils susceptibles de libérer des fibres d'amiante.](sous-section-3)
