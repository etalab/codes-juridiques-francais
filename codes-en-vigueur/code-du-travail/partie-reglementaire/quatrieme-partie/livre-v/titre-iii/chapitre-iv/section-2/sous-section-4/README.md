# Sous-section 4 : Examens, vérifications, registres.

- [Article R4534-15](article-r4534-15.md)
- [Article R4534-16](article-r4534-16.md)
- [Article R4534-17](article-r4534-17.md)
- [Article R4534-18](article-r4534-18.md)
- [Article R4534-19](article-r4534-19.md)
- [Article R4534-20](article-r4534-20.md)
