# Sous-section 1 : Désignation des entreprises extérieures et de leurs représentants.

- [Article R4523-5](article-r4523-5.md)
- [Article R4523-6](article-r4523-6.md)
- [Article R4523-7](article-r4523-7.md)
- [Article R4523-8](article-r4523-8.md)
- [Article R4523-9](article-r4523-9.md)
- [Article R4523-10](article-r4523-10.md)
- [Article R4523-11](article-r4523-11.md)
- [Article R4523-12](article-r4523-12.md)
- [Article R4523-13](article-r4523-13.md)
