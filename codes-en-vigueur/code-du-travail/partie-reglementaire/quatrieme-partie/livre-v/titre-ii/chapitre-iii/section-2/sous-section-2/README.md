# Sous-section 2 : Fonctionnement du comité élargi.

- [Article R4523-14](article-r4523-14.md)
- [Article R4523-15](article-r4523-15.md)
- [Article R4523-16](article-r4523-16.md)
