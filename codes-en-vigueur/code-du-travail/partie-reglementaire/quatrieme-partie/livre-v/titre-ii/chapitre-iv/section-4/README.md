# Section 4 : Fonctionnement.

- [Article R4524-7](article-r4524-7.md)
- [Article R4524-8](article-r4524-8.md)
- [Article R4524-9](article-r4524-9.md)
- [Article R4524-10](article-r4524-10.md)
