# Section 2 : Inspection commune préalable.

- [Article R4512-2](article-r4512-2.md)
- [Article R4512-3](article-r4512-3.md)
- [Article R4512-4](article-r4512-4.md)
- [Article R4512-5](article-r4512-5.md)
