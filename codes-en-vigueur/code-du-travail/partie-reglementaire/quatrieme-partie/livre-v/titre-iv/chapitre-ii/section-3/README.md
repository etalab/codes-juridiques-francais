# Section 3 : Mesures et moyens de prévention.

- [Article R4542-4](article-r4542-4.md)
- [Article R4542-5](article-r4542-5.md)
- [Article R4542-6](article-r4542-6.md)
- [Article R4542-7](article-r4542-7.md)
- [Article R4542-8](article-r4542-8.md)
- [Article R4542-9](article-r4542-9.md)
- [Article R4542-10](article-r4542-10.md)
- [Article R4542-11](article-r4542-11.md)
