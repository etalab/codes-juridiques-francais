# Chapitre III : Interventions sur les équipements élévateurs et installés à demeure.

- [Section 1 : Champ d'application.](section-1)
- [Section 2 : Etude de sécurité spécifique.](section-2)
- [Section 3 : Information des travailleurs intervenants.](section-3)
- [Section 4 : Organisation de l'intervention.](section-4)
- [Section 5 : Travailleurs isolés.](section-5)
- [Section 6 : Formation des travailleurs.](section-6)
- [Section 7 : Montage et démontage des ascenseurs.](section-7)
