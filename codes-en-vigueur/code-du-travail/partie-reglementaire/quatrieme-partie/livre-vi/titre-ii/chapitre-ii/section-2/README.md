# Section 2 : Services autonomes de santé au travail.

- [Sous-section 1 : Services de santé au travail de groupe, d'entreprise ou d'établissement.](sous-section-1)
- [Sous-section 2 : Services de santé au travail interétablissements.](sous-section-2)
- [Sous-section 3 : Services de santé au travail communs aux entreprises constituant une unité économique et sociale.](sous-section-3)
