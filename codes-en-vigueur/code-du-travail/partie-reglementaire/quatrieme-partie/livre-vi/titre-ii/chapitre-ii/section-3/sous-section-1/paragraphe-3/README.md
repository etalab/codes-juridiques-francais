# Paragraphe 3 : Secteurs.

- [Article D4622-25](article-d4622-25.md)
- [Article D4622-26](article-d4622-26.md)
- [Article D4622-27](article-d4622-27.md)
