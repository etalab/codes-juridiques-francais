# Section 2 : Suivi individuel de l'état de santé du salarié.

- [Sous-section 1 : Examen d'embauche.](sous-section-1)
- [Sous-section 2 : Examens périodiques.](sous-section-2)
- [Sous-section 3 : Surveillance médicale renforcée.](sous-section-3)
- [Sous-section 4 : Examens de préreprise et de reprise du travail.](sous-section-4)
- [Sous-section 5 : Examens complémentaires.](sous-section-5)
- [Sous-section 6 : Déroulement des examens médicaux.](sous-section-6)
- [Sous-section 7 : Déclaration d'inaptitude.](sous-section-7)
- [Sous-section 8 : Contestation des avis médicaux d'aptitude ou d'inaptitude.](sous-section-8)
