# Sous-section 3 : Dossier médical en santé au travail et fiches médicales d'aptitude.

- [Article R4624-46](article-r4624-46.md)
- [Article R4624-47](article-r4624-47.md)
- [Article R4624-49](article-r4624-49.md)
