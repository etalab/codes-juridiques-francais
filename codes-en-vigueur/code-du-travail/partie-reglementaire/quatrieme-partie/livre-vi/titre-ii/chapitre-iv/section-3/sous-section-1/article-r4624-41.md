# Article R4624-41

Le modèle de fiche d'entreprise est fixé par arrêté du ministre chargé du travail.
