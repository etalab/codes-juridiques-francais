# Paragraphe 3 : Surveillance médicale renforcée.

- [Article R4626-27](article-r4626-27.md)
- [Article R4626-28](article-r4626-28.md)
