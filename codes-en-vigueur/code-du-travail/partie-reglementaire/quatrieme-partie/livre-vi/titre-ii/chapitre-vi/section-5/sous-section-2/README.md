# Sous-section 2 : Dossier médical et fiche médicale d'aptitude.

- [Article D4626-33](article-d4626-33.md)
- [Article D4626-34](article-d4626-34.md)
- [Article D4626-35](article-d4626-35.md)
