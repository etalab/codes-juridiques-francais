# Sous-section 3 : Action du médecin du travail.

- [Paragraphe 1er : Action sur le milieu de travail](paragraphe-1er)
- [Paragraphe 2 : Examens médicaux.](paragraphe-2)
