# Sous-section 3 : Organisation et fonctionnement

- [Article D4641-36](article-d4641-36.md)
- [Article D4641-37](article-d4641-37.md)
- [Article D4641-38](article-d4641-38.md)
- [Article D4641-39](article-d4641-39.md)
- [Article R4641-35](article-r4641-35.md)
