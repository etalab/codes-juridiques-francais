# Paragraphe 2 : Comités régionaux.

- [Article R4643-19](article-r4643-19.md)
- [Article R4643-20](article-r4643-20.md)
- [Article R4643-21](article-r4643-21.md)
- [Article R4643-22](article-r4643-22.md)
- [Article R4643-23](article-r4643-23.md)
- [Article R4643-24](article-r4643-24.md)
- [Article R4643-25](article-r4643-25.md)
- [Article R4643-26](article-r4643-26.md)
- [Article R4643-27](article-r4643-27.md)
