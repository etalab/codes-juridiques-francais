# Article R4642-26

Les crédits budgétaires nécessaires à la mise en place et au fonctionnement de l'Agence sont inscrits au budget de l'Etat au titre de la mission relevant du travail.
