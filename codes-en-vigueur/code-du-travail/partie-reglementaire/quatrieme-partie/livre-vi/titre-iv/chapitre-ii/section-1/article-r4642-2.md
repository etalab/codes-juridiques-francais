# Article R4642-2

L'Agence nationale pour l'amélioration des conditions de travail a pour objet d'entreprendre et de favoriser toute action tendant à améliorer les conditions de travail, notamment dans les domaines suivants :

1° L'organisation du travail et du temps de travail ;

2° L'environnement physique du salarié et l'adaptation des postes et locaux de travail ;

3° La participation des salariés à l'organisation du travail ;

4° Les méthodes d'étude et d'appréciation des conditions de travail. A cette fin, elle est chargée, en particulier :

a) De rassembler et diffuser l'information utile ;

b) D'organiser des échanges et des rencontres ;

c) De coordonner et susciter des recherches ;

d) D'inciter les constructeurs à concevoir des machines et des bâtiments industriels adaptés ;

e) D'apporter son concours à des actions de formation ;

f) De susciter et d'encourager le développement d'opérations et d'expériences dans les services publics et les entreprises, notamment en fournissant des informations et en donnant la possibilité de consulter des experts.
