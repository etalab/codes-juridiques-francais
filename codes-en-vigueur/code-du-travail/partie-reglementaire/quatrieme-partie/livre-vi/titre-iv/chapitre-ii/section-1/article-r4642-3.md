# Article R4642-3

L'Agence nationale pour l'amélioration des conditions de travail est un établissement public à caractère administratif placé sous la tutelle du ministre chargé du travail.
