# Article R4642-25

Les conventions conclues avec les associations déterminent les conditions dans lesquelles l'Agence leur apporte son concours et coordonne leurs actions régionales en matière d'amélioration des conditions de travail.
