# Article R4642-7

Le conseil d'administration est réuni au moins quatre fois par an, sur convocation de son président.

Le président du conseil d'administration réunit également celui-ci sur demande de la moitié de ses membres en exercice.
