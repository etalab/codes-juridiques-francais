# Chapitre II : Attributions

- [Section 1 : Missions.](section-1)
- [Section 2 : Consultations obligatoires dans les établissements comportant une ou plusieurs installations soumises à autorisation ou une installation nucléaire de base.](section-2)
- [Section 3 : Rapport et programme annuels.](section-3)
