# Chapitre V :  Comité d'hygiène, de sécurité et des conditions de travail dans certains établissements de santé, sociaux et médico-sociaux

- [Section 1 : Champ d'application et définitions.](section-1)
- [Section 2 : Conditions de mise en place.](section-2)
- [Section 3 : Composition et désignation.](section-3)
- [Section 4 : Fonctionnement.](section-4)
- [Section 5 : Formation.](section-5)
