# Sous-section 4 : Caractéristiques et conditions particulières d'utilisation  des différents catégories d'équipements de travail

- [Paragraphe 1 : Échafaudages](paragraphe-1)
- [Paragraphe 2 : Échelles, escabeaux et marchepieds](paragraphe-2)
- [Paragraphe 3 : Cordes](paragraphe-3)
