# Sous-section 1 : Protecteurs et dispositifs de protection

- [Article R4324-1](article-r4324-1.md)
- [Article R4324-2](article-r4324-2.md)
- [Article R4324-3](article-r4324-3.md)
- [Article R4324-4](article-r4324-4.md)
- [Article R4324-5](article-r4324-5.md)
- [Article R4324-6](article-r4324-6.md)
- [Article R4324-7](article-r4324-7.md)
