# Sous-section 3 : Dispositifs d'alerte et de signalisation

- [Article R4324-16](article-r4324-16.md)
- [Article R4324-17](article-r4324-17.md)
