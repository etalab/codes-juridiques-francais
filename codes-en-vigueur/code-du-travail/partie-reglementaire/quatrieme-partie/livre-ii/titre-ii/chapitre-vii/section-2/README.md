# Section 2 : Dégagements

- [Article R4227-4](article-r4227-4.md)
- [Article R4227-5](article-r4227-5.md)
- [Article R4227-6](article-r4227-6.md)
- [Article R4227-7](article-r4227-7.md)
- [Article R4227-8](article-r4227-8.md)
- [Article R4227-9](article-r4227-9.md)
- [Article R4227-10](article-r4227-10.md)
- [Article R4227-11](article-r4227-11.md)
- [Article R4227-12](article-r4227-12.md)
- [Article R4227-13](article-r4227-13.md)
- [Article R4227-14](article-r4227-14.md)
