# Section 4 : Emploi et stockage  de matières explosives et inflammables

- [Article R4227-22](article-r4227-22.md)
- [Article R4227-23](article-r4227-23.md)
- [Article R4227-24](article-r4227-24.md)
- [Article R4227-25](article-r4227-25.md)
- [Article R4227-26](article-r4227-26.md)
- [Article R4227-27](article-r4227-27.md)
