# Chapitre VII : Risques d'incendies et d'explosions et évacuation

- [Section 1 : Champ d'application](section-1)
- [Section 2 : Dégagements](section-2)
- [Section 3 : Chauffage des locaux](section-3)
- [Section 4 : Emploi et stockage  de matières explosives et inflammables](section-4)
- [Section 5 : Moyens de prévention  et de lutte contre l'incendie](section-5)
- [Section 6 : Prévention des explosions](section-6)
- [Section 7 : Dispenses partielles accordées  par l'autorité administrative](section-7)
