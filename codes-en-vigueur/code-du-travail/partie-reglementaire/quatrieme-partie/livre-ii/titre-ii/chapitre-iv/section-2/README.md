# Section 2 : Portes et portails

- [Article R4224-9](article-r4224-9.md)
- [Article R4224-10](article-r4224-10.md)
- [Article R4224-11](article-r4224-11.md)
- [Article R4224-12](article-r4224-12.md)
- [Article R4224-13](article-r4224-13.md)
