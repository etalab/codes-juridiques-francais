# Section 4 :  Autres dispositions particulières

- [Article R4226-11](article-r4226-11.md)
- [Article R4226-12](article-r4226-12.md)
- [Article R4226-13](article-r4226-13.md)
