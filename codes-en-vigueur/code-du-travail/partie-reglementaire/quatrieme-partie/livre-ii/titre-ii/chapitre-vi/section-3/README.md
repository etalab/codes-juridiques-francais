# Section 3 :  Dispositions particulières à certains locaux ou emplacements

- [Article R4226-8](article-r4226-8.md)
- [Article R4226-9](article-r4226-9.md)
- [Article R4226-10](article-r4226-10.md)
