# Section 2 :  Dispositions générales

- [Article R4226-5](article-r4226-5.md)
- [Article R4226-6](article-r4226-6.md)
- [Article R4226-7](article-r4226-7.md)
