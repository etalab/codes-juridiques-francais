# Sous-section 2 : Vestiaires collectifs

- [Article R4228-2](article-r4228-2.md)
- [Article R4228-3](article-r4228-3.md)
- [Article R4228-4](article-r4228-4.md)
- [Article R4228-5](article-r4228-5.md)
- [Article R4228-6](article-r4228-6.md)
