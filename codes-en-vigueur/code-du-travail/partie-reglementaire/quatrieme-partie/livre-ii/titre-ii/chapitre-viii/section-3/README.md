# Section 3 : Hébergement

- [Article R4228-26](article-r4228-26.md)
- [Article R4228-27](article-r4228-27.md)
- [Article R4228-28](article-r4228-28.md)
- [Article R4228-29](article-r4228-29.md)
- [Article R4228-30](article-r4228-30.md)
- [Article R4228-31](article-r4228-31.md)
- [Article R4228-32](article-r4228-32.md)
- [Article R4228-33](article-r4228-33.md)
- [Article R4228-34](article-r4228-34.md)
- [Article R4228-35](article-r4228-35.md)
- [Article R4228-36](article-r4228-36.md)
- [Article R4228-37](article-r4228-37.md)
