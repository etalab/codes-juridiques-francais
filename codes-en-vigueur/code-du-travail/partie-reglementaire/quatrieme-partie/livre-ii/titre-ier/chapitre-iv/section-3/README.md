# Section 3 : Quais et rampes de chargement

- [Article R4214-18](article-r4214-18.md)
- [Article R4214-19](article-r4214-19.md)
- [Article R4214-20](article-r4214-20.md)
- [Article R4214-21](article-r4214-21.md)
