# Chapitre IV : Sécurité des lieux de travail

- [Section 1 : Caractéristiques des bâtiments](section-1)
- [Section 2 : Voies de circulation et accès](section-2)
- [Section 3 : Quais et rampes de chargement](section-3)
- [Section 4 : Aménagement des lieux et postes de travail](section-4)
- [Section 5 : Accessibilité des lieux de travail aux travailleurs handicapés](section-5)
