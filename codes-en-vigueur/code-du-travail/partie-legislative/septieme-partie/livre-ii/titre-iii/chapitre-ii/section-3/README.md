# Section 3 : Dispositions d'application.

- [Article L7232-7](article-l7232-7.md)
- [Article L7232-8](article-l7232-8.md)
- [Article L7232-9](article-l7232-9.md)
