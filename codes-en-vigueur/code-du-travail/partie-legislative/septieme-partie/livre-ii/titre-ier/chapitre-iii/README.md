# Chapitre III : Congés payés.

- [Article L7213-1](article-l7213-1.md)
- [Article L7213-2](article-l7213-2.md)
- [Article L7213-3](article-l7213-3.md)
- [Article L7213-4](article-l7213-4.md)
- [Article L7213-5](article-l7213-5.md)
- [Article L7213-6](article-l7213-6.md)
- [Article L7213-7](article-l7213-7.md)
