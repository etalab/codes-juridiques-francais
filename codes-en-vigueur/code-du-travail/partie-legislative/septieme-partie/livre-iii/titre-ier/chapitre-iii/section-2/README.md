# Section 2 : Conclusion et exécution du contrat de travail

- [Sous-section 1 : Période d'essai.](sous-section-1)
- [Sous-section 2 : Clause d'exclusivité.](sous-section-2)
