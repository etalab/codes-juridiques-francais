# Section 2 : Activité d'entrepreneur de spectacles vivants à titre occasionnel.

- [Article L7122-19](article-l7122-19.md)
- [Article L7122-20](article-l7122-20.md)
- [Article L7122-21](article-l7122-21.md)
