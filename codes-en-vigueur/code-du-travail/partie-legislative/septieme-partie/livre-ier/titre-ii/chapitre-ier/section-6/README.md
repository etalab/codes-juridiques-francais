# Section 6 : Dispositions pénales

- [Article L7121-15](article-l7121-15.md)
- [Article L7121-16](article-l7121-16.md)
- [Article L7121-17](article-l7121-17.md)
