# Section 4 : Interdictions.

- [Article L7124-13](article-l7124-13.md)
- [Article L7124-14](article-l7124-14.md)
- [Article L7124-15](article-l7124-15.md)
- [Article L7124-16](article-l7124-16.md)
- [Article L7124-17](article-l7124-17.md)
- [Article L7124-18](article-l7124-18.md)
- [Article L7124-19](article-l7124-19.md)
- [Article L7124-20](article-l7124-20.md)
