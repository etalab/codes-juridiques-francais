# Article L6523-1

Dans chacun des     départements d'outre-mer, à Saint-Barthélemy et à Saint-Martin, les contributions mentionnées au chapitre Ier du titre III du livre III de la présente partie ne peuvent être collectées  que par des organismes agréés à compétence interprofessionnelle, à l'exception des contributions des entreprises relevant du champ professionnel des organismes collecteurs paritaires agréés autorisés à collecter dans ces territoires par arrêté conjoint des ministres chargés de la formation professionnelle et de l'outre-mer.

Un décret détermine les modalités et les critères selon lesquels cette autorisation est accordée, en fonction notamment de la collecte et des services de proximité aux entreprises que les organismes collecteurs paritaires agréés sont en mesure d'assurer sur les territoires concernés.
