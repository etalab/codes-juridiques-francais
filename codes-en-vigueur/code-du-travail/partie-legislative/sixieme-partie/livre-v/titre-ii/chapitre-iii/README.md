# Chapitre III : La formation professionnelle continue

- [Section 1 : Financement de la formation professionnelle continue.](section-1)
- [Section 2 : Parrainage.](section-2)
- [Section 3 : Stagiaire de la formation professionnelle.](section-3)
- [Section 3 bis : Comité régional de l'emploi, de la formation et de l'orientation professionnelles](section-3-bis)
- [Section 3 ter : Comité paritaire interprofessionnel régional pour l'emploi et la formation](section-3-ter)
- [Section 4 : Dispositions d'adaptation.](section-4)
