# Article L6423-2

<div align="left">Le comité régional de l'emploi, de la formation et de l'orientation professionnelles et le Conseil national de l'emploi, de la formation et de l'orientation professionnelles assurent le suivi statistique des parcours de validation des acquis de l'expérience, selon des modalités définies par décret en Conseil d'Etat. <br/>
</div>
