# Section 2 : Création de sections d'apprentissage et d'unités de formation par apprentissage.

- [Article L6232-6](article-l6232-6.md)
- [Article L6232-7](article-l6232-7.md)
- [Article L6232-8](article-l6232-8.md)
- [Article L6232-9](article-l6232-9.md)
- [Article L6232-10](article-l6232-10.md)
