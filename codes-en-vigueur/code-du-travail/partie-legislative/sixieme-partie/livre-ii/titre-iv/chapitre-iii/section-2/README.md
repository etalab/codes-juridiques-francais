# Section 2 : Cotisations dues au titre de l'emploi des apprentis.

- [Article L6243-2](article-l6243-2.md)
- [Article L6243-3](article-l6243-3.md)
