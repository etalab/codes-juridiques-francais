# Section 2 : Suspension de l'exécution du contrat et interdiction de recrutement.

- [Article L6225-4](article-l6225-4.md)
- [Article L6225-5](article-l6225-5.md)
- [Article L6225-6](article-l6225-6.md)
- [Article L6225-7](article-l6225-7.md)
