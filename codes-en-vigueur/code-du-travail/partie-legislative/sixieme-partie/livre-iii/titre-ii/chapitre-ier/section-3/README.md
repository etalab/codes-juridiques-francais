# Section 3 : Actions de formation du salarié occupant un emploi saisonnier.

- [Article L6321-13](article-l6321-13.md)
- [Article L6321-14](article-l6321-14.md)
- [Article L6321-15](article-l6321-15.md)
