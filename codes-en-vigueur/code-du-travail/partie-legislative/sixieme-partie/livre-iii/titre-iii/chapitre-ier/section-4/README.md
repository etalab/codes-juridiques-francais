# Section 4 : Dispositions applicables à certaines catégories d'employeurs

- [Sous-section 1 : Employeurs du bâtiment et des travaux publics.](sous-section-1)
- [Sous-section 2 : Travailleurs indépendants, membres des professions libérales et professions non salariées.](sous-section-2)
- [Sous-section 3 : Employeurs occupant des salariés intermittents du spectacle.](sous-section-3)
- [Sous-section 4 : Particuliers employeurs.](sous-section-4)
- [Sous-section 5 : Employeurs de la pêche maritime et des cultures marines.](sous-section-5)
- [Sous-section 6 : Artistes auteurs.](sous-section-6)
