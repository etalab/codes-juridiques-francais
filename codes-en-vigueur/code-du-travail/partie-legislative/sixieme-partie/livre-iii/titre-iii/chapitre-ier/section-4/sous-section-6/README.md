# Sous-section 6 : Artistes auteurs.

- [Article L6331-65](article-l6331-65.md)
- [Article L6331-66](article-l6331-66.md)
- [Article L6331-67](article-l6331-67.md)
- [Article L6331-68](article-l6331-68.md)
