# Sous-section 3 : Employeurs occupant des salariés intermittents du spectacle.

- [Article L6331-55](article-l6331-55.md)
- [Article L6331-56](article-l6331-56.md)
