# Section 1 : Dispositions générales

- [Sous-section 1 : Agrément.](sous-section-1)
- [Sous-section 2 : Gestion des fonds.](sous-section-2)
- [Sous-section 3 : Dispositions d'application.](sous-section-3)
