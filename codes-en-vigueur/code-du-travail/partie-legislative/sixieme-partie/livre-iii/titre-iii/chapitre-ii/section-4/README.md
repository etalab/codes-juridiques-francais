# Section 4 : Fonds paritaire de sécurisation des parcours professionnels.

- [Article L6332-18](article-l6332-18.md)
- [Article L6332-19](article-l6332-19.md)
- [Article L6332-21](article-l6332-21.md)
- [Article L6332-22](article-l6332-22.md)
- [Article L6332-22-1](article-l6332-22-1.md)
- [Article L6332-22-2](article-l6332-22-2.md)
