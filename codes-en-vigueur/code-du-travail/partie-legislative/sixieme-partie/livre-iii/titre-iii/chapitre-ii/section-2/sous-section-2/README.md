# Sous-section 2 : Fonds d'assurance-formation de non-salariés.

- [Article L6332-9](article-l6332-9.md)
- [Article L6332-10](article-l6332-10.md)
- [Article L6332-11](article-l6332-11.md)
- [Article L6332-12](article-l6332-12.md)
