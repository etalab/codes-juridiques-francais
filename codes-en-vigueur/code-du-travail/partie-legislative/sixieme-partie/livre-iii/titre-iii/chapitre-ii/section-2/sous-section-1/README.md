# Sous-section 1 : Fonds d'assurance-formation de salariés.

- [Article L6332-7](article-l6332-7.md)
- [Article L6332-8](article-l6332-8.md)
