# Section 1 : Financement des stages rémunérés par l'Etat ou la région.

- [Article L6341-1](article-l6341-1.md)
- [Article L6341-2](article-l6341-2.md)
- [Article L6341-3](article-l6341-3.md)
- [Article L6341-4](article-l6341-4.md)
- [Article L6341-5](article-l6341-5.md)
- [Article L6341-6](article-l6341-6.md)
