# Chapitre II : Protection sociale du stagiaire

- [Section 1 : Affiliation à un régime de sécurité sociale.](section-1)
- [Section 2 : Prise en charge des cotisations par l'Etat ou la région.](section-2)
- [Section 3 : Droits aux prestations.](section-3)
- [Section 4 : Règlement des litiges.](section-4)
- [Section 5 : Dispositions d'application.](section-5)
