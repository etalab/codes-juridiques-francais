# Chapitre II : Durée du travail.

- [Article L3162-1](article-l3162-1.md)
- [Article L3162-2](article-l3162-2.md)
- [Article L3162-3](article-l3162-3.md)
