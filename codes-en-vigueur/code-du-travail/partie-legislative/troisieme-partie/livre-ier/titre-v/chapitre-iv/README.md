# Chapitre IV : Garantie et liquidation des droits

- [Article L3154-1](article-l3154-1.md)
- [Article L3154-2](article-l3154-2.md)
- [Article L3154-3](article-l3154-3.md)
