# Article L3152-1

Le compte épargne-temps peut être institué par convention ou accord d'entreprise ou d'établissement ou, à défaut, par une convention ou un accord de branche.
