# Section 1 : Répartition des horaires sur une période supérieure à la semaine et au plus égale à l'année

- [Article L3122-1](article-l3122-1.md)
- [Article L3122-2](article-l3122-2.md)
- [Article L3122-3](article-l3122-3.md)
- [Article L3122-4](article-l3122-4.md)
- [Article L3122-5](article-l3122-5.md)
- [Article L3122-6](article-l3122-6.md)
