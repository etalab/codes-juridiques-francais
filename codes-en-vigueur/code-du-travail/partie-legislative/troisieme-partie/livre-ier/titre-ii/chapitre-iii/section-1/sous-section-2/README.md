# Sous-section 2 : Mise en oeuvre à l'initiative de l'employeur.

- [Article L3123-2](article-l3123-2.md)
- [Article L3123-3](article-l3123-3.md)
- [Article L3123-4](article-l3123-4.md)
