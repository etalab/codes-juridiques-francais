# Section 3 : Durées maximales de travail

- [Sous-section 1 : Temps de pause.](sous-section-1)
- [Sous-section 2 : Durée quotidienne maximale.](sous-section-2)
- [Sous-section 3 : Durées hebdomadaires maximales.](sous-section-3)
