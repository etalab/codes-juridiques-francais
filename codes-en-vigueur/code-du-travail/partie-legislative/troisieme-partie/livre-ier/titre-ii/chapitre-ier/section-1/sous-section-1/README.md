# Sous-section 1 : Travail effectif.

- [Article L3121-1](article-l3121-1.md)
- [Article L3121-2](article-l3121-2.md)
- [Article L3121-3](article-l3121-3.md)
- [Article L3121-4](article-l3121-4.md)
