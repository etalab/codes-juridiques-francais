# Section 5 : Dispositions d'application.

- [Article L3121-52](article-l3121-52.md)
- [Article L3121-53](article-l3121-53.md)
- [Article L3121-54](article-l3121-54.md)
