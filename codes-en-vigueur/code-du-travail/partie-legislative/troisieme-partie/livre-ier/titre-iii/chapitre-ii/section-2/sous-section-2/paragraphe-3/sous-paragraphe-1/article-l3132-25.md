# Article L3132-25

Sans préjudice des dispositions de l'article L. 3132-20, les établissements de vente au détail situés dans les communes d'intérêt touristique ou thermales et dans les zones touristiques d'affluence exceptionnelle ou d'animation culturelle permanente peuvent, de droit, donner le repos hebdomadaire par roulement pour tout ou partie du personnel.

La liste des communes d'intérêt touristique ou thermales intéressées et le périmètre des zones touristiques d'affluence exceptionnelle ou d'animation culturelle permanente sont établis par le préfet sur proposition de l'autorité administrative visée au premier alinéa de l'article L. 3132-26 [Dispositions résultant de la décision du Conseil constitutionnel n° 2009-588 DC du 6 août 2009], après avis du comité départemental du tourisme, des syndicats d'employeurs et de salariés intéressés, ainsi que des communautés de communes, des communautés d'agglomération, des métropoles et des communautés urbaines, lorsqu'elles existent.

Un décret en Conseil d'Etat détermine les modalités d'application du présent article.
