# Article L3142-33

La durée du congé de solidarité internationale et la durée cumulée de plusieurs de ces congés pris de façon continue ne peuvent excéder six mois.
