# Paragraphe 3 : Dispositions communes au congé pour la création d'entreprise et au congé sabbatique

- [Sous-paragraphe 1 : Possibilités de report ou de refus du congé.](sous-paragraphe-1)
- [Sous-paragraphe 2 : Report de congés payés.](sous-paragraphe-2)
