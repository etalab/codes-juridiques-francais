# Paragraphe 4 : Dispositions diverses.

- [Article L3142-105](article-l3142-105.md)
- [Article L3142-106](article-l3142-106.md)
- [Article L3142-107](article-l3142-107.md)
