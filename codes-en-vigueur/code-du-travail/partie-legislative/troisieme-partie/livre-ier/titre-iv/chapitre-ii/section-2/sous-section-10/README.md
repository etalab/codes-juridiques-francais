# Sous-section 10 : Congé et période de travail à temps partiel pour la création ou la reprise d'entreprise et congé sabbatique

- [Paragraphe 1 : Congé et période de travail à temps partiel pour la création ou la reprise d'entreprise.](paragraphe-1)
- [Paragraphe 2 : Congé sabbatique.](paragraphe-2)
- [Paragraphe 3 : Dispositions communes au congé pour la création d'entreprise et au congé sabbatique](paragraphe-3)
- [Paragraphe 4 : Dispositions diverses.](paragraphe-4)
