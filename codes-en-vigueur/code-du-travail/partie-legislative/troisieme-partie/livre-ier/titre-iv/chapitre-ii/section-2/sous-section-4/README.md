# Sous-section 4 : Congé pour catastrophe naturelle.

- [Article L3142-41](article-l3142-41.md)
- [Article L3142-42](article-l3142-42.md)
