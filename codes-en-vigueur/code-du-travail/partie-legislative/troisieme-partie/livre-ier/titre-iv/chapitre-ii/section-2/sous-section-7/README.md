# Sous-section 7 : Congé de représentation.

- [Article L3142-51](article-l3142-51.md)
- [Article L3142-52](article-l3142-52.md)
- [Article L3142-53](article-l3142-53.md)
- [Article L3142-54](article-l3142-54.md)
- [Article L3142-55](article-l3142-55.md)
