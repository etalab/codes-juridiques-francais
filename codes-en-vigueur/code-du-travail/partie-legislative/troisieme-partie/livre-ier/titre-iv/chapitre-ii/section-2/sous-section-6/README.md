# Sous-section 6 : Congé mutualiste de formation.

- [Article L3142-47](article-l3142-47.md)
- [Article L3142-48](article-l3142-48.md)
- [Article L3142-49](article-l3142-49.md)
- [Article L3142-50](article-l3142-50.md)
