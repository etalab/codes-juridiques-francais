# Section 2 : Rémunération mensuelle minimale

- [Sous-section 1 : Dispositions générales.](sous-section-1)
- [Sous-section 2 : Modalités de fixation.](sous-section-2)
- [Sous-section 3 : Allocation complémentaire.](sous-section-3)
