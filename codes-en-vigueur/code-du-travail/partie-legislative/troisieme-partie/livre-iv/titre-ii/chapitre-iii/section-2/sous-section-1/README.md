# Sous-section 1 : Dispositions générales.

- [Article L3423-5](article-l3423-5.md)
- [Article L3423-6](article-l3423-6.md)
