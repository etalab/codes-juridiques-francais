# Section 2 : Répartition de l'intéressement.

- [Article L3314-5](article-l3314-5.md)
- [Article L3314-6](article-l3314-6.md)
- [Article L3314-7](article-l3314-7.md)
