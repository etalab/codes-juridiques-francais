# Section 4 : Information des salariés.

- [Article L3341-6](article-l3341-6.md)
- [Article L3341-7](article-l3341-7.md)
- [Article L3341-8](article-l3341-8.md)
