# Section 2 : Formation économique, financière et juridique des représentants des salariés.

- [Article L3341-2](article-l3341-2.md)
- [Article L3341-3](article-l3341-3.md)
- [Article L3341-4](article-l3341-4.md)
