# Section 5 : Indisponibilité des sommes, déblocage anticipé et liquidation.

- [Article L3332-25](article-l3332-25.md)
- [Article L3332-26](article-l3332-26.md)
