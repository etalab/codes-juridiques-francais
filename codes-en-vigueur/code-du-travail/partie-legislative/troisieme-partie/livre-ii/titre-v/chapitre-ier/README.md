# Chapitre Ier : Retenues.

- [Article L3251-1](article-l3251-1.md)
- [Article L3251-2](article-l3251-2.md)
- [Article L3251-3](article-l3251-3.md)
- [Article L3251-4](article-l3251-4.md)
