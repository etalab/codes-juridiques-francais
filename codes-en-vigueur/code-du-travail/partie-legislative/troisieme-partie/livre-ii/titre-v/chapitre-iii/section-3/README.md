# Section 3 : Privilèges spéciaux.

- [Article L3253-22](article-l3253-22.md)
- [Article L3253-23](article-l3253-23.md)
