# Chapitre II : Titres-restaurant

- [Section 1 : Emission.](section-1)
- [Section 2 : Utilisation.](section-2)
- [Section 3 : Exonérations.](section-3)
- [Section 4 : Dispositions d'application.](section-4)
