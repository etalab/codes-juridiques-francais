# Sous-section 1 : Garantie du pouvoir d'achat des salariés.

- [Article L3231-4](article-l3231-4.md)
- [Article L3231-5](article-l3231-5.md)
