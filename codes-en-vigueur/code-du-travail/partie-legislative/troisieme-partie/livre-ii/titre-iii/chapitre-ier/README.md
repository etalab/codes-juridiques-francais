# Chapitre Ier : Salaire minimum interprofessionnel de croissance

- [Section 1 : Champ d'application.](section-1)
- [Section 2 : Principes.](section-2)
- [Section 3 : Modalités de fixation](section-3)
- [Section 4 : Minimum garanti.](section-4)
