# Chapitre II : Droits du salarié étranger.

- [Article L 8252-4](article-l-8252-4.md)
- [Article L8252-1](article-l8252-1.md)
- [Article L8252-2](article-l8252-2.md)
- [Article L8252-3](article-l8252-3.md)
