# Titre VIII : VIGILANCE DU DONNEUR D'ORDRE EN MATIÈRE D'APPLICATION DE LA LÉGISLATION DU TRAVAIL.

- [Chapitre unique : Obligation de vigilance et responsabilité du donneur d'ordre.](chapitre-unique)
