# Chapitre III : Droits des salariés et actions en justice.

- [Section 1 : Droits des salariés.](section-1)
- [Section 2 : Actions en justice.](section-2)
