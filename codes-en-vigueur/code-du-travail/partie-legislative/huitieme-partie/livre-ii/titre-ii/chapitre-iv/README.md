# Chapitre IV : Dispositions pénales.

- [Article L8224-1](article-l8224-1.md)
- [Article L8224-2](article-l8224-2.md)
- [Article L8224-3](article-l8224-3.md)
- [Article L8224-4](article-l8224-4.md)
- [Article L8224-5](article-l8224-5.md)
- [Article L8224-5-1](article-l8224-5-1.md)
- [Article L8224-6](article-l8224-6.md)
