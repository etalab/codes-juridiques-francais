# Chapitre II : Sanctions administratives.

- [Article L8272-1](article-l8272-1.md)
- [Article L8272-2](article-l8272-2.md)
- [Article L8272-3](article-l8272-3.md)
- [Article L8272-4](article-l8272-4.md)
- [Article L8272-5](article-l8272-5.md)
