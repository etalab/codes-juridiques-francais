# TITRE Ier : COMPÉTENCES ET MOYENS D'INTERVENTION

- [Chapitre II : Compétence des agents](chapitre-ii)
- [Chapitre III : Prérogatives et moyens d'intervention](chapitre-iii)
- [Chapitre IV : Dispositions pénales.](chapitre-iv)
