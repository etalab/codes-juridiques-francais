# Chapitre III : Appui à l'inspection du travail

- [Section 1 : Médecin inspecteur du travail.](section-1)
- [Section 2 : Ingénieurs de prévention.](section-2)
- [Section 3 : Missions spéciales temporaires confiées à des médecins et ingénieurs.](section-3)
