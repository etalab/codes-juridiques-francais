# Section 1 : Médecin inspecteur du travail.

- [Article L8123-1](article-l8123-1.md)
- [Article L8123-2](article-l8123-2.md)
- [Article L8123-3](article-l8123-3.md)
