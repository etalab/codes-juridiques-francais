# Chapitre IV : Régimes particuliers

- [Section 1 : Dispositions particulières à certains salariés.](section-1)
- [Section 2 : Entreprises du bâtiment et des travaux publics privées d'emploi par suite d'intempéries.](section-2)
- [Section 3 : Professions de la production cinématographique, de l'audiovisuel ou du spectacle.](section-3)
