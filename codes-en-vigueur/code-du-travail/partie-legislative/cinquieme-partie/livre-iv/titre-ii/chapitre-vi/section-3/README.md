# Section 3 : Pénalité administrative.

- [Article L5426-5](article-l5426-5.md)
- [Article L5426-6](article-l5426-6.md)
- [Article L5426-7](article-l5426-7.md)
- [Article L5426-8](article-l5426-8.md)
