# Chapitre VII : Organismes gestionnaires du régime d'assurance chômage

- [Section 1 : Gestion confiée à des organismes de droit privé par voie d'accord ou de convention.](section-1)
- [Section 2 : Gestion confiée à un établissement public en l'absence de convention.](section-2)
- [Section 3 : Dispositions communes.](section-3)
