# Section 1 : Gestion confiée à des organismes de droit privé par voie d'accord ou de convention.

- [Article L5427-1](article-l5427-1.md)
- [Article L5427-2](article-l5427-2.md)
- [Article L5427-3](article-l5427-3.md)
- [Article L5427-4](article-l5427-4.md)
- [Article L5427-5](article-l5427-5.md)
- [Article L5427-6](article-l5427-6.md)
