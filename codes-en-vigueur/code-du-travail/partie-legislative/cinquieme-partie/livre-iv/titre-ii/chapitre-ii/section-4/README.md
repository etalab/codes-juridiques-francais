# Section 4 : Modalités de recouvrement et de contrôle des contributions.

- [Article L5422-15](article-l5422-15.md)
- [Article L5422-16](article-l5422-16.md)
