# Chapitre Ier : Aides à la création ou à la reprise d'entreprise

- [Section 1 : Exonération de charges sociales.](section-1)
- [Section 2 : Avance remboursable.](section-2)
- [Section 3 : Maintien d'allocations.](section-3)
- [Section 4 : Financement d'actions de conseil, de formation et d'accompagnement.](section-4)
- [Section 5 : Dispositions d'application.](section-5)
