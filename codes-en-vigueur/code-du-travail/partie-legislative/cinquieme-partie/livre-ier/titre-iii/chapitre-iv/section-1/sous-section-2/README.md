# Sous-section 2 : Convention.

- [Article L5134-3](article-l5134-3.md)
- [Article L5134-4](article-l5134-4.md)
- [Article L5134-5](article-l5134-5.md)
- [Article L5134-6](article-l5134-6.md)
- [Article L5134-7](article-l5134-7.md)
- [Article L5134-8](article-l5134-8.md)
