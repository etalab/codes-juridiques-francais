# Article L5134-13

Le contrat de travail peut être suspendu à l'initiative du salarié avec l'accord de l'employeur afin de lui permettre d'accomplir la période d'essai afférente à une offre d'emploi.

En cas d'embauche à l'issue de cette période d'essai, le contrat est rompu sans préavis.
