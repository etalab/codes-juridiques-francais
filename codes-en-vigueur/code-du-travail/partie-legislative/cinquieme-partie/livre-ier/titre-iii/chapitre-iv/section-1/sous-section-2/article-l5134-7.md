# Article L5134-7

Le contenu et la durée des conventions, les conditions dans lesquelles leur exécution est suivie et contrôlée ainsi que les modalités de dénonciation de la convention en cas de non-respect de celle-ci sont déterminés par décret.
