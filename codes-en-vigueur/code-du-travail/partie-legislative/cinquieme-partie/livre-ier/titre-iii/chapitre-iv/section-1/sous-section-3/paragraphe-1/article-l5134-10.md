# Article L5134-10

Le contrat emploi-jeune ne peut être conclu par les services de l'Etat.
