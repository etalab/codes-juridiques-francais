# Paragraphe 1 : Dispositions communes.

- [Article L5134-9](article-l5134-9.md)
- [Article L5134-10](article-l5134-10.md)
- [Article L5134-11](article-l5134-11.md)
- [Article L5134-12](article-l5134-12.md)
- [Article L5134-13](article-l5134-13.md)
