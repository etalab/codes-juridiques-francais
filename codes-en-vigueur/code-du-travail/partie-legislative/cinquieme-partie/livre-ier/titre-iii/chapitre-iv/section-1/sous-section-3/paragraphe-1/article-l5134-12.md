# Article L5134-12

Lorsqu'à l'issue du contrat emploi-jeune le contrat se poursuit, l'emploi pour lequel le contrat emploi-jeune a été conclu est intégré dans les grilles de classification des conventions ou accords collectifs dont relève l'activité lorsque ces conventions ou accords existent.
