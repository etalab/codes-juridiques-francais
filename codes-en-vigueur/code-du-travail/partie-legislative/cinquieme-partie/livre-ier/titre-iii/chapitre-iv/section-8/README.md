# Section 8 : Emploi d'avenir

- [Sous-section 1 : Dispositions générales](sous-section-1)
- [Sous-section 2 : Aide à l'insertion professionnelle](sous-section-2)
- [Sous-section 3 : Contrat de travail](sous-section-3)
- [Sous-section 4 : Reconnaissance des compétences acquises](sous-section-4)
- [Sous-section 5 : Dispositions d'application](sous-section-5)
