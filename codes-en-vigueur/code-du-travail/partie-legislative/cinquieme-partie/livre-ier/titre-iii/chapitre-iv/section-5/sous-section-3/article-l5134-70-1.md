# Article L5134-70-1

La durée hebdomadaire du travail d'un salarié titulaire d'un contrat de travail associé à une aide à l'insertion professionnelle au titre d'un  contrat initiative-emploi ne peut être inférieure à vingt heures.
