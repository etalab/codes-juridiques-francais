# Section 3 : Mise en oeuvre des actions d'insertion par l'activité économique

- [Sous-section 1 : Structures d'insertion par l'activité économique.](sous-section-1)
- [Sous-section 2 : Entreprises d'insertion.](sous-section-2)
- [Sous-section 3 : Entreprises de travail temporaire d'insertion.](sous-section-3)
- [Sous-section 4 : Associations intermédiaires.](sous-section-4)
- [Sous-section 5 : Ateliers et chantiers d'insertion.](sous-section-5)
- [Sous-section 6 : Groupes économiques solidaires.](sous-section-6)
