# Sous-section 2 : Contrat d'insertion dans la vie sociale.

- [Article L5131-4](article-l5131-4.md)
- [Article L5131-5](article-l5131-5.md)
- [Article L5131-6](article-l5131-6.md)
