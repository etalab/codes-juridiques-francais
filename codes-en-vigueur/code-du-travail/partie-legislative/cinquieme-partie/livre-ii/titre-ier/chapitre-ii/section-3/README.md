# Section 3 : Modalités de mise en oeuvre de l'obligation

- [Sous-section 1 : Mise en oeuvre partielle.](sous-section-1)
- [Sous-section 2 : Mise en oeuvre par application d'un accord.](sous-section-2)
- [Sous-section 3 : Mise en oeuvre par le versement d'une contribution annuelle.](sous-section-3)
- [Sous-section 4 : Sanction administrative.](sous-section-4)
