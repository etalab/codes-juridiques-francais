# Article L5212-6

L'employeur peut s'acquitter partiellement de l'obligation d'emploi en passant des contrats de fournitures de sous-traitance ou de prestations de services avec :

1° Soit des entreprises adaptées ;

2° Soit des centres de distribution de travail à domicile ;

3° Soit des établissements ou services d'aide par le travail.

Cet acquittement partiel est proportionnel au volume de travail fourni à ces ateliers, centres, établissements ou services.

Les modalités et les limites de cet acquittement partiel sont déterminées par voie réglementaire.
