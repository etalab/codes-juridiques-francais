# Section 4 : Bénéficiaires de l'obligation d'emploi

- [Sous-section 1 : Catégories de bénéficiaires.](sous-section-1)
- [Sous-section 2 : Calcul du nombre de bénéficiaires.](sous-section-2)
