# Sous-section 2 : Calcul du nombre de bénéficiaires.

- [Article L5212-14](article-l5212-14.md)
- [Article L5212-15](article-l5212-15.md)
