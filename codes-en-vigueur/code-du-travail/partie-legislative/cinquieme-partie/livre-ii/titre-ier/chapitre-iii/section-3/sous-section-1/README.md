# Sous-section 1 : Droits et garanties des travailleurs handicapés.

- [Article L5213-6](article-l5213-6.md)
- [Article L5213-7](article-l5213-7.md)
- [Article L5213-8](article-l5213-8.md)
- [Article L5213-9](article-l5213-9.md)
