# Article L5213-3

Tout travailleur handicapé peut bénéficier d'une réadaptation, d'une rééducation ou d'une formation professionnelle.
