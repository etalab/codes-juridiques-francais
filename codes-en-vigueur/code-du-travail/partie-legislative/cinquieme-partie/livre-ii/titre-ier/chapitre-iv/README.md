# Chapitre IV : Institutions et organismes concourant à l'insertion professionnelle des handicapés

- [Section 1 : Fonds de développement pour l'insertion professionnelle des handicapés.](section-1)
- [Section 1 bis : Organismes de placement spécialisés dans l'insertion professionnelle des personnes handicapées](section-1-bis)
- [Section 1  A : Pilotage des politiques en faveur de l'emploi des personnes handicapées](section-1-a)
- [Section 2 : Actions en justice.](section-2)
- [Section 3 : Dispositions d'application.](section-3)
