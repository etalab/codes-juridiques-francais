# Chapitre II : Interdictions.

- [Article L5222-1](article-l5222-1.md)
- [Article L5222-2](article-l5222-2.md)
