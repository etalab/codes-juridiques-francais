# Section 1 : Champ d'application.

- [Article L4111-1](article-l4111-1.md)
- [Article L4111-2](article-l4111-2.md)
- [Article L4111-3](article-l4111-3.md)
- [Article L4111-4](article-l4111-4.md)
- [Article L4111-5](article-l4111-5.md)
