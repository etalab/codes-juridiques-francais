# Section 2 : Utilisations du compte personnel
de prévention de la pénibilité

- [Sous-section 1 : Utilisation du compte pour la formation professionnelle](sous-section-1)
- [Sous-section 2 : Utilisation du compte pour le passage à temps partiel](sous-section-2)
- [Sous-section 3 : Utilisation du compte pour la retraite](sous-section-3)
- [Article L4162-4](article-l4162-4.md)
