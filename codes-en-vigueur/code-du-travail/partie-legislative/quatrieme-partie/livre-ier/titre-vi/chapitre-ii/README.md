# Chapitre II : Compte personnel de prévention de la pénibilité

- [Section 1 : Ouverture et abondement du compte personnel de prévention de la pénibilité](section-1)
- [Section 2 : Utilisations du compte personnel
de prévention de la pénibilité](section-2)
- [Section 3 : Gestion des comptes, contrôle et réclamations](section-3)
- [Section 4 : Financement](section-4)
- [Section 5 : Dispositions d'application](section-5)
