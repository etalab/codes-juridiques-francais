# Article L4162-3

Les points sont attribués au vu des expositions du salarié déclarées par l'employeur, sur la base de la fiche mentionnée à l'article L. 4161-1 du présent code, auprès de la caisse mentionnée aux articles L. 215-1 ou L. 222-1-1 du code de la sécurité sociale ou à l'article L. 723-2 du code rural et de la pêche maritime dont il relève.

Chaque année, l'employeur transmet au salarié une copie de la fiche mentionnée à l'article L. 4161-1 du présent code.

(1) Chaque année, l'employeur transmet une copie de cette fiche à la caisse mentionnée au premier alinéa du présent article.
