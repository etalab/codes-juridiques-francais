# Chapitre IV : Opérations de bâtiment et de génie civil.

- [Article L4744-1](article-l4744-1.md)
- [Article L4744-2](article-l4744-2.md)
- [Article L4744-3](article-l4744-3.md)
- [Article L4744-4](article-l4744-4.md)
- [Article L4744-5](article-l4744-5.md)
- [Article L4744-6](article-l4744-6.md)
- [Article L4744-7](article-l4744-7.md)
