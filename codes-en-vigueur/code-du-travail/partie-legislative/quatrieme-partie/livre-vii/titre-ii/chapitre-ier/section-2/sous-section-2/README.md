# Sous-section 2 : Mise en demeure préalable à l'arrêt temporaire d'activité.

- [Article L4721-8](article-l4721-8.md)
