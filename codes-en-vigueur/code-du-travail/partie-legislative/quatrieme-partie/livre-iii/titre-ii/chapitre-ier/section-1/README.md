# Section 1 : Principes.

- [Article L4321-1](article-l4321-1.md)
- [Article L4321-2](article-l4321-2.md)
- [Article L4321-3](article-l4321-3.md)
