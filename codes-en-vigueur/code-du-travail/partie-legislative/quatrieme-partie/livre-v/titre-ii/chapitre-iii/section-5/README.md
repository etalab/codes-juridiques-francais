# Section 5 : Comité élargi.

- [Article L4523-11](article-l4523-11.md)
- [Article L4523-12](article-l4523-12.md)
- [Article L4523-13](article-l4523-13.md)
- [Article L4523-14](article-l4523-14.md)
- [Article L4523-15](article-l4523-15.md)
- [Article L4523-16](article-l4523-16.md)
- [Article L4523-17](article-l4523-17.md)
