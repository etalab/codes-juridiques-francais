# Sous-section 1 : Déclaration des substances et préparations

- [Paragraphe 1 : Mise sur le marché.](paragraphe-1)
- [Paragraphe 2 : Information des autorités.](paragraphe-2)
- [Paragraphe 3 : Exceptions.](paragraphe-3)
