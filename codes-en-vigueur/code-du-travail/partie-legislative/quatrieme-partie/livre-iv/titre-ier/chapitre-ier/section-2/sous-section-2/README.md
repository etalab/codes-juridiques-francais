# Sous-section 2 : Protection des utilisateurs et acheteurs

- [Paragraphe 1 : Information des utilisateurs.](paragraphe-1)
- [Paragraphe 2 : Résolution de la vente.](paragraphe-2)
