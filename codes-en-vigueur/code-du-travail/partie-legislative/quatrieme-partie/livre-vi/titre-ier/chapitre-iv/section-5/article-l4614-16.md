# Article L4614-16

La charge financière de la formation des représentants du personnel au comité d'hygiène, de sécurité et des conditions de travail incombe à l'employeur dans des conditions et limites déterminées par voie réglementaire.
