# Section 1 : Présidence et modalités de délibération.

- [Article L4614-1](article-l4614-1.md)
- [Article L4614-2](article-l4614-2.md)
