# Article L4614-2

Les décisions du comité d'hygiène, de sécurité et des conditions de travail portant sur ses modalités de fonctionnement et l'organisation de ses travaux sont adoptées à la majorité des membres présents, conformément à la procédure définie au premier alinéa de l'article L. 2325-18.

Il en est de même des résolutions que le comité adopte.
