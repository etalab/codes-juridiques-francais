# Section 1 : Principes.

- [Article L4622-1](article-l4622-1.md)
- [Article L4622-2](article-l4622-2.md)
- [Article L4622-3](article-l4622-3.md)
- [Article L4622-4](article-l4622-4.md)
- [Article L4622-5](article-l4622-5.md)
- [Article L4622-6](article-l4622-6.md)
