# Article L4622-5

Selon l'importance des entreprises, les services de santé au travail peuvent être propres à une seule entreprise ou communs à plusieurs.
