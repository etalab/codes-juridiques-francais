# Chapitre III : Délégué syndical

- [Section 1 : Conditions de désignation](section-1)
- [Section 2 : Mandat.](section-2)
- [Section 3 : Exercice des fonctions](section-3)
- [Section 4 : Attributions complémentaires dans les entreprises de moins de trois cents salariés.](section-4)
- [Section 5 : Conditions de désignation dérogatoire](section-5)
