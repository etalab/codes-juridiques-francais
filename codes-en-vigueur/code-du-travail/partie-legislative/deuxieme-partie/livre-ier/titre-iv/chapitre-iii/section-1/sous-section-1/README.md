# Sous-section 1 : Conditions d'âge et d'ancienneté.

- [Article L2143-1](article-l2143-1.md)
- [Article L2143-2](article-l2143-2.md)
