# Section 3 : Exercice des fonctions

- [Sous-section 1 : Heures de délégation.](sous-section-1)
- [Sous-section 2 : Déplacements et circulation.](sous-section-2)
- [Sous-section 3 : Secret professionnel.](sous-section-3)
