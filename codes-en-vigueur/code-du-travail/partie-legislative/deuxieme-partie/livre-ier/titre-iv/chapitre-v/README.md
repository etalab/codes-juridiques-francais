# Chapitre V : Formation économique, sociale et syndicale des salariés appelés à exercer des fonctions syndicales.

- [Article L2145-1](article-l2145-1.md)
- [Article L2145-2](article-l2145-2.md)
- [Article L2145-3](article-l2145-3.md)
- [Article L2145-4](article-l2145-4.md)
