# Chapitre V : Ressources et moyens

- [Section 1 : Certification et publicité des comptes   des organisations syndicales et professionnelles](section-1)
- [Section 2 : Mise à disposition des salariés auprès des organisations syndicales](section-2)
- [Section 3 : Financement mutualisé des organisations syndicales de salariés et des organisations professionnelles d'employeurs](section-3)
