# Section 3 : Financement mutualisé des organisations syndicales de salariés et des organisations professionnelles d'employeurs

- [Article L2135-9](article-l2135-9.md)
- [Article L2135-10](article-l2135-10.md)
- [Article L2135-11](article-l2135-11.md)
- [Article L2135-12](article-l2135-12.md)
- [Article L2135-13](article-l2135-13.md)
- [Article L2135-14](article-l2135-14.md)
- [Article L2135-15](article-l2135-15.md)
- [Article L2135-16](article-l2135-16.md)
- [Article L2135-17](article-l2135-17.md)
- [Article L2135-18](article-l2135-18.md)
