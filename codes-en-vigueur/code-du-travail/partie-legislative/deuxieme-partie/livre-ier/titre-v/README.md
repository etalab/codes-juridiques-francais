# TITRE V : REPRÉSENTATIVITÉ PATRONALE

- [Chapitre Ier : Critères de représentativité](chapitre-ier)
- [Chapitre II : Organisations professionnelles d'employeurs représentatives](chapitre-ii)
