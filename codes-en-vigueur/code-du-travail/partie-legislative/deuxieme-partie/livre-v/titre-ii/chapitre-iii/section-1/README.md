# Section 1 : Désignation du médiateur.

- [Article L2523-1](article-l2523-1.md)
- [Article L2523-2](article-l2523-2.md)
- [Article L2523-3](article-l2523-3.md)
