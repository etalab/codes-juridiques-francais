# Section 1 : Arbitre.

- [Article L2524-1](article-l2524-1.md)
- [Article L2524-2](article-l2524-2.md)
- [Article L2524-3](article-l2524-3.md)
- [Article L2524-4](article-l2524-4.md)
- [Article L2524-5](article-l2524-5.md)
- [Article L2524-6](article-l2524-6.md)
