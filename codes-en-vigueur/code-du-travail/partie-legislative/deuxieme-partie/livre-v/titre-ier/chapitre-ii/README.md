# Chapitre II : Dispositions particulières dans les services publics.

- [Article L2512-1](article-l2512-1.md)
- [Article L2512-2](article-l2512-2.md)
- [Article L2512-3](article-l2512-3.md)
- [Article L2512-4](article-l2512-4.md)
- [Article L2512-5](article-l2512-5.md)
