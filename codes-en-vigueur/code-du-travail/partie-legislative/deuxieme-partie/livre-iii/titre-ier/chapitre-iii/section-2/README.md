# Section 2 : Attributions particulières dans les entreprises de cinquante salariés et plus dépourvues de comité d'entreprise ou de comité d'hygiène, de sécurité et des conditions de travail.

- [Article L2313-13](article-l2313-13.md)
- [Article L2313-14](article-l2313-14.md)
- [Article L2313-15](article-l2313-15.md)
- [Article L2313-16](article-l2313-16.md)
