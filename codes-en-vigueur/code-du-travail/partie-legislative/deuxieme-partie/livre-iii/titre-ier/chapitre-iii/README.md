# Chapitre III : Attributions

- [Section 1 : Attributions générales.](section-1)
- [Section 2 : Attributions particulières dans les entreprises de cinquante salariés et plus dépourvues de comité d'entreprise ou de comité d'hygiène, de sécurité et des conditions de travail.](section-2)
