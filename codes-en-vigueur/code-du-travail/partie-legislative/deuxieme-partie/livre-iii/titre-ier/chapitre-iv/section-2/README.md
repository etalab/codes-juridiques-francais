# Section 2 : Election

- [Sous-section 1 : Organisation des élections.](sous-section-1)
- [Sous-section 2 : Collèges électoraux.](sous-section-2)
- [Sous-section 3 : Electorat et éligibilité.](sous-section-3)
- [Sous-section 4 : Mode de scrutin et résultat des élections.](sous-section-4)
- [Sous-section 5 : Contestations.](sous-section-5)
