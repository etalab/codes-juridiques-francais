# Sous-section 4 : Mode de scrutin et résultat des élections.

- [Article L2314-21](article-l2314-21.md)
- [Article L2314-22](article-l2314-22.md)
- [Article L2314-23](article-l2314-23.md)
- [Article L2314-24](article-l2314-24.md)
