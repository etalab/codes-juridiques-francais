# Section 1 : Groupe spécial de négociation.

- [Article L2342-1](article-l2342-1.md)
- [Article L2342-2](article-l2342-2.md)
- [Article L2342-3](article-l2342-3.md)
- [Article L2342-4](article-l2342-4.md)
- [Article L2342-5](article-l2342-5.md)
- [Article L2342-6](article-l2342-6.md)
- [Article L2342-7](article-l2342-7.md)
- [Article L2342-8](article-l2342-8.md)
