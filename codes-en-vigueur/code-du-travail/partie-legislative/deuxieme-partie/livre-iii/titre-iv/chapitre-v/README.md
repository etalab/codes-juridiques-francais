# Chapitre V : Suppression du comité.

- [Article L2345-1](article-l2345-1.md)
- [Article L2345-2](article-l2345-2.md)
