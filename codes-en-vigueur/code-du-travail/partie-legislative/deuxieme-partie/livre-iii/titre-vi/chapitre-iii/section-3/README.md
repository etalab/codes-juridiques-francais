# Section 3 : Dispositions applicables aux sociétés coopératives européennes non soumises initialement à la constitution du groupe spécial de négociation

- [Article L2363-12](article-l2363-12.md)
- [Article L2363-13](article-l2363-13.md)
- [Article L2363-14](article-l2363-14.md)
- [Article L2363-15](article-l2363-15.md)
- [Article L2363-16](article-l2363-16.md)
- [Article L2363-17](article-l2363-17.md)
