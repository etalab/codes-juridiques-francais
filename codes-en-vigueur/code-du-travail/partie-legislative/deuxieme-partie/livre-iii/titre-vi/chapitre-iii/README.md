# Chapitre III : Comité de la société coopérative européenne et participation des salariés en l'absence d'accord

- [Section 1 : Comité de la société coopérative européenne](section-1)
- [Section 2 : Participation des salariés au conseil d'administration et de surveillance](section-2)
- [Section 3 : Dispositions applicables aux sociétés coopératives européennes non soumises initialement à la constitution du groupe spécial de négociation](section-3)
- [Section 4 : Dispositions relatives à la participation des salariés à l'assemblée générale ou aux assemblées de section ou de branche](section-4)
