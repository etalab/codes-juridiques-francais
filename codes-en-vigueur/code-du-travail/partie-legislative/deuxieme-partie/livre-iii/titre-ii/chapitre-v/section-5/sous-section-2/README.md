# Sous-section 2 : Ordre du jour.

- [Article L2325-15](article-l2325-15.md)
- [Article L2325-16](article-l2325-16.md)
- [Article L2325-17](article-l2325-17.md)
