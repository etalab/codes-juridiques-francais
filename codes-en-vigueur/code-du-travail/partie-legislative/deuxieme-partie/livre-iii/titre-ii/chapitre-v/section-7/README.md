# Section 7 : Recours à un expert

- [Sous-section 1 : Experts rémunérés par l'entreprise](sous-section-1)
- [Sous-section 2 : Experts rémunérés par le comité d'entreprise.](sous-section-2)
- [Sous-section 3 : Obligation de secret et de discrétion des experts.](sous-section-3)
- [Sous-Section 4 :  Délai de l'expertise](sous-section-4)
