# Sous-section 2 : Collèges électoraux.

- [Article L2324-11](article-l2324-11.md)
- [Article L2324-12](article-l2324-12.md)
- [Article L2324-13](article-l2324-13.md)
