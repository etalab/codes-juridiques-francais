# Paragraphe 7 : Modification dans l'organisation économique ou juridique de l'entreprise.

- [Article L2323-19](article-l2323-19.md)
- [Article L2323-20](article-l2323-20.md)
