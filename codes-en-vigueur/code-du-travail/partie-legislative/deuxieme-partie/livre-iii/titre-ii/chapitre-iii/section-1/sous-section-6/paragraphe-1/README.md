# Paragraphe 1 : Rapports et information dans les entreprises de moins de trois cents salariés

- [Sous-paragraphe 1 : Information trimestrielle.](sous-paragraphe-1)
- [Sous-paragraphe 2 : Information annuelle.](sous-paragraphe-2)
