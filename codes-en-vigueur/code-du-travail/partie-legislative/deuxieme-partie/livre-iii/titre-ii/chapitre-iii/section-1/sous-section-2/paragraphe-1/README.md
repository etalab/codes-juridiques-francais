# Paragraphe 1 : Marche générale de l'entreprise.

- [Article L2323-6](article-l2323-6.md)
- [Article L2323-7](article-l2323-7.md)
- [Article L2323-7-1](article-l2323-7-1.md)
- [Article L2323-7-2](article-l2323-7-2.md)
- [Article L2323-7-3](article-l2323-7-3.md)
