# Article L2322-7

La suppression d'un comité d'entreprise est subordonnée à un accord entre l'employeur et l'ensemble des organisations syndicales représentatives.

A défaut d'accord, l'autorité administrative peut autoriser la suppression du comité d'entreprise en cas de réduction importante et durable du personnel ramenant l'effectif au-dessous de cinquante salariés.
