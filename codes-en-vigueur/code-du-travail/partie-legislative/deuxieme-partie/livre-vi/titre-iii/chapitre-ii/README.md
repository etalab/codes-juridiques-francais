# Chapitre II : Institutions représentatives du personnel

- [Section 1 : Comité central d'entreprise et comités d'établissement.](section-1)
- [Section 2 : Comité de groupe.](section-2)
