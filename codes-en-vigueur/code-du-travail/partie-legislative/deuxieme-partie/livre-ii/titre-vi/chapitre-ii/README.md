# Chapitre II : Effets de l'application des conventions et accords

- [Section 1 : Obligations d'exécution.](section-1)
- [Section 2 : Information et communication.](section-2)
- [Section 3 : Actions en justice.](section-3)
