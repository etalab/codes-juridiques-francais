# Sous-section 3 : Procédures d'extension et d'élargissement.

- [Article L2261-24](article-l2261-24.md)
- [Article L2261-25](article-l2261-25.md)
- [Article L2261-26](article-l2261-26.md)
- [Article L2261-27](article-l2261-27.md)
- [Article L2261-28](article-l2261-28.md)
- [Article L2261-29](article-l2261-29.md)
- [Article L2261-30](article-l2261-30.md)
- [Article L2261-31](article-l2261-31.md)
