# Chapitre Ier : Conditions d'applicabilité des conventions et accords

- [Section 1 : Date d'entrée en vigueur.](section-1)
- [Section 2 : Détermination de la convention collective applicable.](section-2)
- [Section 3 : Adhésion.](section-3)
- [Section 4 : Révision.](section-4)
- [Section 5 : Dénonciation](section-5)
- [Section 6 : Mise en cause.](section-6)
- [Section 7 : Extension et élargissement](section-7)
- [Section 8 : Restructuration des branches professionnelles](section-8)
