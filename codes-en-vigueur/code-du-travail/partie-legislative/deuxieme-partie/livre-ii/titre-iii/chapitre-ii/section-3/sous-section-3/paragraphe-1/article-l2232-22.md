# Article L2232-22

La validité des accords d'entreprise ou d'établissement négociés et conclus conformément à l'article L. 2232-21 est subordonnée à leur conclusion par des membres titulaires élus au comité d'entreprise ou, à défaut, des délégués du personnel titulaires représentant la majorité des suffrages exprimés lors des dernières élections professionnelles et à l'approbation par la commission paritaire de branche. La commission paritaire de branche contrôle que l'accord collectif n'enfreint pas les dispositions législatives, réglementaires ou conventionnelles applicables.

Si l'une des deux conditions n'est pas remplie, l'accord est réputé non écrit.

A défaut de stipulations différentes d'un accord de branche, la commission paritaire de branche comprend un représentant titulaire et un représentant suppléant de chaque organisation syndicale de salariés représentative dans la branche et un nombre égal de représentants des organisations professionnelles d'employeurs.
