# Article L2232-5

Le champ d'application territorial des conventions de branches et des accords professionnels peut être national, régional ou local.
