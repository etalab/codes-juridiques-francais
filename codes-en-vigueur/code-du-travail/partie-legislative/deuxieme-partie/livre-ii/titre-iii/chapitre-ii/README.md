# Chapitre II : Règles applicables à chaque niveau de négociation

- [Section 1 : Accords interprofessionnels.](section-1)
- [Section 2 : Conventions de branche et accords professionnels.](section-2)
- [Section 3 : Conventions et accords d'entreprise ou d'établissement](section-3)
- [Section 4 : Conventions ou accords de groupe.](section-4)
