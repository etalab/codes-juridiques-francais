# Chapitre II : Entreprises et établissements du secteur public.

- [Article L2282-1](article-l2282-1.md)
- [Article L2282-2](article-l2282-2.md)
- [Article L2282-3](article-l2282-3.md)
