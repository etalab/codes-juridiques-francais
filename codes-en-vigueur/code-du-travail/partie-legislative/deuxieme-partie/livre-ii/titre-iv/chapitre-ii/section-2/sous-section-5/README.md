# Sous-section 5 : Travailleurs handicapés.

- [Article L2242-13](article-l2242-13.md)
- [Article L2242-14](article-l2242-14.md)
