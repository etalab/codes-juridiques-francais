# Chapitre III : Dispositions pénales.

- [Article L2243-1](article-l2243-1.md)
- [Article L2243-2](article-l2243-2.md)
