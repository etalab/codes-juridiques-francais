# Section 4 : Dispositions communes à la négociation annuelle et à la négociation quinquennale.

- [Article L2241-9](article-l2241-9.md)
- [Article L2241-10](article-l2241-10.md)
- [Article L2241-11](article-l2241-11.md)
- [Article L2241-12](article-l2241-12.md)
