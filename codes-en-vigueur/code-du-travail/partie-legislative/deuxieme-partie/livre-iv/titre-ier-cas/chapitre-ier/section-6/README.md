# Section 6 : Licenciement d'un membre du groupe spécial de négociation, d'un représentant au comité de la société européenne, d'un représentant au comité de la société coopérative européenne ou d'un représentant au comité de la société issue d'une fusion transfrontalière.

- [Article L2411-12](article-l2411-12.md)
