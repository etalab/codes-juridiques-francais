# Article L1454-2

En cas de partage, l'affaire est renvoyée devant le même bureau de conciliation, le même bureau de jugement ou la même formation de référé, présidé par un juge du tribunal d'instance dans le ressort duquel est situé le siège du conseil de prud'hommes ou le juge d'instance désigné par le premier président en application du dernier alinéa. L'affaire est reprise dans le délai d'un mois.

Le premier président de la cour d'appel désigne chaque année les juges chargés de ces fonctions, que le ressort du conseil comprenne un ou plusieurs tribunaux d'instance.

En cas de pluralité de conseils de prud'hommes dans le ressort d'un tribunal de grande instance, le premier président de la cour d'appel peut, si l'activité le justifie, désigner les juges du tribunal d'instance dans le ressort duquel est situé le siège du tribunal de grande instance.
