# Article L1441-19

Les conditions d'éligibilité des candidats s'apprécient à la date du scrutin.
