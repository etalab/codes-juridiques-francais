# Paragraphe 2 : Collèges électoraux.

- [Article L1441-3](article-l1441-3.md)
- [Article L1441-4](article-l1441-4.md)
- [Article L1441-5](article-l1441-5.md)
