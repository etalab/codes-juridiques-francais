# Chapitre Ier : Dispositions générales.

- [Article L1521-1](article-l1521-1.md)
- [Article L1521-2](article-l1521-2.md)
- [Article L1521-3](article-l1521-3.md)
- [Article L1521-4](article-l1521-4.md)
