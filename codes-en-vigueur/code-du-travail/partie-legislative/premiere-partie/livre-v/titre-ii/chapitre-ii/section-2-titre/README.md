# Section 2 : Titre de travail simplifié.

- [Article L1522-3](article-l1522-3.md)
- [Article L1522-4](article-l1522-4.md)
- [Article L1522-5](article-l1522-5.md)
- [Article L1522-6](article-l1522-6.md)
- [Article L1522-7](article-l1522-7.md)
- [Article L1522-8](article-l1522-8.md)
- [Article L1522-9](article-l1522-9.md)
- [Article L1522-10](article-l1522-10.md)
- [Article L1522-11](article-l1522-11.md)
- [Article L1522-12](article-l1522-12.md)
