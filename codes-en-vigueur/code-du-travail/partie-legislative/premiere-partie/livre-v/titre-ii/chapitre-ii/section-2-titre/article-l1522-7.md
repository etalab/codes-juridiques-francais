# Article L1522-7

L'embauche du salarié ne peut intervenir qu'après que l'employeur a satisfait à l'obligation de déclaration préalable prévue à l'article L. 1221-10.
