# Article L1522-10

Les cotisations sociales d'origine légale ou conventionnelle imposées par la loi dues au titre des rémunérations versées aux salariés mentionnés à l'article L. 1522-4 sont calculées sur une base forfaitaire réduite et font l'objet d'un versement unique à la caisse générale de sécurité sociale.

Par dérogation, ces cotisations peuvent être calculées, d'un commun accord entre l'employeur et le salarié, sur les rémunérations réellement versées au salarié.

Elles sont calculées sur les rémunérations réellement versées au salarié dans le cas d'un contrat à durée indéterminée.
