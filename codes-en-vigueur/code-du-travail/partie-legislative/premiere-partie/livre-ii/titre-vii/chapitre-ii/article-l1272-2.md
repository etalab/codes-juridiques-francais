# Article L1272-2

Le chèque-emploi associatif permet de simplifier les déclarations et paiements des cotisations et contributions dues :

1° Au régime de sécurité sociale ou au régime obligatoire de protection sociale des salariés agricoles ;

2° Au régime d'assurance chômage ;

3° Aux institutions de retraites complémentaires et de prévoyance.

Lorsque ce titre-emploi comprend une formule de chèque, il peut être utilisé pour rémunérer le salarié.
