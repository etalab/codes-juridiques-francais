# Article L1272-5

Le chèque-emploi associatif peut être émis et délivré par les établissements de crédit ou par les institutions ou services énumérés à l'article L. 518-1 du code monétaire et financier qui ont passé une convention avec l'Etat. Lorsque ce titre-emploi ne comprend pas de formule de chèque, il est délivré par les organismes de recouvrement du régime général de sécurité sociale mentionnés à l'articleL. 133-8-4 du code de la sécurité sociale.
