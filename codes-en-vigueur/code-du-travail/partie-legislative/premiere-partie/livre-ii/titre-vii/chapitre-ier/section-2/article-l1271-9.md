# Article L1271-9

Le chèque emploi-service universel, lorsqu'il a la nature d'un chèque au sens du chapitre Ier du titre III du livre Ier du code monétaire et financier, est émis par les établissements de crédit ou les institutions ou services énumérés par l'article L. 518-1 du même code qui ont passé une convention avec l'Etat.

Lorsque ce titre-emploi ne comporte pas de formule de chèque, il est délivré par l'union de recouvrement des cotisations de sécurité sociale et d'allocations familiales territorialement compétente ou l'organisme de recouvrement du régime général de sécurité sociale mentionné à l'article L. 133-8 du code de la sécurité sociale.
