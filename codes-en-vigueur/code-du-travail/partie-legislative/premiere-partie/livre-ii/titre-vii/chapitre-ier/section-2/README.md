# Section 2 : Dispositions financières.

- [Article L1271-9](article-l1271-9.md)
- [Article L1271-10](article-l1271-10.md)
- [Article L1271-11](article-l1271-11.md)
- [Article L1271-12](article-l1271-12.md)
- [Article L1271-13](article-l1271-13.md)
- [Article L1271-14](article-l1271-14.md)
- [Article L1271-15](article-l1271-15.md)
- [Article L1271-15-1](article-l1271-15-1.md)
