# Section 1 : Objet et modalités de mise en oeuvre.

- [Article L1271-1](article-l1271-1.md)
- [Article L1271-2](article-l1271-2.md)
- [Article L1271-3](article-l1271-3.md)
- [Article L1271-4](article-l1271-4.md)
- [Article L1271-5](article-l1271-5.md)
- [Article L1271-6](article-l1271-6.md)
- [Article L1271-7](article-l1271-7.md)
- [Article L1271-8](article-l1271-8.md)
