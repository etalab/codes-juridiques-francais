# Article L1273-4

A partir des informations dont il dispose, l'organisme habilité pour recouvrer les cotisations et les contributions dues au titre de l'emploi du salarié délivre à l'employeur, pour remise au salarié, un bulletin de paie qui est réputé remplir les conditions prévues à l'article L. 3243-2. Par dérogation, un décret peut préciser les cas dans lesquels le bulletin de paie est délivré au salarié.
