# Chapitre IV : Portage salarial

- [Section 1 : Définition et champ d'application](section-1)
- [Section 2 : Conditions et interdictions de recours au portage salarial](section-2)
- [Section 3 : Contrat de travail](section-3)
- [Section 4 : Le contrat commercial de prestation de portage salarial](section-4)
- [Section 5 : L'entreprise de portage salarial](section-5)
