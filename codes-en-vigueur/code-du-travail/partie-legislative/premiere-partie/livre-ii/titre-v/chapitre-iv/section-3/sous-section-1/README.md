# Sous-section 1 : Dispositions communes

- [Article L1254-7](article-l1254-7.md)
- [Article L1254-8](article-l1254-8.md)
- [Article L1254-9](article-l1254-9.md)
