# Paragraphe 1 : Rupture anticipée du contrat.

- [Article L1251-26](article-l1251-26.md)
- [Article L1251-27](article-l1251-27.md)
- [Article L1251-28](article-l1251-28.md)
