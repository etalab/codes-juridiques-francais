# Paragraphe 1 : Fixation du terme et durée du contrat.

- [Article L1251-11](article-l1251-11.md)
- [Article L1251-12](article-l1251-12.md)
- [Article L1251-13](article-l1251-13.md)
