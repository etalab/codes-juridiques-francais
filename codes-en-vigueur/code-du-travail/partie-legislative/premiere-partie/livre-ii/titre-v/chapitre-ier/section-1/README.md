# Section 1 : Définitions.

- [Article L1251-1](article-l1251-1.md)
- [Article L1251-2](article-l1251-2.md)
- [Article L1251-3](article-l1251-3.md)
- [Article L1251-4](article-l1251-4.md)
