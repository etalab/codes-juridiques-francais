# Paragraphe 2 : Garantie financière et défaillance de l'entreprise de travail temporaire.

- [Article L1251-49](article-l1251-49.md)
- [Article L1251-50](article-l1251-50.md)
- [Article L1251-51](article-l1251-51.md)
- [Article L1251-52](article-l1251-52.md)
- [Article L1251-53](article-l1251-53.md)
