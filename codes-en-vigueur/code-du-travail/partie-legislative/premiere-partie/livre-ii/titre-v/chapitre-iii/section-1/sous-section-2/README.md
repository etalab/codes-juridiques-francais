# Sous-section 2 : Constitution et adhésion.

- [Article L1253-2](article-l1253-2.md)
- [Article L1253-3](article-l1253-3.md)
- [Article L1253-6](article-l1253-6.md)
- [Article L1253-7](article-l1253-7.md)
- [Article L1253-8](article-l1253-8.md)
