# Chapitre II : Contrat conclu avec une entreprise de travail à temps partagé

- [Section 1 : Définitions.](section-1)
- [Section 2 : Contrat de travail à temps partagé.](section-2)
- [Section 3 : Contrat de mise à disposition et entreprise de travail à temps partagé.](section-3)
