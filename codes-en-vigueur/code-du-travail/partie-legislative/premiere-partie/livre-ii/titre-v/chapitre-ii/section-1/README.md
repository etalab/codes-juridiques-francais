# Section 1 : Définitions.

- [Article L1252-1](article-l1252-1.md)
- [Article L1252-2](article-l1252-2.md)
- [Article L1252-3](article-l1252-3.md)
