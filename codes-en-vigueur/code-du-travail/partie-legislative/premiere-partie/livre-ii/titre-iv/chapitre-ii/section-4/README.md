# Section 4 : Forme, contenu et transmission du contrat.

- [Article L1242-12](article-l1242-12.md)
- [Article L1242-12-1](article-l1242-12-1.md)
- [Article L1242-13](article-l1242-13.md)
