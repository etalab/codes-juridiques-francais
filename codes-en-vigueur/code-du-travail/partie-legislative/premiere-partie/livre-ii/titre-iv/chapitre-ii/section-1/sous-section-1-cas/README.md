# Sous-section 1 : Cas de recours.

- [Article L1242-1](article-l1242-1.md)
- [Article L1242-2](article-l1242-2.md)
- [Article L1242-3](article-l1242-3.md)
- [Article L1242-4](article-l1242-4.md)
