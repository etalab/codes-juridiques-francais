# Chapitre IV : Succession de contrats

- [Section 1 : Contrats successifs avec le même salarié.](section-1)
- [Section 2 : Contrats successifs sur le même poste.](section-2)
