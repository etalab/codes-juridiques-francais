# Section 3 : Modification du contrat de travail en cas d'accord de réduction du temps de travail.

- [Article L1222-7](article-l1222-7.md)
- [Article L1222-8](article-l1222-8.md)
