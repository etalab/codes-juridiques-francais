# Sous-section 3 : Inaptitude consécutive à un accident du travail ou à une maladie professionnelle.

- [Article L1226-10](article-l1226-10.md)
- [Article L1226-11](article-l1226-11.md)
- [Article L1226-12](article-l1226-12.md)
