# Sous-section 1 : Inaptitude consécutive à une maladie ou un accident non professionnel.

- [Article L1226-2](article-l1226-2.md)
- [Article L1226-3](article-l1226-3.md)
- [Article L1226-4](article-l1226-4.md)
- [Article L1226-4-1](article-l1226-4-1.md)
- [Article L1226-4-2](article-l1226-4-2.md)
- [Article L1226-4-3](article-l1226-4-3.md)
