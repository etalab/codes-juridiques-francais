# Article L1221-7

Dans les entreprises d'au moins cinquante salariés, les informations mentionnées à l'article L. 1221-6 et communiquées par écrit par le candidat à un emploi ne peuvent être examinées que dans des conditions préservant son anonymat.

Les modalités d'application du présent article sont déterminées par décret en Conseil d'Etat.
