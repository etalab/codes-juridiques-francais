# Chapitre Ier : Formation du contrat de travail

- [Section 1 : Dispositions générales.](section-1)
- [Section 2 : Recrutement.](section-2)
- [Section 3 : Formalités à l'embauche et à l'emploi](section-3)
- [Section 4 : Période d'essai.](section-4)
