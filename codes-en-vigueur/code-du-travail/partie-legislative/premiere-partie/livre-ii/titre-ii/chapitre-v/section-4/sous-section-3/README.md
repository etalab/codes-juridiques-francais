# Sous-section 3 : Démission pour élever un enfant.

- [Article L1225-66](article-l1225-66.md)
- [Article L1225-67](article-l1225-67.md)
- [Article L1225-68](article-l1225-68.md)
- [Article L1225-69](article-l1225-69.md)
