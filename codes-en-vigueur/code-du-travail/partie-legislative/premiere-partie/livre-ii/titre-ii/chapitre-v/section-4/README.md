# Section 4 : Congés d'éducation des enfants

- [Sous-section 1 : Congé parental d'éducation et passage à temps partiel.](sous-section-1)
- [Sous-section 2 : Congés pour maladie d'un enfant](sous-section-2)
- [Sous-section 3 : Démission pour élever un enfant.](sous-section-3)
