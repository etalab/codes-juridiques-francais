# Sous-section 5 : Dispositions particulières à l'allaitement.

- [Article L1225-30](article-l1225-30.md)
- [Article L1225-31](article-l1225-31.md)
- [Article L1225-32](article-l1225-32.md)
- [Article L1225-33](article-l1225-33.md)
