# Section 1 : Protection de la grossesse et de la maternité

- [Sous-section 1 : Embauche, mutation et licenciement.](sous-section-1)
- [Sous-section 2 : Changements temporaires d'affectation](sous-section-2)
- [Sous-section 3 : Autorisations d'absence et congé de maternité.](sous-section-3)
- [Sous-section 4 : Interdiction d'emploi prénatal et postnatal.](sous-section-4)
- [Sous-section 5 : Dispositions particulières à l'allaitement.](sous-section-5)
- [Sous-section 6 : Démission.](sous-section-6)
