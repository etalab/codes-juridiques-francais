# Sous-section 2 : Rupture abusive du contrat.

- [Article L1237-2](article-l1237-2.md)
- [Article L1237-3](article-l1237-3.md)
