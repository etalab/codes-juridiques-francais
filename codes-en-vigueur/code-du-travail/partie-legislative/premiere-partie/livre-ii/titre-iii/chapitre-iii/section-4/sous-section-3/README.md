# Sous-section 3 : Procédure à l'égard des salariés

- [Paragraphe 1 : Entretien préalable.](paragraphe-1)
- [Paragraphe 2 : Notification du licenciement.](paragraphe-2)
- [Paragraphe 3 : Priorité de réembauche.](paragraphe-3)
- [Paragraphe 4 : Mesures de reclassement interne.](paragraphe-4)
