# Paragraphe 1 : Réunions des représentants du personnel.

- [Article L1233-28](article-l1233-28.md)
- [Article L1233-29](article-l1233-29.md)
- [Article L1233-30](article-l1233-30.md)
- [Article L1233-31](article-l1233-31.md)
- [Article L1233-32](article-l1233-32.md)
- [Article L1233-33](article-l1233-33.md)
