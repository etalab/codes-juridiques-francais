# Paragraphe 1 : Information de l'autorité administrative.

- [Article L1233-46](article-l1233-46.md)
- [Article L1233-48](article-l1233-48.md)
- [Article L1233-49](article-l1233-49.md)
- [Article L1233-50](article-l1233-50.md)
- [Article L1233-51](article-l1233-51.md)
