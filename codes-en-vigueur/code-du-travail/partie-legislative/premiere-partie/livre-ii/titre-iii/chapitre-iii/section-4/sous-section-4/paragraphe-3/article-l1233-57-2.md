# Article L1233-57-2

<div align="left">L'autorité administrative valide l'accord collectif mentionné à l'article L. 1233-24-1 dès lors qu'elle s'est assurée de : <p>1° Sa conformité aux articles L. 1233-24-1 à L. 1233-24-3 ; </p>
<p>2° La régularité de la procédure d'information et de consultation du comité d'entreprise et, le cas échéant, du comité d'hygiène, de sécurité et des conditions de travail et de l'instance de coordination mentionnée à l'article L. 4616-1 ; </p>
<p>3° La présence dans le plan de sauvegarde de l'emploi des mesures prévues aux articles L. 1233-61 et L. 1233-63 ; </p>
<p>4° La mise en œuvre effective, le cas échéant, des obligations prévues aux articles L. 1233-57-9 à L. 1233-57-16, L. 1233-57-19 et L. 1233-57-20.</p>
</div>
