# Sous-section 4 : Information et intervention de l'autorité administrative

- [Paragraphe 1 : Information de l'autorité administrative.](paragraphe-1)
- [Paragraphe 2 : Intervention de l'autorité administrative concernant les entreprises non soumises à l'obligation d'établir un plan de sauvegarde de l'emploi.](paragraphe-2)
- [Paragraphe 3 : Intervention de l'autorité administrative concernant les entreprises soumises à l'obligation d'établir un plan de sauvegarde de l'emploi.](paragraphe-3)
