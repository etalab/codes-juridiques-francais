# Paragraphe 2 : Notification du licenciement.

- [Article L1233-15](article-l1233-15.md)
- [Article L1233-16](article-l1233-16.md)
- [Article L1233-17](article-l1233-17.md)
- [Article L1233-18](article-l1233-18.md)
