# Sous-section 1 : Procédure de consultation des représentants du personnel propre au licenciement collectif.

- [Article L1233-8](article-l1233-8.md)
- [Article L1233-9](article-l1233-9.md)
- [Article L1233-10](article-l1233-10.md)
