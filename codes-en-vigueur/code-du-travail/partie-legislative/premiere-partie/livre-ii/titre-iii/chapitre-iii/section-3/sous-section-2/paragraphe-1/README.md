# Paragraphe 1 : Entretien préalable.

- [Article L1233-11](article-l1233-11.md)
- [Article L1233-12](article-l1233-12.md)
- [Article L1233-13](article-l1233-13.md)
- [Article L1233-14](article-l1233-14.md)
