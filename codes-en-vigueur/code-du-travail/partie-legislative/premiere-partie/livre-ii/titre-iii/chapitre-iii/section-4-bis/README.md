# Section 4 bis : Obligation de rechercher un repreneur
en cas de projet de fermeture d'un établissement

- [Sous-section 1 : Information des salariés et de l'autorité administrative de l'intention de fermer un établissement](sous-section-1)
- [Sous-section 2 : Recherche d'un repreneur](sous-section-2)
- [Sous-section 3 : Dispositions d'application](sous-section-3)
