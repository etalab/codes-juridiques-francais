# Paragraphe 3 : Clôture de la période de recherche

- [Article L1233-57-19](article-l1233-57-19.md)
- [Article L1233-57-20](article-l1233-57-20.md)
- [Article L1233-57-21](article-l1233-57-21.md)
