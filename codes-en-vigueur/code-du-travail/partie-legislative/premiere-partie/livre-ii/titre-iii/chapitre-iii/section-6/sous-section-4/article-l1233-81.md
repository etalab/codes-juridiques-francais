# Article L1233-81

L'acceptation par le salarié de la proposition de congé de mobilité dispense l'employeur de l'obligation de lui proposer le congé de reclassement.
