# Sous-section 4 : Congé de mobilité.

- [Article L1233-77](article-l1233-77.md)
- [Article L1233-78](article-l1233-78.md)
- [Article L1233-79](article-l1233-79.md)
- [Article L1233-80](article-l1233-80.md)
- [Article L1233-81](article-l1233-81.md)
- [Article L1233-82](article-l1233-82.md)
- [Article L1233-83](article-l1233-83.md)
