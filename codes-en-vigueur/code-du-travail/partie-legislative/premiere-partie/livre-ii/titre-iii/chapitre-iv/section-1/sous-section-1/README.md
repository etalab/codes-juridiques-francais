# Sous-section 1 : Préavis et indemnité compensatrice de préavis.

- [Article L1234-1](article-l1234-1.md)
- [Article L1234-2](article-l1234-2.md)
- [Article L1234-3](article-l1234-3.md)
- [Article L1234-4](article-l1234-4.md)
- [Article L1234-5](article-l1234-5.md)
- [Article L1234-6](article-l1234-6.md)
- [Article L1234-7](article-l1234-7.md)
- [Article L1234-8](article-l1234-8.md)
