# Chapitre IV : Conséquences du licenciement

- [Section 1 : Préavis et indemnité de licenciement](section-1)
- [Section 2 : Documents remis par l'employeur](section-2)
