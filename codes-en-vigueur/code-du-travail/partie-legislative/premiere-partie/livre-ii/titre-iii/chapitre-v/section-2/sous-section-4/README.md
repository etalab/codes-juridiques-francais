# Sous-section 4 : Sanction des irrégularités.

- [Article L1235-10](article-l1235-10.md)
- [Article L1235-11](article-l1235-11.md)
- [Article L1235-12](article-l1235-12.md)
- [Article L1235-13](article-l1235-13.md)
- [Article L1235-14](article-l1235-14.md)
- [Article L1235-15](article-l1235-15.md)
- [Article L1235-16](article-l1235-16.md)
- [Article L1235-17](article-l1235-17.md)
