# Article L1235-16

L'annulation de la décision de validation mentionnée à l'article L. 1233-57-2 ou d'homologation mentionnée à l'article L. 1233-57-3 pour un motif autre que celui mentionné au deuxième alinéa de l'article L. 1235-10 donne lieu, sous réserve de l'accord des parties, à la réintégration du salarié dans l'entreprise, avec maintien de ses avantages acquis.

A défaut, le salarié a droit à une indemnité à la charge de l'employeur, qui ne peut être inférieure aux salaires des six derniers mois. Elle est due sans préjudice de l'indemnité de licenciement prévue à l'article L. 1234-9.
