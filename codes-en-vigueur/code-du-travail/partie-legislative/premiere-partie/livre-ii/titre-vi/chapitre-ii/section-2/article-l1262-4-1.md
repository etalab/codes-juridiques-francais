# Article L1262-4-1

Le donneur d'ordre ou le maître d'ouvrage qui contracte avec un prestataire de services qui détache des salariés, dans les conditions mentionnées aux articles L. 1262-1 et L. 1262-2, vérifie auprès de ce dernier, avant le début du détachement, qu'il s'est acquitté des obligations mentionnées aux I et II de l'article L. 1262-2-1.
