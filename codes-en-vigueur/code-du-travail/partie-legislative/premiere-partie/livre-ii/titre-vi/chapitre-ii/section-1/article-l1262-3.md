# Article L1262-3

Un employeur ne peut se prévaloir des dispositions applicables au détachement de salariés lorsque son activité est entièrement orientée vers le territoire national ou lorsqu'elle est réalisée dans des locaux ou avec des infrastructures situées sur le territoire national à partir desquels elle est exercée de façon habituelle, stable et continue. Il ne peut notamment se prévaloir de ces dispositions lorsque son activité comporte la recherche et la prospection d'une clientèle ou le recrutement de salariés sur ce territoire.

Dans ces situations, l'employeur est assujetti aux dispositions du code du travail applicables aux entreprises établies sur le territoire national.
