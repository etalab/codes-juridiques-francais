# Section 1 : Conditions de détachement.

- [Article L1262-1](article-l1262-1.md)
- [Article L1262-2](article-l1262-2.md)
- [Article L1262-2-1](article-l1262-2-1.md)
- [Article L1262-3](article-l1262-3.md)
