# Chapitre VI : Dispositions pénales.

- [Article L1146-1](article-l1146-1.md)
- [Article L1146-2](article-l1146-2.md)
- [Article L1146-3](article-l1146-3.md)
