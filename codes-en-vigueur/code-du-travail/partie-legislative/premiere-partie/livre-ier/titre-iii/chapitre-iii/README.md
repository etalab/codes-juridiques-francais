# Chapitre III : Différences de traitement autorisées.

- [Article L1133-1](article-l1133-1.md)
- [Article L1133-2](article-l1133-2.md)
- [Article L1133-3](article-l1133-3.md)
- [Article L1133-4](article-l1133-4.md)
- [Article L1133-5](article-l1133-5.md)
