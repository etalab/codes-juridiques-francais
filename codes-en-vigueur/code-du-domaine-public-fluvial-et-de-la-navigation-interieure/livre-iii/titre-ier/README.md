# Titre Ier : Des patrons bateliers

- [Article 160](article-160.md)
- [Article 161](article-161.md)
- [Article 165](article-165.md)
- [Article 168](article-168.md)
