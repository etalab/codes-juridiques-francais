# Article 230

Lorsqu'une créance hypothécaire régie par le titre Ier du livre II ci-dessus est en concours avec un privilège soumis aux articles 102 et suivants de la loi locale du 15 juin 1895 sur les rapports de droit privé dans la navigation intérieure, le rang de l'hypothèque continue à être déterminé par l'article 109 de ladite loi locale.

Les créanciers privilégiés sont tenus, en cas d'aliénation du bateau sur saisie ou sur surenchère du dixième, de notifier leurs droits au plus tard à l'audience de distribution du prix devant le tribunal cantonal.

Les dispositions du présent article ne sont pas applicables aux bateaux ne circulant pas habituellement sur le Rhin.
