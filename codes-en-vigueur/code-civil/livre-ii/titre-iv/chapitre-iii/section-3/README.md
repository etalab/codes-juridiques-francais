# Section 3 : Des droits du propriétaire du fonds auquel la servitude est due

- [Article 697](article-697.md)
- [Article 698](article-698.md)
- [Article 699](article-699.md)
- [Article 700](article-700.md)
- [Article 701](article-701.md)
- [Article 702](article-702.md)
