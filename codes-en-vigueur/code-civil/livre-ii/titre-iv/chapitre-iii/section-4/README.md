# Section 4 : Comment les servitudes s'éteignent

- [Article 703](article-703.md)
- [Article 704](article-704.md)
- [Article 705](article-705.md)
- [Article 706](article-706.md)
- [Article 707](article-707.md)
- [Article 708](article-708.md)
- [Article 709](article-709.md)
- [Article 710](article-710.md)
