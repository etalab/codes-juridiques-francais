# Titre III : De l'usufruit, de l'usage et de l'habitation

- [Chapitre Ier : De l'usufruit](chapitre-ier)
- [Chapitre II : De l'usage et de l'habitation](chapitre-ii)
