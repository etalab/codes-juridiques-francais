# Chapitre IV : De la séparation de corps

- [Section 1 : Des cas et de la procédure de la séparation de corps](section-1)
- [Section 2 : Des conséquences de la séparation de corps](section-2)
- [Section 3 : De la fin de la séparation de corps](section-3)
