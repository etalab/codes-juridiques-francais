# Chapitre Ier : Des cas de divorce

- [Section 1 : Du divorce par consentement mutuel](section-1)
- [Section 4 : Du divorce pour faute](section-4)
- [Section 5 : Des modifications du fondement d'une demande en divorce](section-5)
- [Article 229](article-229.md)
