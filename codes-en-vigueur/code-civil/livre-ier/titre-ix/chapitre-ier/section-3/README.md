# Section 3 : De la délégation de l'autorité parentale

- [Article 376](article-376.md)
- [Article 376-1](article-376-1.md)
- [Article 377](article-377.md)
- [Article 377-1](article-377-1.md)
- [Article 377-2](article-377-2.md)
- [Article 377-3](article-377-3.md)
