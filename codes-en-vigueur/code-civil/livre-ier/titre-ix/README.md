# Titre IX : De l'autorité parentale

- [Chapitre Ier : De l'autorité parentale relativement à la personne de l'enfant](chapitre-ier)
- [Chapitre II : De l'autorité parentale relativement aux biens de l'enfant](chapitre-ii)
