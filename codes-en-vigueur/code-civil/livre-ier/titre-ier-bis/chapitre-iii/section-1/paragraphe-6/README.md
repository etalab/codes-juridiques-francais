# Paragraphe 6 : Dispositions communes à certains modes d'acquisition de la nationalité française

- [Article 21-26](article-21-26.md)
- [Article 21-27](article-21-27.md)
- [Article 21-27-1](article-21-27-1.md)
