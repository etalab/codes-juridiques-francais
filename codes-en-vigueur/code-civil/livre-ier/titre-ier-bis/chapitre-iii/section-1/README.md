# Section 1 : Des modes d'acquisition de la nationalité française

- [Paragraphe 1 : Acquisition de la nationalité française à raison de la filiation](paragraphe-1)
- [Paragraphe 2 : Acquisition de la nationalité française à raison du mariage](paragraphe-2)
- [Paragraphe 3 : Acquisition de la nationalité française à raison de la naissance et de la résidence en France](paragraphe-3)
- [Paragraphe 4 : Acquisition de la nationalité française par déclaration de nationalité](paragraphe-4)
- [Paragraphe 5 : Acquisition de la nationalité française par décision de l'autorité publique](paragraphe-5)
- [Paragraphe 6 : Dispositions communes à certains modes d'acquisition de la nationalité française](paragraphe-6)
- [Paragraphe 7 : De la cérémonie d'accueil dans la citoyenneté française](paragraphe-7)
