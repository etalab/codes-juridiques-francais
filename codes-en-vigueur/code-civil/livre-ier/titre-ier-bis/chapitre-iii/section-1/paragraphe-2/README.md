# Paragraphe 2 : Acquisition de la nationalité française à raison du mariage

- [Article 21-1](article-21-1.md)
- [Article 21-2](article-21-2.md)
- [Article 21-3](article-21-3.md)
- [Article 21-4](article-21-4.md)
- [Article 21-5](article-21-5.md)
- [Article 21-6](article-21-6.md)
