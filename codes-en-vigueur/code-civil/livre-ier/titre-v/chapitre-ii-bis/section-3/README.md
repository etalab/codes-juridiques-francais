# Section 3 : De la transcription du mariage célébré à l'étranger par une autorité étrangère

- [Article 171-5](article-171-5.md)
- [Article 171-6](article-171-6.md)
- [Article 171-7](article-171-7.md)
- [Article 171-8](article-171-8.md)
