# Chapitre VI : Des devoirs et des droits respectifs des époux

- [Article 212](article-212.md)
- [Article 213](article-213.md)
- [Article 214](article-214.md)
- [Article 215](article-215.md)
- [Article 216](article-216.md)
- [Article 217](article-217.md)
- [Article 218](article-218.md)
- [Article 219](article-219.md)
- [Article 220](article-220.md)
- [Article 220-1](article-220-1.md)
- [Article 220-2](article-220-2.md)
- [Article 220-3](article-220-3.md)
- [Article 221](article-221.md)
- [Article 222](article-222.md)
- [Article 223](article-223.md)
- [Article 225](article-225.md)
- [Article 225-1](article-225-1.md)
- [Article 226](article-226.md)
