# Chapitre Ier : Des qualités et conditions requises pour pouvoir contracter mariage

- [Article 143](article-143.md)
- [Article 144](article-144.md)
- [Article 145](article-145.md)
- [Article 146](article-146.md)
- [Article 146-1](article-146-1.md)
- [Article 147](article-147.md)
- [Article 148](article-148.md)
- [Article 149](article-149.md)
- [Article 150](article-150.md)
- [Article 151](article-151.md)
- [Article 154](article-154.md)
- [Article 155](article-155.md)
- [Article 156](article-156.md)
- [Article 157](article-157.md)
- [Article 159](article-159.md)
- [Article 160](article-160.md)
- [Article 161](article-161.md)
- [Article 162](article-162.md)
- [Article 163](article-163.md)
- [Article 164](article-164.md)
