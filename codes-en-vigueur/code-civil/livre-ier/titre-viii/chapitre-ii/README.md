# Chapitre II : De l'adoption simple

- [Section 1 : Des conditions requises et du jugement](section-1)
- [Section 2 : Des effets de l'adoption simple](section-2)
