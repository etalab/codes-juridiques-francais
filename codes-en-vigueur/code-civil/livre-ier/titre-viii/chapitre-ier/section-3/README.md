# Section 3 : Des effets de l'adoption plénière

- [Article 355](article-355.md)
- [Article 356](article-356.md)
- [Article 357](article-357.md)
- [Article 357-1](article-357-1.md)
- [Article 358](article-358.md)
- [Article 359](article-359.md)
