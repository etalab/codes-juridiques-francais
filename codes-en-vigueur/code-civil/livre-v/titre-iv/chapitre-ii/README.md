# Chapitre II : Dispositions diverses

- [Section 1 : Privilèges et hypothèques](section-1)
- [Section 2 : Expropriation forcée](section-2)
