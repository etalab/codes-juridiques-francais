# Titre IV : Dispositions relatives à l'immatriculation des immeubles et aux droits sur les immeubles.

- [Chapitre Ier : Du régime de l'immatriculation des immeubles](chapitre-ier)
- [Chapitre II : Dispositions diverses](chapitre-ii)
- [Article 2509](article-2509.md)
