# Chapitre IV : Du bail à cheptel

- [Section 1 : Dispositions générales.](section-1)
- [Section 2 : Du cheptel simple.](section-2)
- [Section 3 : Du cheptel à moitié.](section-3)
- [Section 4 : Du cheptel donné par le propriétaire à son fermier ou métayer.](section-4)
- [Section 5 : Du contrat improprement appelé cheptel.](section-5)
