# Chapitre III : Du louage d'ouvrage et d'industrie.

- [Section 1 : Du louage de service.](section-1)
- [Section 2 : Des voituriers par terre et par eau.](section-2)
- [Section 3 : Des devis et des marchés.](section-3)
- [Article 1779](article-1779.md)
