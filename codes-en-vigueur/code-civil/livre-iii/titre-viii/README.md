# Titre VIII : Du contrat de louage

- [Chapitre Ier : Dispositions générales.](chapitre-ier)
- [Chapitre II : Du louage des choses.](chapitre-ii)
- [Chapitre III : Du louage d'ouvrage et d'industrie.](chapitre-iii)
- [Chapitre IV : Du bail à cheptel](chapitre-iv)
