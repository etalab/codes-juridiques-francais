# Chapitre Ier : Dispositions générales.

- [Article 1708](article-1708.md)
- [Article 1709](article-1709.md)
- [Article 1710](article-1710.md)
- [Article 1711](article-1711.md)
- [Article 1712](article-1712.md)
