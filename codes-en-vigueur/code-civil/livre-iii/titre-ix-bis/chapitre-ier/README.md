# Chapitre Ier : Des conventions relatives à l'exercice des droits indivis en l'absence d'usufruitier.

- [Article 1873-2](article-1873-2.md)
- [Article 1873-3](article-1873-3.md)
- [Article 1873-4](article-1873-4.md)
- [Article 1873-5](article-1873-5.md)
- [Article 1873-6](article-1873-6.md)
- [Article 1873-7](article-1873-7.md)
- [Article 1873-8](article-1873-8.md)
- [Article 1873-9](article-1873-9.md)
- [Article 1873-10](article-1873-10.md)
- [Article 1873-11](article-1873-11.md)
- [Article 1873-12](article-1873-12.md)
- [Article 1873-13](article-1873-13.md)
- [Article 1873-14](article-1873-14.md)
- [Article 1873-15](article-1873-15.md)
