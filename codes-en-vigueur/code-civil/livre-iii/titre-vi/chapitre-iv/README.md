# Chapitre IV : Des obligations du vendeur

- [Section 1 : Dispositions générales.](section-1)
- [Section 2 : De la délivrance.](section-2)
- [Section 3 : De la garantie.](section-3)
