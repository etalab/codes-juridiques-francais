# Paragraphe 2 : De la garantie des défauts de la chose vendue.

- [Article 1641](article-1641.md)
- [Article 1642](article-1642.md)
- [Article 1642-1](article-1642-1.md)
- [Article 1643](article-1643.md)
- [Article 1644](article-1644.md)
- [Article 1645](article-1645.md)
- [Article 1646](article-1646.md)
- [Article 1646-1](article-1646-1.md)
- [Article 1647](article-1647.md)
- [Article 1648](article-1648.md)
- [Article 1649](article-1649.md)
