# Chapitre V : Des obligations de l'acheteur.

- [Article 1650](article-1650.md)
- [Article 1651](article-1651.md)
- [Article 1652](article-1652.md)
- [Article 1653](article-1653.md)
- [Article 1654](article-1654.md)
- [Article 1655](article-1655.md)
- [Article 1656](article-1656.md)
- [Article 1657](article-1657.md)
