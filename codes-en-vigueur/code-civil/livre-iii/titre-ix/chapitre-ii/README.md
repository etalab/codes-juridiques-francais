# Chapitre II : De la société civile

- [Section 1 : Dispositions générales.](section-1)
- [Section 2 : Gérance.](section-2)
- [Section 3 : Décisions collectives.](section-3)
- [Section 4 : Information des associés.](section-4)
- [Section 5 : Engagement des associés à l'égard des tiers.](section-5)
- [Section 6 : Cession des parts sociales.](section-6)
- [Section 7 : Retrait ou décès d'un associé.](section-7)
