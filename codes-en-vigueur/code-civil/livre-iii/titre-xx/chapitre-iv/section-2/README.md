# Section 2 : De la renonciation à la prescription.

- [Article 2250](article-2250.md)
- [Article 2251](article-2251.md)
- [Article 2252](article-2252.md)
- [Article 2253](article-2253.md)
