# Chapitre Ier : Dispositions générales.

- [Article 2219](article-2219.md)
- [Article 2220](article-2220.md)
- [Article 2221](article-2221.md)
- [Article 2222](article-2222.md)
- [Article 2223](article-2223.md)
