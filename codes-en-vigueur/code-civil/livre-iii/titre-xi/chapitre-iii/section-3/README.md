# Section 3 : Du séquestre ou dépôt judiciaire.

- [Article 1961](article-1961.md)
- [Article 1962](article-1962.md)
- [Article 1963](article-1963.md)
