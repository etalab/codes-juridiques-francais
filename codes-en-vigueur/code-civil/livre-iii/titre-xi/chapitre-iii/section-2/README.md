# Section 2 : Du séquestre conventionnel.

- [Article 1956](article-1956.md)
- [Article 1957](article-1957.md)
- [Article 1958](article-1958.md)
- [Article 1959](article-1959.md)
- [Article 1960](article-1960.md)
