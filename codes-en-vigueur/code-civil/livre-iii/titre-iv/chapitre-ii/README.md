# Chapitre II : Des délits et des quasi-délits.

- [Article 1382](article-1382.md)
- [Article 1383](article-1383.md)
- [Article 1384](article-1384.md)
- [Article 1385](article-1385.md)
- [Article 1386](article-1386.md)
