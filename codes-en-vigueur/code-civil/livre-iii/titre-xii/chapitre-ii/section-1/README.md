# Section 1 : Des conditions requises pour la validité du contrat.

- [Article 1968](article-1968.md)
- [Article 1969](article-1969.md)
- [Article 1970](article-1970.md)
- [Article 1971](article-1971.md)
- [Article 1972](article-1972.md)
- [Article 1973](article-1973.md)
- [Article 1974](article-1974.md)
- [Article 1975](article-1975.md)
- [Article 1976](article-1976.md)
