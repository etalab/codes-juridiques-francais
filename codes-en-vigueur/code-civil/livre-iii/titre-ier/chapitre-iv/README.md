# Chapitre IV : De l'option de l'héritier

- [Section 1 : Dispositions générales.](section-1)
- [Section 2 : De l'acceptation pure et simple de la succession.](section-2)
- [Section 3 : De l'acceptation de la succession à concurrence de l'actif net.](section-3)
- [Section 4 : De la renonciation à la succession.](section-4)
