# Chapitre VII : Du régime légal de l'indivision.

- [Section 1 : Des actes relatifs aux biens indivis.](section-1)
- [Section 2 : Des droits et des obligations des indivisaires.](section-2)
- [Section 3 : Du droit de poursuite des créanciers.](section-3)
- [Section 4 : De l'indivision en usufruit.](section-4)
- [Article 815](article-815.md)
- [Article 815-1](article-815-1.md)
