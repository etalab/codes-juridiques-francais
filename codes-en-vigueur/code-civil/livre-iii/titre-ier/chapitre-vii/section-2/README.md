# Section 2 : Des droits et des obligations des indivisaires.

- [Article 815-8](article-815-8.md)
- [Article 815-9](article-815-9.md)
- [Article 815-10](article-815-10.md)
- [Article 815-11](article-815-11.md)
- [Article 815-12](article-815-12.md)
- [Article 815-13](article-815-13.md)
- [Article 815-14](article-815-14.md)
- [Article 815-15](article-815-15.md)
- [Article 815-16](article-815-16.md)
