# Paragraphe 2 : Des parts et des lots.

- [Article 825](article-825.md)
- [Article 826](article-826.md)
- [Article 827](article-827.md)
- [Article 828](article-828.md)
- [Article 829](article-829.md)
- [Article 830](article-830.md)
