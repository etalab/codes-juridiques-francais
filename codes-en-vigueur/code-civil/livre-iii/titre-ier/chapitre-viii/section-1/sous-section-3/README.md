# Sous-section 3 : Du partage judiciaire.

- [Article 840](article-840.md)
- [Article 840-1](article-840-1.md)
- [Article 841](article-841.md)
- [Article 841-1](article-841-1.md)
- [Article 842](article-842.md)
