# Section 3 : Du paiement des dettes

- [Paragraphe 1 : Des dettes des copartageants](paragraphe-1)
- [Paragraphe 2 : Des autres dettes](paragraphe-2)
