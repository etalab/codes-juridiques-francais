# Section 1 : Des qualités requises pour succéder.

- [Article 725](article-725.md)
- [Article 725-1](article-725-1.md)
- [Article 726](article-726.md)
- [Article 727](article-727.md)
- [Article 727-1](article-727-1.md)
- [Article 728](article-728.md)
- [Article 729](article-729.md)
- [Article 729-1](article-729-1.md)
