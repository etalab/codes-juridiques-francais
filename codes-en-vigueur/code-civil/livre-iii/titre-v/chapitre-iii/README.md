# Chapitre III : Du régime de séparation de biens.

- [Article 1536](article-1536.md)
- [Article 1537](article-1537.md)
- [Article 1538](article-1538.md)
- [Article 1539](article-1539.md)
- [Article 1540](article-1540.md)
- [Article 1541](article-1541.md)
- [Article 1542](article-1542.md)
- [Article 1543](article-1543.md)
