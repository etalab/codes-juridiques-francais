# Section 2 : De la capacité des parties contractantes.

- [Article 1123](article-1123.md)
- [Article 1124](article-1124.md)
- [Article 1125](article-1125.md)
- [Article 1125-1](article-1125-1.md)
