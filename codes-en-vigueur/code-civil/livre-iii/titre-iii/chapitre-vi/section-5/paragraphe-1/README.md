# Paragraphe 1 : Du serment décisoire.

- [Article 1358](article-1358.md)
- [Article 1359](article-1359.md)
- [Article 1360](article-1360.md)
- [Article 1361](article-1361.md)
- [Article 1362](article-1362.md)
- [Article 1363](article-1363.md)
- [Article 1364](article-1364.md)
- [Article 1365](article-1365.md)
