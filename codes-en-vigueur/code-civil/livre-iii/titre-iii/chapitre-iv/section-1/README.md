# Section 1 : Des obligations conditionnelles.

- [Paragraphe 1 : De la condition en général, et de ses diverses espèces.](paragraphe-1)
- [Paragraphe 2 : De la condition suspensive.](paragraphe-2)
- [Paragraphe 3 : De la condition résolutoire.](paragraphe-3)
