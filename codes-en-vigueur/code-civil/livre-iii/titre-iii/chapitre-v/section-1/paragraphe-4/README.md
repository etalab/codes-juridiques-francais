# Paragraphe 4 : Des offres de paiement, et de la consignation.

- [Article 1257](article-1257.md)
- [Article 1258](article-1258.md)
- [Article 1260](article-1260.md)
- [Article 1261](article-1261.md)
- [Article 1262](article-1262.md)
- [Article 1263](article-1263.md)
- [Article 1264](article-1264.md)
