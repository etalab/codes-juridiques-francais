# Chapitre Ier : Du prêt à usage, ou commodat

- [Section 1 : De la nature du prêt à usage.](section-1)
- [Section 2 : Des engagements de l'emprunteur.](section-2)
- [Section 3 : Des engagements de celui qui prête à usage.](section-3)
