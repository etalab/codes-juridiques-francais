# Titre X : Du prêt

- [Chapitre Ier : Du prêt à usage, ou commodat](chapitre-ier)
- [Chapitre II : Du prêt de consommation, ou simple prêt](chapitre-ii)
- [Chapitre III : Du prêt à intérêt.](chapitre-iii)
- [Article 1874](article-1874.md)
