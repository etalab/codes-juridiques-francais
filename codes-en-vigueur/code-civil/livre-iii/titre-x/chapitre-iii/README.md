# Chapitre III : Du prêt à intérêt.

- [Article 1905](article-1905.md)
- [Article 1906](article-1906.md)
- [Article 1907](article-1907.md)
- [Article 1908](article-1908.md)
- [Article 1909](article-1909.md)
- [Article 1910](article-1910.md)
- [Article 1911](article-1911.md)
- [Article 1912](article-1912.md)
- [Article 1913](article-1913.md)
- [Article 1914](article-1914.md)
