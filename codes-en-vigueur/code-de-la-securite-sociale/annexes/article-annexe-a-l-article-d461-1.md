# Article Annexe à l'article D461-1

Liste des maladies ayant un caractère professionnel dont la déclaration est obligatoire pour tout docteur en médecine qui peut en avoir connaissance.

A - Maladies susceptibles d'avoir une origine professionnelle et d'être imputée aux agents chimiques suivants (1)

<table>
<tbody>
<tr>
<td>
<p align="center">NUMERO d'ordre</p>
</td>
<td>
<p align="center">AGENTS CHIMIQUES</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">4</p>
</td>
<td valign="top">
<p>Glucinium (béryllium) et ses composés.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">5</p>
</td>
<td valign="top">
<p>Boranes.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">6</p>
</td>
<td valign="top">
<p>Composés du carbone suivants (2) : Oxyde de carbone ; Oxychlorure de carbone ; Sulfure de carbone ; Acide cyanhydrique ; Cyanures métalliques ; Composés du cyanogène ; Esters isocyaniques.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">7</p>
</td>
<td valign="top">
<p>Composés de l'azote suivants : Ammoniaque ; Oxydes d'azote ; Acide nitrique.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">8</p>
</td>
<td valign="top">
<p>Ozone.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">9</p>
</td>
<td valign="top">
<p>Fluor et ses composés.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">15</p>
</td>
<td valign="top">
<p>Phosphore et ses composés, notamment les esters phosphoriques, pyrophosphoriques, thiophosphoriques, ainsi que les autres composés organiques du phosphore.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">16</p>
</td>
<td valign="top">
<p>Composés du soufre suivants : Hydrogène sulfuré ; Anhydride sulfureux ; Acide sulfurique ; Mercaptans et thioéthers, thiophène, thiophénol et homologues, ainsi que les dérivés halogénés de ces substances ; Esters des acides du soufre.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">17</p>
</td>
<td valign="top">
<p>Chlore et composés minéraux.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">23</p>
</td>
<td valign="top">
<p>Oxydes de vanadium.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">24</p>
</td>
<td valign="top">
<p>Chrome et ses composés.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">25</p>
</td>
<td valign="top">
<p>Manganèse et ses composés.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">28</p>
</td>
<td valign="top">
<p>Nickel et ses composés.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">30</p>
</td>
<td valign="top">
<p>Oxyde de zinc.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">33</p>
</td>
<td valign="top">
<p>Arsenic et ses composés.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">35</p>
</td>
<td valign="top">
<p>Brome et ses composés minéraux.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">48</p>
</td>
<td valign="top">
<p>Cadmium et ses composés.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">53</p>
</td>
<td valign="top">
<p>Iode et ses composés minéraux.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">80</p>
</td>
<td valign="top">
<p>Mercure et ses composés.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">81</p>
</td>
<td valign="top">
<p>Thallium et ses composés.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">82</p>
</td>
<td valign="top">
<p>Plomb et ses composés.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">601</p>
</td>
<td valign="top">
<p>Hydrocarbures aliphatiques, saturés ou non, cycliques ou non : Benzène, toluène, xylènes et autres homologues du benzène ; Vinylbenzène, divinylbenzène, diphényle, tétraline ; Naphthalènes et homologues.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">602</p>
</td>
<td valign="top">
<p>Dérivés halogénés des hydrocarbures aliphatiques ou aromatiques.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">603</p>
</td>
<td valign="top">
<p>Alcools, polyalcools et leurs esters nitriques :Ethers, tétrahydrofurane, dioxane, oxyde de diphényle et autres oxydes organiques, ainsi que leurs dérivés halogénés.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">604</p>
</td>
<td valign="top">
<p>Phénols et homologues, naphtols et homologues, ainsi que leurs dérivés halogénés.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">605</p>
</td>
<td valign="top">
<p>Aldéhydes, furfural.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">606</p>
</td>
<td valign="top">
<p>Cétones, benzoquinone.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">607</p>
</td>
<td valign="top">
<p>Acides organiques, leurs anhydrides, leurs esters, ainsi que les dérivés halogénés de ces substances.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">608</p>
</td>
<td valign="top">
<p>Nitriles.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">609</p>
</td>
<td valign="top">
<p>Dérivés nitrés aliphatiques.</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
</td>
<td valign="top">
<p>Dérivés nitrés des hydrocarbures aromatiques et des phénols.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">610</p>
</td>
<td valign="top">
<p>Dérivés halogénés des dérivés nitrés des hydrocarbures et des phénols.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">611</p>
</td>
<td valign="top">
<p>Dérivés azoxiques et azoïques.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">612</p>
</td>
<td valign="top">
<p>Amines aliphatiques et leurs dérivés halogénés.</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
</td>
<td valign="top">
<p>Amines et hydrazines aromatiques, ainsi que leurs dérivés halogénés, phénoliques, nitrosés, nitrés et sulfonés.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">613</p>
</td>
<td valign="top">
<p>Pyridine et autres bases hétérocycliques.</p>
</td>
</tr>
<tr>
<td valign="top">
<br/>
</td>
<td valign="top">
<p>Alcaloïdes.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">620</p>
</td>
<td valign="top">
<p>Substances hormonales.</p>
</td>
</tr>
<tr>
<td valign="top">
<p align="center">620</p>
</td>
<td valign="top">
<p>Substances hormonales.</p>
</td>
</tr>
</tbody>
</table>

(1) Les agents chimiques ont été classés dans l'ordre des numéros atomiques de l'élément le plus caractéristique.

(2) En raison de leur nombre considérable et de leur importance, les hydrocarbures et leurs dérivés ont fait l'objet d'une classification particulière prenant en considération leur fonction chimique (rubrique 601 et suivantes).

B - Maladies susceptibles d'avoir une origine professionnelle et d'être imputées aux agents physiques suivants :

1. Rayonnements ionisants.

2. Energie radiante.

3. Bruit.

4. Milieux où la pression est supérieure à la pression atmosphérique.

5. Vibrations mécaniques.

C - Maladies infectieuses ou parasitaires susceptibles d'avoir une origine professionnelle :

1. Maladies provoquées par les helminthes, l'ankylostomeduodénal, l'anguillule de l'intestin.

2. Infection charbonneuse, tétanos, leptospiroses, brucelloses.

3. Autres maladies infectieuses ou parasitaires transmises à l'homme par des animaux ou débris d'animaux.

4. Maladies infectieuses du personnel s'occupant de prévention, soins, assistance à domicile, recherches.

5. Maladies tropicales, notamment : paludisme, amibiase, trypanosomiase, dengue, fièvre à pappataci, fièvre de Malte, fièvre récurrente, fièvre jaune, peste, leischmaniose, pian, lèpre, typhus exanthématique et autres rickettsioses.

D - Maladies de la peau susceptibles d'avoir une origine professionnelle (autres que celles imputables à l'une des causes sus-énumérées) :

1. Cancers cutanés et affections cutanées précancéreuses éventuellement imputables à certains produits tels que : brais, goudrons, bitumes, suies, huiles anthracéniques, huiles minérales et paraffines brutes.

2. Affections cutanées imputables aux alcalis cautiques, aux ciments, aux bois exotiques et autres produits irritants.

3. Affections cutanées imputables à toute autre cause en relation avec le milieu professionnel.

E - Affections des voies respiratoires susceptibles d'avoir une origine professionnelle :

1. Pneumoconioses.

2. Affections broncho-pulmonaires imputables à des poussières ou fumées.

3. Asthme.

F - Autres affections susceptibles d'avoir une origine professionnelle :

1. Maladies des bourses périarticulaires dues à des pressions, cellulites sous-cutanées.

2. Maladies consécutives au surmenage des gaines tendineuses, du tissu péritendineux, des insertions musculaires et tendineuses.

3. Lésions du ménisque.

5. Arrachements par surmenage des apophyses épineuses.

6. Paralysies des nerfs dues à la pression.

7. Crampes.

8. Nystagmus.

9. Scorbut.
