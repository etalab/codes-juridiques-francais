# Article Annexe à l'article A951-1

**URSSAF compétences CCAMIP**

<table>
<tbody>
<tr>
<td valign="top">
<p>Régions administratives</p>
</td>
<td valign="top">
<p>Organisme de recouvrement des cotisations du régime général compétent pour le recouvrement de la redevance prévue à l'article L. 951-1 du code de la sécurité sociale</p>
</td>
</tr>
<tr>
<td valign="top">
<p>- Nord-Pas-de-Calais, Picardie, Bourgogne, Champagne, Ardenne, Lorraine, Alsace, Franche-Comté</p>
</td>
<td valign="top">
<p>URSSAF de la Côte-d'Or.</p>
</td>
</tr>
<tr>
<td valign="top">
<p>- Bretagne, Pays de la Loire, Poitou-Charentes, Aquitaine, Limousin, Midi-Pyrénées</p>
</td>
<td valign="top">
<p>URSSAF de la Vienne.</p>
</td>
</tr>
<tr>
<td valign="top">
<p>- Basse-Normandie, Haute-Normandie, Centre, Ile-de-France, Guadeloupe, Martinique, Guyane, Réunion</p>
</td>
<td valign="top">
<p>URSSAF de Rouen.</p>
</td>
</tr>
<tr>
<td valign="top">
<p>- Rhône-Alpes, Auvergne, Provence-Alpes-Côte d'Azur, Corse, Languedoc-Roussillon</p>
</td>
<td valign="top">
<p>URSSAF de Montpellier, Lodève</p>
</td>
</tr>
</tbody>
</table>
