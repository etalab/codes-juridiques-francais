# Article Annexe II : Tableau n° 83

**LESIONS PROVOQUEES PAR LES TRAVAUX EFFECTUES DANS UN MILIEU OU LA PRESSION EST INFERIEURE A LA PRESSION ATMOSPHERIQUE ET SOUMISE A VARIATIONS**

<table>
<tbody>
<tr>
<td width="246">
<p align="center">DÉSIGNATION DES MALADIES</p>
</td>
<td width="76">
<p align="center">DÉLAI de prise en charge</p>
</td>
<td width="284">
<p align="center">LISTE LIMITATIVE DES TRAVAUX SUSCEPTIBLES de provoquer ces maladies</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Otites moyennes subaiguës.</p>
</td>
<td valign="top" width="76">
<p align="center">6 mois</p>
</td>
<td valign="top" width="284">
<p>Travaux effectués en service aérien.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Otites moyennes chroniques.</p>
</td>
<td valign="top" width="76">
<p align="center">1 an</p>
</td>
<td valign="top" width="284"/>
</tr>
<tr>
<td valign="top" width="246">
<p>Lésions de l'oreille interne.</p>
</td>
<td valign="top" width="76">
<p align="center">1 an</p>
</td>
<td valign="top" width="284"/>
</tr>
<tr>
<td valign="top" width="246">
<p>Le diagnostic dans tous les cas doit être confirmé par des examens cliniques et audiométriques spécifiques.</p>
</td>
<td valign="top" width="76"/>
<td valign="top" width="284"/>
</tr>
</tbody>
</table>
