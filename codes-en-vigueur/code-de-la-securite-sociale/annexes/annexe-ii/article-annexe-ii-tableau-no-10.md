# Article Annexe II : Tableau n° 10

**ULCERATIONS ET DERMITES PROVOQUEES PAR L'ACIDE CHROMIQUE, LES CHROMATES ET BICHROMATES ALCALINS, LE CHROMATE DE ZINC ET LE SULFATE DE CHROME**.

<table>
<tbody>
<tr>
<td width="178">
<p align="center">DÉSIGNATION DES MALADIES</p>
</td>
<td width="59">
<p align="center">DÉLAI DE prise en charge</p>
</td>
<td width="371">
<p align="center">LISTE INDICATIVE DES PRINCIPAUX TRAVAUX SUSCEPTIBLES DE provoquer ces maladies</p>
</td>
</tr>
<tr>
<td valign="top" width="178">
<p>Ulcérations nasales.</p>
</td>
<td valign="top" width="59">
<p align="center">30 jours</p>
</td>
<td rowspan="3" valign="top" width="371">
<p>Préparation, emploi, manipulation de l'acide chromique, des chromates et bichromates alcalins, du chromate de zinc et du sulfate de chrome, notamment : - fabrication de l'acide chromique, des chromates et bichromates alcalins ; - fabrication de pigments (jaune de chrome, etc.) au moyen de chromates ou bichromates alcalins ;- emploi de bichromates alcalins dans le vernissage d'ébénisterie ;- emploi des chromates ou bichromates alcalins comme mordants en teinture ;- tannage au chrome ;- préparation, par procédés photomécaniques, de clichés pour impression ;- chromage électrolytique des métaux.</p>
</td>
</tr>
<tr>
<td valign="top" width="178">
<p>Ulcérations cutanées chroniques ou récidivantes.</p>
</td>
<td valign="top" width="59">
<p align="center">30 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="178">
<p>Lésions eczématiformes récidivant en cas de nouvelle exposition au risque ou confirmées par un test épicutané.</p>
</td>
<td valign="top" width="59">
<p align="center">15 jours</p>
</td>
</tr>
</tbody>
</table>
