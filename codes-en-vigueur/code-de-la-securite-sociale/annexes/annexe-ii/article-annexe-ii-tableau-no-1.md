# Article Annexe II : Tableau n° 1

<div align="center">
<br/>
<p>
<strong>Affections dues au plomb et à ses composés </strong>
</p>
<strong/>
</div>
<div align="center">
<strong/>

<table>
<tbody>
<tr>
<th>
<br/>DÉSIGNATION DES MALADIES <br/>
</th>
<th>
<br/>DÉLAI DE PRISE EN CHARGE <br/>
</th>
<th>
<br/>LISTE INDICATIVE DES PRINCIPAUX TRAVAUX <p>susceptibles de provoquer ces maladies <br/>
</p>
</th>
</tr>
<tr>
<td align="center">
<br/>A. Anémie (hémoglobine sanguine inférieure à 13 g / 100 ml chez l'homme et 12 g / 100 ml chez la femme) avec une ferritinémie normale ou élevée et une plombémie supérieure ou égale à 800 µg / L, confirmée par une deuxième plombémie de même niveau ou par une concentration érythrocytaire de protoporphyrine zinc égale ou supérieure à 40 µg / g d'hémoglobine. <br/>
</td>
<td align="center">
<br/>3 mois <br/>
</td>
<td align="center">
<br/>Extraction, traitement, préparation, emploi, manipulation du plomb, de ses minerais, de ses alliages, de ses combinaisons et de tout produit en renfermant.<br/>
</td>
</tr>
<tr>
<td align="center">
<br/>B. Syndrome douloureux abdominal apyrétique avec constipation, avec plombémie égale ou supérieure à 500 µg / L et confirmée par une deuxième plombémie de même niveau ou une concentration érythrocytaire de protoporphyrine zinc égale ou supérieure à 20 µg / g d'hémoglobine. <br/>
</td>
<td align="center">
<br/>30 jours <br/>
</td>
<td align="center">
<br/>Récupération du vieux plomb. <p>Grattage, brûlage, découpage au chalumeau de matières recouvertes de peintures plombifères.<br/>
</p>
</td>
</tr>
<tr>
<td align="center">
<br/>C. 1. Néphropathie tubulaire, caractérisée par au moins 2 marqueurs biologiques urinaires concordants témoignant d'une atteinte tubulaire proximale (protéinurie de faible poids moléculaire : retinol binding protein (RBP), alpha-1-micro-globulinurie, béta-2-microglobulinurie...), et associée à une plombémie égale ou supérieure à 400 µg / L, confirmée par une deuxième plombémie de même niveau ou une concentration érythrocytaire de protoporphyrine zinc égale ou supérieure à 20 µg / g d'hémoglobine. <br/>
</td>
<td align="center">
<br/>1 an <br/>
</td>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>C. 2. Néphropathie glomérulaire et tubulo-interstitielle confirmée par une albuminurie supérieure à 200 mg / l et associée à deux plombémies antérieures égales ou supérieures à 600 µg / l après exclusion des affections acquises susceptibles d'entraîner une macro albuminurie (complications d'un diabète). <br/>
</td>
<td align="center">
<br/>10 ans (sous réserve d'une durée minimale d'exposition de 10 ans)<br/>
</td>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>D. 1. Encéphalopathie aiguë associant au moins deux des signes suivants : <p>- hallucinations ; </p>
<p>- déficit moteur ou sensitif d'origine centrale ; </p>
<p>- amaurose ; </p>
<p>- coma ; </p>
<p>- convulsions, </p>
<p>avec une plombémie égale ou supérieure à 2 000 µg / L. <br/>
</p>
</td>
<td align="center">
<br/>30 jours <br/>
</td>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>D. 2. Encéphalopathie chronique caractérisée par des altérations des fonctions cognitives constituées par au moins trois des cinq anomalies suivantes : <p>- ralentissement psychomoteur ; </p>
<p>- altération de la dextérité ; </p>
<p>- déficit de la mémoire épisodique ; </p>
<p>- troubles des fonctions exécutives ; </p>
<p>- diminution de l'attention </p>
<p>et ne s'aggravant pas après cessation de l'exposition au risque. </p>
<p>Le diagnostic d'encéphalopathie toxique sera établi, après exclusion des troubles cognitifs liés à la maladie alcoolique, par des tests psychométriques et sera confirmé par la répétition de ces tests au moins 6 mois plus tard et après au moins 6 mois sans exposition au risque. Cette encéphalopathie s'accompagne d'au moins deux plombémies égales ou supérieures à 400 µg / L au cours des années antérieures. <br/>
</p>
</td>
<td align="center">
<br/>1 an <br/>
</td>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>D. 3. Neuropathie périphérique confirmée par un ralentissement de la conduction nerveuse à l'examen électrophysiologique et ne s'aggravant pas après arrêt de l'exposition au risque. <p>L'absence d'aggravation est établie par un deuxième examen électrophysiologique pratiqué au moins 6 mois après le premier et après au moins 6 mois sans exposition au risque. </p>
<p>La neuropathie périphérique s'accompagne d'une plombémie égale ou supérieure à 700 µg / L confirmée par une deuxième plombémie du même niveau ou une concentration érythrocytaire de protoporphyrine zinc égale ou supérieure à 30 µg / g d'hémoglobine. <br/>
</p>
</td>
<td align="center">
<br/>1 an <br/>
</td>
<td align="center"/>
</tr>
<tr>
<td align="center">
<br/>E. Syndrome biologique, caractérisé par une plombémie égale ou supérieure à 500 µg / L associée à une concentration érythrocytaire de protoporphyrine zinc égale ou supérieure à 20 µg / g d'hémoglobine. Ce syndrome doit être confirmé par la répétition des deux examens dans un délai maximal de 2 mois. <p>Les dosages de la plombémie doivent être pratiqués par un organisme habilité conformément à l'article R. 4724-15 du code du travail. <br/>
</p>
</td>
<td align="center">
<br/>30 jours <br/>
</td>
<td align="center"/>
</tr>
</tbody>
</table>

<br/>
</div>
