# Article Annexe II :  Tableau n° 8

**AFFECTIONS CAUSEES PAR LES CIMENTS (ALUMINO-SILICATES DE CALCIUM)**

<table>
<tbody>
<tr>
<td width="181">
<p align="center">DÉSIGNATION DES MALADIES</p>
</td>
<td width="82">
<p align="center">DÉLAI DE<br/>prise en charge</p>
</td>
<td width="342">
<p align="center">LISTE INDICATIVE DES PRINCIPAUX TRAVAUX <br/>susceptibles de provoquer ces maladies</p>
</td>
</tr>
<tr>
<td valign="top" width="181">
<p>Ulcérations, pyodermites.</p>
</td>
<td valign="top" width="82">
<p align="center">30 jours</p>
</td>
<td rowspan="4" valign="top" width="342">
<p>Fabrication, concassage, broyage, ensachage et transport à dos d'homme des ciments. Fabrication, à l'aide de ciments, de matériaux agglomérés et d'objets moulés. Emploi des ciments dans les chantiers du bâtiment et des travaux publics. </p>
</td>
</tr>
<tr>
<td valign="top" width="181">
<p>Dermites eczématiformes récidivant en cas de nouvelle exposition au risque ou confirmées par un test épicutané.</p>
</td>
<td valign="top" width="82">
<p align="center">15 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="181">
<p>Blépharite.</p>
</td>
<td valign="top" width="82">
<p align="center">30 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="181">
<p>Conjonctivite.</p>
</td>
<td valign="top" width="82">
<p align="center">30 jours</p>
</td>
</tr>
</tbody>
</table>
