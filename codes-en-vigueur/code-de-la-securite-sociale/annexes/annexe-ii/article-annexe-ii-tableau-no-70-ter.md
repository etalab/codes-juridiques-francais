# Article Annexe II : Tableau n° 70 ter

**AFFECTIONS CANCEREUSES BRONCHO-PULMONAIRES PRIMITIVES CAUSEES PAR L'INHALATION DE POUSSIERES DE COBALT ASSOCIEES AU CARBURE DE TUNGSTENE AVANT FRITTAGE**

<table>
<tbody>
<tr>
<td width="246">
<p align="center">DÉSIGNATION DES MALADIES</p>
</td>
<td width="76">
<p align="center">DÉLAI de prise en charge</p>
</td>
<td width="284">
<p align="center">LISTE LIMITATIVE DES TRAVAUX SUSCEPTIBLES de provoquer ces maladies</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Cancer broncho-pulmonaire primitif.</p>
</td>
<td valign="top" width="76">
<p align="center">35 ans (sous réserve d'une exposition de 5 ans minimum)</p>
</td>
<td valign="top" width="284">
<p>Travaux exposant à l'inhalation associée de poussières de cobalt et de carbure de tungstène dans la fabrication des carbures métalliques à un stade avant le frittage (mélange de poudres, compression, rectification et usinage du préfritté).</p>
</td>
</tr>
</tbody>
</table>
