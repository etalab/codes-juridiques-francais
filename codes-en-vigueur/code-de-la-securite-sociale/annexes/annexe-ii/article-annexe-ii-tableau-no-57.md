# Article Annexe II : Tableau n° 57

Affections périarticulaires provoquées par certains gestes et postures de travail

<table>
<tbody>
<tr>
<td>
<p align="center">DÉSIGNATION DES MALADIES</p>
</td>
<td>
<p align="center">DÉLAI<br/>de prise en charge</p>
</td>
<td>
<p align="center">LISTE LIMITATIVE DES TRAVAUX<br/>susceptibles de provoquer ces maladies</p>
</td>
</tr>
<tr>
<td width="246">
<p align="center">- A -</p>
</td>
<td width="76">
<br/>
<br/>
</td>
<td width="283">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="246">
<p align="center">Epaule</p>
</td>
<td width="76">
<br/>
<br/>
</td>
<td width="283">
<br/>
<br/>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Tendinopathie aiguë non rompue non calcifiante avec ou sans enthésopathie de la coiffe des rotateurs.<br/>
</p>
</td>
<td valign="top" width="76">
<p>30 jours</p>
</td>
<td valign="top" width="283">
<p>Travaux comportant des mouvements ou le maintien de l'épaule sans soutien en abduction (**) avec un angle supérieur ou égal à 60° pendant au moins 3 h 30 par jour en cumulé.<br/>
</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Tendinopathie chronique non rompue non calcifiante avec ou sans enthésopathie de la coiffe des rotateurs objectivée par IRM (*).<br/>
</p>
</td>
<td valign="top" width="76">
<p>6 mois (sous réserve d'une durée d'exposition de 6 mois)<br/>
</p>
</td>
<td valign="top" width="283">
<p align="left">Travaux comportant des mouvements ou le maintien de l'épaule sans soutien en abduction (**) : </p>
<p>- avec un angle supérieur ou égal à 60° pendant au moins deux heures par jour en cumulé</p>
<p>ou</p>
<p>- avec un angle supérieur ou égal à 90° pendant au moins une heure par jour en cumulé.</p>
<br/>
</td>
</tr>
<tr>
<td>Rupture partielle ou transfixiante de la coiffe des rotateurs objectivée par IRM (*).</td>
<td>1 an (sous réserve d'une durée d'exposition d'un an)<br/>
</td>
<td>
<p align="left">Travaux comportant des mouvements ou le maintien de l'épaule sans soutien en abduction (**) : </p>
<p>- avec un angle supérieur ou égal à 60° pendant au moins deux heures par jour en cumulé</p>
<p>ou</p>
<p>- avec un angle supérieur ou égal à 90° pendant au moins une heure par jour en cumulé. </p>
<br/>
</td>
</tr>
<tr>
<td colspan="3">
<p align="left">(*) Ou un arthroscanner en cas de contre-indication à l'IRM. </p>
<p>(**) Les mouvements en abduction correspondent aux mouvements entraînant un décollement des bras par rapport au corps.</p>
</td>
</tr>
<tr>
<td width="246">
<p align="center">- B -</p>
</td>
<td width="76">
<br/>
<br/>
</td>
<td width="283">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="246">
<p align="center">Coude</p>
</td>
<td width="76">
<br/>
<br/>
</td>
<td width="283">
<br/>
<br/>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p align="left">Tendinopathie d'insertion des muscles épicondyliens associée ou non à un syndrome du tunnel radial.</p>
</td>
<td valign="top" width="76">
<p>14 jours</p>
</td>
<td valign="top" width="283">
<p>Travaux comportant habituellement des mouvements répétés de préhension ou d'extension de la main sur l'avant-bras ou des mouvements de pronosupination.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p align="left">Tendinopathie d'insertion des muscles épitrochléens </p>
</td>
<td valign="top" width="76">
<p>14 jours</p>
</td>
<td valign="top" width="283">
<p>Travaux comportant habituellement des mouvements répétés d'adduction ou de flexion et pronation de la main et du poignet ou des mouvements de pronosupination.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p align="left">Hygroma : épanchement des bourses séreuses ou atteintes inflammatoires des tissus sous-cutanés des zones d'appui du coude.<br/>
<br/>- forme aiguë ;<br/>
<br/>- forme chronique.</p>
</td>
<td valign="top" width="76">
<p align="left">7 jours<br/>
<br/>90 jours</p>
</td>
<td valign="top" width="283">
<p align="left">Travaux comportant habituellement un appui prolongé sur la face postérieure du coude.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p align="left">Syndrome canalaire du nerf ulnaire dans la gouttière épithrochléo-oléocranienne confirmé par électroneuromyographie (EMG)</p>
</td>
<td valign="top" width="76">
<p align="left">90 jours (sous réserve d'une durée d'exposition de 90 jours)</p>
</td>
<td valign="top" width="283">
<p>Travaux comportant habituellement des mouvements répétitifs et/ou des postures maintenues en flexion forcée.<br/>
<br/>Travaux comportant habituellement un appui prolongé sur la face postérieure du coude. </p>
</td>
</tr>
<tr>
<td width="246">
<p align="center">- C -</p>
</td>
<td width="76">
<br/>
<br/>
</td>
<td width="283">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="246">
<p align="center">Poignet - Main et doigt</p>
</td>
<td width="76">
<br/>
<br/>
</td>
<td width="283">
<br/>
<br/>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Tendinite.</p>
</td>
<td valign="top" width="76">
<p>7 jours</p>
</td>
<td rowspan="2" valign="top" width="283">
<p>Travaux comportant de façon habituelle des mouvements répétés ou prolongés des tendons fléchisseurs ou extenseurs de la main et des doigts.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Ténosynovite.</p>
</td>
<td valign="top" width="76">
<p>7 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Syndrome du canal carpien.</p>
</td>
<td valign="top" width="76">
<p>30 jours</p>
</td>
<td rowspan="2" valign="top" width="283">
<p>Travaux comportant de façon habituelle, soit des mouvements répétés ou prolongés d'extension du poignet ou de préhension de la main, soit un appui carpien, soit une pression prolongée ou répétée sur le talon de la main.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Syndrome de la loge de Guyon.</p>
</td>
<td valign="top" width="76">
<p>30 jours</p>
</td>
</tr>
<tr>
<td width="246">
<p align="center">- D -</p>
</td>
<td width="76">
<br/>
<br/>
</td>
<td width="283">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="246">
<p align="center">Genou</p>
</td>
<td width="76">
<br/>
<br/>
</td>
<td width="283">
<br/>
<br/>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Syndrome de compression du nerf sciatique poplité externe.</p>
</td>
<td valign="top" width="76">
<p>7 jours</p>
</td>
<td valign="top" width="283">
<p>Travaux comportant de manière habituelle une position accroupie prolongée.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Hygromas :</p>
</td>
<td valign="top" width="76"/>
<td valign="top" width="283"/>
</tr>
<tr>
<td valign="top" width="246">
<p>- hygroma aigu des bourses séreuses ou atteinte inflammatoire des tissus sous-cutanés des zones d'appui du genou ;</p>
</td>
<td valign="top" width="76">
<p>7 jours</p>
</td>
<td valign="top" width="283">
<p>Travaux comportant de manière habituelle un appui prolongé sur le genou.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>- hygroma chronique des bourses séreuses.</p>
</td>
<td valign="top" width="76">
<p>90 jours</p>
</td>
<td valign="top" width="283">
<p>Travaux comportant de manière habituelle un appui prolongé sur le genou.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Tendinite sous-quadricipitale ou rotulienne.</p>
</td>
<td valign="top" width="76">
<p>7 jours</p>
</td>
<td valign="top" width="283">
<p>Travaux comportant de manière habituelle des mouvements répétés d'extension ou de flexion prolongées du genou.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Tendinite de la patte d'oie.</p>
</td>
<td valign="top" width="76">
<p>7 jours</p>
</td>
<td valign="top" width="283">
<p>Travaux comportant de manière habituelle des mouvements répétés d'extension ou de flexion prolongées du genou.</p>
</td>
</tr>
<tr>
<td width="246">
<p align="center">- E -</p>
</td>
<td width="76">
<br/>
<br/>
</td>
<td width="283">
<br/>
<br/>
</td>
</tr>
<tr>
<td width="246">
<p align="center">Cheville et pied</p>
</td>
<td width="76">
<br/>
<br/>
</td>
<td width="283">
<br/>
<br/>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Tendinite achiléenne.</p>
</td>
<td valign="top" width="76">
<p>7 jours</p>
</td>
<td valign="top" width="283">
<p>Travaux comportant de manière habituelle des efforts pratiqués en station prolongée sur la pointe des pieds.</p>
</td>
</tr>
</tbody>
</table>
