# Article Annexe II : Tableau n° 39

**MALADIES PROFESSIONNELLES ENGENDREES PAR LE BIOXYDE DE MANGANESE**

Date de création : 9 janvier 1958.

Dernière mise à jour :

<table>
<tbody>
<tr>
<td width="246">
<p align="center">DÉSIGNATION DES MALADIES</p>
</td>
<td width="76">
<p align="center">DÉLAI de prise en charge</p>
</td>
<td width="284">
<p align="center">LISTE INDICATIVE DES PRINCIPAUX TRAVAUX susceptibles de provoquer ces maladies</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Syndrome neurologique du type parkinsonien.</p>
</td>
<td valign="top" width="76">
<p align="center">1 an</p>
</td>
<td valign="top" width="284">
<p>Extraction, concassage, broyage, tamisage, ensachage et mélange à l'état sec du bioxyde de manganèse, notamment dans la fabrication des piles électriques.</p>
</td>
</tr>
<tr>
<td valign="top" width="246"/>
<td valign="top" width="76"/>
<td valign="top" width="284">
<p>Emploi du bioxyde de manganèse pour le vieillissement des tuiles.</p>
</td>
</tr>
<tr>
<td valign="top" width="246"/>
<td valign="top" width="76"/>
<td valign="top" width="284">
<p>Emploi du bioxyde de manganèse pour la fabrication du verre.</p>
</td>
</tr>
<tr>
<td valign="top" width="246"/>
<td valign="top" width="76"/>
<td valign="top" width="284">
<p>Broyage et ensachage des scories Thomas renfermant du bioxyde de manganèse.</p>
</td>
</tr>
</tbody>
</table>
