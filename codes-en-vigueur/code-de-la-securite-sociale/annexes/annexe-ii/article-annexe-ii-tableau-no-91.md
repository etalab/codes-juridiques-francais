# Article Annexe II : Tableau n° 91

**Broncho-pneumopathie chronique obstructive du mineur de charbon**

<table>
<tbody>
<tr>
<td width="246">
<p align="center">DÉSIGNATION DE LA MALADIE</p>
</td>
<td width="76">
<p align="center">DÉLAI de prise en charge</p>
</td>
<td width="284">
<p align="center">LISTE LIMITATIVE DES TRAVAUX SUSCEPTIBLES de provoquer cette maladie</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Broncho-pneumopathie chronique obstructive entraînant un déficit respiratoire chronique. Elle est caractérisée par l'association de signes cliniques tels que dyspnée, toux, hypersécrétion bronchique et d'un syndrome ventilatoire de type obstructif avec un volume expiratoire maximal par seconde (VEMS) abaissé d'au moins 30 % par rapport à la valeur moyenne théorique. Cet abaissement doit être constaté en dehors de tout épisode aigu.</p>
</td>
<td valign="top" width="76">
<p align="center">10 ans (sous réserve d'une durée d'exposition de 10 ans)</p>
</td>
<td valign="top" width="284">
<p>Travaux au fond dans les mines de charbon.</p>
</td>
</tr>
</tbody>
</table>
