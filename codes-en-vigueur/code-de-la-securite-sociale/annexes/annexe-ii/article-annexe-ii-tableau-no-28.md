# Article Annexe II : Tableau n° 28

**<font size="1">
<br/>ANKYLOSTOMOSE PROFESSIONNELLE</font>**

<font size="1">
<strong>ANEMIE ENGENDREE PAR L'ANKYLOSTOMOSE DUODENALE</strong>
</font>

Date de création : 11 février 1949.

<table>
<tbody>
<tr>
<td width="246">
<p align="center">DÉSIGNATION DE LA MALADIE</p>
</td>
<td width="76">
<p align="center">DÉLAI de prise en charge</p>
</td>
<td width="284">
<p align="center">LISTE LIMITATIVE DES TRAVAUX SUSCEPTIBLES de provoquer cette maladie</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Anémie, confirmée par la présence de plus de 200 œufs d'ankylostome par centimètre cube de selles, un nombre de globules rouges égal ou inférieur à 3 500 000 par millimètre cube et un taux d'hémoglobine inférieur à 70 %.</p>
</td>
<td valign="top" width="76">
<p align="center">3 mois</p>
</td>
<td valign="top" width="284">
<p>Travaux souterrains effectués à des températures égales ou supérieures à 20 °C.</p>
</td>
</tr>
</tbody>
</table>
