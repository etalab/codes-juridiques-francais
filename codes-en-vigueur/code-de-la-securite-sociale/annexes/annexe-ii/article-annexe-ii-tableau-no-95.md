# Article Annexe II : Tableau n° 95

**Affections professionnelles de mécanisme allergique provoquées par les protéines du latex (ou caoutchouc naturel)**

<table>
<tbody>
<tr>
<td width="246">
<p align="center">DÉSIGNATION DE LA MALADIE</p>
</td>
<td width="76">
<p align="center">DÉLAI de prise en charge</p>
</td>
<td width="284">
<p align="center">LISTE INDICATIVE DES PRINCIPAUX TRAVAUX susceptibles de provoquer ces maladies</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Urticaire de contact ayant récidivé après nouvelle exposition au risque et confirmée par un test.</p>
</td>
<td valign="top" width="76">
<p align="center">7 jours</p>
</td>
<td rowspan="4" valign="top" width="284">
<p>Préparation, emploi et manipulation du latex naturel et des produits en renfermant, notamment : - production et traitement du latex naturel ; - fabrication et utilisation d'objets en latex naturel.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Rhinite, asthme, conjonctivite aiguë bilatérale, ayant récidivé après nouvelle exposition au risque et confirmés par un test.</p>
</td>
<td valign="top" width="76">
<p align="center">7 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Réactions allergiques systémiques telles que : urticaire géante, œdème de Quincke, choc anaphylactique, survenus à l'occasion d'une exposition au latex.</p>
</td>
<td valign="top" width="76">
<p align="center">3 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Lésions eczématiformes ayant récidivé après nouvelle exposition au risque ou confirmées par un test épicutané positif.</p>
</td>
<td valign="top" width="76">
<p align="center">15 jours</p>
</td>
</tr>
</tbody>
</table>
