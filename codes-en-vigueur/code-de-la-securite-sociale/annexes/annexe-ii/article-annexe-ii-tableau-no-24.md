# Article Annexe II : Tableau n° 24

**BRUCELLOSES PROFESSIONNELLES**.

Date de création : 18 juillet 1945.

Dernière mise à jour : 28 janvier 1982

<table>
<tbody>
<tr>
<td width="265">
<p align="center">DÉSIGNATION DES MALADIES</p>
</td>
<td width="86">
<p align="center">DÉLAI de prise en charge</p>
</td>
<td width="254">
<p align="center">LISTE LIMITATIVE des travaux susceptibles de provoquer ces maladies</p>
</td>
</tr>
<tr>
<td valign="top" width="265">
<p>Brucellose aiguë avec septicémie : tableau de fièvre ondulante sudoro-algique ; tableau pseudo-grippal ; tableau pseudo-typhoïdique.</p>
</td>
<td valign="top" width="86">
<p align="center">2 mois</p>
</td>
<td valign="top" width="254">
<p>Travaux exposant au contact avec des caprins, ovins, bovins, porcins, avec leurs produits ou leurs déjections.</p>
</td>
</tr>
<tr>
<td valign="top" width="265">
<p>Brucellose subaiguë avec focalisation : monoarthrite aiguë fébrile, polyarthrite ; bronchite, pneumopathie ; réaction neuro-méningée ; formes hépato-spléniques subaiguës.</p>
</td>
<td valign="top" width="86">
<p align="center">2 mois</p>
</td>
<td valign="top" width="254">
<p>Travaux exécutés dans les laboratoires servant au diagnostic de la brucellose, à la préparation des antigènes brucelliens ou des vaccins anti-brucelliens, ainsi que dans les laboratoires vétérinaires.</p>
</td>
</tr>
<tr>
<td valign="top" width="265">
<p>Brucellose chronique : arthrite séreuse ou suppurée, ostéo-arthrite, ostéite, spondylodiscite, sacrocoxite ; orchite, épididymite, prostatite, salpingite ; bronchite, pneumopathie, pleurésie sérofibrineuse ou purulente ; hépatite ; anémie, purpura, hémorragie, adénopathie ; néphrite ; endocardite, phlébite ; réaction méningée, méningite, arachnoïdite, méningo-encéphalite, myélite, névrite radiculaire ; manifestations cutanées d'allergie ; manifestations psychopathologiques : asthénie profonde associée ou non à un syndrome dépressif.</p>
</td>
<td valign="top" width="86">
<p align="center">1 an</p>
</td>
<td valign="top" width="254"/>
</tr>
</tbody>
</table>

NOTA - L'origine brucellienne des manifestations aiguës ou subaiguës est démontrée par l'isolement du germe, ou par les résultats combinés de deux réactions sérologiques utilisées par l'organisation mondiale de la santé (OMS) quel que soit leur taux.

Les manifestations chroniques de la brucellose doivent être associées à une intradermo-réaction positive à un allergène brucellien avec ou sans réaction sérologique positive.
