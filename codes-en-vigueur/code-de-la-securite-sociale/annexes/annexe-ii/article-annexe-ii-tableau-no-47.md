# Article Annexe II : Tableau n° 47

**AFFECTIONS PROFESSIONNELLES PROVOQUEES PAR LES POUSSIERES DE BOIS**.

<table>
<tbody>
<tr>
<td width="246">
<p align="center">DÉSIGNATION DES MALADIES</p>
</td>
<td width="76">
<p align="center">DÉLAI de prise en charge</p>
</td>
<td width="283">
<p align="center">LISTE LIMITATIVE DES TRAVAUX SUSCEPTIBLES de provoquer ces maladies</p>
</td>
</tr>
<tr>
<td width="246">
<p align="center">- A -</p>
</td>
<td width="76">
<br/>
</td>
<td width="283">
<p align="center">- A -</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Lésions eczématiformes récidivant en cas de nouvelle exposition au risque ou confirmées par un test épicutané</p>
</td>
<td valign="top" width="76">
<p align="center">15 jours</p>
</td>
<td valign="top" width="283">
<p>Manipulation, traitement et usinage des bois et tous travaux exposant aux poussières de bois.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Conjonctivite récidivant en cas de nouvelle exposition au risque ou confirmée par test</p>
</td>
<td valign="top" width="76">
<p align="center">7 jours</p>
</td>
<td valign="top" width="283"/>
</tr>
<tr>
<td valign="top" width="246">
<p>Rhinite récidivant en cas de nouvelle exposition au risque ou confirmée par test</p>
</td>
<td valign="top" width="76">
<p align="center">7 jours</p>
</td>
<td valign="top" width="283"/>
</tr>
<tr>
<td valign="top" width="246">
<p>Asthme objectivé par explorations fonctionnelles respiratoires récidivant en cas de nouvelle exposition au risque ou confirmé par test</p>
</td>
<td valign="top" width="76">
<p align="center">7 jours</p>
</td>
<td valign="top" width="283"/>
</tr>
<tr>
<td valign="top" width="246">
<p>Syndrome respiratoire avec dyspnée, toux, expectoration, récidivant après nouvelle exposition au risque, dont l'étiologie professionnelle est confirmée par la présence dans le sérum d'anticorps précipitant permettant d'identifier l'agent pathogène correspondant au produit responsable.</p>
</td>
<td valign="top" width="76">
<p align="center">30 jours</p>
</td>
<td valign="top" width="283"/>
</tr>
<tr>
<td valign="top" width="246">
<p>Fibrose pulmonaire avec signes radiologiques et troubles respiratoires confirmés par l'exploration fonctionnelle lorsqu'il y a des signes immunologiques significatifs.</p>
</td>
<td valign="top" width="76">
<p align="center">1 an</p>
</td>
<td valign="top" width="283"/>
</tr>
<tr>
<td width="246">
<p align="center">- B -</p>
</td>
<td width="76">
<br/>
</td>
<td width="283">
<p align="center">- B -</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Cancer primitif : carcinome des fosses nasales, de l'ethmoïde et des autres sinus de la face.</p>
</td>
<td valign="top" width="76">
<p align="center">40 ans sous réserve d'une durée d'exposition de 5 ans</p>
</td>
<td valign="top" width="283">
<p>Travaux exposant à l'inhalation des poussières de bois, notamment : - travaux d'usinage des bois tels que sciage, fraisage, rabotage, perçage et ponçage ; - travaux effectués dans les locaux où sont usinés les bois.</p>
</td>
</tr>
</tbody>
</table>
