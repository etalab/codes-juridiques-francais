# Article Annexe II : Tableau n° 30

**AFFECTIONS PROFESSIONNELLE CONSECUTIVES A L'INHALATION DES POUSSIERES D'AMIANTE**.

Date de création : 3 août 1945.

Dernière mise à jour : 23 juin 1985.

Délais de prise en charge fixés sous réserve des dispositions des articles D. 461-5 à D. 461-24 et notamment des articles D. 461-23 et D. 461-24.

<table>
<tbody>
<tr>
<td width="246">
<p align="center">DÉSIGNATION DES MALADIES</p>
</td>
<td width="76">
<p align="center">DÉLAI de prise en charge</p>
</td>
<td width="283">
<p align="center">LISTE INDICATIVE DES PRINCIPAUX TRAVAUX susceptibles de provoquer ces maladies</p>
</td>
</tr>
<tr>
<td width="246">
<br/>
</td>
<td width="76">
<br/>
</td>
<td width="283">
<p align="center">Cette liste est commune à l'ensemble des affections désignées aux paragraphes A, B, C, D et E</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>A. - Asbestose : fibrose pulmonaire diagnostiquée sur des signes radiologiques spécifiques, qu'il y ait ou non des modifications des explorations fonctionnelles respiratoires. Complications : insuffisance respiratoire aiguë, insuffisance ventriculaire droite.</p>
</td>
<td valign="top" width="76">
<p align="center">35 ans (sous réserve d'une durée d'exposition de 2 ans)</p>
</td>
<td rowspan="8" valign="top" width="283">
<p>Travaux exposant à l'inhalation de poussières d'amiante, notamment : - extraction, manipulation et traitement de minerais et roches amiantifères. Manipulation et utilisation de l'amiante brut dans les opérations de fabrication suivantes : - amiante-ciment ; amiante-plastique ; amiante-textile ; amiante-caoutchouc ; carton, papier et feutre d'amiante enduit ; feuilles et joints en amiante ; garnitures de friction contenant de l'amiante ; produits moulés ou en matériaux à base d'amiante et isolants. Travaux de cardage, filage, tissage d'amiante et confection de produits contenant de l'amiante. Application, destruction et élimination de produits à base d'amiante : - amiante projeté ; calorifugeage au moyen de produits contenant de l'amiante ; démolition d'appareils et de matériaux contenant de l'amiante, déflocage. Travaux de pose et de dépose de calorifugeage contenant de l'amiante. Travaux d'équipement, d'entretien ou de maintenance effectués sur des matériels ou dans des locaux et annexes revêtus ou contenant des matériaux à base d'amiante. Conduite de four. Travaux nécessitant le port habituel de vêtements contenant de l'amiante.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>B. - Lésions pleurales bénignes avec ou sans modifications des explorations fonctionnelles respiratoires :</p>
</td>
<td valign="top" width="76">
<br/>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>- plaques calcifiées ou non péricardiques ou pleurales, unilatérales ou bilatérales, lorsqu'elles sont confirmées par un examen tomodensitométrique ;</p>
</td>
<td valign="top" width="76">
<p align="center">40 ans</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>- pleurésie exsudative ;</p>
</td>
<td valign="top" width="76">
<p align="center">35 ans (sous réserve d'une durée d'exposition de 5 ans)</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>- épaississement de la plèvre viscérale, soit diffus soit localisé lorsqu'il est associé à des bandes parenchymateuses ou à une atélectasie par enroulement. Ces anomalies constatées devront être confirmées par un examen tomodensitométrique.</p>
</td>
<td valign="top" width="76">
<p align="center">35 ans (sous réserve d'une durée d'exposition de 5 ans)</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>C. - Dégénérescence maligne bronchopulmonaire compliquant les lésions parenchymateuses et pleurales bénignes ci-dessus mentionnées.</p>
</td>
<td valign="top" width="76">
<p align="center">35 ans (sous réserve d'une durée d'exposition de 5 ans)</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>D. - Mésothéliome malin primitif de la plèvre, du péritoine, du péricarde.</p>
</td>
<td valign="top" width="76">
<p align="center">40 ans</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>E. - Autres tumeurs pleurales primitives.</p>
</td>
<td valign="top" width="76">
<p align="center">40 ans (sous réserve d'une durée d'exposition de 5 ans)</p>
</td>
</tr>
</tbody>
</table>
