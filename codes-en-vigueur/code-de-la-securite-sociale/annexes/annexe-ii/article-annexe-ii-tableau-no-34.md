# Article Annexe II : Tableau n° 34

**Affections provoquées par les phosphates, pyrophosphates et thiophosphates d'alcoyle, d'aryle ou d'alcoylaryle et autres organophosphores, anticholinestérasiques ainsi que par les phosphoramides et carbamates hétérocycliques anticholinestérasiques**

Date de création : 21 octobre 1951.

<table>
<tbody>
<tr>
<td width="246">
<p align="center">DÉSIGNATION DES MALADIES</p>
</td>
<td width="76">
<p align="center">DÉLAI de prise en charge</p>
</td>
<td width="284">
<p align="center">LISTE INDICATIVE DES PRINCIPAUX TRAVAUX susceptibles de provoquer ces maladies</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>A. - Troubles digestifs : crampes abdominales, hypersalivation, nausées ou vomissements, diarrhée.</p>
</td>
<td valign="top" width="76">
<p align="center">3 jours</p>
</td>
<td rowspan="5" valign="top" width="284">
<p>Toute préparation ou manipulation des phosphates, pyrophosphates et thiophosphates d'alcoyle, d'aryle ou d'alcoylaryle et autres organophosphorés anticholinestérasiques ainsi que des phosphoramides et carbamates hétérocycliques anticholinestérasiques.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>B. - Troubles respiratoires : dyspnée asthmatiforme, oedème broncho-alvéolaire.</p>
</td>
<td valign="top" width="76">
<p align="center">3 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>C. - Troubles nerveux : céphalées, vertiges, confusion mentale accompagnée de myosis.</p>
</td>
<td valign="top" width="76">
<p align="center">3 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>D. - Troubles généraux et vasculaires : asthénie, bradycardie et hypotension, amblyopie. Le diagnostic sera confirmé dans tous les cas (A B, C, D) par un abaissement significatif du taux de la cholinestérase sérique et de l'acétylcholinestérase des globules rouges, à l'exception des affections professionnelles provoquées par les carbamates.</p>
</td>
<td valign="top" width="76">
<p align="center">3 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>E. - Syndrome biologique caractérisé par un abaissement significatif de l'acétylcholinestérase des globules rouges.</p>
</td>
<td valign="top" width="76">
<p align="center">3 jours</p>
</td>
</tr>
</tbody>
</table>
