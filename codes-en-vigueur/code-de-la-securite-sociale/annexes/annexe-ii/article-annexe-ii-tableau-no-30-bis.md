# Article Annexe II : Tableau n° 30 bis

<div align="left"/>

**CANCER BRONCHO-PULMONAIRE PROVOQUE PAR L'INHALATION DE POUSSIERES D'AMIANTE**.

<div align="left"/>
<div align="left">
<br/>
</div>
<div align="center"/>
<div align="center"/>
<div align="center">
<br/>
<br/>
</div>
<div align="center">
<table>
<tbody>
<tr>
<td width="246">
<p align="center">DÉSIGNATION DE LA MALADIE</p>
</td>
<td width="76">
<p align="center">DÉLAI de prise en charge</p>
</td>
<td width="283">
<p align="center">LISTE LIMITATIVE DES TRAVAUX SUSCEPTIBLES de provoquer cette maladie</p>
</td>
</tr>
<tr>
<td rowspan="9" valign="top" width="246">
<p>Cancer broncho-pulmonaire primitif.</p>
</td>
<td rowspan="9" valign="top" width="76">
<p align="center">40 ans (sous réserve d'une durée d'exposition de 10 ans)</p>
</td>
<td valign="top" width="283">
<p>Travaux directement associés à la production des matériaux contenant de l'amiante.</p>
</td>
</tr>
<tr>
<td valign="top" width="283">
<p>Travaux nécessitant l'utilisation d'amiante en vrac.</p>
</td>
</tr>
<tr>
<td valign="top" width="283">
<p>Travaux d'isolation utilisant des matériaux contenant de l'amiante.</p>
</td>
</tr>
<tr>
<td valign="top" width="283">
<p>Travaux de retrait d'amiante.</p>
</td>
</tr>
<tr>
<td valign="top" width="283">
<p>Travaux de pose et de dépose de matériaux isolants à base d'amiante.</p>
</td>
</tr>
<tr>
<td valign="top" width="283">
<p>Travaux de construction et de réparation navale.</p>
</td>
</tr>
<tr>
<td valign="top" width="283">
<p>Travaux d'usinage, de découpe et de ponçage de matériaux contenant de l'amiante.</p>
</td>
</tr>
<tr>
<td valign="top" width="283">
<p>Fabrication de matériels de friction contenant de l'amiante.</p>
</td>
</tr>
<tr>
<td valign="top" width="283">
<p>Travaux d'entretien ou de maintenance effectués sur des équipements contenant des matériaux à base d'amiante.</p>
</td>
</tr>
</tbody>
</table>
</div>
