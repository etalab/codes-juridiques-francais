# Article Annexe II : Tableau n° 21

**INTOXICATION PROFESSIONNELLE PAR L'HYDROGENE ARSENIE**.

Date de création : 20 décembre 1942.

Dernière mise à jour : 15 septembre 1955.

<table>
<tbody>
<tr>
<td width="242">
<p align="center">DÉSIGNATION DES MALADIES</p>
</td>
<td width="81">
<p align="center">DÉLAI DE prise en charge</p>
</td>
<td width="282">
<p align="center">LISTE INDICATIVE DES PRINCIPAUX TRAVAUX susceptibles de provoquer ces maladies</p>
</td>
</tr>
<tr>
<td valign="top" width="242">
<p>Hémoglobinurie.</p>
</td>
<td valign="top" width="81">
<p align="center">15 jours</p>
</td>
<td rowspan="4" valign="top" width="282">
<p>Travaux exposant aux émanations d'hydrogène arsénié, notamment : - traitement des minerais arsenicaux ; - préparation et emploi des arséniures métalliques ; - décapage des métaux ; - détartrage des chaudières ; - gonflement des ballons avec de l'hydrogène impur.</p>
</td>
</tr>
<tr>
<td valign="top" width="242">
<p>Ictère avec hémolyse.</p>
</td>
<td valign="top" width="81">
<p align="center">15 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="242">
<p>Néphrite azotémique.</p>
</td>
<td valign="top" width="81">
<p align="center">30 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="242">
<p>Accidents aigus (coma), en dehors des cas considérés comme accidents du travail.</p>
</td>
<td valign="top" width="81">
<p align="center">3 jours</p>
</td>
</tr>
</tbody>
</table>
