# Article Annexe II : Tableau n° 43

**AFFECTIONS PROVOQUEES PAR L'ALDEHYDE FORMIQUE ET SES POLYMERES**.

<table>
<tbody>
<tr>
<td width="246">
<p align="center">DÉSIGNATION DES MALADIES</p>
</td>
<td width="76">
<p align="center">DÉLAI de prise en charge</p>
</td>
<td width="283">
<p align="center">LISTE INDICATIVE DES PRINCIPAUX TRAVAUX susceptibles de provoquer ces maladies</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Dermatites irritatives  </p>
</td>
<td valign="top" width="76">
<p align="center">7 jours</p>
</td>
<td rowspan="4" valign="top" width="283">
<p>Préparation, emploi et manipulation de l'aldéhyde formique, de ses solutions (formol) et de ses polymères, notamment : - fabrication de substances chimiques, à partir de l'aldéhyde formique ; - fabrication de matières plastiques à base de formol ; - travaux de collage exécutés avec des matières plastiques renfermant un excès de formol ; - opérations de désinfection ; - apprêtage des peaux ou des tissus.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Lésions eczématiformes récidivant en cas de nouvelle exposition au risque ou confirmées par un test épicutané.</p>
</td>
<td valign="top" width="76">
<p align="center">15 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Rhinite récidivant en cas de nouvelle exposition au risque ou confirmée par test.</p>
</td>
<td valign="top" width="76">
<p align="center">7 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Asthme objectivé par explorations fonctionnelles respiratoires récidivant en cas de nouvelle exposition au risque ou confirmé par test.</p>
</td>
<td valign="top" width="76">
<p align="center">7 jours</p>
</td>
</tr>
</tbody>
</table>
