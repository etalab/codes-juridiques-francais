# Article Annexe II : Tableau n° 14

**AFFECTIONS PROVOQUEES PAR LES DERIVES NITRES DU PHENOL (DINITROPHENOLS, DINITRO-ORTHOCRESOLS, DINOSEB), PAR LE PENTACHLOROPHéNOL, LES PENTACHLOROPHENATES ET PAR LES DERIVES HALOGENES DE L'HYDROXYBENZONITRILE (BROMOXYRIL, IOXYNIL)**

Date de création : 14 décembre 1938.

Dernière mise à jour : 23 juin 1985. <table>
<tbody>
<tr>
<td width="242">
<p align="center">DÉSIGNATION DES MALADIES</p>
</td>
<td width="81">
<p align="center">DÉLAI DE prise en charge</p>
</td>
<td width="282">
<p align="center">LISTE INDICATIVE DES PRINCIPAUX TRAVAUX susceptibles de provoquer ces maladies</p>
</td>
</tr>
<tr>
<td valign="top" width="242">
<p>A. - Intoxication suraiguë avec hyperthermie, oedème pulmonaire, éventuellement atteinte hépatique, rénale et myocardique.</p>
</td>
<td valign="top" width="81">
<p align="center">3 jours</p>
</td>
<td rowspan="6" valign="top" width="282">
<p>Préparation, emploi, manipulation des dérivés nitrés du phénol (dinitrophénols, dinitro-orthocrésol, dinoseb, leurs homologues et leurs sels) notamment : - fabrication des produits précités. - fabrication de matières colorantes au moyen des produits précités ; - préparation et manipulation d'explosifs renfermant l'un ou l'autre des produits précités ; - travaux de désherbage utilisant les produits précités ; - travaux antiparasitaires entraînant la manipulation de ces produits précités. Préparation, emploi, manipulation des dérivés halogénés de l'hydroxybenzonitrile notamment : - fabrication des produits précités ; - fabrication et conditionnement des pesticides en contenant. Préparation, manipulation, emploi du pentachlorophénol, des pentachlorophénates » ainsi que des produits en renfermant notamment au cours des travaux ci-après : - trempage du bois ; - empilage du bois fraîchement trempé ; - pulvérisation du produit ; - préparation des peintures en contenant ; - lutte contre les xylophages ; - traitement des charpentes en place par des préparations associant du pentachlorophénol, ses homologues et ses sels à du lindane.</p>
</td>
</tr>
<tr>
<td valign="top" width="242">
<p>B. - Intoxication aiguë ou subaiguë avec asthénie, amaigrissement rapide, hypersudation suivie d'hyperthermie avec gêne respiratoire.</p>
</td>
<td valign="top" width="81">
<p align="center">7 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="242">
<p>C. - Manifestations digestives (douleurs abdominales, vomissements, diarrhées) associées à la présence du toxique ou de ses métabolites dans le sang ou les urines.</p>
</td>
<td valign="top" width="81">
<p align="center">7 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="242">
<p>D. - Irritation des voies aériennes supérieures et conjonctivites.</p>
</td>
<td valign="top" width="81">
<p align="center">7 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="242">
<p>E. - Dermites irritatives.</p>
</td>
<td valign="top" width="81">
<p align="center">7 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="242">
<p>F. - Syndrome biologique caractérisé par : Neutropénie franche (moins de 1.000 polynucléaires neutrophiles par mm3) liée à des préparations associant du pentachlorophénol, ses homologues ou ses sels, à du lindane.</p>
</td>
<td valign="top" width="81">
<p align="center">90 jours</p>
</td>
</tr>
</tbody>
</table>
