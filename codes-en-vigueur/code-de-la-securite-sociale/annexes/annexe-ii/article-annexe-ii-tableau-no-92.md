# Article Annexe II : Tableau n° 92

**Infections professionnelles à Streptococcus suis**

<table>
<tbody>
<tr>
<td width="246">
<p align="center">DÉSIGNATION DES MALADIES</p>
</td>
<td width="76">
<p align="center">DÉLAI de prise en charge</p>
</td>
<td width="284">
<p align="center">LISTE LIMITATIVE DES TRAVAUX SUSCEPTIBLES de provoquer ces maladies</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Méningite purulente avec bactériémie, accompagnée le plus souvent d'une atteinte cochléo-vestibulaire : surdité de perception unie ou bilatérale, avec acouphènes et troubles de l'équilibre (vertiges et ataxie).</p>
</td>
<td valign="top" width="76">
<p align="center">25 jours</p>
</td>
<td valign="top" width="284">
<p>Travaux exposant au contact de porcs, de leur viande, carcasses, os, abats ou sang, dans les élevages de porcs, les abattoirs, les entreprises d'équarrissage, les boucheries, charcuteries, triperies, boyauderies, cuisines, entreprises de transport de porcs ou viande de porc.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Atteinte cochléo-vestibulaire aiguë et ses complications cochléaires (troubles de l'audition irréversibles).</p>
</td>
<td valign="top" width="76">
<p align="center">25 jours</p>
</td>
<td valign="top" width="284">
<p>Travaux d'inspection de viande de porc, travaux vétérinaires, travaux de laboratoire au contact de porc.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Septicémie isolée, tableau de coagulopathie intravasculaire disséminée.</p>
</td>
<td valign="top" width="76">
<p align="center">25 jours</p>
</td>
<td valign="top" width="284">
<p>Travaux de l'industrie alimentaire avec fabrication d'aliments à base de viande de porc.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Arthrites inflammatoires ou septiques.</p>
</td>
<td valign="top" width="76">
<p align="center">25 jours</p>
</td>
<td valign="top" width="284"/>
</tr>
<tr>
<td valign="top" width="246">
<p>Endophtalmie, uvéite.</p>
</td>
<td valign="top" width="76">
<p align="center">25 jours</p>
</td>
<td valign="top" width="284"/>
</tr>
<tr>
<td valign="top" width="246">
<p>Myocardite.</p>
</td>
<td valign="top" width="76">
<p align="center">25 jours</p>
</td>
<td valign="top" width="284"/>
</tr>
<tr>
<td valign="top" width="246">
<p>Pneumonie, paralysie faciale.</p>
</td>
<td valign="top" width="76">
<p align="center">25 jours</p>
</td>
<td valign="top" width="284"/>
</tr>
<tr>
<td valign="top" width="246">
<p>Endocardite.</p>
</td>
<td valign="top" width="76">
<p align="center">60 jours</p>
</td>
<td valign="top" width="284"/>
</tr>
<tr>
<td valign="top" width="246">
<p>Dans tous les cas, il est nécessaire de mettre en évidence le Steptococcus suis et de procéder à son typage.</p>
</td>
<td valign="top" width="76"/>
<td valign="top" width="284"/>
</tr>
</tbody>
</table>
