# Article Annexe II : Tableau n° 41

**Maladies engendrées par les bétalactamines (notamment pénicillines et leurs sels) et les céphalosporines**

<table>
<tbody>
<tr>
<td width="246">
<p align="center">DÉSIGNATION DES MALADIES</p>
</td>
<td width="76">
<p align="center">DÉLAI de prise en charge</p>
</td>
<td width="283">
<p align="center">LISTE INDICATIVE DES PRINCIPAUX TRAVAUX susceptibles de provoquer ces maladies</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Lésions eczématiformes récidivant en cas de nouvelle exposition au risque ou confirmées par un test épicutané</p>
</td>
<td valign="top" width="76">
<p align="center">15 jours</p>
</td>
<td rowspan="3" valign="top" width="283">
<p>Travaux comportant la préparation ou l'emploi des bétalactamines (notamment pénicillines et leurs sels) ou des céphalosporines, notamment : - travaux de conditionnement ; - application des traitements.</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Rhinite récidivant en cas de nouvelle exposition au risque ou confirmée par test</p>
</td>
<td valign="top" width="76">
<p align="center">7 jours</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Asthme objectivé par explorations fonctionnelles respiratoires récidivant en cas de nouvelle exposition au risque ou confirmé par test</p>
</td>
<td valign="top" width="76">
<p align="center">7 jours</p>
</td>
</tr>
</tbody>
</table>
