# Article Annexe II : Tableau n° 68

**TULAREMIE**

<table>
<tbody>
<tr>
<td width="246">
<p align="center">DÉSIGNATION DE LA MALADIE</p>
</td>
<td width="76">
<p align="center">DÉLAI de prise en charge</p>
</td>
<td width="284">
<p align="center">LISTE LIMITATIVE DES PRINCIPAUX TRAVAUX susceptibles de provoquer la maladie</p>
</td>
</tr>
<tr>
<td valign="top" width="246">
<p>Syndrome pouvant revêtir soit l'aspect, en tout ou partie, d'une des grandes formes cliniques (brachiale, oculaire, pharyngée, pulmonaire ou typhoïde), soit un aspect atypique. Dans tous les cas, le diagnostic sera authentifié par un examen sérologique spécifique.</p>
</td>
<td valign="top" width="76">
<p>15 jours</p>
</td>
<td valign="top" width="284">
<p>Travaux de gardes-chasse et gardes forestiers exposant notamment au contact des léporidés sauvages. Travaux d'élevage, abattage, transport, manipulation, vente de léporidés, de petits rongeurs et d'animaux à fourrure. Transport et manipulation de peaux. Travaux de laboratoire exposant au contact des léporidés et des petits rongeurs.</p>
</td>
</tr>
</tbody>
</table>
