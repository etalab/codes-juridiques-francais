# Sous-section 2 : Fusion et scission

- [Article A931-4-1](article-a931-4-1.md)
- [Article A931-4-2](article-a931-4-2.md)
- [Article A931-4-3](article-a931-4-3.md)
- [Article A931-4-4](article-a931-4-4.md)
- [Article A931-4-5](article-a931-4-5.md)
