# Article A931-11-1

Outre les documents prévus par le code de commerce, les institutions de prévoyance et les unions d'institutions de prévoyance doivent tenir le livre des balances trimestrielles donnant avant la fin du mois suivant chaque trimestre civil la récapitulation des soldes de tous les comptes ouverts au grand livre général, arrêtés au dernier jour du trimestre civil écoulé.
