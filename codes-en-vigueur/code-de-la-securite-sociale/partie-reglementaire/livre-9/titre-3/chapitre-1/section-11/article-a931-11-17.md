# Article A931-11-17

Les états d'analyse des comptes visés au 3° du premier alinéa de l'article A. 931-11-15 sont les suivants :

C 1.-Résultats techniques par catégories d'opérations ;

C 2.-Engagements et résultats techniques par pays ;

C 3.-Acceptations et cessions en réassurance ;

C 4.-Cotisations par catégories d'opérations et de garanties ;

C 5.-Représentation des engagements réglementés ;

C 6.-Marge de solvabilité ;

C 7.-Provisionnement des rentes en service ;

C 10.-Cotisations et résultats par année de survenance des sinistres ;

C 11.-Sinistres par année de survenance ;

C 12.-Sinistres et résultats par année de souscription ;

C 13.-Part des réassureurs dans les sinistres ;

C 20.-Mouvements des bulletins d'adhésion aux règlements ou des contrats, des capitaux et rentes ;

C 21.-Etat détaillé des provisions techniques ;

C 30.-Cotisations, sinistres et commissions des opérations non-vie dans l'Espace économique européen ;

C 31.-Cotisations des opérations vie dans l'Espace économique européen ;

C 40.-Opérations réalisées pour le compte d'unions d'institutions de prévoyance ;

C 41.-Action sociale.

Ces états sont établis annuellement d'après les comptes définis à l'article A. 931-11-16 et dans la forme fixée en annexe au présent article.

Les opérations réalisées sur l'ensemble du territoire de la République française ainsi que sur le territoire monégasque sont considérées comme opérations réalisées en France.

Les opérations directes à l'étranger, ainsi que celles acceptées, des catégories 20 à 31 de l'article A. 931-11-10, sont assimilées à des opérations pluriannuelles à cotisation unique ou non révisable lorsque les usages de marché conduisent à rattacher les sinistres par exercice de souscription.
