# Article A931-11-15

Le compte rendu détaillé annuel visé au 1° du I de l'article A. 931-11-13 comprend :

1° Les renseignements généraux énumérés à l'annexe au présent article ;

2° Les comptes définis à l'article A. 931-11-16 ;

3° Les états d'analyse des comptes énumérés à l'article A. 931-11-17 ;

4° Les états statistiques relatifs à la protection sociale complémentaire énumérés à l'article D. 931-37.

Il est établi dans la même monnaie que les comptes annuels mentionnés au 2° du I de l'article A. 931-11-13.

Le compte rendu détaillé annuel est certifié par le président du conseil d'administration de l'institution ou de l'union sous la formule suivante : " Le présent document, comprenant X feuillets numérotés, est certifié, sous peine de l'application des sanctions prévues à l'article L. 951-11 du code de la sécurité sociale, conforme aux écritures de l'institution de prévoyance (l'union d'institutions de prévoyance) et aux dispositions de la section 11 du chapitre Ier du titre III du livre IX du code de la sécurité sociale et de son annexe.
