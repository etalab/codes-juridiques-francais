# Article A931-11-11

Le bilan, le compte de résultat et l'annexe doivent être établis conformément aux modèles types annexés au présent article. Ils doivent être utilisés par les institutions de prévoyance et les unions d'institutions de prévoyance dans les conditions suivantes :

1. Les institutions et les unions agréées pour pratiquer les opérations visées au a de l'article L. 931-1 utilisent le modèle de bilan (à l'exception des postes intitulés Non-vie), les parties II et III du modèle de compte de résultat et le modèle d'annexe ;

2. Les institutions et les unions agréées pour pratiquer les opérations visées aux b et/ ou c de l'article L. 931-1 utilisent le modèle de bilan (à l'exception des postes intitulés Vie), les parties I et III du modèle de compte de résultat et le modèle d'annexe ;

3. Les institutions et les unions agréées pour pratiquer simultanément les opérations visées au a et au b de l'article L. 931-1 utilisent le modèle de bilan, les parties I, II et III du modèle de compte de résultat et le modèle d'annexe.
