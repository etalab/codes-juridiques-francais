# Sous-section 2 : Constitution

- [Article A931-1-4](article-a931-1-4.md)
- [Article A931-1-5](article-a931-1-5.md)
- [Article A931-1-6](article-a931-1-6.md)
- [Article A931-1-7](article-a931-1-7.md)
