# Article A941-1-2

L'Autorité de contrôle prudentiel et de résolution détermine le nombre d'exemplaires et les supports matériels utilisés par les institutions pour la fourniture des documents mentionnés à l'article A. 941-1-1.
